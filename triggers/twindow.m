function twindow(tp,tm,name)
%This application  can be used to search for available time windows around
%different GRBs using data from two different detectors

%Written by Banto Balazs, Peter Raffai
%E-mail: bantobalazs@yahoo.com, praffai@bolyai.elte.hu
%Last updated: 03.11.2011
%Inputs: tp - time window after the current GRB
%        tm - time window before the current GRB
%        name - the detector pair's name
%             - for example: 'S6_H1V1_GRBjoblist.txt'

%Output: a .txt job file, which contains for colums: 
% first column: the number 1
% second column: the GPS time where the time window begins
% third column: the GPS time where the time window ends
% the length of the time window

% Description: 
% 
% This Matlab script produces joblists for the GRB search. The script takes
% in three inputs: two of them are the plus and minus times around a GRB
% trigger time, that defines an on-source time window for the search. The
% third input is a file name that corresponds to a chosen pair of detectors
% from the set of {H1, L1, V1}. These input files contain the timing data
% of all the overlapping data segments that were generated in a coincident
% science run of the two detectors.
%
% The user can specify an on-source time window around a GRB trigger time,
% he or she can specify a pair of detectors, and the program goes through
% all the GRBs from the GRBWiki trigger tables
% (https://wiki.ligo.org/Main/GRBWiki), and produces a joblist file
% for all the GRBs that have any coincident science data around their
% trigger time,  within the time window, and from the given pair of
% detectors.
%
% The program determines a job around a GRB trigger by giving the longest
% time window around the trigger time, that is not wider than time window
% specified by the user, and where we also have coincident data from the
% selected pair of detectors.


%Loading 'gps.txt', which consists of two columns. The first is the names 
% of the GRBs, and the second is the GPS time when the GRB trigger started
[grbname,grbgps]=textread('gpst.txt','%s%f');

%Loading the jobblist for different detector pairs
data=load(name);

%Loading the begining and the end of the time windows from the joblist
tb=data(:,2);
te=data(:,3);
%Defining the output filename 
FileName=sprintf('job_%s.txt',name);
HLV=fopen(FileName,'w');

%Searching for the longest available time window around the GRB between the 
%specified interval
for k=1:length(grbgps)
    
    for i=1:length(tb)
        if grbgps(k)>=tb(i) && grbgps(k)<=te(i)
            begint=max(tb(i),grbgps(k)-tm);
            endt=min(te(i),grbgps(k)+tp);
            fprintf(HLV,'%i %i %i %i\n',1,begint,endt,endt-begint);
        end
    end 
end

fclose(HLV);

end