To run the STAMP tutorials, you need to have checked out all of the standard STAMP code, which is in the matapps repository.  If you haven't done this already, here is how.

To check out all of matapps:
svn --username albert.einstein co https://ligo-vcs.phys.uwm.edu/svn/matapps

To check out just stamp2:
svn --username albert.einstein co https://ligo-vcs.phys.uwm.edu/svn/matapps/packages/stochastic/tags/stamp2

Replace 'albert.einstein' with your own LIGO username.  It will take a few hours if you decide to check out all of matapps.
The next thing you need to do is to prepare a startup file for Matlab - this script executes whenever you start Matlab and it is designed to tell Matlab where all of your important code (like the STAMP code is).  Matlab can't execute the code if it doesn't know where it is!

To get your startup file setup, copy the 'startup.m' file in this directory to your home directory.  Create a directory called 'matlab' and move startup.m into that directory.  Then, open startup.m with a text editor and modify the paths to point to YOUR stamp2 directory.

Here are a few commands to help you with getting your startup.m file in the right place:
mkdir ~/matlab
cp startup.m ~/matlab/

Once you're modified the startup.m file and you've got it in the right place, start Matlab and enter 'which clustermap.m'.  If your startup file is working correctly, you should get something like:

>> which clustermap.m
/home/prestegard/matapps/packages/stochastic/trunk/stamp2/src/clustermap.m

Great!  Now you can move on to the tutorials.
