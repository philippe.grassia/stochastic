Instructions

1. This tutorial will do the pre-processing and STAMP analysis all in one step.  As in the previous tutorial, you will run stamp_clustermap_wrapper from within Matlab.  However, it is set up to run with different parameters.  Plots will be produced in the ./plots/ directory and can be viewed as described in tutorial 1.

Note: doing everything in one step is sometimes useful, but not always.  It may be useful in that no intermediate .mat files are written and general bookkeeping is easier, but it can take much longer if the analysis needs to be re-run multiple times.



