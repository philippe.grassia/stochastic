%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameter file for Monte Carlo for LGRB analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STAMP parameters
stochmap true
batch true
mapsize 200

% high-pass parameters
doHighPass1 true
doHighPass2 true
highPassFreq1 32
highPassFreq2 32
highPassOrder1 6
highPassOrder2 6

% overalpping segments
doOverlap true

% do not use folded data
doSidereal false

% do not analyze less than 200s of data
minDataLoadLength 200
 
% do not exclude any additional times
doBadGPSTimes false

% perform a time shift
doShift1 true
doShift2 false
ShiftTime1 1
ShiftTime2 0

% use Hanford-Livingston pair
ifo1 H1
ifo2 L1

% define properties of STAMP pixels
segmentDuration 1
numSegmentsPerInterval 9
ignoreMidSegment true

% duration of Hann window
hannDuration1 1.00
hannDuration2 1.00

% define frequency range and resolution
flow 40
fhigh 2000
deltaF 1

% define standard stochastic parameters
alphaExp 0
fRef 100
simOmegaRef 0
heterodyned false
resampleRate1 4096
resampleRate2 4096
bufferSecs1 2
bufferSecs2 2
nResample1 10
nResample2 10
betaParam1 5
betaParam2 5

% strain channel names
ASQchannel1 LSC-STRAIN
ASQchannel2 LSC-STRAIN

% frame types
frameType1 H1_RDS_C03_L2
frameType2 L1_RDS_C03_L2

% use default calibration
alphaBetaFile1 none
alphaBetaFile2 none
calCavGainFile1 none
calCavGainFile2 none
calResponseFile1 none
calResponseFile2 none

% never mask frequency bins at the pre-processing stage
doFreqMask false
freqsToRemove
nBinsToRemove

% paths to cachefiles
gpsTimesPath1
gpsTimesPath2
frameCachePath1
frameCachePath2

% path to cache files (new cache method)
cacheFile ./cache.mat

% do not use Monte Carlo
doDetectorNoiseSim false

% output file location
outputFilePrefix 
outputfiledir /home/mcoughlin/STAMP/Examples/ex1/matfiles/
outputfilename HL-SID_nspi9_df1_dt1_ts1
storemats false
