gpsTimesPath1 ./cachefiles/
gpsTimesPath2 ./cachefiles/
frameCachePath1 ./cachefiles/
frameCachePath2 ./cachefiles/
outputfiledir ./mats/

doHighPass1 true
doHighPass2 true
doOverlap true

doSidereal false

minDataLoadLength 200
 
doBadGPSTimes false
maxDSigRatio 1.2
minDSigRatio 0.8

doShift1 true
ShiftTime1 1
doShift2 false
ShiftTime2 0

ifo1 H1
ifo2 L1

segmentDuration 1.00

numSegmentsPerInterval 9
ignoreMidSegment true

flow 40
fhigh 2000
deltaF 1.00

alphaExp 0
fRef 100

resampleRate1 4096
resampleRate2 4096

bufferSecs1 2
bufferSecs2 2

ASQchannel1 LSC-STRAIN
ASQchannel2 LSC-STRAIN

frameType1 H1_RDS_C03_L2
frameType2 L1_RDS_C03_L2
frameDuration1 -1
frameDuration2 -1

hannDuration1 1.00
hannDuration2 1.00

nResample1 10
nResample2 10
betaParam1 5
betaParam2 5

highPassFreq1 32
highPassFreq2 32
highPassOrder1 6
highPassOrder2 6

alphaBetaFile1 none
alphaBetaFile2 none
calCavGainFile1 none
calCavGainFile2 none
calResponseFile1 none
calResponseFile2 none

simOmegaRef 0
heterodyned false

doFreqMask false
freqsToRemove
nBinsToRemove

doStampFreqMask false
StampFreqsToRemove 118,119,120,121,122,178,179,180,181,182,239,240,241,328,329,330,331,341,342,343,344,345,346,348,349,599,685,686,687,688,689,690,691,692,693,694,695,696,697,959,1028,1029,1030,1031,1032,1033,1034,1035,1036,1039,1040,1041,1043,1044,1045,1143,1144,1145,1146,1150,1151,1152,1153
StampnBinsToRemove 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
DoStampInj false
StampInjRA 6
StampInjDECL 30

doDetectorNoiseSim false
DetectorNoiseFile LIGOsrdPSD_40Hz.txt

outputFilePrefix 
outputfilename HL-SID_nspi9_df1_dt1_ts1

stochmap true
fft1dataWindow -1
fft2dataWindow -1
startGPS 816065659
endGPS 816065957
kludge 1
ra 6
dec 30
fmin 100
fmax 1200
doPolar false
saveMat false
savePlots false
debug false
doRadon false
doBoxsearch false
doLH false
doRadiometer false
doRadonReconstruction false
saveAutopower false
fixAntennaFactors false
doClusterSearch true
phaseScramble 0
glitchCut 8
bknd_study false
bknd_study_dur 100
stamp_pem false
Autopower true
loudPixel false
pp_seed -1
skypatch false
fastring false
dtheta 0.25
thetamax 10
cluster.doClusterSearch false
cluster.doCombineCluster 0
cluster.NN 15
cluster.NR 4
cluster.NCN 80
cluster.NCR 16
cluster.pixelThreshold 0.75
cluster.tmetric 2
cluster.fmetric 1
glitchCut 8
DQcut 1
DQmatfile /home/stamp/matapps/packages/stochastic/trunk/stamp/input
doCoincidentCut 1
crunch_map 1
