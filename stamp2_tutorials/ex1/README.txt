Instructions

1. Begin by making STAMP mat files. You will need to run stamp_preproc_wrapper.  You shouldn't need to modify anything in this file, but it might be useful to open it up and take a look just to get a feel for simple Matlab code. To run the code, open Matlab and enter 'stamp_preproc_wrapper()' (without the quotes).  This will run the Matlab script 'stamp_preproc_wrapper.m.'  This script pre-processes the raw data from the detectors and cross-correlates the data streams.  The output of this script will be a number of mat files in the 'mats' directory.

2. Now you are ready to run the STAMP analysis. Open Matlab and enter 'stamp_clustermap_wrapper()'. This script will use the mat files you created in step 1 to make STAMP ft-maps of various STAMP statistics.  It will also run a search algorithm on the SNR ft-map.  The output will be several .png plots of STAMP ft-maps and a summary output map.mat file.

If you want to view the plots you have created, you'll need to copy them to your website.  Assuming you are working on the Caltech cluster, you need to do the following:
1. Create a directory called 'public_html' in your home directory (if it doesn't exist already).
2. Move or copy your plots to that directory.
3. You can view the plots in your web browser using the following address: https://ldas-jobs.ligo.caltech.edu/~prestegard/
   (substitute your username in place of 'prestegard')
