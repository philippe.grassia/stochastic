function label = get_name(str)

if strcmp(str,'a') || strcmp(str,'A')
   label = 'log10(A)';
elseif strcmp(str,'lam')
   label = 'log10(\lambda)';
elseif strcmp(str,'r')
   label = 'RA [Hours]';
elseif strcmp(str,'d')
   label = 'Declination [Degrees]';
else
   label = str;
end

