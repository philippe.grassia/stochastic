%this script is meant to study the stochastic sensitivity for the GW150914
%event

analysisType = 'BBHNoTable';
dataSet = 'ThirtyPlusThirtySEOBNRv2';
dataSet = 'ThirtyPlusThirtyIdeal';
plotDir = '.';

[freq,omega_data,sigma_data,fmin,fmax,ff_true,omega_true,sigma_true] = ...
   get_data(analysisType,dataSet,plotDir);

m1 = 30;
m2 = 30;
mc = ((m1*m2)^(3/5))/(m1+m2)^(1/5);
lambda = log10(2.1088e-05);

global sfr;
sfr = 'k';

global waveform;
waveform.form = 'power';
waveform.order = -1;
waveform.tmin = 0.1;

global zinf;
zinf = 0;

global Iz;

constant;

fgmin = 0;
zmax = 6;    % dimensionless --max of the redshift
tmax = 13.5 ; % in Gyr, max time delay considered in the delay integral
dz = 0.01;

% normalizer of delay time distribution
ztmp = 0:dz:12;
Iz_int = zeros(1,length(ztmp));

%Calculating the integrand of the redshift integral

%first calculate the denominator
Ez = @(M,V,z) (M*(1+z).^3+V).^0.5;  % dimensionless
denom = Ez(OmegaM,OmegaV,ztmp).*(1+ztmp).^(1/3);

%next, set up the delay-time integral
tsup = (cosmic_time(10)-cosmic_time(ztmp)); % integral upbound
accu = 10000;
trapez = logspace(-100,0,accu);
t    = transpose(trapez) * tsup;
tf   = ones(accu,1) * cosmic_time(ztmp)+t;
zf   = cosmic_time_reverse(tf); %computing the redshift of formation
tem1 =  Rate_StarForm(sfr,zf)./(1+zf);

%computing the probability distribution for time-delay
pnorm = log(tmax/waveform.tmin);
tem2 = t.^waveform.order./pnorm .* (waveform.tmin < t) .* (t < tmax) ;

%next, compute the integral over time-delay
tem  = tem1 .* tem2;
rate = diff(trapez) * tem(1:end-1,:) .* tsup;

%finally compute the integrand of the redshift integral
Iz = rate./denom;

omega = BBH_no_table(freq',mc,lambda,sfr,waveform,zinf);

%repeat for dynamic BBH for GCs, using maximum <N> from Rodriguez paper
Gyr = 60 * 60 * 24 * 365 * 1e9; %in seconds
NN = 3100 / 12 / Gyr; %specify the average number of dynamic BBH per GC per 12 Gyr

omega3 = BBH_dynamic(freq',NN,1);

baseplotDir = '/home/mcoughlin/Stochastic/MDC/PE/plots_test_mdc';

%plotDir = [baseplotDir '/' dataSet '/' analysisType];
plotDir = baseplotDir;
createpath(plotDir);

params.savePlots = 1;

if params.savePlots

      plotName = [plotDir '/omega'];
      figure;
      loglog(freq,omega,'m');
      hold on
      %loglog(freq,omega3,'b');
      loglog(freq,omega_data,'k--');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

end







