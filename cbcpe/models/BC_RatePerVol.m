function rate = BC_RatePerVol(sfr, waveform, z)

% tdomain = 0:0.001:cosmic_time(10);
% zfdomain = cosmic_time_reverse(tdomain);
% r_sf =  Rate_StarForm(sfr,zf)./(1+zf) ;

if strcmp(waveform.form, 'delta')
    
    rate =  Rate_StarForm(sfr,z)./(1+z);

else

    tsup = (cosmic_time(10)-cosmic_time(z)); % integral upbound

    accu = 10000;
    trapez = logspace(-100,0,accu);

    t    = transpose(trapez) * tsup;
    tf   = ones(accu,1) * cosmic_time(z)+t;
    zf   = cosmic_time_reverse(tf);

    tem1 = Rate_StarForm(sfr,zf)./(1+zf) ;
    tem2 = BC_LifeDistr(waveform,t);
    tem  = tem1 .* tem2;

    rate = diff(trapez) * tem(1:end-1,:) .* tsup;

end

end
  
