function time = cosmic_time(z)
% Cosmic (look back) time at the redshift of coalescence z 
% in unit of Gyr    
% z is in the range of 0 and 10


load('cosmic_time_data');

time = interp1(redshifts,CosmsTime,z,'cubic');


end



