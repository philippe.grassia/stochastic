function [f, Omega] = NSBH(f,m,lambda)
% Stochastic Energy Spectrum of NS-BH Model
% f - recieved frequency
% m - average chirp mass in solar mass 
% lambda - mass fraction rate
% sfr - star formation history 
    
lambda = 10^lambda;

constant;
sfr = 'h';

mm = m*Msolar; % convert unit to gram 
Kb = (G*pi)^(2/3)*mm^(5/3)/3;  % cm^2*g/s^(4/3)

% Cutoffs of recieved frequence range 
% per second
% here 2^(6/5)*m is the total mass of the two NS
fgmin = 10;
fgmax = c^3/(6^1.5*pi*G*(2^(6/5)*mm)); 

% dimensionless 
% max of the redshift
zmax = 6;   

% normalizer of delay time distribution
tmin = 0.1;

Const = (8*pi*G)/(3*c^2*H0^3)*lambda*Kb/yr/Mpc^3;

%disp ('*************************************************')
%disp ('Beginning NS-BH Calculation')

Omega = zeros (1,length(f));

indexes = ~((f >=  fgmin) & (f<= fgmax));
zsups = (fgmax./f) - 1;
zsups(zsups>zmax) = zmax;

Omega = Const*f.^(2/3).*BC_integrand(tmin,zsups,sfr);
Omega(indexes) = 0;

%disp ('Finished NS-BH Calculation')
%disp ('*************************************************')

end
