function Omega = DNSDBH(f,m1,lambda1,m2,lambda2)

Omega1 = DNS(f,m1,lambda1);
Omega2 = DBH(f,m2,lambda2);
Omega = Omega1+Omega2;

