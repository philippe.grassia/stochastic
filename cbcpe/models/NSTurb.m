function Omegagw = NS_turb(f,dOm,N,v,M_star,R_star)
%Calculate and plot the gravitational wave energy density (Omega_gw) as a
%function of frequency
% f, sfr - (only inserted for consistency with general programs)
%  d0m   - Crust core differential shear
%   N    - Neutron Star Fraction
%   v    - kinematic viscosity
% M_star - NS mass
% R_star - NS radius

dOm = 10^dOm;
N = 10^N;
v = 10^v;

sfr = 'h';

% Equation numbers refer to Lasky, Bennett & Melatos (2013) 

% tt      = tic;        % timer to measure the performance.

% Parameters to be input from the website are included in the module
% 'inputparameters' are now included as functional parameters

% Define some constants.
c       = 3.0e08;       %speed of light
G       = 6.673e-11;    %gravitational constant

M_sun   = 1.9891e30;    %mass of the sun in kg
R_star  = R_star*1e3;   %convert to meters

% Calculate some constants:
ks  = 2*pi / R_star;    % Stirring wavenumber - Defined after eqn (7)
eps = (R_star^2)*(dOm^3);   % energy dissipation rate
eta = (1/sqrt(2*pi))*(eps^(1/3))*(ks^(2/3));    % defined in eqn (7)
kd  = (8^(1/4))*(eps^(1/4))*(27^(-1/4))*(v^(-3/4));  % viscous dissipation wavenumber

%Calculate N(z)...
%-----------------------------------
cosmoconst;
distances;              
IMF;
starformationrate;
diffsourceformrate; % Calculates the differential star formation rate
numneutronstar; % Calculates the total number density of neutron stars [N(z) in equation (18)]

%Calculate Omega_gw
%-----------------------------------
calculatedEdSdv;
% Construct Omegagw as a function of frequency, v_, by looping
% through the domain of ve and substitute a numerical value of each ve
% into the integrand before performing the numerical integration 
% with respect to z.

% Use exponential steps to product the constant steps on log-scale
% The min and max values may need to be input from the website.
step    = 0.1;                  % step size as appear on log-scale
min     = 1e-5;                 % minimum value of v_ (must be positive)
max     = 1e5;                  % maximum value of v_

% Restrict the domain because the code does not work well at very low v_
% i.e. at low frequency, small \Delta\Omega's give the code trouble - however, for
% small values of the frequency, the gravitational wave amplitude becomes
% vanishingly small (goes like seventh power of \Delta\Omega), hence
% ignoring these values is okay.
v_peak  = dOm/(10*(26e-3));
if min < 1e-2*v_peak
    min = 1e-2*v_peak;
end

i       = log10(min):step:log10(max);             
v_      = 10.^i;                % generate exponential step for v
Omegagw = zeros(1,numel(v_));   % reserve the memory for computation result

% Increase speed of calculation by increasing the tolerance.  The tolerance
% is updated from the maximum percentage error (MPE) as a function of the
% magnitude of Omega_gw.  
MPE       = 1e-10;              % Maximum Percentage Error
tolerance = 1e-20;              % initial integration error tolerance


% t = tic; % timer to measure performane

% Main loop for calculating Omega_gw as function of frequency
for i_ = 1:numel(v_)
    ffdEdSdv = @(z_) fdEdSdv(v_(i_).*(1+z_),z_);
        % construct a new function with z as the only variable.
        % This includes the extra factor of 5/4 pi that comes from the sum
        % over tensor spherical harmonics for \ell=2, m=-2,...,2.
    integrand = @(z_) (5/(4.*pi)).*...
        (4.*pi.*(fdLmetres(z_).^2).*ffdEdSdv(z_).*...
        v_(i_).*(1+z_)).*(fNumDenNS(z_)./((1+z_).*((Mpc2m).^3)));
    Omegagw(i_)  = (1/(pc*(c^2)))*quadl(integrand,0,20,tolerance);
        % integrate and store the result in the array.
        % the integration is done only from z = 0 to 20 to speed up the 
        % computation because the contribution from the 20 to 100 
        % part is very small (i.e. no neutron stars exist for z > 20)
    tolerance   = MPE*Omegagw(i_);
end
%elapsed = toc(t)

% Convert Omega_gw array into function of frequency
fOmegagw  = @(v__) N*interp1(v_,Omegagw,v__);


% MAIN PLOT
%figure(1);                     %plot the result.
%loglog(v_, fOmegagw(v_));
%xlim([min max]);
%ylim([1e-15 1e-5]);
%title('Omega_{gw} vs nu');
%xlabel('nu (Hz)');
%ylabel('Omega_{gw}');
%grid on;

%fnameout = 'result';
%han=figure(1);
%saveas(han, [fnameout '.pdf'], 'pdf');
% totelapsed = toc(tt) % Measure of performance
