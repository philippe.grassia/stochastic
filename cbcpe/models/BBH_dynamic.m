function Omega = BBH_dynamic(freq,NN,flag)
%function [f, Omega] = BBH_dynamic(freq,NN,flag)
%
%This function computes the SGWB spectrum for the dynamic BBH model. The
%inputs are:
% freq = frequency array at which to calculate the spectrum
% NN = average number of BBH per GC per second
% flag = defines the choice of the probability density function in the
% redshift vs chirp mass plane. 
%
% flag = 1: uniform distribution, 0 < z < 3.5 and 4 < Mc < 24
% flag = 2: a distribution closer to the simulation shown in the Rodriguez
% paper
% flag = 3: uniform distribution, 0 < z < 3.5 and 4 < Mc < 40
% flag = 4: uniform distribution, 0 < z < 3.5 and 4 < Mc < 16
% flag = 5: uniform distribution, 0 < z < 6 and 4 < Mc < 24
% 
% The code is based on the Rodriguez paper to compute the rate of binaries
% as a function of redshift and chirp mass.
%
% V. Mandic, 2015/10/07

%disp ('*************************************************')
%disp ('Beginning BBH_dynamic Calculation')

NN = 10^NN;
flag = floor(flag);

Mpc = 3.085e22; %in meters
Msolar = 1.989e30; % in kg
Myr = 3.1536e13; % in sec
H0 = 2.269e-18; %for h = 0.70
c = 2.9979e8; % in  m/s
OmegaM = 0.3;
OmegaV = 0.7;
G = 6.6738e-11; % in N m^2 / kg^2
rhoGC = 0.77 / Mpc^3; % from Rodriguez paper


Const = 8*(pi*G)^(5/3) * rhoGC * NN / (9*c^2*H0^3);
fgmin = 0;
if flag == 5 % define the max of the redshift
    zmax = 6;
else
    zmax = 3.5;    
end
Mcmin = 1; % minimum chirp mass in solar mass units
Mcmax = 40; % maximum chirp mass in solar mass units

zz = [0:0.01:1]*zmax; % define the redshift array
dz = zz(2) - zz(1);
zz = zz + dz/2;
zz(end) = [];
Mc = ([0:0.01:1]*(Mcmax - Mcmin) + Mcmin) * Msolar; %define the array of chirp masses
dMc = Mc(2) - Mc(1);
Mc = Mc + dMc/2;
Mc(end) = [];
zarray = transpose(zz) * ones(size(Mc)); % 2D array of redshifts
Mcarray = transpose(ones(size(zz))) * Mc; % 2D array of chirp masses

if flag == 1 | flag ==5 %uniform distribution in Mc-z plane
   P = ones(size(zarray)) / zmax / (24 - 4) / Msolar; %define the uniform PDF in z and Mc
   P(Mcarray<4*Msolar) = 0;
   P(Mcarray>24*Msolar) = 0;
elseif flag == 2 %distribution closer to the rodriguez paper
   P = ones(size(zarray));  %define the uniform PDF in z and Mc
   cut1 = zarray<0.8 | (Mcarray<24*Msolar & Mcarray>18*Msolar);
   P(cut1) = 3;
   cut2 = (zarray>2 & zarray<3.5) & (Mcarray<23*Msolar & Mcarray>20*Msolar);
   P(cut2) = 6;
   cut3 = (zarray>0.5 & zarray<3.5) & (Mcarray<=17.5*Msolar & Mcarray>=16*Msolar);
   P(cut3) = 6; 
   cut4 = (zarray>=3.3 & zarray<=3.5) & (Mcarray<14*Msolar & Mcarray>4*Msolar);
   P(cut4) = 9;   
   P(Mcarray<4*Msolar) = 0;
   P(Mcarray>24*Msolar) = 0;
   tmpN = sum(sum(P))*dz*dMc;
   P = P / tmpN;

   if 0
       figure(20)
       clf
       hhh = axes;
       surf(zarray,Mcarray/Msolar,P*Msolar)
       view(2)
       set(hhh,'XDir',reverse')
       colorbar    
       xlabel('z','FontSize',16)
       ylabel('M_c (M_{sun})','FontSize',16)
       
       print -dpng results/BBHdynamic_Mc_z_distribution.png
   end
   
   
elseif flag == 3 %uniform distribution including larger chirp masses
   P = ones(size(zarray)) / zmax / (40 - 4) / Msolar; %define the uniform PDF in z and Mc
   P(Mcarray<4*Msolar) = 0;
   P(Mcarray>40*Msolar) = 0;

elseif flag == 4 %uniform distribution excluding larger chirp masses
   P = ones(size(zarray)) / zmax / (16 - 4) / Msolar; %define the uniform PDF in z and Mc
   P(Mcarray<4*Msolar) = 0;
   P(Mcarray>16*Msolar) = 0;

else 
    P = 0;
end

LAL_MTSUN_SI = 4.9254923218988636432342917247829673e-6; %/**< Geometrized solar mass, s. = LAL_MSUN_SI / LAL_MPL_SI * LAL_TPL_SI */

etha = 0.25;
xi = 0;

root = sqrt(0.25-etha);
fraction = (0.5+root) / (0.5-root);
invfraction = 1/fraction;
m2= Mcarray * (1+fraction)^(0.2) / fraction^(0.6);
m1= Mcarray * (1+invfraction)^(0.2) / invfraction^(0.6);
Mtot = m1 + m2;

Ecosmo = sqrt( OmegaM * (1+zarray).^3 + OmegaV  );%the cosmology factor
zfactor = (1 + zarray).^(1/3); %the redshift factor

r = Mcarray/8.7/Msolar .* (1+zarray); % ratio of the chirp mass and 8.7 solar mass
v1 = 404./r; %  v1= 404 for mc = 8.7 solar mass
v2 = 807./r;
sigma = 237./r;
fgmax = 1153./r; % per second

v1 = Msolar * 404*((0.66389*etha*etha-0.10321*etha+0.10979)/0.125481)*(20./Mtot) ./ (1 + zarray);
v2 = Msolar * 807.*((1.3278*etha*etha-0.20642*etha+0.21957)/0.250953)*(20./Mtot) ./ (1 + zarray);
fgmax = Msolar * 1153.*((1.7086*etha*etha-0.26592*etha+0.28236)/0.322668)*(20./Mtot) ./ (1 + zarray);
sigma = Msolar * 237.*((1.1383*etha*etha-0.177*etha+0.046834)/0.0737278)*(20./Mtot) ./ (1 + zarray);

v1 = v1(:)'; v2 = v2(:)'; sigma = sigma(:)'; fgmax = fgmax(:)'; 
P = P(:)'; Ecosmo = Ecosmo(:)'; zfactor = zfactor(:)'; Mcarray = Mcarray(:)'; zarray = zarray(:)';
tmp = Const .* P ./ Ecosmo ./ zfactor .* Mcarray.^(5/3);
Mtot = Mtot(:)' / Msolar;

f = freq;

farray = repmat(f, length(v1), 1);
v1array = repmat(v1, length(f), 1)';
v2array = repmat(v2, length(f), 1)';
fgmaxarray = repmat(fgmax, length(f), 1)';
sigmaarray = repmat(sigma, length(f), 1)';
zarrayarray = repmat(zarray, length(f), 1)';
tmparray = repmat(tmp,length(f), 1)';
Mtot = repmat(Mtot,length(f),1)';

ix1 = ((farray >  0 ) & (farray <  v1array ));
ix2 = ((farray >=  v1array ) & (farray <=  v2array ));
ix3 = ((farray > v2array) & (farray < fgmaxarray));
ix4 = ((zarrayarray <= zmax));

v = (pi*Mtot.*farray*LAL_MTSUN_SI).^(1/3);
mod = (1 + (-323/224 + 451*etha/168)*v.^2 + xi*(27/8 - 11*etha/6)*v.^3).^2;

v = farray.^(-1/3) .* ix1 .* mod + (farray.^(2/3) ./v1array) .* ix2 .* mod + ...
   (farray.^2 ./ (1+4*(farray-v2array).^2 ./ sigmaarray.^2).^2 ./ v1array ./ v2array.^(4/3)) .* ix3;
integrand_array = farray .* tmparray .* v .* ix4;
Omega = sum(integrand_array,1) * dz * dMc;

%tmp = Const .* P ./ Ecosmo ./ zfactor .* Mcarray.^(5/3);
%for kk = 1:length(freq)
%    f = freq(kk);

       %next compute the frequency dependent factor
%    v = f^(-1/3) * (f >  0 ) * (f <  v1 ) + ...
%        f^(2/3) ./ v1 .* (f >= v1) .* (f <= v2 ) + ...
%        f^2 ./ (1+ 4.*(f-v2).^2 ./sigma.^2).^2 ./ v1 ./ v2.^(4/3) .* (f > v2) .* (f < fgmax);
    
    %finally pull it all together
%    integrand = f * tmp .* v .* (zarray <= zmax) ;
%    Omega(kk) = sum(sum(integrand)) * dz * dMc;

%end

%disp ('finishing BBH_dynamic Calculation')
%disp ('*************************************************')


         
end
