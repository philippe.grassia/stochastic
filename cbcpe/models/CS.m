%%%%%%%%%%%%%%%%%%%%%%%
% Cosmic String Model % 
%%%%%%%%%%%%%%%%%%%%%%%

function Omega_CS = CS_Model(f,  Prob, Size,Gmu) 

 %Prob = 5e-3;
 %Gmu = 1e-7;
 %Size = 1;
 
 %xmin = 1e-12;
 %xmax = 1e10;
 %numpoints = 100;
 
numpoints = length(f);

%disp('***********************************');
%disp('Beginning Cosmic String Calculation');
  
% This command works. Keep others around for a bit as backup of what I did 
 command = ['/home/mcoughlin/matapps/packages/stochastic/trunk/cbcpe/CS/cs_lambda_stochastic  -a ' ...
 num2str(log10(f(1))) ' -b ' ...
 num2str(log10(f(numpoints))) ' -c ' ...
 num2str(numpoints) ' -d ' ...
 num2str(log10(Gmu)) ' -e ' ...
 num2str(log10(Gmu)) ' -f  1 -g ' num2str(log10(Size)) ' -i ' ...
 num2str(log10(Size)) ' -j 1   --log-pstart ' num2str(log10(Prob)) ...
 ' --log-pend ' num2str(log10(Prob)) ' --np  1  --ln-zstart  -10 --ln-zend  64  --dlnz  0.01  --index 1'];
 
system(command);

% Assign freq and spectrum arrays from data file created by executable
try;
   cc = load('./stochastic_OmegaGW.dat');
   freq = cc(:,4);
   Omega_CS = cc(:,6);
catch
   Omega_CS = zeros(size(f));
end

system('rm ./stochastic_OmegaGW.dat');

%disp('Finished Cosmic String Calculation')
%disp('*************************************************')
%disp(freq)
%%disp(Omega_CS)

%loglog(f,Omega_CS, 'marker','*')
%axis([1e-13 1e10 1e-7 1e-4])

end
