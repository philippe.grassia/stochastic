function rhof = Rate_StarForm(sfr, z )
%  STAR_FORM_RATE Summary of this function goes here
%  In unit of M_solar per year per Mpc^3
%  
%
tmax = 13.5 ;
switch sfr

    %Hopkins & Beacom 2006;

    case 'h'

     rhof =  0.7*(0.017+0.13*z)./(1+(z./3.3).^5.3);

    %Fardal 2007;

    case 'f'

     rhof = (0.0103+0.088*z)./(1+(z./2.4).^2.8);

    %Wilken 2008;

    case 'w'

     rhof = (0.014+0.11.*z)./(1+(z./1.4).^2.2);

    %Nagamine et al. 2006 (fossil model)

    case 'n'

     %disk   

     rhod = @(t) 0.056.*(t./4.5).*exp(-t./4.5);

     %bulge

     rhob = @(t) 0.198.*(t./1.5).*exp(-t./1.5);

     time = tmax - cosmic_time(z);

     rhof = rhod(time) + rhob(time);  
	 
    %Springel & Hernquist 

    case 's' 
     
      r_num = 0.15.*(14.0/15).*exp(0.6.*(z - 5.4));
      r_nom = (14.0/15) - 0.6 + 0.6.*exp((14.0/15).*(z - 5.4));
      
      rhof = r_num./r_nom;

    case 'k' %Kistler, GRB rescaled, using Springel and Hernquist form
        
      r_num = 0.146 * 2.80 * exp(2.46 * (z - 1.72));
      r_nom = 2.80 - 2.46 + 2.46 * exp(2.80 * (z - 1.72));
      
      rhof = r_num./r_nom;

    case 'b' %Behroozi et al (2003) galaxy observations, springel and hernquist form
        
      r_num = 0.178 * 2.37 * exp(1.80 * (z - 2.00));
      r_nom = 2.37 - 1.80 + 1.80 * exp(2.37 * (z - 2.00));
      
      rhof = r_num./r_nom;

    case '3' %Pop3 component needed to complement Behroozi et al (2003) galaxy observations, springel and hernquist form
        
      r_num = 0.00218 * 13.81 * exp(13.36 * (z - 11.87));
      r_nom = 13.81 - 13.36 + 13.36 * exp(13.81 * (z - 11.87));
      
      rhof = r_num./r_nom;




end

end



