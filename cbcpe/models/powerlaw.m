function y = powerlaw(freq,a,k);

x = freq/100;
% calculate y-values
y = (10.^a)*(x.^k);

