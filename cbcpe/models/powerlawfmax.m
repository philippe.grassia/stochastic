function y = powerlawfmax(freq,a,k,fmax);

x = freq;
% calculate y-values
y = (10.^a)*(x.^k);
y(x>fmax) = 0;

