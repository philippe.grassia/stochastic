function z = cosmic_time_reverse(t)
% redshift at the Cosmic (look back) time of coalescence t
% in unit of Gyr    
% z is in the range of 0 and 10


load('cosmic_time_data');

z = interp1(CosmsTime,redshifts, t ,'linear','extrap');


end



