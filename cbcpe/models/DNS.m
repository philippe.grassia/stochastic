function Omega = Dual_NS (f,m,lambda)
% Stochastic Energy Spectrum of Double Neutron Star Model 	      
% f - recieved frequency
% m - average chirp mass in solar mass 
% lambda - mass fraction rate
% sfr - star formation history 

global f_sfr;
global r_sfr;

lambda = 10^lambda;

constant;
sfr = 'h';

mm = m*Msolar; % convert unit to gram 
Kb = (G*pi)^(2/3)*mm^(5/3)/3;  % cm^2*g/s^(4/3)

% Cutoffs of recieved frequence range 
% per second
% here 2^(6/5)*m is the total mass of the two NS
fgmin = 10;
fgmax = c^3/(6^1.5*pi*G*(2^(6/5)*mm)); 

% dimensionless 
% max of the redshift
zmax = 6;   

% normalizer of delay time distribution
tmin = 0.02;

Const = (8*pi*G)/(3*c^2*H0^3)*lambda*Kb/yr/Mpc^3;

%disp ('****************************************')
%disp ('Beginning DNS Calculation')

Omega = zeros (1,length(f));

indexes = ~((f >=  fgmin) & (f<= fgmax));
zsups = (fgmax./f) - 1;
zsups(zsups>zmax) = zmax;

val = interp1(r_sfr,f_sfr,zsups,'linear','extrap');
%val = BC_integrand(tmin,zsups,sfr);

Omega = Const*f.^(2/3).*val;
Omega(indexes) = 0;

%disp ('Finished DNS Calculation')
%disp ('****************************************')

end
