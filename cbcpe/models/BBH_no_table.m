function Omega = BBH_no_table(f,mc,lambda,sfr,waveform,zinf)
%function [f, Omega] = BBH_no_table(f,mc,lambda,sfr,waveform,zinf)
%
%This function calculates the spectrum of the SGWB due to BBH, assuming the
%field BBH model and including the inspiral, merger and ringdown
%contributions. The input parameters are:
% f = frequency array for which the spectrum will be evaluated
% mc = average chirp mass
% lambda = mass fraction parameter
% sfr = choice of the star formation rate
% waveform = a structure containing information about the time delay
% distribution
% zinf = the lowest redshift included in the calculation
%
%Code based on older codes by T. Regimbau, C. Wu, and V. Mandic
%
%VM, 2015/10/07

constant;

lambda = 10^lambda;

Kb = (G*pi)^(2/3)*(mc*Msolar)^(5/3)/3;

Const = (8*pi*G)/(3*c^2*H0^3)*lambda*Kb/yr/Mpc^3;
 
fgmin = 0;
zmax = 6;    % dimensionless --max of the redshift
tmax = 13.5 ; % in Gyr, max time delay considered in the delay integral
dz = 0.01;

% normalizer of delay time distribution
ztmp = 0:dz:12;

global Iz;
if 0
   Iz_int = zeros(1,length(ztmp));

   %Calculating the integrand of the redshift integral
   
   %first calculate the denominator
   Ez = @(M,V,z) (M*(1+z).^3+V).^0.5;  % dimensionless
   denom = Ez(OmegaM,OmegaV,ztmp).*(1+ztmp).^(1/3);

   %next, set up the delay-time integral
   tsup = (cosmic_time(10)-cosmic_time(ztmp)); % integral upbound
   accu = 10000;
   trapez = logspace(-100,0,accu);
   t    = transpose(trapez) * tsup;
   tf   = ones(accu,1) * cosmic_time(ztmp)+t;
   zf   = cosmic_time_reverse(tf); %computing the redshift of formation
   tem1 =  Rate_StarForm(sfr,zf)./(1+zf);

   %computing the probability distribution for time-delay
   pnorm = log(tmax/waveform.tmin);
   tem2 = t.^waveform.order./pnorm .* (waveform.tmin < t) .* (t < tmax) ;

   %next, compute the integral over time-delay
   tem  = tem1 .* tem2;
   rate = diff(trapez) * tem(1:end-1,:) .* tsup;

   %finally compute the integrand of the redshift integral
   Iz = rate./denom;
end

LAL_MTSUN_SI = 4.9254923218988636432342917247829673e-6; %/**< Geometrized solar mass, s. = LAL_MSUN_SI / LAL_MPL_SI * LAL_TPL_SI */

etha = 0.25;
xi = 0;

root = sqrt(0.25-etha);
fraction = (0.5+root) / (0.5-root);
invfraction = 1/fraction;
m2= mc * (1+fraction)^(0.2) / fraction^(0.6);
m1= mc* (1+invfraction)^(0.2) / invfraction^(0.6);
Mtot = m1 + m2;

%compute the parameters of the frequency spectrum
%r = mc/8.7 * (1 + ztmp); % ratio of the chirp mass and 8.7 solar mass
%v1 = 404./r; %  v1= 404 for mc = 8.7 solar mass
%v2 = 807./r;
%sigma = 237./r;
%fgmax = 1153./r; % per second

v1 = 404*((0.66389*etha*etha-0.10321*etha+0.10979)/0.125481)*(20./Mtot) ./ (1 + ztmp);
v2 = 807.*((1.3278*etha*etha-0.20642*etha+0.21957)/0.250953)*(20./Mtot) ./ (1 + ztmp);
fgmax = 1153.*((1.7086*etha*etha-0.26592*etha+0.28236)/0.322668)*(20./Mtot) ./ (1 + ztmp);
sigma = 237.*((1.1383*etha*etha-0.177*etha+0.046834)/0.0737278)*(20./Mtot) ./ (1 + ztmp);

farray = repmat(f, length(v1), 1);
v1array = repmat(v1, length(f), 1)';
v2array = repmat(v2, length(f), 1)';
fgmaxarray = repmat(fgmax, length(f), 1)';
sigmaarray = repmat(sigma, length(f), 1)';
Izarray = repmat(Iz, length(f), 1)';
ztmparray = repmat(ztmp, length(f), 1)';

ix1 = ((farray >  0 ) & (farray <  v1array ));
ix2 = ((farray >=  v1array ) & (farray <=  v2array ));
ix3 = ((farray > v2array) & (farray < fgmaxarray));
ix4 = ((ztmparray <= zmax) & (ztmparray >= zinf));

v = (pi*Mtot*farray*LAL_MTSUN_SI).^(1/3);
mod = (1 + (-323/224 + 451*etha/168)*v.^2 + xi*(27/8 - 11*etha/6)*v.^3).^2; 

v = farray.^(-1/3) .* ix1 .* mod + (farray.^(2/3) ./v1array) .* ix2 .*mod + ...
   (farray.^2 ./ (1+4*(farray-v2array).^2 ./ sigmaarray.^2).^2 ./ v1array ./ v2array.^(4/3)) .* ix3;

Iz_tmp_array = Izarray .* v;
Iz_tmp_sum = Iz_tmp_array .* ix4;
Iz_tmp_sum(isnan(Iz_tmp_sum)) = 0;
Iz_int_array = sum(Iz_tmp_sum,1) * dz;
Omega = Const * f .* (f > fgmin) .* Iz_int_array;

end
