%%%%%%%%%%%%%%%%%%%%
% Supernovae Model % 
%%%%%%%%%%%%%%%%%%%%

function Omega_SuNva = SuNva (f,alpha,beta)
%% Constants 

% debugging
% xmin = 1;
% xmax = 2000;
% numpoints = 10;


 % f (1) = (xmin);
 %  f (numpoints) = (xmax);
 %  slope = (log10 (xmax) - log10 (xmin))/(numpoints-1);

  % for i = 2:numpoints-1
  %  f (i) = f (i-1)*10^(slope);
  % end
% end debugging   

D = 1;
numpoints = length(f);

constant;

disp ('*************************************************')
disp ('Beginning DNS Calculation')

%% Finally get omega    

% Omega_DNS = zeros (1,numpoints);
A = ones(size(f));

for i=1:numpoints
    z = 0:0.01:20;
    fz = f(i)*(z+1);
    integ =  Rs(z,alpha,beta)./((1+z).^2.*H0.*(0.3.*(1+z).^3+0.7).^(0.5)).*fz.*(1+fz./200).^6.*exp(-2*fz./300);
    integral = trapz (z,integ);      
    Omega_SuNva(i) = A (i)*integral;
end

disp ('Finished Supernovae Calculation')
disp ('*************************************************')

% disp (Omega_DNS);
% keyboard
% loglog (f,Omega_DNS,'marker','*');
% disp ('number of zeros is:')
% disp (numzeros)
end
