function Ptd = BC_LifeDistr(waveform,td)

tmax = 13.5;

switch	waveform.form

case 'power'
	
	tmin = waveform.tmin;
	
	a = waveform.order;

	if a == -1
		pnorm = log(tmax/tmin);
	else
		pnorm = (tmax^(a+1)-tmin^(a+1))/(a+1);
	end
	
	Ptd = td.^a./pnorm .* (tmin < td) .* (td < tmax) ;
    
case 'log'

    sigma = waveform.sigma;

	tx = waveform.tao;

	Ptd = 1./( td .* sigma .* sqrt(2*pi) .* ...
		       exp((log(td)-log(tx)).^2./2./sigma^2));

end

Ptd(isnan(Ptd)) = 0;

end
