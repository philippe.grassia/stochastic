function y = powerlaw2(freq,a,k,b,l);

x = freq;
% calculate y-values
y = powerlaw(freq,a,k) + powerlaw(freq,b,l);

