function val = bayesian_inj_data(analysisType,dataSet,param);

val = NaN;
if strcmp(dataSet,'2')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -9.2; 
      elseif strcmp(param,'k')
         val = (2/3);
      end
   elseif strcmp(analysisType,'DNS')
      if strcmp(param,'M')
         val = 1.2188;
      elseif strcmp(param,'lam')
         val = log10(4.7933e-4);
      end
   end
elseif strcmp(dataSet,'3')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -9.5;
      elseif strcmp(param,'k')
         val = (2/3);
      end
   elseif strcmp(analysisType,'DNS')
      if strcmp(param,'M')
         val = 8.7055;;
      elseif strcmp(param,'lam')
         val = log10(1.2653e-5);
      end
   end
elseif strcmp(dataSet,'4')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -9.5;
      elseif strcmp(param,'k')
         val = (2/3);
      end
   elseif strcmp(analysisType,'DNS')
      if strcmp(param,'M')
         val =  1.3715;
      elseif strcmp(param,'lam')
         val = log10(4.6981e-4);
      end
   end
elseif strcmp(dataSet,'5')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -9.5;
      elseif strcmp(param,'k')
         val = (2/3);
      end
   elseif strcmp(analysisType,'DNS')
      if strcmp(param,'M')
         val = 1.3715;
      elseif strcmp(param,'lam')
         val = log10(4.6981e-4);
      end
   end
elseif strcmp(dataSet,'ET')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -10.7;
      elseif strcmp(param,'k')
         val = (2/3);
      end
   elseif strcmp(analysisType,'DNS')
      if strcmp(param,'M')
         val = 1.4;
      elseif strcmp(param,'lam')
         val = -4.9;
      end
   end
elseif strcmp(dataSet,'ET_noDetect')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -12.9;
      elseif strcmp(param,'k')
         val = (2/3);
      end
   elseif strcmp(analysisType,'DNS')
      if strcmp(param,'M')
         val = 1.4;
      elseif strcmp(param,'lam')
         val = -7.1;
      end
   end
end

