
analysisType = 'powerlaw';
dataSet = '2';
dataSet = '4post';
dataSet = '2';
dataSet = '2ideal';
%dataSet = '4ideal';
%dataSet = '3ideal';
%analysisType = 'powerlaw2';
%analysisType = 'DNS';

doRun = 0;
%doRun = 1;

dataSets = {'2ideal','3ideal','4ideal','5ideal'};
analysisTypes = {'powerlaw','DNS'};

dataSets = {'2','3','4'};
dataSets = {'2'};
dataSets = {'1'};
dataSets = {'3'};
dataSets = {'4'};

%dataSets = {'3ideal'};
%dataSets = {'4'};
%dataSets = {'4ideal'};
%dataSets = {'none'};
%dataSets = {'2ideal'};
%dataSets = {'2'};
%dataSets = {'ET','ET_noDetect'};
%dataSets = {'ET_noDetect'};
analysisTypes = {'powerlaw','DNS'};
%dataSets = {'0'};
%analysisTypes = {'DBH'};
analysisTypes = {'DNS'};

for i = 1:length(analysisTypes)
   %for j = 4
   for j = 1:length(dataSets)
      analysisType = analysisTypes{i};
      dataSet = dataSets{j};
      stoch_bf(analysisType,dataSet,doRun);
   end
end

