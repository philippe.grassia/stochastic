
analysisType = 'powerlaw';
dataSet = '2';
dataSet = '4post';
dataSet = '2';
dataSet = '2ideal';
%dataSet = '4ideal';
%dataSet = '3ideal';
%analysisType = 'powerlaw2';
%analysisType = 'DNS';

doRun = 0;
doRun = 1;

%stoch_mn(analysisType,dataSet,doRun);

dataSets = {'2ideal','3ideal','4ideal','5ideal'};
analysisTypes = {'powerlaw','powerlaw2','DNS','DNS2'};
%analysisTypes = {'powerlaw2'};
%analysisTypes = {'DNS2'};
analysisTypes = {'DNS2'};
dataSets = {'5ideal'};
analysisTypes = {'powerlaw2fmax'};
%dataSets = {'inj'};
dataSets = {'5ideal'};
analysisTypes = {'DNS'};
analysisTypes = {'NSBH'};
analysisTypes = {'DBH'};
analysisTypes = {'DBH'};
%analysisTypes = {'MAG'};
%analysisTypes = {'sunva'};
%analysisTypes = {'NSTurb'};
%analysisTypes = {'PBB'};
%analysisTypes = {'CS'};
%analysisTypes = {'rmode'};
%analysisTypes = {'barmode'};
analysisTypes = {'DNSDBH'};

for i = 1:length(analysisTypes)
   %for j = 4
   for j = 1:length(dataSets)
      analysisType = analysisTypes{i};
      dataSet = dataSets{j};
      stoch_mn(analysisType,dataSet,doRun);
   end
end

