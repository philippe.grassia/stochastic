
analysisType = 'powerlaw';
dataSet = '2';
dataSet = '4post';
dataSet = '2';
dataSet = '2ideal';
%dataSet = '4ideal';
%dataSet = '3ideal';
%analysisType = 'powerlaw2';
%analysisType = 'DNS';

doRun = 0;
doRun = 1;

dataSets = {'2ideal','3ideal','4ideal','5ideal'};
analysisTypes = {'powerlaw','DNS'};

dataSets = {'inj'};
analysisTypes = {'DNS'};
analysisTypes = {'NSBH'};
analysisTypes = {'DBH'};

for i = 1:length(analysisTypes)
   %for j = 4
   for j = 1:length(dataSets)
      analysisType = analysisTypes{i};
      dataSet = dataSets{j};
      stoch_bf(analysisType,dataSet,doRun);
   end
end

