
analysisType = 'powerlaw';
dataSet = '2';
dataSet = '4post';
dataSet = '2';
dataSet = '2ideal';
%dataSet = '4ideal';
%dataSet = '3ideal';
%analysisType = 'powerlaw2';
%analysisType = 'DNS';

doRun = 0;
doRun = 1;

dataSets = {'2ideal','3ideal','4ideal','5ideal'};
analysisTypes = {'powerlaw','DNS'};
analysisTypes = {'renorm'};
dataSets = {'O1test'};
%dataSets = {'O1testideal'};

for i = 1:length(analysisTypes)
   %for j = 4
   for j = 1:length(dataSets)
      analysisType = analysisTypes{i};
      dataSet = dataSets{j};
      stoch_bf_renorm(analysisType,dataSet,doRun);
   end
end

