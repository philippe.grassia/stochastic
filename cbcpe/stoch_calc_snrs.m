
paramsFile_inj = '/home/mcoughlin/Stochastic/Bayesian/params/params_inj_2.txt';
outputDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_inj_2';

paramsFile_inj = '/home/mcoughlin/Stochastic/Bayesian/params/params_inj_2_mid.txt';
outputDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_inj_2_mid';

%paramsFile_inj = '/home/mcoughlin/Stochastic/Bayesian/params/params_inj_2_low.txt';
%outputDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_inj_2_low';

paramsFile = '/home/mcoughlin/Stochastic/Bayesian/params/params_zero.txt';
jobsFile = '/home/mcoughlin/Stochastic/Bayesian/jobfile_inj.txt';
jobNumber = 1;
%stochasticmod(paramsFile, jobsFile, jobNumber);

%ptEstInt = load('/home/mcoughlin/Stochastic/Bayesian/output/output_none/output_ccspectra.job1.trial1.dat');
%sensInt = load('/home/mcoughlin/Stochastic/Bayesian/output/output_none/output_sensints.job1.trial1.dat');
ptEstInt = load('/home/mcoughlin/Stochastic/Bayesian/output/output_none/output_combined_ccspectra.job1.trial1.dat');
sensInt = load('/home/mcoughlin/Stochastic/Bayesian/output/output_none/output_combined_sensints.job1.trial1.dat');
ff = ptEstInt(:,3);
deltaF = ff(2)-ff(1);
sigma_J = 1./sqrt(sensInt(:,4)*deltaF);
Y_J = 2*deltaF*sum(sensInt(:,4)*deltaF)*ptEstInt(:,4)./(sensInt(:,4)*deltaF);

var_J = sigma_J.^2;
cut = isnan(Y_J);
Y_J(cut) = 0;
YTot_1 = sum(real(Y_J)./var_J)/sum(1./var_J);
sigmaTot_1 = sqrt(1/sum(1./var_J));
snr_1 = abs(YTot_1)/sigmaTot_1;

paramsFile = paramsFile_inj;
jobsFile = '/home/mcoughlin/Stochastic/Bayesian/jobfile_inj.txt';
jobNumber = 1;
stochasticmod(paramsFile, jobsFile, jobNumber);

%ptEstInt = load([outputDir '/output_ccspectra.job1.trial1.dat']);
%sensInt = load([outputDir '/output_sensints.job1.trial1.dat']);
ptEstInt = load([outputDir '/output_combined_ccspectra.job1.trial1.dat']);
sensInt = load([outputDir '/output_combined_sensints.job1.trial1.dat']);
ff = ptEstInt(:,3);
deltaF = ff(2)-ff(1);
sigma_J = 1./sqrt(sensInt(:,4)*deltaF);
Y_J = 2*deltaF*sum(sensInt(:,4)*deltaF)*ptEstInt(:,4)./(sensInt(:,4)*deltaF);

% cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
var_J = sigma_J.^2;
cut = isnan(Y_J);
Y_J(cut) = 0;
YTot_2 = sum(real(Y_J)./var_J)/sum(1./var_J);
sigmaTot_2 = sqrt(1/sum(1./var_J));
snr_2 = abs(YTot_2)/sigmaTot_2;

fprintf('Point Estimate = %g \n',YTot_2);
fprintf('Final error bar = %g \n',sigmaTot_1);
fprintf('SNR = %.5f \n',abs(YTot_2)/sigmaTot_1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

