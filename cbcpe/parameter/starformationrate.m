%Star formation rate - Currently, Hopkins & Beacom (2006) implemented
aHB6 = 0.0170;
bHB6 = 0.13;
cHB6 = 3.3;
dHB6 = 5.3;
h100 = H/(1000*100);
fSFR  = @(z_) (aHB6 + bHB6*z_) * h100/(1+(z_/cHB6).^dHB6);
z     = 1:0.1:100;
SFR = arrayfun(fSFR,z);

% figure;
% loglog(z,SFR);