%Total number of stars is the integral of the star formation rate. 

Hyr = (73/(3.0857e19))*60*60*24*365.25; % Hubble constant in units 1/yr
%Numerical integration again.
fdNumNSdz   = @(z_, NumNS_) (1/(Hyr*M_star))*fdNdz(z_)/((1+z_)*fEE(z_));
option  = odeset('maxstep', 1e-1);
[z,NumberNS]= ode45(fdNumNSdz, [0 100], 0);
%integrate from z_ to 100 = [integration from 0 to 100] - [integration from
%0 to z_]
fNumberNS   = @(z_) interp1(z,NumberNS,100)-interp1(z,NumberNS,z_);
fNumNSdVdz   = @(z_) fNumberNS(z_)/dVdz(z_);
z__   = 0:0.01:15;
NumNSdVdz   = arrayfun(fNumNSdVdz, z__);

% figure;
% semilogy(z__,NumNSdVdz);

fdNumDenNSdz = @(z_, NumDenNS_) (IMFint/(Hyr*M_star))*(fSFR(z_)/(1+z_));
[z,EENumDenNS] = ode45(fdNumDenNSdz, [0 100], 0, option);
fNumDenNS     = @(z_) (1./(fEE(z_).*(1+z_))).*...
    (interp1(z,EENumDenNS,20)-interp1(z,EENumDenNS,z_));
z__     = 0:0.01:100;
NumDenNS    = arrayfun(fNumDenNS, z__);

% figure;
% semilogy(z__,NumDenNS);
% xlim([0 100]);
% ylim([1e-3,1e7]);
% 
% grid on;
