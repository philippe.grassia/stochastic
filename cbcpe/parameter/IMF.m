%IMF - Currently, Salpeter IMF is implemented
syms M;
IMFscale = 1/double(int(M^-2.35, M, 0.1, 100));
fIMF     = @(M_) IMFscale * (M_^-2.35);
Mmin = 8;   %unit of M_sun
Mmax = 40;  %unit of M_sun
IMFint = double(int(fIMF(M), M, Mmin, Mmax));

