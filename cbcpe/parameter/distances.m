%Distances.
dH      = c/H;   %Hubble distance (Mpc).
%syms a

%symbolic integration is too slow to calculate dC. 
%Use numerical integration.
ddCdz   = @(z_, dC_) dH/fEE(z_);
option  = odeset('maxstep', 1e-1);
[z,dC]  = ode45(ddCdz, [0 100], 0,option);
fdC     = @(z_) interp1(z,dC,z_);
%dC      = @(z_) dH*quad(1/fEE(a),0,z_);
dM      = @(z_) fdC(z_);
dL      = @(z_) (1+z_).*dM(z_);
dA      = @(z_) dM(z_) / (1+z_);
dVdz    = @(z_) 4*pi*((fdC(z_))^2)*dH/fEE(z_);
Vc      = @(z_) (4*pi/3)*(dM(z_)^3);
