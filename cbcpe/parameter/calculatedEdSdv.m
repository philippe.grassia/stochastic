% Units

Mpc2m   = 3.08e22;
fdVdzmetres  = @(z_) dVdz(z_)*((Mpc2m)^3);
fVcmetres    = @(z_) Vc(z_)*((Mpc2m)^3);
fVLmetres    = @(z_) (4*pi/3)*(dL(z_)^3)*((Mpc2m)^3);
fdCmetres    = @(z_) fdC(z_)*(Mpc2m);
fdLmetres    = @(z_) dL(z_)*(Mpc2m);
Hsec         = H/Mpc2m;

% pc = \rho_{c} (i.e. critical density of Universe)
pc           = 3*(Hsec^2)/(8*pi*G);


% h_rms for a single star at redshift z
fh_rms_redshift = @(z_) 5e-28*(M_star/1.4)*...
    ((R_star/1e4)^3)*((dOm/10)^3)*(1./(dL(z_)*1000));

% To plot figure of h_rms(z), uncomment below.
% figure;
% z            = 1e-8:100;
% h_rms_redshift  = arrayfun(fh_rms_redshift,z);
% loglog(z,h_rms_redshift);

fvv          = @(z_) Om_m*((1+z_)^3);
fLL          = @(z_) log((sqrt(fvv(z_)+Om_L)+sqrt(Om_L))/...
    (sqrt(fvv(z_)+Om_L)-sqrt(Om_L)));

% figure;
% z            = 0:0.1:100;
% LL           = arrayfun(fLL,z);
% plot(z,LL);

% Lookback time as function of z
fTLookBack   = @(z_) (fLL(0) - fLL(z_))/(3*Hsec*sqrt(Om_L));
% figure;
% z            = 0:0.1:100;
% TLookBack    = arrayfun(fTLookBack,z);
% plot(z,TLookBack);

syms zz;
dTdz__       = diff(fTLookBack(zz), zz);
%pre-evaluate the derivative for a faster computation.
z            = 0:0.1:100;
dTdz_        = subs(dTdz__,z);
fdTdz        = @(z_) interp1(z,dTdz_,z_);
fdTdzTLB     = @(z_) fdTdz(z_)/fTLookBack(z_);
%plot...
% z            = 0:0.1:10;
% dTdzTLB      = arrayfun(fdTdzTLB,z);
% figure;
% semilogy(z,dTdzTLB);
% xlim([0 10]);
% ylim([1e-1 1e6]);
% grid on;

% Calculate dE/dSd\nu.  Do this in a few parts, then combine at the end.

fdEdSdv1 = @(ve_,z_) ...
    (1/((4*sqrt(2))*(pi^(7/2))*(dOm^3)))*...
    exp(-(ve_.^2)./((2^(1/3))*(pi^(4/3))*(dOm^2))).*...
    (fh_rms_redshift(z_).^2).*(ve_.^2).*...
    (-3*(2^(1/3))*(pi^(4/3))*(dOm^2)+(7*(ve_.^2)));

B1 = 0.5*sqrt(pi)*eta*(kd^(2/3))*(ks^(-2/3));
fdEdSdv2 = @(ve_,z_) ...
    exp(-(ve_.^2)./(4*(B1^2))).*...
    (fh_rms_redshift(z_).^2)*sqrt(pi/2)*(dOm^3).*...
    (48*(B1^6)+12*(B1^4).*(ve_.^2)+(ve_.^6))./...
    (16*(B1^6)*(ve_.^2));

B2 = 0.5*sqrt(pi)*eta;
fdEdSdv3 = @(ve_,z_) ...
    -exp(-(ve_.^2)./(4*(B2^2))).*...
    (fh_rms_redshift(z_).^2)*sqrt(pi/2)*(dOm^3).*...
    (48*(B2^6)+12*(B2^4).*(ve_.^2)+(ve_.^6))./...
    (16*(B2^6)*(ve_.^2));

fdEdSdv = @(ve_,z_) (fdTdz(z_)*(c^3)/G).*...
    (fdEdSdv1(ve_,z_)+fdEdSdv2(ve_,z_)+fdEdSdv3(ve_,z_));

% To plot dE/dSd\nu as function of \nu, uncomment the following.
% figure;
% ve           = 1:1e6;
% dEdSdv     = ((fdTdz(0.00000024)*(c^3)/G)^(-1))*fdEdSdv(ve,0.00000024);
% % dEdSdv       = fdEdSdv(ve,2*sqrt(2)*(pi^(5/2))*(dOm^3);1)/...
% %     ((c^3).*fdTdz(1).*fh_rms_redshift(1)./(16*(pi^2)*G));
% loglog(ve,dEdSdv);
% %xlim([1e-20 1e2]);
% ylim([1e-66 1e-46]);
% grid on;