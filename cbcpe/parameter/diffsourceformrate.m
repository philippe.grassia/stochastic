%Differntial star formation rate (in units of h100 Msun/yr)

fdNdz   = @(z_) IMFint*fSFR(z_)*dVdz(z_)/(1+z_);
z       = 0:0.1:100;
dNdz    = arrayfun(fdNdz,z);

% figure;
% semilogy(z,dNdz);