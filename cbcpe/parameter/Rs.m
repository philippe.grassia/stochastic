function y = Rs(z,alpha,beta)

y = zeros(size(z));
indexes = find(z<1);
y(indexes) = (1+z(indexes)).^beta;
indexes = find(z>=1 & z<=20);
y(indexes) = 2^(beta-alpha).*(1+z(indexes));

