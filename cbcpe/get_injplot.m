function get_injplot(analysisType,dataSet);

hold on
if strcmp(dataSet,'inj')
   if strcmp(analysisType,'DNS')
      M = 1.4; lam = -3.5;
      plot(M,lam,'kx','markersize',20);
   elseif strcmp(analysisType,'NSBH')
      M = 3.0; lam = -3.5;
      plot(M,lam,'kx','markersize',20);
   elseif strcmp(analysisType,'DBH')
      M = 7.0; lam = -3.5;
      plot(M,lam,'kx','markersize',20);
   elseif strcmp(analysisType,'MAG')
      B = 15; epsilon = -4;
      P0 = 0.002; lambda = -3;
      plot(B,epsilon,'kx','markersize',20);
   elseif strcmp(analysisType,'NSTurb');
      dOm = 0; N = -3;
      v = 0; M_star = 1.4;
      R_star = 15;
      plot(dOm,N,'kx','markersize',20);
   elseif strcmp(analysisType,'PBB');
      mu = 1.5; F1 = 10;
      Fs = 2;
      plot(mu,F1,'kx','markersize',20);
   elseif strcmp(analysisType,'CS');
      P = -2; epsilon = -10;
      Gmu = -8;
      plot(P,epsilon,'kx','markersize',20);
   elseif strcmp(analysisType,'rmode');
      P0 = 1; lambda = -5;
      plot(P0,lambda,'kx','markersize',20);
   elseif strcmp(analysisType,'barmode');
      P0 = 1; lambda = -5;
      plot(P0,lambda,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC2inj')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      a = log10(2e-4); k = 2/3;
      plot(a,k,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC2inj_mid')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = log10(2e-6); k = 2/3;
      plot(a,k,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC2inj_low')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = log10(2e-8); k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      a = log10(2e-8); k = 2/3;
      plot(a,k,'kx','markersize',20);
   end
elseif strcmp(dataSet,'O1test') || strcmp(dataSet,'O1testideal') || strcmp(dataSet,'ThirtyPlusThirtyIdeal') || strcmp(dataSet,'ThirtyPlusThirtySEOBNRv2')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -7.14141; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS') || strcmp(analysisType,'DBH') || strcmp(analysisType,'BBHNoTable') || strcmp(analysisType,'BBHDynamic')
      M = 26.12; lam = log10(2.1088e-05);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'2') || strcmp(dataSet,'2ideal')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -7.86; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.2188; lam = log10(4.7933e-4);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'3')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -9.5; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 8.7055; lam = log10(1.2653e-5);
      plot(M,lam,'kx','markersize',20);
   elseif strcmp(analysisType,'DBH');
      M = 8.7055; lam = log10(1.2653e-5);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'4')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -9.5; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.3715; lam = log10(4.6981e-4);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'5')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -9.5; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.3715; lam = log10(4.6981e-4);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'6')
   if strcmp(analysisType,'powerlaw');
      a = -9.5; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.3715; lam = log10(4.6981e-4);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'ET')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -10.7; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.4; lam = -4.9;
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'ET_noDetect')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -12.9; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.4; lam = -7.1;
      plot(M,lam,'kx','markersize',20);
   end
end
hold off

