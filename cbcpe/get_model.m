function [model,prior] = get_model(analysisType);

if strcmp(analysisType,'powerlaw')
   model = @powerlaw_model;
   prior = {'a', 'uniform', -15, 0, 'fixed'; ...
           'k', 'uniform', -3, 3, 'fixed'};
   % ET no detect
   prior = {'a', 'uniform', -13.5, -12.5, 'fixed'; ...
           'k', 'uniform', 0.6, 1.2, 'fixed'};
   % 2
   prior = {'a', 'uniform', -11, -7, 'fixed'; ...
           'k', 'uniform', -5, 5, 'fixed'};
   % 3
   %prior = {'a', 'uniform', -14, -7, 'fixed'; ...
   %        'k', 'uniform', -5, 5, 'fixed'};
   % 4
   %prior = {'a', 'uniform', -9, -7, 'fixed'; ...
   %        'k', 'uniform', -0.5, 2.0, 'fixed'};

   % 0
   %prior = {'a', 'uniform', -15, 0, 'fixed'; ...
   %        'k', 'uniform', -3, 3, 'fixed'};

   % O1 test
   prior = {'a', 'uniform', -9, -5, 'fixed'; ...
           'k', 'uniform', -5, 5, 'fixed'};

elseif strcmp(analysisType,'powerlawPSD')
   model = @powerlaw_model;
   prior = {'a', 'uniform', -15, -7, 'fixed'; ...
           'k', 'uniform', -6, 6, 'fixed'; ...
           'A', 'uniform', -5, 5, 'fixed'};
elseif strcmp(analysisType,'powerlawfmax')
   model = @powerlaw_model;
   prior = {'a', 'uniform', -11, -6, 'fixed'; ...
           'k', 'uniform', 0, 2, 'fixed';...
           'fmax','uniform', 50, 256, 'fixed'};
elseif strcmp(analysisType,'powerlaw2')
   model = @powerlaw2_model;
   prior = {'a', 'uniform', -11, -8, 'fixed'; ...
           'k', 'uniform', 0, 1, 'fixed'; ...
           'b', 'uniform', -11, -8, 'fixed'; ...
           'l', 'uniform', 0, 1, 'fixed'};
elseif strcmp(analysisType,'powerlaw2fmax')
   model = @powerlaw2fmax_model;
   prior = {'a', 'uniform', -11, -8, 'fixed'; ...
           'k', 'uniform', 0, 1, 'fixed'; ...
           'fmax1','uniform', 70, 100, 'fixed'; ...
           'b', 'uniform', -11, -8, 'fixed'; ...
           'l', 'uniform', 0, 1, 'fixed';...
           'fmax2','uniform', 250, 256, 'fixed'};
elseif strcmp(analysisType,'DNS')
   model = @DNS_model;
   % ET no detect
   prior = {'M', 'uniform', 0.5, 35, 'fixed'; ...
           'lam', 'uniform', -6, -3, 'fixed'};
   % 2
   %prior = {'M', 'uniform', 0, 10, 'fixed'; ...
   %        'lam', 'uniform', -7, 0, 'fixed'};

   %prior = {'M', 'uniform', 0.5, 100, 'fixed'; ...
   %        'lam', 'uniform', -8, -3, 'fixed'};

   tmin = 0.02;
   if tmin ==0.02
      load('BC_fit_20.mat');
   else
      load('BC_fit_100.mat');
   end
   sfr = 'h';
   global f_sfr;
   f_sfr = eval(['fit.' sfr]);
   global r_sfr;
   r_sfr = r;

elseif strcmp(analysisType,'DNS2')
   model = @DNS2_model;
   prior = {'M1', 'uniform', 0.5, 25, 'fixed'; ...
           'lam1', 'uniform', 0, -6, 'fixed';...
           'M2', 'uniform', 0.5, 25, 'fixed'; ...
           'lam2', 'uniform', 0, -6, 'fixed'};

   tmin = 0.02;
   if tmin ==0.02
      load('BC_fit_20.mat');
   else
      load('BC_fit_100.mat');
   end
   sfr = 'h';
   global f_sfr;
   f_sfr = eval(['fit.' sfr]);
   global r_sfr;
   r_sfr = r;

elseif strcmp(analysisType,'DNSDBH')
   model = @DNS2_model;
   prior = {'M1', 'uniform', 1, 2, 'fixed'; ...
           'lam1', 'uniform', 0, -6, 'fixed';...
           %'M2', 'uniform', 6, 20, 'fixed'; ...
           %'lam2', 'uniform', -7, -5, 'fixed'};

           'M2', 'uniform', 20, 100, 'fixed'; ...
           'lam2', 'uniform', -7, -5, 'fixed'};
elseif strcmp(analysisType,'NSBH')
   model = @NSBH_model;
   prior = {'M', 'uniform', 1, 5, 'fixed'; ...
           'lam', 'uniform', 0, -6, 'fixed'};
elseif strcmp(analysisType,'DBH')
   model = @DBH_model;
   prior = {'M', 'uniform', 1.4, 35, 'fixed'; ...
           'lam', 'uniform', -6, -3, 'fixed'};

   tmin = 0.1;
   if tmin ==0.02
      load('BC_fit_20.mat');
   else
      load('BC_fit_100.mat');
   end
   sfr = 'h';
   global f_sfr;
   f_sfr = eval(['fit.' sfr]);
   global r_sfr;
   r_sfr = r;

elseif strcmp(analysisType,'BBHNoTable')
   model = @BBH_no_table_model;
   prior = {'M', 'uniform', 1.4, 35, 'fixed'; ...
           'lam', 'uniform', -6, -3, 'fixed'};

   global sfr;
   sfr = 'k';

   global waveform;
   waveform.form = 'power';
   waveform.order = -1;
   waveform.tmin = 0.1;

   global zinf;
   zinf = 0;

   global Iz;

   constant;

   fgmin = 0;
   zmax = 6;    % dimensionless --max of the redshift
   tmax = 13.5 ; % in Gyr, max time delay considered in the delay integral
   dz = 0.01;

   % normalizer of delay time distribution
   ztmp = 0:dz:12;
   Iz_int = zeros(1,length(ztmp));

   %Calculating the integrand of the redshift integral

   %first calculate the denominator
   Ez = @(M,V,z) (M*(1+z).^3+V).^0.5;  % dimensionless
   denom = Ez(OmegaM,OmegaV,ztmp).*(1+ztmp).^(1/3);

   %next, set up the delay-time integral
   tsup = (cosmic_time(10)-cosmic_time(ztmp)); % integral upbound
   accu = 10000;
   trapez = logspace(-100,0,accu);
   t    = transpose(trapez) * tsup;
   tf   = ones(accu,1) * cosmic_time(ztmp)+t;
   zf   = cosmic_time_reverse(tf); %computing the redshift of formation
   tem1 =  Rate_StarForm(sfr,zf)./(1+zf);

   %computing the probability distribution for time-delay
   pnorm = log(tmax/waveform.tmin);
   tem2 = t.^waveform.order./pnorm .* (waveform.tmin < t) .* (t < tmax) ;

   %next, compute the integral over time-delay
   tem  = tem1 .* tem2;
   rate = diff(trapez) * tem(1:end-1,:) .* tsup;

   %finally compute the integrand of the redshift integral
   Iz = rate./denom;

elseif strcmp(analysisType,'BBHDynamic')
   model = @BBH_dynamic_model;
   prior = {'NN', 'uniform', -20, -10, 'fixed'; ...
           'flag', 'uniform', 1, 5, 'fixed'};

elseif strcmp(analysisType,'DNSWide')
   model = @DNS_model;
   prior = {'M', 'uniform', 1, 20, 'fixed'; ...
           'lam', 'uniform', -5, -10, 'fixed'};
elseif strcmp(analysisType,'sunva')
   model = @sunva_model;
   prior = {'alpha', 'uniform', 0, 2, 'fixed'; ...
           'beta', 'uniform', 0, 2, 'fixed'};
elseif strcmp(analysisType,'MAG')
   model = @MAG_model;
   prior = {'B', 'uniform', log10(7e13), 17, 'fixed'; ...
            'epsilon', 'uniform', -6, -2, 'fixed'; ...
            'P0', 'uniform', 0.001, 0.005, 'fixed'; ...
            'lambda', 'uniform', -4, -2, 'fixed'};
elseif strcmp(analysisType,'NSTurb')
   model = @NSTurb_model;
   prior = {'d0m', 'uniform', -4, 3, 'fixed'; ...
            'N', 'uniform', -6, 0, 'fixed'; ...
            'v', 'uniform', -1, 1, 'fixed'; ...
            'M_star', 'uniform', 1, 2, 'fixed'; ...
            'R_star', 'uniform', 7, 20, 'fixed'};
elseif strcmp(analysisType,'PBB')
   model = @PBB_model;
   prior = {'mu', 'uniform', 0, 2, 'fixed'; ...
            'F1', 'uniform', 8, 11, 'fixed'; ...
            'Fs', 'uniform', 0, 11, 'fixed'};
elseif strcmp(analysisType,'CS')
   model = @CS_model;
   prior = {'P', 'uniform', -3, 0, 'fixed'; ...
            'epsilon', 'uniform', -13, 0, 'fixed'; ...
            'Gmu', 'uniform', -12, -6, 'fixed'};
elseif strcmp(analysisType,'rmode')
   model = @rmode_model;
   prior = {'P0', 'uniform', 0.5, 10, 'fixed'; ...
           'lambda', 'uniform', -10, -2, 'fixed'};
elseif strcmp(analysisType,'barmode')
   model = @barmode_model;
   prior = {'P0', 'uniform', 0.5, 10, 'fixed'; ...
           'lambda', 'uniform', -10, -2, 'fixed'};
else
   error('Unknown model...');
end

