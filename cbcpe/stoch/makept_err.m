
set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

ptEstInt = load('output/output_ccspectra.job1.trial1.dat');
sensInt = load('output/output_sensints.job1.trial1.dat');
ff = ptEstInt(:,3);
deltaF = ff(2)-ff(1);
sigma_J = 1./sqrt(sensInt(:,4)*deltaF);
Y_J = 2*deltaF*sum(sensInt(:,4)*deltaF)*ptEstInt(:,4)./(sensInt(:,4)*deltaF);

% cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
var_J = sigma_J.^2;
cut = isnan(Y_J);
Y_J(cut) = 0;
fprintf('Point Estimate = %g \n',sum(real(Y_J)./var_J)/sum(1./var_J));
fprintf('Final error bar = %g \n',sqrt(1/sum(1./var_J)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fRef = 100;
alpha = 2/3;
Y_J_par = Y_J.*(ff/fRef).^alpha;
sigma_J_par = sigma_J.*(ff/fRef).^alpha; 

det1 = getdetector('LHO');
det2 = getdetector('LLO');
gamma = overlapreductionfunction(ff,det1,det2);

dataSet = '2ideal';
baseplotDir = '/home/mcoughlin/Stochastic/MDC/PE/plots_stoch';
plotDir = [baseplotDir '/' dataSet];
createpath(plotDir);

plotName = [plotDir '/Y'];
figure;
loglog(ff,abs(Y_J),'k*');
hold on
loglog(ff,sigma_J,'b*');
loglog(ff,powerlaw(ff,26,2/3),'m--');
loglog(ff,powerlaw(ff,24,1+(2/3)),'r--');
hold off
xlabel('Frequency [Hz]');
ylabel('\Omega_{GW}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

