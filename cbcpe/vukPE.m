%this script is meant to study parameter estimation for BNS

flag = 1; %1 to compute likelihood, 0 to just plot computed likelihood

if isempty(which('DNS'))
   %addpath(genpath('C:\Users\Vuk\Dropbox\matlab\stoch\models\chengjian\2dscan-new')); 
   addpath(genpath('/Users/dmeacher/WorkSpace/Stochastic/aLIGO_MDC_PE'));
end

%Mmat = kron(M,ones(size(lam)));
%lammat = kron(lam,ones(size(M)));


if 0   %S5HL result, SFR Nagamine et al, P~1/t, tmin=0.02
    sfr = 'n';
    waveform.form = 'power';
    waveform.order = -1;
    waveform.tmin = 0.02;
    load input/S5HL_upperlimit_freqdep.mat
    cut = ff1 >= 41.5 & ff1<=169.25 & ~isnan(Y_J);
    freq = ff1(cut);
    YY = real(Y_J(cut));
    vv = sigma2_J(cut);
    M = linspace(1,2.5,20);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(0.02,0.6,30));
    fname = 'results/BNSlik_S5HL_SFRNag_pow1_tmin002.mat';
    fname_likelihood = 'results/BNS_S5HL_SFRNag_pow1_tmin002_likelihood';
    fname_contours = 'results/BNS_S5HL_SFRNag_pow1_tmin002_contours';
end

if 0   %S5HL result, SFR Hopkins, P~1/t, tmin=0.02
    sfr = 'h';
    waveform.form = 'power';
    waveform.order = -1;
    waveform.tmin = 0.02;
    load input/S5HL_upperlimit_freqdep.mat
    cut = ff1 >= 41.5 & ff1<=169.25 & ~isnan(Y_J);
    freq = ff1(cut);
    YY = real(Y_J(cut));
    vv = sigma2_J(cut);
    M = linspace(1,2.5,20);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(0.02,0.6,30));
    fname = 'results/BNSlik_S5HL_pow1_tmin002.mat';
    fname_likelihood = 'results/BNS_S5HL_pow1_tmin002_likelihood';
    fname_contours = 'results/BNS_S5HL_pow1_tmin002_contours';
end

if 0   %S5HL result, SFR Hopkins, P~1/t^1.5, tmin=0.02
    sfr = 'h';
    waveform.form = 'power';
    waveform.order = -1.5;
    waveform.tmin = 0.02;
    load input/S5HL_upperlimit_freqdep.mat
    cut = ff1 >= 41.5 & ff1<=169.25 & ~isnan(Y_J);
    freq = ff1(cut);
    YY = real(Y_J(cut));
    vv = sigma2_J(cut);
    M = linspace(1,2.5,20);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(0.02,0.6,30));
    fname = 'results/BNSlik_S5HL_pow15_tmin002.mat';
    fname_likelihood = 'results/BNS_S5HL_pow15_tmin002_likelihood';
    fname_contours = 'results/BNS_S5HL_pow15_tmin002_contours';
end

if 0 %aLIGO H1H2, SFR Hopkins, P~1/t, tmin=0.02
    sfr = 'h';
    waveform.form = 'power';
    waveform.order = -1;
    waveform.tmin = 0.02;
    load input/aLIGO_sigma_f_new.mat
    freq = f1;
    YY = zeros(size(freq));
    vv = sigma_f.^2;
    M = linspace(1,2.5,20);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(0.5,10,30))*1e-5;
    fname = 'results/BNSlik_aLIGO_pow1_tmin002_new.mat';
    fname_likelihood = 'results/BNS_aLIGO_pow1_tmin002_likelihood_new';
    fname_contours = 'results/BNS_aLIGO_pow1_tmin002_contours_new';
end

if 0 %aLIGO H1H2, SFR Nagamine, P~1/t, tmin=0.02
    sfr = 'n';
    waveform.form = 'power';
    waveform.order = -1;
    waveform.tmin = 0.02;
    load input/aLIGO_sigma_f_new.mat
    freq = f1;
    YY = zeros(size(freq));
    vv = sigma_f.^2;
    M = linspace(1,2.5,20);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(0.5,10,30))*1e-5;
    fname = 'results/BNSlik_aLIGO_SFRNag_pow1_tmin002_new.mat';
    fname_likelihood = 'results/BNS_aLIGO_SFRNag_pow1_tmin002_likelihood_new';
    fname_contours = 'results/BNS_aLIGO_SFRNag_pow1_tmin002_contours_new';
end

if 0 %aLIGO H1H2, SFR Hopkins, P~1/t^1.5, tmin=0.02
    sfr = 'h';
    waveform.form = 'power';
    waveform.order = -1.5;
    waveform.tmin = 0.02;
    load input/aLIGO_sigma_f_new.mat
    freq = f1;
    YY = zeros(size(freq));
    vv = sigma_f.^2;
    M = linspace(1,2.5,20);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(0.5,10,30))*1e-5;
    fname = 'results/BNSlik_aLIGO_pow15_tmin002_new.mat';
    fname_likelihood = 'results/BNS_aLIGO_pow15_tmin002_likelihood_new';
    fname_contours = 'results/BNS_aLIGO_pow15_tmin002_contours_new';
end



if 0 %aLIGO H1H2 with signal Mc=1.22, SFR Hopkins, P~1/t, tmin=0.02
    sfr = 'h';
    waveform.form = 'power';
    waveform.order = -1;
    waveform.tmin = 0.02;
    load input/aLIGO_sigma_f_2Hz_new.mat
    freq = f1;
    [domain,YY] = DNS(freq,1.22,9.5e-5,sfr,waveform);
    YY = YY;
    vv = sigma_f.^2;
    M = linspace(1,2.5,1000);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(0.1,10,1000))*1e-4;
    fname = 'results/BNSlik_aLIGO_pow1_tmin002_signalMc122_fineres_new.mat';
    fname_likelihood = 'results/BNS_aLIGO_pow1_tmin002_signalMc122_likelihood_fineres_new';
    fname_contours = 'results/BNS_aLIGO_pow1_tmin002_signalMc122_contours_fineres_new';
%    M = linspace(1,2.5,20);
%    lam = transpose(linspace(0.02,0.6,30));
end

if 0 %aLIGO H1L1 early, SFR Hopkins, P~1/t, tmin=0.02
    sfr = 'h';
    waveform.form = 'power';
    waveform.order = -1;
    waveform.tmin = 0.02;
    load C:\Users\Vuk\Dropbox\matlab\stoch\sensitivity\results\aLIGO_H1L1_early.mat
    freq = ff;
    YY = zeros(size(freq));
    vv = sigma_f.^2;
    M = linspace(1,2.5,20);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(20,2000,30))*1e-5;
    fname = 'results/BNSlik_aLIGO_H1L1early_pow1_tmin002.mat';
    fname_likelihood = 'results/BNS_aLIGO_H1L1early_pow1_tmin002_likelihood';
    fname_contours = 'results/BNS_aLIGO_H1L1early_pow1_tmin002_contours';
end

if 0 %aLIGO H1L1 mid, SFR Hopkins, P~1/t, tmin=0.02
    sfr = 'h';
    waveform.form = 'power';
    waveform.order = -1;
    waveform.tmin = 0.02;
    load C:\Users\Vuk\Dropbox\matlab\stoch\sensitivity\results\aLIGO_H1L1_mid.mat
    freq = ff;
    YY = zeros(size(freq));
    vv = sigma_f.^2;
    M = linspace(1,2.5,20);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(5,1000,30))*1e-5;
    fname = 'results/BNSlik_aLIGO_H1L1mid_pow1_tmin002.mat';
    fname_likelihood = 'results/BNS_aLIGO_H1L1mid_pow1_tmin002_likelihood';
    fname_contours = 'results/BNS_aLIGO_H1L1mid_pow1_tmin002_contours';
end

if 1 %aLIGO H1L1 late, SFR Hopkins, P~1/t, tmin=0.02
    sfr = 'h';
    waveform.form = 'power';
    waveform.order = -1;
    waveform.tmin = 0.02;
    load('aLIGO_H1L1_late');
    freq = ff;
    YY = zeros(size(freq));
    vv = sigma_f.^2;
    M = linspace(1,2.5,20);
    %lam = transpose(logspace(-4,0,10));
    lam = transpose(linspace(0.5,100,30))*1e-5;
    fname = 'results/BNSlik_aLIGO_H1L1late_pow1_tmin002.mat';
    fname_likelihood = 'results/BNS_aLIGO_H1L1late_pow1_tmin002_likelihood';
    fname_contours = 'results/BNS_aLIGO_H1L1late_pow1_tmin002_contours';
end

%{
sfr = 'h';
waveform.form = 'power';
waveform.order = -1;
waveform.tmin = 0.02;
freq = ff;
YY = zeros(size(freq));
vv = sigma_f.^2;
M = linspace(1,2.5,20);
lam = transpose(linspace(0.5,100,30))*1e-5;
%}

if flag==1
%lik = zeros(length(lam),length(M));
loglik = zeros(length(lam),length(M));

%[domain,omega] = Dual_NS(freq,1.22,1e-5,'h');
%[domain,omega] = Dual_NS(freq,1.22,3e-3,'h');
%norm = 0.5*sum((YY - omega').^2 ./ vv );
for kk = 1:length(M)
    disp([num2str(kk) '/' num2str(length(M))])
    %[domain,omega] = Dual_NS(freq,M(kk),lam(1),sfr);
    [domain,omega] = DNS(freq,M(kk),lam(1),sfr,waveform);
    loglik(1,kk) = -0.5*sum((YY - omega).^2 ./ vv ) ;

    for jj = 2:length(lam)
        omega2 = omega * lam(jj) / lam(1);
        loglik(jj,kk) = -0.5*sum((YY - omega2).^2 ./ vv ) ;
    end
end

RVzero = BC_RatePerVol(sfr,waveform,0)*1e6;
save(fname,'M','lam','loglik','RVzero');

else
    
load(fname);

end

if 0
figure(100);
clf
surf(M, lam, lik);
axis([min(M) max(M) min(lam) max(lam)]);
xlabel('M_c');
ylabel('\lambda');
zlabel('L');
view([0 90]);
%pretty;
eval(['print -dpng ' fname_likelihood '.png']);
eval(['print -depsc ' fname_likelihood '.eps']);

zmax = max(lik(:));
zvals = 0:zmax/2000:zmax;
znorm = sum(lik(:));
for ll=1:length(zvals)
  prob(ll) = sum(sum(lik(lik>=zvals(ll))));
end
prob = prob/znorm;
%fprintf('Contours available up to p=%1.4f.\n', prob(2));

zvals68 = zvals(abs(prob-0.68)==min(abs(prob-0.68)));
zvals95 = zvals(abs(prob-0.95)==min(abs(prob-0.95)));
zvals99 = zvals(abs(prob-0.99)==min(abs(prob-0.99)));
zcontours = [zvals68 zvals95 zvals99];

figure(200);
clf
[C, h] = contour(M, lam, lik, zcontours);
xlabel('M_c');
ylabel('\lambda');
zlabel('L');
%pretty;
eval(['print -dpng ' fname_contours '.png']);
eval(['print -depsc ' fname_contours '.eps']);

end




