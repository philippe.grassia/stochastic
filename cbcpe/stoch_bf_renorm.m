function stoch_bf_renorm(analysisType,dataSet,doRun);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/MDC/PE/plots_bf_renorm';
plotDir = [baseplotDir '/' dataSet '/' analysisType];
createpath(plotDir);

[ff,omega_data,sigma_data,fmin,fmax,ff_true,omega_true,sigma_true] = get_data(analysisType,dataSet,plotDir);
%omega_data = sigma_data;
sigma_data = ones(size(sigma_data));
%omega_data = sigma_data;
%sigma_data = omega_data;

if strcmp(dataSet,'2ideal')
   indexes = find(ff >= 10 & ff <= 400);
   ff = ff(indexes);
   omega_data = omega_data(indexes);
   sigma_data = sigma_data(indexes);
elseif strcmp(dataSet,'3ideal')
   indexes = find(ff >= 10 & ff <= 200);
   ff = ff(indexes);
   omega_data = omega_data(indexes);
   sigma_data = sigma_data(indexes);
elseif strcmp(dataSet,'4ideal')
   indexes = find(ff >= 10 & ff <= 400);
   ff = ff(indexes);
   omega_data = omega_data(indexes);
   sigma_data = sigma_data(indexes);
elseif strcmp(dataSet,'5ideal')
   indexes = find(ff >= 10 & ff <= 90);
   ff = ff(indexes);
   omega_data = omega_data(indexes);
   sigma_data = sigma_data(indexes);
elseif strcmp(dataSet,'O1testideal') || strcmp(dataSet,'O1test')
   indexes = find(ff >= 10 & ff <= 100);
   ff = ff(indexes);
   omega_data = omega_data(indexes);
   sigma_data = sigma_data(indexes);

   indexes = find(ff >= 10 & ff <= 100);
   ff_true = ff_true(indexes);
   omega_true = omega_true(indexes);
   sigma_true = sigma_true(indexes);

   omega_true = interp1(ff_true,omega_true,ff);
end

matFile = [plotDir '/bf.mat'];

if doRun

   fRefOld = 1;
   alphaExpOld = 0; 

   fRefs = 40:41;
   alphaExps = linspace(-3,3,1000);

   y = zeros(length(fRefs),length(alphaExps));
   sigma = zeros(length(fRefs),length(alphaExps));

   for i = 1:length(fRefs)
      fprintf('%d\n',i);
      for j = 1:length(alphaExps)

         fRefNew = fRefs(i);
         alphaExpNew = alphaExps(j);

         ccSpectrum = omega_data; sensInt = sigma_data;
         flow = ff(1); fhigh = ff(end); deltaF = ff(2)-ff(1);
         modifyFilter = ones(size(ff)).* ff.^(alphaExpNew - alphaExpOld) * ...
            fRefOld^alphaExpOld / fRefNew^alphaExpNew;
         modifyFilter = modifyFilter ./ sum(modifyFilter);

         [ccStatNew, ccSigmaNew, ccSpectrumNew, sensIntNew] = ...
            renormalizeData(ccSpectrum, sensInt, modifyFilter, flow, fhigh, deltaF);

         y(i,j) = ccStatNew;
         sigma(i,j) = ccSigmaNew;

      end
   end

   snr = y./sigma;

   modifyFilter = omega_true;
   %modifyFilter = modifyFilter ./ sum(modifyFilter);

   [ccStatNew, ccSigmaNew, ccSpectrumNew, sensIntNew] = ...
       renormalizeData(ccSpectrum, sensInt, modifyFilter, flow, fhigh, deltaF);

   y_true = ccStatNew;
   sigma_true = ccSigmaNew;
   snr_true = y_true / sigma_true;

   fprintf('%.5e %.5e %.5f\n',y_true,sigma_true,snr_true);
   keyboard

   save(matFile);
else
   %load(matFile);
   load(matFile,'snr','y','sigma','rRefs','alphaExps');
end

params.savePlots = 1;

if params.savePlots


   [C,I] = max(snr(:));
   [I1,I2] = ind2sub(size(snr),I);
   snr_max = snr(I1,I2);

   alphaExpNew = 2/3; fRefNew = 1;
   modifyFilter1 = 1e-9 * ones(size(ff)).* ff.^(alphaExpNew - alphaExpOld) * ...
      fRefOld^alphaExpOld / fRefNew^alphaExpNew;
   %modifyFilter = modifyFilter ./ sum(modifyFilter);
   [ccStatNew_1, ccSigmaNew_1, ccSpectrumNew_1, sensIntNew_1] = ...
      renormalizeData(ccSpectrum, sensInt, modifyFilter1, flow, fhigh, deltaF);

   alphaExps(I2)
   fRefs(I1)

   alphaExpNew = alphaExps(I2); fRefNew = fRefs(I1);
   modifyFilter2 = 1e-9 * ones(size(ff)).* ff.^(alphaExpNew - alphaExpOld) * ...
      fRefOld^alphaExpOld / fRefNew^alphaExpNew;
   %modifyFilter = modifyFilter ./ sum(modifyFilter);
   [ccStatNew_2, ccSigmaNew_2, ccSpectrumNew_2, sensIntNew_2] = ...
      renormalizeData(ccSpectrum, sensInt, modifyFilter2, flow, fhigh, deltaF);

   plotName = [plotDir '/omega_best'];
   figure;
   loglog(ff,omega_data,'k--');
   hold on
   loglog(ff,modifyFilter1,'b');
   loglog(ff,modifyFilter2,'r');
   hold off
   xlabel('Frequency [Hz]');
   ylabel('\Omega_{GW}');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   plotName = [plotDir '/alpha'];
   figure()
   plot(alphaExps,snr(1,:));
   %set(gca,'YDir','normal')
   %set(gca,'XScale','lin');
   hold on
   plot([alphaExps(I2) alphaExps(I2)],[min(snr(1,:)) max(snr(1,:))],'r--')
   hold off
   xlabel('alpha');
   ylabel('SNR');
   title(sprintf('Max SNR: %.10f',snr_true));
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   plotName = [plotDir '/alphanorm'];
   figure()
   plot(alphaExps,snr(1,:)./snr_true);
   %set(gca,'YDir','normal')
   %set(gca,'XScale','lin');
   hold on
   plot([alphaExps(I2) alphaExps(I2)],[min(snr(1,:)) max(snr(1,:))]./snr_true,'r--')
   hold off
   xlabel('alpha');
   ylabel('SNR');
   title(sprintf('Max SNR: %.10f',max(snr(1,:)./snr_true)));
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   [junk,index] = min(abs(alphaExps-2/3));
   fprintf('Best Alpha: %.10f, Normalized SNR: %.10f\n',alphaExps(I2),max(snr(1,:)./snr_true));
   fprintf('2/3 Alpha: %.10f, Normalized SNR: %.10f\n',alphaExps(index),snr(1,index)./snr_true);

   [X,Y] = meshgrid(fRefs,alphaExps);

   plotName = [plotDir '/contour'];
   figure()
   pcolor(X,Y,snr');
   %set(gca,'YDir','normal')
   %set(gca,'XScale','lin');
   xlabel('rRef');
   ylabel('alpha');
   cbar = colorbar;
   set(get(cbar,'title'),'String','log10(y)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;
%   keyboard

end




