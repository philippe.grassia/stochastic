function mnPE(analysisType,dataSet,doRun);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

baseplotDir = '/home/mcoughlin/Stochastic/MDC/PE/plots_mn';
plotDir = [baseplotDir '/' dataSet '/' analysisType];
createpath(plotDir);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

[ff,omega_data,sigma_data,fmin,fmax] = get_data(analysisType,dataSet,plotDir);

det1 = getdetector('LHO');
det2 = getdetector('LLO');
gamma = overlapreductionfunction(ff,det1,det2);

psd = load(psdfile);
indexes = find(ff >= fmin & ff <= fmax);
ff = ff(indexes);
omega_data = omega_data(indexes);
%omega_psd_data = omega_psd_data(indexes);
sigma_data = sigma_data(indexes);
psd_data = interp1(psd(:,1),psd(:,2),ff);
psd_data_norm = psd_data./sum(psd_data);
psd_data_norm = ones(size(psd_data_norm));

YY = omega_data;
vv = sigma_data.^2;

[model,prior] = get_model(analysisType);
likelihood = @stoch_logL;

data = {};
data{1} = YY;
%vv = ones(size(YY));
data{2} = vv;
data{3} = ff;
data{4} = psd_data;

global verbose;
verbose = 1;
global DEBUG;
DEBUG = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% define nested sampling parameters
Nlive = 200;
%Nlive = 1000;

Nmcmc = 0;
%Nmcmc = 100;
%tolerance = 0.0001;
%tolerance = 1e-8;
%tolerance = 0.1;
%tolerance = 1;
%tolerance = 0.001;
%tolerance = 1e-4;
tolerance = 1e-6;
tolerance = 1e-20;

if strcmp(analysisType,'powerlawPSD');
   likelihood = @stoch_logL_psd;
else
   likelihood = @stoch_logL;
end

extraparams = {};

params.plotdir = plotDir;
params.savePlots = 1;

[size_prior_x,size_prior_y] = size(prior);

matFile = [params.plotdir '/multinest.mat'];
if doRun
   % called nested sampling routine
   [logZ, nest_samples, post_samples] = stoch_nested_sampler(params, data, Nlive, Nmcmc, ...
      tolerance, likelihood, model, prior, extraparams);
   save(matFile);
else
   %load(matFile);
   load(matFile,'logZ','nest_samples','post_samples');
end

if params.savePlots

   %nest_samples = nest_samples(end-3000:end,:);
   nest_samples = nest_samples(end-1000:end,:);
   %nest_samples = nest_samples(floor(length(nest_samples(:,1))*0.1):end,:);

   nest_samples = post_samples;

   % plot posterior distributions
   for i = 1:size_prior_x
      plotName = [params.plotdir '/PE_' prior{i,1}];
      stoch_posteriors(post_samples, i, {prior{i,1}}, plotName,analysisType,dataSet);

      plotName = [params.plotdir '/PE_nest_post_' prior{i,1}];
      stoch_nest_posteriors(nest_samples, i, {prior{i,1}}, plotName,analysisType,dataSet);

      plotName = [params.plotdir '/PE_nest_' prior{i,1}];
      stoch_nest_samples_plot(nest_samples, i, {prior{i,1}}, plotName,analysisType,dataSet);

      for j = 1:size_prior_x
         if j<=i
            continue
         end

         plotName = [params.plotdir '/PE_' prior{i,1} '_' prior{j,1}];
         stoch_posteriors(post_samples, [i j], {prior{i,1}, prior{j,1}}, plotName,analysisType,dataSet);
         plotName = [params.plotdir '/PE_logl_' prior{i,1} '_' prior{j,1}];
         %posteriors_logl(post_samples, [i j], {prior{i,1}, prior{j,1}}, plotName);

      end

   end

   if strcmp(analysisType,'powerlaw');

      a = median(nest_samples(:,1));
      k = median(nest_samples(:,2));
      powerlaw_data = powerlaw(ff,a,k);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,3));
      fprintf('a: %.5f, k: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3));
      fprintf('k: %.5f +- %.5f\n',median(nest_samples(:,2)),std(nest_samples(:,2)));
      fprintf('a: %.5e +- %.5e\n',10^median(nest_samples(:,1)),10^(median(nest_samples(:,1)))*log(10)*std(nest_samples(:,1)));

   elseif strcmp(analysisType,'powerlaw2');

      a = median(nest_samples(:,1));
      k = median(nest_samples(:,2));
      b = median(nest_samples(:,3));
      l = median(nest_samples(:,4));
      powerlaw_data_1 = powerlaw(ff,a,k);
      powerlaw_data_2 = powerlaw(ff,b,l);
      powerlaw_data = powerlaw2(ff,a,k,b,l);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      loglog(ff,powerlaw_data_1,'r');
      loglog(ff,powerlaw_data_2,'g');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

   elseif strcmp(analysisType,'powerlawPSD');

      a = median(nest_samples(:,1));
      k = median(nest_samples(:,2));
      A = median(nest_samples(:,3));
      %K = median(nest_samples(:,4));
      powerlaw_data_1 = powerlaw(ff,a,k);
      %powerlaw_data_2 = aLIGOPSD(ff,A,K);
      powerlaw_data_2 = Sn .* 10^A;
      powerlaw_data = powerlaw_data_1 + powerlaw_data_2;

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      loglog(ff,powerlaw_data_1,'r');
      loglog(ff,powerlaw_data_2,'g');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      %ylim([1e-12 1e-6]);
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;


      plotName = [plotDir '/omega_best_residual'];
      figure;
      loglog(ff,abs(omega_data-powerlaw_data_2),'k--');
      hold on
      loglog(ff,powerlaw_data_1,'r');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      ylim([1e-12 1e-6]);
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,4));
      fprintf('a: %.5f, k: %.5f, A: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3),nest_samples(index,4));
      fprintf('k: %.5f +- %.5f\n',median(nest_samples(:,2)),std(nest_samples(:,2)));
      fprintf('a: %.5e +- %.5e\n',10^median(nest_samples(:,1)),10^(median(nest_samples(:,1)))*log(10)*std(nest_samples(:,1)));
      %fprintf('K: %.5f +- %.5f\n',median(nest_samples(:,4)),std(nest_samples(:,4)));
      fprintf('A: %.5e +- %.5e\n',10^median(nest_samples(:,3)),10^(median(nest_samples(:,3)))*log(10)*std(nest_samples(:,3)));

   elseif strcmp(analysisType,'powerlaw2fmax');

      a = median(nest_samples(:,1));
      k = median(nest_samples(:,2));
      fmax1 = median(nest_samples(:,3));
      b = median(nest_samples(:,4));
      l = median(nest_samples(:,5));
      fmax2 = median(nest_samples(:,6));

      powerlaw_data_1 = powerlawfmax(ff,a,k,fmax1);
      powerlaw_data_2 = powerlawfmax(ff,b,l,fmax2);
      powerlaw_data = powerlaw2fmax(ff,a,k,fmax1,b,l,fmax2);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      loglog(ff,powerlaw_data_1,'r');
      loglog(ff,powerlaw_data_2,'g');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,7));
      fprintf('a: %.5f, k: %.5f, fmax1: %.5f, b: %.5f, l: %.5f, fmax2: %.5f likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3),nest_samples(index,4),nest_samples(index,5),nest_samples(index,6),nest_samples(index,7));
      fprintf('k: %.5f +- %.5f\n',median(nest_samples(:,2)),std(nest_samples(:,2)));
      fprintf('a: %.5e +- %.5e\n',10^median(nest_samples(:,1)),10^(median(nest_samples(:,1)))*log(10)*std(nest_samples(:,1)));
      fprintf('fmax1: %.5f +- %.5f\n',median(nest_samples(:,3)),std(nest_samples(:,3)));
      fprintf('l: %.5f +- %.5f\n',median(nest_samples(:,5)),std(nest_samples(:,5)));
      fprintf('b: %.5e +- %.5e\n',10^median(nest_samples(:,4)),10^(median(nest_samples(:,4)))*log(10)*std(nest_samples(:,4)));
      fprintf('fmax2: %.5f +- %.5f\n',median(nest_samples(:,6)),std(nest_samples(:,6)));

   elseif strcmp(analysisType,'DNS');
      M = median(nest_samples(:,1));
      lam = median(nest_samples(:,2));
      DNS_data = DNS(ff,M,lam);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,DNS_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,3));
      fprintf('M: %.5f, lam: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3));
      fprintf('M: %.5f +- %.5f\n',median(nest_samples(:,1)),std(nest_samples(:,1)));
      fprintf('lam: %.5e +- %.5e\n',10^median(nest_samples(:,2)),10^(median(nest_samples(:,2)))*log(10)*std(nest_samples(:,2)));

   elseif strcmp(analysisType,'DNSWide');
      M = median(nest_samples(:,1));
      lam = median(nest_samples(:,2));
      DNS_data = DNS(ff,M,lam);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,DNS_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,3));
      fprintf('M: %.5f, lam: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3));
      fprintf('M: %.5f +- %.5f\n',median(nest_samples(:,1)),std(nest_samples(:,1)));
      fprintf('lam: %.5e +- %.5e\n',10^median(nest_samples(:,2)),10^(median(nest_samples(:,2)))*log(10)*std(nest_samples(:,2)));

   elseif strcmp(analysisType,'DNS2');
      M1 = median(nest_samples(:,1));
      lam1 = median(nest_samples(:,2));
      M2 = median(nest_samples(:,3));
      lam2 = median(nest_samples(:,4));

      DNS_data_1 = DNS(ff,M1,lam1);
      DNS_data_2 = DNS(ff,M2,lam2);
      DNS_data = DNS2(ff,M1,lam1,M2,lam2);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,DNS_data,'b');
      loglog(ff,DNS_data_1,'r');
      loglog(ff,DNS_data_2,'g');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,5));
      fprintf('M1: %.5f, lam1: %.5f, M2: %.5f, lam2: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3),nest_samples(index,4),nest_samples(index,5));

   elseif strcmp(analysisType,'DNSDBH');
      M1 = median(nest_samples(:,1));
      lam1 = median(nest_samples(:,2));
      M2 = median(nest_samples(:,3));
      lam2 = median(nest_samples(:,4));

      DNS_data_1 = DNS(ff,M1,lam1);
      DNS_data_2 = DBH(ff,M2,lam2);
      DNS_data = DNSDBH(ff,M1,lam1,M2,lam2);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,DNS_data,'b');
      loglog(ff,DNS_data_1,'r');
      loglog(ff,DNS_data_2,'g');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,5));
      fprintf('M1: %.5f, lam1: %.5f, M2: %.5f, lam2: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3),nest_samples(index,4),nest_samples(index,5));
      fprintf('M1: %.5f +- %.5f\n',median(nest_samples(:,1)),std(nest_samples(:,1)));
      fprintf('lam1: %.5e +- %.5e\n',10^median(nest_samples(:,2)),10^(median(nest_samples(:,2)))*log(10)*std(nest_samples(:,2)));
      fprintf('M2: %.5f +- %.5f\n',median(nest_samples(:,3)),std(nest_samples(:,3)));
      fprintf('lam2: %.5e +- %.5e\n',10^median(nest_samples(:,4)),10^(median(nest_samples(:,4)))*log(10)*std(nest_samples(:,4)));


   elseif strcmp(analysisType,'DBH');
      M = median(nest_samples(:,1));
      lam = median(nest_samples(:,2));
      DBH_data = DBH(ff,M,lam);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,DBH_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,3));
      fprintf('M: %.5f, lam: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3));
      fprintf('M: %.5f +- %.5f\n',median(nest_samples(:,1)),std(nest_samples(:,1)));
      fprintf('lam: %.5e +- %.5e\n',10^median(nest_samples(:,2)),10^(median(nest_samples(:,2)))*log(10)*std(nest_samples(:,2)));

   elseif strcmp(analysisType,'NSBH');
      M = median(nest_samples(:,1));
      lam = median(nest_samples(:,2));
      NSBH_data = NSBH(ff,M,lam);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,NSBH_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,3));
      fprintf('M: %.5f, lam: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3));

   elseif strcmp(analysisType,'MAG');
      B = median(nest_samples(:,1));
      epsilon = median(nest_samples(:,2));
      P0 = median(nest_samples(:,3));
      lambda = median(nest_samples(:,4));

      MAG_data = MAG(ff,B,epsilon,P0,lambda);

      plotName = [plotDir '/omega_best'];
      figure;
      loglog(ff,omega_data,'k--');
      hold on
      loglog(ff,MAG_data,'b');
      hold off
      xlabel('Frequency [Hz]');
      ylabel('\Omega_{GW}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,5));
      fprintf('B: %.5f, epsilon: %.5f, P0: %.5f, lambda: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3),nest_samples(index,4),nest_samples(index,5));

   end

   xlab = get_name(prior{1,1});
   ylab = get_name(prior{2,1});

   plotName = [plotDir '/contour'];
   % make 2-d histogram plot
   wp = [1 2];
   p1 = wp(1);
   p2 = wp(2);
   nbins = 50;
   edges1 = linspace(min(nest_samples(:,p1)), max(nest_samples(:,p1)), nbins);
   edges2 = linspace(min(nest_samples(:,p2)), max(nest_samples(:,p2)), nbins);
   histmat = hist2(nest_samples(:,p1), nest_samples(:,p2), edges1, edges2);

   figure()
   numcontours = 5;
   contourf(edges1, edges2, transpose(histmat), numcontours);
   %xlim([0 2e-9]);
   %ylim([0.4 0.7]);
   xlabel(xlab);
   ylabel(ylab);
   set(gca,'XScale','lin');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   if strcmp(analysisType,'powerlawPSD')
      lik = nest_samples(:,4);
   else
      lik = nest_samples(:,3);
   end
   plotName = [plotDir '/likelihood'];
   figure()
   scatter(nest_samples(:,1), nest_samples(:,2), 20, lik,'filled');
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   set(get(cbar,'ylabel'),'String','Likelihood');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;
 
   %x = 10.^nest_samples(:,1); y = nest_samples(:,2); lik = nest_samples(:,3);
   x = linspace(min(nest_samples(:,1)),max(nest_samples(:,1)),1000);
   y = linspace(min(nest_samples(:,2)),max(nest_samples(:,2)),1000);
   [X,Y] = meshgrid(x,y);

   [Xq,Yq,Vq] = griddata(nest_samples(:,1), nest_samples(:,2), lik, X,Y);
   Vq(isnan(Vq)) = min(Vq(:));

   samples = sort(Vq(:),'descend');
   cmin = samples(floor(length(samples)*0.25)); cmax = samples(1);

   plotName = [plotDir '/likelihood_interp'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   set(get(cbar,'ylabel'),'String','Likelihood');
   caxis([cmin cmax]);
   %print('-dpng',plotName);
   %print('-depsc2',plotName);
   close;

   lik = Vq;

   norm = max(lik(:));
   lik = exp(lik - norm);
   Vq = lik';
   %Vq = Vq - max(Vq(:));

   %% make 2-d histogram plot
   %wp = [1 2];
   %p1 = wp(1);
   %p2 = wp(2);
   %nbins = 500;
   %edges1 = linspace(min(nest_samples(:,p1)), max(nest_samples(:,p1)), nbins);
   %edges2 = linspace(min(nest_samples(:,p2)), max(nest_samples(:,p2)), nbins);
   %histmat = hist2(nest_samples(:,p1), nest_samples(:,p2), edges1, edges2);

   %[Xq,Yq] = meshgrid(edges1,edges2);
   %Vq = histmat;
   %lik = Vq;

   %[X,Y] = meshgrid(edges1,edges2);

   %plotName = [plotDir '/hist2d'];
   %figure()
   %pcolor(edges1, edges2, histmat);
   %xlim([0 2e-9]);
   %ylim([0.4 0.7]);
   %xlabel(xlab);
   %ylabel(ylab);
   %print('-dpng',plotName);
   %print('-depsc2',plotName);
   %close;

   zmax = max(lik(:));
   zvals = 0:zmax/2000:zmax;
   zvals = linspace(0,zmax,10000);
   znorm = sum(lik(:));
   for ll=1:length(zvals)
      prob(ll) = sum(sum(lik(lik>=zvals(ll))));
   end
   prob = prob/znorm;
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD') || strcmp(analysisType,'DNS');
      prob = fliplr(prob);
   end
  
   zvals68 = zvals(abs(prob-0.68)==min(abs(prob-0.68)));
   zvals95 = zvals(abs(prob-0.95)==min(abs(prob-0.95)));
   zvals99 = zvals(abs(prob-0.99)==min(abs(prob-0.99)));
   zcontours = [zvals68(1) zvals95(1) zvals99(1)];

   plotName = [plotDir '/likecontour'];
   figure;
   [C, h] = contour(Xq, Yq, Vq, zcontours(1),'k');
   hold on
   [C, h] = contour(Xq, Yq, Vq, zcontours(2),'b');
   [C, h] = contour(Xq, Yq, Vq, zcontours(3),'r');
   hold off
   xlabel(xlab);
   ylabel(ylab);
   %cbar = colorbar;
   %set(get(cbar,'ylabel'),'String','Probability');

   get_injplot(analysisType,dataSet);

   legend('68 CL','95 CL','99 CL','True value','Location','northeast','Orientation','horizontal')
   pretty;
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;
 
end



