function [ff,omega_data,sigma_data,fmin,fmax,ff_true,omega_true,sigma_true] = ...
   getdata(analysisType,dataSet,plotDir)

fmin = 10;
fmax = 500;

if strcmp(dataSet,'2ideal')
   postDir = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/PostProcessing/H1L1/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 100; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/omega.txt';
   omega = load(omegafile);
   ff_omega = omega(:,1);
   omega_data = omega(:,2);
   omega_psd_data = omega(:,3);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 1500;

   if strcmp(analysisType,'powerlaw')
      fmin = 10;
      fmax = 400;
   end

   plotName = [plotDir '/bestfit.png'];
   figure;
   loglog(ff_omega,omega_data,'k--');
   hold on
   loglog(ff,powerlaw(ff,-9.2,2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   %omega_data = Y_J_par;
   omega_data = interp1(ff_omega,omega_data,ff);
   sigma_data = sigma_J_par;

elseif strcmp(dataSet,'3ideal')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_3_BBH_nonDet/omega.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 1500;

   plotName = [plotDir '/bestfit.png'];
   figure;
   loglog(ff,omega_data,'k--');
   hold on
   loglog(ff,powerlaw(ff,-9.5,2/3),'b');
   hold off
   print('-dpng',plotName);
   close;
   if strcmp(analysisType,'powerlaw')
      fmin = 10;
      fmax = 400;
   end

elseif strcmp(dataSet,'4ideal')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_4_BNS_BBH_nonDet/omega_all.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 1500;

   bnsfile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_4_BNS_BBH_nonDet/omega_bns.txt';
   bns = load(bnsfile);
   omega_bns = bns(:,2);

   bbhfile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_4_BNS_BBH_nonDet/omega_bbh.txt';
   bbh = load(bbhfile);
   omega_bbh = bbh(:,2);

   plotName = [plotDir '/bnsbbh.png'];
   figure;
   loglog(ff,omega_data,'k--');
   hold on
   loglog(ff,omega_bns,'b');
   loglog(ff,omega_bbh,'r');
   hold off
   print('-dpng',plotName);
   close;

   plotName = [plotDir '/bestfit.png'];
   figure;
   loglog(ff,omega_data,'k--');
   hold on
   loglog(ff,powerlaw(ff,-9.0,2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   if strcmp(analysisType,'powerlaw')
      fmin = 10;
      fmax = 400;
   end

elseif strcmp(dataSet,'5ideal')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_5_BNS_BBH_IMBH_High_nonDet/omega_all.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 300;

   plotName = [plotDir '/imbh.png'];
   figure;
   loglog(ff,omega_data,'k--');
   print('-dpng',plotName);
   close;

   plotName = [plotDir '/bestfit.png'];
   figure;
   loglog(ff,omega_data,'k--');
   hold on
   loglog(ff,powerlaw(ff,-9.0,2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   if strcmp(analysisType,'powerlaw')
      fmin = 10;
      fmax = 400;
   end

elseif strcmp(dataSet,'6ideal')
   omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_6_BNS_BBH_IMBH_High_nonDet/omega_all.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 300;

   plotName = [plotDir '/imbh.png'];
   figure;
   loglog(ff,omega_data,'k--');
   print('-dpng',plotName);
   close;

   if strcmp(analysisType,'powerlaw')
      fmin = 10;
      fmax = 400;
   end

elseif strcmp(dataSet,'O1testideal')
   omegafile = '/home/dmeacher/stochastic_MDC/O1/data/test/omega.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   sigma_data = 1e-8 * ones(size(omega_data));
   fmin = 10;
   fmax = 300;

   indexes = find(isnan(omega_data));
   omega_data(indexes) = 0;
   sigma_data(indexes) = 1e100;

   plotName = [plotDir '/imbh.png'];
   figure;
   loglog(ff,omega_data,'k--');
   print('-dpng',plotName);
   close;

   if strcmp(analysisType,'powerlaw')
      fmin = 10;
      fmax = 400;
   end

   ff_true = ff;
   omega_true = omega_data;
   sigma_true = sigma_data;

elseif strcmp(dataSet,'O1test');
   postDir = '/home/mcoughlin/Stochastic/MDC/O1/HR/postprocessing/output/H1L1/10-100/0.666667/0.250000/';
   %postDir = '/home/dmeacher/StochAnalysis/PostProcessing/output/H1L1/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 40; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   Y_J_par = Y_J_par/0.7^2;
   sigma_J_par = sigma_J_par/0.7^2;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;

   omegafile = '/home/dmeacher/stochastic_MDC/O1/data/test/omega.txt';
   omega = load(omegafile);
   ff_true = omega(:,1);
   omega_true = omega(:,2);
   sigma_true = 1e-8 * ones(size(omega_data));

   indexes = find(isnan(omega_true));
   omega_true(indexes) = 0;
   sigma_true(indexes) = 0;

elseif strcmp(dataSet,'MDC2inj');
   postDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_inj_2/';

   ptEstIntFile = [postDir '/output_combined_ccspectra.job1.trial1.dat'];
   sensIntFile = [postDir '/output_combined_sensints.job1.trial1.dat'];

   ptEstInt = load(ptEstIntFile);
   sensInt = load(sensIntFile);

   ff = ptEstInt(:,3);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(sensInt(:,4)*deltaF);
   Y_J = 2*deltaF*sum(sensInt(:,4)*deltaF)*ptEstInt(:,4)./(sensInt(:,4)*deltaF);
   Y_J = abs(Y_J);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;
   fprintf('Point Estimate = %g \n',sum(real(Y_J)./var_J)/sum(1./var_J));
   fprintf('Final error bar = %g \n',sqrt(1/sum(1./var_J)));
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   fRef = 100;
   alpha = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alpha;
   sigma_J_par = sigma_J.*(ff/fRef).^alpha;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(2e-4),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = abs(Y_J_par).*abs(gamma);
   sigma_data = sigma_J_par.*abs(gamma);

elseif strcmp(dataSet,'MDC2inj_mid');
   postDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_inj_2_mid/';

   ptEstIntFile = [postDir '/output_combined_ccspectra.job1.trial1.dat'];
   sensIntFile = [postDir '/output_combined_sensints.job1.trial1.dat'];

   ptEstInt = load(ptEstIntFile);
   sensInt = load(sensIntFile);

   ff = ptEstInt(:,3);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(sensInt(:,4)*deltaF);
   Y_J = 2*deltaF*sum(sensInt(:,4)*deltaF)*ptEstInt(:,4)./(sensInt(:,4)*deltaF);
   Y_J = abs(Y_J);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;
   fprintf('Point Estimate = %g \n',sum(real(Y_J)./var_J)/sum(1./var_J));
   fprintf('Final error bar = %g \n',sqrt(1/sum(1./var_J)));
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   fRef = 100;
   alpha = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alpha;
   sigma_J_par = sigma_J.*(ff/fRef).^alpha;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(2e-6),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = abs(Y_J_par).*abs(gamma);
   sigma_data = sigma_J_par.*abs(gamma);

   plotName = [plotDir '/snr.png'];
   figure;
   loglog(ff,abs(Y_J_par)./sigma_J_par,'k--');
   print('-dpng',plotName);
   close;

   omega_data = abs(Y_J_par).*abs(gamma);
   sigma_data = sigma_J_par.*abs(gamma);

elseif strcmp(dataSet,'MDC2inj_low');
   postDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_inj_2_low/';

   ptEstIntFile = [postDir '/output_combined_ccspectra.job1.trial1.dat'];
   sensIntFile = [postDir '/output_combined_sensints.job1.trial1.dat'];

   ptEstInt = load(ptEstIntFile);
   sensInt = load(sensIntFile);

   ff = ptEstInt(:,3);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(sensInt(:,4)*deltaF);
   Y_J = 2*deltaF*sum(sensInt(:,4)*deltaF)*ptEstInt(:,4)./(sensInt(:,4)*deltaF);
   Y_J = abs(Y_J);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;
   fprintf('Point Estimate = %g \n',sum(real(Y_J)./var_J)/sum(1./var_J));
   fprintf('Final error bar = %g \n',sqrt(1/sum(1./var_J)));
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   fRef = 100;
   alpha = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alpha;
   sigma_J_par = sigma_J.*(ff/fRef).^alpha;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(2e-4),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = (Y_J_par).*abs(gamma);
   sigma_data = sigma_J_par.*abs(gamma);

elseif strcmp(dataSet,'none');
   postDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_none/';

   ptEstIntFile = [postDir '/output_combined_ccspectra.job1.trial1.dat'];
   sensIntFile = [postDir '/output_combined_sensints.job1.trial1.dat'];

   ptEstInt = load(ptEstIntFile);
   sensInt = load(sensIntFile);

   ff = ptEstInt(:,3);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(sensInt(:,4)*deltaF);
   Y_J = 2*deltaF*sum(sensInt(:,4)*deltaF)*ptEstInt(:,4)./(sensInt(:,4)*deltaF);
   %Y_J = abs(Y_J);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;
   fprintf('Point Estimate = %g \n',sum(real(Y_J)./var_J)/sum(1./var_J));
   fprintf('Final error bar = %g \n',sqrt(1/sum(1./var_J)));
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   fRef = 100;
   alpha = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alpha;
   sigma_J_par = sigma_J.*(ff/fRef).^alpha;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(2e-4),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par.*gamma;
   sigma_data = sigma_J_par.*abs(gamma);

elseif strcmp(dataSet,'0');
   postDir = '/home/dmeacher/StochAnalysis/output_Set0/PostProcessing/H1L1/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 100; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;

elseif strcmp(dataSet,'1');
   postDir = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_1_BNS_all/PostProcessing/H1L1/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 100; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   Y_J_par = Y_J_par/0.7^2;
   sigma_J_par = sigma_J_par/0.7^2;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;

elseif strcmp(dataSet,'2');
   postDir = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/PostProcessing/H1L1/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 100; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;
 
   Y_J_par = Y_J_par/0.7^2;
   sigma_J_par = sigma_J_par/0.7^2;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;

elseif strcmp(dataSet,'3');
   postDir = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_3_BBH_nonDet/PostProcessing/H1L1/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 100; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   Y_J_par = Y_J_par/0.7^2;
   sigma_J_par = sigma_J_par/0.7^2;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;

elseif strcmp(dataSet,'4');
   postDir = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_4_BNS_BBH_nonDet/PostProcessing/H1L1/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 100; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   Y_J_par = Y_J_par/0.7^2;
   sigma_J_par = sigma_J_par/0.7^2;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;

elseif strcmp(dataSet,'5');
   postDir = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_5_BNS_BBH_IMBH_High_nonDet/PostProcessing/H1L1/';
   %postDir = '/home/dmeacher/StochAnalysis/PostProcessing/output/H1L1/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 100; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;

elseif strcmp(dataSet,'6');
   postDir = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_6_BNS_BBH_IMBH_Low_nonDet/PostProcessing/H1L1/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 100; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;
 
elseif strcmp(dataSet,'ET');

   fmin = 10;
   fmax = 200;
   fRef = 100; alphaExp = 2/3;

   postDir = '/home/mcoughlin/Stochastic/MDC/ET/MDC2/postprocessing/output/E1E2/10-150/0.666667/0.250000/';
   ptEstIntFile = [postDir '/E1E2_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/E1E2_sensIntegrand.dat'];
   [ff,Y_J_par_1,sigma_J_par_1] = load_ptest(ptEstIntFile,sensIntFile,fRef,alphaExp);

   postDir = '/home/mcoughlin/Stochastic/MDC/ET/MDC2/postprocessing/output/E1E3/10-150/0.666667/0.250000/';
   ptEstIntFile = [postDir '/E1E3_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/E1E3_sensIntegrand.dat'];
   [ff,Y_J_par_2,sigma_J_par_2] = load_ptest(ptEstIntFile,sensIntFile,fRef,alphaExp);

   postDir = '/home/mcoughlin/Stochastic/MDC/ET/MDC2/postprocessing/output/E2E3/10-150/0.666667/0.250000/';
   ptEstIntFile = [postDir '/E2E3_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/E2E3_sensIntegrand.dat'];
   [ff,Y_J_par_3,sigma_J_par_3] = load_ptest(ptEstIntFile,sensIntFile,fRef,alphaExp);
 
   pe = zeros(size(Y_J_par_1)); eb = zeros(size(Y_J_par_1)); 
   eb = eb + sigma_J_par_1.^-2;
   pe = pe + Y_J_par_1./sigma_J_par_1.^2;
   eb = eb + sigma_J_par_2.^-2;
   pe = pe + Y_J_par_2./sigma_J_par_2.^2;
   eb = eb + sigma_J_par_3.^-2;
   pe = pe + Y_J_par_3./sigma_J_par_3.^2;

   combinedErrorBar = eb.^(-.5);
   combinedPtEstimate = pe./eb;

   plotName = [plotDir '/sigma.png'];
   figure;
   loglog(ff,eb,'k--');
   print('-dpng',plotName);
   close;

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par_1),'k--');
   hold on
   loglog(ff,abs(combinedPtEstimate),'b--');
   loglog(ff,powerlaw(ff,-11,2/3),'r');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = combinedPtEstimate;
   sigma_data = combinedErrorBar;

elseif strcmp(dataSet,'ET_noDetect');

   fmin = 10;
   fmax = 200;
   fRef = 100; alphaExp = 2/3;

   postDir = '/home/mcoughlin/Stochastic/MDC/ET/MDC2_noDetect/postprocessing/output/E1E2/10-150/0.666667/0.250000/';
   ptEstIntFile = [postDir '/E1E2_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/E1E2_sensIntegrand.dat'];
   [ff,Y_J_par_1,sigma_J_par_1] = load_ptest(ptEstIntFile,sensIntFile,fRef,alphaExp);

   postDir = '/home/mcoughlin/Stochastic/MDC/ET/MDC2_noDetect/postprocessing/output/E1E3/10-150/0.666667/0.250000/';
   ptEstIntFile = [postDir '/E1E3_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/E1E3_sensIntegrand.dat'];
   [ff,Y_J_par_2,sigma_J_par_2] = load_ptest(ptEstIntFile,sensIntFile,fRef,alphaExp);

   postDir = '/home/mcoughlin/Stochastic/MDC/ET/MDC2_noDetect/postprocessing/output/E2E3/10-150/0.666667/0.250000/';
   ptEstIntFile = [postDir '/E2E3_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/E2E3_sensIntegrand.dat'];
   [ff,Y_J_par_3,sigma_J_par_3] = load_ptest(ptEstIntFile,sensIntFile,fRef,alphaExp);

   pe = zeros(size(Y_J_par_1)); eb = zeros(size(Y_J_par_1));
   eb = eb + sigma_J_par_1.^-2;
   pe = pe + Y_J_par_1./sigma_J_par_1.^2;
   eb = eb + sigma_J_par_2.^-2;
   pe = pe + Y_J_par_2./sigma_J_par_2.^2;
   eb = eb + sigma_J_par_3.^-2;
   pe = pe + Y_J_par_3./sigma_J_par_3.^2;

   combinedErrorBar = eb.^(-.5);
   combinedPtEstimate = pe./eb;

   plotName = [plotDir '/sigma.png'];
   figure;
   loglog(ff,eb,'k--');
   print('-dpng',plotName);
   close;

   plotName = [plotDir '/absy.png'];
   figure;
   loglog(ff,abs(Y_J_par_1),'k--');
   hold on
   loglog(ff,abs(combinedPtEstimate),'b--');
   loglog(ff,powerlaw(ff,-11,2/3),'r');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = combinedPtEstimate;
   sigma_data = combinedErrorBar;

elseif strcmp(dataSet,'ETideal')
   omegafile = '/home/mcoughlin/Stochastic/MDC/ET/MDC2/omega.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 150;

   if strcmp(analysisType,'powerlaw')
      fmin = 10;
      fmax = 150;
   end

   plotName = [plotDir '/bestfit.png'];
   figure;
   loglog(ff,omega_data,'k--');
   hold on
   loglog(ff,powerlaw(ff,-10.7,2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   plotName = [plotDir '/bestfit_DNS.png'];
   figure;
   loglog(ff,omega_data,'k--');
   hold on
   loglog(ff,DNS(ff,1.4,-4.9),'b');
   hold off
   print('-dpng',plotName);
   close;

elseif strcmp(dataSet,'ETideal_noDetect')
   omegafile = '/home/mcoughlin/Stochastic/MDC/ET/MDC2_noDetect/omega.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   sigma_data = ones(size(omega_data));
   fmin = 10;
   fmax = 150;

   if strcmp(analysisType,'powerlaw')
      fmin = 10;
      fmax = 150;
   end

   plotName = [plotDir '/bestfit.png'];
   figure;
   loglog(ff,omega_data,'k--');
   hold on
   loglog(ff,powerlaw(ff,-12.9,2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   plotName = [plotDir '/bestfit_DNS.png'];
   figure;
   loglog(ff,omega_data,'k--');
   hold on
   loglog(ff,DNS(ff,1.4,-7.1),'b');
   hold off
   print('-dpng',plotName);
   close;

elseif strcmp(dataSet,'ThirtyPlusThirtySEOBNRv2Residual');
   postDir = '/home/mcoughlin/Stochastic/MDC/O1/ThirtyPlusThirtySEOBNRv2Residual/postprocessing/output/H1L1/10-100/0.666667/0.250000/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 40; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   Y_J_par = Y_J_par/0.7^2;
   sigma_J_par = sigma_J_par/0.7^2;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;

   ff_true = ff;
   omega_true = omega_data;
   sigma_true = sigma_data;

elseif strcmp(dataSet,'ThirtyPlusThirtySEOBNRv2');
   postDir = '/home/mcoughlin/Stochastic/MDC/O1/ThirtyPlusThirtySEOBNRv2/postprocessing/output/H1L1/10-100/0.666667/0.250000/';

   ptEstIntFile = [postDir '/H1L1_ptEstIntegrand.dat'];
   sensIntFile = [postDir '/H1L1_sensIntegrand.dat'];

   data1 = load(ptEstIntFile); data2 = load(sensIntFile);

   ff = data1(:,2);
   deltaF = ff(2)-ff(1);
   sigma_J = 1./sqrt(data2(:,3)*deltaF);
   Y_J = 2*deltaF*sum(data2(:,3)*deltaF)*data1(:,3)./(data2(:,3)*deltaF);

   % cross check the values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   var_J = sigma_J.^2;
   cut = isnan(Y_J);
   Y_J(cut) = 0;

   fRef = 40; alphaExp = 2/3;
   Y_J_par = Y_J.*(ff/fRef).^alphaExp;
   sigma_J_par = sigma_J.*(ff/fRef).^alphaExp;

   Y_J_par = Y_J_par/0.7^2;
   sigma_J_par = sigma_J_par/0.7^2;

   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   gamma = overlapreductionfunction(ff,det1,det2);

   plotName = [plotDir '/absy.png'];
   figure
   loglog(ff,abs(Y_J_par),'k--');
   hold on
   loglog(ff,abs(Y_J_par).*abs(gamma),'r.-');
   loglog(ff,powerlaw(ff,log10(1e-9),2/3),'b');
   hold off
   print('-dpng',plotName);
   close;

   omega_data = Y_J_par;
   sigma_data = sigma_J_par;

   ff_true = ff;
   omega_true = omega_data;
   sigma_true = sigma_data;

elseif strcmp(dataSet,'ThirtyPlusThirtyIdeal')
   omegafile = '/home/stochastic/MDC/O1_MDC/waveformTest/TestSet_30_EOBNR/omega.txt';
   omega = load(omegafile);
   ff = omega(:,1);
   omega_data = omega(:,2);
   sigma_data = 1e-8 * ones(size(omega_data));
   fmin = 10;
   fmax = 100;

   indexes = find(isnan(omega_data));
   omega_data(indexes) = 0;
   sigma_data(indexes) = 1e100;

   if strcmp(analysisType,'powerlaw')
      fmin = 10;
      fmax = 100;
   end

   ff_true = ff;
   omega_true = omega_data;
   sigma_true = sigma_data;

elseif strcmp(dataSet,'inj')

   ff = 10:256;
   if strcmp(analysisType,'DNS')
      M = 1.4; lam = -3.5;
      omega_data = DNS(ff,M,lam);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   elseif strcmp(analysisType,'NSBH')
      M = 3.0; lam = -3.5;
      omega_data = NSBH(ff,M,lam);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   elseif strcmp(analysisType,'DBH')
      M = 7.0; lam = -3.5;
      omega_data = DBH(ff,M,lam);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   elseif strcmp(analysisType,'sunva')
      alpha = 0.5; beta = 1.5;
      omega_data = sunva(ff,alpha,beta);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   elseif strcmp(analysisType,'MAG')
      B = 15; epsilon = -4;
      P0 = 0.002; lambda = -3;
      omega_data = MAG(ff,B,epsilon,P0,lambda);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   elseif strcmp(analysisType,'NSTurb');
      dOm = 0; N = -3;
      v = 0; M_star = 1.4;
      R_star = 15;
      omega_data = NSTurb(ff,dOm,N,v,M_star,R_star);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   elseif strcmp(analysisType,'PBB');
      mu = 1.5; F1 = 10;
      Fs = 2;
      omega_data = PBB(ff,mu,F1,Fs);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   elseif strcmp(analysisType,'CS');
      P = -2; epsilon = -10;
      Gmu = -8;
      omega_data = CS(ff,P,epsilon,Gmu);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   elseif strcmp(analysisType,'rmode');
      P0 = 1; lambda = -5;
      omega_data = rmode(ff,P0,lambda);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   elseif strcmp(analysisType,'barmode');
      P0 = 1; lambda = -5;
      omega_data = barmode(ff,P0,lambda);
      sigma_data = ones(size(omega_data));
      fmin = 10;
      fmax = 400;
   end
end

omega_data(isnan(omega_data)) = 0;
sigma_data(isnan(sigma_data)) = 0;

