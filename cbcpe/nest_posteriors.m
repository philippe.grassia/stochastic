function posteriors(post_samples, wp, parnames, plotName)

% function posteriors(post_samples, wp)
%
% This function displays joint and/or marginalized posterior 
% distributions, calculates summary statistics, etc.
%
% post_samples is an Nx(npars+2) array where N is the number of 
% posterior samples, npars is the number of parameters, and the last two 
% columns in this array contain the values of logL and logPosterior 
% (=prior weighted likelihood/evidence).
% 
% wp is a vector containing the parameter(s) for which you want to create
% the posterior e.g., wp=1 will provide a posterior only on the 1st
% parameter; wp=[1 3] will provide a 2D posterior on the 1st and 3rd
% parameters. For the moment wp can only contain a maximum of two params.
%
% parnames is a cell array of string names corresponding to the
% parameters specified by wp.  Eg., parnames = {'b', 'slope'}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
npars = size(post_samples,2)-2;
lwp = length(wp);
lparnames = length(parnames);
nbins = 50; % number of bins for historgram plots

if lwp == 1
    % calculate mean and std deviation
    par_mean = mean(post_samples(:,wp));
    par_std = std(post_samples(:,wp));
    fprintf('Parameter %s: Mean = %f, stddev = %f\n', ...
            parnames{1}, par_mean, par_std);

    samples_sort = sort(post_samples(:,wp),'descend');
    samples_width = samples_sort(1:end-1) - samples_sort(2:end);
    if wp == 1
       binWidth = median(samples_width) * 20;
       bins = par_mean - 2*par_std : binWidth : par_mean + 2*par_std;
    elseif wp == 2
       binWidth = median(samples_width) * 20;
       bins = par_mean - 2*par_std : binWidth : par_mean + 2*par_std;
    else
       binWidth = median(samples_width) * 50;
       bins = par_mean - 2*par_std : binWidth : par_mean + 2*par_std;
    end
    %n = hist(post_samples(:,wp), bins);
    [n,bins] = hist(post_samples(:,wp),20);
    n = n/sum(n);

    % make histogram plot
    figure()

    plot(bins,n);
    if strcmp(parnames{1},'M')
       xlabel('M [solar masses]','fontsize',12);
    elseif strcmp(parnames{1},'lam')
       xlabel('lam','fontsize',12);
    else
       xlabel(parnames{1},'fontsize',12);
    end
    hold on
    if strcmp(parnames{1},'M')
       %plot([1.2 1.2],[0 max(n)],'r--');
       %xlim([1.1 1.3])
    elseif strcmp(parnames{1},'lam')
       %plot([9.5e-5 9.5e-5],[0 max(n)],'r--');
       %xlim([5.5e-5 5.5e-4])
    end
    hold off
    xlabel(parnames{1},'fontsize',12);
    ylabel('PDF');

elseif lwp == 2
    % make 2-d histogram plot
    p1 = wp(1);
    p2 = wp(2);
    edges1 = linspace(min(post_samples(:,p1)), max(post_samples(:,p1)), nbins);
    edges2 = linspace(min(post_samples(:,p2)), max(post_samples(:,p2)), nbins);
    histmat = hist2(post_samples(:,p1), post_samples(:,p2), edges1, edges2);

    figure()
    imagesc(edges1, edges2, transpose(histmat));
    set(gca,'YDir','normal')
    xlabel(parnames{1},'fontsize',12);
    ylabel(parnames{2},'fontsize',12);

end

pretty;
print('-dpng', plotName);
print('-depsc2', plotName);
close;

return

