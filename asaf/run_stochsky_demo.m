function run_stochsky_demo
% function run_stochsky_demo

% load map with complex snr.   key fields include:
% q.snr = complex valued snr
% q.sigma = measure of detector sensitivity
% q.map.npixels = normalisation factor depends on coarse-graining effect
% q.map.segstarttime = segment start times
% q.map.f = frequencies
% others may be relevant too
q = load('demo_data.mat');

% default parameters
[params, det1, det2] = fast_params(q.map);
params = paramCheck(params);

% F parameter does not matter for asaf
params.stochtrack.F = 1;

% finish processing steps normally carried out by clustermap
q.map.snr = q.map.y./q.map.sigma;
% scale sigma by npixels in order to get reasonable SNR values
q.map.sigma = q.map.sigma / sqrt(q.map.npixels);

% run stochtrack
tic;
%params.stochtrack.T = 100; % 3s to run
%params.stochtrack.T = 2000; % 32s to run
params.stochtrack.T = 1; % overridden anyway
params.stochtrack.T = 1; % overridden anyway
%max_cluster = asaf(q.map, params);
max_cluster = asaf_v2(q.map, params);
toc;
stoch_out.max_SNR = max_cluster.snr_gamma;
stoch_out.cluster = max_cluster;

% best fit direction
fprintf('best fit (ra,dec)=(%1.1f, %1.1f)\n', max_cluster.ra, max_cluster.dec);

% save output mat file (takes a while)
save('run_stochsky_demo.mat');

% make a special plot
rmap = sqrt(315)*stoch_out.cluster.reconMax;
map.xvals = (q.map.segstarttime - q.map.segstarttime(1))/3600;
map.yvals = q.map.f;
printmap(rmap, map.xvals, map.yvals, 't (hr)', 'f (Hz)', 'SNR', [-5, 5]);
set(gca,'XTick', 0:6:48);
print('-dpng', [params.plotdir '/rmap0_sidereal']);
print('-depsc2', [params.plotdir '/rmap0_sidereal']);

return
