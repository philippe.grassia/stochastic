function plot_data
% function plot_data

load('demo_data.mat');

  printmap(snr0, map.xvals/3600, map.yvals, xlabel_text, ylabel_text, ...
    'SNR', [-5 5]);
  set(gca,'XTick', 0:6:48);
  print('-dpng', 'plot_data');
  print('-depsc2', 'plot_data');

return
