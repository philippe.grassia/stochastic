Documentation by eric.thrane@ligo.org
18 May, 2015

This directory contains demonstration code for an all-sky, all-frequency (ASAF)
radiometer search.  This documentation will eventually be superseeded as the
code is developed.  Note, that you need to use addpath to point to the stamp2
repository to use code there too:

  -bash-4.1$ svn co https://ligo-vcs.phys.uwm.edu/svn/matapps/packages/stochastic/trunk/stamp2
  >> addpath(genpath('/home/ethrane/stamp2/src'));
  >> addpath(genpath('/home/ethrane/stamp2/input'));

The code is currently set to ru on an 8-core CPU, but this can be changed.

run_stochsky_demo.m = wrapper to analyse the data in demo_data.mat.  Run this!
It should take ~170s to complete.

Supporting code for run_stochsky_demo.m
  demo_data.mat = example data file containing folded spectrogram data

  fast_params.m = defines parameters

  asaf_v2.m = based on stochtrack; carries out all-sky, all-frequency 
    calculation using healpix.

  HealpixLib/ = Healpix code from web

  run_stochsky_demo.mat = output file

  img/rmap0_sidereal.png = recovered signal

To visualise the raw data, run
  plot_data.m
