% use Hanford-Livingston pair
ifo1 L1
ifo2 L1

% sample rates to use
sampleRate 256

% frame duration (s)
frameDuration 2048

% strain channel names
ASQchannel1 STRAIN
ASQchannel2 MAGNETOMETER

ShiftTime1 0
ShiftTime2 0

% antenna factors
fixAntennaFactors false
bestAntennaFactors false

% output file location
framedir /home/mcoughlin/schumann/frames/noise_schumann_cbc/frames/L1/
framename L-NOISE_SCHUMANN_CBC

% plots
doPlots true
plotdir /home/mcoughlin/schumann/frames/noise_schumann_cbc/plots/L1/

% noise simulation
doDetectorNoiseSim true
DetectorNoiseFile1 /home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd_10Hz.txt
DetectorNoiseFile2 /home/mcoughlin/matapps/packages/stochastic/trunk/schumann/input/magnetometer.txt

% schumann
doSchumann true
schumannAmp1 1e-20
schumannAmp2 100

% cbc
doCBC true
%cbcFile /home/mcoughlin/matapps/packages/stochastic/trunk/schumann/mdc/waveforms/bns-a.txt
cbcFile /home/mcoughlin/matapps/packages/stochastic/trunk/schumann/cbc/waveforms/distance_100.0_mass1_1.4_mass2_1.4_spin2z_0.0_spin1z_0.0_inclination_0.0/waveform.txt
stamp.alpha 10

% randomseed
randomseed 1

