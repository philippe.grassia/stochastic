% use Hanford-Livingston pair
ifo1 L1
ifo2 L1

% sample rates to use
sampleRate 256

% frame duration (s)
frameDuration 2048

% strain channel names
ASQchannel1 STRAIN
ASQchannel2 MAGNETOMETER

ShiftTime1 0
ShiftTime2 0

% antenna factors
fixAntennaFactors false
bestAntennaFactors false

% output file location
framedir /home/mcoughlin/schumann/frames/noise_cbc/frames/L1/
framename L-NOISE_CBC

% plots
doPlots true
plotdir /home/mcoughlin/schumann/frames/noise_cbc/plots/L1/

% noise simulation
doDetectorNoiseSim true
DetectorNoiseFile1 /home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd_10Hz.txt
DetectorNoiseFile2 /home/mcoughlin/matapps/packages/stochastic/trunk/schumann/input/magnetometer.txt

% schumann
doSchumann false
schumannAmp1 1e-20
schumannAmp2 100

% cbc
doCBC true
cbcFile /home/mcoughlin/matapps/packages/stochastic/trunk/schumann/mdc/waveforms/bns-a.txt
stamp.alpha 1

% randomseed
randomseed 1


