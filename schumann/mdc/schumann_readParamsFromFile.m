function params = readParamsFromFile(paramsFile)
%
%  readParamsFromFile --- read in search parameters from a file
%
%  readParamsFromFile(paramsFile) reads in search parameters from a
%  file, returning the parameters in a structure.
%
%  Assumes parameters are given by name/value pair.
%
%  Routine written by Joseph D. Romano, John T. Whelan, Vuk Mandic.
%  Contact Joseph.Romano@astro.cf.ac.uk, john.whelan@ligo.org and/or
%  vmandic@ligo.caltech.edu.  Edited by Eric Thrane and Shivaraj Kandhasamy for
%  STAMP
%
%  Note: this file is organized into different sections:
%    * backward compatibility
%    * things for stochastic.m and STAMP
%    * things for stochastic.m only
%    * things for STAMP only
%  If you add additional parameters, please add them to the appropriate section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% seed random number generator for STAMP injection iota/psi
rand('state',sum(clock*1e6));

%% read in name/value pairs
[names,values] = ...
  textread(paramsFile, '%s %s\n', -1, 'commentstyle', 'matlab');

%% check that number of names and values are equal
if length(names)~=length(values)
  error('invalid parameter file');
end

%% loop over parameter names, assigning values to structure
for ii=1:length(names)

  switch names{ii}
    %--------------------------------------------------------------------------
    % for stochastic.m backward compatibility
    %--------------------------------------------------------------------------
    case 'ASQchannel'
      params.ASQchannel1 = values{ii};
      params.ASQchannel2 = values{ii};

    case 'framePath'
      params.framePath1 = values{ii};
      params.framePath2 = values{ii};

    %--------------------------------------------------------------------------
    % for stochastic.m and STAMP
    %--------------------------------------------------------------------------
    case 'doShift1'
      params.doShift1 = str2num(values{ii});

    case 'doShift2'
      params.doShift2 = str2num(values{ii});

    case 'ifo1'
      params.ifo1 = values{ii};

    case 'ifo2'
      params.ifo2 = values{ii};

    case 'ASQchannel1'
      params.ASQchannel1 = values{ii};

    case 'ASQchannel2'
      params.ASQchannel2 = values{ii};

    case 'frameType1'
      params.frameType1 = values{ii};

    case 'frameType2'
      params.frameType2 = values{ii};

    case 'framePath1'
      params.framePath1 = values{ii};

    case 'framePath2'
      params.framePath2 = values{ii};

    case 'frameDuration'
      params.frameDuration = str2num(values{ii});

    % used in preproc for STAMP studies
    case 'doDetectorNoiseSim'
      params.doDetectorNoiseSim = str2num(values{ii});

    case 'DetectorNoiseFile1'
      params.DetectorNoiseFile1 = values{ii};
  
    case 'DetectorNoiseFile2'
      params.DetectorNoiseFile2 = values{ii};

    case 'sampleRate'
      params.sampleRate = str2num(values{ii});

    case 'framedir'
      params.framedir = values{ii};

    case 'framename'
      params.framename = values{ii};

    case 'doPlots'
      params.doPlots = str2num(values{ii});

    case 'plotdir'
      params.plotdir = values{ii};

    case 'ShiftTime1'
      params.ShiftTime1 = str2num(values{ii});

    case 'ShiftTime2'
      params.ShiftTime2 = str2num(values{ii});

    case 'fixAntennaFactors'
      params.fixAntennaFactors = str2num(values{ii});

    case 'bestAntennaFactors'
      params.bestAntennaFactors = str2num(values{ii});

    case 'stamp.alpha'
      params.pass.stamp.alpha = str2num(values{ii});

    case 'doSchumann'
      params.doSchumann = str2num(values{ii});

    case 'schumannAmp1'
      params.schumannAmp1 = str2num(values{ii});

    case 'schumannAmp2'
      params.schumannAmp2 = str2num(values{ii});

    case 'doCBC'
      params.doCBC = str2num(values{ii});

    case 'cbcFile'
      params.cbcFile = values{ii};

    case 'randomseed'
      params.randomseed = str2num(values{ii});

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Analysis variables

    case 'path'
      params.path = values{ii};

    end % switch

end %% loop over parameter names

return
