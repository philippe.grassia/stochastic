function [t,hp,hc] = schumann_sinegaussian(tau,f0,fdot,hrss,fs);

 dt = 1/fs;
 endt=tau;
 T = endt;     
       t=0:dt:endt;
       t = t(1:end-1);
       %f = f0 + fdot*t;
       fwidth = f0*0.1;

       mc1.transfer(:,1) = 0:(1/fs):(fs/2);
       mc1.transfer(:,2) = zeros(size(mc1.transfer(:,1)));

       transfer(1,1) = f0-(1/2)*fwidth;
       transfer(2,1) = f0;
       transfer(3,1) = f0+(1/2)*fwidth;

       transfer(1,2) = 1e-5;
       transfer(2,2) = 1;
       transfer(3,2) = 1e-5;

       mc1.transfer(:,2) = interp1(transfer(:,1),transfer(:,2),mc1.transfer(:,1),'cubic',0);

       cos_portion = schumann_create_noise(fs,tau,mc1);
 
        % ---- Required parameters.
        T0 = tau/2;

        % ---- Optional parameters.
        alpha = 0;
        delta = 0;
        iota = 0;

        Q = 0.23;
        % ---- Waveform.
        %h = 2^0.5*hrss*exp(-((t-T0).^2)./(Q*tau).^2) .* cos(2*pi*f.*t);
        h = 2^0.5*hrss*exp(-((t-T0).^2)./(Q*tau).^2) .* cos_portion;

        hp = 1/2*(1+(cos(iota))^2) * real(h);
        hc = cos(iota) * imag(h);

   taper = linspace(0,1-dt,fs);
   mask = ones(size(t));
   %mask(1:fs)=sin(pi/2.*taper).^2;
   %mask(end-fs+1:end) = fliplr(mask(1:fs));
   
    hp = hp .* mask;
    hc = hc .* mask;


