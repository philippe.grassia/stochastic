function [vector1,vector2] = schumann_create_cbc(params,sampleRate,dataDuration,startGPS);

num_cbcs = 1;
fs = sampleRate;

amps = 1e20 * linspace(1.0,10,num_cbcs);

endGPS = startGPS + dataDuration;

t = startGPS:(1/sampleRate):endGPS;
t = t(1:end-1);
vector1 = zeros(length(t),1); vector2 = zeros(length(t),1);

stamp_inj.file = params.cbcFile;
params.pass.stamp.inj_type = 'cbc';
[hp_inj, hc_inj] = load_injection(params, stamp_inj, fs);
t_inj = (0:length(hp_inj)-1)*(1/fs);
inj_dur = t_inj(end);
hp_inj = hp_inj * params.pass.stamp.alpha;
hc_inj = hc_inj * params.pass.stamp.alpha;

for i = 1:num_cbcs

   t_inj = t_inj;
   hp_inj = hp_inj;
   hc_inj = hc_inj;

   delayGPS = floor(rand*(dataDuration-inj_dur));
   params.pass.stamp.startGPS = startGPS + delayGPS;

   params.pass.stamp.hp = hp_inj; params.pass.stamp.hx = hc_inj;
   params.pass.stamp.t = (0:length(params.pass.stamp.hp)-1)/sampleRate + ...
        params.pass.stamp.startGPS;

   params.ShiftTime1 = round(params.ShiftTime1 * sampleRate)/sampleRate;
   params.ShiftTime2 = round(params.ShiftTime2 * sampleRate)/sampleRate;
   params.pass.stamp.t1 =  params.pass.stamp.t - params.ShiftTime1;
   params.pass.stamp.t2 =  params.pass.stamp.t - params.ShiftTime2;

   % calculate the duration of the injection
   params.pass.stamp.dur = length(params.pass.stamp.hp)/sampleRate;

   params.pass.stamp.ra = rand*(24);
   params.pass.stamp.decl = rand*(360);

   params = getPointSourceData_IM(params,startGPS,sampleRate);

   [tmp, idx_h1, idx_fr] = intersect(params.pass.stamp.t1, t);
   % add the injection
   vector1(idx_fr) = vector1(idx_fr) + params.pass.stamp.h1(idx_h1);
   % find the overlapping time indices for injection and frame data
   [tmp, idx_h2, idx_fr] = intersect(params.pass.stamp.t2, t);
   % add the injection
   vector2(idx_fr) = vector2(idx_fr) + params.pass.stamp.h2(idx_h2);

end

