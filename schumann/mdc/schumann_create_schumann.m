function [vector1,vector2] = schumann_create_schumann(params,sampleRate,dataDuration,startGPS);

num_schumanns = 16;
tau = 2; fdot = 0; ampFull = 1; fs = sampleRate;
n = 1:4;
%n = 1:5;
%amps = 10.^linspace(0,-3,length(n));
amps = logspace(2,-1,length(n));

endGPS = startGPS + dataDuration;

t = startGPS:(1/sampleRate):endGPS;
t = t(1:end-1);
vector1 = zeros(length(t),1); vector2 = zeros(length(t),1);

freqs = 6.0 * sqrt(n.*(n+1));

for j = 1:num_schumanns

   t_inj = []; hp_inj = []; hc_inj = [];

   for i = 1:length(freqs)
      f0 = freqs(i);
      hrss = 1;
      [t_inj_temp,hp_inj_temp,hc_inj_temp] = schumann_sinegaussian(tau,f0,fdot,hrss,fs);
      if isempty(t_inj)
         t_inj = t_inj_temp';
         hp_inj = amps(i)*hp_inj_temp';
         hc_inj = amps(i)*hc_inj_temp';
      else
         hp_inj = hp_inj + amps(i)*hp_inj_temp';
         hc_inj = hc_inj + amps(i)*hc_inj_temp';
      end
   end

   delayGPS = floor(rand*(dataDuration-tau));
   params.pass.stamp.startGPS = startGPS + delayGPS;

   params.pass.stamp.hp = hp_inj; params.pass.stamp.hx = hc_inj;
   params.pass.stamp.t = (0:length(params.pass.stamp.hp)-1)/sampleRate + ...
        params.pass.stamp.startGPS;

   params.ShiftTime1 = round(params.ShiftTime1 * sampleRate)/sampleRate;
   params.ShiftTime2 = round(params.ShiftTime2 * sampleRate)/sampleRate;
   params.pass.stamp.t1 =  params.pass.stamp.t - params.ShiftTime1;
   params.pass.stamp.t2 =  params.pass.stamp.t - params.ShiftTime2;

   % calculate the duration of the injection
   params.pass.stamp.dur = length(params.pass.stamp.hp)/sampleRate;

   params.pass.stamp.ra = rand*(24);
   params.pass.stamp.decl = rand*(360);

   params = getPointSourceData_IM(params,startGPS,sampleRate);

   [tmp, idx_h1, idx_fr] = intersect(params.pass.stamp.t1, t);
   % add the injection
   vector1(idx_fr) = vector1(idx_fr) + params.pass.stamp.h1(idx_h1) * params.schumannAmp1 * amps(i);
   % find the overlapping time indices for injection and frame data
   [tmp, idx_h2, idx_fr] = intersect(params.pass.stamp.t2, t);
   % add the injection
   vector2(idx_fr) = vector2(idx_fr) + params.pass.stamp.h2(idx_h2) * params.schumannAmp2 * amps(i);

end

