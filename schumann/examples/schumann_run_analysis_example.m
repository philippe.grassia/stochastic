
doPSD = 1;
doCoherence = 1;
doWiener = 1;

startGPS = 900000000;
endGPS =   900003600;

%endGPS =   900086400;

paramsFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/schumann/input/schumann_mdc_params_analysis_noiseschumann.txt';

% Loads channel list
[station,samplef,calibration] = textread('/home/mcoughlin/matapps/packages/stochastic/trunk/schumann/input/schumann_station_list.txt','%s %f %f');
nch=length(station);

if doPSD
   type = 'psd';
   % Loop over channels placing each in its own job (runs in standard universe)
   for i=1:nch
      schumann_run(paramsFile,startGPS,endGPS,type,station{i})
   end
end

if doCoherence
   type = 'coherence';
   for i=1:nch
      for j=1:nch
         if i >= j
            continue
         end
         channel = sprintf('%s,%s',station{i},station{j});
         schumann_run(paramsFile,startGPS,endGPS,type,channel)
      end
   end
end

if doWiener
   type = 'psdwiener';
   channel = 'H1:STRAIN,H1:MAGNETOMETER';
   schumann_run(paramsFile,startGPS,endGPS,type,channel)
   channel = 'L1:STRAIN,L1:MAGNETOMETER';
   schumann_run(paramsFile,startGPS,endGPS,type,channel)
   type = 'coherencewiener';
   channel = 'H1:STRAIN,L1:STRAIN';
   schumann_run(paramsFile,startGPS,endGPS,type,channel)
end

