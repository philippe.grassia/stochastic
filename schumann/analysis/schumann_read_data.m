function [data,channel] = homestake_read_data(params,channel,startGPS,endGPS)

% function data = homestake_read_data(channel)
% Written by M. Coughlin (coughlim@carleton.edu)
%
% Load data from channels in structure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data = [];
which_channels = [];

frames = channel.frames;

for i=1:length(channel)

   frames = channel(i).frames;

   fprintf('Reading data for channel %s\n',channel(i).station);

   tt = []; samples = []; these_samples = [];
   for j=1:length(channel(i).frames)
      frame_split = regexp(strrep(channel(i).frames{j},'.','-'),'-','split');
      tt_frames(j) = str2num(frame_split{3});
      dur_frames(j) = str2num(frame_split{4});
      if endGPS < tt_frames(j) || startGPS > tt_frames(j) + dur_frames(j)
         continue
      end

      these_samples{j} = frextract(channel(i).frames{j},channel(i).station);
      if these_samples{j} == 0
         these_samples{j} = [];
      end
      if ~isempty(these_samples{j})
         these_tt{j} = (tt_frames(j) + (0:length(these_samples{j})-1)*(1/channel(i).samplef))';
      else
         these_tt{j} = [];
      end

      if ~isempty(these_tt{j})
         which_tt = find(these_tt{j} >= startGPS & these_tt{j} <= endGPS);
         these_tt{j} = these_tt{j}(which_tt);
         these_samples{j} = these_samples{j}(which_tt);
      end

      if mod(j,100)==0 | mod(j,length(frames))==0
         fprintf('%d/%d Frames Completed\n',j,length(frames));
      end
   end
   if iscell(these_samples)
      tt = cell2mat(these_tt(:));
      samples = cell2mat(these_samples(:));
   end
   if length(find(isnan(samples))) == length(samples)
      samples = [];
   end

   if ~isempty(samples) 

      samples=samples/channel(i).calibration;

      dataIndex = length(data) + 1;
      data(dataIndex).samples = samples;
      data(dataIndex).tt = tt;
      data(dataIndex).station = channel(i).station;

      which_channels = [which_channels i];

   end
   fprintf('Channel %s completed\n',channel(i).station);
   fprintf('\n');
end

channel = channel(which_channels);
