function plots = schumann_coh(params,channel1,channel2)
% function homestake_daily_coh(params,channel1,channel2)
% Given a Homestake params struct and two channel names, generates coherence
% plots for that day
% Routine written by Michael Coughlin.
% Modified: August 17, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

% Location of PSDs for each channel
psdMatLocation1 = [params.path '/Mat_Files/PSD/' channel1.station_underscore '/' num2str(params.startGPS) '-' num2str(params.endGPS) '.mat'];
psdMatLocation2 = [params.path '/Mat_Files/PSD/' channel2.station_underscore '/' num2str(params.startGPS) '-' num2str(params.endGPS) '.mat'];

% Run analysis if PSDs exist
if exist(psdMatLocation1) && exist(psdMatLocation2) 

   % Load channel spectra
   channel1.data = load(psdMatLocation1,'data'); channel2.data = load(psdMatLocation2,'data');

   which_spectra_1 = find(~isnan(sum(channel1.data.data.fftamp)));
   which_spectra_2 = find(~isnan(sum(channel2.data.data.fftamp))); 

   % Keep time vector for coincident data
   tt_1 = channel1.data.data.tt(which_spectra_1); tt_2 = channel1.data.data.tt(which_spectra_2);

   [tt,ia,ib] = intersect(tt_1,tt_2);

   if isempty(tt)
      return;
   end

   % Initialize vectors
   data.tt = tt;
   data.ff = channel1.data.data.ff;
   data.r = zeros(length(data.ff),1);

   data.fftamp = zeros(length(data.ff),length(data.tt));

   data.ff = channel1.data.data.ff;

   rho2 = logspace(-7,0,500);
   prho2 = (1-rho2).^(length(tt)-1);

   threshold = 0.001;
   [junk,threshold_index] = min(abs(prho2-threshold));

   prho2_threshold = prho2(threshold_index);

   vetoed = [];

   for f = 1:length(data.ff)
      % Calculate coherence
      index = 1:length(data.tt);
      a1 = channel1.data.data.fftamp(f,index);
      psd1 = mean(abs(a1).^2,2);
      a2 = channel2.data.data.fftamp(f,index);
      psd2 = mean(abs(a2).^2,2);
      csd12 = mean(a1.*conj(a2),2);
      
      data.r(f) = csd12./ mean(a2.*conj(a2));
      data.coherence(f) = abs(csd12)./sqrt(psd1.*psd2);

      if data.coherence(f)^2 < prho2_threshold
         continue
      end
      data.fftamp(f,index) = a1 - data.r(f) * a2;
      vetoed = [vetoed f];
   end

else
   return
end

% Wiener mat file location
wienerLocation = [params.path '/Mat_Files/PSDWiener/' channel1.station_underscore];
homestake_createpath(wienerLocation);

% Save PSD information in mat file
save([wienerLocation '/' num2str(params.startGPS) '-' num2str(params.endGPS)],'data','channel1','-v7.3');

if params.doPlots

   % Create plot location
   plotLocation = [params.path '/' channel1.station_underscore '/' num2str(params.startGPS) '-' num2str(params.endGPS)];
   homestake_createpath(plotLocation);

   % Coherence plot
   figure;
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   loglog(data.ff,abs(mean(channel1.data.data.fftamp,2)),'r','LineWidth',1);
   hold on
   loglog(data.ff,abs(mean(data.fftamp,2)),'b','LineWidth',1);
   hold off
   grid;
   xlabel('Frequency [Hz]');
   ylabel('FFT');
   legend({'Original','Residual'})
   grid
   set(gca,'Layer','top')
   xlim([params.fmin params.fmax])

   print('-dpng',[plotLocation '/psdwiener.png']);
   print('-depsc2',[plotLocation '/psdwiener.eps']);

   close;

   % Coherence plot
   figure;
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   loglog(data.ff,data.coherence,'b','LineWidth',1);
   hold on
   loglog(data.ff(vetoed),data.coherence(vetoed),'k*','LineWidth',1);
   hold off
   grid;
   xlabel('Frequency [Hz]');
   ylabel('\rho');
   grid
   set(gca,'Layer','top')
   xlim([params.fmin params.fmax])

   print('-dpng',[plotLocation '/rho.png']);
   print('-depsc2',[plotLocation '/rho.eps']);

   close;

   % Coherence plot
   figure;
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   semilogx(rho2,prho2,'b','LineWidth',1);
   grid;
   xlabel('\rho^2');
   ylabel('CDF(\rho^2)');
   grid
   set(gca,'Layer','top')

   print('-dpng',[plotLocation '/prho.png']);
   print('-depsc2',[plotLocation '/prho.eps']);

   close;

end 
