function schumann_coh_wiener(params,channel1,channel2)
% function homestake_daily_coh(params,channel1,channel2)
% Given a Homestake params struct and two channel names, generates coherence
% plots for that day
% Routine written by Michael Coughlin.
% Modified: August 17, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

% Location of PSDs for each channel
psdMatLocation1 = [params.path '/Mat_Files/PSDWiener/' channel1.station_underscore '/' num2str(params.startGPS) '-' num2str(params.endGPS) '.mat'];
psdMatLocation2 = [params.path '/Mat_Files/PSDWiener/' channel2.station_underscore '/' num2str(params.startGPS) '-' num2str(params.endGPS) '.mat'];

% Run analysis if PSDs exist
if exist(psdMatLocation1) && exist(psdMatLocation2)

   % Load channel spectra
   channel1.data = load(psdMatLocation1,'data'); channel2.data = load(psdMatLocation2,'data');

   which_spectra_1 = find(~isnan(sum(channel1.data.data.fftamp)));
   which_spectra_2 = find(~isnan(sum(channel2.data.data.fftamp))); 

   % Keep time vector for coincident data
   tt_1 = channel1.data.data.tt(which_spectra_1); tt_2 = channel1.data.data.tt(which_spectra_2);

   [tt,ia,ib] = intersect(tt_1,tt_2);

   if isempty(tt)
      return;
   end

   % Initialize vectors
   data.tt = tt;
   data.ff = channel1.data.data.ff;
   data.coherence = NaN*ones(length(data.ff),length(tt));

   data.ff = channel1.data.data.ff;

   data.coherence = zeros(size(data.ff));

   for f = 1:length(data.ff)
      % Calculate coherence
      index = 1:length(data.tt);
      a1 = channel1.data.data.fftamp(f,index);
      psd1 = mean(abs(a1).^2,2);
      a2 = channel2.data.data.fftamp(f,index);
      psd2 = mean(abs(a2).^2,2);
      csd12 = mean(a1.*conj(a2),2);
      data.coherence(f) = abs(csd12)./sqrt(psd1.*psd2);
   end
else
   return
end

% Coherence mat file location
cohLocation = [params.path '/Mat_Files/COHWiener/' channel1.station_underscore '_' channel2.station_underscore];
homestake_createpath(cohLocation);

% Save PSD information in mat file
save([cohLocation '/' num2str(params.startGPS) '-' num2str(params.endGPS)],'data','channel1','channel2','-v7.3');

if params.doPlots

   % Create plot location
   plotLocation = [params.path '/' channel1.station_underscore '_' channel2.station_underscore '/' num2str(params.startGPS) '-' num2str(params.endGPS)];
   homestake_createpath(plotLocation);

   % Coherence plot
   figure;
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   loglog(data.ff,data.coherence,'LineWidth',1);
   hold on
   loglog(data.ff,1/sqrt(length(tt)),'k--')
   hold off
   grid;
   xlabel('Frequency [Hz]');
   ylabel('Coherence');
   legend({'actual','theoretical'})
   grid
   set(gca,'Layer','top')
   axis([params.fmin params.fmax 0 1])

   print('-dpng',[plotLocation '/cohwiener.png']);
   print('-depsc2',[plotLocation '/cohwiener.eps']);

   close;

end 
