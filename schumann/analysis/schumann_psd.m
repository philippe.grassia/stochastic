function schumann_psd(params,channel)
% function homestake_daily_psd(params,channel)
% Given a Homestake params struct and channel name, generates PSD and diurnal
% plots for that day
% Routine written by Michael Coughlin.
% Modified: August 17, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

% PSD parameters
data.T = 16;
data.nfft = data.T*channel.samplef;
data.nbin = bitshift(data.nfft,-1);
data.window = nuttallwin(data.nfft);

gps = params.startGPS:data.T:params.endGPS;

check_init = 0;
% Loop over frames
for i = 1:length(gps)-1

   % Retrieve data for this frame
   [data_out,channelHold] = schumann_read_data(params,channel,gps(i),gps(i+1));

   % Change from counts into m/s
   samples=data_out.samples;
   % Calculate PSD for this frame
   [data_out, ff] = schumann_calculate_psd(samples,channel.samplef,data.window);

   % Populate array if PSD is non-zero
   if ~isempty(data_out)

      if check_init == 0
         data.ff = ff; data.spectra = NaN*ones(length(data.ff),length(gps)-1);
         data.fftamp = NaN*ones(length(data.ff),length(gps)-1);
         check_init = 1;
         data.tt = gps(1:end-1);
      end
      data.spectra(:,i) = sqrt(data_out.log_average); data.fftamp(:,i) = data_out.fftamp(1:data.nbin);

   end

   if mod(i,100) == 0
      fprintf('Completed %d/%d\n',i,length(gps-1));
   end

end

try data.spectra;
catch
   return;
end

if length(find(~isnan(data.spectra(1,:)))) < 2
   return;
end

f_min_index = find(data.ff==params.fmin);
fic = unique(floor(logspace(log10(f_min_index),log10(length(data.ff)),1000)));

% Binning parameters
nb = 500;
lowBin = log10(min(data.spectra(:)));
highBin = log10(max(data.spectra(:)));

range_binning = logspace(lowBin,highBin,nb);
data.range_binning = range_binning;

% Determine frequencies to keep from PSD
desired_frequencies = ff(fic);
for i=1:length(ff(fic))
   which_freq = find(min(abs(data.ff-ff(fic(i))))==abs(data.ff-ff(fic(i))));
   fic_new(i) = which_freq(1);
end   

fic = fic_new;
ff = data.ff(fic);

% Indexes of those frequencies
data.ff = ff; data.fic = fic;

% Reduce fft arrays to correct size
data.spectra = data.spectra(data.fic,:); data.fftamp = data.fftamp(data.fic,:);

% Determine which spectra to keep
which_spectra = 1:length(data.spectra(1,:));

% Calculate bin histogram of PSDs
data.spectral_variation = hist(data.spectra(:,which_spectra)',range_binning)';
np = sum(data.spectral_variation(1,:),2);

% Convert to percentiles
data.spectral_variation_norm = data.spectral_variation * 100 / np;

% Initialize arrays
data.spectral_variation_norm_1per = [];
data.spectral_variation_norm_10per = [];
data.spectral_variation_norm_50per = [];
data.spectral_variation_norm_90per = [];
data.spectral_variation_norm_99per = [];

for i = 1:length(data.ff)
   data.spectral_variation_norm_1per(i) = homestake_calculate_percentiles(data.spectral_variation_norm(i,:),data.range_binning,1);
   data.spectral_variation_norm_10per(i) = homestake_calculate_percentiles(data.spectral_variation_norm(i,:),data.range_binning,10);
   data.spectral_variation_norm_50per(i) = homestake_calculate_percentiles(data.spectral_variation_norm(i,:),data.range_binning,50);
   data.spectral_variation_norm_90per(i) = homestake_calculate_percentiles(data.spectral_variation_norm(i,:),data.range_binning,90);
   data.spectral_variation_norm_99per(i) = homestake_calculate_percentiles(data.spectral_variation_norm(i,:),data.range_binning,99);

end

psdLocation = [params.path '/Mat_Files/PSD/' channel.station_underscore];
homestake_createpath(psdLocation)

% Save PSD information in mat file
save([psdLocation '/' num2str(params.startGPS) '-' num2str(params.endGPS)],'data','channel','-v7.3');

if params.doPlots

   % Create plot location
   plotLocation = [params.path '/' channel.station_underscore '/' num2str(params.startGPS) '-' num2str(params.endGPS)];
   homestake_createpath(plotLocation);

   % PSD plot
   figure;
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   loglog(data.ff,data.spectral_variation_norm_10per,'LineWidth',1);
   hold all
   loglog(data.ff,data.spectral_variation_norm_50per,'LineWidth',1);
   loglog(data.ff,data.spectral_variation_norm_90per,'LineWidth',1);
   loglog(data.ff,mean(data.spectra,2),'LineWidth',1);
   hold off
   grid;
   xlabel('Frequency [Hz]');
   ylabel('ASD');
   hold on
   grid
   set(gca,'Layer','top')
   hold off
   legend('10','50','90','Mean')
   xlim([params.fmin params.fmax])

   print('-dpng',[plotLocation '/psd.png']);
   print('-depsc2',[plotLocation '/psd.eps']);

   close;

   % Binning parameters
   number_of_averages = 100;
   nb_new = floor(nb/number_of_averages);

   indexes_old = 1:length(fic);
   indexes = unique(indexes_old(round(logspace(0,log10(length(indexes_old)),100))));

   data.spectral_variation_norm = data.spectral_variation_norm(indexes,:);
   data.spectral_variation_norm_reduced = zeros(length(indexes),number_of_averages);

   for i=1:number_of_averages
      range(i) = range_binning((i-1) * nb_new + 1);
      for j = 1:length(indexes)
         data.spectral_variation_norm_reduced(j,i) = sum(data.spectral_variation_norm(j,(i-1) * nb_new + 1: i* nb_new));
      end
   end

   [X,Y] = meshgrid(data.ff(indexes),range);

   % Spectral variation plot
   data.spectral_variation_norm_reduced(data.spectral_variation_norm_reduced==0) = NaN;

   figure;
   set(gcf, 'PaperSize',[10 8])
   set(gcf, 'PaperPosition', [0 0 10 8])
   clf

   colormap(jet);
   pcolor(X,Y,data.spectral_variation_norm_reduced');

   caxis([0 1])
   set(gcf,'Renderer','zbuffer');

   shading interp
   set(gca,'xscale','log','XLim',[params.fmin params.fmax])
   set(gca,'yscale','log','YLim',[range(1) range(end)])
   set(gca,'clim',[0 5])
   xlabel('Frequency [Hz]')
   ylabel('ASD')
   grid
   set(gca,'Layer','top')

   hold on
   loglog(data.ff,data.spectral_variation_norm_10per,'w',data.ff,data.spectral_variation_norm_90per,'w',data.ff,data.spectral_variation_norm_50per,'w','LineWidth',2)
   hold off

   print('-dpng',[plotLocation '/specvar.png']);
   print('-depsc2',[plotLocation '/specvar.eps']);
   close;

   tt = (data.tt - data.tt(1)) / (60*60);

   [X,Y] = meshgrid(data.ff(indexes),tt);

   % Frequency-time plot
   figure;
   set(gcf, 'PaperSize',[10 8])
   set(gcf, 'PaperPosition', [0 0 10 8])
   clf

   hC = pcolor(X,Y,log10(data.spectra(indexes,:))');
   set(gcf,'Renderer','zbuffer');
   colormap(jet)
   shading interp

   t = colorbar('peer',gca);
   set(get(t,'ylabel'),'String','log10(Spectra)');
   set(gca,'xscale','log','XLim',[params.fmin params.fmax])
   set(hC,'LineStyle','none');
   grid
   %set(gca,'clim',[-30 -10])
   xlabel('Frequency [Hz]')
   ylabel('Time [Hours]');

   print('-dpng',[plotLocation '/tf.png']);
   print('-depsc2',[plotLocation '/tf.eps']);
   close;

   schumann_psd_page(params,channel);

end 
