function plots = schumann_coh_page(params,channel1,channel2)
% function homestake_daily_coh_page(params,channel)
% Given a Homestake params struct and two channel names, generates a html page
% linking to coherence plots for that day
% Routine written by Michael Coughlin.
% Modified: August 17, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

webpageLocation = [params.path '/' channel1.station_underscore '_' channel2.station_underscore];
homestake_createpath(webpageLocation);

% Open HTML page for writing
fid=fopen([webpageLocation '/coh.html'],'w+');

% HTML Header
fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
fprintf(fid,'\n');
fprintf(fid,'<html>\n');
fprintf(fid,'\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<title>Coherence Plots for %s and %s</title>\n',channel1.station,channel2.station);
fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../main.css">\n');
fprintf(fid,'<script src="../../sorttable.js"></script>\n');
fprintf(fid,'</head>\n');

% Coherence plots block
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Coherence Plots for %s and %s</span></big></big></big></big><br>\n',channel1.station,channel2.station);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

% Links to coherence plots
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./coh.png"><img alt="" src="./coh.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./cohvar.png"><img alt="" src="./cohvar.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./cohtf.png"><img alt="" src="./cohtf.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
%fprintf(fid,'<td style="vertical-align: top;">\n');
%fprintf(fid,'<a href="../psd/%s_all_tf_normal.jpeg"><img alt="" src="../psd/%s_all_tf_normal.jpeg" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',channel_name,channel_name);
%fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

% Close html file
fprintf(fid,'</html>\n');
fclose(fid);
