function schumann_run(paramsFile,startGPS,endGPS,type,channel)
 
% Turn warnings off
warning off all;

if isstr(startGPS)
   startGPS = str2num(startGPS);
end

if isstr(endGPS)
   endGPS = str2num(endGPS);
end

% For condor purposes
if startGPS == 0
   return
else

   params = schumann_readParamsFromFile(paramsFile);

   params.startGPS = startGPS; params.endGPS = endGPS;
   params.filterOrder = 1000;
   params.channel = channel;
   params.type = type;

   clear channel;  
 
   % Loads channel list
   [station,samplef,calibration] = textread('/home/mcoughlin/matapps/packages/stochastic/trunk/schumann/input/schumann_station_list.txt','%s %f %f');
   nch=length(station);
   
   for c=1:nch
      channel(c).station = station{c};
      channel(c).station_underscore = strrep(channel(c).station,':','_');
      channel(c).ifo = station{c}(1:2);
      channel(c).samplef = samplef(c);
      channel(c).calibration = calibration(c);
      frames = dir([params.framedir '/' channel(c).ifo '/*.gwf']); 
      channel(c).frames = {};
      for i = 1:length(frames)
         channel(c).frames{i} = [params.framedir '/' channel(c).ifo '/' frames(i).name];
      end 
   end
  
   % Output path for run
   homestake_createpath(params.path);
   
   % Run parameters
   params.fmin = 5;
   params.fmax = 100;

   if strcmp(params.type,'psd')
      % PSD run
      whichChannel = find(strcmpi(params.channel,{channel.station}));
      schumann_psd(params,channel(whichChannel));
   elseif strcmp(params.type,'psdcombine')
      % PSD run
      whichChannel = find(strcmpi(params.channel,{channel.station}));
      schumann_psd_combine(params,channel(whichChannel));
   elseif strcmp(params.type,'coherence')
      channelSplit = regexp(params.channel,',','split');
      whichChannel1 = find(strcmpi(channelSplit{1},{channel.station}));
      whichChannel2 = find(strcmpi(channelSplit{2},{channel.station}));
      schumann_coh(params,channel(whichChannel1),channel(whichChannel2));
   elseif strcmp(params.type,'fir') 
      schumann_fir(params,channel);
   elseif strcmp(params.type,'psdwiener')
      channelSplit = regexp(params.channel,',','split');
      whichChannel1 = find(strcmpi(channelSplit{1},{channel.station}));
      whichChannel2 = find(strcmpi(channelSplit{2},{channel.station}));
      schumann_psd_wiener(params,channel(whichChannel1),channel(whichChannel2));
   elseif strcmp(params.type,'coherencewiener')
      channelSplit = regexp(params.channel,',','split');
      whichChannel1 = find(strcmpi(channelSplit{1},{channel.station}));
      whichChannel2 = find(strcmpi(channelSplit{2},{channel.station}));
      schumann_coh_wiener(params,channel(whichChannel1),channel(whichChannel2));
   end
end   
   
