function schumann_psd_combine(params,channel)
% function homestake_daily_psd(params,channel)
% Given a Homestake params struct and channel name, generates PSD and diurnal
% plots for that day
% Routine written by Michael Coughlin.
% Modified: August 17, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdLocation = [params.path '/Mat_Files/PSD/' channel.station_underscore];
matFiles = dir([psdLocation '/*.mat']);

data.tt = []; data.ff = []; data.spectra = []; data.fftamp = [];

check_init = 0;
% Loop over frames
for i = 1:length(matFiles)

   data_out = load([psdLocation '/' matFiles(i).name]);

   % Populate array if PSD is non-zero
   if ~isempty(data_out)

      if check_init == 0
         data.ff = data_out.data.ff;
      end
      data.tt = [data.tt data_out.data.tt];
      data.spectra = [data.spectra data_out.data.spectra];
      data.fftamp = [data.fftamp data_out.data.fftamp];
   end

   if mod(i,100) == 0
      fprintf('Completed %d/%d\n',i,length(matFiles));
   end

end

psdLocation = [params.path '/Mat_Files/PSD'];
homestake_createpath(psdLocation)

% Save PSD information in mat file
save([psdLocation '/' channel.station_underscore],'data','channel','-v7.3');

if params.doPlots

   % Create plot location
   plotLocation = [params.path '/' channel.station_underscore];
   homestake_createpath(plotLocation);

   % PSD plot
   figure;
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   loglog(data.ff,mean(data.spectra,2),'LineWidth',1);
   grid;
   xlabel('Frequency [Hz]');
   ylabel('Spectrum [(m/s)/\surd Hz]');
   hold on
   grid
   set(gca,'Layer','top')
   hold off
   xlim([params.fmin params.fmax]);

   print('-dpng',[plotLocation '/psd.png']);
   print('-depsc2',[plotLocation '/psd.eps']);

   close;
end
