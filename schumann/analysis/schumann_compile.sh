#!/bin/bash

source ${HOME}/matlab_script_2012a.sh

mkdir -p ${HOME}/Stochastic/Schumann/bin/
cd ${HOME}/Stochastic/Schumann/bin/
rm -rf *
cp ${HOME}/matapps/packages/stochastic/trunk/schumann/schumann_startup.m .
/ldcg/matlab_r2012a/bin/matlab <<EOF
 schumann_startup
 mcc -C -R -nojvm -R -nodisplay -m schumann_run
EOF
./schumann_run 0 0 0 0 0 0

cd ${HOME}/matapps/packages/stochastic/trunk/schumann/

