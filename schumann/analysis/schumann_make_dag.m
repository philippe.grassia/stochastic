function schumann_make_dag
% function homestake_daily_make_dag(homestakeParamsFile,year,doy)
% Given a homestake parameter file, year and doy, creates condor .dag and .sub
% files for running the daily homestake pipeline.
% Routine written by Michael Coughlin.
% Modified: August 17, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Output path   
params.path = '/home/mcoughlin/Stochastic/Schumann/analysis';
system(['rm -r ' params.path '/condor']); 
homestake_createpath([params.path '/condor/logs']);

job_number=0;

% Loads channel list
[station,samplef,calibration] = textread('/home/mcoughlin/matapps/packages/stochastic/trunk/schumann/schumann_station_list.txt','%s %f %f');
nch=length(station);

%frame_types = {'nosignal','signal'};
frame_types = {'signal'};
gps = 800000000:8000:800204800;

% Open dag file for writing
fid=fopen([params.path '/condor/schumann.dag'],'w+');

% Loop over channels placing each in its own job (runs in standard universe)
for i=1:nch
   for j = 1:length(gps)-1
      for k = 1:length(frame_types)
         job_number = job_number + 1;
         params.doPlots = 0;
         fprintf(fid,'JOB %d %s/condor/schumann.sub\n',job_number,params.path)
         fprintf(fid,'RETRY %d 3\n',job_number)
         fprintf(fid,'VARS %d jobNumber="%d" startGPS="%d" endGPS="%d" type="%s" frame_type="%s" channel="%s" doPlots="%d"\n',job_number,job_number,gps(j),gps(j+1),'psd',frame_types{k},station{i},params.doPlots); 
         fprintf(fid,'\n\n');
      end
   end
end

job_number_middle = job_number;

% Loop over channels placing each in its own job (runs in standard universe)
for i=1:nch
   for k = 1:length(frame_types)
      job_number = job_number + 1;
      params.doPlots = 1;
      fprintf(fid,'JOB %d %s/condor/schumann.sub\n',job_number,params.path)
      fprintf(fid,'RETRY %d 3\n',job_number)
      fprintf(fid,'VARS %d jobNumber="%d" startGPS="%d" endGPS="%d" type="%s" frame_type="%s" channel="%s" doPlots="%d"\n',job_number,job_number,gps(j),gps(j+1),'psdcombine',frame_types{k},station{i},params.doPlots);
      fprintf(fid,'\n\n');
   end
end

job_number_middle_2 = job_number;

% Loop over channels placing each in its own job (runs in standard universe)
for i=1:nch
   for j = 1:nch
      for k = 1:length(frame_types)
         if i > j
            continue
         end
         job_number = job_number + 1;
         params.doPlots = 1;
         fprintf(fid,'JOB %d %s/condor/schumann.sub\n',job_number,params.path)
         fprintf(fid,'RETRY %d 3\n',job_number)
         fprintf(fid,'VARS %d jobNumber="%d" startGPS="%d" endGPS="%d" type="%s" frame_type="%s" channel="%s,%s" doPlots="%d"\n',job_number,job_number,gps(j),gps(j+1),'coherence',frame_types{k},station{i},station{j},params.doPlots);
         fprintf(fid,'\n\n');
      end
   end
end

job_number_last=job_number;

% Zeroth job ensures no condor issues
fprintf(fid,'JOB 0 %s/condor/schumann.sub\n',params.path)
fprintf(fid,'VARS 0 jobNumber="0" startGPS="0" endGPS="0" type="0" frame_type="0" channel="0" doPlots="0"\n');
fprintf(fid,'\n\n');

% Only run jobs when zeroth job completes
fprintf(fid,'PARENT 0 CHILD');
for i=1:job_number_last
   fprintf(fid,' %d',i);
end
fprintf(fid,'\n\n');

% Only run jobs when zeroth job completes
fprintf(fid,'PARENT');
for i=1:job_number_middle
   fprintf(fid,' %d',i);
end
fprintf(fid,' CHILD');
for i=job_number_middle+1:job_number_last
   fprintf(fid,' %d',i);
end
fprintf(fid,'\n\n');

% Only run jobs when zeroth job completes
fprintf(fid,'PARENT');
for i=1:job_number_middle_2
   fprintf(fid,' %d',i);
end
fprintf(fid,' CHILD');
for i=job_number_middle_2+1:job_number_last
   fprintf(fid,' %d',i);
end
fprintf(fid,'\n\n');

fclose(fid);

LD_LIBRARY_PATH = '/ldcg/matlab_r2012a/runtime/glnxa64:/ldcg/matlab_r2012a/sys/os/glnxa64:/ldcg/matlab_r2012a/bin/glnxa64:/ldcg/matlab_r2012a/sys/java/jre/glnxa64/jre/lib/amd64/native_threads:/ldcg/matlab_r2012a/sys/java/jre/glnxa64/jre/lib/amd64/server:/ldcg/matlab_r2012a/sys/java/jre/glnxa64/jre/lib/amd64:/ldcg/matlab_r2012a/sys/opengl/lib/glnxa64:/ligotools/lib';

% Open standard universe sub file for writing
fid=fopen([params.path '/condor/schumann.sub'],'w+');
fprintf(fid,'executable = /home/mcoughlin/matapps/packages/stochastic/trunk/stamp2/condor-matlab-init\n');
fprintf(fid,'output = %s/condor/logs/out.$(jobNumber)\n',params.path);
fprintf(fid,'error = %s/condor/logs/err.$(jobNumber)\n',params.path);
fprintf(fid,'arguments = /home/mcoughlin/Stochastic/Schumann/bin/schumann_run $(startGPS) $(endGPS) $(type) $(frame_type) $(channel) $(doPlots)\n');
fprintf(fid,'notification = never\n');
fprintf(fid,'getenv = False\n');
fprintf(fid,'environment = LD_LIBRARY_PATH=%s\n',LD_LIBRARY_PATH);
fprintf(fid,'log = /usr1/mcoughlin/schumann.log\n');
fprintf(fid,'+MaxHours = 24\n');
%fprintf(fid,'universe = standard\n');
fprintf(fid,'queue 1\n');
fclose(fid);

