function [t,hp,hc] = chirplet(tau,f0,fdot,distance,fs);

 dt = 1/fs;
 endt=tau;
 T = endt;     
       t=0:dt:endt;
       f = f0 + fdot*t;

        % ---- Required parameters.
        h_rss = 1/distance;
        T0 = tau/2;

        % ---- Optional parameters.
        alpha = 0;
        delta = 0;
        iota = 0;

        % ---- Waveform.
        h = 2^0.5*h_rss*exp(...
                (-1+j*alpha)*(t-T0).^2./(4*tau.^2) ...
                +j*2*pi*(t-T0).*f ...
                +j*delta  ...
            )./(2*pi*tau^2).^(1/4);
        hp = 1/2*(1+(cos(iota))^2) * real(h);
        hc = cos(iota) * imag(h);

   taper = linspace(0,1-dt,fs);
   mask = ones(size(t));
   mask(1:fs)=sin(pi/2.*taper).^2;
   mask(end-fs+1:end) = fliplr(mask(1:fs));
   
    hp = hp .* mask;
    hc = hc .* mask;


