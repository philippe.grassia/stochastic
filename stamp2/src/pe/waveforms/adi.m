function [t,hplus,hcross] = adi(Mbh,astar,epsilon,distance,fs);

% PMVP waveform generator
% van Putten-based model for SN GW radiation
% theta colatitude
% phi azimuth

% EHT
% physical parameters
%Mbh = 10. * (1.98892e33)  % mass of the central BH (3-10 Msun)
%Mbh = 5. * (1.98892e33)  % mass of the central BH (3-10 Msun)
%Mbh = 1.3 * (1.98892e33)  % causes the code to break at t=82.5s; the peak is at 114 Hz
Mbh = Mbh * (1.98892e33);  % causes the code to break at t=82.5s; the peak is at 114 Hz
%astar = 0.95      % spin of the central BH; astar = Jbh/Mbh^2 (0.3-0.99)
%astar = 0.99      % spin of the central BH; astar = Jbh/Mbh^2 (0.3-0.99)
Jbh = astar * Mbh^2. * (6.6726e-8)/(2.9979e10);  % In cgs units. astar is dimensionless
Mdisk = 1 * (1.98892e33);    % mass of the accretion disk/torus (how big?)
% DEFAULT VALUE
%Mdisk = 1.5 * (1.98892e33)    % mass of the accretion disk/torus (how big?)
%Mdisk = 3 * (1.98892e33)    % mass of the accretion disk/torus (how big?)
%epsilon = 0.2    % fraction of Mdisk that goes into the clumps (0.01-0.5 ?)
%epsilon = 0.05    % fraction of Mdisk that goes into the clumps (0.01-0.5 ?)
%epsilon = 0.5    % fraction of Mdisk that goes into the clumps (0.01-0.5 ?)
mch = epsilon * Mdisk;  % mass of the chunks forming the 'binary'
rct = 10000000.;    % radius of the thin disk. Binary is at rct + Risco
D = 3.08568e18*10*1000*distance;  % 10 kpc

% sky position
theta = 0.0;
phi = 0.0;

% Initial conditions
M0 = Mbh;
J0 = Jbh;
Risco0 = Risco(J0,M0);

olddata = [Risco0 J0 M0];   % In cgs units

% R_isco of a Kerr BH (Bardeen et al 72).
% Take direct (not retrograde) orbits

% Let's have fun
% parameters
totaltime = 10000; % seconds, duration
%dt = 6.1035e-05;  % sampling time
dt = 1/fs;

fprintf('.....................................\n');
fprintf('Integrating system with central BH of\n');
fprintf('mass (Msun) = %f and spin a* = %f\n',Mbh/(1.98892e33), astar);
fprintf('mass of each of the clumps (Msun) = %f\n', mch/(1.98892e33));
fprintf('orbiting at (risco + %f km)\n',rct/1e5);
fprintf('evolving the system for %f seconds\n',totaltime);
fprintf('.....................................\n');

% Output file
%EHT
time = 0.0;

% Variables to store Risco, J, M in case we want to plot them
t = [time];
r = [Risco0];
jbh = [J0];
mbh = [M0];
ast = [astar];
hplus = [0.];
hcross = [0.];

idotdot = Idotdot(mch,rct + Risco0,Omega(M0, rct + Risco0),time);

% Integrate
while (time < totaltime && Jbh >= 0.)

    vars = [mch rct];   

    % Evolve variables using Runge Kutta 4
    newdata = rk4(olddata,time,dt,vars);
    time = time + dt;

    % Store data in variables
    t = [t time];
    r = [r newdata(1)];
    jbh = [jbh newdata(2)];
    mbh = [mbh newdata(3)];
    astar = ((2.9979e10)/(6.6726e-8)) * newdata(1)/(newdata(2)*newdata(2));

    ast = [ast astar];

    % Actualize variable to check complete spin-down
    Jbh = newdata(2);

    % Having evolved Risco, Jbh, Mbh, compute the mass quadrupole momentum
    idotdot = Idotdot(mch,rct + newdata(1),Omega(newdata(3),rct + newdata(1)),time);
    % Compute gravitational radiation!
    h = 0.0;
    for m = [-2,-1,0,1,2]
        cylm = re_ylm_2(2,m,theta,phi) + 1j * im_ylm_2(2,m,theta,phi);
        Hlm  = re_Hlm(2,m,idotdot) + 1j *im_Hlm(2,m,idotdot);
        h = h + cylm * Hlm;
    end
    hp = real(h)/D;
    hx = imag(h)/D;

    hplus = [hplus hp];
    hcross = [hcross hx];

    olddata = newdata;
end

% Check whether we ran out of time or whether the BH was completely spun down
if Jbh < 0
    fprintf('The BH was totally spun down at time t = %f\n', time);
    fprintf('Evolution stopped before given total time = %f\n', totaltime);
end

end

% R_isco of a Kerr BH (Bardeen et al 72). 
% Take direct (not retrograde) orbits
function z1 = Z1(J,M)
    z1 = 1. + ((1-J*(2.9979e10)/(6.6726e-8)/M^2)^(1/3) + (1+J*(2.9979e10)/(6.6726e-8)/M^2.)^(1/3)) * (1-J^2.*(2.9979e10)^2./(6.6726e-8)^2./M^4.)^(1/3);
end

function z2 = Z2(J,M)
    z1 = Z1(J,M);
    z2 = sqrt(z1^2. + 3.*(J*(2.9979e10)/(6.6726e-8)/M^2)^2);
end

function risco =  Risco(J,M)
    z1 = Z1(J,M);
    z2 = Z2(J,M);
    % Return Risco in cgs units
    risco = M*(3. + z2 - sqrt((3.-z1)*(3.+z1+2.*z2))) * (6.6726e-8)/(2.9979e10)^2;
end

% Derivatives of above quantities (check Risco.nb)
function z1dot = Z1dot(J,M,Jdot,Mdot)
    jm2 = (2.9979e10)/(6.6726e-8) * J/M^2;
    num = (2.9979e10) * (M*Jdot - 2.*J*Mdot);
    f1 = 3.*(2.9979e10)^2. * J^2. * ( (1+jm2)^(2./3.) - ((1-jm2)^(2./3.)) );
    f2 = -2.* (2.9979e10) * (6.6726e-8) * J * M^2. * ( (1+jm2)^(2./3.) + ((1-jm2)^(2./3.)) );
    f3 = (6.6726e-8)^2. * M^4. * ( (1-jm2)^(2./3.) - ((1+jm2)^(2./3.)) );
    den = 3*(6.6726e-8)^3. * M^7. * (1 - jm2^2.)^(4./3.);
    z1dot = (f1 + f2 + f3) * num/den;
end

function z2dot = Z2dot(J,M,Jdot,Mdot)
    jm2 = (2.9979e10)/(6.6726e-8) * J/M^2;
    num = 3.*(2.9979e10)^2.*J*M*Jdot - 6.*(2.9979e10)^2.*J^2 * Mdot + (6.6726e-8)^2.*M^5 * Z1(J,M)*Z1dot(J,M,Jdot,Mdot);
    den = (6.6726e-8)^2.*M^5 * sqrt(3.*jm2^2. + Z1(J,M)^2);
    z2dot = num / den;
end

function riscodot = Riscodot(J,M,Jdot,Mdot)
    f1  = Mdot * (3. + Z2(J,M) - sqrt((3. - Z1(J,M)) * (3. + Z1(J,M) + 2.*Z2(J,M))));
    num = (Z1(J,M) + Z2(J,M))*Z1dot(J,M,Jdot,Mdot) - (3.-Z1(J,M))*Z2dot(J,M,Jdot,Mdot);
    den = sqrt((3. - Z1(J,M)) * (3. + Z1(J,M) + 2.*Z2(J,M)));
    riscodot = f1 + M * (Z2dot(J,M,Jdot,Mdot) + num/den) ;
    % Return Riscodot in cgs units
    riscodot = riscodot * (6.6726e-8)/(2.9979e10)^2;
end

% Angular velocity courtesy of Kepler.
function omega = Omega(M,dist)
    omega = sqrt((6.6726e-8) * M / dist^3.);
end

% Loss of angular momentum and mass = P_GW/c^2
function jdot = Jdot(m,M,dist)
    jdot = -128./5. * m^2. * dist^4. * (Omega(M,dist))^5. * (6.6726e-8) / (2.9979e10)^5.;
end

function mdot = Mdot(m,M,dist)
    mdot = -128./5. * m^2. * dist^4. * (Omega(M,dist))^6. * (6.6726e-8) / (2.9979e10)^7.;
end

% Functions for the RK4 integration scheme
% We evolve Risco, J, M simultaneously
function ret = diff(t,dat,vars)
   
    mch = vars(1); rct = vars(2);

    r = dat(1);
    jbh = dat(2);
    mbh = dat(3);

    jdot = Jdot(mch, mbh, rct + r);
    mdot = Mdot(mch, mbh, rct + r);
    rdot = Riscodot(jbh, mbh, jdot, mdot);

    ret = [rdot jdot mdot];
end

function new = rk4(old,t,dt,vars)
    k1 = diff(t,old,vars);
    k2 = diff(t + 0.5*dt, old + 0.5*dt*k1,vars);
    k3 = diff(t + 0.5*dt, old + 0.5*dt*k2,vars);
    k4 = diff(t + dt, old + dt*k3,vars);

    new = old + dt * (k1 + 2.*k2 + 2.*k3 + k4)/6.;
end

% Second derivative of the reduced mass quadrupole moment
function idotdot = Idotdot(m,dist,omega,time)
    fact = 4. * m * dist^2. * omega^2.;
    idotdot = zeros(3,3);
    idotdot(1,1) = - cos(2.0*omega*time);
    idotdot(1,2) = - sin(2.0*omega*time);
    idotdot(1,3) = 0.0;
    idotdot(2,1) = - sin(2.0*omega*time);
    idotdot(2,2) =   cos(2.0*omega*time);
    idotdot(2,3) = 0.0;
    idotdot(3,1) = 0.0;
    idotdot(3,2) = 0.0;
    idotdot(3,3) = 0.0;
    idotdot = idotdot * fact;   
end

% Useful function that gets hp, hx and returns instantaneous frequency
function freq = instfreq(hp,hx,dt)
    % Construct dot vectors (2nd order differencing)
    hpdot = [0.];
    hxdot = [0.];
    for i = 1:length(hp)-1
        hpdot = [hpdot (hp(i+1)-hp(i-1))*(1./(2.*dt))];
        hxdot = [hxdot (hx(i+1)-hx(i-1))*(1./(2.*dt))];
    end
    hpdot = [hpdot 0.];
    hxdot = [hxdot 0.];
    % Compute frequency using the fact that  
    % h(t) = A(t) e^(i Phi) = Re(h) + i Im(h) 
    freq = [0.];
    for i = 1:length(hp)
        den = (hp(i)^2. + hx(i)^2)
        freq = [freq (-hxdot(i) * hp(i) + hpdot(i) * hx(i))/den];
    end
end

% Spherical harmonics
function ret = re_ylm_2(l,m,theta,phi)
    if l ~= 2
        'l != 2 not implemented! Good Bye!'
        error()
    elseif m < -2 or m > 2
        'm must be in [-2,2]! Good Bye!'
        error()
    elseif m == 0
        ret = sqrt(15.0/32.0/pi) * sin(theta)^2;
    elseif m == 1
        ret = sqrt(5.0/16.0/pi) * (sin(theta) + (1.0+cos(theta))) * cos(phi);
    elseif m == 2
        ret = sqrt(5.0/64.0/pi) * (1.0+cos(theta))^2 * cos(2.0*phi);
    elseif m == -1
        ret = sqrt(5.0/16.0/pi) * (sin(theta) + (1.0-cos(theta))) * cos(phi);
    elseif m == -2
        ret = sqrt(5.0/64.0/pi) * (1.0-cos(theta))^2 * cos(2.0*phi);
    end
end

function ret = im_ylm_2(l,m,theta,phi)
    if l ~= 2
        'l != 2 not implemented! Good Bye!'
        error()
    elseif m < -2 or m > 2
        'm must be in [-2,2]! Good Bye!'
        error()
    elseif m == 0
        ret = 0.0e0;
    elseif m == 1
        ret = sqrt(5.0/16.0/pi) * (sin(theta) + (1.0+cos(theta))) * sin(phi);
    elseif m == 2
        ret = sqrt(5.0/64.0/pi) * (1.0+cos(theta))^2 * sin(2.0*phi);
    elseif m == -1
        ret = - sqrt(5.0/16.0/pi) * (sin(theta) + (1.0-cos(theta))) * sin(phi);
    elseif m == -2
        ret = - sqrt(5.0/64.0/pi) * (1.0-cos(theta))^2 * sin(2.0*phi);
    end
end

% Expansion parameters to get the (l,m) modes
function ret = re_Hlm(l,m,Idd)
    if l ~= 2
        'l != 2 not implemented! Good Bye!'
        error()
    elseif m < -2 or m > 2
        'm must be in (-2,2)! Good Bye!'
        error()
    elseif m == 0
        ret = (8.2609e-50) * sqrt(32*pi/15.0) * ...
              (Idd(3,3) - 0.5e0*(Idd(1,1) + Idd(2,2)));
    elseif m == 1
        ret =  - (8.2609e-50) * sqrt(16*pi/5) * ...
              Idd(1,3);
    elseif m == 2
        ret = (8.2609e-50) * sqrt(4*pi/5) * ...
              (Idd(1,1) - Idd(2,2));
    elseif m == -1
        ret =  (8.2609e-50) * sqrt(16*pi/5) * ...
              Idd(1,3);
    elseif m == -2
        ret = (8.2609e-50) * sqrt(4.0*pi/5) * ...
               (Idd(1,1) - Idd(2,2));
    end
end

function ret = im_Hlm(l,m,Idd)
    if l ~= 2
        'l != 2 not implemented! Good Bye!'
        error()
    elseif m < -2 or m > 2
        'm must be in (-2,2)! Good Bye!'
        error()
    elseif m == 0
        ret = 0.0e0;
    elseif m == 1
        ret =  (8.2609e-50) * sqrt(16*pi/5) * ...
              Idd(2,3);
    elseif m == 2
        ret = (8.2609e-50) * sqrt(4*pi/5) * ...
              -2 * Idd(1,2);
    elseif m == -1
        ret = +(8.2609e-50) * sqrt(16*pi/5) * ...
              Idd(2,3);
    elseif m == -2
        ret =  (8.2609e-50) * sqrt(4.0*pi/5) * ...
              +2 * Idd(1,2);
    end
end    

