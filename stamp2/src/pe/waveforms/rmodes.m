function [t,hp,hc] = rmodes(alpha,f0,distance,fs)

 %% This is the Owen model as described in Owen% Lindblom, 1998
        
 k=-1.8*alpha^2*10^-21 ;
 
 dt = 1/fs;
 endt=5000;
 T = endt;     
       t=0:dt:endt;
       fOwen= 1./(1/f0^6 - 6*k*t).^(1/6);
       I=-(1/(5*k))*(f0^(-6)-6*k*t).^(5/6);        % This is the definite integral of f(t) calculated at t 
       int=I+(1/(5*k))*(f0^(-6)).^(5/6);           % This is the integral at t minus the integral at 0 
       Phi = (2*pi)*int;      
  
     d=0.001 * distance;            % In units of Mpc
     fOwen=fOwen(1:end);
     ho=3.6*10^-23*(1/d)*(fOwen/1000).^3*alpha;    

     w=0; % i is the inclination angle that is assumed to be zero
     hp=ho.*(1+cos(w).^2).*cos(Phi);                          % this is the plus polarization
     hc=2*ho.*cos(w).*sin(Phi);                               % this is the cross polarization 
  
   
   taper = linspace(0,1-dt,fs);
   mask = ones(size(t));
   mask(1:fs)=sin(pi/2.*taper).^2;
   mask(end-fs+1:end) = fliplr(mask(1:fs));
   
    hp = hp .* mask;
    hc = hc .* mask;

  
 
