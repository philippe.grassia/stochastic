function [L,mask] = stamp_likelihood(t,hp,map,params)

fs = 1/(t(2) - t(1));
fs = gGather(fs,params);

[a,b] = size(map.snr);
mask = gZeros(a,b,params);
map_f = map.f;
dt = map.xvals(2) - map.xvals(1);
map_t = map.xvals;

NFFT = 2^nextpow2(fs); % Next power of 2 from length of y
f = fs/2*linspace(0,1,NFFT/2+1);
windowconst = 0.375;

segmentStart = 16;

for i = 1:length(map_t)
   minIndex = round(max((((i-segmentStart)-1.5)*dt*fs) + 1,1));
   maxIndex = round(min(((i-segmentStart)+0.5)*dt*fs,length(t)));

   indexes = minIndex:maxIndex;

   window = hann(length(indexes));

   if length(indexes) == 0
      continue
   end

   if indexes(end) > length(hp)
      continue
   end

   L = length(indexes);
   Y = fft(hp(indexes) .* window',NFFT);
   Y_abs = 4 * (Y(1:NFFT/2+1) .* conj(Y(1:NFFT/2+1))) / (L*fs*windowconst);

   Y_interp = interp1(f,Y_abs,map_f);
   
   mask(:,i) = Y_interp;

end

mask = gGather(mask,params);

mask = map.snr - (mask ./ map.sigma);

snr_Ls = stamp_snr_dist(map.snr(:));
mask_Ls = stamp_snr_dist(mask(:));

snr_logLs = log(snr_Ls); mask_logLs = log(mask_Ls);

L = sum(mask_logLs) - sum(snr_logLs);

