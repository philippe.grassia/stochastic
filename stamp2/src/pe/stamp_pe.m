function stoch_out = stamp_pe(map,params,stoch_out)
% function stoch_out = stamp_pe(map,params,stoch_out)
%
% Runs STAMP parameter estimation.
%
% Arguments:
% map     - STAMP map struct.
% params  - STAMP params struct
% stoch_out - STAMP output struct
%
% Written by Michael Coughlin
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

% Calculate xvals.
map.xvals = map.segstarttime+params.segmentDuration/4 - ...
    map.segstarttime(1);

try 
   stoch_out.fits;
catch
   stoch_out.fits = [];
end

cluster = stoch_out.cluster;

% PRINT LARGE CLUSTER
printmap(cluster.reconMax, map.xvals, map.f, 't (s)', ['f' ...
		    ' (Hz)'],'SNR',[-5 5],0,0);
text_title = sprintf('Max. SNR = %.3f',cluster.snr_gamma);
title(text_title);
pretty;
print('-dpng', [params.plotdir '/PE_large_cluster']);
print('-depsc2', [params.plotdir '/PE_large_cluster']);
close;

cluster_snr_1d = cluster.reconMax(:);

points = [];
for i = 1:length(map.xvals)
   for j = 1:length(map.f)
      if cluster.reconMax(j,i) > 0
         points = [points; map.xvals(i) map.f(j) cluster.reconMax(j,i)];
      end
   end
end

xdata = points(:,1); ydata = points(:,2); zdata = points(:,3);

slope = (ydata(end) - ydata(1)) / (xdata(end) - xdata(1));

f = fittype('a + b*x');
fit1 = fit(xdata,ydata,f,'StartPoint',[mean(ydata) slope]);

fdata = feval(fit1,xdata);
I = abs(fdata - ydata) > 1.5*std(ydata);
outliers = excludedata(xdata,ydata,'indices',I);

fit2 = fit(xdata,ydata,f,'StartPoint',[mean(ydata) slope],'Exclude',outliers);
fit3 = fit(xdata,ydata,f,'StartPoint',[mean(ydata) slope],'Robust','on');

figure;
plot(fit1,'r-',xdata,ydata,'k.',outliers,'m*')
hold on
plot(fit2,'c--')
plot(fit3,'b:')
hold off
pretty;
print('-dpng', [params.plotdir '/PE_large_cluster_fit']);
print('-depsc2', [params.plotdir '/PE_large_cluster_fit']);
close;

f0 = fit3(0);
df = fit3.b;

global verbose;
verbose = 1;
global DEBUG;
DEBUG = 0;

data{1} = map;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% define nested sampling parameters
Nlive = 50;

Nmcmc = 0;
%Nmcmc = 100;
%tolerance = 1e-5;
tolerance = 0.3;
likelihood = @stamp_logL;

if strcmp(params.pe.model,'rmode');
   model = @rmodes_model;
   %prior = {'alpha', 'uniform', -4, 0, 'fixed'; ...
   %         'f0', 'uniform', 600, 1550, 'fixed'; ...
   %         'distance', 'uniform', 70, 80, 'fixed'};
   prior = {'alpha', 'uniform', -4, 0, 'fixed'; ...
            'f0', 'uniform',f0-20, f0+20, 'fixed';
            'distance', 'uniform', 70, 80, 'fixed'};
elseif strcmp(params.pe.model,'chirplet');
   model = @chirplet_model;
   prior = {'tau', 'uniform', 1, 1000, 'fixed'; ...
            'f0', 'uniform',f0-20, f0+20, 'fixed'};
            %'fdot', 'uniform',-1, 1, 'fixed';...
            %'distance', 'uniform', 70, 80, 'fixed'};
elseif strcmp(params.pe.model,'adi');
   model = @adi_model;
   prior = {'tau', 'uniform', 1, 1000, 'fixed'; ...
            'f0', 'uniform',f0-20, f0+20, 'fixed'};
            %'fdot', 'uniform',-1, 1, 'fixed';...
            %'distance', 'uniform', 70, 80, 'fixed'};
else
   errorMessage = sprintf('Model: %s not supported\n',params.pe.model);
   error(errorMessage);
end

extraparams = {}; % fixed signal frequency

[size_prior_x,size_prior_y] = size(prior);

matFile = [params.plotdir '/multinest.mat'];
doRun = 1;
if doRun
   % called nested sampling routine
   [logZ, nest_samples, post_samples] = nested_sampler(data, Nlive, Nmcmc, ...
      tolerance, likelihood, model, prior, extraparams);
   save(matFile);
else
   load(matFile);
end

% plot posterior distributions
for i = 1:size_prior_x
   plotName = [params.plotdir '/PE_' prior{i,1}];
   posteriors(post_samples, i, {prior{i,1}}, plotName);

   plotName = [params.plotdir '/PE_nest_' prior{i,1}];
   nest_samples_plot(nest_samples, i, {prior{i,1}}, plotName);

   for j = 1:size_prior_x
      if j<=i
         continue
      end

      plotName = [params.plotdir '/PE_' prior{i,1} '_' prior{j,1}];
      posteriors(post_samples, [i j], {prior{i,1}, prior{j,1}}, plotName);
      plotName = [params.plotdir '/PE_logl_' prior{i,1} '_' prior{j,1}];
      posteriors_logl(post_samples, [i j], {prior{i,1}, prior{j,1}}, plotName);

   end

end

[logL_max,logL_max_index] = max(post_samples(:,size_prior_x+1));
fs = 4096;

if strcmp(params.pe.model,'rmode');
   alpha_max = 10^post_samples(logL_max_index,1);
   f0_max = post_samples(logL_max_index,2);
   distance_max = post_samples(logL_max_index,3);

   [t,hp,hc] = rmodes(alpha_max,f0_max,distance_max,fs);

elseif strcmp(params.pe.model,'chirplet');
   tau_max = post_samples(logL_max_index,1);
   f0_max = post_samples(logL_max_index,2);
   fdot_max = 0;
   distance_max = 5 * (10^(21)) * 0.1;

   [t,hp,hc] = chirplet(tau_max,f0_max,fdot_max,distance_max,fs);
elseif strcmp(params.pe.model,'adi');
end

[L,mask] = stamp_likelihood(t,hp,map);

%alpha_max = 0.1; f0_max = 800; distance_max = 75;
%fs = 4096;
%[t,hp,hc] = rmodeswaveforms(alpha_max,f0_max,distance_max,fs);
%[L,mask] = rmodes_likelihood(t,hp,map);

%indexes = find(map.f > 600 & map.f < 800);
%map.f = map.f(indexes);
%mask = mask(indexes,:);
%map.y = map.y(indexes,:);
%map.snr = map.snr(indexes,:);

printmap(mask, map.xvals, map.f, 't (s)', ['f' ...
                    ' (Hz)'],'SNR',[min(min(map.snr)) max(max(map.snr))],0,0);
%text_title = sprintf('alpha = %.3e, f0 = %.1f, distance=%.1f',alpha_max,f0_max,distance_max);
%title(text_title);
pretty;
print('-dpng', [params.plotdir '/PE_mask']);
print('-depsc2', [params.plotdir '/PE_mask']);
close;

printmap(map.snr, map.xvals, map.f, 't (s)', ['f' ...
                    ' (Hz)'],'SNR',[min(min(map.snr)) max(max(map.snr))],0,0);
%text_title = sprintf('alpha = %.3e, f0 = %.1f, distance=%.1f',alpha_max,f0_max,distance_max);
%title(text_title);
pretty;
print('-dpng', [params.plotdir '/PE_snr']);
print('-depsc2', [params.plotdir '/PE_snr']);
close;

binMin = min([min(min(map.snr)) min(min(mask))]);
binMax = max([max(max(map.snr)) max(max(mask))]);

bins = binMin:0.1:binMax;
hist_values_snr = hist(map.snr(:),bins);
hist_values_mask = hist(mask(:),bins);

sigma = 1.0;
K = besselk(0,abs(bins)/sigma);

normprod_pdf = K / (sigma*pi);
normprod_pdf = normprod_pdf / sum(normprod_pdf);

figure;
set(gcf, 'PaperSize',[10 8])
set(gcf, 'PaperPosition', [0 0 10 8])
clf

stairs(bins,hist_values_snr/sum(hist_values_snr),'r')
hold on
stairs(bins,hist_values_mask/sum(hist_values_mask),'b')
plot(bins,normprod_pdf,'k')
hold off

hleg1 = legend('SNR','SNR-waveform','Normal Product');
set(hleg1,'Location','NorthEast')
set(hleg1,'Interpreter','none')

xlim([-5 5]);

print('-dpng',[params.plotdir '/PE_hist.png']);
close;


keyboard

return;
