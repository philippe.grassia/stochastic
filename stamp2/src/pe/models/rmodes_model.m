function mask = rmodes_model(map, parnames, parvals)

% function y = sinusoid_model(t, parnames, parvals)
%
% This function creates a sinusiod given by the equation
%     y = amp * sin(2*pi*f*t + phi)
% The input parameters are:
%
% t - the discrete times (in sec) at which y will be calculated
% parnames - a cell array containing the parameter names.  
%     These can be in any order, but must include the following
%         {'amp', 'phi', 'f'}
%     where amp is the amplitude, phi is the initial phase 
%     (in radians), and f is the frequency (in Hz)
% parvals - a cell array containing the values of the parameters
%     given in parnames.  These must be in the same order as in
%     parnames
%
%--------------------------------------------------------------------------
%           This is the format required by nested_sampler.m.
%--------------------------------------------------------------------------
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% check that parnames and parvals have the same length
lpn = length(parnames);
lpv = length(parvals);
if lpn ~= lpv
    error('Error: parnames and parvals are not the same length!');
end

nparams = lpn;

% extract parameter values
for ii=1:nparams
  switch parnames{ii}
    case 'f0'
      f0 = parvals{ii};
    case 'alpha'
      alpha = parvals{ii};
    case 'distance'
      distance = parvals{ii};
  end
end

fs = 4096;
dt = 0.5;
%distance = log10(75);

%[t_waveform,hp,hc] = rmodeswaveforms(alpha,f0,fs);
[t,hp,hc] = rmodes(10^alpha,f0,distance,fs);

fs = 1/(t(2) - t(1));

mask = zeros(size(map.snr));
map_f = map.f;
dt = map.xvals(2) - map.xvals(1);
map_t = map.xvals;

window = hann(dt*fs);

NFFT = 2^nextpow2(fs); % Next power of 2 from length of y
f = fs/2*linspace(0,1,NFFT/2+1);
windowconst = 0.375;

for i = 1:length(map_t)
   minIndex = round(max(((i-1.5)*dt*fs) + 1,1));
   maxIndex = round(min((i+0.5)*dt*fs,length(t)));

   indexes = minIndex:maxIndex;

   window = hann(length(indexes));

   L = length(indexes);
   Y = fft(hp(indexes) .* window',NFFT);
   Y_abs = 4 * (Y(1:NFFT/2+1) .* conj(Y(1:NFFT/2+1))) / (L*fs*windowconst);

   Y_interp = interp1(f,Y_abs,map_f);

   mask(:,i) = Y_interp;

end


end
