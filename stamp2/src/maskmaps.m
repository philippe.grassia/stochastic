function map = maskmaps(map, params)
% function map = maskmaps(map, params)
% Eric Thrane: applies user-specified frequency bin notches

% find map frequency bin indices corresponding to notches
[tmp, tmp2, mapidx] = intersect(params.StampFreqsToRemove, map.f);

% remove frequencies outside the range of interest
params.StampFreqsToRemove = ...
    params.StampFreqsToRemove((params.StampFreqsToRemove>=params.fmin));
params.StampFreqsToRemove = ...
    params.StampFreqsToRemove((params.StampFreqsToRemove<=params.fmax));

% check to make sure that all of the removed frequencies could be notched
if length(mapidx) ~= length(params.StampFreqsToRemove)
  error(['StampFreqsToRemove contains bins that are not in data.']);
end

% apply notches
map.cc(mapidx,:) = NaN;
map.sensInt(mapidx,:) = NaN;
map.naiP1(mapidx,:) = NaN;
map.naiP2(mapidx,:) = NaN;
map.P1(mapidx,:) = NaN;
map.P2(mapidx,:) = NaN;

return
