function tau = calfau(det1, det2, GPSTimes, source, params)
% function tau = caltau(det1, det2, GPSTimes, source, params)
% E. Thrane - moved from ccSpecReadout_stamp
% The same as calF.m except this version has been stripped down to only 
% calculate tau.

% make sure that ra and dec are in range
if any(source(:,1)<0 | source(:,1)>24)
  error('ra out of range');
end
%
if any(source(:,2)<-90 | source(:,2)>90)
  error('dec out of range');
end

% calculate sidereal time
time = GPStoGreenwichMeanSiderealTime(GPSTimes);

% replicate time, ra, and dec so we can calculate the filter
% for multiple combinations of GPS times and sky directions.
time = repmat(time,size(source,1),1);
ras = repmat(source(:,1),1,size(time,2));
decs = repmat(source(:,2),1,size(time,2));

% convert hours into radians: 24 hrs / 2pi rad
w = pi/12;
N = length(GPSTimes);

% calculate ingredients needed for antenna factors
psi = w*(time-ras);
theta = -pi/2+pi/180*decs;
ctheta = cos(theta);
stheta = sin(theta);
cpsi = cos(psi);
spsi = sin(psi);
Omega1 = -cpsi .* stheta;
Omega2 = spsi .* stheta;
Omega3 = ctheta;
pp11 = (ctheta.^2) .* (cpsi.^2) - (spsi.^2);
pp12 = -(ctheta.^2+1) .* cpsi .* spsi;
pp13 = ctheta .* cpsi .* stheta;
pp22 = (ctheta.^2) .* (spsi.^2) - (cpsi.^2);
pp23 = -ctheta .* spsi .* stheta;
pp33 = stheta.^2;
px11 = -2*ctheta .* cpsi .* spsi;
px12 = ctheta .* ((spsi.^2)-(cpsi.^2));
px13 = -stheta .* spsi;
px22 = 2*ctheta .* cpsi .* spsi;
px23 = -stheta .* cpsi;
%px33 = 0;

% calculate vector for the separation between sites.
s = det2.r - det1.r;

% time delay between detectors
tau = (Omega1*s(1)+Omega2*s(2)+Omega3*s(3))/params.c;

return;
