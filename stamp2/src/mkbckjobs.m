function mkbackjobs()
% function mkbackjobs()
% This function tailors the given job file such that each job in a new jobfile
% (that it will produce) will produce ft-maps with the same size when run
% with clustermap.m
% INPUT: a jobfile
% OUTPUT: another job file (psuedojobs.txt)
%
% This code is similar to prep4bknd.m.  Here is a description of the
% difference:
% > Does this code do anything different from prep4bknd.m?
%  Both mkbckjobs.m and prep4bknd.m produce a job file which when ran with
% clustermap.m, for each job, will produce maps with same number of time
% segments. But the way they produce that file is different and the new
% job files they produce themselves are different.
% (i) prep4bknd.m relies on findmats.m which in turn relies on the
% availability of mat files. So a complete set of mat files from already
% run preproc.m is a necessary condition for running prep4bknd.m while new
% mkbckjobs.m takes only the initial job file and produces a new job file
% (one of the main purpose of new clustermap -> preproc is not to produce
% any mat files).
%  (ii) The new job file is different. Since the mat files were already
% produced, with prep4bknd.m we don't have to worry about the boundaries
% of each job (to account neighboring segments, buffer seconds). But in
% clustermap -> preproc method, we run preproc for the first time and
% hence have to include seconds for neighboring segments and buffer. Also
% the jobs in the new job file will overlap (in contrast to jobfile
% produced byprep4bknd.m), because of the addition of neighboring and
% buffer seconds to each job.

% input parameters
jobs = load('S5H1L1_full_run.txt'); % load given job file
mapTimeSize = 1500; % required map size (in secs)
segmentDuration = 1; % segmentDuration
numSegmentsPerInterval = 9;% number of segmentsPerInterval
bufferSecsMax = 2; % maximim of the two buffer second durations
doOverlap = true; % overlap true or false
fid = fopen('psuedojobs.txt','w'); % outputfile

% calculation starts
mapNoofSegments = floor(mapTimeSize/segmentDuration)*2-1; % total number of overlapping segments
ii = 1;
kk = 1;
segmentTimeVector = [];
newJobstartTime = jobs(ii,2);
while(ii <= length(jobs(:,1)))
  startTime = jobs(ii,2);
  jobDuration = jobs(ii,4);
  M = floor( (jobDuration - 2*bufferSecsMax)/segmentDuration );
  if doOverlap
    if(mod((jobDuration-2*bufferSecsMax)/segmentDuration,0.5)==0)
      M = (jobDuration - 2*bufferSecsMax)/segmentDuration;
    end
    numSegmentsTotal = 2*M-1;
    numIntervalsTotal = 2*(M - (numSegmentsPerInterval-1)) - 1;
    intervalTimeStride = segmentDuration/2;
  else
    numSegmentsTotal = M;
    numIntervalsTotal = M - (numSegmentsPerInterval-1);
    intervalTimeStride = segmentDuration;
  end
  centeredStartTime = startTime + bufferSecsMax + ...
    floor( (jobDuration - 2*bufferSecsMax - M*segmentDuration)/ 2 );
  usefulStartTime = centeredStartTime + (numSegmentsPerInterval-1)*segmentDuration/2; % vector containing startTimes of all useful segments in job number ii
  segmentTimeVector =  [segmentTimeVector usefulStartTime:intervalTimeStride:usefulStartTime+(numIntervalsTotal-1)*intervalTimeStride];
  while(length(segmentTimeVector) >= mapNoofSegments)
    newJobstartTime = segmentTimeVector(1)-(numSegmentsPerInterval-1)*segmentDuration/2-bufferSecsMax;
    newJobendTime = segmentTimeVector(mapNoofSegments)+segmentDuration+(numSegmentsPerInterval-1)*segmentDuration/2+bufferSecsMax;
    fprintf(fid,'%5d  %12.2f %12.2f %10.2f \n', kk, newJobstartTime, newJobendTime, newJobendTime-newJobstartTime);
    segmentTimeVector = segmentTimeVector(mapNoofSegments+1:end);
    kk = kk+1;
  end 
  ii = ii +1;
end  
fclose(fid);
