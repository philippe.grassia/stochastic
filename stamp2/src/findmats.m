function [matlist, params] = findmats(params)
% function matlist = findmats(params)
% Eric Thrane: this function returns a list of mat files that are required to
% process the requested data.  The user must define params.inmats, e.g.,
% params.inmats = '/home/sharan.banagiri/stamp2/src/anteproc/mats';
% Modifications by Michael Coughlin.

% determine the STAMP mat file directories required for this job
% each directory contains up to 10K s of data

% Minus one added to account for special case:
% Ex: params.startGPS = 832740080 => startdirtime = 83274.
% But the .mat file containing this data is 200 seconds long,
% starting at GPS time 832739986.
startdirtime = floor(params.startGPS/10000) - 1;
enddirtime = floor(params.endGPS/10000);
dirtimes = startdirtime:enddirtime;

% record all the matlab files available
files = [];
index = 0;
% loop over possible directories containing mat files
for ii=1:length(dirtimes)
  matdir = [params.inmats '-' num2str(dirtimes(ii))];
  % list of mat files in the appropriate directories
  all_files = dir([matdir '/*.mat']);
  % for each file record its directory
  for jj = 1:length(all_files)
    index = index + 1;
    files{index} = all_files(jj).name;
    paths{index} = matdir;
  end
end

% if no mat files are found, quit clustermap
if length(files)==0
  error('No mat files found; quitting clustermap.');
end

% determine what the file prefix should be based on directory name
% e.g.: map-81606/map-816065665-200.mat
[temp pre_string] = regexp(params.inmats, '.*/(.*)', 'match', 'tokens');
params.prefix = pre_string{1};

% determine GPS times of mat files
for jj=1:length(files)
  % first, test if file names match expectation
  [temp test_string] = regexp(files{jj}, '(.*)-.*-.*', 'match', 'tokens');
  test_string = test_string{1};
  if ~strcmp(params.prefix, test_string)
    warning('findmats has encountered mat files with unexpected names.');
  end

  % determine GPS start time of mat file from file name
%EHT_JULY_4:  [temp gps_string] = regexp(files{jj}, '.*-(.*)-[0-9]*\.mat', ...
%EHT_JULY_4:    'match', 'tokens');
  [temp gps_string] = regexp(files{jj}, '.*-(.*)-.*\.mat', ...
    'match', 'tokens');
  matgps_start(jj) = str2num(char(gps_string{1}));

  % determine GPS end time from mat file name
%EHT_JULY_4:  [temp dur_string] = regexp(files{jj}, '.*-.*-([0-9])*\.mat', ...
%EHT_JULY_4:    'match', 'tokens');
  [temp dur_string] = regexp(files{jj}, '.*-.*-(.)*\.mat', ...
    'match', 'tokens');
  matdur = str2num(char(dur_string{1}));
  matgps_end(jj) = matgps_start(jj) + matdur;
end

% if the segments are more than a second long, round start and endGPS to
% nearest segment.  include try-catch syntax since findmats is called by other
% code that does not run stampDefaults
try
  params.longsegs;
catch
  params.longsegs=false;
end
if params.longsegs
  params = longsegs(params, paths, files);
end

% keep mat files that overlap with requested time
cut = params.startGPS < matgps_end & params.endGPS > matgps_start;

% record all the matlab files available
matlist = [];
index = 0;
for jj=1:length(files)
  if cut(jj)
    index = index + 1;
    matlist{index} = [paths{jj} '/' files{jj}];
  end
end

% if no mat files are found, quit clustermap
if length(matlist)==0
  error('No mat files between startGPS-stopGPS; quitting clustermap.');
end

return;
