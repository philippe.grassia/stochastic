function saveplots(params, map)
% function saveplots(params, map)
% E. Thrane: saves .png and .eps files for key diagnostic plots

% determine x-values and y-values
map.xvals = map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);
map.yvals = map.f;

if params.stamp_pem

%   map = stamp_pem_concatmaps(params, map);

   xlabel_text = sprintf('t(s) [%.1f-%.1f]',map.segstarttime(1),map.segstarttime(end));
   ylabel_text = 'f (Hz)';

else
   xlabel_text = 't (s)';
   ylabel_text = 'f (Hz)';
end

%%% For injection plot zoomed map around the injection
if params.stampinj
    tt=map.segstarttime>max(params.pass.stamp.startGPS-0.10*params.pass.stamp.dur,map.segstarttime(1))&...
       map.segstarttime<min(params.pass.stamp.startGPS+params.pass.stamp.dur+0.10*params.pass.stamp.dur,map.segstarttime(end));
    if strcmp(params.pass.stamp.inj_type,'fly')
       ff = 1:length(map.f);
    else
      % Get file extension
      [~,~,ext] = fileparts(params.pass.stamp.file);
      % .mat file
      if strcmpi(ext,'.mat')
       ff = 1:length(map.f);
      else
       [fmin,fmax]=textread(params.pass.stamp.file, '%*s %d %d %*s %*s\n',1,'headerlines', 1);
       ff=map.f>max(fmin-0.1*(fmax-fmin),map.f(1)) & ...
          map.f<min(fmax+0.1*(fmax-fmin),map.f(end));
      end
    end
end

% SNR map
printmap(map.snr, map.xvals, map.yvals, xlabel_text, ylabel_text, 'SNR', [-5 5]);
print('-dpng',[params.plotdir '/snr']);
print('-depsc2',[params.plotdir '/snr']);
if params.stampinj
  if sum(tt) >= 2
    printmap(map.snr(ff,tt), map.xvals(tt), map.yvals(ff), xlabel_text, ylabel_text, 'SNR', [-5 5]);
    print('-dpng',[params.plotdir '/snr_zoomed']);
    print('-depsc2',[params.plotdir '/snr_zoomed']);
  end
end

% y map
printmap(map.y, map.xvals, map.yvals, xlabel_text, ylabel_text, ...
  'y (strain^2)', [-params.yMapScale params.yMapScale]);
print('-dpng',[params.plotdir '/y_map']);
print('-depsc2',[params.plotdir '/y_map']);
if params.stampinj
  if sum(tt) >= 2
    printmap(map.y(ff,tt), map.xvals(tt), map.yvals(ff), xlabel_text, ylabel_text, ...
             'y (strain^2)', [-params.yMapScale params.yMapScale]);
    print('-dpng',[params.plotdir '/y_map_zoomed']);
    print('-depsc2',[params.plotdir '/y_map_zoomed']);
  end
end

% Xi map
printmap(map.Xi_snr, map.xvals, map.yvals, xlabel_text, ylabel_text, ...
  'SNR_{\Xi}', [-1 1]);
print('-dpng',[params.plotdir '/Xi_snr_map']);
print('-depsc2',[params.plotdir '/Xi_snr_map']);
if params.stampinj
  if sum(tt) >= 2
    printmap(map.Xi_snr(ff,tt), map.xvals(tt), map.yvals(ff), xlabel_text, ylabel_text, ...
             'SNR_{\Xi}', [-1 1]);
    print('-dpng',[params.plotdir '/Xi_snr_map_zoomed']);
    print('-depsc2',[params.plotdir '/Xi_snr_map_zoomed']);
  end
end


% sigma map
printmap(map.sigma, map.xvals, map.yvals, xlabel_text, ylabel_text, ...
  '-log_{10}(\sigma)', [min(min(map.sigma)) max(max(map.sigma))]);
print('-dpng',[params.plotdir '/sig_map']);
print('-depsc2',[params.plotdir '/sig_map']);
if params.stampinj
  if sum(tt) >= 2
    printmap(map.sigma(ff,tt), map.xvals(tt), map.yvals(ff), xlabel_text, ylabel_text, ...
             '-log_{10}(\sigma)', [min(min(map.sigma)) max(max(map.sigma))]);
    print('-dpng',[params.plotdir '/sig_map_zoomed']);
    print('-depsc2',[params.plotdir '/sig_map_zoomed']);
  end
end


if params.stamp_pem

   map.P1_log10(isnan(map.P1_log10)) = 0;

   % P1 map
   P1_min = mean(map.P1_log10(:)) - 2*std(map.P1_log10(:));
   P1_max = mean(map.P1_log10(:)) + 2*std(map.P1_log10(:));
   printmap(map.P1_log10, map.xvals, map.yvals, xlabel_text, ylabel_text, ... 
      strrep(params.darmChannel,'_','\_'), [P1_min P1_max]);
   print('-dpng',[params.plotdir '/P1']);
   print('-depsc2',[params.plotdir '/P1']);

   map.P2_log10(isnan(map.P2_log10)) = 0;

   % P2 map
   P2_min = mean(map.P2_log10(:)) - 2*std(map.P2_log10(:));
   P2_max = mean(map.P2_log10(:)) + 2*std(map.P2_log10(:));
   printmap(map.P2_log10, map.xvals, map.yvals, xlabel_text, ylabel_text, ... 
      strrep(params.pemChannel,'_','\_'), [P2_min P2_max]);
   print('-dpng',[params.plotdir '/P2']);
   print('-depsc2',[params.plotdir '/P2']);

   indexes = intersect(find(~isnan(max(map.fft1))),find(~isnan(max(map.fft2))));

   a1 = map.fft1(:,indexes);
   psd1 = mean(abs(a1).^2,2);
   a2 = map.fft2(:,indexes);
   psd2 = mean(abs(a2).^2,2);
   csd12 = mean(a1.*conj(a2),2);
   map.coherence = abs(csd12)./sqrt(psd1.*psd2);

   map.coherence = NaN * ones(length(map.f),1);
   for i = 1:length(map.f)
      a1 = map.fft1(i,indexes);
      psd1 = mean(abs(a1).^2,2);
      a2 = map.fft2(i,indexes);
      psd2 = mean(abs(a2).^2,2);
      csd12 = mean(a1.*conj(a2),2);
      %map.coherence(i) = abs(csd12)./sqrt(psd1.*psd2);
      map.coherence(i) = abs(xcorr(a1-mean(a1),a2-mean(a2),0,'coeff'));
   end

   if log10(max(map.f)) - log10(min(map.f)) > 1
      doLog = 1;
   else
      doLog = 0;
   end

   theoretical = ones(size(map.coherence)) * sqrt(1/length(a1));

   figure;
   if doLog
      loglog(map.f,map.coherence,'b')
      hold on
      loglog(map.f,theoretical,'r--')
      hold off
   else
      semilogy(map.f,map.coherence,'b')
      hold on
      semilogy(map.f,theoretical,'r--')
      hold off
   end
   xlabel('f (Hz)')
   ylabel('Coherence')
   xlim([min(map.f),max(map.f)])
   legend({'true','theoretical'});
   pretty;
   print('-dpng',[params.plotdir '/coherence']);
   print('-depsc2',[params.plotdir '/coherence']);
   close;

   if params.doBicoherence
      map.bicoherence = NaN * ones(length(map.f),length(map.f));
      map.bicoherence_pvalue = NaN * ones(length(map.f),length(map.f));
      map.bicoherence_threshold = NaN * ones(length(map.f),length(map.f));

      for i = 1:length(map.f) 
         for j = 1:length(map.f)
            a1 = map.fft1(i,indexes);
            psd1 = mean(abs(a1).^2,2);
            a2 = map.fft2(j,indexes);
            psd2 = mean(abs(a2).^2,2);
            csd12 = mean(a1.*conj(a2),2);
            %map.bicoherence(i,j) = abs(csd12)./sqrt(psd1.*psd2);
            map.bicoherence(i,j) = abs(xcorr(a1-mean(a1),a2-mean(a2),0,'coeff'));

            rho2 = map.bicoherence(i,j).^2;
            prho2 = (1-rho2).^(length(a1)-1);

            %map.bicoherence_pvalue(i,j) = 1-prho2;
            map.bicoherence_pvalue(i,j) = rho2 * length(a1);

            [junk,a1_indexes] = sort(a1,'descend');
            [junk,a2_indexes] = sort(a2,'descend');

            N = floor(length(a1) * 1);
            a1_indexes = a1_indexes(1:N);

            a1 = a1(a1_indexes);
            psd1 = mean(abs(a1).^2,2);
            a2 = a2(a1_indexes);
            psd2 = mean(abs(a2).^2,2);
            csd12 = mean(a1.*conj(a2),2);
            %map.bicoherence(i,j) = abs(csd12)./sqrt(psd1.*psd2);
            map.bicoherence_threshold(i,j) = ...
               abs(xcorr(a1-mean(a1),a2-mean(a2),0,'coeff'));
         end
      end

      rho2 = logspace(-7,0,500);
      prho2 = (1-rho2).^(length(a1)-1);
      threshold = 0.001;
      [junk,threshold_index] = min(abs(prho2-threshold));
      prho2_threshold = prho2(threshold_index);

      ticks = logspace(log10(min(map.f)),log10(max(map.f)),15);
      ticks = 10.^ticks;
      ticks = unique(floor(ticks));
      %ticks =min(map.f):2:max(map.f);

      [X,Y] = meshgrid(map.f,map.f);
      figure;
      hC = pcolor(X,Y,map.bicoherence');
      set(gcf,'Renderer','zbuffer');
      colormap(jet)
      shading interp
      t = colorbar('peer',gca);
      set(get(t,'ylabel'),'String','Coherence');
      set(t,'fontsize',25);
      set(hC,'LineStyle','none');
      grid
      set(gca,'clim',[0 0.1])
      %set(gca,'clim',[threshold 1])
      %set(gca,'clim',[min(min(map.bicoherence)) max(max(map.bicoherence))])
      xlabel(sprintf('Frequency [Hz] [%s]',strrep(params.darmChannel,'_','\_')))
      ylabel(sprintf('Frequency [Hz] [%s]',strrep(params.pemChannel,'_','\_')));
      if doLog
         set(gca,'xscale','log');
         set(gca,'yscale','log');
      end
      set(gca,'XTick',ticks)
      set(gca,'YTick',ticks)
      pretty;
      print('-dpng',[params.plotdir '/bicoherence']);
      print('-depsc2',[params.plotdir '/bicoherence']);
      close;

      figure;
      hC = pcolor(X,Y,map.bicoherence_pvalue');
      set(gcf,'Renderer','zbuffer');
      colormap(jet)
      shading interp
      t = colorbar('peer',gca);
      set(get(t,'ylabel'),'String','1 - cdf(coherence^2)');
      set(t,'fontsize',25);
      set(hC,'LineStyle','none');
      grid
      set(gca,'clim',[0 1])
      %set(gca,'clim',[0.999 1])
      if ~isnan(min(min(map.bicoherence_pvalue))) && ~isnan(max(max(map.bicoherence_pvalue)))
         set(gca,'clim',[min(min(map.bicoherence_pvalue)) max(max(map.bicoherence_pvalue))])
      end
      xlabel(sprintf('Frequency [Hz] [%s]',strrep(params.darmChannel,'_','\_')))
      ylabel(sprintf('Frequency [Hz] [%s]',strrep(params.pemChannel,'_','\_')));
      if doLog
         set(gca,'xscale','log');
         set(gca,'yscale','log');
      end
      set(gca,'XTick',ticks)
      set(gca,'YTick',ticks)
      pretty;
      print('-dpng',[params.plotdir '/bicoherence_pvalue']);
      print('-depsc2',[params.plotdir '/bicoherence_pvalue']);
      close;

      figure;
      hC = pcolor(X,Y,map.bicoherence_threshold');
      set(gcf,'Renderer','zbuffer');
      colormap(jet)
      shading interp
      t = colorbar('peer',gca);
      set(get(t,'ylabel'),'String','Coherence');
      set(t,'fontsize',25);
      set(hC,'LineStyle','none');
      grid
      set(gca,'clim',[0 0.1])
      %set(gca,'clim',[0.999 1])
      %set(gca,'clim',[min(min(map.bicoherence_pvalue)) max(max(map.bicoherence_pvalue))])
      xlabel(sprintf('Frequency [Hz] [%s]',strrep(params.darmChannel,'_','\_')))
      ylabel(sprintf('Frequency [Hz] [%s]',strrep(params.pemChannel,'_','\_')));
      if doLog
         set(gca,'xscale','log');
         set(gca,'yscale','log');
      end
      set(gca,'XTick',ticks)
      set(gca,'YTick',ticks)
      pretty;
      print('-dpng',[params.plotdir '/bicoherence_threshold']);
      print('-depsc2',[params.plotdir '/bicoherence_threshold']);
      close;

      figure;
      if doLog
         loglog(map.f,map.bicoherence(:,1),'b')
      else
         semilogy(map.f,map.coherence(:,1),'b')
      end
      xlabel('f (Hz)')
      ylabel('Coherence (first bin)')
      xlim([min(map.f),max(map.f)])
      pretty;
      print('-dpng',[params.plotdir '/bicoherence_firstbin']);
      print('-depsc2',[params.plotdir '/bicoherence_firstbin']);
      close;
   end

   if params.doFrequencyBins 
      stamp_pem_frequencybins(params,map);
   end
end 

return
