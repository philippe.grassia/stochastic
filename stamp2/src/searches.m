function [stoch_out, ss] = searches(map, params, kk, ss, stoch_out, pp)
% function [stoch_out] = searches(map, params, kk, ss)
% E. Thrane: this script calls different search algorithms depending on user-
% specified variables
% 
% kk: search direction
% ss: search number
% stoch_out: struct returned by clustermap
% map: struct containing f-tmaps
% params: parameter struct

% burstegard: a clustering algorithm inspired by burstCluster
% but faster, more efficient, and better at finding clusters
if params.doBurstegard
  ss=ss+1;
  % the zebragard option is in development
  if params.doZebragard
    %fprintf('WARNING: zebragard is in development!\n');
    fprintf('zebragard cluster search for direction %1.0f\n', kk);
    max_cluster = zebratrack(map, params);
  else
    % the standard targeted burstegard search
    fprintf('burstegard cluster search for direction %1.0f\n', kk);
    max_cluster = run_burstegard(map,params);
  end
  % record results
  stoch_out.max_SNR(ss) = max_cluster.snr_gamma;
  stoch_out.search{ss} = ['bgcluster' num2str(kk)];
  stoch_out.cluster = max_cluster;

  % in specific cases, add new fields to the stoch_out structure
  if params.doAllClusters
     stoch_out = doAllClusters(stoch_out,params,map,max_cluster,ss);
  end
end

% the Radon algorithm - not currently maintained
if params.doRadon
  ss=ss+1;
  map0 = clean_maps(map, maskindex);
  warning('NaN values removed from map struct for Radon search.');
  % p, estimated p-value, is currently not used for anything
  % same goes for Y_Gamma_max, the strain power in the loudest cluster
  [max_SNR, p, Y_Gamma_max] = searchRadon(params, map0);
  stoch_out.max_SNR(ss) = max_SNR;
  stoch_out.search{ss} = 'Radon';
end

% box search 
if params.doBoxSearch
  ss=ss+1;
  max_cluster = searchBox(params, map);
  stoch_out.max_SNR(ss) = max_cluster.snr_gamma;
  stoch_out.search{ss} = 'box';
  stoch_out.cluster = max_cluster;
end

% Locust and Hough algorithms - not currently maintained
if params.doLH
  ss=ss+1;
  searchLH(params, map);
  stoch_out.max_SNR(ss) = 1e99;  % temporary assignment of SNR
  stoch_out.search{ss} = 'LH';
end

% burstCluster aka pixelCluster algorithm - last in widespread use in March, 
% 2012 before being largely replaced by burstegard
if params.doClusterSearch
  ss=ss+1;
  fprintf('cluster search for direction %1.0f\n', kk);
  max_cluster = searchClusters(params, map);
  stoch_out.max_SNR(ss) = max_cluster.snr_gamma;
  stoch_out.search{ss} = ['cluster' num2str(kk)];
  stoch_out.cluster = max_cluster;
end

if params.stamp_pem
   output = stamp_pem_line_glitch(map,params);  
   stoch_out.line_glitch = output;

   map0 = map;
   map0.y = map0.P1_log10;
   map0.sigma = ones(size(map0.sigma));
   stoch_out.P1_cluster = run_burstegard(map, params);

   map0.y = map0.P2_log10;
   map0.sigma = ones(size(map0.sigma));
   stoch_out.P2_cluster = run_burstegard(map, params);

end

if params.doStochtrack & ~params.stochtrack.doViterbi
  tic;
  ss = ss + 1;
  if params.stochtrack.lonetrack
    fprintf('lonetrack search for direction %1.0f\n', kk);
    max_cluster = lonetrack(map,params,pp);
    stoch_out.lonetrack = max_cluster.lonetrack;
  elseif params.stochtrack.singletrack.doSingletrack
    fprintf('singletrack search for direction %1.0f\n', kk);
    max_cluster = singletrack(map,params);
    max_cluster.timedelay = 0;
  else
    fprintf('stochtrack search for direction %1.0f\n', kk);
    max_cluster = stochtrack(map,params);
    max_cluster.timedelay = 0;
  end
  stoch_out.max_SNR(ss) = max_cluster.snr_gamma;
  stoch_out.search{ss} = ['stochtrack' num2str(kk)];
  stoch_out.NE = NullEnergy(map,max_cluster.reconMax>0,1);
  stoch_out.cluster = max_cluster;
  stoch_out.tmin=max_cluster.tmin;
  stoch_out.tmax=max_cluster.tmax;
  stoch_out.fmin=max_cluster.fmin;
  stoch_out.fmax=max_cluster.fmax;
  stoch_out.timedelay=max_cluster.timedelay;
  stoch_out.SNRfrac=max_cluster.SNRfrac;
  stoch_out.SNRfracTime=max_cluster.SNRfracTime;
  stoch_out.NbPix=max_cluster.nPix;
  toc;
end

if params.doStochtrack & params.stochtrack.doViterbi
  tic;
  ss = ss + 1;
  fprintf('viterbi search for direction %1.0f\n', kk);
  max_cluster = viterbi(map,params);
  stoch_out.max_SNR(ss) = max_cluster.snr_gamma;
  stoch_out.search{ss} = ['stochtrack' num2str(kk)];
  stoch_out.NE = NullEnergy(map,max_cluster.reconMax>0,1);
  stoch_out.cluster = max_cluster;
  stoch_out.tmin=max_cluster.tmin;
  stoch_out.tmax=max_cluster.tmax;
  stoch_out.fmin=max_cluster.fmin;
  stoch_out.fmax=max_cluster.fmax;
  stoch_out.timedelay=max_cluster.timedelay;
  stoch_out.SNRfrac=max_cluster.SNRfrac;
  stoch_out.SNRfracTime=max_cluster.SNRfracTime;
  stoch_out.NbPix=max_cluster.nPix;
  toc;
end

return
