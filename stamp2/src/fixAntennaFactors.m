function g = fixAntennaFactors(g,N)
% function g = fixAntennaFactors(g)
% Fixes antenna factors to values for H1L1 for (ra,decl)=(6,30) and lst=8.5546.
% This combination of direction and time produces adecent sensitivity to plus
% and cross polarizations.  For optimal sensitivity, use bestAntennaFactors.

  g.F1p = 0.3643*ones(1,N);
  g.F1x = 0.4085*ones(1,N);
  g.F2p = -0.7010*ones(1,N);
  g.F2x = -0.2667*ones(1,N);
  g.gamma0 = -0.1822*ones(1,N);

return
