function params = stampDefaults(params)
% function params = stampDefaults(params)
% Sets default parameters for clustermap by filling the param struct.
% The params struct must be defined in order to call clustermap.
% by E. Thrane
%
% Note: if you add/remove parameters from stampDefaults, please make sure to 
% make the corresponding change in paramCheck--the code that checks to see if
% all parameters have been set.  Also, note that the parameters are organized
% into categories and the ordering of the categories is the same in these two
% routines.  When making modifications, please take care to put parameters in 
% a sensible location and to maintain the order with paramCheck.m.

%------------------------------------------------------------------------------
% Basic options
%------------------------------------------------------------------------------
% are mat files available or should preproc be run to generate the data?
params.matavailable = true;
% default name for output mat files
params.outputfilename = 'map';
% default name for output plot folder
params.plotdir = './';
% define typical ranges for ft-map plots.
params.yMapScale = 1e-43;
% return map struct as stoch_out.map?
params.returnMap = false;

%------------------------------------------------------------------------------
% Options related to data cleaning in time and frequency domains
%------------------------------------------------------------------------------
params.doTFNotch = false;

% to keep/remove gaps in the data, when the start and end GPS times span more 
% than one job (also the gaps in the beginning and at the end); useful for 
% background estimation; default is true
params.gap = true;

% Glitch struct
% doCut turns on the glitch identification algorithm
params.glitch.doCut = false;
% number of frequency bands to use for cut (default is 1)
params.glitch.numBands = 1;
% new cut for coincident glitches (ask Tanner)
params.glitch.doCoincidentCut = false;
% Glitch cut parameters
% Xi_min and Xi_max define range in Xi_snr for "glitchy" pixels.
% if Xi_min < X < Xi_max or -Xi_min > X > -Xi_max, pixel is glitchy.
params.glitch.Xi_min = 0.93;
params.glitch.Xi_max = 1.07;
% Xi_frac defines required fraction of glitchy pixels in a column
% for it to be cut
params.glitch.Xi_frac = 0.027;
% AP_cut defines mean autopower "SNR" required for
% column to have "significant power." If one detector has this, then it's
% probably a glitch, if both detectors have this, it's probably a signal,
% so we don't veto it.
params.glitch.AP_thresh = 2;

% coarse grain map
params.doCoarseGrainMap = 0;

%------------------------------------------------------------------------------
% Output options
%------------------------------------------------------------------------------
% save a mat file
params.saveMat=false;
% save eps and png images of ft-map plots.
params.savePlots=true;
% displays verbose error messages
params.debug=false;

%------------------------------------------------------------------------------
% Search algorithms
%------------------------------------------------------------------------------
% Radon transform
params.doRadon=false;
params.doRadonReconstruction=false;
% box search
params.doBoxSearch=false;
% Locust and Hough search
params.doLH=false;
% density-based clustering (burstCluster)
params.doClusterSearch=false;  
% crunch_map is an option for burstCluster
params.crunch_map = false;
% burstegard search
params.doBurstegard = false;
% zebragard variation
params.doZebragard = false;
% stochtrack
params.doStochtrack = false;
% viterbi
params.doViterbi = false;

%------------------------------------------------------------------------------
% All-sky options
%------------------------------------------------------------------------------
% loop over sky positions in a search cone
params.skypatch=false;
% loop over only directions with different phases at fmax
params.fastring=false;
% burstegard returns list of SNR for all the clusters
params.doAllClusters=false;
%------------------------------------------------------------------------------
% injection options
%------------------------------------------------------------------------------
% add injected power to map
params.powerinj=false;
% begin injection at fixed time
params.fixedInjectionTime=false;
% keep injection at a fixed sky location
params.fixedInjectionLocation=false;
% when to perform the injection if and only if fixedInjectionTime==true
fixedInjectionTimeGPS=-1;
% number of trials due to injection studies
params.inj_trials=1;
% injection increment
params.increment=1.2;
% injection increment type (distance or alpha)
params.incrementType='alpha';

%------------------------------------------------------------------------------
% glitch injection options
%------------------------------------------------------------------------------
% add glitch to map
params.glitchinj=false;

%------------------------------------------------------------------------------
% anteproc options
%------------------------------------------------------------------------------
% load .mat files produced by anteproc?
params.anteproc.loadFiles = false;
% timeshifts
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;
% do timeshift by jobFile instead?
params.anteproc.jobFileTimeShift = false;
% do background study (remove columns of NaNs separating chunks of data)?
params.anteproc.bkndstudy = false;
% Set directories for loading anteproc'd .mat files to [] by default.
[params.anteproc.inmats1, params.anteproc.inmats2] = deal([]);
% Use a cachefile to find anteproc'd .mat files?
params.anteproc.useCache = false;
% Injection done in a single window
params.injectionWindowIndex=-1;
%------------------------------------------------------------------------------
% parameter estimation options
%------------------------------------------------------------------------------

params.doPE = false;

%------------------------------------------------------------------------------
% diagnostic options and other less common options
%------------------------------------------------------------------------------
% multiply CSD by array of random phases
params.phaseScramble = false;
% randomly scrambles ft-map pixels
params.pixelScramble = false;
% {F} set to values for (ra,dec,lst) = (6,30,8.55)
params.fixAntennaFactors=false;
% {F} set for ideal sky location for H1L1 circularly polarized source
params.bestAntennaFactors=false;
% Artificially set time-delay between IFOs to zero.
params.NoTimeDelay=false;
% background study
params.bkndstudy=false;
% compare with radiometer algorithm
params.doRadiometer=false;
% use a polarized filter
params.doPolar=false;
% polarized injection
params.doPolarInjection=false;
% use a pure plus filter
params.purePlus=false;
% use a pure cross filter
params.pureCross=false;
% use a circularly polarized filter
params.circular=false;
% do not use imaginary filter in csnr_v3
params.no_imaginary_polarized_filter=false;
% stamp_pem option allows for strain - PEM correlation for detchar studies
params.stamp_pem = false;
% long segments should be used when analyzing data that contains (or might 
% contain segments with duration >1
params.longsegs = false;
% alternative_sigma splits ft-maps in two parts and swap variances map to optimize SNR
% calculation, especially for monochromatic signals.
params.alternative_sigma = false;
% mask a cluster from a .mat file
params.maskCluster = false;

%------------------------------------------------------------------------------
% STAMP-PEM options
%------------------------------------------------------------------------------
params.stamp_pem = false;
params.pemPSD = false;

return
