% MPIIR_FILTER_LINE - subtract a static frequency line from a data
% stream
% Ed Daw, 31st August 2011
%
% useage: [<data_out>, <filter_state_out>] =
%            iwave_track_line(<data_in>, <number of data points>,
%            <filter state in>)
%
% here the arrays data_in and data_out is a double
% precision vectors all of the same number of elements, nelements
% is that number of elements , and filter_state_in and
% filter_state_out are arrays of doubles defining the filter state.
% The first time the filter is called, filter_state_in should have
% previously been initialized using miwave_construct.
