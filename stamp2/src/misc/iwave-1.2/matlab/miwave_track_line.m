% MPIIR_TRACK_LINE - returns arrays containing estimates of the line
% amplitude as a function of time in the I phase (in phase with the 
% line of the input data) and Q phase (90 degrees out of phase).
% Ed Daw, 30/08/2011 e.daw@shef.ac.uk
%
% useage: [<data_out_Iphase>, <data_out_Qphase>, <filter_state_out>] =
%            iwave_track_line(<data_in>, <number of data points>,
%            <filter state in>)
%
% here the arrays data_out_Iphase, data_out_Qphase are double
% precision vectors all of the same number of elements, nelements
% is that number of elements , and filter_state_in and
% filter_state_out are arrays of doubles defining the filter state.
% The first time the filter is called, filter_state_in should have
% previously been initialized using miwave_construct.
