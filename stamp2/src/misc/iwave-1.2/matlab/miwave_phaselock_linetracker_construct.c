/* wrapper code for MATLAB to access a simple, non frequency tracking */
/* iwave line monitor */
/* Ed Daw, 30th August 2011 */
#include <mex.h>
#include <iwave.h>
#include <stdio.h>

void mexFunction( int nlhs,
		  mxArray *plhs[], 
		  int nrhs, 
		  const mxArray *prhs[]) {

  double fs;
  double fline;
  double tau;
  double tstart;
  double* outdata;

  fs = (double)(*((double*)mxGetPr(prhs[0])));
  fline = (double)(*((double*)mxGetPr(prhs[1])));
  tau = (double)(*((double*)mxGetPr(prhs[2])));
  tstart = (double)(*((double*)mxGetPr(prhs[3])));
  plhs[0]=mxCreateDoubleMatrix(1,iwave_phaselock_size(),mxREAL);
  outdata=mxGetPr(plhs[0]);
  iwave_phaselock_linetracker_construct(fs, tau, fline, tstart, outdata);
  
  return;
}
