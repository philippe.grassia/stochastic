% MPIIR_QPHASE_DETECTOR_CONSTRUCT - a constructor for a line amplitude
% and phase fluctuation monitor.
% Ed Daw, 31st August 2011
%
% useage: <fstate>=miwave_qphase_detector_construct(
%                    <sampling frequency (Hz)>,
%                    <line frequency (Hz)>,
%                    <response time (s)>)
%
