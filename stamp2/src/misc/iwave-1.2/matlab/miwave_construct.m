% MPPIR_CONSTRUCT - returns the state data for a non frequency
% tracking PIIR filter.
% Ed Daw, 30/08/2011 e.daw@shef.ac.uk
%
% useage: <state data vector> = 
%                    miwave_construct( <sampling rate (Hz)>,
%                                     <line frequency (Hz)>,
%                                     <response time (s)> )

