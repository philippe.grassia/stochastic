/* header for iwave complex IIR line filter */
/* Ed Daw, 12th August 2011                 */

#ifdef __cplusplus
extern "C" {
#endif

#define IWAVEPI 3.141592653589793238
#define STATESIZE 15
#define PHASESIZE 14
#define PHASELOCKSTATESIZE (STATESIZE+PHASESIZE)
#define PHASELOCKSTARTOFFSET (STATESIZE)

/* state vector for the phase locked tracker */
typedef enum {loopopen, loopclosed} feedbackstate; 

/* return the size of the state vector for a simple IWAVE filter */
int iwave_notrack_size(void);

/* return the size of the state vector for a quad phase IWAVE mixer filter */
int iwave_qphase_size(void);

/* return the size of the state vector for a phaselocked IWAVE filter */
int iwave_phaselock_size(void);

/* initialise a iwave filter specifically for narrow lines */
int iwave_construct(double fs_in, 
		   double tau_in, 
		   double fline_in,
		   double fstate[]);

/* just the core IIR algorithm, without the matrix multiplication */
int iwave_core(double data_in[],
	      double data_out_iphase[],
	      double data_out_qphase[],
	      int ndata,
	      double fstate[]);

/* apply iwave filter specifically for narrow lines to a data array*/
int iwave_track_line(double data_in[],
		    double data_out_iphase[],
		    double data_out_qphase[],
		    int ndata,
		    double fstate[]);

/* use simple feedback from iphase to make a IWAVE oscillator */
int iwave_oscillator(double data_out[],
		    int ndata,
	            double fstate[]);

/* use iwave filter to remove a line */
int iwave_filter_line(double data_in[],
		     double data_out[],
		     int ndata,
		     double fstate[]);

/* initialize a pair of iwave filters to track phase jitter in */
/* narrow lines. One filter detects and synthesizes the line */
/* at frequency fs_in, the other filters out the 2*fs_in component */
/* in the product of the q-phase output of the first filter and */
/* the data stream */
int iwave_qphase_detector_construct(double fs_in,
				   double tau_in,
				   double fline_in,
				   double fstate[]);

/* reconstruct the amplitude of the synthsized tracking */
/* oscillator, and the phase shift at timescales shorter */
/* than tau of the input data with respect to the tracker */
int iwave_qphase_mix(double data_in[],
		    double data_intermed[],
		    double qmix_out[],
		    double amp_out[],
		    int ndata,
		    double fstate[]);

/* modify coefficients of a iwave filter for shift in frequency */
int iwave_filter_alterstate(double delta_phi, 
			   double fstate[]);

/* modify the feedback phase shift per sample, and the frequency */
int iwave_phaselock_alter_filters(double ddelta,
				 double fstate[]);

/* modify the feedback phase shift per sample, and the frequency, */
/* with hard endstops on the allowed frequency */
int iwave_phaselock_alter_filters_limitf(double ddelta,
					double fstate[]);

/* construct a phase locked loop */
int iwave_phaselock_linetracker_construct(double fs_in,
					 double tau_in,
					 double fline_in,
					 double starttime,
					 double fstate[]);
 
/* apply a phase locked loop to some data */
int iwave_phaselock_linetracker_iterate(double indata[],
				       double qmix_out[],
				       double amp_out[],
				       double freq_out[],
				       double filtered_out[],
				       int ndata,
				       double fstate[]);
  
/* construct a phase locked loop with end stops */
int iwave_phaselock_linetracker_limitf_construct(double fs_in,
						double tau_in,
						double fline_in,
						double starttime,
						double f_lowerlimit,
						double f_upperlimit,
						double fstate[]);
  
/* apply a frequency limited phase locked loop to some data */ 
int iwave_phaselock_linetracker_limitf_iterate(double indata[],
					      double qmix_out[],
					      double amp_out[],
					      double freq_out[],
					      double filtered_out[],
					      int ndata,
					      double fstate[]);

#ifdef __cplusplus
}
#endif
