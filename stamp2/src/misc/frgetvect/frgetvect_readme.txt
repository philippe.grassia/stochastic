This version of frgetvect is copied from ligotools/ during Feb 2012.

It is kept here because there are multiple files with the same names in both
ligotools/ and matapps.  Examples include frgetvect and mkframe.  matlab is not
very flexible when it comes to chosing which of several identically named 
scripts to use, so to make this easier, we have just copied frgetvect from
ligotools.
