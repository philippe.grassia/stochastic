function response_return = filter_response(map, params)
% filter_response
% Given a stamp parameter struct and map struct determines the response of the preproc filter
% used for determinging response of stamp filter

T = 64;
fs = 4096;
mc = [];
mc.transfer(:,1) = map.f;
mc.transfer(:,2) = ones(size(map.f));

%vector = create_noise(fs,T,mc);
averages = 100;
t = 0:(1/fs):T;

window = hann(fs*T);

for j = 1:averages

   %vector = create_noise(fs,T,mc);
   vector = gaussian_noise(mc.transfer,fs,T,1)';
   vector = vector.*window';

   highPassOrder = 16;
   highPassFreq = 9;
   
   if highPassOrder < 6
      [b,a] = butter(highPassOrder, highPassFreq/(fs/2), 'high');
      y = filtfilt(b,a,vector);
   else
      y = cascadefilter(vector, params.stochtrack.filter.highPassOrder, ...
         params.stochtrack.filter.highPassFreq, fs);
   end

   L = length(t);
   NFFT = 2^nextpow2(L); % Next power of 2 from length of y
   f = fs/2*linspace(0,1,NFFT/2+1);

   if j == 1
      total_single_sided_filtered = zeros(length(f),1);
      total_single_sided_unfiltered = zeros(length(f),1);
      total_response = zeros(length(f),1);
   end

   sT = floor(NFFT/fs);
   windowconst = 0.375;

   Y = fft(vector,NFFT);
   single_sided_unfiltered  = 2*abs(Y(1:NFFT/2+1));
   single_sided_unfiltered = 2*(abs(Y(1:NFFT/2+1)).^2)/(L*fs*windowconst);
   %single_sided_unfiltered = sqrt(single_sided_unfiltered);
   Y = fft(y,NFFT);
   single_sided_filtered = 2*abs(Y(1:NFFT/2+1));
   single_sided_filtered = 2*(abs(Y(1:NFFT/2+1)).^2)/(L*fs*windowconst);
   %single_sided_filtered = sqrt(single_sided_filtered);

   response = single_sided_filtered ./ single_sided_unfiltered;
   total_single_sided_filtered = total_single_sided_filtered + single_sided_filtered';
   total_single_sided_unfiltered = total_single_sided_unfiltered + single_sided_unfiltered';
   total_response = total_response + response';
end
   
single_sided_filtered = total_single_sided_filtered / averages;
single_sided_unfiltered = total_single_sided_unfiltered / averages;
response = total_response / averages;

response_return = interp1(f,response,map.f);

