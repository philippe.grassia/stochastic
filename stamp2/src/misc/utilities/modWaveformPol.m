function [hp_out hx_out] = modWaveformPol(params,hp_in,hx_in)
% function [hp_out hx_out] = modWaveformPol(params,hp_in,hx_in)
% Uses input waveform(s) to calculate elements of GW metric perturbation
% for arbitrary polarization angles.  Uses iota, psi in degrees.
% This code assumes that if you are going to modify a waveforms
% polarization, the input waveform has iota = 0, psi = 0.
%
% Written by: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check if injection type (params.pass.stamp.inj_type) is one that
% has been studied. If it is not, do not modify waveform.
% Currently understood: ADI, CW

if ~isempty(strfind(params.pass.stamp.inj_type,'adi')) || ~isempty(strfind(params.pass.stamp.inj_type,'cw')) || (~isempty(strfind(params.pass.stamp.inj_type,'fly')) && isfield(params.pass.stamp, 'fly_waveform') && (strcmp(params.pass.stamp.fly_waveform, 'half_sg') || strcmp(params.pass.stamp.fly_waveform, 'ringdown')))
  fprintf('Waveform type: %s.\n',params.pass.stamp.inj_type);
  if ~isempty(strfind(params.pass.stamp.inj_type,'fly')) && isfield(params.pass.stamp, 'fly_waveform') && (strcmp(params.pass.stamp.fly_waveform, 'half_sg') || strcmp(params.pass.stamp.fly_waveform, 'ringdown'))
    fprintf('On the fly waveform type: %s.\n',params.pass.stamp.fly_waveform);
  end
  fprintf('Iota: %.4f, Psi: %.4f.\n',params.pass.stamp.iota,params.pass.stamp.psi);

  % New polarization angles, convert to degrees.
  iota = params.pass.stamp.iota;
  phi = -1*params.pass.stamp.psi;

  % Calculate metric components in source frame.
  if ~isempty(strfind(params.pass.stamp.inj_type,'adi'))
    h1122 = 2*hp_in; % h11 - h22
    h12 = hx_in;
    h112233 = 0; % h11 + h22 - 2*h33 = h11 + h22 = 0
    h13 = 0;
    h23 = 0;
  elseif ~isempty(strfind(params.pass.stamp.inj_type,'cw'))
    h1122 = 2*hp_in; % h11 - h22
    h12 = hx_in;
    h112233 = 0; % h11 + h22 - 2*h33 = h11 + h22 = 0
    h13 = 0;
    h23 = 0;
  elseif ~isempty(strfind(params.pass.stamp.inj_type,'fly')) && isfield(params.pass.stamp, 'fly_waveform') && (strcmp(params.pass.stamp.fly_waveform, 'half_sg') || strcmp(params.pass.stamp.fly_waveform, 'ringdown'))
    h1122 = 2*hp_in; % h11 - h22
    h12 = hx_in;
    h112233 = 0; % h11 + h22 - 2*h33 = h11 + h22 = 0
    h13 = 0;
    h23 = 0;
%  elseif strcmpi(params.pass.stamp.inj_type,'ebbh') % ebbh
    % load other 2 basis waveforms
    % should have 45_0 and 45_90 elements
    % calculate matrix elements    
  else
    error('Error in modWaveformPol.m: injection type not found.');
  end

  % Calculate pattern functions (from inclination angle).
  Ap = (1+cosd(iota)^2)/2;
  Ax = cosd(iota);

  % Calculate hplus and hcross for new polarization.
  hp_out = (h1122/2)*Ap*cosd(2*phi) - (h112233/2)*((sind(iota)^2)/2)*cosd(2*phi) - ...
	   h12*Ax*sind(2*phi) + (h13/2)*sind(2*iota)*cosd(2*phi) - ...
	   h23*sind(iota)*sind(2*phi);

  hx_out = (h1122/2)*Ap*sind(2*phi) - (h112233/2)*((sind(iota)^2)/2)*sind(2*phi) +...
 	   h12*Ax*cosd(2*phi) + (h13/2)*sind(2*iota)*sind(2*phi) +...
	   h23*sind(iota)*cosd(2*phi);

else
  hp_out = hp_in;
  hx_out = hx_in;
end


return;
