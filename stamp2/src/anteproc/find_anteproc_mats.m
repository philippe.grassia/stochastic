function [matlist,out_params] = find_anteproc_mats(params)
% function matlist = find_anteproc_mats(params)
%
% Finds .mat files produced by anteproc that contain data
% corresponding to the times specified by the user in the
% params struct. 
%
% The user must specify the directories containing the
% .mat files (params.inmats1 & params.inmats2) and any
% timeshift they want to apply (params.timeShift1 &
% params.timeshift2).  If no timeshift is desired, the
% user should set these parameters to 0.
%
% Output:
% matlist - struct with two members: ifo1 and ifo2. Each
%           member is a cell array containing the full
%           path to each .mat file containing some part
%           of the desired data.
%
% Based On Findmats.m.
% Written by T. Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check if doing jobfile time-shifts.
if params.anteproc.jobFileTimeShift
  % Make sure job numbers are defined.
  if sum(~isfield(params.anteproc,{'jobNum1','jobNum2'}))
    error(['Must define params.anteproc members ''jobNum1'' and ''jobNum2''.']);
  else
    jobnums = [params.anteproc.jobNum1 params.anteproc.jobNum2];

    % load jobfile and get GPS times.
    jobfile = load(params.anteproc.jobfile);
    params.anteproc.startGPS = jobfile(jobnums,2);
    params.anteproc.endGPS = jobfile(jobnums,3);
  end
else
  % If not doing jobfile time-shifts, use user-specified time-shifts.
  % Check if time-shift variables are defined.
  if sum(~isfield(params.anteproc,{'timeShift1','timeShift2'}))
    error(['Must define members of params.anteproc: timeShift1 and' ...
	   ' timeShift2.']);
  end

  % Apply time-shifts.
  params.anteproc.startGPS(1) = params.startGPS - params.anteproc.timeShift1;
  params.anteproc.startGPS(2) = params.startGPS - params.anteproc.timeShift2;
  params.anteproc.endGPS(1) = params.endGPS - params.anteproc.timeShift1;
  params.anteproc.endGPS(2) = params.endGPS - params.anteproc.timeShift2;
end


% Define structs for ease of coding.
if ((isempty(params.anteproc.inmats1) | isempty(params.anteproc.inmats2)) & ...
    ~params.anteproc.useCache)
  warning(['You are not using a cachefile, but inmats1 and/or inmats2 is not' ...
	   ' defined.']);
end

% Loop over ifos.  Should always be exactly 2 ifos,
% but this can be modified if needed.
inmats = {params.anteproc.inmats1 params.anteproc.inmats2};
for ii=1:2

  % Set up start and end GPS times, and struct member name.
  startGPS = params.anteproc.startGPS(ii);
  endGPS = params.anteproc.endGPS(ii);
  ifo = ['ifo' num2str(ii)];
  file_list.(ifo) = cell(0);

  if params.anteproc.useCache
    M = load(params.anteproc.cacheFile);
    file_list.(ifo) = M.matlist.(ifo);
    gps_starts.(ifo) = M.matlist.(['gps' num2str(ii)]);
    durs.(ifo) = M.matlist.(['durs' num2str(ii)]);
  else
    startdirtime = floor(startGPS/10000) - 1;
    enddirtime = floor(endGPS/10000);
    dirtimes = startdirtime:enddirtime;
    % Check available directories and get qualifying dirtimes.
    av_dirs = dir([inmats{ii} '-*']);
    av_dirtimes = regexp({av_dirs.name},'-(\d+)$','tokens','once');
    
    
    if isempty(av_dirtimes)
      dirtimes = [];
    else
      av_dirtimes = cellfun(@(x) str2num(x{1}),av_dirtimes);
      dirtimes = intersect(dirtimes,av_dirtimes);
    end

    % Loop over directories.
    for jj=1:length(dirtimes)
      matdir = [inmats{ii} '-' num2str(dirtimes(jj))];
      temp = dir([matdir '/*.mat']);
      temp = strcat([matdir '/'],{temp.name}); % add path prefix
      file_list.(ifo) = [file_list.(ifo) temp];
    end
    temp = regexp(file_list.(ifo),'-(\d{9,10}(|\.\d+))-','tokens','once');
    gps_starts.(ifo) = cellfun(@(x) str2num(x{1}), temp);
    temp = regexp(file_list.(ifo),'-(\d+|\d+\.\d+)\.mat','tokens','once');
    durs.(ifo) = cellfun(@(x) str2num(x{1}), temp);
  end

  % Compile list of GPS start times and durations.
  %temp = regexp(file_list.(ifo),'-(\d{9,10}(|\.\d+))-','tokens','once');
  %gps_starts.(ifo) = cellfun(@(x) str2num(x{1}), temp);
  %temp = regexp(file_list.(ifo),'-(\d+|\d+\.\d+)\.mat','tokens','once');
  %durs.(ifo) = cellfun(@(x) str2num(x{1}), temp);

  % GPS end times.
  % NOTE: these time correspond to the END of the last segment stored
  %       in each .mat file.
  gps_ends.(ifo) = gps_starts.(ifo) + durs.(ifo);

  % Make list of files that have data that falls in the desired times.
  keep.(ifo) = (gps_starts.(ifo) < endGPS & gps_ends.(ifo) > startGPS);
  matlist.(ifo) = file_list.(ifo)(keep.(ifo));
  matlist.(['gps' num2str(ii)]) = gps_starts.(ifo)(keep.(ifo));
  matlist.(['durs' num2str(ii)]) = durs.(ifo)(keep.(ifo));
end

% return params.
out_params = params;

return;
