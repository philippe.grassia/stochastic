%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters for stochastic search (name/value pairs) %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fix antenna factors
fixAntennaFactors false

% random number seed
pp_seed -1

% ifo names
ifo1 H1

% segment duration (sec)
segmentDuration 100

% duration of hann portion of tukey window 
% (hannDuration = segmentDuration is a pure hann window)
hannDuration1 100

% freq resolution and freq cutoffs for CC statistic sum (Hz)
flow 40
fhigh 1800
deltaF 1

% parameters for sliding psd estimation:
% numSegmentsPerInterval should be odd; ignoreMidSegment is a flag 
% that allows you to ignore (if true) or include (if false) the 
% analysis segment when estimating power spectra
numSegmentsPerInterval 9
ignoreMidSegment true

% ASQ channel
ASQchannel1 LSC-STRAIN

% frame type and duration
frameType1 H1_HOFT_C02
frameDuration1 -1

% MC noise
doDetectorNoiseSim false
DetectorNoiseFile /home/prestegard/matapps/packages/stochastic/trunk/PreProcessing/IGOsrdPSD_40Hz.txt
%DetectorNoiseFile ALIGO-HighP_PSD_10Hz.txt

% path to cache files
gpsTimesPath1 ./cachefiles/
frameCachePath1 ./cachefiles/
cacheFile /home/sharan.banagiri/cache.mat

% output filename prefix
outputFilePrefix 

% output path for .mat files
outputfiledir ./mats/
outputfilename H-H1_STAMPAS
% CIT nodes
%outputfiledir /usr1/prestegard/mats/
% ATLAS nodes
%outputfiledir /local/user/prestegard/mats/

stochmap true
mapsize 4000
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Things I never change %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% flags for optional operations
doFreqMask false
doHighPass1 true
doOverlap true

doSidereal false
minDataLoadLength 200
 
doBadGPSTimes false
%badGPSTimesFile 
maxDSigRatio 1.2
minDSigRatio 0.8

% params for Omega_gw (power-law exponent and reference freq in Hz)
alphaExp 0
fRef 100

% resample rate (Hz)
resampleRate1 4096

% buffer added to beginning and end of data segment to account for
% filter transients (sec)
bufferSecs1 2

% params for matlab resample routine
nResample1 10
betaParam1 5

% params for high-pass filtering (3db freq in Hz, and filter order) 
highPassFreq1 32
highPassOrder1 6

% coherent freqs and number of freq bins to remove if doFreqMask=true;
% NOTE: if an nBin=0, then no bins are removed even if doFreqMask=true
% (coherent freqs are typically harmonics of the power line freq 60Hz
% and the DAQ rate 16Hz)
freqsToRemove 1799
nBinsToRemove 1
 
% calibration filenames
alphaBetaFile1 none
calCavGainFile1 none
calResponseFile1 none

% stochastic test
simOmegaRef1 0
heterodyned false
