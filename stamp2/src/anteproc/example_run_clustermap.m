function example_run_clustermap()
% NOTE: anteproc params are near the bottom.

% output file prefix
str ='test';

% other
stoch_out.start=true;
job=1;

% initialize default parameters
params = stampDefaults;
params.seed = -1;
params.matavailable = true;

% input mat directory
params.inmats = './mats/map';

% GPS times
startGPS = 816065690;
endGPS = 816065700;

% search direction------------------------------------------------------------
params.ra = 6;
params.dec = 30;

% frequency range--------------------------------------------------------------
params.fmin = 100;
params.fmax = 1000;

% save mat files and diagnostic plots
params.saveMat=false;
params.savePlots=false;

% apply notches
%params.doStampFreqMask=true;
%params.StampFreqsToRemove = [119 120 121];
%params = mask_S5H1L1_1s1Hz(params);
params.Autopower = true;

% all-sky
params.skypatch = false;
params.fastring = false;
params.thetamax = 180;
params.dtheta = 0.7;

params.returnMat = false;

% perform a clustering search
params = burstegardDefaults(params);

% uncomment out the next line to apply a glitch cut
params.glitchCut = false;

% ANTEPROC PARAMS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use anteproc'd data?
params.anteproc.loadFiles = true;

% Location of anteproc'd .mat files
params.anteproc.inmats1 = '/home/prestegard/all_sky/anteproc_tests/mats/map-H1';
params.anteproc.inmats2 = '/home/prestegard/all_sky/anteproc_tests/mats/map-L1';

% Time-shift amount (if doing a "time-based" time-shift).
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;

% Do a jobfile time-shift?
params.anteproc.jobFileTimeShift = true;

% Jobfile to use.
params.anteproc.jobfile = ['/home/prestegard/all_sky/anteproc_tests/' ...
		    'bknd_jobfile.txt'];

% Load data from which job for each detector?
params.anteproc.jobNum1 = 2;
params.anteproc.jobNum2 = 6;

% Remove gaps in data?
params.anteproc.bkndstudy = true;

% How long are the maps supposed to be (in seconds)? The code double-checks
% the map size after removing gaps.
params.anteproc.bkndstudydur = 100;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% call clustermap
stoch_out = clustermap(params, startGPS, endGPS);


return;