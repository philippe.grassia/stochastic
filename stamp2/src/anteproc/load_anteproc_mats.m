function [out_map, out_params] = load_anteproc_mats(matlist,params,ifo_num)
% function [out_map, out_params] = load_anteproc_mats(matlist,params,ifo_num)
%
% Loads .mat files found by find_anteproc_mats.m and organizes all of the
% loaded data into an ft-map.  Only loads file for one detector at a time
% (specified by 'ifo_num');
%
% Based on loadmats.m.
% Written by T. Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Setup. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (ifo_num ==1)
  ts = params.anteproc.timeShift1;
elseif (ifo_num == 2)
  ts = params.anteproc.timeShift2;
else
  error('ifo_num must be 1 or 2.');
end

% Get start/end GPS times.
sGPS = params.anteproc.startGPS(ifo_num);
eGPS = params.anteproc.endGPS(ifo_num);
fn = ['ifo' num2str(ifo_num)];

% List of .mat files.
matlist = matlist.(fn);


% Determine number of .mat files to load.
nMatLoads = length(matlist);
if (nMatLoads < 1)
  error('No .mat files found.')
end

% Load data. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii=1:nMatLoads
  M=load(matlist{ii});

  % Check that the mat files include the requested band.
  if ((params.fmin < M.params.flow) || (params.fmax > M.params.fhigh))
    error('Requested frequencies out of range of mat files.');
  end
  
  % Use the first .mat file to get information about the data.
  if (ii==1)
    % Record metadata.
    out_params = M.params;

    % Initialize new ft-maps with template.
    out_map.segDur = out_params.segmentDuration;
    if params.anteproc.bkndstudy
      out_map.segstarttime = [];
      [out_map.rbartilde,out_map.naiP,out_map.P] = deal([]);
    else
      if out_params.doOverlap
        out_map.segstarttime = sGPS:(out_params.segmentDuration/2): ...
	  (eGPS - out_params.segmentDuration/2);
      else
        out_map.segstarttime = sGPS:(out_params.segmentDuration): ...
          (eGPS - out_params.segmentDuration);
      end
      T = length(out_map.segstarttime);
      out_map.rbartilde = NaN*ones(size(M.map.rbartilde,1),T);
      out_map.naiP = NaN*ones(size(M.map.naiP,1),T);
      out_map.P = NaN*ones(size(M.map.P,1),T);
    end
  end

  if params.anteproc.bkndstudy
    cut = ((M.map.segstarttime < eGPS) & (M.map.segstarttime) >= sGPS);
    out_map.segstarttime = horzcat(out_map.segstarttime,M.map.segstarttime(cut));
    out_map.rbartilde = horzcat(out_map.rbartilde,M.map.rbartilde(:,cut));
    out_map.naiP = horzcat(out_map.naiP,M.map.naiP(:,cut));
    out_map.P = horzcat(out_map.P,M.map.P(:,cut));
    out_map.realsegstarttime = out_map.segstarttime;
  else
    % Find relevant time indices from new and old ft-maps, accounting for
    % the appropriate timeshift.
    [C, new_t, old_t] = intersect(out_map.segstarttime,M.map.segstarttime);
    out_map.realsegstarttime(new_t) = M.map.segstarttime(old_t);  
    % Fill new maps,
    out_map.rbartilde(:, new_t) = M.map.rbartilde(:, old_t);
    out_map.naiP(:, new_t) = M.map.naiP(:, old_t);
    out_map.P(:, new_t) = M.map.P(:, old_t);

    % For segmentDuration > 2, sometimes segments are off by 1s relative to that requested from map
    if isempty(C) && (out_params.segmentDuration > 2)
       fprintf('Shifting segments by 1s to align with requested map...\n');
       [C, new_t, old_t] = intersect(out_map.segstarttime,M.map.segstarttime+1);
       out_map.realsegstarttime(new_t) = M.map.segstarttime(old_t);
       % Fill new maps,
       out_map.rbartilde(:, new_t) = M.map.rbartilde(:, old_t);
       out_map.naiP(:, new_t) = M.map.naiP(:, old_t);
       out_map.P(:, new_t) = M.map.P(:, old_t);
    end

  end
end

% If there params.anteproc.bkndstudy = false and there is missing data, we
% may need to adjust out_map.realsegstarttime.
if ~params.anteproc.bkndstudy
  % adjust start
  idx1 = find(out_map.realsegstarttime == 0);
  fixGPS = fliplr(idx1*(out_params.segmentDuration/2));
  out_map.realsegstarttime(idx1) = out_map.realsegstarttime(max(idx1)+1) - fixGPS;

  % adjust end
  if (numel(out_map.realsegstarttime) < numel(out_map.segstarttime))
    Norig = numel(out_map.realsegstarttime);
    Nextra = numel(out_map.segstarttime) - numel(out_map.realsegstarttime);
    gps_plus = (1:Nextra)*(out_params.segmentDuration/2);
    out_map.realsegstarttime((Norig+1):(Norig+Nextra)) = ...
	out_map.realsegstarttime(Norig) + gps_plus;
  end

end

% Remove duplicate columns and sort by GPS time.
if params.anteproc.bkndstudy
  [out_map.segstarttime,idx] = unique(out_map.segstarttime,'sorted');
  out_map.realsegstarttime = out_map.realsegstarttime(idx);
  out_map.rbartilde = out_map.rbartilde(:,idx);
  out_map.naiP = out_map.naiP(:,idx);
  out_map.P = out_map.P(:,idx);
end

% Calculate frequencies based on last .mat file.
% Note: DON'T CUT FREQUENCIES YET.  This will be done after the
% cross-correlation is completed.
numFreqs = floor((M.params.fhigh-M.params.flow)/M.params.deltaF)+1;
out_map.f = M.params.flow + M.params.deltaF*transpose([0:numFreqs-1]);

% Adjust segment start times and check number of columns.
if params.anteproc.bkndstudy
  nCols = numel(out_map.segstarttime);
  out_map.segstarttime = [0:nCols-1]*out_map.segDur/2 + ...
      out_map.segstarttime(1);
  
  % check with expected map size
  try
    N = 2/(out_map.segDur)*params.anteproc.bkndstudydur-1;
  catch
    error('Define params.anteproc.bkndstudydur to check pseudo job lengths.');
  end
  
  if (nCols ~= N)
    fprintf('N=%i, nCols=%i\n', N, nCols);
    error('Pseudo job does not have the correct length.');
  end
end

% Use last .mat file to get rbartilde info.
out_map.rbtinfo = M.map.rbtinfo;

return;
