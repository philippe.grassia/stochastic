function anteproc_prep4bknd()
% function anteproc_prep4bknd()
%
% Creates a jobfile for doing a background study using
% data pre-processed by anteproc.  Searches the specified
% directories for all available data and creates a new
% containing jobs of a user-specified length.  The new
% jobfile ignores the gaps between times of good data;
% it just stitches together the available data to make
% jobs of the desired length.  Assumes that the user
% will set params.anteproc.bkndstudy = true when doing
% the background study in order to remove gaps in the
% data.
%
% The user needs to specify the start and end GPS times
% of the available data set, the segment duration, the
% locations of the data for the two detectors, and the
% desired length of the jobs in the new jobfile.
%
% This script outputs a new jobfile for background studies
% called 'bknd_jobfile.txt'.
% 
% NEW FEATURE: also makes a jobfile for the zero-lag
% analysis (called 'zerolag_jobfile.txt'). The last
% job in a continuous stretch may not be long enough for
% a total job; in that case, it will be overlapped with
% the previous job.
%
% Users can specify the amount of overlap between jobs
% (if any) by using the 'overlap_time' parameter.
%
% Written by T. Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Users - modify these parameters. %%%%%%%%%%%%%%%%%%%%%%%
% Start and end GPS times to find data between.
params.startGPS = 816065659;
params.endGPS = 827290806;
%params.endGPS = 877591542;

% Segment duration of available data.
params.segmentDuration = 1;

% Location of data.
params.anteproc.inmats1 = './mats/map-H1';
params.anteproc.inmats2 = './mats/map-L1';

% Length of output job (seconds). Should be an integer
% multiple of the segment duration.
job_time = 100;
overlap_time = 0; % amount jobs should overlap

% Get data from cachefile or find the .mats?
useCache = false;
cacheFile = '/home/prestegard/all_sky/analysis/S5_background/anteproc_cache.mat';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Don't change these parameters!
% Useful quantities.
segDur = params.segmentDuration/2;
nSegsPerJob = job_time/segDur - 1;
nSegsOverlap = overlap_time/segDur - 1;

params.anteproc.jobFileTimeShift = false;
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;

% Find all anteproc'd .mat files with GPS times
% within the specified start and end times.
if useCache
  M = load(cacheFile);
  matlist = M.matlist;
else
  matlist = find_anteproc_mats(params);
end

% Loop over mat files corresponding to detector 1.  Calculate data
% available based on GPS start time and total length (determined from file
% names).  Make a list (gps1) of all segment START times available.
gps1 = [];
for ii=1:length(matlist.ifo1)
  gps_start = regexp(matlist.ifo1{ii},'-(\d{9,10}(|\.\d+))-','tokens','once');
  gps_start = str2num(gps_start{:});
  dur = regexp(matlist.ifo1{ii},'-(\d+|\d+\.\d+)\.mat','tokens','once');
  dur = str2num(dur{:});
  gps1 = [gps1 gps_start:segDur:(gps_start + dur - params.segmentDuration)];
end

% Do same calculation for detector 2.
gps2 = [];
for ii=1:length(matlist.ifo2)
  gps_start = regexp(matlist.ifo2{ii},'-(\d{9,10}(|\.\d+))-','tokens','once');
  gps_start = str2num(gps_start{:});
  dur = regexp(matlist.ifo2{ii},'-(\d+|\d+\.\d+)\.mat','tokens','once');
  dur = str2num(dur{:});
  gps2 = [gps2 gps_start:segDur:(gps_start + dur - params.segmentDuration)];
end

% Remove any duplicate GPS times that may occur due to possible
% overlap of .mat files.
gps1 = sort(unique(gps1));
gps2 = sort(unique(gps2));

% Find available coincident data for the two detectors of interest.
coincident_data = sort(intersect(gps1,gps2));

% Find gaps in coincident data.
gaps = find(diff(coincident_data) > segDur);
gaps = [0 gaps numel(coincident_data)];

% Print zero-lag jobfile.
fprintf('Printing zero-lag jobfiles...\n');
fid = fopen('zerolag_jobfile.txt','w+');
n = 1;
for ii=1:(numel(gaps)-1)
  
  % Start GPS times of chunk of continuous data.
  gps_times = coincident_data((gaps(ii) + 1):gaps(ii+1));

  % Total duration of this data and number of new jobs it will
  % be split into.
  totTime = (numel(gps_times)+1)*segDur;
  nJobs = ceil((totTime-overlap_time)/(job_time-overlap_time));

  % Loop over new jobs.
  for jj=1:nJobs
    idx1 = 1 + (jj-1)*(nSegsPerJob - nSegsOverlap);
    idxN = jj*(nSegsPerJob - nSegsOverlap) + nSegsOverlap;

    if (totTime < job_time)
      idx1 = 1;
      idxN = numel(gps_times);
    elseif (jj == nJobs)
      if (idxN < numel(gps_times))
	error('There are more jobs than the calculated number... fix the code!');
      else
	idxN = numel(gps_times);
	idx1 = idxN - nSegsPerJob + 1;
      end
    end
    
    startGPS = gps_times(idx1); % start time of 1st segment in job
    endGPS = gps_times(idxN); % start time of last segment in job
    endGPS = endGPS + segDur; % add segmentDuration/2 to endGPS time based on
			      % the way endGPS times are handled in STAMP.
			
    % Calculate job duration
    dur = endGPS - startGPS + segDur;
    
    % Print job to zero-lag jobfile.
    fprintf(fid,'%i  %.1f  %.1f  %.1f\n',n,startGPS,endGPS,dur);

    n = n + 1;
  end

end
fclose(fid);

% Print background study jobfile.
fprintf('Printing background study jobfile...\n');
fid = fopen('bknd_jobfile.txt','w+');

% Total duration of all data.
totTime = (numel(coincident_data)+1)*segDur;

% Number of new jobs it will be broken into.
nJobs = ceil((totTime-overlap_time)/(job_time-overlap_time));


% Loop over each new job.
for ii=1:nJobs
  idx1 = 1 + (ii-1)*(nSegsPerJob - nSegsOverlap);
  idxN = ii*(nSegsPerJob - nSegsOverlap) + nSegsOverlap;

  if (ii == nJobs)
    if (idxN < numel(coincident_data))
      error('There are more jobs than the calculated number... fix the code!');
    else
      idxN = numel(coincident_data);
      idx1 = idxN - nSegsPerJob + 1;
    end
  end

  startGPS = coincident_data(idx1); % start time of 1st segment in job
  endGPS = coincident_data(idxN); % start time of last segment in job
  endGPS = endGPS + segDur; % add segmentDuration/2 to endGPS time based on
                            % the way endGPS times are handled in STAMP

  fprintf(fid,'%i  %.1f  %.1f  %.1f\n',ii,startGPS,endGPS,job_time);
end
fclose(fid);


return;