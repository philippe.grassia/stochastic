function anteproc_cache()

% Parameters to modify. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.startGPS = 1;
params.endGPS = 1e10;
params.ifo1 = 'H1';
params.ifo2 = 'L1';
%mat_loc = '/home/prestegard/STAMPAS/BKG/Start-870008200_Stop-870628710_ID-1/matfiles';
mat_loc = '/home/prestegard/test_code/a_mats/';
mat_pref = 'map';
params.anteproc.inmats1 = [mat_loc '/' params.ifo1(1) '-' params.ifo1 '_' mat_pref];
params.anteproc.inmats2 = [mat_loc '/' params.ifo2(1) '-' params.ifo2 '_' mat_pref];
params.segmentDuration = 1;

% search nodes?
doNodes = false;
% directory containing all .mat file folders
% ex: /data/node1/prestegard/mats/map-81606
%     (here, 'mats/' is the node_dir)
%     (must end with '/')
node_dir = 'STAMP_S5_dt1_df1_nspi17/';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Don't change these parameters.
params.anteproc.jobFileTimeShift = false;
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;
params.anteproc.useCache = false;

if doNodes
  fprintf('Looking for .mat files on the nodes...\n');
  % get username
  [no_use,username] = system('whoami');
  username = username(1:end-1); % remove newline
  % check: LDAS or ATLAS?
  [no_use,h] = system('hostname -d');
  if regexp(h,'ligo-wa')
    % LHO
    warning('Not sure how many nodes at Hanford, using 516, as for CIT.');
    node_pref = '/data/node';
    nodes = 1:516;
  elseif regexp(h,'ligo-la')
    % LLO
    warning('Not sure how many nodes at Livingston, using 516, as for CIT.');
    node_pref = '/data/node';
    nodes = 1:516;
  elseif regexp(h,'ligo')
    % CIT
    node_pref = '/data/node';
    nodes = [1:320 322:327 330:490 497:499 501:548];
  elseif regexp(h,'atlas')
    % ATLAS
    node_pref = '/atlas/user/n';
    nodes = 1:1675;
  else
    error('Your cluster is not included in this code.');
  end

  % Loop over all nodes and find .mat files.
  [matlist.ifo1,matlist.ifo2] = deal([]);
  for ii=1:numel(nodes)
    params.anteproc.inmats1 = [node_pref num2str(ii) '/' username '/' ...
		    node_dir params.ifo1(1) '-' params.ifo1 '_' mat_pref];
    params.anteproc.inmats2 = [node_pref num2str(ii) '/' username '/' ...
		    node_dir params.ifo2(1) '-' params.ifo2 '_' mat_pref];

    temp = find_anteproc_mats(params);
    matlist.ifo1 = [matlist.ifo1 temp.ifo1];
    matlist.ifo2 = [matlist.ifo2 temp.ifo2];
  end
else
  % If not looking for .mat files on nodes, just use standard
  % find_anteproc_mats to get the list of .mat files.
  fprintf(['Looking for .mat files in ' mat_loc '.\n']);
  matlist = find_anteproc_mats(params);
end


% Get start times from file names.
mat_starts1 = regexp(matlist.ifo1,'-(\d{9,10}(|\.\d+))-','tokens','once');
mat_starts1 = cellfun(@(x) str2num(x{1}), mat_starts1);
mat_starts2 = regexp(matlist.ifo2,'-(\d{9,10}(|\.\d+))-','tokens','once');
mat_starts2 = cellfun(@(x) str2num(x{1}), mat_starts2);
mat_starts_both = intersect(mat_starts1,mat_starts2);

% Get durations from file names.
durs1 = regexp(matlist.ifo1,'-(\d+|\d+\.\d+)\.mat','tokens','once');
durs1 = cellfun(@(x) str2num(x{1}), durs1);
durs2 = regexp(matlist.ifo2,'-(\d+|\d+\.\d+)\.mat','tokens','once');
durs2 = cellfun(@(x) str2num(x{1}), durs2);

% Check if we have the same data for both detectors.
overlap = true;
if (numel(mat_starts_both) ~= numel(mat_starts1))
  warning('Some data does not overlap between the two detectors.')
  overlap = false;
end

% Set up matinfo struct for saving to cachefile.
params.startGPS = min(min(mat_starts1),min(mat_starts2));
params.endGPS = max(max(mat_starts1+durs1),max(mat_starts2+durs2));
matinfo = params;
matinfo = rmfield(matinfo,'anteproc');

% true if we have the same data for both detectors.
matinfo.dataOverlap = overlap;

% Add GPS start times and durations to cachefile
matlist.gps1 = mat_starts1;
matlist.gps2 = mat_starts2;
matlist.durs1 = durs1;
matlist.durs2 = durs2;

% Save cachefile.
fprintf('Saving cachefile...\n');
save('anteproc_cache.mat','matlist','matinfo');


return;