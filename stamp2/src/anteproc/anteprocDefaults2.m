function params = anteprocDefaults2(params)
% function params = anteprocDefaults2(params)
% Fills detector 2 variables with dummy values
% E Thrane

% detector
params.site2 = 'XYZ';

% resampling
params.resampleRate2 = NaN;
params.bufferSecs2 = NaN;
params.nResample2 = NaN;
params.betaParam2 = NaN;

% data conditioning
params.hannDuration2 = NaN;
params.doHighPass2 = false;
params.highPassFreq2 = NaN;
params.highPassOrder2 = NaN;

% frame and channel
params.ASQchannel2 = NaN;
params.frameType2 = NaN;

% cache
params.frameCachePath2 = 'XYZ';

return
