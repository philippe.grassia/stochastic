function [map, params0] = createmats_anteproc(params,idx)
% function [map, params0] = createmats_anteproc(params)
%
% This code calls anteproc to produce ft-maps on the fly.
% It requires a params file and jobfile like normal pre-processing.
% The ft-maps can be stored by specifying the required parameters
% in the params file.
%
% INPUT:
%   params - standard STAMP params struct. Some important fields of
%            this struct are the paramfile fields (params.paramsFile1
%            and params.paramsFile2) and the jobfile (params.jobsFile)
%            that anteproc uses to produce ft-maps.  
%   idx    - specifies which detector the pre-processing is for.
%            Example: idx = 1 indicates detector 1, so we use
%            params.paramsFile1 for the pre-processing.
%
% OUTPUT:
%   map     - ft-map containing P, naiP, rbartilde, segstarttime,
%             and rbtinfo.
%   params0 - pre-processing paramters.
%
% Written by Tanner Prestegard (prestegard@physics.umn.edu) 8/14/2015
% Adapted from createmats.m by S. Kandhasamy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set up paramfile based on detector idx.
if ischar(params.paramsFile)
  % If params.paramsFile is only a single file, then
  % it should contain parameters for BOTH detectors.
  % So we parse the file and split up the parameters.
  params_both = readParamsFromFile2(params.paramsFile, false);
  param_array = split_params_anteproc(params_both);
  paramfile = param_array(idx);

  % MWC 2/20/16: Need to change seed for H1 and L1 when doing MC
  if paramfile.doDetectorNoiseSim
    paramfile.pp_seed = paramfile.pp_seed + idx;
    randn('state', paramfile.pp_seed);
    rand('state', paramfile.pp_seed);
  end
elseif iscell(params.paramsFile)
  % Otherwise, it should be a cell array of two filenames.
  paramfile = params.paramsFile{idx};
else
  error(['params.paramsFile should be a single filename string or a cell array' ...
	 ' with two filename strings.']);
end

% Read in jobfile (assumed to have 4 columns)
% 1 = "job number", not useful.
% 2 = start GPS time of job
% 3 = end GPS time of job
% 4 = duration of job (3 - #2)
jobs = load(params.jobsFile); % prepare to parse four-column jobfile

% Consider only jobs between startGPS and endGPS
keep_idx = ((jobs(:,3) > params.startGPS) & (jobs(:,2) < params.endGPS));
kept_jobs = jobs(keep_idx,:);

% MAB 14/09/2015: params.jobsFile is now the complete joblist for
% injections. It is then necessary to extract from the jobs list
% the window containing the injection.
if params.injectionWindowIndex>0
  keep_idx = (kept_jobs(:,1) == params.injectionWindowIndex);
  kept_jobs = kept_jobs(keep_idx,:);
end

% If no jobs have data in the specified time interval, throw an error.
if(isempty(kept_jobs))
  error('No data available for given start and end GPS times');
end

% All details for jobs other than the first and last ones
% should be OK.  But params.startGPS may fall after the
% start time of the first job and params.endGPS may fall
% before the end time of the last job.  So we adjust those
% times to account for this.
% Adjust first job.
kept_jobs(1,2) = params.startGPS;
kept_jobs(1,4) = kept_jobs(1,3) - kept_jobs(1,2);
% Adjust last job.
kept_jobs(end,3) = params.endGPS;
kept_jobs(end,4) = kept_jobs(end,3) - kept_jobs(end,2);

% Loop over jobs.
for ii=1:length(kept_jobs(:,1))

  % Get start and end GPS times of job ii.
  startGPS_ii = kept_jobs(ii,2);
  endGPS_ii = kept_jobs(ii,3);

  % Call anteproc.
  [tempmap, params0] = anteproc(paramfile, params.jobsFile, ...
				startGPS_ii, endGPS_ii);
  % We don't cut down to the frequency range specified yet because
  % that is done after the cross-correlation is calculated.
  % If this is the first job being processed, take the map as it is. (help to get the map structure)
  if(ii==1)
    map = tempmap;
  else % Otherwise keep adding to the first map.
    map.P = [map.P tempmap.P];
    map.naiP = [map.naiP tempmap.naiP];
    map.rbartilde = [map.rbartilde tempmap.rbartilde];
    map.segstarttime = [map.segstarttime tempmap.segstarttime];
    % Don't need to add map.rbtinfo as it just contains a few parameters
    % which should be constant for all jobs.
  end

end

% Set up frequencies.
map.f = [params0.flow:params0.deltaF:params0.fhigh]';

% Not sure what this is...
if params.pemPSD
  % Handle detector index.
  if (idx == 1)
    pemPSD = params.pemP1;
  elseif (idx == 2)
    pemPSD = params.pemP2;
  end

  % Loop over columns.
  for ii=1:length(map.P(1,:))
    map.P(:,ii) = pemPSD;
  end
end

% add NaN's when there is no data available; for some studies, like background
% estimation and in case of maps with larger gaps, we don't want NaN's in the 
% data which will unnecessarily increase the memory size of the maps. In those
% cases we can avoid adding NaN's by setting params.gap to false; by default 
% it is set to true.
if params.gap   
  map = addNaNs_anteproc(map, params0, params.startGPS, params.endGPS);
end

% Fix map duration.
map.dur = map.segstarttime(end) - map.segstarttime(1) + params0.segmentDuration;

return;
