function new_map = addNaNs_anteproc(old_map, params, startGPS, endGPS)
% function new_map = addNaNs_anteproc(old_map, params, startGPS, endGPS)
%
% This function adds NaNs when there is missing data during
% the requested period.
%
% INPUT:
%   old_map  - original ft-map.
%   startGPS - start time of requested period.
%   endGPS   - end time of requested period.
%
% OUTPUT:
%   new_map - map with NaNs when there is no data available.
%
% Routine by Tanner Prestegard (prestegard@physics.umn.edu) 8/26/2015
% Adapted from addNanstomap.m by S. Kandhasamy.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate all possible segments in requested period (irrespective of 
% whether data is available or not)
segDur = params.segmentDuration;
if params.doOverlap
   roundStart = startGPS + mod(old_map.segstarttime(1)-startGPS, segDur/2);
   roundEnd = endGPS - mod(endGPS-old_map.segstarttime(1), segDur/2);
   Allsegstarttime = roundStart:segDur/2:roundEnd-segDur;
else
   roundStart = startGPS + mod(old_map.segstarttime(1)-startGPS, segDur);
   roundEnd = endGPS - mod(endGPS-old_map.segstarttime(1), segDur);
   Allsegstarttime = roundStart:segDur:roundEnd-segDur;
end

% Create a map consisting of only NaN's, with the expected size
new_map.P = NaN*ones(length(old_map.f),length(Allsegstarttime));
new_map.naiP = NaN*ones(length(old_map.f),length(Allsegstarttime));
% rbartilde has a different frequency binning at this point.
new_map.rbartilde = NaN*ones(size(old_map.rbartilde,1),length(Allsegstarttime));

% ctimeindex is an array of indices for data in Allsegstarttime that are also
% in segstarttime, i.e., available data 
[commontime, ctimeindex] = intersect(Allsegstarttime, old_map.segstarttime);

% Find the indices corresponding to all available data; for continuous data 
% there is no problem and this part won't be invoked; but for discontinuous 
% data and for segment duration > 1 sec, the segment times can have 
% discontinuity, so this part of the code will take care of it.
Allsegstarttime_shift = Allsegstarttime;
while(length(ctimeindex)~=length(old_map.segstarttime))
  Allsegstarttime_shift = Allsegstarttime_shift + 1;
  [no_use, ctimeindex_shift] = intersect(Allsegstarttime_shift, ...
       old_map.segstarttime);
  ctimeindex = [ctimeindex ctimeindex_shift];
end

% Replace NaN's with data when it is available.
new_map.P(:,ctimeindex) = old_map.P;
new_map.naiP(:,ctimeindex) = old_map.naiP;
new_map.rbartilde(:,ctimeindex) = old_map.rbartilde;
new_map.segstarttime = Allsegstarttime;

% Other metadata.
new_map.f = old_map.f;
new_map.rbtinfo = old_map.rbtinfo;

return;
