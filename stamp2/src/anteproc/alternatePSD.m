function newMap = alternatePSD(psdMap, psdtype, numSegs)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A script for alternative calculations of PSD. This script is 
% - useful if the usual way to calculate noise psd is biased/contaminated by signal
% - is designed to take input of map.naiP from anteproc
% 
% The psdtype parameter specifies the kind of psd estimate wanted. 
% - default returns the input without any changes
% - IPSD gives the psd value by using frequency bin removed from the pixel in question
% - boxPSD gives the psd by drawing a box around the pixels. The dimensions of this box are nrows x numSegmentsPerInterval
%
% Written by Sharan Banagiri (sharan.banagiri@ligo.org)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Number of frequencies and time segments in the map
T = size(psdMap, 2);
F = size(psdMap, 1);

newMap = NaN(F, T);
if strcmp(psdtype, 'default')  % If default then return the psd unchanged. 
   ncols = (numSegs-1)/2 ;
   psdMap = [NaN(F,ncols), psdMap, NaN(F,ncols)]; 
   fprintf('doing default scheme for PSD...\n');
   for ii = 1:1:T  %IgnoreMidseg is set to true for default for the defualt way 
     if sum(isnan(psdMap(:, ii+ncols))) == F  % Make sure that the buffer second nana's aren't taken out
         newMap(:, ii) = NaN(F,1);
      else 
	newMap(:, ii) = mean([psdMap(:,ii:(ii+ncols -1)),psdMap(:, (ii+ncols+1):(ii+2*ncols))], 2,'omitnan');   
     end
   end

elseif strcmp(psdtype, 'IPSD') % If IPSD. Change nrows to change the order of the IPSD scheme
  fprintf('doing 2IPSD...\n')
  nrows = 2;
   ncols = (numSegs-1)/2; 
   
   % Doing horizotnal concatenation with nans the ends of time series   
   psdMap = [NaN(F,ncols), psdMap, NaN(F,ncols)];
   for ii =1:1:T % for non-default methods ignoreMidSeg  is false 

      if sum(isnan(psdMap(:, ii+ncols))) == F  % Make sure that the buffer nans aren't taken out
	 newpsdMap(:, ii) = NaN(F, 1);
      else
	newpsdMap(:, ii) = mean(psdMap(:, (ii):(ii+2*ncols)),2, 'omitnan'); 
      end
 
   end
   psdMap = newpsdMap;   

   % Doing vertical concatenation with nans at the ends of freq series
   psdMap = [NaN(nrows, T); psdMap; NaN(nrows, T)];   
   for ii = 1:1:F
      newMap(ii, :) = mean( [psdMap(ii, :); psdMap(ii + 2*nrows, :)], 1, 'omitnan'); 
   end            
 
elseif strcmp(psdtype, 'box')  % Average over a box of dimensions 2*(nrows-skip) x numSegmentsPerInterval
   fprintf('doing box average for PSD...\n')
   nrows = 32;
   ncols = (numSegs-1)/2 ;
   skip =  49;

   % Doing horizotnal concatenation with nans the ends of time series
   psdMap = [NaN(F,ncols), psdMap, NaN(F,ncols)];
   for ii =1:1:T
     if sum(isnan(psdMap(:, ii+ncols))) == F  % Make sure that the buffer nans aren't taken out
         newpsdMap(:, ii) = NaN(F, 1);
      else % skip mid segment
	newpsdMap(:, ii) = mean([psdMap(:,ii:(ii+ncols -1)),psdMap(:, (ii+ncols+1):(ii+2*ncols))], 2,'omitnan');
     end
   end
   psdMap = newpsdMap;

   % Doing vertical concatenation with nans at the ends of freq series
   psdMap = [NaN(nrows+skip, T); psdMap; NaN(nrows+skip, T)];
   for ii = 1:1:F
      newMap(ii, :) = mean( [psdMap(ii:(ii+ nrows -1), :); psdMap((ii+nrows + 1+2*skip):(ii+ 2*nrows+2*skip), :)], 1, 'omitnan');
   end

end
