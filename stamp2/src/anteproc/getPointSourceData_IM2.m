function params = getPointSourceData_IM2(params, dataStartTime, sampleRate, dataDuration)
% function params = getPointSourceData_IM2(params, dataStartTime, sampleRate)
%
% Calculates GW time delay between the center of the earth and the
% detector in question for a coherent point source.
% Also calculates antenna factors and converts GW polarizations
% to strain.  Time-shifts the detector time-series to account for time delay.
%
% Input:
%   params - parameter struct
%   dataStartTime - start time of current segment
%   sampleRate - data sampling rate (typically 16 kHz)
%   
% Output:
%  params - modified parameter struct
%
%  Routine adopted from getPointSourceData.m (by Stefan Ballmer)
%  and getPointSourceData_IM.m (by the STAMP group).
%  Contact: shivaraj@phy.olemiss.edu, ethrane@ligo.caltech.edu,
%           prestegard@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get the sidereal time for the segment.
%siderealtime = GPStoGreenwichMeanSiderealTime(dataStartTime);

% Get detectors.
det = getdetector(getsitefromletter(params.ifo1(1)));


% We take the user-specified injection time
% as the arrival time at the center of the earth.
% Thus, we invent a fake detector there so that we
% can use the existing infrastructure.'
det0.r = [0 0 0]';
det0.d = det.d; % this doesn't matter, we won't use antenna factors

% Get antenna factors and time delay.
source = [params.pass.stamp.ra params.pass.stamp.decl];
g = calF(det0, det, params.pass.stamp.t, source, params);

% Override antenna factors if requested by user.
if params.fixAntennaFactors
  g = fixAntennaFactors(g,length(params.pass.stamp.t));
  fprintf('Using fixed antenna factors.\n');

elseif params.bestAntennaFactors
  g = bestAntennaFactors(g,1);
  fprintf('Using optimal antenna factors.\n');
elseif params.useReferenceAntennaFactors
  g = getReferenceAntennaFactors(det0, det, dataStartTime, source, params, g);
  fprintf('Using reference time to set antenna factors and detector time delay for injection.\n');
end

% Convert GW polarizations into strain.
% The detector of interest is always detector 2 (passed to
% the filter function second) in the new setup.
h_ra_dec = (params.pass.stamp.hp.*g.F2p' + params.pass.stamp.hx.*g.F2x');

% Sharan Banagiri: This has been modified to make a seperate shift at each point in time, and to interpolate rather than round to adjust for the finite sampleRate.  
% The older way is commented our and not removed.

% Delay the signal with respect to the center of the Earth by tau. 
% Physically the GW signal strain series h_ra_dec is assosciated with delayedTime.
delayedTime = params.pass.stamp.t  - g.tau;

[delayedTime_un, itimes]  = unique(delayedTime);
h_ra_dec = h_ra_dec(itimes);

% Next we round g.tau to the account for the finite sampling rate. 
nsamples =  round(g.tau * sampleRate) ;
dt = params.pass.stamp.t(2) - params.pass.stamp.t(1);
params.pass.stamp.t = params.pass.stamp.t - (dt*nsamples);

% We then interpolate h_ra_dec to get the GW strains to the rounded times of params.pass.stamp.t
% We use spline interpolation for now, but other methods might be useful. 
% Caution: Unlike the cubic interpolation methods, linear or other lower order methods are not set 
% to extrapolate by default. So to  avoid nans, use the 'extrap' when using them. 
h_round = interp1(delayedTime_un, h_ra_dec, params.pass.stamp.t , 'spline');

% Scale injection by sqrt(alpha). We transpose the strain array because interp transposes it. 
params.pass.stamp.h = sqrt(params.pass.stamp.alpha)*h_round';

% Print out information.
fprintf('injection power scaled by %1.1e\n', params.pass.stamp.alpha);

% Delay the signal with respect to the center of the earth by tau.
% The first step is to round tau to one sampling time.
% g.tau = round(g.tau * sampleRate) / sampleRate;
% nsamples = abs(g.tau)*sampleRate;

% nsamples = g.tau*sampleRate;

% Adjust time by nsamples to delay signal.
% Note from Tanner: the convention here seems to be that a positive
% g.tau means shifting the injection back in time.  Not sure why,
% but it works, so we dont mess with it.  This is all set up so
% that we can do coincident injections with 2 or more detectors.

% if (g.tau > 0)
%  params.pass.stamp.t = params.pass.stamp.t - (dt*nsamples);
% elseif (g.tau < 0)
%  params.pass.stamp.t = params.pass.stamp.t + (dt*nsamples);
% end

% Scale injection by sqrt(alpha).
% params.pass.stamp.h = sqrt(params.pass.stamp.alpha)*h_ra_dec;

% Print out information.
% fprintf('injection power scaled by %1.1e\n', params.pass.stamp.alpha);

return;
