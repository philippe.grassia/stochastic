function out_params = split_params_anteproc(params0)
% function out_params = split_params_anteproc(params0)
%
% Function to take in an array of parameters for
% two detectors (as would be used for preproc) and split
% it into two separate arrays for anteproc.
%
% INPUT:
%   params0 - params struct for two detectors (as would be
%             used for preproc).
%
% OUTPUT:
%   out_params - array of two params structs; each one is
%                compatible with anteproc.
%
% Written by Tanner Prestegard (prestegard@physics.umn.edu) 8/26/2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Get fieldnames of params.
fn = fieldnames(params0);

% Find parameter names ending in '1'.
% There should be the same parameters for
% detector 2 that end in '2'.
fields1 = regexp(fn,'(.*)1$','tokens','once');
fields1 = fields1(~cellfun('isempty',fields1));
fields1 = [fields1{:}];

% Find fieldnames that don't have a number at the end.
fields = regexp(fn,'.*\D$','match','once');
fields = fields(~cellfun('isempty',fields));

% Loop over numbered fields and sort them into
% different param structs.  Fields ending in '1'
% will go to out_params(1) and still end in '1'.
% Fields ending in '2' will go to out_params(2) and will
% now end in '1'. (this is how anteproc is set up).
for ii=1:length(fields1)
  end1 = [fields1{ii} '1'];
  end2 = [fields1{ii} '2'];
  out_params(1).(end1) = params0.(end1);
  out_params(2).(end1) = params0.(end2);
end

% Loop over non-numbered fields and assign them
% to both param structs.
for ii=1:length(fields)
  out_params(1).(fields{ii}) = params0.(fields{ii});
  out_params(2).(fields{ii}) = params0.(fields{ii});
end


return;