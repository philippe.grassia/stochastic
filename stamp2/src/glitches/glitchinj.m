function [map_out] = glitches(params, map)
% function [map_out, inj] = powerinj3(params, map, tt, inj)
% E. Thrane: adds injection to the cross- and auto- power maps
%   v3 includes fluctuation terms and it handles the averages over neighboring
%   segments correctly.  NOTE: several buffer segments are required in order
%   to do the averaging calculation and as of June 5, 2012, there is no check
%   in place that this is done correctly.
%   
%   v2 is cleaner and more like loadmats.m
%   It also checks to see if the injection file has already been loaded to save
%   time.  It does not apply random phases to calculate n(f)
%
% params: the parameter struct
% map: the struct containing the ft-maps
% tt: an integer that keeps track of the number of injection strengths that
%   have been performed so far
% inj: a struct that include an ft-map of the injection, which is passed back
%   and forth to avoid having to re-read an injection file that has already
%   been processed
% map_out is the map struct with the injection added

map_out = map;

% mean number of glitches
lambda = 0.5;
number_of_glitches = poissrnd(lambda);

fprintf('Adding %d glitch(es) to data\n',number_of_glitches);

N = length(map.segstarttime);
M = length(map.f);

for i = 1:number_of_glitches

   tt_index = 1 + round(rand*(N-1));
   ff_index = 1 + round(rand*(M-1));

   all_pixels = sort(map.y(:),'descend');
   glitch_amp_max = all_pixels(round(length(all_pixels)*0.25));

   %glitch_amp_max = min(max(map.y)) * rand;
   %glitch_amp_max = mean(mean(map.y(~isnan(map.y)))) * rand;
   %glitch_amp_max = ninety_percent;

   glitch_ff = [1 ff_index M];
   glitch_amp = [0 glitch_amp_max 0];
   ff = 1:M;

   glitch_amp_interp = interp1(glitch_ff,glitch_amp,ff,'spline'); 

   map_out.y(:,tt_index) = map_out.y(:,tt_index) + glitch_amp_interp';

end

map_out.snr = map_out.y./map_out.sigma;
fprintf('  glitch added to data\n');

return
