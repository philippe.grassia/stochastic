function [map, params, pp] = createmats(params)
% function [map, params, pp] = createmats(params)
% This routine creates (and if requested, stores) ft-maps. 
% INPUT:
%   params STRUCT
%     Two important fields of this struct are jobsFile and paramsFile
%     that preproc2 uses to produce ft-maps  
% OUPUT:
%   map -> ft-map containing P1,P2,naiP1,naiP2,cc,sensInt,ccVar,segstarttime...
%   params -> initial params STRUCT with added details
%   pp -> STRUCT to be used by ccSpecReadout_stamp.m
% Routine by S. kandhasamy 
% contact shivaraj@physics.umn.edu

% Read in jobfile
Job_details = load(params.jobsFile); % prepare to parse four-column jobfile
AllStartTimes = Job_details(:,2);
AllEndTimes = Job_details(:,3);
AlljobDurations = Job_details(:,4);
% Consider only jobs between startGPS and endGPS
cut = AllEndTimes > params.startGPS & AllStartTimes < params.endGPS;
kept_jobs = Job_details(cut,:); % jobs that will be passed to preproc2
if(isempty(kept_jobs))
  error('No data available for given start and end GPS times');
end
 % adjust start time and dur of the first job
kept_jobs(1,2) = params.startGPS;
kept_jobs(1,4) = kept_jobs(1,3) - kept_jobs(1,2);
% adjust end time and dur of the last job
kept_jobs(end,3) = params.endGPS;
kept_jobs(end,4) = kept_jobs(end,3) - kept_jobs(end,2);

for ii = 1:length(kept_jobs(:,1))
  [tempmap, params1] = ...
    preproc(params.paramsFile, params.jobsFile, kept_jobs(ii,2), ...
      kept_jobs(ii,3));
  if(ii==1) %if first job take the map as it is (help to get the map structure)
     cutf = tempmap.f >= params.fmin & tempmap.f <= params.fmax;
     map.P1 = tempmap.P1(cutf,:);
     map.P2 = tempmap.P2(cutf,:);
     map.naiP1 = tempmap.naiP1(cutf,:);
     map.naiP2 = tempmap.naiP2(cutf,:);
     map.cc = tempmap.cc(cutf,:);
     map.sensInt = tempmap.sensInt(cutf,:);
     map.ccVar = tempmap.ccVar;
     map.segstarttime = tempmap.segstarttime;
     map.segDur = tempmap.segDur;
     map.start = tempmap.segstarttime(1);
     map.f = tempmap.f(cutf);
     map.fft1 = tempmap.fft1(cutf,:);
     map.fft2 = tempmap.fft2(cutf,:);
  else % otherwise keep adding to the first map
    map.P1 = [map.P1 tempmap.P1(cutf,:)];
    map.P2 = [map.P2 tempmap.P2(cutf,:)];
    map.naiP1 = [map.naiP1 tempmap.naiP1(cutf,:)];
    map.naiP2 = [map.naiP2 tempmap.naiP2(cutf,:)];
    map.cc = [map.cc tempmap.cc(cutf,:)];
    map.sensInt = [map.sensInt tempmap.sensInt(cutf,:)];
    map.ccVar = [map.ccVar tempmap.ccVar];
    map.segstarttime = [map.segstarttime tempmap.segstarttime];
    map.fft1 = [map.fft1 tempmap.fft1(cutf,:)];
    map.fft2 = [map.fft2 tempmap.fft2(cutf,:)];
  end

end

if params.pemPSD
   for i = 1:length(map.P1(1,:))
      map.P1(:,i) = params.pemP1;
      map.P2(:,i) = params.pemP2;
   end
end

% add NaN's when there is no data available; for some studies, like background
% estimation and in case of maps with larger gaps, we don't want NaN's in the 
% data which will unnecessarily increase the memeory size of the maps. In those
% cases we can avoid adding NaN's by setting params.gap to false; by default 
% it is set to true.
if params.gap   
  map = addNaNstomap(map, params.startGPS, params.endGPS);
end

% creating other meta data structure
pp.flow = params1.flow;
pp.fhigh = params1.fhigh;
pp.deltaF = params1.deltaF;
pp.w1w2bar = params1.w1w2bar;
pp.w1w2squaredbar = params1.w1w2squaredbar;
pp.w1w2ovlsquaredbar = params1.w1w2ovlsquaredbar;

% add a few important fields to existing params STRUCT
params.deltaF = params1.deltaF;
params.segmentDuration = params1.segmentDuration;
params.ifo1 = params1.ifo1;
params.ifo2 = params1.ifo2;
params.numSegmentsPerInterval = params1.numSegmentsPerInterval;
params.bufferSecs1 = params1.bufferSecs1;
params.doOverlap = params1.doOverlap;

return;
