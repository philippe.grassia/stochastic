function new_map = addNaNstomap(old_map, startGPS, endGPS)
% function new_map = addNaNstomap(old_map, startGPS, endGPS)
%   -To add NaNs when there is missing data during the requested period
% INPUT:
%  old_map -> original ft-map
%  startGPS -> start time of requested period
%  endGPS -> end time of requested period  
% OUPUT:
%  new_map -> map with NaNs when there is no data available
% Routine by S. kandhasamy 
% contact shivaraj@physics.umn.edu

% calculate all possible segments in requested period (irrespective of 
% whether data is available or not)
roundStart = startGPS + mod(old_map.segstarttime(1)-startGPS, old_map.segDur/2);
roundEnd = endGPS - mod(endGPS-old_map.segstarttime(1), old_map.segDur/2);
Allsegstarttime = roundStart:old_map.segDur/2:roundEnd-old_map.segDur;

% create a map consisting of only NaN's, with the expected size
new_map.P1 = NaN*ones(length(old_map.f),length(Allsegstarttime));
new_map.P2 = NaN*ones(length(old_map.f),length(Allsegstarttime));
new_map.naiP1 = NaN*ones(length(old_map.f),length(Allsegstarttime));
new_map.naiP2 = NaN*ones(length(old_map.f),length(Allsegstarttime));
new_map.cc = NaN*ones(length(old_map.f),length(Allsegstarttime));
new_map.sensInt = NaN*ones(length(old_map.f),length(Allsegstarttime));
new_map.ccVar = NaN*ones(1,length(Allsegstarttime));
new_map.fft1 = NaN*ones(length(old_map.f),length(Allsegstarttime));
new_map.fft2 = NaN*ones(length(old_map.f),length(Allsegstarttime));

% ctimeindex is an array of indices for data in Allsegstarttime that are also
% in segstarttime, i.e., available data 
[commontime, ctimeindex] = intersect(Allsegstarttime, old_map.segstarttime);

% find the indices corresponding to all available data; for continuous data 
% there is no problem and this part won't be invoked; but for discontinuous 
% data and for segment duration > 1 sec, the segment times can have 
% discontinuity, so this part of the code will take care of it.
Allsegstarttime_shift = Allsegstarttime;
while(length(ctimeindex)~=length(old_map.segstarttime))
  Allsegstarttime_shift = Allsegstarttime_shift + 1;
  [no_use, ctimeindex_shift] = intersect(Allsegstarttime_shift, ...
       old_map.segstarttime);
  ctimeindex = [ctimeindex ctimeindex_shift];
end

% replace NaN's with data when it is available
new_map.P1(:,ctimeindex) = old_map.P1;
new_map.P2(:,ctimeindex) = old_map.P2;
new_map.naiP1(:,ctimeindex) = old_map.naiP1;
new_map.naiP2(:,ctimeindex) = old_map.naiP2;
new_map.cc(:,ctimeindex) = old_map.cc;
new_map.sensInt(:,ctimeindex) = old_map.sensInt;
new_map.ccVar(:,ctimeindex) = old_map.ccVar;
new_map.segstarttime = Allsegstarttime;
new_map.fft1(:,ctimeindex) = old_map.fft1;
new_map.fft2(:,ctimeindex) = old_map.fft2;

%other meta data
new_map.segDur = old_map.segDur;
new_map.f = old_map.f;

return
