function [map,params] = altsigma(map,params)

sigmas = zeros(size(map.sigma));
for ii = 1:length(map.segstarttime)
   for jj = 1:length(map.f)
      index1 = jj - params.altsigma_bins;
      index2 = jj + params.altsigma_bins;

      if index1 <= 0
         index1 = 1;
      end

      if index2 >= length(map.f)+1
         index2 = length(map.f);
      end

      sigmas_all = map.sigma(index1:index2,ii);
      sigmas_all = sigmas_all(~isnan(sigmas_all));
      if isempty(sigmas_all)
         sigmas_all = NaN;
      end
      sigma = 10^(median(log10(sigmas_all)));
      sigma = median(sigmas_all);
      sigmas(jj,ii) = sigma;
   end
end

%figure;
%plot(map.f,median(map.sigma'),'r')
%hold on
%plot(map.f,median(sigmas'),'b')
%hold off
%print('-dpng','sigma.png');
%close;

map.sigma = sigmas;

