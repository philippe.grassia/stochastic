function [cut_col, det_cut, xi_frac] = Xi_cut(map,params)
% function [cut_col, det_cut] = Xi_cut(map)
% Written by T. Prestegard (prestegard@physics.umn.edu)
%
% Takes a STAMP map struct and identifies time segments
% where the autopower in the two detectors is not consistent
% with noise + a GW signal.
%
% Inputs:
%   map    - STAMP map struct
%   params - STAMP search parameters (struct).
%
% Outputs:
%   cut_col - 1D array of 1s and 0s, each elemnt corresponding to a
%             segment.  1 indicates the column is glitchy and should
%             be cut, 0 indicates that the column is OK.
%   det_cut - 1D array that specifies which detector the glitch
%             occurred in.  1 indicates detector 1, 2 indicates
%             detector 2, 3 indicates both detectors (only when the 
%             coincident cut is on), and 0 indicates no glitch.
%
% For additional documentation, see
% https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=71680
% Specifically, refer to Eqs. 23 and 24.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find NaNs, set to 0 so glitch cut is not affected.
% NaNs in P1, P2 set to 1 since we divide by these statistics
% to calculate AP_det1 and AP_det2.
cut = isnan(map.snr);
map.snr(cut) = 0;
map.naiP1(cut) = 0;
map.naiP2(cut) = 0;
map.P1(cut) = 1;
map.P2(cut) = 1;
map.Xi_snr(cut) = 0;

% Define "glitch-like" Xi SNR range.
cut_low = params.glitch.Xi_min;
cut_high = params.glitch.Xi_max;

% Fraction of Xi pixels that must be in that range to identify
% a column as glitch-like.
Xi_frac = params.glitch.Xi_frac;

% Xi_frac was tuned for the case of narrowband signals in a 1100 Hz wide band.
% If the band is significantly narrower, the false dismissal rate may be 
% unacceptably high, and Xi-cut parameters may need to be retuned.  If this
% happens, I suggest moving some of these hard-coded parameters into params
% struct parameters that can be passed from the clustermap wrapper. -EHT (3/12)

%MAB 15/09/2015
%if length(map.f)~=1101
%  fprintf(['Caution: you are applying the Xi cut using a bandwidth other ' ...
%    'than 1100 Hz.  Additional tuning may be required in order to achieve '... 
%    'an acceptable false alarm rate and/or adequate glitch rejection.\n']);
%end

% Autopower stationarity ratio must exceed this value for a
% column to be identified as glitch-like.
AP_thresh = params.glitch.AP_thresh;

% loop over frequency bands (default = 1 band)
% and calculate glitch statistics.
nFreqs = ceil(size(map.P1,1)/((params.glitch.numBands+1)/2));
for ii=1:params.glitch.numBands
    % Determine indices of current frequency band
    r1 = 1 + ceil(nFreqs/2)*(ii-1);
    r2 = ceil(nFreqs/2)*(ii+1);
    
    if (r2 > size(map.P1,1))
        r2 = size(map.P1,1);
    end
    ri = r1:r2;
    
    % Find columns with excess autopower in each detector.
    % AP_det1 is the autopower stationarity ratio for detector 1 (see Eq. 22).
    % Averages over frequency.
    AP_det1(ii,:) = mean(map.naiP1(ri,:)./map.P1(ri,:));
    AP_cols_det1(ii,:) = AP_det1(ii,:) > AP_thresh;
    AP_det2(ii,:) = mean(map.naiP2(ri,:)./map.P2(ri,:));
    AP_cols_det2(ii,:) = AP_det2(ii,:) > AP_thresh;

    % Find "hot" positive or negative columns in Xi_snr.
    cut_map_pos = ( (map.Xi_snr(ri,:) > cut_low) & ...
                    (map.Xi_snr(ri,:) < cut_high));
    cols_pos(ii,:) = sum(cut_map_pos)/length(cut_map_pos(:,1));
    Xi_cols_pos(ii,:) = cols_pos(ii,:) > Xi_frac;
    %
    cut_map_neg = ( (map.Xi_snr(ri,:) < -cut_low) & ...
                    (map.Xi_snr(ri,:) > -cut_high));
    cols_neg(ii,:) = sum(cut_map_neg)/length(cut_map_neg(:,1));
    Xi_cols_neg(ii,:) = cols_neg(ii,:) > Xi_frac;
    
    % Positive Xi SNR indicates a glitch in det1.
    cut_det1(ii,:) = Xi_cols_pos(ii,:) & AP_cols_det1(ii,:) & ...
        ~AP_cols_det2(ii,:);
    
    % Negative Xi SNR indicates a glitch in det2.
    cut_det2(ii,:) = Xi_cols_neg(ii,:) & AP_cols_det2(ii,:) & ...
        ~AP_cols_det1(ii,:);

    % Combine results from each detector to obtain all columns that
    % have been identified as glitch-like.
    cut_col(ii,:) = (cut_det1(ii,:) | cut_det2(ii,:));
    
    % Coincidence checks
    if (params.glitch.doCoincidentCut)
        Xi_cols_pos_co(ii,:) = cols_pos(ii,:) > Xi_frac*2;
        Xi_cols_neg_co(ii,:) = cols_neg(ii,:) > Xi_frac*2;
        cut_both(ii,:) = ((Xi_cols_pos_co(ii,:) & AP_cols_det1(ii,:)) | ...
                          (Xi_cols_neg_co(ii,:) & AP_cols_det2(ii,:)));
        %cut_both2 = Xi_cols_pos & AP_cols_det1 & Xi_cols_neg & AP_cols_det2;
        cut_col(ii,:) = (cut_col(ii,:) | cut_both(ii,:));
    end


    if params.doVLT

       % This cut is useful if you know that the signal is probably narrowband in nature. If thus cuts signals which are dominant in one 
      cut_nb(ii, :) = sum(abs(map.snr(ri, :)), 'omitnan') > params.nbThresh;
      cut_plus(ii, :) = circshift(cut_nb(ii, :), 1, 2);
      cut_minus(ii, :) = circshift(cut_nb(ii, :), -1, 2);
      % make sure that narrowband features(signals) aren't cut
      cut_nb2(ii, :) = cut_nb(ii, :) & (~cut_plus(ii, :) & ~cut_minus(ii, :));
      cut_nb(ii, :) = cut_nb2(ii, :);
      
      %for jj  = 1:1:size(map.snr, 2)
      %    seg = map.snr(ri, jj);
      %     seg(seg == max(seg)) = NaN;
      %     map.snr(ri, jj) = seg;
      %end
      %snrstd(ii, :)  = std(map.snr(ri, :), 'omitnan');
      %   cut_nb(ii, :) = (snrstd(ii, :) > 6);
      
      cut_col(ii, :) = (cut_col(ii, :) | cut_nb(ii, :));
      fprintf('Using the narrowband cut...\n')
      
    end
    
end

cut_col = sum(cut_col,1) > 0;

% determine which detector any glitches occurred in
temp1 = sum(cut_det1 > 0);
temp2 = sum(cut_det2 > 0);
det_cut = (temp1 & cut_col)*1 + (temp2 & cut_col)*2;

% determine the frac of hot spot for the glitch 
xi_frac=[sum(cols_pos,1); sum(cols_neg,1)];
xi_frac={xi_frac(1,det_cut==1);xi_frac(2,det_cut==2)};

if params.glitch.doCoincidentCut
    tempboth = sum(cut_both > 0);
    r = find(tempboth > 0);
    det_cut(r) = 3;
end


return;
