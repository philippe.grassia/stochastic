function fakecache(njobs, dur)
% function fakecache(njobs, dur)
%
% Creates N=njobs HL cachefiles, each with duration=dur.  Each job is separated
% by an stretch of missing data so that they are not contiguous.  This avoids
% "seem glitches," which can occur when independently generated colored noise
% time series are concatenated.  These cachefiles are dummy cachefiles for use
% in Monte Carlo noise studies.
%
% The output of this function are
%   fakecache/frameFiles?.*.txt
%   fakecache/gpsTimes?.*.txt
%   mcjobs.txt

% define detector pair
ifo1 = 'H';
ifo2 = 'L';

% arbitrary GPS start time for set of jobs: Sept 14, 2011
t = 1000000000;

% space between jobs
dt = 10;

% define jobfile
jobfile = fopen('mcjobs.txt', 'w+');

% loop over jobs
for ii=1:njobs
  % open and write frame cachefiles
  fr1 = fopen(['fakecache/frameFiles' ifo1 '.' num2str(ii) '.txt'],'w+');
  fprintf(fr1, '/FAKEDATA/%s-FAKE-%i-%i.gwf', ifo1, t, dur);
  fclose(fr1);
  %
  fr2 = fopen(['fakecache/frameFiles' ifo2 '.' num2str(ii) '.txt'],'w+');
  fprintf(fr2, '/FAKEDATA/%s-FAKE-%i-%i.gwf', ifo2, t, dur);
  fclose(fr2);
  % open and write GPS cachefiles
  gps1 = fopen(['fakecache/gpsTimes' ifo1 '.' num2str(ii) '.txt'],'w+');
  fprintf(gps1, '%i', t);
  fclose(gps1);
  %
  gps2 = fopen(['fakecache/gpsTimes' ifo2 '.' num2str(ii) '.txt'],'w+');
  fprintf(gps2, '%i', t);
  fclose(gps2);
  % write job file
  fprintf(jobfile, '%i %i %i %i\n', ii, t, t+dur, dur);
  % increment time
  t = t + dur + dt;
end

% clean up
fclose(jobfile);

return
