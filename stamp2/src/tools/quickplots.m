function quickplots(map, inj, ccmin, ccmax);
% function mkplots(map);
% This function is just like saveplots except it can be called outside of 
% stochmap to make plots associated with a saved mat file.
% default is that inj==false.  if inj==true then we scale the plots as if they
% contain pure signal and no noise

try
  if inj
    % if we want to look at maps of pure injections, plot only the NO orders of
    % magnitude and replace power=0 with NaN
    NO = 3;
    map.P1(map.P1<max(map.P1(:))/10^NO) = 0;
    map.P1(map.P1==0) = NaN;
    map.P2(map.P2<max(map.P2(:))/10^NO) = 0;
    map.P2(map.P2==0) = NaN;
    %
    map.naiP1(map.naiP1<max(map.naiP1(:))/10^NO) = 0;
    map.naiP1(map.naiP1==0) = NaN;
    map.naiP2(map.naiP2<max(map.naiP2(:))/10^NO) = 0;
    map.naiP2(map.naiP2==0) = NaN;
    %
    map.cc(map.cc<max(map.cc(:))/10^NO) = 0;
    map.cc(isnan(map.cc)) = 0;
  end
catch
  inj=false;
end

map.xvals = map.segstarttime-map.segstarttime(1);
map.yvals = map.f;

p1max = max(abs(map.P1(:)));
p1min = min(abs(map.P1(:)));
printmap(map.P1, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'P1 (strain^2)', [p1min p1max]);
print('-dpng','p1_map');

p2max = max(abs(map.P2(:)));
p2min = min(abs(map.P2(:)));
printmap(map.P2, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'P2 (strain^2)', [p2min p2max]);
print('-dpng','p2_map');

naip1max = max(abs(map.naiP1(:)));
naip1min = min(abs(map.naiP1(:)));
printmap(map.naiP1, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'naiP1 (strain^2)', [naip1min naip1max]);
print('-dpng','naip1_map');

naip2max = max(abs(map.naiP2(:)));
naip2min = min(abs(map.naiP2(:)));
printmap(map.naiP2, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'naiP2 (strain^2)', [naip2min naip2max]);
print('-dpng','naip2_map');

if inj
  try
    ccmin;
    ccmax;
  catch
    ccmin = 1e-65;
    ccmax = 1e-60;
  end
  fprintf('cc map scaled from %1.1e to %1.1e\n', ccmin, ccmax);
else
  ccmax = 2*mean(mean(abs(map.cc)));
  ccmin = 0.1*mean(mean(abs(map.cc)));
end
printmap(abs(map.cc), map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'CC (strain^2)', [ccmin ccmax], 0, 1);
print('-dpng','cc_map');

return
