function Cf = csd_eht(x1f, x2f, Fs)
% function Cf = csd_eht(x1f, x2f, Fs)
% input:
%   x1f: single-sided Fourier transform created by fft_eht
%   x2f: single-sided Fourier transform created by fft_eht
%   Fs: sampling frequency
% output
%   Cf: crosspower spectral density with proper normalization so that x has
%       units of strain, then Pf has units of strain^2/Hz.
% Cf will have the same size as xf with bins corresponding to the same
% frequencies.

% check that x1f and x2f are the same size
if any( size(x1f) ~= size(x2f))
  error('csd_eht: FFTs must be the same size.');
end

% calculate the length of the time series used to create xf
length_xf = size(x1f, 1);
if mod(length_xf, 2)
  L = (2*(length_xf-1));
else
  L = (2*(length_xf-2));
end

% calculate CSD
Cf = 2*conj(x1f).*x2f / L / Fs;

return
