function cluster_out = calc_cluster_stats(reconAll,map,params)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Purpose: calculate statistics and determine max. cluster based on the
%          reconAll map.  Update added March 3, 2014 by Eric Thrane: currently
%          this code lives in src/tools/ since it was designed to be generally
%          applicable.  As things have turned out, it is currently only uesd by
%          burstegard since the recent work on seedless clustering (stochtrack)
%          has moved the analogous (similar though not the same) into 
%          stochtrack.m.  Thus, this code has become somewhat specific to
%          burstegard.  For the time being, we are keeping it in tools/.
%
% Inputs:
% reconAll - map of clusters.  Each cluster has a different pixel value.
% map      - STAMP map struct.
% params   - STAMP search parameters.
%
% Outputs:
% cluster_out - output cluster struct, including calculated statistics.
%
% Contact: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Determine number of clusters.
clust_nums = unique(reconAll(reconAll > 0));

% If no clusters, set everything to zero.
% Otherwise, calculate cluster statistics.
if (length(clust_nums) == 0)
  cluster_out.nPix = 0;
  cluster_out.snr_gamma = 0;
  cluster_out.y_gamma = 0;
  cluster_out.sigma_gamma = 1;
  cluster_out.reconMax = zeros(size(map.y));
  cluster_out.reconAll = zeros(size(map.y));
  cluster_out.tmin = 0;
  cluster_out.tmax = 0;
  cluster_out.gps_min = 0;
  cluster_out.gps_max = 0;
  cluster_out.fmin = 0;
  cluster_out.fmax = 0;
else
  % Loop over clusters and calculate statistics.
  for ii=1:length(clust_nums)
    temp = (reconAll == clust_nums(ii));
    nPix(ii) = sum(temp(:));
    temp_y = map.y(temp);
    temp_sigma = map.sigma(temp);
    temp_snr = map.snr(temp);
    
   
    if (params.burstegard.weightedSNR)
      y_gamma(ii) = sum(temp_y ./ (temp_sigma .^ 2)) / sum(1 ./ (temp_sigma ...
						  .^ 2));
      sigma_gamma(ii) = 1 / sqrt(sum(1 ./ (temp_sigma .^ 2)));
      snr_gamma(ii) = y_gamma(ii) ./ sigma_gamma(ii);
    else
      y_gamma(ii) = [];
      sigma_gamma(ii) = [];
      snr_gamma(ii) = sum(temp_snr);
    end
  end
  
  % Determine maximum cluster.
  max_idx = find(snr_gamma == max(snr_gamma));
  
  % Calculate statistics for maximum cluster.
  cluster_out.nPix = nPix(max_idx);
  cluster_out.snr_gamma = snr_gamma(max_idx);
  cluster_out.y_gamma = y_gamma(max_idx);
  cluster_out.sigma_gamma = sigma_gamma(max_idx);
  
  % Format maps for maximum cluster.
  temp = (reconAll == clust_nums(max_idx));
  cluster_out.reconMax = map.snr;
  cluster_out.reconMax(~temp) = 0;
  cluster_out.reconAll = reconAll;
  %cluster_out.reconAll(~temp) = 0;

  % Calculate maximum cluster information.
  xvals = map.segstarttime - map.segstarttime(1);
  [r,c] = find(cluster_out.reconMax > 0);
  cluster_out.tmin = xvals(min(c));
  cluster_out.tmax = xvals(max(c));
  cluster_out.gps_min = map.segstarttime(min(c));
  cluster_out.gps_max = map.segstarttime(max(c));
  cluster_out.fmin = map.f(min(r));
  cluster_out.fmax = map.f(max(r)); 

  % calculate SNR frac
  totalSNR = sum(cluster_out.reconMax(:));
  colsSNR = sum(cluster_out.reconMax,1)/totalSNR;
  temp_idx = find(colsSNR==max(colsSNR));
  cluster_out.SNRfrac = colsSNR(temp_idx);
  cluster_out.SNRfracTime = map.segstarttime(temp_idx) - map.segstarttime(1);
end


return;