function [ra_max, dec_max, epsilon_max, g] = bestdir(GPSTimes)
% function [ra_max, dec_max, epsilon_max, g] = bestdir(GPSTimes)
% returns (ra, dec) with the optimal antenna factor for gpsTimes
% GPSTimes should be a row vector.
% The outputs are all row vectors (g is a struct of row vectors)
% containing the relevant parameter for the corresponding GPS time.

% convert GPSTimes to a row vector if it isn't already.
if ~isrow(GPSTimes)
  GPSTimes = GPSTimes';
  if ~isrow(GPSTimes)
    error('GPSTimes should be a row vector.');
  end
end

% set basic parameters
GPSTimes = strassign(GPSTimes);
params = stampDefaults;
params.c = 299792458;
params.ifo1 = 'H1';
params.ifo2 = 'L1';
[det1 det2] = ifo2dets(params);

% generate list of source positions.
% declination needs to be handled so that we don't end up with a bunch of
% repetitive points near the poles.
ra = 0:24/360:23.999;
dec = 0:(1/180):1;
dec = (180/pi)*(acos(1-2*dec)-pi/2);

% make vector of all sky direction pairs and run calF.
source = combvec(ra,dec)';
g = calF(det1, det2, GPSTimes, source, params);

% calculate epsilon and find max value for each GPS time.
epsilon = (g.F1p .* g.F2p + g.F1x .* g.F2x)/2;
[no_use,idx] = max(abs(epsilon));

% get corresponding sky locations.
source = [source(idx,:)];
ra_max = source(:,1)';
dec_max = source(:,2)';

% re-run calF with max sky positions.
g = calF(det1, det2, GPSTimes, source, params);

% calF gives results for all combinations of sky direction and GPS time,
% the diagonal entries are the ones with the optimal antenna factor for
% a given GPS time.
g.tau = diag(g.tau)';
g.F1p = diag(g.F1p)';
g.F2p = diag(g.F2p)';
g.F1x = diag(g.F1x)';
g.F2x = diag(g.F2x)';

epsilon_max = (g.F1p .* g.F2p + g.F1x .* g.F2x)/2;

return;
