function h = ifft_eht(hf)
% function h = ifft_eht(hf)
% Eric Thrane
% inverse FFT for use in conjunction with fft_eht

% determine the number of segments
nsegs = size(hf, 2);

% construct an fft with positive and negative frequency bins.  note that matlab
% has a funny ordering scheme for frequency bins.  For an fft with resolution =
% df and Nyquist frequency = Fs/2, the frequency bins are:
%   0
%   df
%   2*df
%   ...
%   Fs/2 - df
%   Fs/2
%   -Fs/2 + df
%   -Fs/2 + 2*df
%   ...
%   -2*df
%   -df
tmp = [hf ; flipud(conj(hf(2:end-1,:)))];

% apply ifft
h = ifft(tmp, [], 1);

return
