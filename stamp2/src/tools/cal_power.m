function H = cal_power(file, alpha, doPlot, doParseval)
% function H = cal_power(file, alpha)
%   file is of the form: t hp hx
%   alpha (optional) is the scale factor
%   returns H: strain power
% Eric Thrane

% debugging options
try
  doParseval
catch
  doParseval = false;
end
try
  doPlot;
catch
  doPlot = false;
end

% set alpha=1 if not specified
try
  alpha;
catch
  alpha=1;
  fprintf('alpha not specified; setting alpha=1\n');
end

% load waveform file and parse data
g = load(file);
t = g(:,1);
hp = g(:,2);
hx = g(:,3);

% define sampling frequency
dt = t(2)-t(1);
Fs = 1/dt;

% calculate duration in case we want to compare with 1/df
T = t(end)-t(1);

% calculate FFTs
[hp_f f] = fft_eht(hp, Fs);
[hx_f f] = fft_eht(hx, Fs);
df = f(2)-f(1);

% diagnostic plot
if doPlot
  figure;
  loglog(f, abs(hp_f), 'b', 'linewidth', 2);
  hold on;
  loglog(f, abs(hx_f), 'r--', 'linewidth', 2);
  print('-dpng', 'cal_fluence');
end

% demonstrate Parseval's theorem to show correctness of frequency domain 
% calculation
if doParseval
  X = norm(hp)^2;
  Y = norm(hp_f)^2/numel(hp_f);
  fprintf(['Parseval check: norm(hp)^2 = %1.3e, norm(hp_f)^2/numel(hp_f)' ...
    ' = %1.3e\n'], X, Y);
end

% calculate strain power
H = sum(dt * (abs(hp_f).^2 + abs(hx_f).^2)) / numel(hp_f);

% scale F by scale factor alpha
H = alpha * H;

return
