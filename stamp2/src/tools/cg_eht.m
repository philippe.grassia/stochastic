function [P_cg, f_cg] = cg_eht(Pf, f, deltaFy)
% function [P_cg, f_cg] = cg_eht(Pf, f, deltaFy)
% Pf = power spectrum
% f frequency array (with resolution = f(i+1) - f(i)
% deltaFy = desired frequency resolution

% define variables used in coarseGrain.m
Nx = length(Pf);
flowx = f(1);
deltaFx = f(2)-f(1);

% number of new frequency bins
Ny = floor((f(end)-f(1))/deltaFy);

% starting frequency of coarse-grained spectrum
flowy = flowx + (deltaFy-deltaFx)/2;

% check that frequency spacings and number of elements are > 0
if ( (deltaFx <= 0) | (deltaFy <= 0) | (Ny <= 0) )
  error('bad input arguments');
end

% check that freq resolution of x is finer than desired freq resolution
if ( deltaFy < deltaFx )
  error('deltaF coarse-grain < deltaF fine-grain');
end

% check desired start frequency for coarse-grained series
if ( (flowy - 0.5*deltaFy) < (flowx - 0.5*deltaFx) )
  error('desired coarse-grained start frequency is too low');
end

% check desired stop frequency for coarse-grained series
fhighx = flowx + (Nx-1)*deltaFx;
fhighy = flowy + (Ny-1)*deltaFy;

if ( (fhighy + 0.5*deltaFy) > (fhighx + 0.5*deltaFx) )
  error('desired coarse-grained stop frequency is too high');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% indices for coarse-grained series
i = transpose([0:1:Ny-1]);

% calculate the low and high indices of fine-grained series 
jlow  = 1 + floor( (flowy + (i-0.5)*deltaFy - flowx - 0.5*deltaFx)/deltaFx ); 
jhigh = 1 + floor( (flowy + (i+0.5)*deltaFy - flowx - 0.5*deltaFx)/deltaFx ); 

index1 = jlow(1);
index2 = jhigh(end);

% calculate fractional contributions
fraclow  = (flowx + (jlow+0.5)*deltaFx - flowy - (i-0.5)*deltaFy)/deltaFx;
frachigh = (flowy + (i+0.5)*deltaFy - flowx - (jhigh-0.5)*deltaFx)/deltaFx;
  
frac1 = fraclow(1);
frac2 = frachigh(end);

% calculate coarse-grained values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sum of middle terms
% NOTE:  since this operation takes more time than anything else, i made 
% it into separate routine called sumTerms() which is then converted into
% a MEX file
jtemp = jlow+2;

[midsum_real,midsum_complex] = sumTerms(...
   real(Pf),imag(Pf), jtemp, jhigh, length(jtemp));
midsum = complex(midsum_real,midsum_complex);

% calculate all but final value of y
ya = (deltaFx/deltaFy)*(Pf(jlow (1:Ny-1)+1).*fraclow (1:Ny-1) + ...
                        Pf(jhigh(1:Ny-1)+1).*frachigh(1:Ny-1) + ...
                        midsum(1:Ny-1) );

% calculate final value of y
if (jhigh(Ny) > Nx-1)
  % special case when jhigh exceeds maximum allowed value
  yb = (deltaFx/deltaFy)*(Pf(jlow(Ny)+1)*fraclow(Ny) + midsum(Ny));

else
  yb = (deltaFx/deltaFy)*(Pf(jlow (Ny)+1)*fraclow (Ny) + ...
                          Pf(jhigh(Ny)+1)*frachigh(Ny) + ...
                          midsum(Ny) );
end

% fill structure for coarsed-grained frequency series
P_cg = [ya; yb];
f_cg = flowy + deltaFy*[0:length(P_cg)-1];

return

