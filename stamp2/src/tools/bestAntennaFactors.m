function g = bestAntennaFactors(g,N)
% function g = bestAntennaFactors(g,N)
% Eric Thrane
% Fixes antenna factors to values found to maximize LHO-LLO pair efficiency
try
  N;
catch
  N=1;
end

% H1H2
% g.F1p = -0.8188*ones(1,N);
% g.F1x = -0.8188*ones(1,N);
% g.F2p = -0.5739*ones(1,N);
% g.F2x = -0.5739*ones(1,N);
% g.gamma0 = 0.4999*ones(1,N);
 
% H1L1
g.F1p = 0.6146*ones(1,N);
g.F1x = -0.7530*ones(1,N);
g.F2p = -0.6190*ones(1,N);
g.F2x = 0.7492*ones(1,N);
g.gamma0 = -0.4723*ones(1,N);

return
