function Pf = psd_eht(xf, Fs)
% function Pf = psd_eht(xf, Fs)
% input:
%   xf: single-sided Fourier transform created by fft_eht
%   Fs: sampling frequency
% output
%   Pf: power spectral density with proper normalization so that x has units of
%      strain, then Pf has units of strain^2/Hz.
% Pf will have the same size as xf with bins corresponding to the same
% frequencies.

% calculate the length of the time series used to create xf
length_xf = size(xf, 1);
if mod(length_xf, 2)
  L = (2*(length_xf-1));
else
  L = (2*(length_xf-2));
end

% calculate PSD
Pf = 2*abs(xf).^2 / L / Fs;

return
