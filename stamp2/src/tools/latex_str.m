function str = latex_str(x)
% function str = latex_str(x)
% x is number, str is a latex string

% convert x to a string in matlab scientific notation with three sig figures
tmp = sprintf('%1.2e', x);

% replace e+03 notation with \times10^3
tmp = regexprep(tmp, 'e+', '\\times10^{');
tmp = regexprep(tmp, '+', '');
tmp = regexprep(tmp, '{0', '{');
tmp = ['$' tmp '}$'];
str = regexprep(tmp, '\\times10\^\{0\}', '');

return
