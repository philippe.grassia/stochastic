#!/usr/bin/python

"""
%prog

Michael Coughlin (michael.coughlin@ligo.org)

This program does waveform comparisons.

"""

import os, sys, pickle, math, optparse, glob
import matplotlib.pyplot as plt
import numpy as np
#import pe_waveform_comparison_plot, pe_waveform_comparison_utils
from pylal import SimInspiralUtils, Fr
import lal

#import matchedfilter

import pycbc.filter
from pycbc.psd import aLIGOZeroDetHighPower

import templates

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def load_mdc(mdcdir,mdcchannel, vector, dataStartTime, sampleRate):

    fr_t = np.arange(len(vector))/sampleRate + dataStartTime
    fr_zeros = np.zeros((len(fr_t),1))

    print 'Adding MDC data to vector... '
    numFrames = 0

    frames = glob.glob(mdcdir + '/*.gwf');
    for frame in frames:

        frameFull = frame.replace('.','-')
        frameSplit = frameFull.split("-")

        gps = float(frameSplit[3])
        dur = float(frameSplit[4])

        gpsStart = gps
        gpsEnd = gps+dur

        if gpsStart > fr_t[-1]:
            continue

        if gpsEnd < fr_t[1]:
            continue

        numFrames = numFrames + 1

        data_out = Fr.frgetvect(frame,mdcchannel)
        data_out = data_out[0]
        fs = len(data_out)/dur
        mdc_t = np.arange(len(data_out))/fs + gps
        mdc_interp = np.interp(fr_t,mdc_t,data_out)
        vector = vector + mdc_interp

    if numFrames > 0:
        print 'data added.'
    else:
        print 'no data added'

    return fr_t,vector

psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/LIGOsrdPSD_40Hz.txt'
psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd_10Hz.txt'

plotDir = '/home/mcoughlin/STAMP/Magnetar/condor_stochsky/bursts/plots'

params = {}
params["stochtrack"] = {}
params["stochtrack"]["mindur"] = 100.0
params["stochtrack"]["maxdur"] = 250.0
params["stochtrack"]["fmin"] = 500.0
params["stochtrack"]["fmax"] = 1000.0
params["stochtrack"]["fdiffmin"] = 0.0
params["stochtrack"]["fdiffmax"] = 1.0

params["stochtrack"]["fmin"] = 599.0
params["stochtrack"]["fmax"] = 601.0
params["stochtrack"]["fdiffmin"] = 0.0
params["stochtrack"]["fdiffmax"] = 0.0

params["stochtrack"]["fs"] = 2048.0
params["stochtrack"]["T"] = 248.0

f_low = params["stochtrack"]["fmin"]
f_high = params["stochtrack"]["fmax"]

fs_template = params["stochtrack"]["fs"]
T = params["stochtrack"]["T"]
# define evenly spaced Bezier xvals and work backward to get tt
B1 = np.arange(0,T,1.0/fs_template)

print "Loading MDC"
print "H1"
dataStartTime = 946076555.624748230
sampleRate = 4096.0
mdcdir = '/home/mcoughlin/STAMP/Magnetar/inj_anteproc/1/injframes_new/A/mag-a/H1/';
mdcchannel = 'H1:STRAIN';
Tinj = 250.0
vector = np.zeros((int(Tinj*sampleRate),))
fr_t,ht1 = load_mdc(mdcdir,mdcchannel,vector, dataStartTime, sampleRate)
delta_t = 1/sampleRate
ts1 = pycbc.types.TimeSeries(ht1,delta_t=delta_t,dtype=pycbc.types.float64)

print "Resampling..."
delta_t = 1.0/fs_template
ts1 = pycbc.filter.resample.resample_to_delta_t(ts1, delta_t)

print "Making frequency series..."
data1 = pycbc.filter.make_frequency_series(ts1)
sample_frequencies = data1.sample_frequencies
flen = len(data1)
delta_f = data1.delta_f
psd = aLIGOZeroDetHighPower(flen, delta_f, f_low)

print "L1"
mdcdir = '/home/mcoughlin/STAMP/Magnetar/inj_anteproc/1/injframes_new/A/mag-a/L1/';
mdcchannel = 'L1:STRAIN';
vector = np.zeros((int(Tinj*sampleRate),))
fr_t,ht2 = load_mdc(mdcdir,mdcchannel,vector, dataStartTime, sampleRate)
delta_t = 1/sampleRate
ts2 = pycbc.types.TimeSeries(ht2,delta_t=delta_t,dtype=pycbc.types.float64)

print "Resampling..."
delta_t = 1.0/fs_template
ts2 = pycbc.filter.resample.resample_to_delta_t(ts2, delta_t)

print "Making frequency series..."
data2 = pycbc.filter.make_frequency_series(ts2)

print "Plotting data"
pngFile = os.path.join(plotDir,'data.png')
plt.figure()
plt.plot(data1.sample_frequencies,np.absolute(data1.data))
plt.plot(data2.sample_frequencies,np.absolute(data2.data))
plt.xlim([params["stochtrack"]["fmin"],params["stochtrack"]["fmax"]])
plt.grid()
plt.show()
plt.savefig(pngFile,dpi=200)
plt.close('all')

snr1 = pycbc.filter.sigma(data1,psd=psd,low_frequency_cutoff=f_low,high_frequency_cutoff=f_high)
snr2 = pycbc.filter.sigma(data2,psd=psd,low_frequency_cutoff=f_low,high_frequency_cutoff=f_high)
snrtot = np.sqrt(snr1**2 + snr2**2)
print "SNRs: H1: %.5f, L1: %.5f, Total: %.5f"%(snr1,snr2,snrtot)

print "Generating Templates"
F = 1000
#F = 5
tfs = []
templatesall = []
fs = []
durs = []
for i in xrange(F):
    print i
    f,dur,tf,template = templates.bezier(params,delta_t,sample_frequencies)
    fs.append(f)
    durs.append(dur)
    tfs.append(tf)
    templatesall.append(template)

print "Plotting f(t)"
pngFile = os.path.join(plotDir,'tfs.png')
plt.figure()
#for tf in tfs:
#    plt.plot(B1,tf)
plt.xlim([0,params["stochtrack"]["T"]])
plt.ylim([params["stochtrack"]["fmin"],params["stochtrack"]["fmax"]])
plt.grid()
plt.show()
plt.savefig(pngFile,dpi=200)
plt.close('all')

print "Plotting templates"
pngFile = os.path.join(plotDir,'templates.png')
plt.figure()
for template in templatesall:
    plt.plot(template.sample_frequencies,np.absolute(template.data))
plt.xlim([params["stochtrack"]["fmin"],params["stochtrack"]["fmax"]])
plt.grid()
plt.show()
plt.savefig(pngFile,dpi=200)
plt.close('all')

print "Plotting frequency vs. time"
pngFile = os.path.join(plotDir,'freq_durs.png')
plt.figure()
plt.plot(durs,fs,'k*')
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [s]')
#plt.xlim([0,params["stochtrack"]["T"]])
#plt.ylim([params["stochtrack"]["fmin"],params["stochtrack"]["fmax"]])
plt.grid()
plt.show()
plt.savefig(pngFile,dpi=200)
plt.close('all')

print "Calculating SNRs"
snrs1 = []
snrs2 = []
snrs = []
for template in templatesall:
    snr1,max_id = pycbc.filter.match(data1,template,psd,low_frequency_cutoff=f_low,high_frequency_cutoff=f_high)
    snr1 = np.absolute(snr1)
    snr1 = np.max(snr1)
    snrs1.append(snr1)

    snr2,max_id = pycbc.filter.match(data2,template,psd,low_frequency_cutoff=f_low,high_frequency_cutoff=f_high)
    snr2 = np.absolute(snr2)
    snr2 = np.max(snr2)
    snrs2.append(snr2)

    snrs.append(np.sqrt(snr1**2 + snr2**2))

print "Plotting SNRs"
pngFile = os.path.join(plotDir,'snr.png')
plt.figure()
plt.scatter(durs,fs,s=20,c=np.log10(snrs))
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [s]')
#plt.xlim([0,params["stochtrack"]["T"]])
#plt.ylim([params["stochtrack"]["fmin"],params["stochtrack"]["fmax"]])
plt.colorbar()
plt.grid()
plt.show()
plt.savefig(pngFile,dpi=200)
plt.close('all')


