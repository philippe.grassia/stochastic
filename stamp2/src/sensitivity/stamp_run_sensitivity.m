function stamp_run_sensitivity(params) 

% PSD file
psd = load(params.psdFile);

% get waveform
bestDir = 1;
[t,waveform_original,fs,Nsamp,T] = get_waveform(params,bestDir);

% Apply window?
window = hann(length(t));
window = ones(size(window));
waveform_original = waveform_original .* window';

fprintf('Running %s ...\n',params.waveformType);

% Threshold from background study
th = load(params.backgroundFile);
th = th.th;
fprintf('Running with a threshold of %e\n',th);

params.d = params.d_min;
% create array of distances
done = false;
while ~done
   params.d = [params.d params.increment*params.d(end)];
   if params.d(end) >= params.d_max
      done = true;
   end
end

% automatically add alpha_min=0
d = fliplr(params.d);

% Number of Gaussian noise trials
N = params.N;

SNRs = zeros(N,length(d));

% Range of frequency band
flow = params.flow;
fhigh = params.fhigh;
ht1 = waveform_original';

% Sensitivity Study
for i = 1:N

   %if (mod(i,100) == 0) || i==1
      fprintf('Running trial %d...\n',i);
   %end

   % get waveform
   [t,waveform_original,fs,Nsamp,T] = get_waveform(params,params.doOptimal);
   ht1 = waveform_original';

   % Ask for extra data and then cut it off to the correct length
   vector_original = gaussian_noise(psd,fs,ceil(T+1))';
   vector_original = vector_original(1:Nsamp);

   for j = 1:length(d)

       waveform_vector = waveform_original/d(j);
       vector = (vector_original + waveform_vector).*window';

       ht2 = vector';

       SNR=matchedfilter(ht1, ht2, psd, fs, flow, fhigh);

       SNRs(i,j) = SNR;
       fprintf('SNR for distance %f: %e\n',d(j),SNR);
   end   

end

th_d = -1;

% calculate median snr
snrbar = median(SNRs);
% does median snr exceed threshold?
for ii = 1:length(d)
   fprintf('%f %f %f\n',d(ii),th,snrbar(ii));
   if snrbar(ii)>th
      % is the detection distance outside of the range?
      if d(ii)==max(d)
         fprintf('detection distance exceeds %fe\n', d(ii));
         th_d = -1;
         break;
      else
         fprintf('detection distance = %f\n', d(ii));
      end
      th_d = d(ii);
      break;
   end
end

if th_d < 0
   error(sprintf('No threshold found... please expand limits for %s.\n',params.waveformType))
end

% Create output / plot paths
createpath(params.outputDirectory);
createpath(params.plotDirectory);

save([params.outputDirectory '/' params.waveformType '_' num2str(params.N) '_d.mat'],'th_d','th','SNRs');

% Make a plot
figure;
plot(d, snrbar);
hold on
plot(d(end):1:d(2),th,'r');
hold off
xlabel('distance');
ylabel('snrbar');
pretty;
print('-dpng', [params.plotDirectory '/d_' params.waveformType '_' num2str(params.N) '.png']);


