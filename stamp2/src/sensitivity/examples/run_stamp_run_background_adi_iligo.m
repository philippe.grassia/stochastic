
type = 'ebns_B';
type = 'bns_A';
%type = 'bbh_A';
%type = 'ebbh_B';

params.psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/LIGOsrdPSD_40Hz.txt';
params.gps = 1000000000;

params.outputDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/adi_iligo/output';
params.plotDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/adi_iligo/plots';

% Number of Gaussian noise trials
params.N = 100;
params.N = 100000;

% Number of trials to run at once
params.F = 1000;

% Range of frequency band
params.flow = 100;
params.fhigh = 250;

% Set frequency
params.fs = 512;

waveforms = {'adi-a','adi-b','adi-c','adi-d','adi-e','mono-a','line-a','mono-b','line-b'};

for i = 1:length(waveforms)

   type = waveforms{i};
   type = strrep(type,'-','_');
   type(end) = upper(type(end));

   params.waveformFile = ['/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/waveforms_adi/' type '.dat'];
   params.waveformType = type;

   if ~exist(params.waveformFile)
      continue
   end

   outFile = [params.outputDirectory '/th_' type '_' num2str(params.N) '.mat']; 
   if exist(outFile)
      continue
   end

   stamp_run_background(params);

end

