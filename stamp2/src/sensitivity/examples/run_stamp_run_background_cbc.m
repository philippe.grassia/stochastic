
params.psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd_10Hz.txt';
params.gps = 1000000000;

params.outputDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/cbc/output';
params.plotDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/cbc/plots';

% Number of Gaussian noise trials
params.N = 100;
params.N = 100000;

% Number of trials to run at once
params.F = 1000;

% Range of frequency band
params.flow = 10;
params.fhigh = 150;

% Set frequency
params.fs = 512;

waveforms = {'bns-a','nsbh-a','nsbh-b','bbh-a','bbh-b','bbh-c','ebns-a','ebns-b','ensbh-a','ensbh-b','ensbh-c','ebbh-a','ebbh-b','ebbh-c'};

for i = 1:length(waveforms)

   type = waveforms{i};
   type = strrep(type,'-','_');
   type(end) = upper(type(end));

   params.waveformFile = ['/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/waveforms/' type '.dat'];
   params.waveformType = type;

   if ~exist(params.waveformFile)
      continue
   end

   outFile = [params.outputDirectory '/th_' type '_' num2str(params.N) '.mat']; 
   if exist(outFile)
      continue
   end

   stamp_run_background(params);

end

