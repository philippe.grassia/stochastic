


params.psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd_10Hz.txt';
params.gps = 1000000000;

params.outputDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/cbc/output';
params.plotDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/cbc/plots';

% Number of Gaussian noise trials
params.N = 100;
params.N = 10;

params.background_N = 100000;


% Range of frequency band
params.flow = 10;
params.fhigh = 150;

%stamp_run_background(params);

% Set frequency
params.fs = 512;

waveforms = {'bns-a','nsbh-a','nsbh-b','bbh-a','bbh-b','bbh-c','ebns-a','ebns-b','ensbh-a','ensbh-b','ensbh-c','ebbh-a','ebbh-b','ebbh-c'};

for i = 1:length(waveforms)

   type = waveforms{i};
   type = strrep(type,'-','_');
   type(end) = upper(type(end));

   params.waveformFile = ['/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/waveforms/' type '.dat'];
   params.waveformType = type;

   if ~exist(params.waveformFile)
      continue
   end

   outFile = [params.outputDirectory '/th_' type '_' num2str(params.background_N) '.mat'];
   if ~exist(outFile)
      continue
   end

   params.backgroundFile = outFile;

   if strcmp(type,'bns_A');
      d_min = 500;
   elseif strcmp(type,'bbh_A');
      d_min = 500;
   elseif strcmp(type,'ebbh_B');
      d_min = 500;
   elseif strcmp(type,'ebns_B');
      d_min = 500;
   else
      d_min = 500;
   end
   params.d_min = d_min;
   params.increment = 1.1;
   params.d_max = d_min * params.increment^20;
   
   stamp_run_sensitivity(params);
end 


fprintf('\n\n');
for i = 1:length(waveforms)

   type = waveforms{i};
   type = strrep(type,'-','_');
   type(end) = upper(type(end));
   params.waveformType = type;

   outFile = [params.outputDirectory '/' params.waveformType '_' num2str(params.N) '_d.mat'];

   if exist(outFile)
      data_out = load(outFile);
      fprintf('%s %f %f\n',type,data_out.th,data_out.th_d);
   else
      fprintf('%s %f %f\n',type,NaN, NaN);
   end

end


