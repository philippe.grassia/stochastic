#!/usr/bin/python

"""
%prog

Michael Coughlin (michael.coughlin@ligo.org)

This program does waveform comparisons.

"""

import os, sys, pickle, math, optparse, glob
import matplotlib.pyplot as plt
import numpy as np
#import pe_waveform_comparison_plot, pe_waveform_comparison_utils
from pylal import SimInspiralUtils, Fr
import lal

#import matchedfilter

import pycbc.filter
from pycbc.psd import aLIGOZeroDetHighPower

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def load_mdc(mdcdir,mdcchannel, vector, dataStartTime, sampleRate):

    fr_t = np.arange(len(vector))/sampleRate + dataStartTime
    fr_zeros = np.zeros((len(fr_t),1))

    print 'Adding MDC data to vector... '
    numFrames = 0

    frames = glob.glob(mdcdir + '/*.gwf');
    for frame in frames:

        frameFull = frame.replace('.','-')
        frameSplit = frameFull.split("-")

        gps = float(frameSplit[3])
        dur = float(frameSplit[4])

        gpsStart = gps
        gpsEnd = gps+dur

        if gpsStart > fr_t[-1]:
            continue

        if gpsEnd < fr_t[1]:
            continue

        numFrames = numFrames + 1

        data_out = Fr.frgetvect(frame,mdcchannel)
        data_out = data_out[0]
        fs = len(data_out)/dur
        mdc_t = np.arange(len(data_out))/fs + gps
        mdc_interp = np.interp(fr_t,mdc_t,data_out)
        vector = vector + mdc_interp

    if numFrames > 0:
        print 'data added.'
    else:
        print 'no data added'

    return fr_t,vector

def calcsnr(det1_TimeSeries, det2_TimeSeries):

    f_low = 10.0

    htilde1 = pycbc.filter.make_frequency_series(det1_TimeSeries)
    htilde2 = pycbc.filter.make_frequency_series(det2_TimeSeries)
    f = htilde1.sample_frequencies

    flen = len(htilde1)
    delta_f = htilde1.delta_f

    pngFile = 'python.png'
    plt.figure()
    plt.semilogy(f,np.absolute(htilde1))
    plt.semilogy(f,np.absolute(htilde2))
    plt.xlim([550,650])
    plt.ylim([10**-23,10**-21])
    plt.grid()
    plt.show()
    plt.savefig(pngFile,dpi=200)
    plt.close('all')

    psd = aLIGOZeroDetHighPower(flen, delta_f, f_low)
    sigma1 = pycbc.filter.sigma(htilde1,low_frequency_cutoff=f_low, psd=psd)
    sigma2 = pycbc.filter.sigma(htilde2,low_frequency_cutoff=f_low, psd=psd)

    #sigma1 = pycbc.filter.sigma(det1_TimeSeries,low_frequency_cutoff=f_low, psd=psd)
    #sigma2 = pycbc.filter.sigma(det2_TimeSeries,low_frequency_cutoff=f_low, psd=psd)

    return sigma1, sigma2

psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/LIGOsrdPSD_40Hz.txt'
psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd_10Hz.txt'

flow = 40
fhigh = 1000

psd = np.loadtxt(psdFile)

dataStartTime = 946076555.624748230
sampleRate = 4096.0
mdcdir = '/home/mcoughlin/STAMP/Magnetar/inj_anteproc/1/injframes_new/A/mag-a/H1/';
mdcchannel = 'H1:STRAIN';
vector = np.zeros((250*sampleRate,))
fr_t,ht1 = load_mdc(mdcdir,mdcchannel,vector, dataStartTime, sampleRate)
delta_t = 1/sampleRate

mdcdir = '/home/mcoughlin/STAMP/Magnetar/inj_anteproc/1/injframes_new/A/mag-a/L1/';
mdcchannel = 'L1:STRAIN';
vector = np.zeros((250*sampleRate,))
fr_t,ht2 = load_mdc(mdcdir,mdcchannel,vector, dataStartTime, sampleRate)
delta_t = 1/sampleRate

#TimeSeries = lal.CreateREAL8TimeSeries('timeseries', fr_t[0], 0, delta_t, lal.StrainUnit, len(ht1))
#TimeSeries.data.data = np.copy(ht1)

ts1 = pycbc.types.TimeSeries(ht1,delta_t=delta_t,dtype=pycbc.types.float64)
ts2 = pycbc.types.TimeSeries(ht2,delta_t=delta_t,dtype=pycbc.types.float64)
#htilde1 = pycbc.types.FrequencySeries(np.zeros(len(ht1)/2 + 1), delta_f=1, dtype=pycbc.types.complex128)
#pycbc.fft.fft(ts, htilde1)

snr1,snr2=calcsnr(ts1,ts2)
snrtot = np.sqrt(snr1**2 + snr2**2)

print snr1, snr2, snrtot

