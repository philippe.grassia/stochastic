


params.psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/LIGOsrdPSD_40Hz.txt';
params.gps = 1000000000;

params.backgroundDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/adi_iligo/output';
params.outputDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/adi_iligo_random/output';
params.plotDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/adi_iligo_random/plots';

% Number of Gaussian noise trials
params.N = 100;
params.N = 10;

params.background_N = 100000;


% Range of frequency band
params.flow = 100;
params.fhigh = 250;

% do optimal
params.doOptimal = 0;

%stamp_run_background(params);

% Set frequency
params.fs = 512;

waveforms = {'adi-a','adi-b','adi-c','adi-d','adi-e','mono-a','line-a','mono-b','line-b'};

for i = 1:length(waveforms)

   type = waveforms{i};
   type = strrep(type,'-','_');
   type(end) = upper(type(end));

   params.waveformFile = ['/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/waveforms_adi/' type '.dat'];
   params.waveformType = type;

   if ~exist(params.waveformFile)
      continue
   end

   outFile = [params.backgroundDirectory '/th_' type '_' num2str(params.background_N) '.mat'];

   if ~exist(outFile)
      continue
   end

   params.backgroundFile = outFile;

   outFile = [params.outputDirectory '/' type '_' num2str(params.N) '_d.mat'];

   if exist(outFile)
      continue
   end

   if strcmp(type,'adi_A');
      d_min = 50;
   elseif strcmp(type,'adi_B');
      d_min = 50;
   elseif strcmp(type,'adi_C');
      d_min = 50;
   elseif strcmp(type,'adi_D');
      d_min = 50;
   elseif strcmp(type,'adi_E');
      d_min = 50;
   elseif strcmp(type,'mono_A');
      d_min = 50;
   elseif strcmp(type,'line_A');
      d_min = 50;
   elseif strcmp(type,'mono_B');
      d_min = 50;
   elseif strcmp(type,'line_B');
      d_min = 50;
   else
      d_min = 10;
   end
   params.d_min = d_min;
   params.d_min = d_min/4;
   params.increment = 1.1;
   params.d_max = d_min * params.increment^20;
   
   stamp_run_sensitivity(params);
end 

fprintf('\n\n');
for i = 1:length(waveforms)

   type = waveforms{i};
   type = strrep(type,'-','_');
   type(end) = upper(type(end));
   params.waveformType = type;

   outFile = [params.outputDirectory '/' params.waveformType '_' num2str(params.N) '_d.mat'];

   if exist(outFile)
      data_out = load(outFile);
      fprintf('%s %f %f\n',type,data_out.th,data_out.th_d);
   else
      fprintf('%s %f %f\n',type,NaN, NaN);
   end

end

