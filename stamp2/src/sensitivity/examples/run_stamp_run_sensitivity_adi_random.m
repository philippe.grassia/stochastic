


params.psdFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd_10Hz.txt';
params.gps = 1000000000;

params.backgroundDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/adi/output';
params.outputDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/adi_random/output';
params.plotDirectory = '/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/adi_random/plots';

% Number of Gaussian noise trials
params.N = 100;
%params.N = 10;

params.background_N = 100000;


% Range of frequency band
params.flow = 100;
params.fhigh = 250;

%stamp_run_background(params);

% Set frequency
params.fs = 512;

% do optimal
params.doOptimal = 0;

waveforms = {'adi-a','adi-b','adi-c','adi-d','adi-e','mono-a','line-a','mono-b','line-b'};

for i = 1:length(waveforms)

   type = waveforms{i};
   type = strrep(type,'-','_');
   type(end) = upper(type(end));

   params.waveformFile = ['/home/mcoughlin/stochtrack2/condor_stochsky/matchedfiltering/waveforms_adi/' type '.dat'];
   params.waveformType = type;

   if ~exist(params.waveformFile)
      continue
   end

   outFile = [params.backgroundDirectory '/th_' type '_' num2str(params.background_N) '.mat'];

   if ~exist(outFile)
      continue
   end

   params.backgroundFile = outFile;

   outFile = [params.outputDirectory '/' type '_' num2str(params.N) '_d.mat'];

   if exist(outFile)
      continue
   end

   if strcmp(type,'adi_A');
      d_min = 100;
   elseif strcmp(type,'adi_B');
      d_min = 500;
   elseif strcmp(type,'adi_C');
      d_min = 500;
   elseif strcmp(type,'adi_D');
      d_min = 100;
   elseif strcmp(type,'adi_E');
      d_min = 500;
   elseif strcmp(type,'mono_A');
      d_min = 500;
   elseif strcmp(type,'line_A');
      d_min = 500;
   elseif strcmp(type,'mono_B');
      d_min = 100;
   elseif strcmp(type,'line_B');
      d_min = 100;
   else
      d_min = 100;
   end
   params.d_min = d_min;
   params.d_min = d_min/4;
   params.increment = 1.1;
   params.d_max = d_min * params.increment^20;

   %continue   
   stamp_run_sensitivity(params);
end 

fprintf('\n\n');
for i = 1:length(waveforms)

   waveform = waveforms{i};
   type = waveforms{i};
   type = strrep(type,'-','_');
   type(end) = upper(type(end));
   params.waveformType = type;

   outFile = [params.outputDirectory '/' params.waveformType '_' num2str(params.N) '_d.mat'];


   if exist(outFile)
      data_out = load(outFile);
      %fprintf('%s %f %f\n',type,data_out.th,data_out.th_d);

      if ~(strcmp(waveform,'mono-b') || strcmp(waveform,'line-b') || strcmp(waveform,'mono-a') || strcmp(waveform,'line-a'))
         dd = data_out.th_d;
      else
         dd = 10/data_out.th_d;
      end
      fprintf('%s %f %f\n',type,data_out.th,dd);

   else
      fprintf('%s %f %f\n',type,NaN, NaN);
   end

end

