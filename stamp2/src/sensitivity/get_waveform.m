function [t,waveform_original,fs,Nsamp,T] = get_waveform(params,bestDir)

% Read in waveform
waveform = load(params.waveformFile);
waveform_original = waveform(1:end-1,2)';

% Define GPS time (maybe should change each injection?)
if bestDir
   [ra_max, dec_max, epsilon_max, g] = bestdir(params.gps);
else
   [ra_max, dec_max, epsilon_max, g] = randdir(params.gps);
end

% Determine strain from h+ and hx
waveform_original = (g.F1p*waveform(1:end-1,2) + g.F1x*waveform(1:end-1,3))';

% Time vector
t = waveform(1:end-1,1)';

if t(1) < 0
   t = abs(fliplr(t));
end
T = t(end);
fs = 1/(t(2)-t(1));
t = 0:(1/fs):T;

% Take it to second to last sample
Nsamp = length(t) - 1;
t = t(1:Nsamp);
waveform_original = waveform_original(1:Nsamp);

% Resample if requested
try
   fs = params.fs;
   tnew = 0:(1/fs):T;

   waveform_original = interp1(t,waveform_original,tnew);
   waveform_original(isnan(waveform_original)) = 0;
   t = tnew;

   Nsamp = length(t) - 1;
   t = t(1:Nsamp);
   waveform_original = waveform_original(1:Nsamp);

catch
end


