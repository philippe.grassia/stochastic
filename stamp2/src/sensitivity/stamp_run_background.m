function stamp_run_background(params)

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

% PSD file
psd = load(params.psdFile);

% get waveform
bestDir = 1;
[t,waveform_original,fs,Nsamp,T] = get_waveform(params,bestDir);

% Apply window?
window = hann(length(t));
window = ones(size(window));
waveform_original = waveform_original .* window';

% Number of Gaussian noise trials
N = params.N;
SNRs = zeros(N,1);

% Number of trials to run at once
F = params.F;

% Range of frequency band
flow = params.flow;
fhigh = params.fhigh;
ht1 = waveform_original';

fprintf('Running %s ...\n',params.waveformType);
count = 1;
% Background study
for i = 1:ceil(N/F)

   %if (mod(i,100) == 0) || i==1
      %fprintf('Running trial %d/%d.\n',i,ceil(N/F));
   %end

   % Ask for extra data and then cut it off to the correct length
   vectors = gaussian_noise(psd,fs,ceil(T+1),F)';

   for j = 1:F

      vector = vectors(j,1:Nsamp);
      vector = vector.*window';

      ht2 = vector';

      SNR=matchedfilter(ht1, ht2, psd, fs, flow, fhigh);

      SNRs(count) = SNR;
      count = count + 1;
   end
   
   fprintf('Running trial %d/%d. Max SNR: %f\n',i,ceil(N/F),max(SNRs));
end

th = max(SNRs);

fprintf('Threshold is %f\n',th);

% Create output / plot paths
createpath(params.outputDirectory);
createpath(params.plotDirectory);

save([params.outputDirectory '/th_' params.waveformType '_' num2str(params.N) '.mat'],'th','SNRs');

% make a plot
%bins = min(logls):0.01:max(logls);
figure;
%hist(logls, bins);
hist(SNRs)
xlabel('SNR');
pretty;
print('-dpng', [params.plotDirectory '/logls_' params.waveformType '.png']);

SNRs_ascend = sort(SNRs,'ascend');

pvalues = fliplr((1/length(SNRs_ascend)):(1/length(SNRs_ascend)):1);
figure;
semilogy(SNRs_ascend,pvalues);
hold on
semilogy(min(SNRs):0.1:max(SNRs),0.001,'r')
hold off
xlabel('SNR');
ylabel('p');
pretty;
print('-dpng', [params.plotDirectory '/p_' params.waveformType '.png']);

