function g = getReferenceAntennaFactors(det1, det2, GPSTimes, source, params, g)
% Written by R Quitzow-James

etaExists = isfield(g, 'eta');
if etaExists
  eta = g.eta;
end

startTimeDifference = (params.referenceGPSTime + params.segmentDuration/2) - GPSTimes(1);
referenceTimes = GPSTimes + startTimeDifference;

g = calF(det1, det2, referenceTimes, source, params);

if etaExists
  g.eta = eta;
end
