function [map,mask] = TFNotch(map,params)  
% TFNotch : apply a (T)ime (F)requency Notch to a map
% DESCRIPTION :
%    use TFNotches define in a TFNotchesFile to set all pixel in
%    the TF notches to NaN
%
% SYNTAX :
%   [map,mask] = TFNotch (map,params)
% 
% INPUT : 
%    params : struct of parameters
%    map : map struct, map can be single ifo map (see anteproc)
%          or coherent map (see preproc)
%
% OUTPUT :
%   map : notched  map 
%   mask : logical array where true for notched pixel
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : MAY 2017
%
  
  % check the file exist
  if ~exist(params.TFNotchesFile)
    error('cannot found the TFNotchesFile');
  end
  
  % is single ifo map (from  anteproc) or coherent map (from preproc)
  SIM = ~isfield(map,'naiP1');

  % open field
  fid = fopen(params.TFNotchesFile);
  
  l=fgetl(fid);
  while ischar(l) % loop on the TFNocthes

    % remove comments
    if ~isempty(regexp(l,'^[\ ]*%'))
      l=fgetl(fid);
      continue;
    end

    k = strsplit(l);

    det   = k{1};
    start = str2num(k{2}); 
    stop  = str2num(k{3});
    fmin  = str2num(k{4});
    fmax  = str2num(k{5});

    % ----------------------------------------------------------------
    %% SINGLE IFO MAP
    % ----------------------------------------------------------------
    % we apply the notches only on the ifo map
    %
    if SIM
      if ~strcmp(params.ifo1,det)
        l=fgetl(fid);
        continue
      end

      f = params.flow:params.deltaF:params.fhigh;
      rbtf = map.rbtinfo.flow+map.rbtinfo.deltaF*(0:size(map.rbartilde,1));

      tf_time=map.segstarttime>=start&map.segstarttime<=stop;
      tf_freq_map=f>=fmin&f<=fmax;
      tf_freq_rbt=rbtf>=fmin&rbtf<=fmax;

      mask = zeros(size(map.naiP));
      mask(tf_freq_map,tf_time)=1;

      map.naiP(tf_freq_map,tf_time)      = NaN;
      map.P(tf_freq_map,tf_time)         = NaN;
      map.rbartilde(tf_freq_rbt,tf_time) = NaN;

    else
      % ----------------------------------------------------------------
      %% COHERENT MAP
      % ----------------------------------------------------------------
      % We don't care about ifo, but we apply the notches only on
      % the specific ifo map and the coherent map.
      %

      tf_time=map.segstarttime>=start&map.segstarttime<=stop;
      tf_freq=map.f>=fmin&map.f<=fmax;

      mask = zeros(size(map.naiP));
      mask(tf_freq,tf_time)=1;

      if strcmp(params.ifo1,det)
        map.naiP1(tf_freq,tf_time)      = NaN;
        map.P1(tf_freq,tf_time)         = NaN;
        map.fft1(tf_freq,tf_time)       = NaN;
      else
        map.naiP2(tf_freq,tf_time)      = NaN;
        map.P2(tf_freq,tf_time)         = NaN;
        map.fft2(tf_freq,tf_time)       = NaN;
      end
      map.cc(tf_freq,tf_time)           = NaN;
      map.sensInt(tf_freq,tf_time)      = NaN;
      map.ccVar(tf_freq,tf_time)        = NaN;
      map.segstarttime(tf_freq,tf_time) = NaN;
    end

    l = fgetl(fid);
  end

end

