function gM = gZeros(a, b, params)
% function gM = gZeros(a, b, params)

if params.doGPU
  gM = gpuArray.zeros(a,b,params.gpu.precision);
else 
  gM = zeros(a,b);
end

return
