function gM = gArray(M, params)
% function gM = gArray(M, params)

if params.doGPU
  if strcmp(params.gpu.precision,'single')
    M = single(M);
  end
  gM = gpuArray(M);
else 
  gM = M;
end

return
