function [plist, ptype, pdef, pval, ii] = paramdef_misc(plist, ptype, pdef, pval)
% function [plist, ptype, pdef, pval, ii] = paramdef_misc(plist, ptype, ... 
% pdef, pval)
% support function for paramlist.m.  See that function for more details.
% E Thrane

% set variable counter
ii = length(plist)+1;

plist{ii} = 'intermediate';
ptype{ii} = 'num';
pdef{ii} = 'create SID frames (rarely used)';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'IMScramblePhase';
ptype{ii} = 'num';
pdef{ii} = 'idea of scrambling phase in stamp data; not in wide use anymore';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'intFrameCachePath';
ptype{ii} = 'num';
pdef{ii} = 'cache path for intermediate data (rarely used)';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doSidereal';
ptype{ii} = 'num';
pdef{ii} = 'data must fit into sidereal day (for use with folding)';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'largeShift';
ptype{ii} = 'num';
pdef{ii} = 'not sure';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'largeShiftTime';
ptype{ii} = 'num2';
pdef{ii} = 'not sure';
pval{ii} = 0;
ii=ii+1;

plist{ii} = 'cacheFile';
ptype{ii} = 'str';
pdef{ii} = 'new cache file method: must For large time shift and preproc with start and End GPS';
ii=ii+1;

plist{ii} = 'user';
ptype{ii} = 'str';
pdef{ii} = 'username (for STAMP-PEM)';
ii=ii+1;

plist{ii} = 'darmChannel';
ptype{ii} = 'str';
pdef{ii} = 'h(t) channel (for STAMP-PEM)';
ii=ii+1;

plist{ii} = 'pemChannel';
ptype{ii} = 'str';
pdef{ii} = 'channel to correlate with h(t) channel (for STAMP-PEM)';
ii=ii+1;

plist{ii} = 'dirPath';
ptype{ii} = 'str';
pdef{ii} = 'base path for results (for STAMP-PEM)';
ii=ii+1;

plist{ii} = 'matappsPath';
ptype{ii} = 'str';
pdef{ii} = 'matapps directory path (for STAMP-PEM)';
ii=ii+1;

plist{ii} = 'publicPath';
ptype{ii} = 'str';
pdef{ii} = 'publich html path (for STAMP-PEM)';
ii=ii+1;

plist{ii} = 'runName';
ptype{ii} = 'str';
pdef{ii} = 'analysis name (for STAMP-PEM)';
ii=ii+1;

plist{ii} = 'executableDir';
ptype{ii} = 'str';
pdef{ii} = 'directory that contains stamp-pem executable (for STAMP-PEM)';
ii=ii+1;

plist{ii} = 'fscanPath';
ptype{ii} = 'str';
pdef{ii} = 'directory of fscan results (for stamp-pem comparison)';
ii=ii+1;

plist{ii} = 'segmentFlag';
ptype{ii} = 'str';
pdef{ii} = 'not sure';
ii=ii+1;

plist{ii} = 'fmin';
ptype{ii} = 'num';
pdef{ii} = 'sets minimum frequency For calls to clustermap';
ii=ii+1;

plist{ii} = 'fmax';
ptype{ii} = 'num';
pdef{ii} = 'sets max frequency For calls to clustermap';
ii=ii+1;

plist{ii} = 'channelList';
ptype{ii} = 'str';
pdef{ii} = 'not sure';
ii=ii+1;

return

% use this template for any future additions
plist{ii} = '';
ptype{ii} = '';
pdef{ii} = '';
%pval{ii} = ;
ii=ii+1;
