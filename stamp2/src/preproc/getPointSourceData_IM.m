function params = getPointSourceData_IM(params, dataStartTime, sampleRate)
% function params = getPointSourceData_IM(params, dataStartTime, sampleRate)
%
% Calculates GW time delay between two detectors for a coherent point
% source.  Also calculates antenna factors and converts GW polarizations
% to strain.  Time-shifts detector 2 time-series to account for time delay.
%
% Input:
%   params - parameter struct
%   dataStartTime - start time of current segment
%   sampleRate - data sampling rate (typically 16 kHz)
%
% Output:
%  params - modified parameter struct
%
%  Routine adopted from getPointSourcedata.m written by Stefan Ballmer.
%  Contact: shivaraj@physics.umn.edu, ethrane@ligo.caltech.edu,
%           prestegard@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get the sidereal time for the segment.
siderealtime = GPStoGreenwichMeanSiderealTime(dataStartTime);

% Get detectors.
det1 = getdetector(getsitefromletter(params.ifo1(1)));
det2 = getdetector(getsitefromletter(params.ifo2(1)));

% Get antenna factors.
g = orfIntegrandSymbolic(det1, det2, siderealtime, params.pass.stamp.ra, ...
			 params.pass.stamp.decl);

% Override antenna factors if requested by user.
if params.fixAntennaFactors
  g = fixAntennaFactors(g,1);
  fprintf('Using fixed antenna factors.\n');
elseif params.bestAntennaFactors
  g = bestAntennaFactors(g,1);
  fprintf('Using optimal antenna factors.\n');
end

% STAMP is hard-coded for a coherent source, so there is no need for
% the switch statement that was used previously (now removed).
% Convert GW polarizations into strain.
h1_ra_dec = (params.pass.stamp.hp*g.F1p + params.pass.stamp.hx*g.F1x);
h2_ra_dec = (params.pass.stamp.hp*g.F2p + params.pass.stamp.hx*g.F2x);

% Add sqrt of antenna factors
params.pass.stamp.F1 = sqrt(g.F1p.^2 + g.F1x.^2);
params.pass.stamp.F2 = sqrt(g.F2p.^2 + g.F2x.^2);

% delay the h2 signal with respect to h1 by tau
% the first step is to round tau to one sampling time
g.tau = round(g.tau * sampleRate) / sampleRate;
nsamples = abs(g.tau)*sampleRate;

% add zeros to offset one signal with respect to the other
if g.tau>=0
  params.pass.stamp.h1 = [zeros(nsamples,1) ; h1_ra_dec];
  params.pass.stamp.h2 = [h2_ra_dec ; zeros(nsamples,1)];
else
  params.pass.stamp.h1 = [h1_ra_dec ; zeros(nsamples,1)];
  params.pass.stamp.h2 = [zeros(nsamples,1) ; h2_ra_dec];
end

% scale injection by sqrt(alpha)
params.pass.stamp.h1 = sqrt(params.pass.stamp.alpha)*params.pass.stamp.h1;
params.pass.stamp.h2 = sqrt(params.pass.stamp.alpha)*params.pass.stamp.h2;
fprintf('injection power scaled by %1.1e\n', params.pass.stamp.alpha);

return;
