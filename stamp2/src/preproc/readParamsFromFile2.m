function params = readParamsFromFile2(paramfile, anteproc)
% function params = readParamsFromFile2(paramfile)
% reads in parameters (defined in paramlist) from asci text parameter file
% E Thrane

% load a list of preproc parameters
[plist, ptype, pdef, pval] = paramlist;

% read in parameter pairs (name, value) from asci text file; percentage signs
% act as comments
[names, values]=textread(paramfile, '%s %s\n', -1, 'commentstyle', 'matlab');

% loop over parameter names, assigning values to params struct
for ii=1:length(plist)
  % check if this is a dual channel parameter (in which case ptype ends in 2)
  tmp0 = ptype{ii};
  % if it is, we have to look for three different parameters, corresponding to
  % one for both channels, and separate values for ch1 and ch2
  if strcmp(tmp0(end), '2')
    post = {'', '1', '2'};
  % otherwise there is only one channel to look for
  else
    post = {''};
  end

  % loop over the number of parameter name postfixes
  for kk=1:length(post)
    % match parameters with content of paramfile
    idx = strcmp([plist{ii} post{kk}], names);

    % issue an error message if a parameter is defined more than once
    if sum(idx)>1
      error('%s is defined twice.', [plist{ii} post{kk}]);
    end

    % assign value to param struct
    if sum(idx)==1
      % define useful variables
      tmp = values(idx);
      tmp2 = ptype{ii};
      tmp3 = plist{ii};

      % make special allowance for params.stamp.blah parameters
      if strcmp(tmp3(1:min(6,length(tmp3))), 'stamp.')
        plist{ii} = tmp3(7:end);
        % if ptype==num, convert field to a number
        if strcmp(tmp2(1:3), 'num')
          params.stamp.([plist{ii} post{kk}]) = str2num(tmp{:});
        else
          params.stamp.([plist{ii} post{kk}]) = tmp{:};
        end

      % if we are not dealing with a params.stamp.blah parameter
      else
        % if ptype==num, convert field to a number
        if strcmp(tmp2(1:3), 'num')
          params.([plist{ii} post{kk}]) = str2num(tmp{:});
        else
          params.([plist{ii} post{kk}]) = tmp{:};
        end
      end
      % close loop: sum(idx)==1
    end

    % assign default value if parameter is not assigned and if default value 
    % exists...
    if sum(idx)==0 && ~isempty(pval{ii})
      % ...but only for post='1' or post='2'...not for post=''
      if strcmp(post{kk}, '1') || strcmp(post{kk}, '2')
        params.([plist{ii} post{kk}]) = pval{ii};
      end
      % ...or if there is only post=''
      if length(post)==1
        params.([plist{ii} post{kk}]) = pval{ii};
      end
    end
  end
end

% if we are running anteproc (instead of preproc), then assign dummy values for
% detector two quantities
if anteproc
  params = anteprocDefaults2(params);
end

% check that parameters are correctly set
params = paramCheckPreproc(params);

return
