function [map, params] = compressmap(map, params, pp)
% function [map, params] = compressmap(map, params, pp)
% performs time-averaging to create a map with fewer segments.  each new 
% segment is called a vlong (very long) segment.  The number of (overlapping) 
% original segments in each new vlong segment is params.npixels.  E.g., if you
% want 158s of overlapping 1s segments, then npixels=315.

% first, check that necessary parameter is defined
try
  params.npixels;
catch
  error('params.npixels must be defined to call compressmap.');
end
% check also that it is an integer
if params.npixels-floor(params.npixels) > 0
  error('params.npixels must take an integer value');
end
% check also that it >=2
if params.npixels<2
  error('params.npixels must be >=2');
end
% check that we avoid over-compression from rotation of Earth effects
% see: https://dcc.ligo.org/LIGO-T1400559
if params.npixels > (2*158-1) * (1000/params.fhigh)
  fprintf('WARNING (compressmap.m): data may be over-compressed!\n');
end

% calculate y, z, and sigma
[map, params] =  calyz_forallsky(map, params, pp);

% calcualte the number of averages and loop over vlong segments
params.navg = floor(size(map.y,2)/params.npixels);
for jj=1:params.navg
  aa = 1 + (jj-1)*(params.npixels);
  bb = aa+params.npixels-1;
  % compressed data
  map0.y(:,jj) = mean(map.y(:,aa:bb), 2);
  map0.z(:,jj) = mean(map.z(:,aa:bb), 2);
  map0.sigma(:,jj) = mean(map.sigma(:,aa:bb), 2);
  map0.segstarttime(jj) = map.segstarttime(aa);
  % other compressed data that might be useful
  % do compress: naiP1, naiP2, fft1, fft2
  % do not compress: P1, P2, cc, sensInt, ccVar
  map0.naiP1 = mean(map.naiP1(:,aa:bb), 2);
  map0.naiP2 = mean(map.naiP2(:,aa:bb), 2);
  % diagnostic tools used when creating this code
%  [aa bb]
%  fprintf('%1.1f %1.1f\n', map.segstarttime(aa), map.segstarttime(bb));
end

% copy metadata and replace original map with compressed map
map0.f = map.f;
map0.segDur = map.segDur * params.npixels / 2;
map0.segDur0 = map.segDur;
map0.deltaF = map.deltaF;
map0.start = map.start;

% calculate new map duration two ways and make sure they agree
map0.dur = map.segstarttime(bb) - map.segstarttime(1) + map.segDur;
durcheck = params.navg*((params.npixels+1)/2) - (params.navg-1)*map.segDur/2;
if durcheck ~= map0.dur
  fprintf('WARNING: UNEXPECTED DURATION ENCOUNTERED IN COMPRESSMAP.M\n');
end

% check if any data has been left out
if map0.dur ~= map.dur
  fprintf('WARNING: some data has been left out during compression...\n');
  fprintf('old map duration = %1.1fs\n', map.dur);
  fprintf('new map duration = %1.1fs\n', map0.dur);
  fprintf('%1.1fs not included in compression\n', map.dur-map0.dur);
end

% the following assume non-overlapping vlong segments
map0.xvals = map0.segstarttime+map0.segDur/2;
map0.yvals = map.f;
map0.npixels = params.npixels;
map0.navg = params.navg;
map = map0;

return
