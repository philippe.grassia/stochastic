function [plist, ptype, pdef, pval] = paramlist
% function [plist, ptype, pdef, pval] = paramslist
% paramlist completely characterizes all preproc parameters.  each parameter 
% is described by several variables as demonstrated by the test variable
%   plist{1} = 'test';  % parameter name
%   ptype{1} = 'str';   % parameter type: str, num, str2, num2
%   pdef{1} = '...'     % parameter definition
%   pval{1} = true;     % default value (if applicable)
% str = strings
% num = numbers (floats, ints, etc.)
% str2 = strings that should be applied to channels 1 and 2, e.g., 
%   resampleRate -> resampleRate1 and resampleRate2
% num2 = same as str2 but for numbers
%
% READ ME: please be courteous when editing this code.  Make sure to carefully
% follow the prototype convention.  Include all relevant commands.  Make sure
% sure sure to incldue the ii=ii+1 command as noted below.  Check that the code
% still works before svn committing.  Finally, put related parameters into 
% blocks.  Thank you!
% E Thrane

% initialize variable counter
ii = 1;

% define protypical parameter--------------------------------------------------
% parameter name
plist{1} = 'test';
% parameter type: str, num, str2, num2
ptype{1} = 'num';
% parameter definition
pdef{1} = 'This is the first (prototypical) variable.';
% default value (if applicable)
pval{1} = true;
% this command follows after EVERY parameter definition
ii=ii+1;

% standard stochastic.m parameters---------------------------------------------
plist{ii} = 'doHighPass';
ptype{ii} = 'num2';
pdef{ii} = 'Boolean: should the data be high-pass filtered?  Used to define doHighPass1 and doHighPass2.';
ii=ii+1;

plist{ii} = 'resampleRate';
ptype{ii} = 'num2';
pdef{ii} = 'Boolean: should the data be resampled?  Used to define resampleRate1 and resampleRate2';
ii=ii+1;

plist{ii} = 'bufferSecs';
ptype{ii} = 'num2';
pdef{ii} = 'Number of buffer seconds; default is 2s. Used to define bufferSecs1 and bufferSecs2';
ii=ii+1;

plist{ii} = 'ASQchannel';
ptype{ii} = 'str2';
pdef{ii} = 'strain channel name';
ii=ii+1;

plist{ii} = 'framePath';
ptype{ii} = 'str2';
pdef{ii} = 'path to frame files';
ii=ii+1;

plist{ii} = 'frameType';
ptype{ii} = 'str2';
pdef{ii} = 'frame type, e.g., H1_RDS_C03_L2';
ii=ii+1;

plist{ii} = 'frameDuration';
ptype{ii} = 'num2';
pdef{ii} = 'duration of frame, usually not defined';
pval{ii} = -1;
ii=ii+1;

plist{ii} = 'hannDuration';
ptype{ii} = 'num2';
pdef{ii} = 'duration of Hann window';
ii=ii+1;

plist{ii} = 'nResample';
ptype{ii} = 'num2';
pdef{ii} = 'first resampling parameter';
ii=ii+1;

plist{ii} = 'betaParam';
ptype{ii} = 'num2';
pdef{ii} = 'second resampling parameter';
ii=ii+1;

plist{ii} = 'highPassFreq';
ptype{ii} = 'num2';
pdef{ii} = 'knee frequency for high-pass filter';
ii=ii+1;

plist{ii} = 'highPassOrder';
ptype{ii} = 'num2';
pdef{ii} = 'determines steepness of high-pass filter';
ii=ii+1;

plist{ii} = 'simOmegaRef';
ptype{ii} = 'num';
pdef{ii} = 'this variable is no longer in common use';
pval{ii} = 0;
ii=ii+1;

plist{ii} = 'doShift';
ptype{ii} = 'num2';
pdef{ii} = 'whether to shift time series';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doBadGPSTimes';
ptype{ii} = 'num';
pdef{ii} = 'exclude some GPS times from analysis?';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'heterodyned';
ptype{ii} = 'num';
pdef{ii} = 'data is heterodyned';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doOverlap';
ptype{ii} = 'num';
pdef{ii} = 'use overlapping segments';
% EHT: changed default from false to true to reflect most common usage 10/9/14
pval{ii} = true;
ii=ii+1;

plist{ii} = 'ifo';
ptype{ii} = 'str2';
pdef{ii} = 'detector name';
ii=ii+1;

plist{ii} = 'segmentDuration';
ptype{ii} = 'num';
pdef{ii} = 'Segment duration\n';
ii=ii+1;

plist{ii} = 'numSegmentsPerInterval';
ptype{ii} = 'num';
pdef{ii} = 'number of segments to calculate point estimate and sigma';
ii=ii+1;

plist{ii} = 'ignoreMidSegment';
ptype{ii} = 'num';
pdef{ii} = 'avoid mid segment in background estimation';
pval{ii} = true;
ii=ii+1;

plist{ii} = 'deltaF';
ptype{ii} = 'num';
pdef{ii} = 'frequency resolution';
ii=ii+1;

plist{ii} = 'flow';
ptype{ii} = 'num';
pdef{ii} = 'first frequency bin';
ii=ii+1;

plist{ii} = 'fhigh';
ptype{ii} = 'num';
pdef{ii} = 'last frequency bin';
ii=ii+1;

plist{ii} = 'ShiftTime';
ptype{ii} = 'num2';
pdef{ii} = 'shift channel by this amount of seconds';
pval{ii} = 0;
ii=ii+1;

plist{ii} = 'doFreqMask';
ptype{ii} = 'num';
pdef{ii} = 'notch list of frequencies';
% EHT: new default is to not apply mask unless requested 10/9/2014
pval{ii} = false;
ii=ii+1;

plist{ii} = 'doStampFreqMask';
ptype{ii} = 'num';
pdef{ii} = 'notch list of frequencies';
pval{ii} = true;
ii=ii+1;


plist{ii} = 'doVLT';
ptype{ii} = 'num';
pdef{ii} = 'flag for the VLT search';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'psdtype';
ptype{ii} = 'str';
pdef{ii} = 'flag for the VLT search';
pval{ii} = 'default';
ii=ii+1;

plist{ii} = 'nbThresh';
ptype{ii} = 'num';
pdef{ii} = 'Narrowband cut Threshold';
pval{ii} = 700;
ii=ii+1;

plist{ii} = 'coarseGrainNotch';
ptype{ii} = 'num';
pdef{ii} = 'Enables a scheme to coarseGrain after a notch; useful if notching is done at finer frequencies then required for the search';
pval{ii} = false;
ii = ii+1;

plist{ii} = 'doLineRemoval';
ptype{ii} = 'num';
pdef{ii} = 'notch list of frequencies (removes in time-domain)';
% EHT: new default is to not apply mask unless requested 10/9/2014
pval{ii} = false;
ii=ii+1;

plist{ii} = 'freqsToRemove';
ptype{ii} = 'num';
pdef{ii} = 'list of comma-separated frequencies to remove';
pval{ii} = [];
ii=ii+1;

plist{ii} = 'nBinsToRemove';
ptype{ii} = 'num';
pdef{ii} = 'how many bins to remove around each notch';
pval{ii} = [];
ii=ii+1;

plist{ii} = 'lineFile1';
ptype{ii} = 'str2';
pdef{ii} = 'file listing freq to remove';
pval{ii} = '';
ii=ii+1;

plist{ii} = 'lineFile2';
ptype{ii} = 'str2';
pdef{ii} = 'file listing freq to remove';
pval{ii} = '';
ii=ii+1;

plist{ii} = 'respTime1';
ptype{ii} = 'num';
pdef{ii} = 'time response for the iwave algo';
pval{ii} = '';
ii=ii+1;

plist{ii} = 'respTime2';
ptype{ii} = 'num';
pdef{ii} = 'time response for the iwave algo';
pval{ii} = '';
ii=ii+1;

plist{ii} = 'gpsTimesPath';
ptype{ii} = 'str2';
pdef{ii} = 'path to GPS time cache files';
pval{ii} = '<auto>';
ii=ii+1;

plist{ii} = 'frameCachePath';
ptype{ii} = 'str2';
pdef{ii} = 'path to fraem cache files';
ii=ii+1;

plist{ii} = 'badGPSTimesFile';
ptype{ii} = 'str';
pdef{ii} = 'file listing bad GPS times';
pval{ii} = '';
ii=ii+1;

plist{ii} = 'suppressFrWarnings';
ptype{ii} = 'num';
pdef{ii} = 'turn off frgetvect warnings';
pval{ii} = false;
ii=ii+1;

% additional parameters organized in separate functions
[plist, ptype, pdef, pval, ii] = paramdef_stoch(plist, ptype, pdef, pval);
[plist, ptype, pdef, pval, ii] = paramdef_misc(plist, ptype, pdef, pval);
[plist, ptype, pdef, pval, ii] = paramdef_mc(plist, ptype, pdef, pval);
[plist, ptype, pdef, pval, ii] = paramdef_stamp(plist, ptype, pdef, pval);

% parameters for reference time for injecting into data with a jobfile time-shift
plist{ii} = 'useReferenceAntennaFactors';
ptype{ii} = 'num';
pdef{ii} = 'enable/disable use of reference time for calculating antenna factors and time delay between detectors';
pval{ii} = false;
ii=ii+1;

plist{ii} = 'referenceGPSTime';
ptype{ii} = 'num';
pdef{ii} = 'reference time for calculating antenna factors and time delay between detectors';
ii=ii+1;

% DO NOT ADD ANYTHING AFTER THESE LINES----------------------------------------
plist{ii} = 'paramlist';
ptype{ii} = 'str';
pdef{ii} = 'this parameter ensures the right number of pval entries';
pval{ii} = 'true';
ii=ii+1;
% check parameters
[plist, ptype, pdef, pval] = paramdef_check(plist, ptype, pdef, pval);

return


% use this template for any future additions
plist{ii} = '';
ptype{ii} = '';
pdef{ii} = '';
%pval{ii} = ;
ii=ii+1;
