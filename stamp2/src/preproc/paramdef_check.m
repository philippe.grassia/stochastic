function [plist, ptype, pdef, pval] = paramdef_check(plist, ptype, pdef, pval);
% function [plist, ptype, pdef, pval] = paramdef_check(plist, ptype, ...
%   pdef, pval);
% checks that the parameters defined in paramlist.m are sensibly defined
% E Thrane

% first check for duplicate parameter definitions
tmp = tabulate(plist);
n = cell2mat(tmp(:, 2));
if any(n>1)
  fprintf('paramdef_check has found %s is defined twice!\n', plist{n>1});
  error('Edit the paramdef code to eliminate this error message.');
end

% next, make sure that ptype is a recognized data type
a = strcmp(ptype, 'num');
b = strcmp(ptype, 'str');
c = strcmp(ptype, 'num2');
d = strcmp(ptype, 'str2');
x = a|b|c|d;
if sum(x)<length(x)
  fprintf('%s\n', plist{x==0});
  error('Edit the paramdef code to eliminate this error message.');
end

return
