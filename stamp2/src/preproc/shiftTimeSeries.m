function h_shift=shiftTimeSeries(h0,fsample,tau)
% function h_shift=shiftTimeSeries(h0,fsample,tau)
% This function shifts the time series of hplus and hcross to take into account
% the time shift tau between the two detectors. It returns the real part only.
%
% Routine by E. Thrane
% Contact ethrane@physics.umn.edu

NFFT=length(h0);
h0_tilde=fft(h0)/NFFT;
deltaF=fsample/NFFT;
temp=fftshift([0,(-NFFT/2+1):(NFFT/2-1)])'; 
fdoublesided=temp*deltaF;

% multiply h0_tilde by exp(2pi*i*f*tau) to transform h0(t) to h0(t+tau)
Tf=exp(2*pi*i*fdoublesided*tau);
h0_tilde_shift=Tf.*h0_tilde;

% ifft the new h0_tilde to get the new h
h_shift=ifft(h0_tilde_shift)*NFFT;

% take the real part to eliminate small imaginary numbers
h_shift = real(h_shift);

return

