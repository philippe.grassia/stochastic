function masterCache()
% function masterCache()
% This script combines all the S5 cachefiles for H1 and L1 into a single struct
% called cache and saved as cache.mat
% Originally by E. Thrane and modified by S. Kandhasamy
% Contact shivaraj@physics.umn.edu

gpsTimesPath1 = '/archive/home/ethrane/cache/S5H1L1/';
gpsTimesPath2 = '/archive/home/ethrane/cache/S5H1L1/';
frameCachePath1 = '/archive/home/ethrane/cache/S5H1L1/';
frameCachePath2 = '/archive/home/ethrane/cache/S5H1L1/';


NJobs = 18837;    % total number of in S5 H1L1 job file
site = ['H1' 'L1']; % sites for ifos to cross-correlate

% initialize cache struct
cache.frames1 = '';
cache.frames2 = '';
cache.gps1 = [];
cache.gps2 = [];

NFail = 0;        % keep track of number of failed jobs
for ii=1:NJobs
  try
    % load cachefiles
    fprintf('job no %d \n',ii);
    frames1 = textread([frameCachePath1 'frameFiles' site(1) '.' num2str(ii) ...
					'.txt'],'%s');
    frames2 = textread([frameCachePath2 'frameFiles' site(3) '.' num2str(ii) ...
					'.txt'],'%s');
    gps1 = load([gpsTimesPath1 'gpsTimes' site(1) '.' num2str(ii) '.txt']);
    gps2 = load([gpsTimesPath2 'gpsTimes' site(3) '.' num2str(ii) '.txt']);

    % fill cache struct
    cache.frames1 = [cache.frames1 ; frames1];
    cache.frames2 = [cache.frames2 ; frames2];
    cache.gps1 = [cache.gps1 ; gps1];
    cache.gps2 = [cache.gps2 ; gps2];
  catch
    fprintf('Error with job %i; Check the cache files\n',ii);
    NFail = NFail + 1;
  end
end

% save metadata
cache.site = site;
cache.NJobs = NJobs;
cache.path1 = frameCachePath1;
cache.path2 = frameCachePath2;

% clean-up: now remove duplicate gps times so that the frame and gps arrays
% contain only unique gps times
[nouse1, cut1, nouse2] = unique(cache.gps1);
cache.gps1 = cache.gps1(cut1);
cache.frames1 = {cache.frames1{cut1}};

[nouse1, cut2, nouse2] = unique(cache.gps2);
cache.gps2 = cache.gps2(cut2);
cache.frames2 = {cache.frames2{cut2}};

% get frame durations 
[nouse1, cache.dur1] = decodeFrameNames(cache.frames1');
[nouse2, cache.dur2] = decodeFrameNames(cache.frames2');

% save to mat file
save('cache.mat','cache');

return

