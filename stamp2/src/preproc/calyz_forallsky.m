function [map, params] =  calyz_forallsky(map, params, pp)
% function [map, params] = calyz_forallsky(map, params, pp)
% calculate map.y, map.z, and map.sigma in preparation for an all-sky search

% det structs
[det1 det2] = ifo2dets(params);

% ccspec struct
ccSpec.data = map.cc;
ccSpec.flow = map.f(1);
ccSpec.deltaF = pp.deltaF;

% sensInt struct
sensInt.flow = map.f(1);
sensInt.deltaF = pp.deltaF;
sensInt.data = map.sensInt;

% set (ra,dec)=(0,0) since it does not matter anyway for all-sky search
source=[0 0];

% set speed of light
params.c=3e8;

% set default parameters for calculation of y, z, and sigma
params.doPolar=false; params.purePlus=false; params.pureCross=false;
params.doPolarInjection=false; params.alternative_sigma=false;
params.fixAntennaFactors=false; params.bestAntennaFactors=false;

% for all-sky search, we always set NoTimeDelay=true
params.NoTimeDelay=true;

% set frequency range
params.flow = map.f(1);
params.fhigh = map.f(end);

% calculate y and sigma the usual way
map = ccSpecReadout_stamp(det1, det2, map.segstarttime, ...
  source, ccSpec, map.ccVar, sensInt, params, pp, map);

return
