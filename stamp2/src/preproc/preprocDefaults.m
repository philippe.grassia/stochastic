function params = preprocDefaults(params)
% function params = preprocDefaults(params)
% This function sets defaults parameters for preproc.m
% See also: readParamsFromFile
% Note: parameters are organized by category in coordination with
%   readParamsFromFile.  If you make modifications, please follow the existing 
%   organization.  Thanks!
%
% Routine by E. Thrane, S. kandhasamy, T. Prestegard, M. Coughlin
% Contact ethrane@physics.umn.edu, shivaraj@physics.umn.edu

%--------------------------------------------------------------------------
% for stochastic.m and STAMP
%--------------------------------------------------------------------------
try
  params.doShift1 = params.doShift1;
catch
  params.doShift1 = false;
end

try
  params.doShift2 = params.doShift2;
catch
  params.doShift2 = false;
end

try
  params.doBadGPSTimes = params.doBadGPSTimes;
catch
  params.doBadGPSTimes = false;
end

try
  params.heterodyned = params.heterodyned;
catch
  params.heterodyned = false;
end

try
  params.doOverlap = params.doOverlap;
catch
  params.doOverlap = false;
end

try
  params.ifo1 = params.ifo1;
catch
  error('params.ifo1 parameter not set');
end

try
  params.ifo2 = params.ifo2;
catch
  error('params.ifo2 parameter not set');
end

try
  params.segmentDuration = params.segmentDuration;
catch
  error('params.segmentDuration parameter not set');
end

try
  params.numSegmentsPerInterval = params.numSegmentsPerInterval;
catch
  error('params.numSegmentsPerInterval parameter not set');
end

try
  params.ignoreMidSegment = params.ignoreMidSegment;
catch
  error('params.ignoreMidSegment parameter not set');
end

try
  params.deltaF = params.deltaF;
catch
  error('params.deltaF parameter not set');
end

try
  params.flow = params.flow;
catch
  error('params.flow parameter not set');
end

try
  params.fhigh = params.fhigh;
catch
  error('params.fhigh parameter not set');
end

try
  params.ShiftTime1 = params.ShiftTime1;
catch
  params.ShiftTime1 = 0;
end

try
  params.ShiftTime2 = params.ShiftTime2;
catch
  params.ShiftTime2 = 0;
end

try
  params.resampleRate1 = params.resampleRate1;
catch
  error('params.resampleRate1 parameter not set');
end

try
  params.resampleRate2 = params.resampleRate2;
catch
  error('params.resampleRate2 parameter not set');
end

try
  params.bufferSecs1 = params.bufferSecs1;
catch
  error('params.bufferSecs1 parameter not set');
end

try
  params.bufferSecs2 = params.bufferSecs2;
catch
  error('params.bufferSecs2 parameter not set');
end

try
  params.ASQchannel1 = params.ASQchannel1;
catch
  error('params.ASQchannel1 parameter not set');
end

try
  params.ASQchannel2 = params.ASQchannel2;
catch
  error('params.ASQchannel2 parameter not set');
end

try
  params.frameType1 = params.frameType1;
catch
  error('params.frameType1 parameter not set');
end

try
  params.frameType2 = params.frameType2;
catch
  error('params.frameType2 parameter not set');
end

try
  params.frameType1;
  params.frameType2;
catch
  error('frameType1 and/or frameType2 not defined');
end

try
  params.frameDuration1 = params.frameDuration1;
catch
  params.frameDuration1 = -1;
end

try
  params.frameDuration2 = params.frameDuration2;
catch
  params.frameDuration2 = -1;
end

try
  params.hannDuration1 = params.hannDuration1;
catch
  error('params.hannDuration1 parameter not set');
end

try
  params.hannDuration2 = params.hannDuration2;
catch
  error('params.hannDuration2 parameter not set');
end

try
  params.nResample1 = params.nResample1;
catch
  error('params.nResample1 parameter not set');
end

try
  params.nResample2 = params.nResample2;
catch
  error('params.nResample2 parameter not set');
end

try
  params.betaParam1 = params.betaParam1;
catch
  error('params.betaParam1 parameter not set');
end

try
  params.betaParam2 = params.betaParam2;
catch
  error('params.betaParam2 parameter not set');
end

try
  params.doHighPass1 = params.doHighPass1;
catch
  error('doHighpass1 parameter not set');
end

try
  params.doHighPass2 = params.doHighPass2;
catch
 error('doHighpass2 parameter not set');
end

try
  params.highPassFreq1 = params.highPassFreq1;
catch
  error('params.highPassFreq1 parameter not set');
end

try
  params.highPassFreq2 = params.highPassFreq2;
catch
  error('params.highPassFreq2 parameter not set');
end

try
  params.highPassOrder1 = params.highPassOrder1;
catch
  error('params.highPassOrder1 parameter not set');
end

try
  params.highPassOrder2 = params.highPassOrder2;
catch
  error('params.highPassOrder2 parameter not set');
end

try
  params.freqsToRemove = params.freqsToRemove;
catch
  error('params.freqsToRemove parameter not set');
end

try
  params.nBinsToRemove = params.nBinsToRemove;
catch
  error('params.nBinsToRemove parameter not set');
end

try
  params.gpsTimesPath1 = params.gpsTimesPath1;
catch
  params.gpsTimesPath1 = '<auto>';
end

try
  params.gpsTimesPath2 = params.gpsTimesPath2;
catch
  params.gpsTimesPath2 = '<auto>';
end

try
  params.frameCachePath1 = params.frameCachePath1;
catch
  error('params.frameCachePath1 parameter not set');
end

try
  params.frameCachePath2 = params.frameCachePath2;
catch
  error('params.frameCachePath2 parameter not set');
end

try
  params.badGPSTimesFile = params.badGPSTimesFile;
catch
  params.badGPSTimesFile = '';
end

try
  params.doFreqMask = params.doFreqMask;
catch
  error('params.doFreqMask parameter not set');
end

%------------------------------------------------------------------------------
% for stochastic.m only
%------------------------------------------------------------------------------
try
  params.doDirectional=params.doDirectional;
catch
  params.doDirectional=false;
end

try
  params.doSphericalHarmonics;
catch
  params.doSphericalHarmonics=false;
end

try
  params.doNarrowbandRadiometer=params.doNarrowbandRadiometer;
catch
  params.doNarrowbandRadiometer=false;
end

try
  if ~params.doDirectional
    params.doAllSkyComparison=false;
  end
catch
  params.doAllSkyComparison=false;
end

try
  params.SpHFreqIntFlag;
catch
  params.SpHFreqIntFlag=false;
end

if params.doSphericalHarmonics
  try
    params.SpHLmax;
  catch
    error('SpHLmax must be defined for doSphericalHarmonics');
  end
end

if params.doSphericalHarmonics
  try
    params.gammaLM_coeffsPath;
  catch
    error('gammaLM_coeffsPath must be defined for doSphericalHarmonics');
  end
end

if params.doDirectional
  try
    params.useSkyPatternFile;
  catch
    params.useSkyPatternFile=false;
  end
  % no way to write the uge amount of data to screen
  params.writeResultsToScreen=false;
  try
    useparams.SkyPatternFile=params.useparams.SkyPatternFile;
  catch
    useparams.SkyPatternFile=false;
  end

  try
    params.SkyPatternFile = params.SkyPatternFile;
  catch
    if useparams.SkyPatternFile
      error('params.SkyPatternFile parameter not set');
    else
      params.SkyPatternFile = '';
    end
  end

  try
    params.SkyPatternRightAscensionNumPoints = ...
      params.SkyPatternRightAscensionNumPoints;
  catch
    error('params.SkyPatternRightAscensionNumPoints parameter not set');
  end

  try
    params.SkyPatternDeclinationNumPoints = ...
      params.SkyPatternDeclinationNumPoints;
  catch
    error('params.SkyPatternDeclinationNumPoints parameter not set');
  end

  try
    params.maxCorrelationTimeShift=params.maxCorrelationTimeShift;
  catch
    error('params.maxCorrelationTimeShift parameter not set');
  end

  try
    params.UnphysicalTimeShift=params.UnphysicalTimeShift;
  catch
    params.UnphysicalTimeShift=0;
  end

  try
    params.minMCoff = params.minMCoff;
  catch
    params.minMCoff = 0.1;
  end

  try
    params.maxMCoff = params.maxMCoff;
  catch
    params.maxMCoff = 0.6;
  end

  try
    params.ConstTimeShift = params.ConstTimeShift;
  catch
    params.ConstTimeShift = 0;
  end

  if params.UnphysicalTimeShift~=0
    warning('params.UnphysicalTimeShift~0; result will be garbage!');
  end

else
  useparams.SkyPatternFile=0;
  params.SkyPatternFile='';
  params.SkyPatternRightAscensionNumPoints=0;
  params.SkyPatternDeclinationNumPoints=0;
  params.maxCorrelationTimeShift=0;
  params.UnphysicalTimeShift=0;
end

% intermediate data frames only ###############################################
try
  params.intermediate;
catch
  params.intermediate = false;
end

if params.intermediate
  try
    params.intFrameCachePath;
  catch
    error('intFrameCachePath not defined');
  end
end

try
  params.doSidereal=params.doSidereal;
catch
  params.doSidereal=false;
end

try
  params.doMonteCarlo = params.doMonteCarlo;
catch
  params.doMonteCarlo = false;
end

try
  params.doSimulatedPointSource = params.doSimulatedPointSource;
catch
  params.doSimulatedPointSource = false;
end

try
  params.doMCoffset = params.doMCoffset;
catch
  params.doMCoffset = false;
end

try
  params.doSimulatedSkyMap;
catch
  params.doSimulatedSkyMap = false;
end

if params.doSimulatedPointSource
  try
    params.simulationPath=params.simulationPath;
  catch
    error('params.simulationPath parameter not set');
  end
  try
    params.simulatedPointSourcesFile=params.simulatedPointSourcesFile;
  catch
    error('params.simulatedPointSourcesFile parameter not set');
  end
  try
    params.simulatedPointSourcesPowerSpec;
  catch
    error('params.simulatedPointSourcesPowerSpec parameter not set');
  end
  try
    params.simulatedPointSourcesInterpolateLogarithmic;
  catch
    params.simulatedPointSourcesInterpolateLogarithmic=true;
  end
  try
    params.simulatedPointSourcesBufferDepth;
  catch
    error('params.simulatedPointSourcesBufferDepth parameter not set');
  end
  try
    simulatedPointSourcesHalparams.fRefillLength;
  catch
    error('simulatedPointSourcesHalparams.fRefillLength parameter not set');
  end
  try
    params.simulatedPointSourcesNoRealData;
  catch
    params.simulatedPointSourcesNoRealData=false;
  end
  try
    params.simulatedPointSourcesMakeIncoherent;
  catch
    params.simulatedPointSourcesMakeIncoherent=0;
  end
else
  params.simulationPath='';
  params.simulatedPointSourcesFile='';
  params.simulatedPointSourcesPowerSpec='';
  params.simulatedPointSourcesInterpolateLogarithmic=true;
  params.simulatedPointSourcesBufferDepth=0;
  simulatedPointSourcesHalparams.fRefillLength=0;
  params.simulatedPointSourcesNoRealData=false;
  params.simulatedPointSourcesMakeIncoherent=0;
end

try
  params.doSimulatedDetectorNoise;
catch
  params.doSimulatedDetectorNoise=false;
end

try
  params.numTrials = params.numTrials;
catch
  params.numTrials = 1;
end

try
  params.signalType = params.signalType;
catch
  params.signalType = 'const';
end

try
  params.simOmegaRef = params.simOmegaRef;
catch
  params.simOmegaRef = 0;
end
%##############################################################################

try
  params.doCombine = params.doCombine;
catch
  params.doCombine = false;
end

try
  params.writeResultsToScreen = params.writeResultsToScreen;
catch
  params.writeResultsToScreen = false;
end

try
  params.writeStatsToFiles = params.writeStatsToFiles;
catch
  params.writeStatsToFiles = false;
end

try
  params.writeNaiveSigmasToFiles = params.writeNaiveSigmasToFiles;
catch
  params.writeNaiveSigmasToFiles = false;
end;

try
  params.writeSpectraToFiles = params.writeSpectraToFiles;
catch
  params.writeSpectraToFiles = false;
end

try
  params.writeSensIntsToFiles = params.writeSensIntsToFiles;
catch
  params.writeSensIntsToFiles = false;
end

try
    params.writeCoherenceToFiles = params.writeCoherenceToFiles;
catch
    params.writeCoherenceToFiles = false;
end

try
  params.writeOptimalFiltersToFiles = params.writeOptimalFiltersToFiles;
catch
  params.writeOptimalFiltersToFiles = false;
end

try
  params.writeOverlapReductionFunctionToFiles = params.writeOverlapReductionFunctionToFiles;
catch
  params.writeOverlapReductionFunctionToFiles = false;
end

%if not(params.doDirectional)
% currently sot supported (ORF is now unity...)
  params.writeOverlapReductionFunctionToFiles = false;
%end;

try
  params.writeCalPSD1sToFiles = params.writeCalPSD1sToFiles;
catch
  params.writeCalPSD1sToFiles = false;
end

try
  params.writeCalPSD2sToFiles = params.writeCalPSD2sToFiles;
catch
  params.writeCalPSD2sToFiles = false;
end

try
  params.doTimingTransientSubtraction1 = params.doTimingTransientSubtraction1;
  params.TimingTransientFile1          = params.TimingTransientFile1;
catch
  params.doTimingTransientSubtraction1 = false;
  params.TimingTransientFile1          = '';
end

try
  params.doTimingTransientSubtraction2 = params.doTimingTransientSubtraction2;
  params.TimingTransientFile2          = params.TimingTransientFile2;
catch
  params.doTimingTransientSubtraction2 = false;
  params.TimingTransientFile2          = '';
end

try
  params.azimuth1 = params.azimuth1;
catch
  params.azimuth1 = NaN;
end
try
  params.azimuth2 = params.azimuth2;
catch
  params.azimuth2 = NaN;
end

try
  params.alphaExp = params.alphaExp;
catch
  params.alphaExp = 0;
end

try
  params.fRef = params.fRef;
catch
  error('params.fRef parameter not set');
end

try
  params.maxSegmentsPerMatfile = params.maxSegmentsPerMatfile;
catch
  params.maxSegmentsPerMatfile = 60;
end

try
  params.useSignalSpectrumHfFromFile = params.useSignalSpectrumHfFromFile;
catch
  params.useSignalSpectrumHfFromFile = 0;
end

try
  params.HfFile = params.HfFile;
catch
  if params.useSignalSpectrumHfFromFile
    error('params.HfFile parameter not set');
  else
    params.HfFile = '';
  end
end

try
  params.HfFileInterpolateLogarithmic=params.HfFileInterpolateLogarithmic;
catch
  params.HfFileInterpolateLogarithmic=true;
end

try
  params.fbase1 = params.fbase1;
catch
  params.fbase1 = NaN;
end

try
  params.fbase2 = params.fbase2;
catch
  params.fbase2 = NaN;
end

try
  params.maxDSigRatio = params.maxDSigRatio;
catch
  params.maxDSigRatio = 1000;
end

try
  params.minDSigRatio = params.minDSigRatio;
catch
  params.minDSigRatio = 0;
end

try
  params.minDataLoadLength = params.minDataLoadLength;
catch
  params.minDataLoadLength = 200;
end

try
  params.alphaBetaFile1 = params.alphaBetaFile1;
catch
  error('params.alphaBetaFile1 parameter not set');
end

try
  params.alphaBetaFile2 = params.alphaBetaFile2;
catch
  error('params.alphaBetaFile2 parameter not set');
end

try
  params.calCavGainFile1 = params.calCavGainFile1;
catch
  error('params.calCavGainFile1 parameter not set');
end

try
  params.calCavGainFile2 = params.calCavGainFile2;
catch
  error('params.calCavGainFile2 parameter not set');
end

try
  params.calResponseFile1 = params.calResponseFile1;
catch
  error('params.calResponseFile1 parameter not set');
end

try
  params.calResponseFile2 = params.calResponseFile2;
catch
  error('params.calResponseFile2 parameter not set');
end

% moved to preproc.m on sept 8, 2014:
% extract parameters from the params structure setting to default values
% if necessary
%try
%  params.jobsFileCommentStyle = params.jobsFileCommentStyle;
%catch
%  params.jobsFileCommentStyle = 'matlab';
%end;
%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
% for STAMP only
%------------------------------------------------------------------------------
% firstpass is used in conjunction with injections to make sure that the
% injection file is only read in once.  once it is read in, firstpass is set
% to false.
params.pass.stamp.firstpass=true;

try
  params.batch;
catch
  params.batch=true;
end
if ~params.batch
  fprintf('Batch mode is OFF.\n');
  try
    params.startGPS;
    params.endGPS;
  catch
    error('~params.batch: missing fields');
  end
end
try
  params.mapsize;
catch
  params.mapsize = 200;
end

% set random number generator.  use pp_seed<0 to randomize with clock.
try
  if params.pp_seed>0
    fprintf('Setting seed to %i.\n', params.pp_seed); 
  else
   % This setup should be good until ~year 4294 when seed>2^32-1.
    params.pp_seed =  sum(clock*1000000);
    fprintf('Setting seed using clock.\n');
  end
catch
  fprintf('Setting seed to 1.\n');
  params.pp_seed = 1;
end
randn('state', params.pp_seed);
rand('state', params.pp_seed);

% default is no injected signal
try
  params.stampinj;
catch
  params.stampinj = false;
end


  if strcmp(params.pass.stamp.inj_type, 'fly')
    params.pass.stamp.file = '-1';
    params.pass.stamp.alpha = 1;
    params.pass.stamp.startGPS = params.job.startGPS;
  end
  % proceed with other checks
  try
    if params.pass.stamp.ra < 0 | params.pass.stamp.ra>24
      fprintf('params.ra must be between 0 and 24.');
      error;
    end
    if params.pass.stamp.decl<-90 | params.pass.stamp.decl>90
      error;
    end
    params.pass.stamp.startGPS;
    params.pass.stamp.file;
    params.pass.stamp.alpha;
  catch
    error('missing fields for stampinj==true');
  end
  %
  try
    params.fixAntennaFactors;
  catch
    params.fixAntennaFactors = false;
  end
  %
  try
    params.bestAntennaFactors;
  catch
    params.bestAntennaFactors = false;
  end
end

% moved above on sept 8, 2014
%try
%  params.pass.stamp.inj_type;
%catch
%  params.pass.stamp.inj_type = 'other';
%end

if ~strcmpi(params.pass.stamp.inj_type,'other')
  try
    params.pass.stamp.iota;
    params.pass.stamp.psi;
  catch
    params.pass.stamp.iota = 0;
    params.pass.stamp.psi = 0;
    warning('Injection iota and psi not specified, both set to 0');
  end
end

% simulate noise for STAMP mat files
try
  if params.doDetectorNoiseSim
    fprintf('Simulated detector noise.\n');
    try
      params.DetectorNoiseFile = params.DetectorNoiseFile;
    catch
      error('params.DetectorNoiseFile required for doDetectorNoiseSim==1');
    end
   
    try
      params.doDifferentNoise = params.doDifferentNoise;
    catch
      params.doDifferentNoise = false;
    end

    if params.doDifferentNoise
      try
        params.DetectorNoiseFile2 = params.DetectorNoiseFile2;
      catch
        error('params.DetectorNoiseFile2 required for doDifferentNoise==1');
      end
    end


    try
      params.sampleRate;
    catch
      params.sampleRate = 16384;
      fprintf('sampleRate = %5.0f\n', params.sampleRate);
    end
  end
catch
  params.doDetectorNoiseSim = false;
end

try
  params.stochmap = params.stochmap;
catch
  params.stochmap = false;
end
fprintf('params.stochmap = %i\n', params.stochmap);

try
  params.storemats = params.storemats;
catch
  params.storemats = true;
end

try % used for intermediate frames (not STAMP)
  params.outputFilePrefix = params.outputFilePrefix;
catch
  if ~params.stochmap % writing to intermediate frames (*.gwf)
    error('params.outputFilePrefix parameter not set');
  else
    % if stochmap is on outputFilePrefix (for frames) is not used and 
    % hence define a dummy name
    params.outputFilePrefix = 'test';
  end
end

% output directory for writing *.mat files after preproc stage (for STAMP use)
try
  params.outputfiledir = params.outputfiledir;
catch
  params.outputfiledir = './';
end

try
  params.largeShift = params.largeShift;
catch
  params.largeShift = false;
end

try
  params.largeShiftTime2 = params.largeShiftTime2;
catch
  params.largeShiftTime2 = 0;
end

try % compress ft-map by averaging together many segments
  params.compress;
catch
  params.compress = false;
end

if(params.nargin==4 || params.largeShift)
  try
    params.cacheFile;
  catch
    error('For four input arguments to preproc (or) largeTimeshift on, requires new cachefile method; provide the master cacheFile. \n');
  end 
end
%------------------------------------------------------------------------------
