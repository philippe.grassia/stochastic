function vector = load_mdc(params, vector, dataStartTime, sampleRate)
% function [hp, hx] = load_injection_new(params, stamp_inj, fs)
% e.g., [hp, hx] = load_injection(params, stamp_inj, 4096);
% INPUT: params, stamp_inj -> structs
%        fs -> sampling frequency 
% Routine by E. Thrane
fr_t = (0:length(vector)-1)/sampleRate + dataStartTime;
fr_zeros = zeros(size(fr_t));

fprintf('Adding MDC data to vector... ');
numFrames = 0;

frames = textread(params.stamp.mdclist,'%s\n',-1,'commentstyle','matlab');
for i = 1:length(frames)

  gps=str2num(regexprep(frames{i},'.*-([0-9]*)-([0-9]*).gwf','$1'));
  dur=str2num(regexprep(frames{i},'.*-([0-9]*)-([0-9]*).gwf','$2'));

   gpsStart = gps; gpsEnd = gps+dur;

   if gpsStart > fr_t(end)
      continue
   end

   if gpsEnd < fr_t(1)
      continue
   end

   numFrames = numFrames + 1;
   data_out = frgetvect(frames{i},params.stamp.mdcchannel1,gps,dur);
   fs = length(data_out)/dur;
   mdc_t = (0:length(data_out)-1)/fs + gps;

   mdc_interp = interp1(mdc_t,data_out,fr_t,'spline',0);
   vector = vector + sqrt(params.stamp.alpha)*mdc_interp;
end

if numFrames > 0
   fprintf('data added.\n');
else
   fprintf('no data added.\n');
end

return;
