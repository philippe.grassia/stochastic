function [t,hp,hc]=rmodes(alpha,f0,duration,fs,T0,tstart,tend)

k=-1.8*alpha^2*10^-21;

t1 = tstart - T0;
t2 = tend - T0;

dt=1/fs;
t=t1:dt:t2;
fOwen=1./(1/f0^6-6*k*t).^(1/6);
I=-(1/(5*k))*(f0^(-6)-6*k*t).^(5/6);
int=I+(1/(5*k))*(f0^(-6)).^(5/6);
Phi=(2*pi)*int;

d=0.001*1.0; %Mpc
fOwen=fOwen(1:end);
ho=3.6*10^-23*(1/d)*(fOwen/1000).^3*alpha;

w=0;
hp=ho.*(1+cos(w).^2).*cos(Phi);
hc=2*ho.*cos(w).*sin(Phi);

indexes = find(t < 0);
hp(indexes) = 0;
hc(indexes) = 0;
indexes = find(t > duration);
hp(indexes) = 0;
hc(indexes) = 0;
t = tstart:dt:tend;

t = t'; hp = hp'; hc = hc';

