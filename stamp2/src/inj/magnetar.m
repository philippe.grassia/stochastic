function [hp, hc, tp] = magnetar(eB,f0,duration,fs,T0,tstart,tend)

Bdip = 10^14;
dt = 1/fs;
BdipSI = Bdip * 1e-4; % Tesla;
f0_rot = f0/2.0;

R = 10000; %km
c = 3e8;
M = 1.4 * 1.9891e30; % 1.4 solar masses
I = (2/5) * M * R^2;
G = 6.67384e-11;
D = 3.08567758e16 * 1e6; % 1 Mpc

NwaveAll = duration*fs;
%omega = zeros(Nwave,1);
%omega_dot = zeros(Nwave,1);
%phase = zeros(Nwave,1);
t = T0:dt:(T0+duration);
t = t';

omega = 2*pi*f0_rot;
omega_dot = 0;
phase = 0;

tp = tstart:dt:(tend-dt);
Nwave = length(tp);
phaseAll = zeros(Nwave,1);
omegaAll = zeros(Nwave,1);
index = -1;

for ii = 1:NwaveAll
    thist = t(ii);
    if omega ~= 0
        omega_dot = -((BdipSI^2)*(R^6)/(6*I*c^3))*omega^3 - ((32/5)*G*I*(eB^2)/(c^5))*omega^5;
        omega = omega + dt*omega_dot;
        phase = phase + 2*dt*omega;
    end
    if ((tstart <= thist) && (thist <= tend))
       if index < 0
          index = find(tp == thist);
       else
          index = index + 1;
       end
       omegaAll(index) = omega;
       phaseAll(index) = phase;
    end 
end

f = 2*omegaAll/(2*pi);
h = (4*(pi^2)*G*I*eB/((c^4)*D))*(f.^2);
w = 0;
hp = h .* (1+cos(w).^2) .* cos(phaseAll);
hc = 2 * h .* cos(w) .* sin(phaseAll);

tp = tp'; hp = hp(1:Nwave); hc = hc(1:Nwave);

