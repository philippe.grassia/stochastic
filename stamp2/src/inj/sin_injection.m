function Hf = sin_injection(params, fs)
% function Hf =sin_injection(params, fs)
% creates on-the-fly monochromatic injection

% check to make sure relevant parameters are defined
try
  params.pass.stamp.h0;
  params.pass.stamp.f0;
  params.pass.stamp.phi0;
  params.pass.stamp.fdot;
  params.pass.stamp.start;
  params.pass.stamp.duration;
catch
  error('missing variables required for sin_injection.m');
end

% create time vector corresponding to the total amount of data to be analyzed
dt = 1/fs;
t = [params.pass.stamp.dataStartGPS : dt : params.pass.stamp.dataEndGPS];

% create hp and hx
phi = params.pass.stamp.phi0 + ...
  2*pi*(params.pass.stamp.f0*t + ...
  0.5*params.pass.stamp.fdot*(t-params.pass.stamp.start).^2);
hp = params.pass.stamp.h0*cos(phi);
hx = params.pass.stamp.h0*sin(phi);

% injection starts and stops
cut = t<params.pass.stamp.start | t>(params.pass.stamp.start+params.pass.stamp.duration);
hp(cut) = NaN;
hx(cut) = NaN;

% apply window
indexes = find(abs(hp)>=0);
if length(indexes) > 0
  taper_dur = 1;
  type = 'both';
  hp(indexes) = apply_window(hp(indexes),fs,taper_dur,type);
  hx(indexes) = apply_window(hx(indexes),fs,taper_dur,type);
end



nan_indexes = find(isnan(hp));
hp(nan_indexes) = 0;
hx(nan_indexes) = 0;

% create injection array
Hf = [t ; hp ; hx]';

return
