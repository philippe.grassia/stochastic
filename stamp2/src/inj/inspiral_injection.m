function Hf = geninspiral(params,fs);

mass1 = params.pass.stamp.mass1;
mass2 = params.pass.stamp.mass2;

fmin = params.flow;
Mtot = mass1+mass2;
eta  = mass1*mass2 / (Mtot*Mtot);
LAL_MTSUN_SI = 4.92549095e-6;
totalMass    = Mtot*LAL_MTSUN_SI;
t0   = 5.0/(256.0*eta*(totalMass^(5.0/3.0)) *(pi*fmin)^(8.0/3.0));
t2   = (3715.0 + (4620.0*1.0*eta))/(64512.0*eta*totalMass*(pi*fmin)^(2.0));
t3   = pi/(8.0*eta*(totalMass^(2.0/3.0))*(pi*fmin)^(5.0/3.0));
t4   = (5.0/(128.0*eta*(totalMass^(1.0/3.0))*(pi*fmin)^(4.0/3.0))) * (3058673./1016064. + 5429.0*1.0*eta/1008.0 +617.*1.0*eta*eta/144.0);
t5   = 5.0*(7729.0/252.0 + 1.0*eta)/(256.0*eta*fmin);
duration = t0 + t2 - t3 + t4 - t5;
duration = duration + 1.0; % account for ringdown + delay

if duration > 3600
   duration = 3600;
end

duration = params.pass.stamp.duration;
T = duration;
T0 = params.pass.stamp.start + duration;
iota = 0;
dist = 1;

tstart = params.pass.stamp.dataStartGPS;
tend = params.pass.stamp.dataEndGPS;

%fprintf('%.5f %.5f %.5f\n',tstart,tend,T0);

[hp, hc, tpn, tinspiral] = inspiral2pn(mass1,mass2,iota,dist,fs,T0,fmin,tstart,tend);

if isempty(hp)
   ttotal = tend - tstart;
   % ---- Waveform duration in samples.
   N = floor(ttotal*fs);
   tinspiral = ([0:N-1].' / fs); 
   hp = zeros(N,1); hc = zeros(N,1);
   Hf = [tinspiral hp hc];
   return
end

% apply window
indexes = find(abs(hp)>=0);
if length(indexes) > 0
  taper_dur = 1;
  type = 'both';
  hp(indexes) = apply_window(hp(indexes),fs,taper_dur,type);
  hc(indexes) = apply_window(hc(indexes),fs,taper_dur,type);
end
nan_indexes = find(isnan(hp));
hp(nan_indexes) = 0;
hc(nan_indexes) = 0;

Hf = [tinspiral hp hc]; 

