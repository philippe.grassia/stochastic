function params = get_alpha(params)
% function alpha = get_alpha(params)
% E. Thrane: calculates an array of alpha such that every consecutive entry
% (except the first) is 1.20 greater than the previous entry.  The first entry
% is zero.  The array goes from params.alpha_min to >= params.alpha_max.  The
% user must specify these in the params struct.

% every consecutive entry (except the first) is 1.20 greater than the previous
% entry
%increment = 1.20;

fprintf('Running with %.2f%% increments in %s\n',100*(params.increment - 1),params.incrementType)
increment = params.increment;

if strcmp(params.incrementType,'distance')

   d_min = sqrt(1/params.alpha_max);
   d_max = sqrt(1/params.alpha_min);

   params.d = d_min;
   % create array of distances
   done = false;
   while ~done
      params.d = [params.d increment*params.d(end)];
         if params.d(end) >= d_max
            done = true;
      end
   end

   % automatically add alpha_min=0
   params.alpha = [0 fliplr(params.d.^(-2))];

elseif strcmp(params.incrementType,'alpha')

   params.alpha = params.alpha_min;
   % create array of alphas
   done = false;
   while ~done
      params.alpha = [params.alpha increment*params.alpha(end)];
      if params.alpha(end) >= params.alpha_max
         done = true;
      end
   end

   % automatically add alpha_min=0
   params.alpha = [0 params.alpha_min];

end

% calculate the number of alphas
params.alpha_n = length(params.alpha);

return
