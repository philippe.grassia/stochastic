function phi = randphases(q, params)
% function phi = randphases(q, params)
% Eric Thrane: creates an array of correlated random phases for data with 
% overlapping Hann windows.  Because the segments are overlapping, the phases
% are correlated.
%
% Note: Hann windows are hard-coded.  Other window types not currently
% supported.

% pseudo segment duration
dur = round((q(1)+1)/2)*2;

% number of overlapping segments
no = q(2);

% number of non-overlapping pseudo segments required
n = round((no+1)/2);

% total pseudo data required
L = n*dur;

% simulate time series
x = randn(L, 1);
y = randn(L, 1);

% define window
w = hann(dur);

% split the data into pseudo segments and window each one
t1 = 1;
phi = zeros(dur, no);
for ii=1:no
  t2 = t1 + dur - 1;
  qx = w.*x(t1:t2);
  qy = w.*y(t1:t2);
  xf = fft(qx);
  yf = fft(qy);
  % calculate phases
  phi(:, ii) = phase(conj(xf).*yf);
  t1 = t1 + dur/2;
end

% remove extra bins created from rounding if necessary
phi = phi(2:q(1)+1, 1:q(2));

return
