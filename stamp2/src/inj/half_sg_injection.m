function Hf = half_sg_injection(params, fs)
% creates on-the-fly monochromatic (last-half) sine-Gaussian injection
% based on sin_injection.m
%
% Written by R Quitzow-James

% check to make sure relevant parameters are defined
try
  params.pass.stamp;
  params.pass.stamp.h0;
  params.pass.stamp.f0;
  params.pass.stamp.phi0;
  params.pass.stamp.fdot;
  params.pass.stamp.start;
  params.pass.stamp.duration;
  params.pass.stamp.tau;
  %params.pass.stamp.startGPS;
catch
  error('missing variables required for half_sg_injection.m');
end

% create time vector corresponding to the total amount of data to be analyzed
dt = 1/fs;
t = [params.pass.stamp.dataStartGPS : dt : params.pass.stamp.dataEndGPS];
wave_t = t-params.pass.stamp.start;

% create hp and hx
phi = params.pass.stamp.phi0 + ...
  2*pi*(params.pass.stamp.f0*wave_t + ...
  0.5*params.pass.stamp.fdot*wave_t.^2);
hp = params.pass.stamp.h0*exp(-(wave_t/params.pass.stamp.tau).^2).*cos(phi);
hx = params.pass.stamp.h0*exp(-(wave_t/params.pass.stamp.tau).^2).*sin(phi);

% injection starts and stops
% REMOVED 2018-12-06 by Paul Schale - this caused the injection to always start at beginning of anteproc window
%cut = t<params.pass.stamp.start | t>(params.pass.stamp.start+params.pass.stamp.duration);
%hp(cut) = NaN;
%hx(cut) = NaN;
%non_nan_indexes = find(~isnan(hp));
%hp = hp(non_nan_indexes);
%hx = hx(non_nan_indexes);
%t_out = t(non_nan_indexes);

% apply window
indexes = find(abs(hp)>=0);
taper_dur = 1;
type = 'both';
hp(indexes) = apply_window(hp(indexes),fs,taper_dur,type);
hx(indexes) = apply_window(hx(indexes),fs,taper_dur,type);
nan_indexes = find(isnan(hp));
hp(nan_indexes) = 0;
hx(nan_indexes) = 0;

% create injection array
Hf = [t ; hp ; hx]';

return
