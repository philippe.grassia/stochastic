function [hp, hx, params] = load_injection(params, stamp_inj, fs)
% function [hp, hx] = load_injection_new(params, stamp_inj, fs)
% e.g., [hp, hx] = load_injection(params, stamp_inj, 4096);
% INPUT: params, stamp_inj -> structs
%        fs -> sampling frequency 
% Routine by E. Thrane

% make sure stamp_inj.file is defined
try
  stamp_inj.file;
catch
  error('Error: missing variables in stamp_inj struct.\n');
end

% Check if injection start time is during unusable data.
totalBuffer = (params.numSegmentsPerInterval-1)/2*params.segmentDuration + ...
    params.bufferSecs1;
if (params.pass.stamp.startGPS < (params.job.startGPS + totalBuffer + 1))
  warning('Injection starts during unusable data.');
end

% Check if stamp_inj.file is a directory.
% If it is, then we assume the user has multiple waveforms
% in the directory and wants to pick a random one, for marginalizing
% over polarization or sky direction or something similar.
% Note: stamp_inj.file should end in '/' to specify a directory.
% The directory should ONLY contain injection files.
%if ~isempty(regexp(stamp_inj.file,'.*/$'))
%  rand('state',sum(clock*1e6));
%  temp = dir(stamp_inj.file);
%  temp = {temp.name};
%  % remove './' and '../' entries
%  inj_files = temp(3:end);
%  % remove .svn entries, if they exist
%  r = find(strcmp(inj_files,'.svn'));
%  inj_files(r) = [];
%  n = ceil(rand*numel(inj_files));
%  stamp_inj.file = [stamp_inj.file inj_files{n}];
%  fprintf('ETHRANE ON SEPT 2, 2014: THIS CODE IS SLATED FOR REMOVAL\n');
%  fprintf('INSTEAD OF RANDOMLY CHOSING WAVEFORMS IN STAMP CODE, USERS\n');
%  fprintf('ARE ASKED TO WRITE A WRAPPER TO DO THE SAME THING.\n');
%end

% inj_file is of the form: time, hplus, hcross
% injection may be calculated on the fly...
if strcmpi(params.pass.stamp.inj_type, 'fly')
  fprintf('calculating injection on the fly...');
  if isfield(params.pass.stamp, 'fly_waveform') && strcmpi(params.pass.stamp.fly_waveform, 'half_sg')
    fprintf('half sine-Gaussian selected...');
    Hf = half_sg_injection(params, fs);
  elseif isfield(params.pass.stamp, 'fly_waveform') && strcmpi(params.pass.stamp.fly_waveform, 'ringdown')
    fprintf('Ringdown selected...');
    Hf = ringdown_injection(params, fs);
  elseif (isfield(params.pass.stamp, 'fly_waveform') && strcmpi(params.pass.stamp.fly_waveform, 'inspiral')) || ~isempty(strfind(params.pass.stamp.file,'inspiral'))
    Hf = inspiral_injection(params, fs);
  elseif (isfield(params.pass.stamp, 'fly_waveform') && strcmpi(params.pass.stamp.fly_waveform, 'gw170817')) || ~isempty(strfind(params.pass.stamp.file,'gw170817'))
    Hf = inspiral_injection(params, fs);
  elseif (isfield(params.pass.stamp, 'fly_waveform') && strcmpi(params.pass.stamp.fly_waveform, 'rmodes')) || ~isempty(strfind(params.pass.stamp.file,'rmodes'))
    Hf = rmodes_injection(params, fs);
  elseif (isfield(params.pass.stamp, 'fly_waveform') && strcmpi(params.pass.stamp.fly_waveform, 'msmagnetar')) || ~isempty(strfind(params.pass.stamp.file,'msmagnetar'))
    Hf = msmagnetar_injection(params, fs);
  elseif (isfield(params.pass.stamp, 'fly_waveform') && strcmpi(params.pass.stamp.fly_waveform, 'magnetar')) || ~isempty(strfind(params.pass.stamp.file,'magnetar'))
    Hf = magnetar_injection(params, fs);
  elseif (isfield(params.pass.stamp, 'fly_waveform') && strcmpi(params.pass.stamp.fly_waveform, 'sinusoid')) || ~isempty(strfind(params.pass.stamp.file,'sinusoid'))
    Hf = sin_injection(params, fs);
  end
  t = Hf(:,1); hp = Hf(:,2); hx = Hf(:,3);
  fprintf('done\n');
else
  % ...or read in from a file
  fprintf('loading STAMP injection "%s"...', stamp_inj.file);

  % Get file extension
  [~,~,ext] = fileparts(stamp_inj.file);

  % .mat file
  if strcmpi(ext,'.mat')
    % Injection mat files should contain a struct called 'inj'
    % which has 3 members: t, hp, hx.
    load(stamp_inj.file);
    try
      t = inj.t; hp = inj.hp; hx = inj.hx; fmin = inj.fmin; fmax = inj.fmax;
    catch
      error('The injection .mat file is not set up correctly.');
    end
    else
    
    % Text file
    
    params.pass.stamp.tempInj = load(stamp_inj.file); % Inj added by me in name definition
    
    
    % Get time and plus and cross waveforms
    if(size(params.pass.stamp.tempInj,2)==3)
      
     
      t =  params.pass.stamp.tempInj(:,1);
      hp = params.pass.stamp.tempInj(:,2);
      hx = params.pass.stamp.tempInj(:,3);
      
    else
      error('Time series injections require a three-column file.\n');
    end
  end

  % Make sure signal is divisible by sampling time.
  if (mod(t(end,1), 1/fs)~=0)
    t = [t; t(end) - mod(t(end), 1/fs)+1/fs];
    hp = [hp; 0]; hx = [hx; 0];
  end

  % print info.
  fprintf('done\n');
end

% determine signal duration from the injection file
stamp_inj.dur = t(end) - t(1);

% Adjust waveform polarization.
[hp hx] = modWaveformPol(params,hp,hx);

% Interpolate the injection if necessary
InjSamplingFreq = 1/(t(2)- t(1));
if abs(InjSamplingFreq-fs)/fs < 1e-4
  fprintf('Injection sampled at the expected rate.\n');
else
  fprintf('Warning: injection interpolated; artifacts possible.\n');
  tt = 1/fs:1/fs:stamp_inj.dur;
  hp = interp1(t - t(1),hp,tt,'spline')';
  hx = interp1(t - t(1),hx,tt,'spline')';
end

return;
