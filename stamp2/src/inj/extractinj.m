function smallmap = extractinj(map)
% function smallmap = extractinj(map)
% Eric Thrane
% crops an injection map to keep only the part with non-zero signal
  
% remove time associated with NaNs in y-map
cut_t = any(isnan(map.y),1);

% copy metadata
smallmap = map;

% apply cut to processed data
smallmap.segstarttime(cut_t) = [];
smallmap.y(:, cut_t) = [];
smallmap.snr(:, cut_t) = [];
smallmap.z(:, cut_t) = [];
smallmap.snrz(:, cut_t) = [];
smallmap.sigma(:, cut_t) = [];
smallmap.eps_12(:, cut_t) = [];
smallmap.eps_11(:, cut_t) = [];
smallmap.eps_22(:, cut_t) = [];
smallmap.Xi(:, cut_t) = [];
smallmap.sigma_Xi(:, cut_t) = [];
smallmap.Xi_snr(:, cut_t) = [];
% apply cut to raw data maps
smallmap.cc(:, cut_t) = [];
smallmap.sensInt(:, cut_t) = [];
%smallmap.sensInt.data(:, cut_t) = []; 
smallmap.ccVar(cut_t) = [];
smallmap.naiP1(:, cut_t) = [];
smallmap.naiP2(:, cut_t) = [];
smallmap.P1(:, cut_t) = [];
smallmap.P2(:, cut_t) = [];

% calculate the new map duration
smallmap.dur = (smallmap.segDur/2)*(length(smallmap.segstarttime)+1);

% set a flag to know that this routine has already been run
map.extractinj = true;

return
