function Hf = magnetar_injection(params,fs);

eB = params.pass.stamp.eB;
f0 = params.pass.stamp.f0;
duration = params.pass.stamp.duration;
tstart = params.pass.stamp.dataStartGPS;
tend = params.pass.stamp.dataEndGPS;
T0 = params.pass.stamp.start;

[hp, hc, tinspiral] = magnetar(eB,f0,duration,fs,T0,tstart,tend);

if isempty(hp)
   ttotal = tend - tstart;
   % ---- Waveform duration in samples.
   N = floor(ttotal*fs);
   tinspiral = ([0:N-1].' / fs);
   hp = zeros(N,1); hc = zeros(N,1);
   Hf = [tinspiral hp hc];
   return
end

% apply window
indexes = find(abs(hp)>=0);
if length(indexes) > 0
  taper_dur = 1;
  type = 'both';
  hp(indexes) = apply_window(hp(indexes),fs,taper_dur,type);
  hc(indexes) = apply_window(hc(indexes),fs,taper_dur,type);
end
nan_indexes = find(isnan(hp));
hp(nan_indexes) = 0;
hc(nan_indexes) = 0;

Hf = [tinspiral hp hc];

