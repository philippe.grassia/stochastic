function h_out = apply_window(h_in,fs,taper_dur,type)
% function h_out = apply_window(h_in,fs,taper_dur,type)
%
% Function for applying a Hann taper to a waveform.
%
% Inputs:
%   h_in      - strain time-series
%   fs        - sampling frequency.
%   taper_dur - duration of taper (seconds).
%   type      - where the taper should be applied. Possible
%               values are 'start', 'end', or 'both'.
%
% Outputs:
%   h_out - tapered strain time-series.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get injection sample times.
N = numel(h_in);
t = (0:N)/fs;

% Define window (factor of two comes from the fact
% that we only use 1/2 of a Hann window.
n_taper = ceil(2*taper_dur*fs);
w = (1/2)*(1-cos(2*pi*(0:(n_taper-1))/(n_taper-1)));
if length(h_in) < ceil(n_taper/2)
   h_out = h_in;
   return
end

if iscolumn(h_in)
  w = w';
end

% Get taper indices
taper_idx_start = 1:ceil(n_taper/2);
wf_idx_start = taper_idx_start;
taper_idx_end = (floor(n_taper/2)+1):n_taper;
wf_idx_end = (N-numel(taper_idx_end)+1):N;

% Determine where to apply the taper and which
% part of the taper to use.
if strcmpi(type,'start')
  taper_idx = taper_idx_start;
  wf_idx = wf_idx_start;
elseif strcmpi(type,'end')
  taper_idx = taper_idx_end;
  wf_idx = wf_idx_end;
elseif strcmpi(type,'both')
  taper_idx = [taper_idx_start taper_idx_end];
  wf_idx = [wf_idx_start wf_idx_end];
else
  error('type should be ''start'', ''end'', or ''both''.');
end

% Apply taper.
h_out = h_in;
h_out(wf_idx) = h_out(wf_idx) .* w(taper_idx);

return;
