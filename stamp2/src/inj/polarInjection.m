function ccSpec = polarInjection(det1, det2, GPSTimes, source, ccSpec, params, g, map)

iota0=params.polarinjection.iota;  psi0=params.polarinjection.psi;   % injection parameters in radians

Ap = (1/2)*(1 + (cos(iota0))^2); % a_plus, from STAMP paper
Ax = cos(iota0);                 % a_cross, from STAMP paper
term1 = (g.F1p .* g.F2p)*( (Ap^2)*(cos(2*psi0))^2 + ...
                             (Ax^2)*(sin(2*psi0))^2);
term2 = ((g.F1p .* g.F2x) + (g.F1x .* g.F2p))*(Ap^2-Ax^2)* ...
          cos(2*psi0)*sin(2*psi0);
term3 = (g.F1x .* g.F2x)*( (Ap^2)*(sin(2*psi0))^2 + ...
                             (Ax^2)*(cos(2*psi0))^2 );
term4 = i*(Ap*Ax)*((g.F1x .* g.F2p) - (g.F1p .* g.F2x));

gamma0 = abs(term1 + term2 + term3 + term4)/( (Ap^2) + (Ax^2) );
eta = -phase( (term1 + term2 + term3 + term4)/( (Ap^2) + (Ax^2) ) );

f0 = params.polarinjection.f0;
index = find(map.f == f0);
CC0 = exp(i*2*pi*f0 .* g.tau -i*eta) .* params.polarinjection.amplitude;
ccSpec.data(index,:) = ccSpec.data(index,:) + CC0;

