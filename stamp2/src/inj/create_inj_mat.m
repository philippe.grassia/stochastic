function create_inj_mat(file, output_file)
% function create_inj_mat(file, output_file)
%
% example: create_inj_mat('adi_D.dat','adi_D.mat') creates output file adi_D.mat
% note: you need the following files in the directory where this script is run
%   zero_noise.txt
%   inj_params.txt
%   S5H1L1_full_run.txt
%
% You will need to be on ldas to run this script because it points to
% cachefiles in ~ethrane/.  If necessary, you can create your own cachefiles,
% but this is a non-trivial step, and we suggest you use the ones that already
% exist.
%
% The code is hard-coded for numSegmentsPerInterval = 9, though, this can be
% modified if we stop using this as standard value.  Likewise, we have hard-
% coded the fact that we use 2 buffer seconds.  Finally, we have hard-coded
% the S5 jobfile as our template jobfile.
%
% E. Thrane

% the hard-coded S5 jobfile
jobfile = 'S5H1L1_full_run.txt';

% check to make sure the output file doesn't already exist
if exist(output_file)
  fprintf([output_file ' already exists...quitting.\n']);
  return;
end
fprintf('***create_inj_mat:\n');

% load injection dat file to determine the duration
fprintf('***loading injection...');
q = load(file);
fprintf('done\n');
duration = q(end,1) - q(1,1);
% round up to nearest second
duration = ceil(duration);

% load the job file to obtain the job durations
h = load(jobfile);
jobdurs = h(:,4);
jobstarts = h(:,2);
jobends = h(:,3);

% determine some parameters from paramfile
xyzparms = readParamsFromFile('inj_params.txt');
segdur = xyzparms.segmentDuration;

% time required for neighboring segments plus a bit extra to be safe
extra_t = 9*segdur + 2*2 + 5*segdur;

% find a suitable job
idx = find(jobdurs-extra_t >= duration);
job = min(idx(jobdurs(idx) == min(jobdurs(idx))));

% job start time plus some extra
start = jobstarts(job) + 5*segdur + 2 + 2*segdur;

% prepare a new paramfile
out = read_doc('inj_params.txt');
out = regexprep(out, 'xyz_mapsize', num2str(max(duration,200)));
out = regexprep(out, 'xyz_startgps', num2str(start));
out = regexprep(out, 'xyz_stampfile', file);
if ~exist('.inj')
  !mkdir .inj;
end

% write new paramfile
fid=fopen('.inj/inj_params.txt','w+');
fprintf(fid, '%c', out);
fclose(fid);

% call preproc
fprintf('***set-up is done: calling preproc...');
preproc('.inj/inj_params.txt', jobfile, job);
fprintf('done.\n');

% run clustermap
params = stampDefaults;
params.fmin = xyzparms.flow;
params.fmax = xyzparms.fhigh;
params.ra = xyzparms.pass.stamp.ra;
params.dec = xyzparms.pass.stamp.decl;
params.inmats = [xyzparms.outputfiledir xyzparms.outputfilename];
params.saveMat=true;
params.savePlots=true;
fixAntennaFactors = true;
fprintf('***running clustermap...');
stoch_out=clustermap(params, jobstarts(job), jobends(job));
fprintf('done.\n');

% load clustermap output
fprintf('***load clustermap output...');
QQ = load('map_1.mat');
params = QQ.params;
pp = QQ.pp;
fprintf('done.\n');

% extract injection
fprintf('***injection extraction...\n');
map = extractinj(QQ.map);
fprintf('done.\n');

% save a new mat file
fprintf('***saving new mat file...');

save(output_file, 'map', 'params', 'pp');
fprintf('done.\n');

% clean up
fprintf('***Clean up?  Press any key to continue...');
pause;
%
rmdir('.inj/', 's');
delete('map_1.mat');
delete('snr.*');
delete('y_map.*');
delete('Xi_snr_map.*');
delete('sig_map.*');
%
% make a diagnostic plot
map.xvals = map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);
map.yvals = map.f;
ymax = max(map.y(:));
ymin = ymax/100;
newpng = regexprep(file, '.mat', '');
printmap(map.y, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'y (strain^2)', [ymin ymax]);
%print('-dpng', [newpng '.png']);
%
fprintf('done\n');

% close all open plots
close all;

return
