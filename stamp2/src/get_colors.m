function color = get_colors(n)
% function color = get_colors(n)
% E. Thrane: this function generates an array of colored line types which can
% be useful when making histograms with many different line types
% n = an optional parameter that specifies the number of colors requested.

color = {'r', 'b', 'g', 'c', 'k', 'r--', 'b--', 'g--', 'c--', 'k--', ...
  'r:', 'b:', 'g:', 'c:', 'k:', 'r-.', 'b-.', 'g-.', 'c-.', 'k-.'};

try
  n;
catch
  n = length(color);
end

if n>length(color)
  color = repmat(color, 1, ceil(n/length(color)));
end

color = color(1:n);

return
