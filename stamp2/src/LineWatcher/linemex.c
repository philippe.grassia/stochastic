/* wrapper code for MATLAB to access the line watcher */
/* Ed Daw, 2nd December 2007                          */
#include <mex.h>
#include "linewatch.h"

/* c function to call linewatcher library */
void linewatch_apply(double indata[],
             double inshifted[],
		     double correction[],
		     double outdata[],
		     int ndata,
		     double frequency[],
			 int nfreq,
		     double width[],
		     double srate,
		     double responsetime,
			 double tsbuffer[]) {
  
  linedata* ld;
  int datacount;
  int freqcount;
  double* correctionelement;
  double real;
  double imag;

  /* make buffers for linedata structures */
  ld=mxCalloc(nfreq,sizeof(linedata));
  
  /* initialize structures to hold line monitor data */
  for(freqcount=0; freqcount<nfreq; ++freqcount) {
	  linewatch_constructor(ld+freqcount, 
	                        frequency[freqcount], 
							width[freqcount], 
							srate,
							responsetime, 
							tsbuffer);
  }
  /* loop over data */
  for(datacount=0; datacount<ndata; ++datacount) {
	  correction[datacount]=0;
	  /* loop over lines */
	  for(freqcount=0; freqcount<nfreq; ++freqcount) {
		  correction[datacount] += 
		      linewatch_increment(ld+freqcount,indata[datacount],
		                          &real,&imag,&correctionelement);
		  inshifted[datacount]=(*correctionelement);
	  }
	  outdata[datacount]=(*correctionelement) - correction[datacount];
	  /* increment timeseries ring buffer and line monitor counters */
	  for(freqcount=0; freqcount<nfreq; ++freqcount) {
		  linewatch_nextsample(ld+freqcount, indata[datacount]);
	  }
  }
  return;
}

/* matlab wrapper */
void mexFunction( int nlhs, 
		  mxArray* plhs[],
		  int nrhs, 
		  const mxArray* prhs[] ) {
  double* indata;
  double* inshifted;
  double* outdata;
  double* correction;
  double* tsbuffer;
  double* ndatabuffer;
  int ndata;
  int nfreq;
  double* frequency;
  double* width;
  double srate;
  double responsetime;
  int datacount;
  int tseriesbuffersize;
  mwSize mdatasize;
  mwSize ndatasize;
  
  /* read function arguments */
  indata = (double*)mxGetPr(prhs[0]);
  frequency=(double*)mxGetPr(prhs[1]);
  width=(double*)mxGetPr(prhs[2]);
  srate=(*((double*)mxGetPr(prhs[3])));
  responsetime=(*((double*)mxGetPr(prhs[4])));
  
  /* get size of data set and write to ndata */
  mdatasize=mxGetM(prhs[0]);
  ndatasize=mxGetN(prhs[0]);
  if((mdatasize>1) && (ndatasize>1)) {
	  mexErrMsgTxt("Input data array should be one dimensional.");
	  return;
  } else {
	  if ((mdatasize<1) || (ndatasize<1)) {
		  mexErrMsgTxt("At least one dimension of input data is too small.");
		  return;
	  } else {
		  if (mdatasize>1) { ndata=mdatasize; }
		  if (ndatasize>1) { ndata=ndatasize; }
		  if ((mdatasize==1) && (ndatasize==1)) { ndata=1; }
	  }
  }
  
  /* get size of frequency set and write to nfreq */
  mdatasize=mxGetM(prhs[1]);
  ndatasize=mxGetN(prhs[1]);
  if((mdatasize>1) && (ndatasize>1)) {
	  mexErrMsgTxt("Frequency array should be one dimensional.");
	  return;
  } else {
	  if ((mdatasize<1) || (ndatasize<1)) {
		  mexErrMsgTxt("At least one dimension of frequency array is too small.");
		  return;
	  } else {
		  if (mdatasize>1) { nfreq=mdatasize; }
		  if (ndatasize>1) { nfreq=ndatasize; }
		  if ((mdatasize==1) && (ndatasize==1)) { nfreq=1; }
	  }
  }
  
  /* allocate memory for output arguments */
  plhs[0]=mxCreateDoubleMatrix(ndata,1,mxREAL);
  plhs[1]=mxCreateDoubleMatrix(ndata,1,mxREAL);
  plhs[2]=mxCreateDoubleMatrix(ndata,1,mxREAL);
  
  /* allocate memory for timeseries buffer */
  tseriesbuffersize=(int)linewatch_gettsbuffersize(srate,responsetime);
  plhs[3]=mxCreateDoubleMatrix(tseriesbuffersize,1,mxREAL);
  plhs[4]=mxCreateDoubleMatrix(1,1,mxREAL);
   
  /* assign pointers to output arguments */
  inshifted = mxGetPr(plhs[0]);
  correction = mxGetPr(plhs[1]);
  outdata = mxGetPr(plhs[2]);
  tsbuffer = mxGetPr(plhs[3]);
  ndatabuffer=mxGetPr(plhs[4]);
  
  /* a returned test parameter for debugging purposes */
  ndatabuffer[0]=(double)tseriesbuffersize;

  /* run line subtractor */
  linewatch_apply(indata,inshifted,correction,outdata,ndata,
    frequency,nfreq,width,srate,responsetime,tsbuffer);

  return;
}

