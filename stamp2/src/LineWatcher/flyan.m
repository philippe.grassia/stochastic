function [f,f2,a]=flyan
% FLYAN- read and analyze data from the real time line remover
% test bed.
%
% Ed Daw, 16th November 2007
fin=fopen('result.txt','r');
a=fscanf(fin,'%le\t%le\t%le\n')';
a=reshape(a,3,length(a)/3)';

% reject data corresponding to startup time for fft %
a=a(1048576:size(a,1),:);

% results plots - original and correction waveforms
f=figure;
[v,f]=nsd(a(:,1),1,16384);
[vs,fs]=nsd(a(:,2),1,16384);
loglog(fs,vs,'-',f,v,'-');
% axis([1 10^4 10^(-14) 10^(-2)]);
xlabel('frequency (Hz)');
ylabel('asd (/\surdHz)');
print -dpng inputandsubtractor.png

% result of subtraction and ratio
f2=figure;
[vo,fo]=nsd(a(:,3),1,16384);
subplot(2,1,1);
loglog(f,v,'-',fo,vo,'-');
xlabel('frequency (Hz)');
ylabel('asd (/\surdHz)');
ratio=vo./v;
subplot(2,1,2);
semilogx(f,ratio,'-');
xlabel('frequency (Hz)');
ylabel('after / before');



