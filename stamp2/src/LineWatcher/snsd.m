function [v,f]=snsd(a,endpoint)
% SNSD - INVESTIGATE NOISE POLLUTION

% the input
b=a(2000000:2000000+endpoint,2);
[v,f]=nsd(b,1,16384);
