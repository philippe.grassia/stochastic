% LINEMEX - execute compiled c linewatcher library implementation
% of the MFT algorithm.
%
% useage: [ <input data shifted by delay of correction>,
%           <correction data> ,
%           <corrected output data> ,
%           <buffer for timeseries>,
%           <arbitrary object for debugging>] =
%        linemex( <input data>,
%                 <line frequencies (Hz)>,
%                 <line widths (Hz)>,
%                 <sampling rate (Hz)>,
%                 <line tracking response time (s)> )
%
% The operation of this algorithm is fully documented in the following
% publication:
%
% Ed Daw, 5th December 2007