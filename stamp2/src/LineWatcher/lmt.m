function fig=lmt(strain,lengthsecs)
% LMT - test linemex on IFO data
%
% Ed Daw, 6th December 2007

% get data
srate=16384;

% design single zero/pole pair 'RC' IIR highpass filter with 50Hz knee
%z=[0]; p=[-2*pi*50]; k=1;
% design double zero/pole pair 'RC' IIR highpass filter with 50Hz knee
z=[0 0]; p=[-2*pi*50 -2*pi*50]; k=1;
zpkcont=zpk(z,p,k);
zpkdisc=c2d(zpkcont,1/srate,'tustin');
[zd,pd,kd]=zpkdata(zpkdisc,'v');
[bd,ad]=zp2tf(zd,pd,kd);
% filter the data
%strainfilt=filter(bd,ad,strain);

% subset of data for analysis
sts=strain(1:srate*lengthsecs);
stsfilt=filter(bd,ad,sts);

% line frequencies and widths
linefreq=[828 1654 384 150 50 250 350 1362 2483];
linewidth=[5 5 2 3 1 1 1 4 1];

% run linemex
[sd,cor,op,ts,nd]=linemex(stsfilt,linefreq,linewidth,srate,3);

% remove first few seconds for timeseries buffer filling
startsec=5;
startsample=startsec*srate;
sdss=sd(startsample:length(sd));
corss=cor(startsample:length(cor));
opss=op(startsample:length(cor));

% make time axis
time=linspace(startsample/srate,(length(cor)-1)/srate,length(corss));

% plot results
fig=figure;
%plot(time,sdss,'-',time,opss,'-');
[vin,f]=nsd(sdss,1,16384);
[vout,f]=nsd(opss,1,16384);
cumin = sqrt(cumsum((vin.^2)));
cumout = sqrt(cumsum((vout.^2)));
cinmax = cumin(length(cumin));
cumin=cumin/cinmax;
cumout=cumout/cinmax;
semilogx(f,cumin,'-',f,cumout,'-');
axis([1 8192 0 1]);
xlabel('frequency (Hz)');
ylabel('cumulative a.s.d.');
