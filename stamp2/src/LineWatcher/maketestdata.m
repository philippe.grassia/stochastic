function maketestdata(npoints)
% MAKETESTDATA - make a data file to test the C version of my code.
% 
% Ed Daw, 12th November 2007
%load('twothousandseconds_20071107.mat');
%strain=data.channel.x;
%clear data;
time=linspace(0,256,16384*256);
strain=10*cos(2*pi*50*time);
strain=strain+1.3*randn(size(strain));
npoints=length(strain);
outfile=fopen('testdata.txt','w');
fprintf(outfile,'%f\n',strain(1:npoints));
fclose(outfile);
