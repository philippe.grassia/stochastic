function params = boxDefaults(params)
% Sets parameters for Box search 
% Written by S. kandhasamy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.doBoxSearch = true;
params.box.freq_width = 200; % ft box size, (in Hz)  
params.box.time_width = 20; % in sec
params.box.debug = 0;

return;
