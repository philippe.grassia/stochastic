function max_cluster = singletrack(map, params)
% This clustering algorithm uses predefined clusters from .mat files.
% Designed for use when finding upper limits.
%
% Written by R Quitzow-James

tempClusterInfo = {};
if isfield(params.stochtrack.singletrack, 'trackInputFiles')
  tempClusterInfo = params.stochtrack.singletrack.trackInputFiles;
elseif isfield(params.stochtrack.singletrack, 'trackInputFile')
  tempClusterInfo{1} = params.stochtrack.singletrack.trackInputFile;
else
  error('No .mat files for tracks defined.')
end

% NaNs occur due to missing data, vetoes, and notches.  When a NaN is
% encountered, y and snr are set to zero (so they do not add to the cluster SNR
% and sigma is set to one (much larger than typical strain power) so that the
% cluster sigma does not depend significantly on the NaN pixels.  Just to be 
% safe, a check is employed to make sure that map.sigma << 1 in case anyone
% uses this with some funny units of strain.
if max(map.sigma(:)) > 1e-10
  error('Surprisingly large value of strain power encountered.');
end

tempSNRmap = map.snr;
tempSNRmap(isnan(tempSNRmap)) = 0;

% map of NaNs: zeros where NaNs are present
nanmap0 = ~isnan(map.snr);

% optional PixelCut removes very loud pixels which can spoil background
if params.stochtrack.doPixelCut
  %map_snr(abs(map.y./map.sigma) > params.stochtrack.pixel_threshold) = 0;
  tempSNRmap(abs(tempSNRmap) > params.stochtrack.pixel_threshold) = 0;
end

max_cluster.snr_gamma = NaN;

% Calculate maximum cluster information.
tvals = map.segstarttime - map.segstarttime(1);
fvals = map.f;

for num=1:length(tempClusterInfo)

  trackInfo = load(tempClusterInfo{num});
  weights = trackInfo.stoch_out.cluster.reconMax;
  temp_nPix = sum(sum(weights(nanmap0)));

  tempSNRunNorm = sum(real(sum(weights.*tempSNRmap)));

  % normalization-----------------------------------------
  % 'npix' normalizes SNR by the number of pixels in the track.  This appears
  % to be the best option.
  if strcmp(params.stochtrack.norm, 'npix')
    tempSNR = tempSNRunNorm/sqrt(temp_nPix);
  elseif strcmp(params.stochtrack.norm, 'mean')
    tempSNR = tempSNRunNorm/temp_nPix;
  elseif strcmp(params.stochtrack.norm, 'ansatz')
    tempSNR = tempSNRunNorm./(temp_nPix^(3/4));
  end

  if params.stochtrack.singletrack.negSNRmode
    tempSNR = -tempSNR;
  elseif ~params.stochtrack.singletrack.posSNRmode
    tempSNR = abs(tempSNR);
  end

  if isnan(max_cluster.snr_gamma) || tempSNR > max_cluster.snr_gamma

    max_cluster.snr_gamma = tempSNR;
    max_cluster.nPix = temp_nPix;
    max_cluster.reconMax = weights;

    % calculate Y and sigma
    rmap = weights;
    rmap(nanmap0) = 0;%not sure if rmap is built correctly
    yr = rmap.*map.y;
    sr = rmap.*map.sigma;
    max_cluster.y_gamma = sum(sum(yr.*sr.^-2)) / sum(sum(sr.^-2));
    max_cluster.sigma_gamma = sum(sum(sr.^-2))^-0.5;

    % Calculate maximum cluster information.
    [r,c] = find(max_cluster.reconMax > 0);
    max_cluster.tmin = tvals(min(c));
    max_cluster.tmax = tvals(max(c));
    max_cluster.gps_min = map.segstarttime(min(c));
    max_cluster.gps_max = map.segstarttime(max(c));
    max_cluster.fmin = fvals(min(r));
    max_cluster.fmax = fvals(max(r));
    
    max_cluster.SNRfrac = trackInfo.stoch_out.cluster.SNRfrac;
    max_cluster.SNRfracTime = trackInfo.stoch_out.cluster.SNRfracTime;
    %max_cluster.source
  end
end

if params.stochtrack.singletrack.negSNRmode
    max_cluster.snr_gamma = -max_cluster.snr_gamma;
end
