function [yvals,xvals,H,ttvals,track_data] = stochtrack_points(map, params, aa);
% function max_cluster = run_stochtrack(map, params)
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.nPix -> number of pixels in the largest cluster
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.y_gamma -> Y (GW estimator) of largest cluster
%        max_cluster.sigma_gamma -> sigma of the above estimator
%        max_cluster.reconMax -> 2D array containing weight numbers for
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
% Written by E Thrane and M Coughlin
% Contact: ethrane@ligo.caltech.edu, michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% map is MxN: M is the # of frequency bins and N is the number of segments
[M,N] = size(map.snr);

% define maxdur to be N (the map time dimension) unless otherwise specified
try
   params.stochtrack.maxdur;
catch
   params.stochtrack.maxdur = N;
end

% define maxband to be M (the same as the frequency dimension unless otherwise
% specified
try
  params.stochtrack.maxband;
catch
  params.stochtrack.maxband = M;
end

% stochtrack parameters-------------------------------------------------------
% number of trials
T = params.stochtrack.T;
% number of trials in for loop
F = params.stochtrack.F;
% minimum track duration (IN PIXELS)
mindur = params.stochtrack.mindur;
% maximum track duration (IN PIXELS)
maxdur = params.stochtrack.maxdur;
%-----------------------------------------------------------------------------

% initialize yvals array
yvals = gZeros(N,T,params);
xvals = repmat(gColon(1,1,N,params)', 1, size(yvals,2));
ttvals = gArray(map.segstarttime,params);
H = gOnes(N,T,params);
d_foft = gOnes(N,T,params);
track_data = [];

if params.stochtrack.doBezier
 
  if params.stochtrack.doBezierPixels
    allvals = sort(abs(map.snr(:)),'descend');
    midval = allvals(round(0.1*length(allvals)));
    pixels = abs(map.snr);
    indexes = find(pixels > midval);
    [I,J] = ind2sub(size(pixels),indexes);
    [J,indexes] = sort(J,'ascend');
    I = I(indexes);
 
    indexes = [randperm(length(J),T) randperm(length(J),T) randperm(length(J),T) randperm(length(J),T)];
    fstart = I(indexes);
    nstarts = J(indexes);

    indexes = [randperm(length(J),T) randperm(length(J),T) randperm(length(J),T) randperm(length(J),T)];
    fmid = I(indexes);
    tmid = J(indexes);

    indexes = [randperm(length(J),T) randperm(length(J),T) randperm(length(J),T) randperm(length(J),T)];
    fstop = I(indexes);
    nstops = J(indexes);

    A = [nstarts tmid nstops];
    B = [fstart fmid fstop];
    [A,ix2] = sort(A,2);
    ix1 = repmat([1:size(A,1)]', [1 size(A,2)]); %//'
    ix = sub2ind(size(A), ix1, ix2);
    B = B(ix);

    nstarts = A(:,1); tmid = A(:,2); nstops = A(:,3);
    fstart = B(:,1); fmid = B(:,2); fstop = B(:,3); 

    durs = nstops - nstarts;
    indexes = find(durs >= mindur & durs <= maxdur);
    fstart = fstart(indexes);
    fmid = fmid(indexes);
    fstop = fstop(indexes);
    nstarts = nstarts(indexes);
    tmid = tmid(indexes);
    nstops = nstops(indexes);

    dfs = fstop - fstart;
    indexes = find(abs(dfs) <= params.stochtrack.maxband);
    fstart = fstart(indexes);
    fmid = fmid(indexes);
    fstop = fstop(indexes);
    nstarts = nstarts(indexes);
    tmid = tmid(indexes);
    nstops = nstops(indexes);

    indexes = find((fstart <= fmid & fmid <= fstop) | (fstart >= fmid & fmid >= fstop));
    fstart = fstart(indexes);
    fmid = fmid(indexes);
    fstop = fstop(indexes);
    nstarts = nstarts(indexes);
    tmid = tmid(indexes);
    nstops = nstops(indexes);

    indexes1 = sub2ind(size(map.snr),fstart,nstarts);
    indexes2 = sub2ind(size(map.snr),fmid,tmid);
    indexes3 = sub2ind(size(map.snr),fstop,nstops);
    snrs = map.snr(indexes1) + map.snr(indexes2) + map.snr(indexes3);

    [junk,indexes] = sort(snrs,'descend');
    indexes = indexes(1:T);
    fstart = fstart(indexes);
    fmid = fmid(indexes);
    fstop = fstop(indexes);
    nstarts = nstarts(indexes);
    tmid = tmid(indexes);
    nstops = nstops(indexes);

  else 
    % create track durations (in pixels) between mindur and maxdur=N
    durs = (maxdur-mindur-1)*gRand(T,1,params) + mindur;
    durs = round(durs);
    % create track start times between 1 and N-durs
    nstarts = (N-durs-1).*gRand(T,1,params) + 1;
    nstarts = round(nstarts);
    % calculate stop times
    nstops = nstarts + durs;
  
    % create start frequencies between 1 and M
    if params.stochtrack.doRamp
       fstart = (M-1)*gRandRamp(T,1,params) + 1;
    else
       fstart = (M-1)*gRand(T,1,params) + 1;
    end
    fstart = round(fstart);
  
    % calculate ending frequencies between 1 and M
    if params.stochtrack.maxband == M & ~ params.stochtrack.doMaxbandPercentage
  %    fstop = (M-1)*gRand(T,1,params) + 1;
  
      if params.stochtrack.doRamp
         fstop = (M-1)*gRandRamp(T,1,params) + 1;
      else
         fstop = (M-1)*gRand(T,1,params) + 1;
      end
  
    else
      % Limit delta f to some percentage of start frequency
      if params.stochtrack.doMaxbandPercentage
         fmax = min(M, fstart+fstart*params.stochtrack.maxbandPercentage);
         fmin = max(1, fstart-fstart*params.stochtrack.maxbandPercentage);
      else
         fmax = min(M, fstart+params.stochtrack.maxband);
         fmin = max(1, fstart-params.stochtrack.maxband);
      end
  
      if params.stochtrack.doRamp
         fstop = (fmax-fmin).*gRandRamp(T,1,params) + fmin;
      else 
         fstop = (fmax-fmin).*gRand(T,1,params) + fmin;
      %fstop = (fmax-fmin).*gRandRamp(T,1,params) + fmin;
      end
    end
    fstop = round(fstop);
  
    % different procedure for doCBC
    if params.stochtrack.doCBC
      fstart_hold = fstart; fstop_hold = fstop;
      fstart = min([fstart_hold fstop_hold]')';
      fstop = max([fstart_hold fstop_hold]')';
    end

    % calculate Bezier midpoint to be between fstart and fstop
    fmid = (fstop-fstart).*gRand(T,1,params) + fstart;
    tmid = nstarts + durs.*gRand(T,1,params);
  end

  % define Bezier parameter on [0,1] with step size 0.01
  p0 = repmat(nstarts', size(yvals,1), 1);
  p1 = repmat(tmid', size(yvals,1), 1);
  p2 = repmat(nstops', size(yvals,1), 1);

  % define evenly spaced Bezier xvals and work backward to get tt
  B1 = repmat(gColon(1,1,N,params)', 1, size(yvals,2));
  a = p0 - 2*p1 + p2;
  b = 2*(p1-p0);
  c = p0 - B1;
  tt = (-b + sqrt(complex(b.^2 - 4*a.*c))) ./ (2*a);

  % calculate Bezier curve B(t) for each trial
  p0 = repmat(fstart', size(tt,1), 1);
  p1 = repmat(fmid', size(tt,1), 1);
  p2 = repmat(fstop', size(tt,1), 1);
  B2 = (1-tt).^2.*p0 + 2*(1-tt).*tt.*p1 + tt.^2.*p2;

  % enforce start and stop times
  ttcut = tt<0 | tt>1;
  B2(ttcut) = NaN;
  yvals = B2;

% otherwise, if we are doing something besides Bezier, call stochtrack_tracks
% e.g., for doCBC and for r-mode tracks
elseif params.stochtrack.doMSmagnetar
  [yvals,xvals,ttvals,track_data] = stochtrack_magnetar(map, params, aa);
  %keyboard
else
  [yvals,xvals,H,ttvals,track_data] = stochtrack_tracks(map, params, aa);    
end

% Cut yvals outside of map
yvalscut = yvals<1 | yvals >M;
yvals(yvalscut) = NaN;

return;
