function stochtrack_createbank
% function run_clustermap_noise
tic;

params = stochtrackHierarchyDefaults;
params.stochtrack.livepoints=10;
params.stochtrack.livefrac = 1;

M=151; N=499;
map.snr = ones(M,N);

params.stochtrack.numPoints = 1000000;
[fstartAll,fstopAll,fmidAll,nstartsAll,nstopsAll] = stochtrack_get_points(map,params);

[snr_gamma,indexes,weight_up,weight_down,templates_up,templates_down,...
   fstart, fstop, fmid, nstarts, nstops] = ...
   stochtrack_calcsnr(map,params,fstartAll,fstopAll,fmidAll,nstartsAll,nstopsAll,params.stochtrack.numPoints);

snr_gamma_all = []; indexes_all = [];
weight_up_all = []; weight_down_all = [];
templates_up_all = []; templates_down_all = [];
fstart_all = []; fstop_all = []; fmid_all = []; 
nstarts_all = []; nstops_all = [];

snr_gamma_all = [snr_gamma_all snr_gamma(1)];
indexes_all = [indexes_all indexes(1)];
weight_up_all = [weight_up_all weight_up(:,1)];
weight_down_all = [weight_down_all weight_down(:,1)];
templates_up_all = [templates_up_all templates_up(:,1)];
templates_down_all = [templates_down_all templates_down(:,1)];
fstart_all = [fstart_all fstart(1)];
fstop_all = [fstop_all fstop(1)];
fmid_all = [fmid_all fmid(1)];
nstarts_all = [nstarts_all nstarts(1)];
nstops_all = [nstops_all nstops(1)];

index = 2;
maxTemplates = 30000;
overlapPercentage = 0.9;

tic;

while (index < length(snr_gamma) && length(snr_gamma_all) < maxTemplates)
   weight_up_1 = weight_up(:,index);
   weight_down_1 = weight_down(:,index);
   templates_up_1 = templates_up(:,index);
   templates_down_1 = templates_down(:,index);

   overlaps = zeros(length(snr_gamma_all),1);

   for j = 1:length(snr_gamma_all)

      weight_up_2 = weight_up_all(:,j);
      weight_down_2 = weight_down_all(:,j);
      templates_up_2 = templates_up_all(:,j);
      templates_down_2 = templates_down_all(:,j);

      [overlap_up,ia_up,ib_up] = intersect(templates_up_1,templates_up_2);
      [overlap_down,ia_down,ib_down] = intersect(templates_down_1,templates_down_2);

      overlap = sum(weight_up_1(ia_up)) / length(weight_up_1) + sum(weight_up_2(ib_up)) / length(weight_up_2) + sum(weight_down_1(ia_down)) / length(weight_down_1) + sum(weight_down_2(ib_down)) / length(weight_down_2);
      
      overlap = overlap / 2;

      overlaps(j) = overlap;

   end

   if max(overlaps) < overlapPercentage

      snr_gamma_all = [snr_gamma_all snr_gamma(1)];
      indexes_all = [indexes_all indexes(1)];
      weight_up_all = [weight_up_all weight_up(:,1)];
      weight_down_all = [weight_down_all weight_down(:,1)];
      templates_up_all = [templates_up_all templates_up(:,1)];
      templates_down_all = [templates_down_all templates_down(:,1)];
      fstart_all = [fstart_all fstart(1)];
      fstop_all = [fstop_all fstop(1)];
      fmid_all = [fmid_all fmid(1)];
      nstarts_all = [nstarts_all nstarts(1)];
      nstops_all = [nstops_all nstops(1)];
       
   end

   if mod(length(snr_gamma_all),1000)==0
      toc;
      fprintf('%d templates....\n',length(snr_gamma_all));
      tic;
   end
      

end

clear snr_gamma; clear indexes; clear weight_up; clear weight_down;
clear templates_up; clear templates_down;

save('stochbank.mat','fstart_all','fstop_all','fmid_all','nstarts_all','nstops_all');

return
