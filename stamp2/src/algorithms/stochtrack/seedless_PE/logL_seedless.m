function llhood  = loglikelihoodPE(map, model, parnames, parvals)




% First we will construct a temporary params struct mainly for gpu operations
paramstmp.doGPU = 1;
paramstmp.gpu.precision = 'double';


%------------------------- Seedless Part-------------------------------------

trueSeedlees = 0;
if trueSeedlees

% map is MxN: M is the # of frequency bins and N is the number of segments
[M,N] = size(map.snr);

% index function: one unique index value for every (x,y) pair
% follow matlab's scheme, which looks like this:
%   tmp = [[1 2 3]' [4 5 6]'];
idx = @(xi, yi) (xi-1)*M + yi;

% Get seedless templates for the msmagnetar waveforms
[yvals,xvals,ttvals,track_data] = stochtrack_magnetar(map, params, f0, tau);

% take real part to make GPU friendly
yvals = real(yvals);

% calculate weights
indices = find(isnan(yvals));
weights.up = mod(yvals,1);
weights.down = 1 - mod(yvals,1);
% set weights to zero where there is no track
weights.up(indices) = 0;
weight.down(indices) = 0;
yvals(indices) = M/2;
xvals(indices) = 1;


% calculate templates
yvals_up = ceil(yvals);
yvals_down = floor(yvals);
templates.up = idx(xvals, yvals_up);
templates.down = idx(xvals, yvals_down);

% number of pixel -  do not include pixels that contains NaNs
npix = sum(weight_up.*nanmap(templates_up),1) + ...
       sum(weight_down.*nanmap(templates_down),1);
end


%-------------------------------------------------------------------------------

% Define nsegs. This is also equal to the number of starting times. 
nsegs = size(map.y, 2);

nfreqs = size(map.y, 1);

% The basemap is the map of gravitational wave power, with the model
% starting at t = 0. To marginalize over start time, we will simply
% move this map, padding wiht zeros at the start.
basemap = feval(model, map, parnames, parvals); 

llhood = gArray(zeros(nsegs, 1), paramstmp);

% Iterate over start time on map
parfor ii = 1:nsegs
    
  % There needs to be (ii-1) zero pads at the beginning to account for each time shift    
  modelY = [zeros(nfreqs, ii-1), basemap(:, 1:(end -ii + 1))];
  
  % Set pixels with notches and glitch vetoes to zero.
  modelY(map.nanidx) = 0;        
  lsnr = (map.y - modelY)./map.sigma;
  [~, ic] = histc(lsnr, [-inf,(map.c(1:end-1)+map.c(2:end))/2,inf]);
  ib = map.p(ic);
  lmap = map.int_vals(ib);
  llhood(ii)  = sum(log(lmap(:)));
end

% Convert to real array to make matlab happy. 
llhood = gather(real(llhood));

% avoiding underflow of likelihood
Mx = max(llhood);
llhood = llhood - Mx;
% Marginalize
llhood = Mx + log(sum(exp(llhood)));

if llhood < -1e300
   llhood = -1e300;
end

if 0
  like = exp(llhood - max(llhood(:)));
  like = like/sum(like(:));

  keyboard
end
