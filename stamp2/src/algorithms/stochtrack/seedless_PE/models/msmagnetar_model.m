function modelY = msmagnetar_model(map,parnames,parvals)

%--------------------------------------------------------------------------------
% This function makes a model map of the gravitational wave power for the 
% msmagnetar model. Because of  how the stamp estimator is constructed, we 
% only need to caculate the GW power. Convolution with the detector response 
% tensor is not required. This would be the basemap with tstart = 0.
%  
% The input parameters are:
%  
%   parnames - a cell array containing the parameters names. These can be
%       in any order, but must include the following parameters:
%           {'alpha', 'nsegs', 'f0', 'tau'}
%
%    where nsegs is the number of time segmant in the ftmap, alpha is the
%    is the amplitude factor, f0 is the starting freq and tau is the time
%    scale parameter of the wave forms.
%  
%   parvals - a cell array containing the values of the parameters given in
%       parnames. These must be in the same order as in parnames.
%
%--------------------------------------------------------------------------------
%           This is the format required by nested_sampler.m.
%--------------------------------------------------------------------------------

  % check that parnames and parvals have the same length - the next few snippets
  % are borrowed from M.Coughlin's code
  nparams = length(parnames);
  lpv = length(parvals);
  if nparams ~= lpv
    error('Error: parnames and parvals are not the same length!');
  end
  
  % extract parameter values
  for ii=1:nparams
    switch parnames{ii}
      case 'beta0'
	beta = parvals{ii};
      case 'alpha'
	alpha = parvals{ii};
      case 'tau'
	tau = parvals{ii};
      case 'nn'
	nn = parvals{ii};      
    end
  end
  

  % fiducial epsilon value
  epsilon = 1e-2;

  % alpha
  alpha = 10^alpha;

  % f0
  f0 = beta*(tau)^(1/(1-nn));

  % we will construct a temp params for gpu operations
  paramstmp.doGPU = 1;
  paramstmp.gpu.precision = 'double';

  % Define some constants 
  segDur = map.segDur;
  nsegs = size(map.snr,2);
  duration = nsegs*segDur;
  fs = 4096;
  T0 = 0;
  NFFT = 2.^nextpow2(fs);
  
  % Hann window constant. 
  windowconst = 0.375;
    
  [hp, hc, t, f] = msmagnetarWF(f0,tau,nn,epsilon,duration,fs,T0);
  
  % Translate into gpu arrays
  hp = gArray(hp, paramstmp);
  hc = gArray(hc, paramstmp);
  
  % list of frequencies at which model powers are desired
  f = map.f(1):map.deltaF:map.f(end);
  
  % Initialize model map array
  modelY = gArray(zeros(length(f), nsegs), paramstmp);
  
  % Array of frequencies for the fft
  fftF = fs/2*linspace(0,1,NFFT/2+1);
  
  
  for ii = 1:nsegs

    idxmin = (ii-1)*fs + 1;
    idxmax = ii*fs;
    
    % Here we compute the psd using the ffts rather than use pwelch
    L = length(idxmin:idxmax);
    
    % Apply hann window for spectral leakage
    window = hann(L);
    
    % Calcualte fft for both the plus and cross pols
    yp = fft(hp(idxmin:idxmax) .* window,NFFT);
    yc = fft(hc(idxmin:idxmax) .* window,NFFT);
    
    % The one sided power is the sum of the plus and cross powers. 
    Y = 2 * ((yp(1:NFFT/2+1) .* conj(yp(1:NFFT/2+1))) + (yc(1:NFFT/2+1) .* conj(yc(1:NFFT/2+1))) ) / (L*fs*windowconst);
    
    modelY(:, ii) = alpha*interp1(fftF, Y, f);
    
  end
  

