function llhood = seedlessPE(map, params)
% function max_cluster = stochtrack(map, params)
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.nPix -> number of pixels in the largest cluster
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.y_gamma -> Y (GW estimator) of largest cluster
%        max_cluster.sigma_gamma -> sigma of the above estimator
%        max_cluster.reconMax -> 2D array containing weight numbers for
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
%        max_cluster.tmin
%        max_cluster.tmax
%        max_cluster.fmin
%        max_cluster.fmax
%        max_cluster.numpixels
%        max_cluster.SNRfrac  
% Written by E Thrane and M Coughlin
% Contact: ethrane@ligo.caltech.edu, michael.coughlin@ligo.org
%
% Modified for Seedless PE by Sharan Banagiri (sharan.banagiri@ligo.org)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% state if parallelization is on

% add multinest to path variable
addpath(genpath('/home/sharan.banagiri/VLT/bezier_VLT/matlabmultinest'))

% make directory for storing pe plots



PEpath = [ params.searchPath, '/pe'];
system(['mkdir -p ', PEpath]);

if params.stochtrack.doSeed
  rng(params.stochtrack.seed,'twister');
  if params.doGPU
    parallel.gpu.rng(params.stochtrack.seed,'Philox4x32-10');
  end
end

if params.doParallel
  fprintf(' (parallel on)...\n');
else
  fprintf(' (parallel off)...\n');
end

  
% map of NaNs: zeros where NaNs are present
nanmap0 = ~isnan(map.snr);

% NaNs occur due to missing data, vetoes, and notches.  When a NaN is
% encountered, y and snr are set to zero (so they do not add to the cluster SNR
% and sigma is set to one (much larger than typical strain power) so that the 
% cluster sigma does not depend significantly on the NaN pixels.  Just to be 
% safe, a check is employed to make sure that map.sigma << 1 in case anyone
% uses this with some funny units of strain.
if max(map.sigma(:)) > 1e-10
  error('Surprisingly large value of strain power encountered.');
end

% An index of where nans occur is necessary for parameter estimation
map.nanidx = isnan(map.snr);
map.snr(isnan(map.snr)) = 0;
map.y(map.nanidx) = 0;
map.z(isnan(map.z)) = 0;
map.sigma(map.nanidx) = 1;
map.sigma(map.sigma==0) = 1;

% --------------------------------------------------------------------------
% stochtrack parameters-------------------------------------------------------
% number of trials
T = params.stochtrack.T;
% number of trials in for loop
F = params.stochtrack.F;
%-----------------------------------------------------------------------------

% define GPU arrays
map_y = gArray(map.y, params);
map_z = gArray(map.z, params);
map_sigma = gArray(map.sigma, params);
nanmap = gArray(nanmap0, params);

%-------------------------------------------------------------------------------
%%% PARAMETER ESTIMATION ANALYSIS: Here we defined things needed for 
%%% Calcualting the likelihood function for parameter estimation


delf =  floor((params.stochtrack.msmagnetar.max_f0 - params.stochtrack.msmagnetar.min_f0)/F);
f0s = gColon(params.stochtrack.msmagnetar.min_f0,delf, params.stochtrack.msmagnetar.max_f0, params)';


% optional PixelCut removes very loud pixels which can spoil background
if params.stochtrack.doPixelCut
  map_snr(abs(map_snr) > params.stochtrack.pixel_threshold) = 0;
end

  
min_tau = params.stochtrack.msmagnetar.min_tau;
max_tau = params.stochtrack.msmagnetar.max_tau;
taus = gLogspace(log10(min_tau),log10(max_tau),params.stochtrack.msmagnetar.numTau, params)';


%------------------------------ SNR DIST---------------------------------------------------------------

if exist('snrDist.mat')
   load('snrDist.mat');
else
    [int_vals, c, p] = stamp_snr_dist();
end

map.c = c;
map.p = p;
map.int_vals  = int_vals; 



%---------------- NESTED SAMPLING ----------------------------------------------------------------------
global verbose;
verbose = 1;
global DEBUG;
DEBUG = 0;

% define nested sampling parameters
Nlive = 500;
Nmcmc = 0;
% Nmcmc = input('Input number of iterations for MCMC sampling: (enter 0 for multinest sampling)\n');
tolerance = 0.1;
likelihood = @logL_seedless;
model = @msmagnetar_model;
prior = {'alpha', 'uniform', 3, 10, 'fixed'; ...
        'beta0', 'uniform',1.55e4, 1.65e4, 'fixed';
        'tau', 'uniform', min_tau, max_tau, 'fixed'};

extraparams = {'fref', 1000; 'nn', 2.5}; % extra parameters beyond what's in the prior

% called nested sampling routine
[logZ, nest_samples, post_samples] = nested_sampler(map, Nlive, ...
  tolerance, likelihood, model, prior, extraparams, 'Nmcmc', Nmcmc);

% Make Plots
for ii = 1:size(prior, 1)
  posteriorPlot(post_samples, ii, {prior{ii, 1}}, [PEpath, '/', prior{ii, 1}, '.png'])
end
save([PEpath, '/pe.mat'], 'post_samples', 'prior');
keyboard
llhood = post_samples
return
