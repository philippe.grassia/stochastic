from __future__ import division
import numpy as np
import scipy.io as spio
import ConfigParser
import pdb
def readMatFile(paramsFile='IO/params.ini'):
    '''
    Read from  the stamp mat file, and extract necessary data
    '''
    
    # Read the pamas file
    config = ConfigParser.ConfigParser()    
    config.read(paramsFile)
    matfile = config.get("path", "matfile")

    # Read model name
    model = config.get("params", "model")
    
    # Load matfile.
    mat = spio.loadmat(matfile)

    # Unpack necessary arrays and structures and pack them 
    # into a dictionary
    map0 = mat['map0']
    params = mat['params']
    snrmap = map0['snr'][()][0, 0]
    ymap = map0['y'][()][0, 0]
    #fft1 = map0['fft1'][()][0, 0]
    #fft2 = map0['fft2'][()][0, 0]
    #CSD = map0['CSD'][()][0, 0]
    sigmap  = map0['sigma'][()][0, 0]
    startGPS = params['startGPS'][()][0, 0][0,0]
    segDur = map0['segDur'][()][0, 0]
    nsegs = snrmap[1, :].size
    freqs = map0['f'][()][0, 0]
    delF = float(map0['deltaF'][()][0, 0])

    segstarts  =  map0['segstarttime'][()][0, 0]
    delT = segstarts[0, 1] - segstarts[0, 0]

    # Get detector structs and antenna factors
    D1, D2 = map0['det1'][()][0, 0] , map0['det2'][()][0, 0]  
    det1 = dict([ ('r', D1['r'][()][0, 0]) , ('d', D1['d'][()][0, 0]) ])
    det2 = dict([ ('r', D2['r'][()][0, 0]) , ('d', D2['d'][()][0, 0]) ])
    gcc = map0['g'][()][0, 0]
    # Get sky direction
    ra   = params['pass'][()][0, 0]['stamp'][()][0, 0]['ra'][()][0, 0][0,0]
    decl = params['pass'][()][0, 0]['stamp'][()][0, 0]['decl'][()][0, 0][0,0]
    # check for large sigma's
    if np.nanmax(sigmap) > 1e-10:
        raise ValueError('Large value of strain in the map')

    # Check for nans
    nanidx = np.isnan(snrmap)
    snrmap[nanidx] = 0
    ymap[nanidx] = 0
    sigmap[nanidx] = 1
    zeroidx = (sigmap == 0)
    sigmap[zeroidx] = 1
    #fft1[nanidx] = 0
    #fft2[nanidx] = 0
    #CSD[nanidx] = 0

   

    # pack the maps into the dictionary
    data = dict([ ('snrmap', snrmap) , ('ymap', ymap)     , ('sigmap', sigmap) , 
                  ('nanidx', nanidx) , ('segDur', segDur) , ('nsegs',nsegs )   , 
                  ('freqs', freqs)   , ('delF', delF)     , ('delT', delT)     , 
                  ('model', model)   , ('det1', det1)     , ('det2', det2)     ,
                  ('startGPS', startGPS), ('segstartGPS', segstarts), ('ra', ra),
                  ('decl', decl) , ('gcc', gcc)])
    
    myinj = 0
    if myinj:
        from models.gwmodels import powermap
        cube = np.zeros(2)
        cube[0] = np.log10(5e6)
        cube[1] = 71
        print "redoing injection..."
        dataY  = powermap(cube, data, 'gwpower')
        sgmat = spio.loadmat('/home/sharan.banagiri/VLT/bezier_VLT/stamp2/src/algorithms/stochtrack/seedless_PE/python/secondPE/PE/IO/signal_25_alp_5e6.mat')
        pdb.set_trace()
        map1 = sgmat['map0']
        my_ymap = map1['my_y'][()][0, 0]
        my_ymap[nanidx] = 0
        rat = np.zeros(my_ymap.shape)
        for ii in range(0, 1771):
            for jj in range(0, 998):
                try:
                    rat[ii,jj] = dataY[ii,jj]/my_ymap[ii,jj]
                except:
                    rat[ii, jj] = 0

        norms1 = np.random.randn(ymap.shape[0], ymap.shape[1])
        ymap = dataY + sigmap*norms1
        ymap[nanidx] = 0
        data['ymap'] = ymap
        
    return data
