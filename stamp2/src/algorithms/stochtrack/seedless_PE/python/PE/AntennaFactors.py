from __future__ import division
import numpy as np

def GPStoGMT(GPSTimes):

   '''
   This is a copy of GPStoGreenwichMeanSiderealTime.m
   '''

   #from International Earth Rotation Service, measured in 2002
   # see http://hypertextbook.com/facts/2002/JasonAtkins.shtml
   wearth= 2*np.pi * (1/365.2425 +1) / 86400;

   # same thing in hours / sec
   w = wearth/np.pi*12;

   # GPS time for 1/1/2000 00:00:00 UTC
   GPS2000 = 630720013;
   
   # sidereal time at GPS2000,in hours
   # from http://www.csgnetwork.com/siderealjuliantimecalc.html
   sT0 = 6 + 39/60 + 51.251406103947375/3600;

   siderealTimes = w*(GPSTimes-GPS2000) + sT0
   
   return siderealTimes


def calcAntennaFactors(ra, decl, GPSTimes, det1, det2):


   '''
   Calculate Antenna factors for the given GPStimes and for ra and decl. This is just a python copy of calF.m

   Input: Source direction, GPS times, and the detector dicts

   Output: Struct containing antenna factors for both the detectors and the time-difference
   sharan.banagiri@ligo.org
   '''


   if ra <0 or ra > 24:
      raise ValueError('ra out of range. Should be between 0 to 24')

   if decl<-90 or decl > 90:
      raise ValueError('decl out of range. Should be between -90 to 90')

   time = GPStoGMT(GPSTimes)
   
   cspeed = 299792458

   # convert hours into radians: 24 hrs / 2pi rad
   w = np.pi/12;
   N = np.size(GPSTimes);
   
   # calculate ingredients needed for antenna factors
   psi = w*(time-ra)
   theta = -np.pi/2 + np.pi*decl/180
   ctheta = np.cos(theta)
   stheta = np.sin(theta)
   cpsi = np.cos(psi)
   spsi = np.sin(psi)
   Omega1 = -cpsi*stheta
   Omega2 = spsi*stheta
   Omega3 = ctheta
   pp11 = (ctheta**2)*(cpsi**2) - (spsi**2)
   pp12 = -(1 + ctheta**2) * cpsi * spsi
   pp13 = ctheta * cpsi * stheta
   pp22 = (ctheta**2) * (spsi**2) - (cpsi**2)
   pp23 = -ctheta * spsi * stheta
   pp33 = stheta**2
   px11 = -2*ctheta * cpsi * spsi
   px12 = ctheta * ((spsi**2)-(cpsi**2))
   px13 = -stheta * spsi
   px22 = 2*ctheta * cpsi * spsi
   px23 = -stheta * cpsi
   
   # calcualte distance between the two detectors
   ss = det2['r'] - det1['r']
   distance = np.linalg.norm(ss)

   tau = (Omega1*ss[0]+Omega2*ss[1]+Omega3*ss[2])/cspeed;

   # define antenna factors
   F1p = det1['d'][0,0]*pp11 + 2*det1['d'][0,1]*pp12 + 2*det1['d'][0, 2]*pp13 + \
         det1['d'][1,1]*pp22 + 2*det1['d'][1,2]*pp23 + det1['d'][2,2]*pp33

   F2p = det2['d'][0,0]*pp11 + 2*det2['d'][0,1]*pp12 + 2*det2['d'][0, 2]*pp13 + \
         det2['d'][1,1]*pp22 + 2*det2['d'][1,2]*pp23 + det2['d'][2,2]*pp33

   F1x = det1['d'][0,0]*px11 + 2*det1['d'][0,1]*px12 + 2*det1['d'][0, 2]*px13 + \
         det1['d'][1,1]*px22 + 2*det1['d'][1,2]*px23 # + det1['d'][2,2]*px33 This was commented out in calF.m, which I retain here

   F2x = det2['d'][0,0]*px11 + 2*det2['d'][0,1]*px12 + 2*det2['d'][0, 2]*px13 + \
         det2['d'][1,1]*px22 + 2*det2['d'][1,2]*px23 # + det2['d'][2,2]*px33 This was commented out in calF.m, which I retain here
   
   # Dict with antenna factors and tau
   antFact = dict([ ('tau', tau), ('F1p', F1p), ('F2p', F2p), ('F1x', F1x), ('F2x', F2x) ])

   return antFact
