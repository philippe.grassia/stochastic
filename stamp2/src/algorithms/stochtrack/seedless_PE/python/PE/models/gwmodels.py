from __future__ import division
import numpy as np
import pdb
from crossPower import modelmap


def powermap(cube, data, maptype):

    # ------------------ Setup Stuff -----------------------
    # map duration
    duration = int(data['nsegs']*data['delT'])
    # Sampling frequency
    fs = 2048
    
    # Hann Window constant
    windconst  = 0.375
    
    # Pixel frequencies
    f = data['freqs']
    nsegs  = data['nsegs']
    segDur = data['segDur'][0, 0]
    # Initialize model map
    modelY = np.zeros((f.size, nsegs))

    # ---------------- Pick Model ----------------------------
    if data['model'] == 'msmagnetar':
    
        alpha, beta, tau, st, nn = cube[0],750,100,cube[1],2.5
        
        # alpha
        alpha = 10**(alpha)

        # Fiducial epsilon value
        epsilon = 1e-2

        #f0 = beta*(tau)**(1/(1-nn))
        f0 = beta
    
        # true duration
        truedur = duration - st
        
        # Calculate the strain
        hp, hc, t  = msmagnetarWF(f0, tau, nn, epsilon, truedur + 0.5, fs)
        
        hp = (np.sqrt(alpha))*np.concatenate([np.zeros(duration*fs - int(truedur*fs)), hp])
        hc = (np.sqrt(alpha))*np.concatenate([np.zeros(duration*fs - int(truedur*fs)), hc])
        
    elif data['model'] == 'mono':

        alpha, f0, st = cube[0], cube[1],cube[2]

        alpha = 10**alpha

        # true duration
        truedur = duration - st
        
        # Calculate the strain
        hp, hc, t  = monoWF(f0, truedur + 0.5, fs)

        hp = (np.sqrt(alpha))*np.concatenate([np.zeros(duration*fs - int(truedur*fs)), hp])
        hc = (np.sqrt(alpha))*np.concatenate([np.zeros(duration*fs - int(truedur*fs)), hc])

    else:
        raise ValueError('Unknown model requested')

    # -------------------- Do FFTs and create power spectrogram -----------------------


    # We actually do the fft and create model maps in here. 
    modelY = modelmap(hp, hc, data, fs, maptype)
    # Check for consistence
    if f.shape[0] != modelY.shape[0]:
        pdb.set_trace()
    
    return modelY
    

## ---------------------------- Magnetar Waveform ----------------------------------------------------

def msmagnetarWF(f0,tau,nn,epsilon,duration,fs):

    T0 = 0
    dt = 1/fs
    try:
        #t, tstep = np.linspace(T0, T0+duration, np.around(1+duration*fs), endpoint=True,retstep=True)
        t = dt*np.arange(T0, T0+np.around(1+fs*duration), 1)
    except:
        pdb.set_trace()


    if t[2] - t[1] !=dt:
        raise ValueError('Time step is not 1/fs. This will drift the calculations')

    # msmagnetar model
    f = f0*(1 + t/tau)**(1/(1-nn))
    
    # Fiducial Moment of Inertia
    II = 1e45
    G = 6.67408*1e-8 # cgs
    c = 3*10**10 # cm/s
    dd = 40*3.0857e24 # cm
    Phi0 = 0
    iota = 0

    consts = 4 * (np.pi**2) * G / (c**4)
    h_0 = consts * II * epsilon * (f**2) / dd
    
    term1 = 2.0* np.pi * tau * f0 * (1.0-nn) / (2.0-nn)
    term2 = (1 + (t/tau))**((2.0-nn) / (1.0-nn)) - 1
    Phi = Phi0 + term1 * term2

    hp = (h_0/2) * (1 + np.cos(iota)**2)*np.cos(Phi)
    hc = h_0 * np.cos(iota) * np.sin(Phi);
    return hp, hc, t

# -------------- Define monochromatic waveform ----------------

def monoWF(f0, duration, fs):

    phi0 = 0
    fdot = 0
    T0 = 0
    h0 = 1e-20
    dt = 1/fs
    t = dt*np.arange(T0, T0+np.around(1+fs*duration), 1)

    # create hp and hx
    phi = phi0 + 2*np.pi*f0*t # +  0.5*fdot*t**2
    hp = h0*np.cos(phi)
    hx = h0*np.sin(phi)
    return hp, hx, t
