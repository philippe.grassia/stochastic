from __future__ import division
import numpy as np
from models.gwmodels import powermap

def loglike(data, zsnr, zsnr_intvals, cube):
    #start_time = time.time()
    
    # Make model power map. 
    modelY = powermap(cube, data, 'crosspower')    

    # Initialize llhood
    llhood = np.zeros((data['nsegs'], 1))

    
    # pdb.set_trace()
    # unpack ymap and sigmamap ..
    ymap = data['ymap']
    sigmap = data['sigmap']
    nsegs = data['nsegs']
    
    # Remove nans
    modelY[data['nanidx']] = 0
    
    lsnr = (ymap-modelY)/(sigmap)
    lm = np.log(np.interp(lsnr.flatten(), zsnr,zsnr_intvals))
    # lm = -(1/2)*((ymap - modelY)/sigmap)**2
    
    llhood = np.sum(lm)/10


    # llhood = llhood[st:et];

    # avoiding underflow
    # Mx = np.amax(llhood)
    #llhood = llhood - Mx
    
    # Marginalize over start time
    # llhood = Mx + np.log(np.sum(np.exp(llhood)))
    #print("--- %s seconds ---" % (time.time() - start_time))

    #    print llhood
    return llhood
