from __future__ import division
import sys,subprocess,ConfigParser, os
import matplotlib
matplotlib.use('Agg')
import json, pdb
import numpy as np
import pymultinest
from logL_stamp import loglike
from snrdist import snrdist
from plotmaker import plotmaker
from IO.dataIO import readMatFile


def prior(cube, ndim, nparams):
    
    cube[0]  = 6*cube[0] + 3.0 # amplitde: log priors from 1e3 to 1e7
    cube[1]  = 20*cube[1] + 260 # start time
    #cube[2]  = (50*cube[2] + 725)  #100*cube[1] + 700 # 50*cube[1] + 725 beta parameter : uniform prior frm 6e3 to 6.5e3
    #cube[2]  = 20*cube[2] + 90 # tau parameter: uniform prior from 90 - 110 seconds
    #cube[4]  = 1*cube[4]+2 # nn value. Should be moved to a more appropriate place later 
    
class stampPE:

    # Initialize data arrays for the class.
    def __init__(self,data, zsnr, zsnr_intvals):
        self.data = data
        self.zsnr = zsnr
        self.zsnr_intvals  = zsnr_intvals

    # define likelihood wrapper for logL_stamp
    def logL(self, cube, ndim, nparams):
        llike = loglike(self.data, self.zsnr, self.zsnr_intvals, cube)
        return llike


def seedlessPE():

    '''
    multinest wrapper for loglikelihood computation with stamp maps. 
    
    '''    

    #---------------- Extract Data ------------------
    # Check if the snr dist file exists
    if not os.path.isfile('snr_dist.txt'):
        snrdist()

    # load the stamp dist file
    zsnr,zsnr_intvals = np.loadtxt('snr_dist.txt')

    # Load data and map parameters
    data = readMatFile()

    # Make some checks on the data, thrown some errors
    if data['segDur'] < 1:
        raise ValueError('Need segment duration to be at least 1 second')
        
    # Check to see if the data is made form 50% overlapping bins
    if 2*data['delT'] != data['segDur']:
        raise ValueError('For now, this code only works with 50% overlapping segments')
     
    
    # Get nlive
    config = ConfigParser.ConfigParser()
    config.read('IO/params.ini')
    nlive = int(config.get("params", "nlive"))

    # -------------- Initialize stampPE class ---------------------------------
    
    msmagPE = stampPE(data, zsnr, zsnr_intvals)
    

    #------------------ NESTED SAMPLING ----------------------------------#

    # Number of parameters
    parameters = ["alpha", "T0"] #, "tau",'T0']
    truevals = np.array([np.log10(5e6), 270])  #, 100, 71])
    n_params = len(parameters)
    
    outdir = "./out_2par_" + str(nlive) + "/"

    # Is there a signal
    sg = 1

    pjson = outdir + "/params.json"
    subprocess.call(["mkdir" , "-p", outdir])
    
    # run Multinest
    pymultinest.run(msmagPE.logL, prior, n_params,outputfiles_basename=outdir, resume = True, verbose = True, n_live_points=nlive, evidence_tolerance = 0.1)
    json.dump(parameters, open(pjson, 'w')) 


    # Make plots
    plotmaker(outdir, sg, parameters, truevals)



if __name__ == "__main__":

    if len(sys.argv) != 1:
        raise ValueError('This function does not take any arguments')    
    else:
        seedlessPE()
