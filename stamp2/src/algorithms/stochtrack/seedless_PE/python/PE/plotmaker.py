from __future__ import division
import numpy as np
import matplotlib
matplotlib.use('Agg')
import corner
import matplotlib.pyplot as plt
import sys, os
import json
import pymultinest
import pdb
plt.rc('xtick',labelsize=16)
plt.rc('ytick',labelsize=16)

def plotmaker(outdir, sg, params, *argv):


    if not argv:
        truevals = np.array([np.log10(5e6), 700, 100])
    else:
        truevals = np.array(argv[0])

    sg = int(sg)
    if outdir[-1] != '/':
        outdir = outdir + '/'

    if not os.path.exists(outdir + 'params.json'):
        sys.stderr.write("Expected the file %sparams.json with the parameter names")

    # List of parameters
    #params = json.load(open(outdir+'params.json'))
    # Number of parameters
    npar = len(params)

   # params[0] =  'log(I'+ '$' + '\\' + "epsilon" +'$' +')'
   # params[1] = '$'+ 'f_0'+ '$'
   # params[2] = '$'+ '\\' + params[2] + '$'
   # params[3] = '$'  + 'T_{0}'  + '$' 
   # params[4] = 'n' 
    # analyse data
    outdata = pymultinest.Analyzer(npar, outputfiles_basename=outdir)
    
    # These are the posteriors. The first three columns are the values of the parameters, 
    # and the last column is the ln(Z)
    multinest_output = outdata.get_equal_weighted_posterior()

    
    # Check that you have posteriors for all dimensions
    if multinest_output.shape[1] != npar +1:
        sys.stderr.write("Number of parameters in the json file doesnt match the number from multinest output")
    #pdb.set_trace()
    post = multinest_output[:, 0:npar]

    # Convert chanins to I\epsilon, f0 and tau posteriors.
    #post[:, 0] = (43 + 0.5*post[:,0])
    #post[:, 1] = post[:, 1]* (post[:, 2])**(1/(1 - post[:, 4]))

    fig = corner.corner(post,smooth1d=True, labels=params, quantiles=(0.025,0.16, 0.84, 0.975), 
                        show_titles=True, title_kwargs={"fontsize": 14}, 
                        label_kwargs={"fontsize":0}, title_fmt=".3f",
                        fill_contours=True, use_math_text=True, max_n_ticks=3, labelsize=10)

    
    # Put correct values
    # Extract the axes
    axes = np.array(fig.axes).reshape((npar, npar))
    
    for ii in range(npar):
        ax = axes[ii, ii]
        if sg:
            ax.axvline(truevals[ii], color="g", label='true value')
    #plt.savefig(outdir + 'corners.png', dpi=200)
    plt.savefig(outdir + 'corners.pdf', dpi=200)

    for ii in range(0, axes.shape[0]):
        for jj in range(0, axes.shape[0]):
            ax = axes[ii, jj]
            ax.tick_params(axis='both', labelsize=20)





if __name__ == "__main__":

    if len(sys.argv) < 3:
        raise ValueError('Insufficient number of inputs provided')
    else:
        plotmaker(sys.argv[1], sys.argv[2])
