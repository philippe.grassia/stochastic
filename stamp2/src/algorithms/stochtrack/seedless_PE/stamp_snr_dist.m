function[int_vals, c, p] = stamp_snr_dist();

  N = 8;
  sigma1 = 1;
  sigma2 = 1/1.1809;
  constant = (N^(2*N))/((2^(2*N-1)) * sigma2^(4*N+2) * gamma(N)^2);
  z = -100:0.1:100;
  x = 0.01:0.01:10;
  int_vals = [];
  for i = 1:length(z)
     y = constant * abs(x) .* exp(-abs(x*z(i))/sigma1^2) .* besselk(0,(N*x)/sigma2^2) .* x.^(2*N - 1);
     int_vals(i) = 100 * trapz(x,y);
   end
   int_vals = int_vals / sum(int_vals);
   [c,p] = sort(z);
   
   save('snrDist.mat','int_vals','c','p');


   return 
