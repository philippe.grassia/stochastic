function max_cluster = lonetrack(map, params, pp)
% function max_cluster = lonetrack(map, params)
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.nPix -> number of pixels in the largest cluster
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.y_gamma -> Y (GW estimator) of largest cluster
%        max_cluster.sigma_gamma -> sigma of the above estimator
%        max_cluster.reconMax -> 2D array containing weight numbers for
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
%        max_cluster.tmin
%        max_cluster.tmax
%        max_cluster.fmin
%        max_cluster.fmax
%        max_cluster.numpixels
%        max_cluster.SNRfrac
% Written by E Thrane and M Coughlin
% Contact: ethrane@ligo.caltech.edu, michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if params.doGPU
   g = gpuDevice();
   reset(g);
end

map.Q = [];
map.eps_12 = [];
map.eps_11 = [];
map.eps_22 = [];
map.snrz = [];
map.g = [];
map.ccSpec = [];
map.pp = [];
map.y_f = [];
map.Xi = [];
map.sigma_Xi = [];
map.Xi_snr = [];

if params.stochtrack.lonetrack_firstDerivCut
%%%% Next line added by Sean Morriss, 10-1-15 to apply the first derivative cut
   map = firstDerivCut(map,params.plotdir);
end

% Any cuts on map.snr (from glitch cut or frequency notches) also apply to naiP1 and naiP2
map.naiP1(isnan(map.snr)) = NaN;
map.naiP2(isnan(map.snr)) = NaN;
map.fft1(isnan(map.snr)) = NaN;
map.fft2(isnan(map.snr)) = NaN;

% apply hot pixel cut (map.snr)
if params.stochtrack.doPixelCut
   indexes = find(abs(map.snr) > params.stochtrack.pixel_threshold); 
   map.naiP1(indexes) = NaN;
   map.naiP2(indexes) = NaN;
   map.fft1(indexes) = NaN;
   map.fft2(indexes) = NaN;
end

params_plotdir = params.plotdir;
if params.savePlots

   plotpath = [params.plotdir '/snr'];
   system(['mkdir -p ' plotpath]);

   map.xvals=map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);
   map.yvals = map.f;

   figure;
   xlabel_text = 't (s)';
   ylabel_text = 'f (Hz)';
   printmap(map.snr, map.xvals, map.yvals, xlabel_text, ylabel_text, 'SNR', [-5 5]);
   print('-dpng',[plotpath '/snr']);
   close;
end

% define single-detector statistic
map.y = map.naiP1;
map.z = 0*map.y;

fprintf('Running with lonetrack sigma type: %s\n',params.stochtrack.lonetrack_sigma);
if strcmp(params.stochtrack.lonetrack_sigma,'mean')
   map.sigma = zeros(size(map.f));
   for ii = 1:length(map.sigma)
      naiP1 = map.naiP1(ii,:);
      indexes = find(~isnan(naiP1));
      map.sigma(ii) = mean(naiP1(indexes));
   end
   sigmaHold = map.sigma;
   map.sigma = repmat(map.sigma, 1, size(map.naiP1,2));
elseif strcmp(params.stochtrack.lonetrack_sigma,'neighbor')
   sigmaHold = nanmean(map.naiP1,2);
   sigmaHold(isnan(sigmaHold)) = 0;
   map.sigma = map.P1;
end

map.sensInt = map.sensInt.data;
map.ccVar = map.ccVar(1,:);

% apply hot pixel cut (map.naiP1)
if params.stochtrack.doLonetrackPixelCut
   indexes = find(map.naiP1./map.sigma > params.stochtrack.lonetrack_pixel_threshold);
   map.naiP1(indexes) = NaN;
   map.sigma(indexes) = NaN;
   fprintf('Chosen threshold removes %.5f percent of pixels\n',100*length(indexes)/length(map.naiP1(:)));
end
if params.stochtrack.doLonetrackAltSigma
   [map,params] = altsigma(map,params);
end 
map1 = map.naiP1./map.sigma;

params_plotdir = params.plotdir;
if params.savePlots

   plotpath = [params.plotdir '/P1'];
   system(['mkdir -p ' plotpath]);

   map.xvals=map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);
   map.yvals = map.f;

   naiP1Hold = map1(~isnan(map1));
   figure;
   xlabel_text = 't (s)';
   ylabel_text = 'f (Hz)';
   printmap(map1, map.xvals, map.yvals, xlabel_text, ylabel_text, 'naiP1', [1 params.stochtrack.lonetrack_pixel_threshold]);
   print('-dpng',[plotpath '/lsnr']);
   close;

   bins = linspace(0,10,100);
   [N,edges] = histc(naiP1Hold,bins);
   figure;
   %bins = (edges(2:end) + edges(1:end-1))/2;
   N = N / sum(N);
   semilogy(bins,N,'k--');
   xlabel('naiP1');
   ylabel('PDF');
   print('-dpng',[plotpath '/hist']);
   close;

   naiP1Statistic = 2*naiP1Hold';
   naiP1Statistic = sort(naiP1Statistic,'ascend');
   chi2cdfdist = chi2cdf(naiP1Statistic,2);
   [chi2cdfdist,indexes] = unique(chi2cdfdist);
   naiP1Statistic = naiP1Statistic(indexes);
   chi2cdfvals = [naiP1Statistic; chi2cdfdist]';

   [ks_h,ks_p] = kstest(naiP1Statistic,chi2cdfvals);
   fprintf('K-S test: %d with p = %e\n',ks_h,ks_p);

%%%% Next line to prevent out-of-memory. sean.morriss@yahoo.com 4-9-16
   edges_step = ceil((max(naiP1Statistic)+2)/40000);
%%%%   edges = [0:1:max(naiP1Statistic)+1];
   edges = [0:edges_step:max(naiP1Statistic)+1];


   figure;
   histc_values = histc(naiP1Statistic,edges);
   hold on
   bar(edges+0.5,histc_values/sum(histc_values),1,'r')
   plot(edges+0.5,chi2pdf(edges+0.5,2),'*')
   hold off
   xlim([0 10]);
   hleg1 = legend('Recolored','chi^2');
   set(hleg1,'Location','NorthEast')
   set(hleg1,'Interpreter','none')
   pretty;
   print('-dpng',[plotpath '/chi2pdf']);
   close;

   figure;
   loglog(map.f,sigmaHold,'k--');
   xlim([min(map.f) max(map.f)]);
   pretty;
   print('-dpng',[plotpath '/psd']);
   close;

   figure;
   semilogx(map.f,nanvar(map1')./nanmean(map1'),'k--');
   xlim([min(map.f) max(map.f)]);
   pretty;
   print('-dpng',[plotpath '/rel_var']);
   close;

   params.plotdir = plotpath;

end

if params.stochtrack.doECBC
% now call stochtrack
   eccentricities = linspace(params.stochtrack.cbc.min_eccentricity,...
      params.stochtrack.cbc.max_eccentricity,params.stochtrack.cbc.n_eccentricity);
   snrs = [];
   datas = [];
   for ii = 1:length(eccentricities)
      params.stochtrack.cbc.min_eccentricity = eccentricities(ii);
      params.stochtrack.cbc.max_eccentricity = eccentricities(ii);
      params.stochtrack.cbc.n_eccentricity = 1;

      params_temp = params;
      params_temp.stochtrack.doPixelCut = 0;

      out1 = stochtrack(map, params_temp);
      datas{ii} = out1;
      snrs(ii) = out1.snr_gamma;
   end
   [junk,index] = max(snrs);
   out1 = datas{index};
elseif params.stochtrack.doBurstegard
   params_temp = params;
   out1 = run_burstegard(map, params_temp);
   if params.savePlots
      burstegard_plot(out1,map,params_temp);
   end
elseif params.stochtrack.doViterbi
   params_temp = params;
   out1 = viterbi(map, params_temp);
else
   params_temp = params;
   params_temp.stochtrack.doPixelCut = 0;
   out1 = stochtrack(map, params_temp);
end

clear map1 params_temp;

% record the zero-lag cross-correlation statistic
recon1 = ceil(out1.reconMax);
idx1 = find(recon1==1);
out1.cc = map.cc(idx1);
out1.sensInt = map.sensInt(idx1);
ccVarMap = repmat(map.ccVar, size(map.cc,1), 1);
out1.ccVar = ccVarMap(idx1);
out1.fft1 = map.fft1(idx1);
fmap = repmat(map.f, 1, size(map.cc,2));
out1.fvals = fmap(idx1);
out1.deltaF = map.f(2) - map.f(1);
out1.P1 = map.P1(idx1);
out1.naiP1 = map.naiP1(idx1);
out1.pp = pp;
out1.params = params;
out1.reconMax = recon1;

out2a.fft2 = map.fft2(idx1);
out2a.P2 = map.P2(idx1);

clear ccVarMap fmap indexes recon1 sigmaHold idx1;
[snrmax1, snr0_1, SNRfrac1, timedelay1] = csnr_v3(params, out1, out2a);

if params.doGPU
   save([params.lonetrackdir '/ifo1'],'out1');
   clear out1 out2a;
end

% repeat for detector two------------------------------------------------------
if params.doGPU
   g = gpuDevice();
   reset(g);
end

map.y = map.naiP2;
map.z = 0*map.y;

if strcmp(params.stochtrack.lonetrack_sigma,'mean')
   map.sigma = zeros(size(map.f));
   for ii = 1:length(map.sigma)
      naiP2 = map.naiP2(ii,:);
      indexes = find(~isnan(naiP2));
      map.sigma(ii) = mean(naiP2(indexes));
   end
   sigmaHold = map.sigma;
   map.sigma = repmat(map.sigma, 1, size(map.naiP2,2));
elseif strcmp(params.stochtrack.lonetrack_sigma,'neighbor')
   sigmaHold = nanmean(map.naiP2,2);
   sigmaHold(isnan(sigmaHold)) = 0;
   map.sigma = map.P2;
end

% apply hot pixel cut (map.naiP2)
if params.stochtrack.doLonetrackPixelCut
   indexes = find(map.naiP2./map.sigma > params.stochtrack.lonetrack_pixel_threshold);
   map.naiP2(indexes) = NaN;
   map.sigma(indexes) = NaN;
   fprintf('Chosen threshold removes %.5f percent of pixels\n',100*length(indexes)/length(map.naiP2(:)));
end
if params.stochtrack.doLonetrackAltSigma
   [map,params] = altsigma(map,params);
end
map2 = map.naiP2 ./ map.P2;

params.plotdir = params_plotdir;
if params.savePlots
   plotpath = [params.plotdir '/P2' ];
   system(['mkdir -p ' plotpath]);

   map.xvals=map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);
   map.yvals = map.f;

   naiP2Hold = map.naiP2./map.sigma;
   naiP2Hold = naiP2Hold(~isnan(naiP2Hold));
   figure;
   xlabel_text = 't (s)';
   ylabel_text = 'f (Hz)';
   printmap(map2, map.xvals, map.yvals, xlabel_text, ylabel_text, 'naiP2', [1 params.stochtrack.lonetrack_pixel_threshold]);
   print('-dpng',[plotpath '/lsnr']);
   close;

   bins = linspace(0,10,100);
   [N,edges] = histc(naiP2Hold,bins);
   figure;
   %bins = (edges(2:end) + edges(1:end-1))/2;
   N = N / sum(N);
   semilogy(bins,N,'k--');
   xlabel('naiP1');
   ylabel('PDF');
   print('-dpng',[plotpath '/hist']);
   close;

   naiP2Statistic = 2*naiP2Hold';
   chi2cdfdist = chi2cdf(naiP2Statistic,2);
   chi2cdfvals = [naiP2Statistic; chi2cdfdist]';

   [ks_h,ks_p] = kstest(naiP2Statistic,chi2cdfvals);
   fprintf('K-S test: %d with p = %e\n',ks_h,ks_p);

%%%% Next line to prevent out-of-memory. sean.morriss@yahoo.com 4-9-16
   edges_step = ceil((max(naiP2Statistic)+2)/40000);
%%%%   edges = [0:1:max(naiP2Statistic)+1];
   edges = [0:edges_step:max(naiP2Statistic)+1];


   figure;
   histc_values = histc(naiP2Statistic,edges);
   hold on
   bar(edges+0.5,histc_values/sum(histc_values),1,'r')
   plot(edges+0.5,chi2pdf(edges+0.5,2),'*')
   hold off
   xlim([0 10]);
   hleg1 = legend('Recolored','chi^2');
   set(hleg1,'Location','NorthEast')
   set(hleg1,'Interpreter','none')
   pretty;
   print('-dpng',[plotpath '/chi2pdf.png']);
   close;

   figure;
   loglog(map.f,sigmaHold,'k--');
   xlim([min(map.f) max(map.f)]);
   pretty;
   print('-dpng',[plotpath '/psd.png']);
   close;

   figure;
   semilogx(map.f,nanvar(map2')./nanmean(map2'),'k--');
   xlim([min(map.f) max(map.f)]);
   pretty;
   print('-dpng',[plotpath '/rel_var']);
   close;

   params.plotdir = plotpath;

end

% call stochtrack
if params.stochtrack.doECBC
% now call stochtrack
   snrs = [];
   datas = [];
   for ii = 1:length(eccentricities)
      params.stochtrack.cbc.min_eccentricity = eccentricities(ii);
      params.stochtrack.cbc.max_eccentricity = eccentricities(ii);
      params.stochtrack.cbc.n_eccentricity = 1;

      params_temp = params;
      params_temp.stochtrack.doPixelCut = 0;

      out2 = stochtrack(map, params_temp);
      datas{ii} = out2;
      snrs(ii) = out2.snr_gamma;
   end
   [junk,index] = max(snrs);
   out2 = datas{index};
elseif params.stochtrack.doBurstegard
   params_temp = params;
   out2 = run_burstegard(map, params_temp);
   if params.savePlots
      burstegard_plot(out2,map,params_temp);
   end
elseif params.stochtrack.doViterbi
   params_temp = params;
   out2 = viterbi(map, params_temp);
else
   params_temp = params;
   params_temp.stochtrack.doPixelCut = 0;
   out2 = stochtrack(map, params_temp);
end

clear map2 params_temp;

% record the zero-lag cross-correlation statistic
recon2 = ceil(out2.reconMax);
idx2 = find(recon2==1);
out2.cc = map.cc(idx2);
out2.sensInt = map.sensInt(idx2);
ccVarMap = repmat(map.ccVar, size(map.cc,1), 1);
out2.ccVar = ccVarMap(idx2);
out2.fft2 = map.fft2(idx2);
fmap = repmat(map.f, 1, size(map.cc,2));
out2.fvals = fmap(idx2);
out2.deltaF = map.f(2) - map.f(1);
out2.P2 = map.P2(idx2);
out2.naiP2 = map.naiP2(idx2);
out2.pp = pp;
out2.params = params;
out2.reconMax = recon2;

out1b.pp = pp;
out1b.params = params;
out1b.deltaF = map.f(2) - map.f(1);
out1b.fvals = fmap(idx2);
out1b.fft1 = map.fft1(idx2);
out1b.P1 = map.P1(idx2);

clear ccVarMap fmap indexes recon2 sigmaHold idx2;
[snrmax2, snr0_2, SNRfrac2, timedelay2] = csnr_v3(params, out1b, out2);

if params.doGPU
   save([params.lonetrackdir '/ifo2'],'out2');
   clear out2, out1b;
end

if params.doGPU
   % Reload out1 and out2 into memory
   load([params.lonetrackdir '/ifo1'])
   load([params.lonetrackdir '/ifo2'])
end

max_cluster.lonetrack.out1 = out1;
max_cluster.lonetrack.out2 = out2;
max_cluster.lonetrack.snrmax1 = snrmax1;
max_cluster.lonetrack.snrmax2 = snrmax2;
max_cluster.lonetrack.timedelay1 = timedelay1;
max_cluster.lonetrack.timedelay2 = timedelay2;

if snrmax1 > snrmax2
  max_cluster.snr_gamma = snrmax1;
  max_cluster.cluster = out1;
  max_cluster.tmin=out1.tmin;
  max_cluster.tmax=out1.tmax;
  max_cluster.fmin=out1.fmin;
  max_cluster.fmax=out1.fmax;
  max_cluster.SNRfrac=SNRfrac1;
  max_cluster.SNRfracTime=out1.SNRfracTime;
  max_cluster.timedelay=timedelay1;
  max_cluster.nPix=out1.nPix;
  max_cluster.reconMax = out1.reconMax;
else
  max_cluster.snr_gamma = snrmax2;
  max_cluster.cluster = out2;
  max_cluster.tmin=out2.tmin;
  max_cluster.tmax=out2.tmax;
  max_cluster.fmin=out2.fmin;
  max_cluster.fmax=out2.fmax;
  max_cluster.SNRfrac=SNRfrac2;
  max_cluster.SNRfracTime=out2.SNRfracTime;
  max_cluster.timedelay=timedelay2;
  max_cluster.nPix=out2.nPix;
  max_cluster.reconMax = out2.reconMax;
end

if params.doGPU
   system(['rm -rf ' params.lonetrackdir '/ifo1.mat']);
   system(['rm -rf ' params.lonetrackdir '/ifo2.mat']);
end

return
