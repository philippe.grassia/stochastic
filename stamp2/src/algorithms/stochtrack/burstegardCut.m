function map_snr = burstegardCut(params, map)
% function map_snr = burstegardCut(params, map)

   params = burstegardDefaults(params);
   params.burstegard.NCN = 1;
   params.burstegard.pixelThreshold = 10;
   map2 = map;
   map2.y = abs(map2.y + 1i*map2.z);
   max_cluster = run_burstegard(map2, params);
   indexes = find(max_cluster.reconAll(:)>0);
   perc = length(indexes)/length(max_cluster.reconAll(:));
   map_snr(indexes) = 0;
   fprintf('vetoing %.2f%% of pixels...\n',perc*100);     

return
