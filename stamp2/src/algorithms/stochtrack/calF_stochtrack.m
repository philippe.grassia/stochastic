function [epsilon, tau] = calF(det1, det2, GPSTimes, source, params)
% function g = calF(det1, det2, GPSTimes, source, params)
% E. Thrane - moved from ccSpecReadout_stamp
%   calculates the antenna factors, e.g., F1p = F(det1, plus polarization)
% det1, det2: detector structs.
% GPSTimes: row vector of mid-segment GPS times.
% source: Nx2 matrix containing right ascension and declination for all N
%   sky locations to look at.
% g: struct containing antenna factors F
% similar to calF except this version is easier on memory for stochtrack

% make sure that ra and dec are in range
if any(source(:,1)<0 | source(:,1)>24)
  error('ra out of range');
end
%
if any(source(:,2)<-90 | source(:,2)>90)
  error('dec out of range');
end

% calculate sidereal time
time = GPStoGreenwichMeanSiderealTime(GPSTimes);

% replicate time, ra, and dec so we can calculate the filter
% for multiple combinations of GPS times and sky directions.
time = repmat(time,size(source,1),1);
ras = repmat(source(:,1),1,size(time,2));
decs = repmat(source(:,2),1,size(time,2));

% convert hours into radians: 24 hrs / 2pi rad
w = pi/12;
N = length(GPSTimes);

% calculate ingredients needed for antenna factors
psi = w*(time-ras);
theta = -pi/2+pi/180*decs;
ctheta = cos(theta);
stheta = sin(theta);
cpsi = cos(psi);
spsi = sin(psi);
Omega1 = -cpsi .* stheta;
Omega2 = spsi .* stheta;
Omega3 = ctheta;
pp11 = (ctheta.^2) .* (cpsi.^2) - (spsi.^2);
pp12 = -(ctheta.^2+1) .* cpsi .* spsi;
pp13 = ctheta .* cpsi .* stheta;
pp22 = (ctheta.^2) .* (spsi.^2) - (cpsi.^2);
pp23 = -ctheta .* spsi .* stheta;
pp33 = stheta.^2;
px11 = -2*ctheta .* cpsi .* spsi;
px12 = ctheta .* ((spsi.^2)-(cpsi.^2));
px13 = -stheta .* spsi;
px22 = 2*ctheta .* cpsi .* spsi;
px23 = -stheta .* cpsi;
%px33 = 0;

% define antenna factors
g.F1p = det1.d(1,1)*pp11+2*det1.d(1,2)*pp12 + 2*det1.d(1,3)*pp13+ ...
      det1.d(2,2)*pp22+2*det1.d(2,3)*pp23+det1.d(3,3)*pp33;
g.F2p = det2.d(1,1)*pp11+2*det2.d(1,2)*pp12 + 2*det2.d(1,3)*pp13+ ...
      det2.d(2,2)*pp22+2*det2.d(2,3)*pp23+det2.d(3,3)*pp33;
Fp = g.F1p .* g.F2p;
g.F1p = [];
g.F12 = [];

g.F1x = det1.d(1,1)*px11+2*det1.d(1,2)*px12 + 2*det1.d(1,3)*px13+ ...
      det1.d(2,2)*px22+2*det1.d(2,3)*px23;%+det1.d(3,3)*px33;
g.F2x = det2.d(1,1)*px11+2*det2.d(1,2)*px12 + 2*det2.d(1,3)*px13+ ...
      det2.d(2,2)*px22+2*det2.d(2,3)*px23;%+det2.d(3,3)*px33;
Fx  = g.F1x .* g.F2x;
g.F1x = [];
g.F2x = [];

% calculate epsilon in steps to avoid memory problems
epsilon = abs(Fp + Fx)'/2;

return;
