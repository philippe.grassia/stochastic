function [snrmax, snr0] = csnr(params, out1, out2, tau_s)
% function [snrmax, snr0] = csnr(params, out1, out2, tau_s)
% calculate coherent snr for intersection of two clusters

% tau_s is optional
try
  tau_s;
catch
  tau_s=NaN;
end

% temporary: tune the number of search directions
ndirs=400;

% determine maximum delay time between detectors
params.ifo1 = 'H1';
params.ifo2 = 'L1';
[det1 det2] = ifo2dets(params);
tau0 = norm(det1.r - det2.r)/3e8;

% create an array of delays to loop over
tau = [1:ndirs]*(2*tau0/(ndirs-1)) - (2*tau0/(ndirs-1)) - tau0;

% normalization factor for SNR calculation
pp = out1.pp;
segmentDuration = out1.params.segmentDuration;
newfac = 2*sqrt(2*out1.deltaF/segmentDuration)/pp.w1w2bar;

% determine cluster intersection
recon1=round(out1.reconMax);
recon2=round(out2.reconMax);
list1 = find(recon1==1);
list2 = find(recon2==1);
[sidx, x1, x2] = intersect(list1, list2);

% if there are no intersecting pixels, snrmax=0
if length(x1)==0
  snrmax=0;
  snr0=0;
else
  % calculate complex SNR for each shared pixel
  rho = newfac * conj(out1.fft1(x1)) .* out2.fft2(x2) ./ ...
    sqrt(out1.P1(x1).*out2.P2(x2));
  rho(isnan(rho)) = 0;
  pixelThreshold = 10;
  rho(abs(rho)>pixelThreshold) = 0;
  rhomap = repmat(rho', ndirs, 1);
  % calculate frequency for each shared pixel
  f = [out1.params.fmin : out1.deltaF : out1.params.fmax]';
  fmap = repmat(f, 1, size(recon1,2));
  fvals = fmap(list1(x1));
  % should be the same as and it is
  % fmap(list2(x2));
  tauarray = repmat(tau', 1, length(fvals));
  farray = repmat(fvals', ndirs, 1);
  % calculate phasor
  phi = exp(-i*2*pi*farray.*tauarray);
  % calculate total SNR
  snr = sum(real(phi.*rhomap), 2);
  % use standard sqrt(npix) normalization
  snr = snr / sqrt(length(x1));
  % hack: epsilon factor
  snr= -snr;
  % save the direction with the loudest SNR
  [snrmax, maxidx] = max(snr);
  % known search direction
  phi = exp(-i*2*pi*fvals'*tau_s);
  snr0 = sum(real(phi.*rho'));
  snr0 = snr0 / sqrt(length(x1));
  % hack: epsilon factor
  snr0 = -snr0;
end

return
