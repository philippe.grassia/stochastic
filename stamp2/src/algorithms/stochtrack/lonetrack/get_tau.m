function [tau] = get_tau(ii)
% function [tau] = get_tau(ii)

%------------------------------------------------------------------------------
% output filename string
str = 'adi-d';
% injection distances for stochtrack (best)
D = 35200*1.1.^[0 1 2 3 4 5];
%------------------------------------------------------------------------------

% select distance based on job
f=@(x) mod(x-1, length(D)) + 1;

% read in mc jobfile
q = load('../weeklong/mcjobs.txt');
njobs = 240;

% calculate GPS time for job middle
gps = (q(:,2) + q(:,3)) / 2;

% buffer for injection time
buff = 15;

% calculate optimal search directions
[ra_max dec_max epsilon_max g] = bestdir(gps(ii));

% get delay time
tau = g.tau;

return
