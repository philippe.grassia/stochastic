function [params, det1, det2] = fast_params(map)
% function [params, det1, det2] = fast_params(map)

% needed for calF
params.ifo1 = 'H1';
params.ifo2 = 'L1';
[det1 det2] = ifo2dets(params);
params.c = 3e8;

% start with default parameters
params = stampDefaults(params);

% needed for stochsky
params.doStochtrack = true;
% OBSOLETE?
params.stochtrack.norm = 'npix';
params.stochtrack.T = 10000;
params.stochtrack.F = 1000;
params.stochtrack.mindur = 80;
params.stochtrack.stochsky = false;
params.stochtrack.doSeed = 0;
%params.doGPU = true;
%params.doParallel = true;
params.stochtrack.demo = false;
% direction needed?
params.ra = 0;
params.dec = 0;
% random seed set with GPS time
params.seed = -1;
params.jobNumber=1;
% save plots
params.savePlots = false;
% frequency range
try
  params.fmin = map.f(1);
  params.fmax = map.f(end);
  params.segmentDuration = map.segDur;
  params.startGPS = map.segstarttime(1);
catch
  fprintf('map struct unavailable\n');
end

% turn off vlong mode
params.stochtrack.vlong = false;

params.stochtrack.doMaxbandPercentage = true;
params.stochtrack.maxbandPercentage = 0.5;

return
