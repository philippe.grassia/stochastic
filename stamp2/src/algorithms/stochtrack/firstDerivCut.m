function map = firstDerivCut(map,outpath);



fprintf('%%%% START firstDerivCut\n');

thresh=1e-46;% min/max allowable slope: You must tailor the threshold value to your analysis.


if ~exist([outpath 'P1']); system(['mkdir -p ' outpath '/P1']); end
if ~exist([outpath 'P2']); system(['mkdir -p ' outpath '/P2']); end
if ~exist([outpath 'snr']); system(['mkdir -p ' outpath '/snr']); end

naiP=map.naiP1;
for p=1:2
  sigmaHold = nanmean(naiP,2);

%% Determine freqs not passing 1st deriv cut
  ndx(1)=1;
  for nn=2:length(sigmaHold)
    if abs(sigmaHold(nn)-sigmaHold(nn-1))>thresh % 1stDeriv too big: NaN to omit
      ndx(nn)=0/0;
    else % 1stDeriv small enough so 1 to keep
      ndx(nn)=1;
    end
  end
  mask(:,p)=transpose(ndx);

%% Make psd_new plot (red and blue)
  figure;
  loglog(map.f,sigmaHold,'r','LineWidth',2);
  hold on;
  grid on;
  loglog(map.f,sigmaHold.*mask(:,p),'b','LineWidth',2);
  title(['P' num2str(p) ' PSD Plot']);
  ylabel('PSD');
  ylim([min(sigmaHold(~isnan(sigmaHold))) max(sigmaHold(~isnan(sigmaHold)))]);
  xlabel('Freq (Hz)');
  xlim([min(map.f) max(map.f)]);
  pretty;
  print('-dpng',[outpath '/P' num2str(p) '/psd_new']);
  close;

  clear fid sigmaHold ndx nn;
  naiP=map.naiP2;
end



mask(:,3)=mask(:,1).*mask(:,2);

for nn=1:size(map.cc,1)
  map.cc(:,nn)=map.cc(:,nn).*mask(:,3);
%%  map.ccVar(:,nn)=map.ccVar(:,nn).*mask(:,3); % Time domain
  map.fft1(:,nn)=map.fft1(:,nn).*mask(:,1);
  map.fft2(:,nn)=map.fft2(:,nn).*mask(:,2);
%%  map.Q(:,nn)=map.Q(:,nn).*mask(:,3); % Time domain
  map.P1(:,nn)=map.P1(:,nn).*mask(:,1);
  map.P2(:,nn)=map.P2(:,nn).*mask(:,2);
  map.naiP1(:,nn)=map.naiP1(:,nn).*mask(:,1);
  map.naiP2(:,nn)=map.naiP2(:,nn).*mask(:,2);
  map.y(:,nn)=map.y(:,nn).*mask(:,3);
  map.z(:,nn)=map.z(:,nn).*mask(:,3);
  map.sigma(:,nn)=map.sigma(:,nn).*mask(:,3);
%%%% the rest of these %% are unused by lonetrack.m
%%  map.eps_12(:,nn)=map.eps_12(:,nn).*mask(:,3);
%%  map.eps_11(:,nn)=map.eps_11(:,nn).*mask(:,3);
%%  map.eps_22(:,nn)=map.eps_22(:,nn).*mask(:,3);
  map.snr(:,nn)=map.snr(:,nn).*mask(:,3);
%%  map.snrz(:,nn)=map.snrz(:,nn).*mask(:,3);
%%  map.y_f(:,nn)=map.y_f(:,nn).*mask(:,3);
%%  map.Xi(:,nn)=map.Xi(:,nn).*mask(:,3);
%%  map.sigma_Xi(:,nn)=map.sigma_Xi(:,nn).*mask(:,3);
%%  map.Xi_snr(:,nn)=map.Xi_snr(:,nn).*mask(:,3);
end

fprintf('%%%% DONE firstDerivCut\n');


return

