function [snrmax, snr0, SNRfrac, timedelay] = csnr_v3(params, out1, out2, N)
% function [snrmax, snr0] = csnr_v3(params, out1, out2, N)
% calculate coherent snr for intersection of two clusters

try
   tau_s;
catch
   tau_s=NaN;
end

% temporary: tune the number of search directions
ndirs=400;

% determine maximum delay time between detectors
params.ifo1 = 'H1';
params.ifo2 = 'L1';
[det1 det2] = ifo2dets(params);
tau0 = norm(det1.r - det2.r)/3e8;

% create an array of delays to loop over
tau = [1:ndirs]*(2*tau0/(ndirs-1)) - (2*tau0/(ndirs-1)) - tau0;

if params.fixedSkyPosition
   GPSTime = (params.startGPS+params.endGPS)/2;
   source = [params.ra params.dec];
   tau = caltau(det1, det2, GPSTime, source, params)*ones(1,ndirs);
end

% normalization factor for SNR calculation
pp = out1.pp;
segmentDuration = out1.params.segmentDuration;
newfac = 2*sqrt(2*out1.deltaF/segmentDuration)/pp.w1w2bar;

% calculate complex SNR for each shared pixel
rho = newfac * conj(out1.fft1(:)) .* out2.fft2(:) ./ ...
  sqrt(out1.P1(:).*out2.P2(:));

if params.stochtrack.doPixelCut
   indexes = find(abs(rho) > params.stochtrack.pixel_threshold);
   rho(indexes) = NaN;
end

if length(find(~isnan(rho))) < params.stochtrack.mindur
   fprintf('This track does not exceed mindur = %d (number of pixels: %d)... setting to NaN.', ...
      params.stochtrack.mindur,length(find(~isnan(rho))));
   rho(:) = NaN;
end

rho(isnan(rho)) = 0;
rhomap = repmat(rho', ndirs, 1);
tauarray = repmat(tau', 1, length(out1.fvals));
farray = repmat(out1.fvals', ndirs, 1);
% calculate phasor
phi = exp(-i*2*pi*farray.*tauarray);
% calculate total SNR
snr = sum(real(phi.*rhomap), 2);
% use standard sqrt(npix) normalization
snr = snr / sqrt(length(out1.fft1));
% hack: epsilon factor
%snr= -snr;
% 10/19/15: abs(snr) to catch (some) linearly polarized signals which can make SNR 
% negative due to the unpolarized filter we use
snr = abs(snr);
% save the direction with the loudest SNR
[snrmax, maxidx] = max(snr);
% find loudest imaginary SNR if flag set
imaginaryLouder = false;
if ~isfield(params, 'no_imaginary_polarized_filter') || ~params.no_imaginary_polarized_filter
  snr2 = sum(imag(phi.*rhomap), 2);
  snr2 = snr2 / sqrt(length(out1.fft1));
  snr2 = abs(snr2);
  [snrmax2, maxidx2] = max(snr2);
  if snrmax2 > snrmax
    snrmax = snrmax2;
    maxidx = maxidx2;
    imaginaryLouder = true;
  end
end
% save the direction with the loudest SNR
if imaginaryLouder
  SNRfrac1=(max(-(imag(phi(maxidx,:).*rhomap(maxidx,:))))/sqrt(length(out1.fft1)))/snrmax;
  SNRfrac2=(max((imag(phi(maxidx,:).*rhomap(maxidx,:))))/sqrt(length(out1.fft1)))/snrmax;
else
  SNRfrac1=(max(-(real(phi(maxidx,:).*rhomap(maxidx,:))))/sqrt(length(out1.fft1)))/snrmax;
  SNRfrac2=(max((real(phi(maxidx,:).*rhomap(maxidx,:))))/sqrt(length(out1.fft1)))/snrmax;
end
SNRfrac = max([SNRfrac1 SNRfrac2]);
timedelay = tau(maxidx); 

%SNRfrac1=std(-(real(phi(maxidx,:).*rhomap(maxidx,:))));
%SNRfrac2=std((real(phi(maxidx,:).*rhomap(maxidx,:))));
%SNRfrac = max([SNRfrac1 SNRfrac2]);

% known search direction
phi = exp(-i*2*pi*out1.fvals'*tau_s);
snr0 = sum(real(phi.*rho'));
if ~isfield(params, 'no_imaginary_polarized_filter') || ~params.no_imaginary_polarized_filter
  snr0_2 = sum(imag(phi.*rho'));
  if snr0_2 > snr0
    snr0 = snr0_2;
  end
end
snr0 = snr0 / sqrt(length(out1.fft1));
% hack: epsilon factor
% snr0 = -snr0;
% 10/19/15: abs(snr) to catch (some) linearly polarized signals which can make SNR 
% negative due to the unpolarized filter we use
snr0 = abs(snr0);

return
