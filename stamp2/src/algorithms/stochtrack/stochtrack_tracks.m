function [yvals,xvals,H,ttvals,track_data] = stochtrack_tracks(map, params, aa);
% function max_cluster = run_stochtrack(map, params)
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.nPix -> number of pixels in the largest cluster
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.y_gamma -> Y (GW estimator) of largest cluster
%        max_cluster.sigma_gamma -> sigma of the above estimator
%        max_cluster.reconMax -> 2D array containing weight numbers for
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
% Written by E Thrane and M Coughlin
% Contact: ethrane@ligo.caltech.edu, michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% map is MxN: M is the # of frequency bins and N is the number of segments
[M,N] = size(map.snr);

% define maxdur to be N (the map time dimension) unless otherwise specified
try
   params.stochtrack.maxdur;
catch
   params.stochtrack.maxdur = N;
end

% define maxband to be M (the same as the frequency dimension unless otherwise
% specified
try
  params.stochtrack.maxband;
catch
  params.stochtrack.maxband = M;
end

% stochtrack parameters-------------------------------------------------------
% number of trials
T = params.stochtrack.T;
% number of trials in for loop
F = params.stochtrack.F;
% minimum track duration (IN PIXELS)
mindur = params.stochtrack.mindur;
% maximum track duration (IN PIXELS)
maxdur = params.stochtrack.maxdur;
%-----------------------------------------------------------------------------

% initialize yvals array
yvals = gZeros(N,T,params);
xvals = gColon(1,1,N,params);
ttvals = gArray(map.segstarttime,params); 
H = gOnes(N,T,params);
d_foft = gOnes(N,T,params);
track_data = [];

if params.stochtrack.doCBC

  LAL_MTSUN_SI = 4.9254923218988636432342917247829673e-6; %/**< Geometrized solar mass, s. = LAL_MSUN_SI / LAL_MPL_SI * LAL_TPL_SI */

  % Generate masses and associated variables
  %m1 = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %   + params.stochtrack.cbc.min_mass;
  %m2 = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %   + params.stochtrack.cbc.min_mass;

  %ms = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %    + params.stochtrack.cbc.min_mass;
  ms = gLinspace(params.stochtrack.cbc.min_mass,params.stochtrack.cbc.max_mass,F,params)'; 

  % choose mass based on loop value and set m1 and m2 equal to one another
  thism = ms(aa);
  m1 = thism * gOnes(T,1,params); 
  m2 = m1;

  track_data.m1 = m1;
  track_data.m2 = m2;

  % Calculate mass ratio, chirp mass, and total mass
  eta = (m1.*m2)./((m1+m2).^2);
  % Convert mc from solar masses to seconds
  mc = (m1 + m2) .* (eta.^(3/5)) * LAL_MTSUN_SI;
  Mtot = (m1 + m2) .* LAL_MTSUN_SI;

  % Set these variables to map size
  t = repmat(gColon(1,1,N,params)' * (params.segmentDuration/2), 1, size(yvals,2));
  eta = repmat(eta, 1, size(yvals,1))';
  mc = repmat(mc, 1, size(yvals,1))';
  tc = repmat((params.segmentDuration/2) * N*gOnes(T,1,params), 1, size(yvals,1))';
  Mtot = repmat(Mtot, 1, size(yvals,1))';

  % Make end of track slightly before coalescence time (so that f(t) does not go to infinity there)
  t(end,:) = N* (params.segmentDuration/2) - (params.segmentDuration/4);

  % Compute reduced time variable
  Tp = t .* (eta.^(8/5)) ./ (5*mc);
  Tc = tc .* (eta.^(8/5)) ./ (5*mc);
  TcminusT = Tc-Tp;
  TcminusT(TcminusT < 0) = 0;

  % Compute h vs. time
  hoft = (TcminusT.^(-1/4)).^2;
  hoft(isinf(hoft)) = 0;
  hoft(end-10:end,:) = hoft(end-10,1);
  sumhoft = sum(hoft);
  sumhoft = repmat(sumhoft', 1, size(yvals,1))';
  H = hoft ./ sumhoft;

  % Compute frequency vs. time
  %foft = (eta.^(3/5) ./ (8*pi*mc)) .* (TcminusT).^(-3/8) .* (1 + ...
  %   (((743/2688) + (11/32).*eta).*(TcminusT).^(-1/4)) - ...
  %   ((3*pi/10)*(TcminusT).^(-3/8)) + ((1855099/14450688) + ...
  %   (56975/258048).*eta + (371/2048).*(eta.^2)) .* (TcminusT).^(-1/2));

  % Equation A1 from http://arxiv.org/abs/0803.0226 (3.5 PN order, no spin terms)

  eg=0.5772156649015328606;
  tauoft = (eta./(5*Mtot)) .* (tc-t);
  tau0 = (eta./(5*Mtot)) .* (tc);

  p0 = 1; p1 = 0;
  p2 = (743/2688) + (11/32)*eta; p3 = -(3/10) * pi;
  p4 = (1855099/14450688) + (56975/258048)*eta + (371/2048)*eta.^2;
  p5 = (-(7729/21504) + (3/256)*eta)*pi;
  p6 = -(720817631400877/288412611379200) + (107/280)*eg + (53/200)*pi^2 - (107/2240)*log(tauoft/256) + ...
    ((25302017977/4161798144) - (451/2048)*pi^2)*eta - (30913/1835008)*eta.^2 + (235925/1769472)*eta.^3;
  p7 = (-(188516689/433520640) - (28099/57344)*eta + (122659/1290240)*eta.^2)*pi;

  phioft = -(1./(4*Mtot)) .* ((p0.*tauoft.^(-(3+0)/8)) + (p1.*tauoft.^(-(3+1)/8)) + (p2.*tauoft.^(-(3+2)/8)) + ...
    (p3.*tauoft.^(-(3+3)/8)) + (p4.*tauoft.^(-(3+4)/8)) + (p5.*tauoft.^(-(3+5)/8)) + ...
    (p6.*tauoft.^(-(3+6)/8)) + (p7.*tauoft.^(-(3+7)/8)));

  foft = phioft ./ (-2*pi);

  d_foft(2:end,:) = foft(2:end,:) - foft(1:end-1,:);
  d_foft(d_foft<1) = 1;
  d_foft = round(d_foft);

  response = interp1(map.f,map.filter_response,foft(:,1));
  response(isnan(response)) = 1;
  response = repmat(response, 1, size(yvals,2));
  H = H.*response;
  sumhoft = sum(H);
  sumhoft = repmat(sumhoft', 1, size(H,1))';
  H = H ./ sumhoft;

  % Convert frequency to map indices
  foft = foft - params.fmin + 1;

  % Do DF calculation (adds pixels to end of track associated with extra pixels track passes
  % through in a given segment
  if params.stochtrack.doDF
     this_d_foft = d_foft(:,1);
     % Choose columns that have df>1 (as they need pixels added)
     xvals_extra = []; yvals_extra = []; H_extra = [];  tt_extra = [];
     indexes = find(this_d_foft > 1);
     
     % Add extra pixels for each column that needs it
     for kk = 1:length(indexes)
        bb = indexes(kk);
        cc = gColon(2,1,this_d_foft(bb),params);
        ii = gOnes(1,this_d_foft(bb)-1,params);

        % Add extra pixels to the end
        xvals_extra = [xvals_extra xvals(bb)*ii];
        yvals_extra = [yvals_extra foft(bb,1)+cc-1];
        H_extra = [H_extra H(bb,1)*ii];
        tt_extra = [tt_extra ttvals(bb)*ii];
     end

     % Add arrays to the end of original
     xvals_extra = xvals_extra;
     yvals_extra = repmat(yvals_extra', 1, size(yvals,2))';
     H_extra = repmat(H_extra', 1, size(yvals,2))';

     xvals = [xvals xvals_extra];
     foft = [foft; yvals_extra'];
     H = [H; H_extra'];
     ttvals = [ttvals tt_extra];

     % Sort by time
     [xvals,index] = sort(xvals,'ascend');
     foft = foft(index,:);
     H = H(index,:);
     ttvals = ttvals(index);

     % Set expanded arrays as ones used
     foft(foft>M) = NaN;
  end

  yvals = foft;

  % Choose coalescense times: the smallest possible stop time is given by 
  % minstop.  The largest possible stop time is N = the last pixel in the map.
  if T < size(yvals,1)-params.stochtrack.cbc.minstop
     nstops = (N-params.stochtrack.cbc.minstop).*gRand(T,1,params) + ...
       params.stochtrack.cbc.minstop;
     nstops = round(nstops);
  else
     nstops = gColon(params.stochtrack.cbc.minstop,1,N,params);
     nstops = padarray(nstops',T-length(nstops),N,'post');
  end
  track_data.nstops = nstops;

  nstops = repmat(nstops, 1, size(yvals,1))';

  % Move waveform to correct starting index
  xvals = repmat(xvals', 1, size(yvals,2));
  xvals = mod(xvals + nstops - 1,N) + 1;

  % remove values below 0
  H(yvals<0) = 0;
  yvals(yvals<0) = NaN;

  % Remove any portion of the waveform after the maximum
  xvals_max_index = xvals(end,:);
  xvals_max_index = repmat(xvals_max_index', 1, size(yvals,1))';
  H(xvals > xvals_max_index) = 0;
  yvals(xvals > xvals_max_index) = NaN;

elseif params.stochtrack.doECBC

  LAL_MTSUN_SI = 4.9254923218988636432342917247829673e-6; %/**< Geometrized solar mass, s. = LAL_MSUN_SI / LAL_MPL_SI * LAL_TPL_SI */

  % Generate masses and associated variables
  %m1 = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %   + params.stochtrack.cbc.min_mass;
  %m2 = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %   + params.stochtrack.cbc.min_mass;

  %ms = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %    + params.stochtrack.cbc.min_mass;
  ms = gLinspace(params.stochtrack.cbc.min_mass,params.stochtrack.cbc.max_mass,F,params)';

  % choose mass based on loop value and set m1 and m2 equal to one another
  thism = ms(aa);
  m1 = thism * gOnes(T,1,params);
  m2 = m1;

  % Calculate mass ratio, chirp mass, and total mass
  eta = (m1.*m2)./((m1+m2).^2);
  % Convert mc from solar masses to seconds
  mc = (m1 + m2) .* (eta.^(3/5)) * LAL_MTSUN_SI;
  Mtot = (m1 + m2) .* LAL_MTSUN_SI;

  % Set these variables to map size
  t = repmat(gColon(1,1,N,params)' * (params.segmentDuration/2), 1, size(yvals,2));
  eta = repmat(eta, 1, size(yvals,1))';
  mc = repmat(mc, 1, size(yvals,1))';
  tc = repmat((params.segmentDuration/2) * N*gOnes(T,1,params), 1, size(yvals,1))';
  Mtot = repmat(Mtot, 1, size(yvals,1))';

  % Make end of track slightly before coalescence time (so that f(t) does not go to infinity there)
  t(end,:) = N* (params.segmentDuration/2) - (params.segmentDuration/4);

  % Compute reduced time variable
  Tp = t .* (eta.^(8/5)) ./ (5*mc);
  Tc = tc .* (eta.^(8/5)) ./ (5*mc);
  TcminusT = Tc-Tp;
  TcminusT(TcminusT < 0) = 0;

  % Compute frequency vs. time
  f0 = (2*pi*(params.fmin/2)*Mtot).^(2/3);

  eccentricities = linspace(params.stochtrack.cbc.min_eccentricity,...
    params.stochtrack.cbc.max_eccentricity,params.stochtrack.cbc.n_eccentricity);

  xvalsAll = gZeros(params.stochtrack.max_pixels,length(eccentricities)*T,params);
  yvalsAll = gZeros(params.stochtrack.max_pixels,length(eccentricities)*T,params);
  HAll = gZeros(params.stochtrack.max_pixels,length(eccentricities)*T,params);
  nstopsAll = gZeros(1,length(eccentricities)*T,params);
  eccAll = gZeros(1,length(eccentricities)*T,params);

  track_data.m1 = thism*gOnes(1,length(eccentricities)*T,params);
  track_data.m2 = thism*gOnes(1,length(eccentricities)*T,params);



  for ee = 1:length(eccentricities)

    ecc = eccentricities(ee);
    if ecc == 0
       ecc = 1e-5;
    end

    xvals = gColon(1,1,N,params);

    tau = 9.829*3.15569e7*1e6*(1/((params.fmin/2)*3600)).^(8/3) *(1./(m1+m2)).^(2/3) .* (1./((m1.*m2)./(m1+m2))).*(1-ecc).^(7/2);
    C0 = f0*((1 - ecc^2)/((ecc^(12/19))*(304 + 121*ecc).^(870/2299)))^(-1);
    A=(5*(31046))./(172 * 2^(2173/2299) * 19^(1118/2299) * eta .* C0.^4);
    B = ((ecc).^(48/19)).*A.*Mtot;
    BminusT = t(end) - t + (params.segmentDuration/4);
    eccoft = ((BminusT)./(A.*Mtot)).^(19/48);
    %eccoft(end,:) = 1e-10;
    X = C0.*((1 - eccoft.^2)./((eccoft.^(12/19)).*(304 + 121.*eccoft.^2).^(870/2299)));
    X(X<0) = 0;
    foft = (X.^(3/2))./(2*pi.*Mtot); 

    % Orbital frequency is 1/2 GW frequency
    foft = 2 * foft;

    d_foft(2:end,:) = foft(2:end,:) - foft(1:end-1,:);
    d_foft(d_foft<1) = 1;
    d_foft = round(d_foft);

    % Compute h vs. time
    hoft = (TcminusT.^(-1/4)).^2;
    hoft(isinf(hoft)) = 0;
    hoft(end-10:end,:) = hoft(end-10,1);
    sumhoft = sum(hoft);
    sumhoft = repmat(sumhoft', 1, N)';
    H = hoft ./ sumhoft;

    response = interp1(map.f,map.filter_response,foft(:,1));
    response(isnan(response)) = 1;
    response = repmat(response, 1, size(yvals,2));
    H = H.*response;
    sumhoft = sum(H);
    sumhoft = repmat(sumhoft', 1, size(H,1))';
    H = H ./ sumhoft;

    % Convert frequency to map indices
    foft = foft - params.fmin + 1;

    % Do DF calculation (adds pixels to end of track associated with extra pixels track passes
    % through in a given segment
    if params.stochtrack.doDF
       this_d_foft = d_foft(:,1);
       % Choose columns that have df>1 (as they need pixels added)
       xvals_extra = []; yvals_extra = []; H_extra = [];  tt_extra = [];
       indexes = find(this_d_foft > 1);

       % Add extra pixels for each column that needs it
       for kk = 1:length(indexes)
          bb = indexes(kk);
          cc = gColon(2,1,this_d_foft(bb),params);
          ii = gOnes(1,this_d_foft(bb)-1,params);

          % Add extra pixels to the end
          xvals_extra = [xvals_extra xvals(bb)*ii];
          yvals_extra = [yvals_extra foft(bb,1)+cc-1];
          H_extra = [H_extra H(bb,1)*ii];
          tt_extra = [tt_extra ttvals(bb)*ii];
       end

       % Add arrays to the end of original
       xvals_extra = xvals_extra;
       yvals_extra = repmat(yvals_extra', 1, size(yvals,2))';
       H_extra = repmat(H_extra', 1, size(yvals,2))';

       xvals = [xvals xvals_extra];
       foft = [foft; yvals_extra'];
       H = [H; H_extra'];
       ttvals = [ttvals tt_extra];

       % Sort by time
       [xvals,index] = sort(xvals,'ascend');
       foft = foft(index,:);
       H = H(index,:);
       ttvals = ttvals(index);

       % Set expanded arrays as ones used
       foft(foft>M) = NaN;
    end

    yvals = foft;

    % Choose coalescense times: the smallest possible stop time is given by
    % minstop.  The largest possible stop time is N = the last pixel in the map.
    if T < size(yvals,1)-params.stochtrack.cbc.minstop
       nstops = (N-params.stochtrack.cbc.minstop).*gRand(T,1,params) + ...
         params.stochtrack.cbc.minstop;
       nstops = round(nstops);
    else
       nstops = gColon(params.stochtrack.cbc.minstop,1,N,params);
       nstops = padarray(nstops',T-length(nstops),N,'post');
    end
    nstopsHold = nstops;

    nstops = repmat(nstops, 1, size(yvals,1))';

    % Move waveform to correct starting index
    xvals = repmat(xvals', 1, size(yvals,2));
    xvals = mod(xvals + nstops - 1,N) + 1;

    % remove values below 0
    H(yvals<0) = 0;
    yvals(yvals<0) = NaN;

    % Remove any portion of the waveform after the maximum
    xvals_max_index = xvals(end,:);
    xvals_max_index = repmat(xvals_max_index', 1, size(yvals,1))';
    H(xvals > xvals_max_index) = 0;
    yvals(xvals > xvals_max_index) = NaN;


    indexes = (ee-1)*T+1:ee*T;
    indexes_2 = params.stochtrack.max_pixels-size(yvals,1)+1:params.stochtrack.max_pixels;

%%%% next line is for keeping track of lower bounds of indexes_2
    fprintf('indexes_2 =%4d,%5d\n',indexes_2(1),indexes_2(end));

    xvalsAll(indexes_2,indexes) = xvals; yvalsAll(indexes_2,indexes) = yvals;
    HAll(indexes_2,indexes) = H;
    nstopsAll(indexes) = nstopsHold;
    eccAll(indexes) = ecc;
  end

  xvals = xvalsAll; yvals = yvalsAll; H = HAll;

  track_data.nstops = nstopsAll;
  track_data.eccentricities = eccAll;

elseif params.stochtrack.doRModes

  % Generate masses and associated variables
  %f0 = (params.stochtrack.rmodes.max_f0-params.stochtrack.rmodes.min_f0).*gRand(T,1,params) ...
  %   + params.stochtrack.rmodes.min_f0;
  f0s = gColon(params.stochtrack.rmodes.min_f0,1,params.stochtrack.rmodes.max_f0,params)';

  if F > length(f0s)
     f0s = padarray(f0s,F-length(f0s),params.stochtrack.rmodes.max_f0,'post');
  end

  % choose mass based on loop value and set m1 and m2 equal to one another
  min_alpha = log10(params.stochtrack.rmodes.min_alpha);
  max_alpha = log10(params.stochtrack.rmodes.max_alpha);

  alphas = gLinspace(min_alpha,max_alpha,params.stochtrack.rmodes.nalpha,params)';
  alphas = 10.^alphas;

  thisf0 = f0s(aa);
  f0 = repmat(thisf0, 1, size(yvals,1))';

  xvalsHold = xvals;

  xvalsAll = gZeros(N,length(alphas)*T,params); yvalsAll = gZeros(N,length(alphas)*T,params);
  HAll = gZeros(N,length(alphas)*T,params); alphasAll = gOnes(1,length(alphas)*T,params);

  for bb = 1:length(alphas)
    alpha = alphas(bb);

    alpha = repmat(alpha, 1, size(yvals,1))';
    t = gColon(1,1,N,params)' * (params.segmentDuration/2);

    k=-1.8*alpha.^2*10^-21 ;
    fOwen= 1./(f0.^(-6) - 6.*k.*t).^(1/6);
    I=-(1./(5*k)).*(f0.^(-6)-6*k.*t).^(5/6);        % This is the definite integral of f(t) calculated at t
    int=I+(1./(5*k)).*(f0.^(-6)).^(5/6);           % This is the integral at t minus the integral at 0
    foft = fOwen;

    % Compute h vs. time
    hoft = fOwen.^6;
    hoft(isinf(hoft)) = 0;
    sumhoft = sum(hoft);
    sumhoft = repmat(sumhoft', 1, size(yvals,1))';
    H = hoft ./ sumhoft;
    H = repmat(H, 1, size(yvals,1));

    % Convert frequency to map indices
    foft = foft - params.fmin + 1;
    yvals = foft;
    yvals = repmat(yvals, 1, size(yvals,1));

    % Choose coalescense times: the smallest possible stop time is given by
    % minstop.  The largest possible stop time is N = the last pixel in the map.
    if T < size(yvals,1)
     nstops = N.*gRand(T,1,params);
     nstops = round(nstops);
    else
     nstops = gColon(1,1,N,params);
     nstops = padarray(nstops',T-length(nstops),N,'post');
    end
    track_data.nstops = nstops;

    nstops = repmat(nstops, 1, size(yvals,1))';

    % Move waveform to correct starting index
    xvals = repmat(xvalsHold', 1, size(yvals,1));
    xvals = mod(xvals + nstops - 1,N) + 1;

    % remove values below 0
    H(yvals<0) = 0;
    yvals(yvals<0) = NaN;

    % Remove any portion of the waveform after the maximum
    xvals_max_index = xvals(end,:);
    xvals_max_index = repmat(xvals_max_index', 1, size(yvals,1))';
    H(xvals > xvals_max_index) = 0;
    yvals(xvals > xvals_max_index) = NaN;

    indexes = (bb-1)*T+1:bb*T;
    xvalsAll(:,indexes) = xvals; yvalsAll(:,indexes) = yvals; 
    HAll(:,indexes) = H; alphasAll(indexes) = alphas(bb);

  end

  xvals = xvalsAll; yvals = yvalsAll; H = HAll; alpha = alphasAll;

  track_data.f0 = repmat(thisf0,1,length(alphas)*T);
  track_data.alpha = alpha;

elseif params.stochtrack.doExponential

  % Generate masses and associated variables
  %f0 = (params.stochtrack.rmodes.max_f0-params.stochtrack.rmodes.min_f0).*gRand(T,1,params) ...
  %   + params.stochtrack.rmodes.min_f0;
  f0s = gColon(params.stochtrack.exponential.min_f0,1,params.stochtrack.exponential.max_f0,params)';

  if F > length(f0s)
     f0s = padarray(f0s,F-length(f0s),params.stochtrack.exponential.max_f0,'post');
  end

  % choose mass based on loop value and set m1 and m2 equal to one another
  min_alpha = params.stochtrack.exponential.min_alpha;
  max_alpha = params.stochtrack.exponential.max_alpha;

  alphas = gLinspace(min_alpha,max_alpha,params.stochtrack.exponential.nalpha,params)';
  thisf0 = f0s(aa);
  f0 = repmat(thisf0, 1, size(yvals,1))';

  xvalsHold = xvals;

  xvalsAll = gZeros(N,length(alphas)*T,params); yvalsAll = gZeros(N,length(alphas)*T,params);
  HAll = gZeros(N,length(alphas)*T,params); alphasAll = gOnes(1,length(alphas)*T,params);

  for bb = 1:length(alphas)
    alpha = alphas(bb);

    alpha = repmat(alpha, 1, size(yvals,1))';
    t = gColon(1,1,N,params)' * (params.segmentDuration/2);
    foft = f0 .* t.^alpha;

    % Compute h vs. time
    hoft = ones(size(foft));
    hoft(isinf(hoft)) = 0;
    sumhoft = sum(hoft);
    sumhoft = repmat(sumhoft', 1, size(yvals,1))';
    H = hoft ./ sumhoft;
    H = repmat(H, 1, size(yvals,1));

    % Convert frequency to map indices
    foft = foft - params.fmin + 1;
    yvals = foft;
    yvals = repmat(yvals, 1, size(yvals,1));

    % Choose coalescense times: the smallest possible stop time is given by
    % minstop.  The largest possible stop time is N = the last pixel in the map.
    if T < size(yvals,1)
     nstops = N.*gRand(T,1,params);
     nstops = round(nstops);
    else
     nstops = gColon(1,1,N,params);
     nstops = padarray(nstops',T-length(nstops),N,'post');
    end
    track_data.nstops = nstops;

    nstops = repmat(nstops, 1, size(yvals,1))';

    % Move waveform to correct starting index
    xvals = repmat(xvalsHold', 1, size(yvals,1));
    xvals = mod(xvals + nstops - 1,N) + 1;

    % remove values below 0
    H(yvals<0) = 0;
    yvals(yvals<0) = NaN;

    % Remove any portion of the waveform after the maximum
    xvals_max_index = xvals(end,:);
    xvals_max_index = repmat(xvals_max_index', 1, size(yvals,1))';
    H(xvals < xvals_max_index) = 0;
    yvals(xvals < xvals_max_index) = NaN;

    indexes = (bb-1)*T+1:bb*T;
    xvalsAll(:,indexes) = xvals; yvalsAll(:,indexes) = yvals;
    HAll(:,indexes) = H; alphasAll(indexes) = alphas(bb);

  end

  xvals = xvalsAll; yvals = yvalsAll; H = HAll; alpha = alphasAll;

  track_data.f0 = repmat(thisf0,1,length(alphas)*T);
  track_data.alpha = alpha;

elseif params.stochtrack.doAntiChirp
  % clusters with exponentially decaying frequency, as suggested
  % by van Putten (2018), which claimed detection of anti chirp after
  % GW170817 (not found by any LVC analysis, including this one)
  % added 2018-06-29 by Paul Schale (paul.schale@ligo.org)

  % Required parameters:
  % params.stochtrack.doAntiChirp set to true
  % struct params.stochtrack.antiChirp with fields:
  % min_alpha, max_alpha, min_fmax, max_fmax, min_f0, max_f0, 
  %    these parameters define shape of exponential curve:
  %    f(t) = (fmax - fmin) * exp(- alpha * t) + fmin
  %    (parameters are sampled from uniform distibution within given bounds)
  %
  %t1, t2
  %    Start time of antichirp selected from uniform distribution
  %    with bounds t1 and t2



  % grab the parameters so we don't have to type all of this again
  min_alpha = params.stochtrack.antiChirp.min_alpha;
  max_alpha = params.stochtrack.antiChirp.max_alpha;
  min_fmax = params.stochtrack.antiChirp.min_fmax;
  max_fmax = params.stochtrack.antiChirp.max_fmax;
  min_f0 = params.stochtrack.antiChirp.min_f0;
  max_f0 = params.stochtrack.antiChirp.max_f0;
  t1 = params.stochtrack.antiChirp.t1;
  t2 = params.stochtrack.antiChirp.t2;

  min_pixels = params.stochtrack.mindur*2/params.segmentDuration;

  % generate T random values for each parameter, uniformly within set bounds
  alphas = (max_alpha - min_alpha) * gRand(1,T,params) + min_alpha;
  fmaxes = (max_fmax - min_fmax) * gRand(1,T,params) + min_fmax;
  f0s = (max_f0 - min_f0) * gRand(1,T,params) + min_f0;
  nstarts = round(((t2-t1)*gRand(T,1,params) + t1)*2/params.segmentDuration);
  nstops = round((N - (nstarts + min_pixels)) .* gRand(T,1,params) + nstarts + min_pixels);

  % need to repmat these two for compatability
  fmaxes = repmat(fmaxes, N, 1);
  f0s = repmat(f0s, N, 1);

  xvalsHold = xvals;
  
  % initialize these matrices
  xvalsAll = gZeros(N,T,params); yvalsAll = gZeros(N,T,params);
  HAll = gZeros(N,T,params); alphasAll = gOnes(1,T,params);

  % make vector of times, calculate f(t)
  t = gColon(1,1,N,params)' * (params.segmentDuration/2);
  foft = (fmaxes - f0s) .* exp(-t*alphas) + f0s;
    
  % Compute h vs. time
  hoft = ones(size(foft));
  hoft(isinf(hoft)) = 0;
  sumhoft = sum(hoft);
  sumhoft = repmat(sumhoft', 1, size(yvals,1))';
  H = hoft ./ sumhoft;

  % Convert frequency to map indices
  foft = (foft - params.fmin)*params.segmentDuration + 1;
  yvals = foft;
    
  xvals = repmat(xvalsHold', 1, T);
  nstarts = repmat(nstarts, 1, N)';
  nstops = repmat(nstops, 1, N)';

  % this is how we enforce the start and end times
  xvals = mod(xvals + nstarts - 1, N) + 1;
  yvals(xvals <= repmat(xvals(end,:)', 1, N)') = NaN;
  yvals(xvals > nstops) = NaN;

  H(xvals >= repmat(xvals(end,:)',1,N)') = 0;
  H(xvals > nstops) = 0;

  track_data.alpha = alphas(1,:);
  track_data.fmax = fmaxes(1,:);
  track_data.f0 = f0s(1,:);

end

return;
