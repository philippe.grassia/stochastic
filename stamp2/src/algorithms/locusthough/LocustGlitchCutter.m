function [SNRMap,Glitches]=LocustGlitchCutter(SNRMap,GlitchCutLevel,CutMode)
% This function is for suppressing vertical glitches in ft-maps before we apply the Locust
% image process on them. A column of the input ft-map, 'SNRMap', is considered to be
% contaminated with a vertical glitch if the absolute value of 'GlitchCutLevel' ratio of
% the elements within the bin exceeds a threshold (or alternatively, if the mean of its
% elements exceed the value of the input parameter 'GlitchCutLevel'). If a column satisfy
% this condition, elements in each row are exchanged with the mean of such closest
% neighbors towards and backwards in time that do not satisfy the condition. If the first
% or the last column of the ft-map is contaminated with a glitch, then all the elements in
% the column are set to zero.
% This way of glitch rejection is optimized for the needs of the Locust image processing
% method that applies a local wandering in finding narrow-band tracks of local maxima in
% the ft-map. It should not be used before any image processing methods other than Locust.
%
%
% Peter Raffai
% Email: praffai_at_bolyai.elte.hu
%
% Last modified: 09/06/2011
%

%%% Here were define a time bin of an ft-map to be contaminated by a vertical glitch if
% the mean of the elements in the bin exceeds a certain value denoted by 'GlitchCutLevel'.
% (this is an alternative definition, which by default is not recommended to be used)
% % % Calculating the mean of elements in each column and storing it in 'MeanVect'. Then
% the indices of columns that satisfy the glitch rejection condition are stored in a vector
% 'GlitchInds'.
% % MeanVect=sum(SNRMap)/size(SNRMap,1);
% % GlitchInds=find(MeanVect>=GlitchCutLevel);
% % Glitches=GlitchInds; % this is one of the outputs of the function


% Here were define a time bin of an ft-map to be contaminated by a vertical glitch if the
% absolute value of 'GlitchCutLevel' ratio of the elements within the bin exceeds 'SNRCut'.
% The indices of columns that satisfy the glitch rejection condition are stored in a vector
% 'GlitchInds'.
SNRCut=2;
GlitchInds=[];
for i=1:size(SNRMap,2)
	Perc=length(find(abs(SNRMap(:,i))>SNRCut))/length(SNRMap(:,i));
        if(Perc>GlitchCutLevel) GlitchInds=[GlitchInds,i]; end
	clear Perc;
end
Glitches=GlitchInds;

% If 'CutMode' is set to 1, all columns from the 'SNRMap' are removed that satisfies the
% glitch rejection criterion.
if(CutMode==1)
	SNRMap(:,GlitchInds)=[];
else
	% If the first or last column of the input ft-map satisfies the glitch rejection
	% criterion, then all the elements in the columns are set to zero.
	if(length(GlitchInds)>0)
    		if(GlitchInds(length(GlitchInds))==size(SNRMap,2))
			SNRMap(:,size(SNRMap,2))=0;
			GlitchInds(length(GlitchInds))=[];
    		end
    		if(GlitchInds(1)==1)
        		SNRMap(:,1)=0;
        		GlitchInds(1)=[];
    		end
	end

	% Processing all columns in the ft-map other than the first and the last column
	Map=SNRMap;
	if(length(GlitchInds)>0)
		for i=1:length(GlitchInds)
        		IndVect(1:length(MeanVect))=1;
       			IndVect(GlitchInds)=0;
        		Col=GlitchInds(i);
        		NormCol1=max(find(IndVect(1:Col-1)));
        		IndVect(1:Col-1)=0;
        		NormCol2=min(find(IndVect));
        		Map(:,Col)=(SNRMap(:,NormCol1)+SNRMap(:,NormCol2))/2;
    		end
    	end
SNRMap=Map; % This is the main output of the function
end

% Visualizing the processed ft-map (optional)
% %figure(88); imagesc(SNRMap); axis xy; colorbar('EastOutSide');

