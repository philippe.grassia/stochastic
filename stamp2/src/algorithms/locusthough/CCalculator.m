function C=CCalculator(Signal,SNRMap,SNRSigmaMap,RadSig)
% This part of the Locust and Hough program calculates the raw test
% statistic for an input trace.
%
%
% written by Peter Raffai, Eotvos University (praffai_"at"_bolyai.elte.hu)
%
% Last modified: 10.01.2011.
%
%
% Input:
% - Signal: the N-by-4 matrix output of the Locust and
% Hough search. Each rows of this matrix corresponds to one pixel of the
% signal candidate trace. The second and third elements of a row represents
% the 'x' and 'y' coordinates of the pixel within the original ft-map,
% - SNRMap,SNRSigmaMap: the first and fourth element of a row in 'Signal'
% gives the corresponding value of the pixel within the SNRMap and
% SNRSigmaMap respectively.
% - RadSig: radius of the environment of a trace for which the test
% statistic is calculated
%
%
% Output:
% - C: value of the raw Locust and Hough test statistic


%Calculating integrals for the test statistic
PolyIntegral = 0;
BackgroundIntegral = 0;
Len=0;
for i=1:size(Signal,1)
    Row=Signal(i,2);
    Col=Signal(i,3);
    if((Row>3*RadSig+(1-isequal(RadSig,0))) ...
            & (Row<size(SNRMap,1)-3*RadSig+isequal(RadSig,0)))
        PolyIntegral = PolyIntegral + Signal(i,1) ...
            + sum(SNRMap(Row-RadSig:Row-1,Col)) ...
            + sum(SNRMap(Row+1:Row+RadSig,Col));
        BackgroundIntegral = BackgroundIntegral + Signal(i,4) ...
            + sum(SNRSigmaMap(Row-RadSig:Row-1,Col)) ...
            + sum(SNRSigmaMap(Row+1:Row+RadSig,Col));
        Len=Len+1;
    end
end

%Calculating the test statistic, 'C'
if(Len==0)
    C=0;
else
    Y=PolyIntegral/BackgroundIntegral;
    Sigma=BackgroundIntegral^-0.5;
    SNR=Y/Sigma;
    C=SNR;
end

