function locustSearch(SNRMap,map,Threshold,OutputDir,RadiusX,RadiusY,RadSig,Glitches)
% Program that carries out the so called Locust search on an input
% ft-map and may output detection candidates.
%
% This program is called by searchLH.m, and therefore
% by default must be in the same directory.
%
% written by Peter Raffai, Eotvos University (praffai_"at"_bolyai.elte.hu)
%
% Last modified: 10.01.2011.
%
%
% Input:
% - SNRMap: the ft-map produced by the searchLH code
% - map: the structure that contains all ft-mapss produced by the stochMap
% code
% - Threshold: the detection threshold parameter which the test
% statistic is compared to.
% - OutputDir: name of the directory where files corresponding to possible
% event candidates are dumped.
% - RadiusX,RadiusY: size parameters of the local search area.
% - RadSig: radius of the environment of a trace for which the test
% statistic is calculated
% - The maximum number of traces separated from the input ft-map
% (SignalNumberMax) is defined within this code.
%
%
% Output:
% The program outputs a .mat file for the most probable detection candidate
% if its detection probability exceeds 'Threshold' (otherwise the search
% ends with a no detection statement on the screen). The output .mat file
% stores all relevant parameters of the candidate.

% Hardcoded parameter (high values might slow the machine down, but might
% increase search efficiency)
SignalNumberMax=500;

% Initializing sigma map
SNRSigmaMap = map.sigma.^-2;

% Adding rows of zeros to the edges of the matrix, to avoid
% indexing errors when these edges are reached within a search loop.
SGN(1:size(SNRMap,1)+2*RadiusY,1:size(SNRMap,2)+2*RadiusX) = 0;
SGNSigma(1:size(SNRMap,1)+2*RadiusY,1:size(SNRMap,2)+2*RadiusX) = 0;
for y = 1:size(SNRMap,1)
    for x = 1:size(SNRMap,2)
        if (SNRMap(y,x) > 0)
            SGN(RadiusY+y,RadiusX+x) = SGN(RadiusY+y,RadiusX+x) + SNRMap(y,x);
            SGNSigma(RadiusY+y,RadiusX+x) = SGNSigma(RadiusY+y,RadiusX+x) ...
                + SNRSigmaMap(y,x);
        end
    end
end


% Starting the count of potential signals with 1
SignalNumber = 1;


% This is the main search loop
while ((length(find(SGN>0)) > 0) & (SignalNumber < SignalNumberMax))
    %Locust search is stopped if all matrix elements equals SNR=CutThreshold
    %or if the maximum number of separated traces is reached
    
    % Looking for the starting point of the search (the global maximum of
    % the recently processed matrix)
    [Max,MaxIndex]=max(SGN);
    [MaxValue,StartX]=max(Max);
    StartY=MaxIndex(StartX);
    Signal(1,:) = [SGN(StartY,StartX),StartY-RadiusY,StartX-RadiusX, ...
        SGNSigma(StartY,StartX)];%storing the first element of the trace
    PrevMaxStart=SGN(StartY,StartX);
    SGN(StartY,StartX)=0;%value of the stored element is set to CutThreshold
    SignalElements = 1;%number of elements in the trace
    
    %Local wandering to forward in time
    Col=StartX;%recent column coordinate
    Row=StartY;%recent row coordinate
    PrevMax=PrevMaxStart;
    while ( max(max(SGN(Row-RadiusY:Row+RadiusY,Col:Col+RadiusX))) ~= 0)
        %wandering stops if local maximum in the search area equals
        %CutThreshold
        %Looking for local maximum element and storing its coordinates and
        %SNR value
        [TempMax,TempMaxIndex]=max( SGN(Row-RadiusY:Row+RadiusY,Col:Col+RadiusX) );
        [TempMaxValue,X]=max(TempMax);
        Y=TempMaxIndex(X);
        Row=Row-RadiusY+Y-1;
        Col=Col-1+X;
        SignalElements=SignalElements+1;
        Signal(SignalElements,:) = ...
            [SGN(Row,Col),Row-RadiusY,Col-RadiusX,SGNSigma(Row,Col)];
        PrevMax=SGN(Row,Col);
        SGN(Row,Col)=0;%value of the stored element is set to zero
    end
    SignalEnd=Col;%storing the column coordinate of the last trace element
    
    %Local wandering to backward in time, starting from the original
    %starting point
    Col=StartX;
    Row=StartY;
    PrevMax=PrevMaxStart;
    while ( max(max(SGN(Row-RadiusY:Row+RadiusY,Col-RadiusX:Col))) ~= 0)
        [TempMax,TempMaxIndex]= ...
            max( SGN(Row-RadiusY:Row+RadiusY,Col-RadiusX:Col) );
        [TempMaxValue,X]=max(TempMax);
        Y=TempMaxIndex(X);
        Row=Row-RadiusY+Y-1;
        Col=Col-RadiusX+X-1;
        SignalElements=SignalElements+1;
        Signal(SignalElements,:) = ...
            [SGN(Row,Col),Row-RadiusY,Col-RadiusX,SGNSigma(Row,Col)];
        PrevMax=SGN(Row,Col);
        SGN(Row,Col)=0;
    end
    SignalStart=Col-1;
    
    %Calculating test statistic
    C(SignalNumber)=CCalculator(Signal,SNRMap,SNRSigmaMap,RadSig);
    Signals{SignalNumber}=Signal;
    SignalNumber=SignalNumber+1;
    clear Signal;

end

NSignals=4;
Integral=0;
CTemp=C;
for i=1:NSignals
    [MaxC,MaxCInd]=max(CTemp);
    Signal=Signals{MaxCInd};
    Integral=Integral+CTemp(MaxCInd);
    CTemp(MaxCInd)=0;
end
p = msnr_prob(Integral,map.nindep);
if((1-p)>Threshold)
    for i=1:NSignals
        [MaxC,MaxCInd]=max(C);
        Signal=Signals{MaxCInd};
        Integral=C(MaxCInd);
        FileNameStr = sprintf('%s/SignalLocust%i.mat',OutputDir,i);
        t=map.xvals;
        f=map.yvals;
        save(FileNameStr,'Signal','SNRMap','SNRSigmaMap','t','f','Integral','p','Glitches');
        C(MaxCInd)=0;
    end    
else
    return;
end
clear Signals;


