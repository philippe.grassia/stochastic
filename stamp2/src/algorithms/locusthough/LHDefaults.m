function params = LHDefaults(params)
% Sets parameters for Locust and Hough searches 
% Written by Peter Raffai (praffai_"at"_bolyai.elte.hu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.doLocust = true;
params.doHough = true;

params.OutputDirectory = 'LH_found';%output directory for the results


params.LocustCutThreshold = 0.99;%percentage of lowest elements to be removed from the map before the search
params.LocustRadiusX = 1;%radius of local environment along the time axis
params.LocustRadiusY = 10;%radius of local environment along the frequency axis
params.LocustRadSig = 3;%radius of environment for which the test statistic is calculated (should be left as =0)
params.LocustDetectionThreshold = 0;%percentage, threshold p value


params.HoughCutNumber = 8;%number of highest ft-map elements to be used in Hough transformation
params.HoughRadSig = 3;%radius of environment for which the test statistic is calculated (should be left as =0)
params.HoughDetectionThreshold = 0;%percentage, threshold p value

% Defining threshold that decides if a certain column in the ft-map is contaminated by a vertical glitch.
% The threshold corresponds to the mean of matrix elements in the column.
params.GlitchCutThreshold = 100;

return;
