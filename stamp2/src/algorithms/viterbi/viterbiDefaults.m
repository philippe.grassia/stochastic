function params = viterbiDefaults(params)
% function params = viterbiDefaults(params)
%
% Routine written by Shivaraj Kandhasamy.
% Contact: shivaraj@lphysics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.stochtrack.doViterbi = True;

% viterbi spindown default
params.viterbi.spindown = 10;

