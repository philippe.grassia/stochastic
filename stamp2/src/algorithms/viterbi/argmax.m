function j = argmax(L)
% PARGMAX Find the indices of the optimal choices in L.

% Probability matrix:
% It is a simplified 1/2, 1/2 model in this application.

[~, j] = max(L);
