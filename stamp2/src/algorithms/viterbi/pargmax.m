function idx = pargmax(L)
% PARGMAX Find the indices of the optimal choices in L.

[m,n]=size(L);

if round(m/2) == m/2
    pad = m/2-1;
    p = [zeros(1,pad) ones(1,pad+2)];
else
    pad = (m-1)/2;
    p = [zeros(1,pad) ones(1,pad+1)];
end

[~, j] = max(L.*repmat(p',1,n));

idx = j-pad;
