function max_cluster = viterbi(map, params)
% function max_cluster = viterbi(map, params)
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.nPix -> number of pixels in the largest cluster
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.y_gamma -> Y (GW estimator) of largest cluster
%        max_cluster.sigma_gamma -> sigma of the above estimator
%        max_cluster.reconMax -> 2D array containing weight numbers for
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
%        max_cluster.tmin
%        max_cluster.tmax
%        max_cluster.fmin
%        max_cluster.fmax
%        max_cluster.numpixels
%        max_cluster.SNRfrac  
% Written by E Thrane and M Coughlin
% Contact: ethrane@ligo.caltech.edu, michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% map of NaNs: zeros where NaNs are present
nanmap0 = ~isnan(map.snr);

% NaNs occur due to missing data, vetoes, and notches.  When a NaN is
% encountered, y and snr are set to zero (so they do not add to the cluster SNR
% and sigma is set to one (much larger than typical strain power) so that the 
% cluster sigma does not depend significantly on the NaN pixels.  Just to be 
% safe, a check is employed to make sure that map.sigma << 1 in case anyone
% uses this with some funny units of strain.
if max(map.sigma(:)) > 1e-10
  error('Surprisingly large value of strain power encountered.');
end
map.snr(isnan(map.snr)) = 0;
map.y(isnan(map.y)) = 0;
map.z(isnan(map.z)) = 0;
map.sigma(isnan(map.sigma)) = 1;
map.sigma(map.sigma==0) = 1;

% map is MxN: M is the # of frequency bins and N is the number of segments
[M,N] = size(map.snr);

%adjust for playing nice with viterbi
adjust = min(map.snr(:));
adj_map = map.snr - adjust;
[path0, delta, psi, score, path_power] = viterbi_colFLT(params.viterbi.spindown, adj_map);
path_power = path_power + length(path0)*adjust;
snr_max = path_power / sqrt(length(path0));

max_cluster.snr_gamma = snr_max;
max_cluster.nPix = length(path0);

% reconstructed track map (shows the loudest track)
rmap = gZeros(size(map.snr,1), size(map.snr,2), params);
% this plots the actual pixels (with the correct pixel SNR) of the track
smap = gZeros(size(map.snr,1), size(map.snr,2), params);
for ii = 1:length(path0)
   rmap(path0(ii),ii) = 1;
   smap(path0(ii),ii) = map.snr(path0(ii),ii);
end

max_cluster.reconMax = rmap;

% calculate Y and sigma
yr = rmap.*map.y;
sr = rmap.*map.sigma;
max_cluster.y_gamma = sum(sum(yr.*sr.^-2)) / sum(sum(sr.^-2));
max_cluster.sigma_gamma = sum(sum(sr.^-2))^-0.5;

% Gather GPU data
max_cluster.reconMax = gather(max_cluster.reconMax);
max_cluster.y_gamma = gather(max_cluster.y_gamma);
max_cluster.sigma_gamma = gather(max_cluster.sigma_gamma);

% Calculate maximum cluster information.                                                 
tvals = map.segstarttime - map.segstarttime(1);
fvals = map.f;
%tvals = sum(max_cluster.reconMax);
%fvals = sum(max_cluster.reconMax');
[r,c] = find(max_cluster.reconMax > 0);
max_cluster.tmin = tvals(min(c));
max_cluster.tmax = tvals(max(c));
max_cluster.gps_min = map.segstarttime(min(c));
max_cluster.gps_max = map.segstarttime(max(c));
max_cluster.fmin = fvals(min(r));
max_cluster.fmax = fvals(max(r));

% Compute fmin and fmax
%fvals = sum(max_cluster.reconMax');
%[junk,maxindex] = max(fvals);

%max_cluster.fmin = fvals(min(find(fvals>0)));
%max_cluster.fmax = fvals(max(find(fvals>0)));
%max_cluster.nPix = max_cluster.nPix;


% calculate SNR frac for all clusters on the map
totalSNR = sum(map.snr(:));
colsSNR=sum(map.snr(:),1)/totalSNR;
temp_idx=find(colsSNR==max(colsSNR));
max_cluster.SNRfrac=colsSNR(temp_idx);
max_cluster.SNRfracTime=map.segstarttime(temp_idx) - ...
    map.segstarttime(1);

idx = find(~isnan(smap));
max_cluster.SNRfrac=max(real(smap(idx)))/sum(real(smap(idx)));

max_cluster.timedelay = 0;

% Gather GPU data
max_cluter.SNRfrac =gather(max_cluster.SNRfrac);

% make a diagnostic plot of the best fit
if params.savePlots
  %index = find(params.viterbi.triggerGPS == map.segstarttime);
  %rmap(:,index) = -5;

  map.xvals=map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);
  map.yvals = map.f;
  printmap(rmap, map.xvals, map.yvals, 't (s)', 'f (Hz)', 'SNR', [-1 1]);
  title(['max cluster SNR = ' num2str(max_cluster.snr_gamma)])
  print('-dpng', [params.plotdir '/rmap']);
  print('-depsc2', [params.plotdir '/rmap']);

end

return
