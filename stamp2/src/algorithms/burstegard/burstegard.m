function [nPix,snr_gamma,y_gamma,sigma_gamma,reconMax,reconAll] = ...
    burstegard(y,sigma,map_size,paramArray,params)
% function [nPix,snr_gamma,y_gamma,sigma_gamma,reconMax,reconAll] = ...
%    burstegard(y,sigma,map_size,paramArray,params)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrapper script for calling burstegard mex code.
% Needed because we have multiple mexed versions of burstegard due
% to people using different versions of Matlab and requiring
% different levels of optimization.
%
% Contact: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Check Matlab version.
vers = version('-release');
vers_year = str2num(vers(1:(end-1)));

if (vers_year >= 2012)
  if ~params.doZebragard
    fprintf('Running optimized burstegard.\n');
  end
  [nPix,snr_gamma,y_gamma,sigma_gamma,reconMax,reconAll] = ...
      burstegard_opt(y', sigma', map_size, paramArray);
else
  if ~params.doZebragard
    fprintf('Running non-optimized burstegard.\n');
  end
  [nPix,snr_gamma,y_gamma,sigma_gamma,reconMax,reconAll] = ...
      burstegard_nonopt(y', sigma', map_size, paramArray);
end

return;