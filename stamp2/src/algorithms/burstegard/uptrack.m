function max_cluster = uptrack(max_cluster, params, map)
% function max_cluster = uptrack(max_cluster, params, map)
%
% Arguments:
% max_cluster - cluster information returned by burstegard code.
% params      - parameters of STAMP search.
% map         - STAMP map struct.
%
% Written by Eric Thrane with help from Tanner Prestegard.
% Contact: ethrane@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Make sure findtrack parameter rr is set.
try
  params.burstegard.rr;
catch
  params.burstegard.rr=35;
end

% Define the distance between two points.
d = @(a, b) sqrt( (a(1)-b(1))^2 + (a(2)-b(2))^2 );

% Prepare t and f maps.
t = 1:size(max_cluster.reconAll,2);
f = 1:size(max_cluster.reconAll,1);
tmap = repmat(t,length(f),1);
fmap = repmat(f,length(t),1)';

% The number of clusters.
nc0 = max(max_cluster.reconAll(:));

if nc0==0
  return;
end

% Find the corners of each cluster.
for ii=1:nc0
  tmin(ii) = min(tmap(find(max_cluster.reconAll==ii)));
  tmax(ii) = max(tmap(find(max_cluster.reconAll==ii)));
  fmin(ii) = min(fmap(find(max_cluster.reconAll==ii)));
  fmax(ii) = max(fmap(find(max_cluster.reconAll==ii)));
end
  
% Sort clusters by tmin.
[dum idx] = sort(tmin);
tmin = tmin(idx);
tmax = tmax(idx);
fmin = fmin(idx);
fmax = fmax(idx);

% Sort clusters by tmin.
for aa=1:nc0
  max_cluster.reconAll(max_cluster.reconAll==idx(aa)) = -aa;
%  fprintf('%i -> %i\n', aa, -idx(aa));
end
max_cluster.reconAll = -max_cluster.reconAll;

qq=1;        % number of mergers
nc=nc0;      % number of clusters
merge = [];  % initialize the array of pairs to merge

% Loop over clusters jj.
jj=1;
while jj<=nc0-1
  % Loop over subsequent clusters (jj+kk).
  kk=1;
  while kk<=nc0-jj
    % up-chirping distance.
    chirpup = d([tmax(jj) fmax(jj)], [tmin(jj+kk) fmin(jj+kk)]);
    %fprintf('> %i %i %i %4.1f\n', jj, jj+kk, nc, chirpdown);
    if chirpup<params.burstegard.rr
%     fprintf('connect %i and %i\n', jj, jj+kk);
%     fprintf('(%3.1f %i) to (%3.1f %i)\n', ...
%       tmax(jj)/2, fmin(jj), tmin(jj+kk)/2, fmax(jj+kk));
      % keep track of which clusters to merge
      nc=nc-1;
      merge(qq,:) = [jj jj+kk];
      qq=qq+1;
    end
    kk=kk+1;
  end
  jj=jj+1;
end

%keyboard
% fix merge, keep uniques
for i=1:(size(merge,1)-1)
  subset = merge((i+1):end,1);
  r = find(subset==merge(i,1));
  subset(r) = merge(i,2);
  merge((i+1):end,1) = subset;
end
merge = unique(merge,'rows');
if ~isempty(merge) > 0
  cut = merge(:,1) ~= merge(:,2);
  merge = merge(cut,:);
end
nc = nc0 - size(merge,1);
fprintf('%i combinations; %i clusters\n', nc0-nc, nc);

% Merge neighboring clusters.
for bb=1:size(merge,1);
  max_cluster.reconAll(max_cluster.reconAll==merge(bb,1)) = merge(bb,2);
  %fprintf('%i -> %i\n', merge(bb,1), merge(bb,2));
end

clust_nums_left = sort(unique(max_cluster.reconAll));
clust_nums_left = clust_nums_left(2:end); % remove 0s

for i=1:length(clust_nums_left)
  max_cluster.reconAll(max_cluster.reconAll == clust_nums_left(i)) = i;
end

% Calculate new snr_gamma, y_gamma, and sigma_gamma.
maxsnr = 0;
maxsnridx = 0;
for dd=1:nc
  cut = max_cluster.reconAll==dd;
  Y = sum(map.y(cut).*map.sigma(cut).^-2) / sum(map.sigma(cut).^-2);
  Sigma = (sum(map.sigma(cut).^-2))^-0.5;
  snr(dd) = Y/Sigma;
  if snr(dd)>maxsnr
    maxsnr = snr(dd);
    maxsnridx = dd;
  end
end

% Define cluster variables.
dd = maxsnridx;
cut = max_cluster.reconAll(:)==dd;
max_cluster.y_gamma = sum(map.y(cut).*map.sigma(cut).^-2) / ...
  sum(map.sigma(cut).^-2);
max_cluster.sigma_gamma = (sum(map.sigma(cut).^-2))^-0.5;
max_cluster.snr_gamma = maxsnr;

% Create reconstruction map.
max_cluster.reconMax = zeros(size(max_cluster.reconAll));
max_cluster.reconMax(cut) = map.snr(cut);

return
