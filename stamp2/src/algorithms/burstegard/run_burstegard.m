function max_cluster = run_burstegard(map, params)
% function max_cluster = run_burstegard(map, params)
%
% Arranges parameters and calls the burstegard
% clustering code (written in C++ and interfaced with MEX).
%
% Arguments:
% map    - STAMP map struct.
% params - struct containing parameters for STAMP search.
%
% Written by Tanner Prestegard and Eric Thrane.
% Contact: prestegard@physics.umn.edu, ethrane@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Determine x-values.
map.xvals = map.segstarttime - map.segstarttime(1);

% Arrange params into a numeric array for passing to cluster code.
paramArray = [params.burstegard.NCN params.burstegard.NR ...
	      params.burstegard.pixelThreshold params.burstegard.tmetric ...
	      params.burstegard.fmetric params.burstegard.debug ...
	      params.burstegard.weightedSNR];

% Run cluster code, assign outputs properly.
[a,b,c,d,e,f] = burstegard(map.y, map.sigma, size(map.snr), paramArray, params);

% Calculate cluster statistics.
% We don't do this when zebragard is run because it wastes a lot of time.
% Cluster stats are calculated at the end of zebragard clustering
% in zebratrack.m.
if (params.doZebragard)
  % set up blank max_cluster struct
  max_cluster.nPix = a;
  max_cluster.snr_gamma = b;
  max_cluster.y_gamma = c;
  max_cluster.sigma_gamma = d;
  max_cluster.reconMax = e';
  max_cluster.reconAll = f';
  max_cluster.tmin = 0;
  max_cluster.tmax = 0;
  max_cluster.gps_min = 0;
  max_cluster.gps_max = 0;
  max_cluster.fmin = 0;
  max_cluster.fmax = 0;
else
  max_cluster = calc_cluster_stats(f',map,params);
end

% findtrack is a burstegard cluster-the-cluster option.
try
  params.burstegard.findtrack;
catch
  params.burstegard.findtrack=false;
end
if params.burstegard.findtrack
  max_cluster0 = max_cluster;
  max_cluster = findtrack(max_cluster0, params, map);
  max_cluster2 = uptrack(max_cluster0, params, map);
  if max_cluster2.snr_gamma > max_cluster.snr_gamma
    max_cluster = max_cluster2;
  end
end

% The super-cluster is the sum of all clusters.
if params.burstegard.super
  cut = max_cluster.reconAll > 0;
  max_cluster.super_snr = sum(sum( map.y(cut).*map.sigma(cut).^-2 ));
  max_cluster.super_snr = max_cluster.super_snr / ...
      sum(sum( map.sigma(cut).^-2 ))^0.5;
end


return;
