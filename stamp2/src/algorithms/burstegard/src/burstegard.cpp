#include "mex.h"

void tree(const double* input_y, const double* input_sigma, const int size_x, const int size_y, const int params_num_pix, const double params_rad, const double params_snr_threshold, const double params_x_metric, const double params_y_metric, const int params_debug, const bool params_weightedSNR, int &nPix, double &snr_gamma, double &y_gamma, double &sigma_gamma, double* output_max, double* output_all);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /* usage: test(M size(M,1), size(M,2)) where M is some matrix */
  /* note: plhs = left-hand side and prhs = right-hand side */
  
  /* initialize input variables */
  const double *in_y = mxGetPr(prhs[0]);        /* values of input y array */
  const double *in_sigma = mxGetPr(prhs[1]);        /* values of input sigma array */
  const double *mapSize = mxGetPr(prhs[2]);           /* array containing map size */
  const int size_y = (int)mapSize[0];
  const int size_x = (int)mapSize[1];
  const double *paramArray = mxGetPr(prhs[3]);  /* input array of parameters */
  const int num_pix = (int)paramArray[0];
  const double rad = paramArray[1];
  const double snr_threshold = paramArray[2];
  const double x_metric = paramArray[3];
  const double y_metric = paramArray[4];
  const int debug = paramArray[5];
  const bool weightedSNR = paramArray[6];

  if (debug > 0) {
    mexPrintf("Params: num_pix=%i rad=%1.2f snr_thresh=%1.2f x_met=%1.2f y_met=%1.2f\n",num_pix,rad,snr_threshold,x_metric,y_metric);
  }
  
  /* initialize output variables */
  mxArray *X0,*X1,*X2,*X3,*X4,*X5; /* MEX arrays to return to Matlab code */
  double *a,*b,*c,*d;   /* pointers to MEX arrays */
  double *out_max;      /* values of output array (max cluster) */
  double *out_all;      /* values of output array (all clusters) */
  int nPix;             /* number of pixels in max. cluster */
  double snr_gamma, y_gamma, sigma_gamma; /* statistics of max. cluster */
  
  /* prepare other outputs */
  X0 = plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  X1 = plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);   
  X2 = plhs[2] = mxCreateDoubleMatrix(1, 1, mxREAL);
  X3 = plhs[3] = mxCreateDoubleMatrix(1, 1, mxREAL);
  X4 = plhs[4] = mxCreateDoubleMatrix(size_x, size_y, mxREAL);
  X5 = plhs[5] = mxCreateDoubleMatrix(size_x, size_y, mxREAL);
  
  /* assign pointers */
  a = mxGetPr(X0);
  b = mxGetPr(X1);
  c = mxGetPr(X2);
  d = mxGetPr(X3);
  out_max = mxGetPr(X4);
  out_all = mxGetPr(X5);
  
  /* call c++ clustering code */
  tree(in_y,in_sigma,size_x,size_y,num_pix,rad,snr_threshold,x_metric,y_metric,debug,weightedSNR,nPix,snr_gamma,y_gamma,sigma_gamma,out_max,out_all);
  
  /* set values of outputs */
  *a = nPix;
  *b = snr_gamma;
  *c = y_gamma;
  *d = sigma_gamma;
  
  /*mexPrintf("nPix = %i\nsnr_gamma = %f\n", nPix, snr_gamma);*/
  
}
