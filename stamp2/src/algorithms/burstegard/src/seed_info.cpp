#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include "cluster.h"

using namespace std;

// PURPOSE:
// Compile information about each seed (location, SNR, Y value, sigma value, etc.)
//
// INPUTS:
// y_vec     - input 1D Y map.
// sigma_vec - input 1D sigma map.
// x         - map size in x direction.
// y         - map size in y direction.
// params    - Params object (contains user-specified clustering parameters).
//
// OUTPUTS:
// seeds     - vector of Seed objects (see cluster.h for details on Seed objects).
// nSeeds    - total number of seeds.
///////////////////////////////////////////////////////////////////////////////////

// Main function.
void seed_info(const double* y_vec, const double* sigma_vec, const int x, const int y, const Params params, vector<Seed> &seeds, vector<Row> &rows, int &nSeeds) {

  // Loop over seeds, determine information.
  int n = 0;
  for (int k = 0; k < (x*y); k++) {
    double snr = y_vec[k] / sigma_vec[k];
      if (snr >= params.snr_threshold) {
	Seed temp_seed;
	temp_seed.num = n;
	temp_seed.x_loc = k % x;
	temp_seed.y_loc = k / x;
	temp_seed.snr = snr;
	temp_seed.y = y_vec[k];
	temp_seed.sigma = sigma_vec[k];
	seeds.push_back(temp_seed);
	rows[k/x].seedlist.push_back(n);
	n++;
      }
  }

  // Assign row index (seed number within that row) to each seed.
  for (unsigned int i = 0; i < rows.size(); i++) {
    for (unsigned int j = 0; j < rows[i].seedlist.size(); j++) {
      seeds[rows[i].seedlist[j]].rowNum = j;
    }
  }
  
  nSeeds = n;

}
