#include "cluster.h"

// Default params constructor.
Params::Params () {
  num_pix = 0;
  rad = 0;
  snr_threshold = 0;
  x_metric = 0;
  y_metric = 0;
  debug = 0;
  weightedSNR = false;
}

// Params constructor with parameters.
Params::Params(int a, double b, double c, double d, double e, int f, bool g) {
  num_pix = a;
  rad = b;
  snr_threshold = c;
  x_metric = d;
  y_metric = e;
  debug = f;
  weightedSNR = g;
}

// Set values of a Params object.
void Params::set_values (int a, double b, double c, double d, double e, int f, bool g) {
  num_pix = a;
  rad = b;
  snr_threshold = c;
  x_metric = d;
  y_metric = e;
  debug = f;
  weightedSNR = g;
}
