#ifndef CLUSTER_H_
#define CLUSTER_H_

#include <vector>
#include <cmath>
using namespace std;

// Params object:
// num_pix       - minimum number of pixels for a cluster to be formed.
// rad           - maximum radius for two pixels to be clustered together.
// snr_threshold - SNR threshold for a pixel to be identified as a seed.
// x_metric      - size of "ellipse" for clustering in x direction.
// y_metric      - size of "ellipse" for clustering in y direction.
// debug         - option to print debug information.  Can be 0, 1, or 2
//                 based on amount of information desired.
// weightedSNR   - true: use STAMP weighted SNR to calculate cluster
//                 statistics.  false: use sum of pixel SNRs.
class Params {
 public:
  unsigned int num_pix;
  double rad;
  double snr_threshold;
  double x_metric;
  double y_metric;
  int debug;
  bool weightedSNR;

  // Constructors.
  Params ();
  Params (int,double,double,double,double,int,bool);

  // Function to set Params values, if not done with a constructor.
  void set_values (int,double,double,double,double,int,bool);
};

// Seed object:
// snr    - STAMP cross-correlation SNR of seed pixel.
// y      - STAMP Y-value of seed pixel.
// sigma  - STAMP sigma of seed pixel.
// num    - overall seed ID number, counting seeds from left to right, top to bottom in input map.
// rowNum - seed ID in row (left to right);
// x_loc  - x location of seed in 2D map/array (column in matrix).
// y_loc  - y location of seed in 2D map/array (row in matrix).
class Seed {
 public:
  double snr, y, sigma;
  int num, rowNum, x_loc, y_loc;
};

// Cluster object:
// clusterIndex - cluster ID number.
// seedlist     - vector containing the seed number (Seed.num) of all pixels included in the cluster.
// nPix         - total number of pixels in the cluster.
// snr_gamma    - STAMP cluster SNR.
// y_gamma      - STAMP cluster Y statistic.
// sigma_gamma  - STAMP cluster sigma.
class Cluster {
 public:
  int clusterIndex;
  vector<int> seedlist;
  int nPix;
  double snr_gamma, y_gamma, sigma_gamma;
};

// Row object:
// index    - row number in 2D array (top to bottom).
// seedlist - list of all above threshold pixels in that row, by seed number.
class Row {
 public:
  int index;
  vector<int> seedlist;
};

// PURPOSE:
// Calculate the distance between two seed pixels.
// does this using two Seed objects.
//
// INPUTS:
// params - Params object.
// seed1 - first Seed object.
// seed2 - second Seed object.
//
// The function is inlined to increase speed.
//////////////////////////////////////////////////
inline double calc_dists(const Params params, const Seed seed1, const Seed seed2) {

  // Calculate distances.
  // params.x_metric and params.y_metric specify the "ellipse" size.
  // Ex: params.x_metric = 2 and params.y_metric = 1 would mean that
  // we would use an ellipse (1/4)x^2 + y^2 <= (params.rad)^2 to cluster.
  double x_dist = (seed2.x_loc - seed1.x_loc)*(1/params.x_metric);
  double y_dist = (seed2.y_loc - seed1.y_loc)*(1/params.y_metric);
  double tot_dist = sqrt( pow(abs(x_dist),2) + pow(abs(y_dist),2) );

  if (x_dist > params.rad) { return -1; }
  else { return tot_dist; }
}


#endif
