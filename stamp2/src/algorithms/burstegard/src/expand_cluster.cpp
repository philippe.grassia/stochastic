#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include "cluster.h"

using namespace std;

// Define other functions called from this function.
//double calc_dists(const Params params, const Seed seed1, const Seed seed2);

// PURPOSE:
// Takes an input seed (seedpoint) and expands a Cluster around it.
// Adds final Cluster to clusters vector.
//
// INPUTS:
// params        - Params object.
// seeds         - vector of Seed objects.  Used in calc_dists.
// seedNum       - the number (Seed.num) of the original seedpoint passed to
//                 this function by cluster_pixels.
// rows          - vector of Row objects.  Modified by this code, can't be const.
//
// OUTPUTS:
// clusters - a vector of Cluster objects.  Will have Clusters added to it by this function.
//////////////////////////////////////////////////////////////////////////////


// Main function.
void expand_cluster(const Params params, const vector<Seed> &seeds, const int seedNum, vector<Row> &rows, vector<Cluster> &clusters) {

  // Define a temporary Cluster object and reserve space for it.
  // Add the current seedpoint to this Cluster.
  Cluster tempCluster;

  // Reserve space for 1000 seeds in each Cluster, which should be okay for realistic
  // signals and reasonable clustering parameters.
  tempCluster.seedlist.reserve(1000);

  // Add current seed pixel to the Cluster and erase it from available seed pixels, since
  // it has been added to a Cluster.  It should be the first pixel in its Row since
  // pixels to the left of it (within its Row) should have been used as seeds already,
  // and been deleted from this Row.
  tempCluster.seedlist.push_back(seedNum);
  rows[seeds[seedNum].y_loc].seedlist.erase(rows[seeds[seedNum].y_loc].seedlist.begin());

  // Expand a Cluster - loop over above threshold pixels and try to cluster them to
  // the seed pixel.  Initially the Cluster only consists of the seed pixel, but other
  // pixels may be added to the Cluster within this loop.
  double distance; 
  vector<Row>::iterator row_iter;
  vector<int>::iterator seed_iter;
  for (unsigned int i = 0; i < tempCluster.seedlist.size(); i++) {

    // Debugging line.
    if (params.debug >= 2) {
      cout << "Clustering seed " << tempCluster.seedlist[i] << "." << endl;
    }

    // Loop over all rows.
    row_iter = rows.begin();
    while (row_iter != rows.end()) {

      // If we are too high above seed pixel, continue to next Row.
      if ( ((seeds[tempCluster.seedlist[i]].y_loc - (*row_iter).index)*(1/params.y_metric)) > params.rad) { 
	row_iter++;
	continue;
      }

      // If we are too far below the seed pixel, we can't cluster any more pixels to it.
      // So we end clustering to this seed.
      if ( (((*row_iter).index)*(1/params.y_metric) - seeds[tempCluster.seedlist[i]].y_loc) > params.rad) { 
	break;
      }

      // Loop over above threshold pixels within a Row.
      seed_iter = (*row_iter).seedlist.begin();
      while (seed_iter != (*row_iter).seedlist.end()) {

	// Debugging.
	if (params.debug >= 2) {
	  cout << "Match " << seeds[tempCluster.seedlist[i]].num << " to " << seeds[(*seed_iter)].num << "." << endl;
	}
	
	// Calculate distance between seed pixels.
	distance = calc_dists(params,seeds[tempCluster.seedlist[i]],seeds[(*seed_iter)]);
	
	// Debugging.
	if (params.debug >= 2) { cout << "\tDistance: " << distance << endl; }

	// Distance = -1 indicates too far right in x-direction.
	// So we move on to the next row for clustering.	
	if (distance == -1) {
	  break;
	}
	// If the distance is less than the clustering radius, add this pixel to the Cluster.
	else if (distance <= params.rad && distance > 0) {
	  // Add pixel to Cluster seedlist and erase it from the Row seedlist.
	  tempCluster.seedlist.push_back((*seed_iter));
	  (*row_iter).seedlist.erase(seed_iter);

	  // Debugging.
	  if (params.debug >= 2) {
	    cout << "\tClustered " << seeds[tempCluster.seedlist[i]].num << " to " << seeds[(*seed_iter)].num << "." << endl;
	  }
	}
	else {
	  seed_iter++;
	}
      }
      row_iter++;
    }
  }

  // At this point, clustering is complete for a set of pixels.
  // If the Cluster contains enough pixels, we add it to the
  // list of "final" Clusters.
  if (tempCluster.seedlist.size() >= params.num_pix) {
    tempCluster.nPix = tempCluster.seedlist.size();
    tempCluster.clusterIndex = clusters.size();
    clusters.push_back(tempCluster);
  }

}
