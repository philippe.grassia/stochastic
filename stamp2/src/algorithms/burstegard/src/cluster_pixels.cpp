#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include "cluster.h"

using namespace std;

// Define functions from other .cpp files.
void expand_cluster(const Params params, const vector<Seed> &seeds, const int seedNum, vector<Row> &rows, vector<Cluster> &clusters);

// PURPOSE:
// Function for grouping pixels into clusters.  Returns a vector of all clusters
// found in the input map.
//
// INPUTS:
// params      - Params object.
// seeds       - an array of Seed objects.
// rows        - an array of Row objects.  Modified by expand_cluster,
//               can't be const.
//
// OUTPUTS:
// clusterList - a vector of cluster objects (see cluster.h for info on Cluster objects).
/////////////////////////////////////////////////////////////////////////////////////////

// Main functions.
void cluster_pixels(const Params params, const vector<Seed> &seeds, vector<Row> &rows, vector<Cluster> &clusterList) {

  // Iterators for looping over seeds.
  vector<Row>::iterator row_iter;
  vector<int>::iterator seed_iter;

  // Start finding clusters, beginning with the first seed pixel (seed 0).
  // Within expand_cluster, pixels are deleted from the row seedlist so that
  // they aren't used as seedpoints once they are already in a cluster.
  // Loop through seeds by row, left to right.
  row_iter = rows.begin();
  while (row_iter != rows.end()) {
   
    seed_iter = (*row_iter).seedlist.begin();
    while (seed_iter != (*row_iter).seedlist.end()) {
      if ( (*row_iter).seedlist.empty() ) { break; }
      
      if (params.debug >= 2) {
	cout << "\tOn row " << (*row_iter).index << " seed " << (*seed_iter) << "." << endl;
      }

      expand_cluster(params,seeds,(*seed_iter),rows,clusterList);

      // Reset the iterator to the first element of the row.  The previous
      // first element should have been deleted from the row within the 
      // expand_cluster function since it is already part of a cluster.
      seed_iter = (*row_iter).seedlist.begin();
    }
    row_iter++;
  }

}
