function params = burstegardDefaults(params)
% function params = burstegardDefaults(params)
% Sets parameters for cluster search.  Eric: this file was last modified on
% March 14.  These values represent our current best guess for the most
% sensitive values for LGRB signals in 1s x 1Hz maps.  Thus users should be %
% ware: these values are only "defaults" for a particular analysis.
%
%
% Routine written by Shivaraj Kandhasamy.
% Contact: shivaraj@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters and their values.
params.doBurstegard = true;
params.burstegard.NCN = 80;
params.burstegard.NR = 2;
params.burstegard.pixelThreshold = 0.75;
params.burstegard.tmetric = 1;
params.burstegard.fmetric = 1;
params.burstegard.debug = 0;
params.burstegard.weightedSNR = true;

% The super-cluster is the sum of all clusters.
params.burstegard.super = false;

% findtrack is a cluster-the-cluster option.
params.burstegard.findtrack = false;

% rr is a variable for findtrack.
params.burstegard.rr = 35;

return;
