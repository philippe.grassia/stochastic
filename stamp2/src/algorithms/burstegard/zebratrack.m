function clusterF = zebratrack(map, params)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inputs:
% map      - STAMP map struct.
% params   - STAMP search parameters.
%
% Outputs:
% clusterF - output cluster struct from zebratrack clustering.
%
% Contact: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 1: identify positive clusters and negative clusters separately.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.burstegard.NCN = params.zebragard.NCN(1);
params.burstegard.pixelThreshold = params.zebragard.pixelThreshold;

% Look for positive tracks.
cluster1 = run_burstegard(map, params);

% Look for negative tracks.
map2 = map; map2.y = -map.y; map2.snr = -map.snr;
cluster2 = run_burstegard(map2, params);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 2: try re-cluster positive clusters with other nearby positive
% clusters and negative clusters with other nearby negative clusters.  We
% want to do this because there can be a small break between clusters of
% the same sign.  We do this by increasing the clustering radius slightly.
% We also increase the number of pixels per cluster requirement in order to
% eliminate most noise clusters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.burstegard.NR = params.zebragard.NR(2);
params.burstegard.NCN = params.zebragard.NCN(2);
nPix_req = params.zebragard.NCN(3);

% Re-cluster positive clusters.
mapP = map;
keepP = (cluster1.reconAll > 0);
mapP.y(~keepP) = 0;
clusterP = run_burstegard(mapP,params);

% Re-cluster negative clusters.
mapN = map;
mapN.y = -mapN.y;
keepN = (cluster2.reconAll > 0);
mapN.y(~keepN) = 0;
clusterN = run_burstegard(mapN,params);

% Define a map with map.y = abs(map.y) for negative clustering and
% calculation of cluster statistics.
mapA = map;
mapA.y = abs(mapA.y); mapA.snr = abs(mapA.snr);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 2: Try to connect positive clusters to negative clusters and vice
% versa.  We go through the possible pairings one by one and form a list of
% "connections."  Then we determine which clusters are connected overall.  So
% if P1 is connected to N2, and N2 is connected to P5 and P6, we would form a
% clusters out of P1, N2, P5, and P6.  We greatly increase the clustering
% radius for this step.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.burstegard.NR = params.zebragard.NR(3);
% We require any final cluster to have more than 80 pixels, but since we are
% trying to form clusters using connections of pairs, we can't just set this
% requirement in the burstegard parameters.

% Determine number of positive and negative clusters.
nPos = length(unique(clusterP.reconAll(:))) - 1;
nNeg = length(unique(clusterN.reconAll(:))) - 1;

%fprintf('Positive clusters: %i\n',nPos);
%fprintf('Negative clusters: %i\n',nNeg);

% If there are no clusters of one type, we don't need to try to pair them.
% So we only pair if there are 1 or more clusters of each type.
if (nPos > 0 && nNeg > 0)
  % Adjust negative clusters so that there is no overlap between cluster
  % indices.
  clust_pix = (clusterN.reconAll > 0);
  clusterN.reconAll(clust_pix) = clusterN.reconAll(clust_pix) + nPos;
  
  % Combine all clusters into a single map and make an array featuring all
  % pairings of +/- clusters.
  clusterA.reconAll = clusterP.reconAll + clusterN.reconAll;
  all_clusts = 1:(nPos+nNeg);

  % NEW, more intelligent calculation of +/- cluster pairings.
  pairs = [];
  for ii=1:nPos
    keep1 = ((clusterA.reconAll == ii) | (clusterA.reconAll > nPos));
    PNmap = clusterA.reconAll; PNmap(~keep1) = 0;

    [x,y] = find(PNmap == ii);
    xmin_idx = min(x); xmax_idx = max(x);
    ymin_idx = min(y); ymax_idx = max(y);
    x_dist = params.burstegard.NR*params.burstegard.tmetric;
    y_dist = params.burstegard.NR*params.burstegard.fmetric;

    [y,x] = find(PNmap == ii);
    xmin_idx = max(min(x) - x_dist,1);
    xmax_idx = min(max(x) + x_dist,size(PNmap,2));
    ymin_idx = max(min(y) - y_dist,1);
    ymax_idx = min(max(y) + y_dist,size(PNmap,1));

    % Find nearby clusters.
    p = unique(PNmap(ymin_idx:ymax_idx,xmin_idx:xmax_idx));

    % Remove first two entries; first entry is 0 and second entry should be
    % the positive cluster (we only want to find which negative clusters it
    % was paired to).

    p(1:2) = [];

    for jj=1:length(p)
      pairs = [pairs; ii p(jj)];
    end
  end

  % Loop over +/- cluster pairings and save connections.  We check whether two
  % clusters are connected by comparing the total number of initial pixels in
  % the two clusters to the number of pixels with SNR > 0 in the final reconMax
  % map.
  connex = [];

  for ii=1:size(pairs,1)
    %fprintf('Trying to group cluster %i to cluster %i.\n',pairs(ii,1),pairs(ii,2));
    temp_map = mapA;
    keep = ((clusterA.reconAll == pairs(ii,1)) | (clusterA.reconAll == pairs(ii,2)));
    temp_map.y(~keep) = 0;
    temp_cluster = run_burstegard(temp_map,params);

    % Mark this pair as "connected" if the criterion above is satisfied.
    nPix_before = sum(temp_map.y(:) > 0);
    nPix_after = temp_cluster.nPix;
    if (nPix_after == nPix_before)
      connex = [connex; pairs(ii,:)];
    end
  end

  % Now, use the pairwise connections to determine groups of clusters.
  temp_connex = connex;
  gN = 0; % Group number.
  
  % Loop over all cluster connections.
  while (size(temp_connex,1) > 0)
    gN = gN + 1;
    groups(gN).clusts = temp_connex(1,:);
    n = 1; % Cluster number in a group.

    % Loop over clusters connected into a group.
    while (n <= length(groups(gN).clusts)) 
      temp = groups(gN).clusts;

      % Find connections relevant to this clusters.
      [r,c] = find(temp_connex == temp(n));

      % Add new connected clusters to this group.
      addC = setdiff([temp_connex(r,1)' temp_connex(r,2)'],groups(gN).clusts);
      groups(gN).clusts = [groups(gN).clusts addC];

      % Remove these lines from future groupings.
      temp_connex(r,:) = [];
      n = n+1;
    end
  end

  % Add non-grouped clusters to their own combs, accounting for the case
  % where no clusters are grouped together.
  if ~exist('groups')
    non_comb = all_clusts;
    init_length = 0;
  else
    non_comb = setdiff(all_clusts,[groups.clusts]);
    init_length = length(groups);
  end
  for jj=1:length(non_comb)
    groups(jj+init_length).clusts = non_comb(jj);
  end

  % Set up all clusters map for final cluster struct.
  clusterF.reconAll = clusterA.reconAll;

  % Regroup pixels, removing clusters with fewer pixels than nPix_req.
  clust_num = 0;
  for ii=1:length(groups)
    clust_num = clust_num + 1;
    for jj=1:length(groups(ii).clusts)
      cut = (clusterF.reconAll == groups(ii).clusts(jj));
      clusterF.reconAll(cut) = -clust_num;
    end

    % Check number of pixels.
    nPix_clust = sum(clusterF.reconAll(:) == -clust_num);
    if (nPix_clust < nPix_req)
      clusterF.reconAll(clusterF.reconAll(:) == -clust_num) = 0;
      clust_num = clust_num - 1;
    end

  end
  clusterF.reconAll = -clusterF.reconAll;

else
  % If there are no positive clusters, return the negative cluster as the
  % maximal cluster, or vice versa.  If there are no clusters at all, the
  % positive "cluster" gets returned.
  if (nPos == 0)
    clusterF = clusterN;
  elseif (nNeg == 0)
    clusterF = clusterP;
  else
    error(['Something has gone terribly wrong.  If you see this message that' ...
	   ' means that you have fewer than zero positive clusters and fewer' ...
	   ' than zero negative clusters.  Neither of these things should' ...
	   ' be possible.']);
  end

  % Check number of pixels.
  clust_nums = unique(clusterF.reconAll(:));
  clust_nums = clust_nums(2:end);
  for ii=1:length(clust_nums)
    nPix_clust = sum(clusterF.reconAll(:) == clust_nums(ii));
    if (nPix_clust < nPix_req)
      clusterF.reconAll(clusterF.reconAll(:) == clust_nums(ii)) = 0;
    end
  end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 3: Calculate cluster statistics.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Send the reconAll map to a different routine to calculate cluster
% statistics.
clusterF = calc_cluster_stats(clusterF.reconAll,mapA,params);

return;