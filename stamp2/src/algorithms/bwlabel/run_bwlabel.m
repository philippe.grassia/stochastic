function max_cluster = run_bwlabel(map, params)
% function max_cluster = run_bwlabel(map, params)
% Cluster the pixels that satisfies certain conditions e.g., SNR threshold 
% using matlab's bwlabel.m with argument 8 (8-way connected).
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.nPix -> no. of pixels in the largest cluster
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.y_gamma -> Y (GW estimator) of largest cluster
%        max_cluster.sigma_gamma -> sigma of the above estimator
%        max_cluster.reconMax -> 2D array containing 1's at the location of 
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
%        max_cluster.reconAll -> 2D array containing 1,2,3..'s at the location
%                                of pixels where each number correspond to 
%                                possible clusters that satisfy total number 
%                                of pixels contraint given in input 
%                                params.burstegard.NCN
% Written by S kandhasamy
% Contact: shivaraj@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% make the logical 2D map of pixels that pass the SNR threshold constrain and
% pass that to bwlabel; 4-way connected -> NR=1, tmetric=1, fmetric=1;
% 8-way connected -> NR=sqrt(2), tmetric=1, fmetric=1 (best suited for STAMP).
% Here max_cluster.reconAll is a 2D array containing 0,1,2,3..'s at the location
% of pixels where each number correspond to possible clusters (0 correspond to
% background pixels) that satisfy total number of pixels contraint given in 
% input params.burstegard.NCN
map.snr_bw = map.snr >= params.burstegard.pixelThreshold;
max_cluster.reconAll = bwlabel(map.snr_bw,8); % 8 way connected

% identifying clusters that satisfy NCN condition and removing other clusters
no_of_clusters = length(unique(max_cluster.reconAll(:)))-1; % all clusters
no_of_bigClusters = 0;
for ii = 1 : no_of_clusters
  cut_cluster = max_cluster.reconAll == ii;  
  no_pixels = sum(cut_cluster(:)); % cut_cluster is logical 2D array
  if(no_pixels >= params.burstegard.NCN)
    no_of_bigClusters = no_of_bigClusters + 1;
    max_cluster.reconAll(cut_cluster) = no_of_bigClusters; % reindex the cluster
  else
    max_cluster.reconAll(cut_cluster) = 0;% otherwise remove the cluster
  end
end
bigClusters = 1:no_of_bigClusters;
fprintf('Total number of big clusters: %i \n', no_of_bigClusters);

% display clustering
xvals = map.segstarttime-map.segstarttime(1);
yvals = map.f;
if params.savePlots %-----------------------------------------------
  figure
  axis([min(xvals) max(xvals) min(yvals) max(yvals)])
  set(gca, 'FontSize', 20);
  xlabel('time [sec]');
  ylabel('Frequency [Hz]');
  hold on;

  % it should cycle through markers instead of relying
  % on a huge vector.  use mod...
  markers = {'ro', 'go', 'bo', 'co', 'mo', 'ko', ...
        'rs', 'gs', 'bs', 'cs', 'ms', 'ks', ...
        'rd', 'gd', 'bd', 'cd', 'md', 'kd', ...
        'rv', 'gv', 'bv', 'cv', 'mv', 'kv', ...
        'r^', 'g^', 'b^', 'c^', 'm^', 'k^', ...
        'r<', 'g<', 'b<', 'c<', 'm<', 'k<', ...
        'r>', 'g>', 'b>', 'c>', 'm>', 'k>', ...
        'rp', 'gp', 'bp', 'cp', 'mp', 'kp', ...
        'rh', 'gh', 'bh', 'ch', 'mh', 'kh'};

  xmap = kron(xvals',ones(1,length(yvals)))';
  ymap = kron(yvals',ones(length(xvals),1))';
  for ii = 1 : length(bigClusters)
    cut_pixels = max_cluster.reconAll == bigClusters(ii);
    handle = plot(xmap(cut_pixels), ymap(cut_pixels), ...
             markers{mod(ii, length(markers))+1 } );
    set(handle, 'MarkerSize', 10);
    set(handle, 'MarkerFaceColor', get(handle, 'Color'));
  end
  title(['number of clusters = ' ...
          num2str(length(bigClusters))], 'FontSize', 20)
  hold off;
  print -depsc all_clusters_plot.eps;
  print -dpng all_clusters_plot.png;
end %-------if params.savePlots--------------------------------------

% calculate Y_Gamma, sigma_Gamma, SNR_Gamma of all big clusters
if length(bigClusters) > 0
  for ii = 1 : length(bigClusters)
    cut_pixels = max_cluster.reconAll == bigClusters(ii);
    y_gamma(ii) = ...
         sum(map.y(cut_pixels)./map.sigma(cut_pixels).^2)/sum(1./map.sigma(cut_pixels).^2);
    sigma_gamma(ii) = 1/sqrt(sum(1./map.sigma(cut_pixels).^2));
    snr_gamma(ii) = y_gamma(ii)/sigma_gamma(ii);
    no_pixels(ii) = length(map.y(cut_pixels));
  end

  % cluster with largest SNR
  [all_snr_gamma,index] = sort(snr_gamma,'descend');
  max_cluster.y_gamma = y_gamma(index(1));
  max_cluster.sigma_gamma = sigma_gamma(index(1));
  max_cluster.snr_gamma = snr_gamma(index(1));
  max_cluster.nPix = no_pixels(index(1));
  fprintf('The largest SNR is %.2f with %d number of pixel(s) \n',...
       max_cluster.snr_gamma, max_cluster.nPix);
  % make 2D logical map of largest cluster
  max_cluster.reconMax = zeros(size(map.snr));
  cut_pixels = max_cluster.reconAll == index(1); % largest cluster
  max_cluster.reconMax(cut_pixels) = 1;  

  % just plot the largest cluster
  if params.savePlots %-----------------------------------------------
    temp_map = max_cluster.reconMax;
    temp_map(cut_pixels) = map.snr(cut_pixels);
    printmap(temp_map, xvals,yvals,'time (s)','f (Hz)', 'SNR',[-5 5])
    title(['Max. SNR = ' num2str(max_cluster.snr_gamma,'%.3f')], 'FontSize', 20)
    print -depsc large_cluster_plot.eps;
    print -dpng large_cluster_plot.png;
  end
else
  fprintf('No significant cluster \n');
  max_cluster.y_gamma = 0;
  max_cluster.sigma_gamma = Inf;
  max_cluster.snr_gamma = 0;
  max_cluster.nPix = 0;
  max_cluster.reconMax = zeros(size(map.snr));
end

return;
