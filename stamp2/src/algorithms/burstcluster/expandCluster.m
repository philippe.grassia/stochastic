function [k, seedpoint] = expandCluster(ii, seedpoint, k)
% function [k, seedpoint] = expandCluster(ii, point, k)
%        Expands the cluster iteratively by going from one pixel to the other.
%
% Routine written by shivaraj kandhasamy (based on the code by Peter Kalmus)
% Contact shivaraj@lphysics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if k < (seedpoint{1}.rLimit) - 10  % Until recursion limit is exceeded
  k = k + 1;
  for j = 1:seedpoint{ii}.neighbor.n
    old = seedpoint{seedpoint{ii}.neighbor.I(j)}.ID;
    seedpoint{seedpoint{ii}.neighbor.I(j)}.ID = seedpoint{ii}.ID;
    if old == 0
      [k, seedpoint] = expandCluster(seedpoint{ii}.neighbor.I(j),seedpoint, k);
    else
      if (old > 0) && (old ~= seedpoint{ii}.ID) % If neighbor is in an older cluster
         gold = seedpoint{ii}.ID;
         for p = 1:seedpoint{1}.N
           if seedpoint{p}.ID == gold
              seedpoint{p}.ID = old;
           end
         end
      end
    end
  end
else
  seedpoint{ii}.ID = 0;
end

return;
