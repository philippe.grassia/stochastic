function seedpoint = clusterPixels(tiles, tmetric, fmetric, NR, NN) 
%function seedpoint = clusterPixels(tiles, tmetric, fmetric, NR, NN)
%
% INPUT :
%     tiles        <- preselected pixels(t,f,snr,y,sigma) with snr > threshold
%     tmetric      <- time metric of ft-map
%     fmetric      <- frequency metric of ft-map
%     NR           <- neighbor radius as determined by metric
%     NN           <- minimum number of neibhours
% OUTPUT :
%     seedpoint    -> cell structure containing the details of all clusters
%
% Routine written by shivaraj kandhasamy with modifications from E. Thrane
% Contact shivaraj@lphysics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% labelling each seed; general information available on first seed only
seedpoint{1}.NN = NN; % 
N = size(tiles,1);
seedpoint{1}.N = N; % number of tiles or seeds

% prepare for cuts
time = tiles(:,1);
freq = tiles(:,2);
idx = 1:length(freq);

% loop over tiles above threshold
for i = 1:N
  % crop map before doing pair-wise calculation
  cut1 = tmetric*abs(time-tiles(i,1))<=NR;
  cut2 = fmetric*abs(freq-tiles(i,2))<=NR;
  tiles0 = tiles(cut1&cut2,:);
  idx0 = idx(cut1&cut2);
  % if there is more than one seed in the cropped map
  if(size(tiles0,1))>1
    % perform pair-wise calculation to get a matrix of pair distances
    distance = clusterDistancestamp(tiles0, tmetric, fmetric);
    % reformat pair-wise distance vector as a matrix for easier manipulation
    distMat = squareform(distance);
    % Get index of neighbors, but not of self
    jj=find(idx0==i);
    seedpoint{i}.neighbor.I = find(distMat(:,jj)<=NR & distMat(:,jj)~=0);
    % record master index for neighbors
    seedpoint{i}.neighbor.idx = idx0(seedpoint{i}.neighbor.I);
    seedpoint{i}.neighbor.I = seedpoint{i}.neighbor.idx';
    % Get number of neighbors
    seedpoint{i}.neighbor.n = length(seedpoint{i}.neighbor.I);
  else % there are no neighbors
    seedpoint{i}.neighbor.I=[];
    seedpoint{i}.neighbor.n=0;
  end

  % Can it be a seed
  if seedpoint{i}.neighbor.n >= NN
    seedpoint{i}.ID = 0; % ID == 0 means it is a seed
    [zz, Index] = sort(tiles(seedpoint{i}.neighbor.I, 3), 'descend');
    seedpoint{i}.neighbor.I = seedpoint{i}.neighbor.I(Index);
    seedpoint{i}.neighbor.idx = seedpoint{i}.neighbor.idx(Index);
    seedpoint{i}.neighbor.I = seedpoint{i}.neighbor.idx;
  else
    seedpoint{i}.ID = NaN;
  end
end  % loop over tiles above threshold

% Index of tiles sorted along SNR
[zz,Index] = sort(tiles(:,3), 'descend');

% Set recursion limit
seedpoint{1}.rLimit = 400; % Default matlap limit is 500
set(0,'RecursionLimit', seedpoint{1}.rLimit);

% Initiate Cluster ID
clusterID = 0;
for i = 1:N
    ii = Index(i); % Pick higher significance seeds first
% If seedpoint is possible seed and not in other cluster already
    if (seedpoint{ii}.ID == 0)
        clusterID  = clusterID + 1;
        seedpoint{ii}.ID = clusterID;
        [k, seedpoint] = expandCluster(ii,seedpoint,3);
    end
end

% looking for isolated island of (or single) pixel with snr > threshold
for i = 1:N
    if ~(seedpoint{i}.ID > 0)
        clusterID  = clusterID + 1;
        seedpoint{i}.ID = clusterID;
    end
end

return;
