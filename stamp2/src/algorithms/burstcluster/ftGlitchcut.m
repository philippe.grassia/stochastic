function map = ftGlitchcut(map)
% function map = ft_glitchcut(map)
% This codes removes columns (actually replaces with zeros) in ft-map that are 
% found to be glitchy.
% INPUT :
%     map.xvals                      <- input GPS times array 
%     map.yvals                      <- input frequencies array 
%     map.snr                        <- snr map
%     map.Y                          <- Y map
%     map.sigma                      <- sigma map
% OUTPUT :
%     map.*
%
% Routine written by shivaraj kandhasamy
% Contact shivaraj@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Naive percentage cut based of number of pixels with SNR <= -2
% The number 0.07 comes from MC simulations
cut_map = map.snr <= -2;
cut_col = (sum(cut_map)/length(cut_map(:,1))) > 0.07; 
map.snr(:,cut_col) = 0;

return;
