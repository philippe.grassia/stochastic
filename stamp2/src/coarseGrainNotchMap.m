function y = coarseGrainNotchMap(x, deltaFx, flowx, deltaFy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  coarseGrainNotchMap --- returns a coarse grained map y with bin size deltaFy, from a finer map x accounting for any nothced out dat
% The notched out bins are assumed to be nans
%
%  coarseGrainNotchMap(x, deltaFx, flowx, deltaFy) returns a frequency-series structure 
%  coarse-grained to the frequency values f = flowx + deltaFy*[0:Ny-1].  
%
% Adapted from coarseGrain.m
% Sharan Banagiri banag002@umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% defining some basic parameters
Nx = size(x,1);
Ny = floor(Nx*deltaFx/deltaFy) + 1;
ty = size(x, 2);
flowy = flowx;
% error checking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check that frequency spacings and number of elements are > 0
if ( (deltaFx <= 0) | (deltaFy <= 0) | (Ny <= 0) )
  error('bad input arguments');
end

% check that freq resolution of x is finer than desired freq resolution
if ( deltaFy < deltaFx )
  error('deltaF coarse-grain < deltaF fine-grain');
elseif (deltaFy == deltaFx)
    y = x;
    return;
end

% define stop frequency for coarse-grained series
fhighx = flowx + (Nx-1)*deltaFx;
fhighy = flowy + (Ny-1)*deltaFy;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% indices for coarse-grained series
i = transpose([0:1:Ny-1]);

% calculate the low and high indices of fine-grained series 
jlow  = max(1 + floor( (flowy + (i-0.5)*deltaFy - flowx - 0.5*deltaFx)/deltaFx ), 1); 
jhigh = min(Nx, 1 + floor( (flowy + (i+0.5)*deltaFy - flowx - 0.5*deltaFx)/deltaFx )); 
index1 = jlow(1);
index2 = jhigh(end);


%indices forlocating all nans in x
nanLocate  = isnan(x);
x(nanLocate) = 0;
%initialize matrix for doing normalization adjustments for nan values
normAdjust = zeros(Ny, ty);
for m = 1:1:Ny
   for n = 1:1:ty 
    normAdjust(m, n) = sum(nanLocate(jlow(m):jhigh(m), n));
  end
end

%matrix for normalization coeffcients and make sure you dont get any infinites
normCoff = (deltaFy/deltaFx - normAdjust);
normCoff(normCoff == 0) = 1;
normCoff(1, :) = ((jhigh(1) - jlow(1) )- normAdjust(1, :));
normCoff(end, :) = ((jhigh(end) - jlow(end) )- normAdjust(end, :));


% calculate fractional contributions
fraclow  = (flowx + (jlow+0.5)*deltaFx - flowy - (i-0.5)*deltaFy)/deltaFx;
frachigh = (flowy + (i+0.5)*deltaFy - flowx - (jhigh-0.5)*deltaFx)/deltaFx;
frac1 = fraclow(1);
frac2 = frachigh(end);

% calculate coarse-grained values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sum of middle terms
% NOTE:  since this operation takes more time than anything else, i made 
% it into separate routine called sumTerms() which is then converted into
% a MEX file
jtemp = jlow+2;


for i  = 1:1:ty
	   [midsum_real(:, i),midsum_complex(:, i)] = sumTerms(...
  real(x(:, i)),imag(x(:, i)), jtemp, jhigh, length(jtemp));
end
midsum = complex(midsum_real,midsum_complex);


 %midsum = nansumTerms(x,ty, jtemp, jhigh);

[~, fraclowmat] = meshgrid(ones(ty, 1), fraclow);
[~, frachighmat] = meshgrid(ones(ty, 1), frachigh);


% calculate all but final value of y
ya = (x(jlow (1:Ny-1)+1, :).*fraclowmat (1:Ny-1, :) + ...
                        x(jhigh(1:Ny-1)+1, :).*frachighmat(1:Ny-1, :) + ...
      midsum(1:Ny-1,:)  )./(normCoff(1:Ny-1, :));

% calculate final value of y
if (jhigh(Ny) > Nx-1)
  % special case when jhigh exceeds maximum allowed value
      yb = (x(jlow(Ny)+1, :)*fraclow(Ny) + midsum(Ny, :))./normCoff(Ny, :);

else
  yb = (x(jlow (Ny)+1, :)*fraclow (Ny) + ...
                 x(jhigh(Ny)+1, :)*frachigh(Ny) + ...
                          midsum(Ny, :) )./normCoff(Ny, :);
end

% fill structure for coarsed-grained frequency series
y = [ya; yb];

y(1, :) = NaN;
y(end, :) = NaN;


% Making sure that any wide injections don't mess up the calculation of the filter function by being zeros.
widemaskIdx = (sum(y, 2) == 0);
y(widemaskIdx, :) = NaN;

 
return

