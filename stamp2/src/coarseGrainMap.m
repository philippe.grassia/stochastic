function map = coarseGrain_map(map,params)

map.deltaF = map.f(2) - map.f(1);
fprintf('Performing map coarse grain ...\n');
fprintf('Old map parameters: %.5f s x %.5f hz \n',map.segDur,map.deltaF);
fprintf('New map parameters: %.5f s x %.5f hz \n',params.coarseGrain_segDur,params.coarseGrain_deltaF);

numaveT = ceil(params.coarseGrain_segDur / map.segDur);
numaveF = ceil(params.coarseGrain_deltaF / map.deltaF);

fprintf('cc, ');
map.cc = blkproc(map.cc,[numaveF numaveT],@(x)sum(x(:)));
fprintf('sensInt, ');
map.sensInt = blkproc(map.sensInt,[numaveF numaveT],@(x)sum(x(:)));
fprintf('ccVar, ');
map.ccVar = blkproc(map.ccVar,[1 numaveT],@(x)sum(x(:)));
%fprintf('Q, ');
%map.Q = blkproc(map.Q,[numaveF numaveT],@(x)sum(x(:)));
fprintf('fft1, ');
map.fft1 = blkproc(map.fft1,[numaveF numaveT],@(x)sum(x(:)));
fprintf('fft2, ');
map.fft2 = blkproc(map.fft2,[numaveF numaveT],@(x)sum(x(:)));
fprintf('P1, ');
map.P1 = blkproc(map.P1,[numaveF numaveT],@(x)sum(x(:)));
fprintf('P2, ');
map.P2 = blkproc(map.P2,[numaveF numaveT],@(x)sum(x(:)));
fprintf('naiP1, ');
map.naiP1 = blkproc(map.naiP1,[numaveF numaveT],@(x)sum(x(:)));
fprintf('naiP2. ');
map.naiP2 = blkproc(map.naiP2,[numaveF numaveT],@(x)sum(x(:)));
map.segDur = params.coarseGrain_segDur;
map.deltaF = params.coarseGrain_deltaF;
map.segstarttime = map.segstarttime(1:numaveT:end);
map.f = map.f(1:numaveF:end);

fprintf('Coarse grain completed.\n');

