function [map] = ccSpecReadout_stamp(det1, det2, ...
  GPSTimes, source, ccSpec, ccVar, sensInt, params, pp, map)
% function [map] = ccSpecReadout_stamp(det1, det2, ...
%   GPSTimes, source, ccSpec, ccVar, sensInt, params, pp, map)
% calculates a ccStat and sigma for each frequency bin.
% Formula:
% ccStat_f = Re[ 1/gamma0 * exp(-i*p*pi*f*tau) *ccSpec/(ccVar * sensInt/2) ]
% sigma_f  = 1/gamma0 * (senseInt * deltaF)^-(1/2)
%
% for these quantities the following identities are true:
% ccStat   = sigma^2 * sum  sigma_f^-2 * ccStat_f
% sigma^-2 =           sum  sigma_f^-2
%
%  Arguments: det1,det2  -  structures for the 2 detectors
%             GPS        -  GPS time used to calculate antenna pattern
%             source   -  Nx2 matrix containing right ascension and declination
%                           for all N points to be looked at.
%             ccSpec     -  cross-correlation spectrum as calculated by
%                           calCrossCorr.m
%             ccVar    -  corresponding sigma^2
%             sensInt  -  Sensitivity integrand as calculated by calOptFilter.m
%
%  Output:    map        -  Nx2 matrix containing ccStat_f and sigma_f for
%                           each frequency and source.
%             zmap       -  Imaginary part of ccStat_f and sigma_f.
%             eps_12     -  pair efficiency for detectors 1 and 2 (H and L).
%             eps_11     -  efficiency of detector 1.
%             eps_22     -  efficiency of detector 2.
%
%  Caution: This routine does not make use of ffts - it is intended only for
%  running on a few (typically one) spot in the sky.
%
%  Routine written by Stefan Ballmer
%  Contact sballmer@ligo.mit.edu
%  modified by: Thrane, Kandhasamy, Prestegard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% define some useful variables starting with minimum frequency
flow = ccSpec.flow;
% frequency resolution
deltaF = ccSpec.deltaF;
% number of frequency bins
numFreqs = size(ccSpec.data,1);
% array of frequency values
f = flow + deltaF*transpose(0:numFreqs-1);

% M is the number of search directions in the source variable.  In stamp, M=1.
M=size(source,1);
if M>1, warning('M>1'); end;
N = length(GPSTimes);

% calculate antenna factors and time delay
g = calF(det1, det2, GPSTimes, source, params);

% default polarization phase offset is zero unless overridden below
g.eta = zeros(size(GPSTimes));

% fixAntennaFactors uses default values of F1 and F2 no matter the direction
% and time (a diagnostic tool)
if params.fixAntennaFactors, g = fixAntennaFactors(g,N); end
if params.bestAntennaFactors, g = bestAntennaFactors(g,N); end
if params.useReferenceAntennaFactors
  g = getReferenceAntennaFactors(det1, det2, GPSTimes, source, params, g);
end

% diagnostic tool sets time delay to zero no matter what
if params.NoTimeDelay, g.tau=0*g.tau; end;

% the user has requested a polarized filter
if params.doPolar||params.purePlus||params.pureCross
  [g, eps_12, eps_11, eps_22] = polarReadout(params, g);
else
  % divide by 2 since Hf is defined as sum over pol of power
  g.gamma0 = (g.F1p .* g.F2p + g.F1x .* g.F2x)/2;	     
  eps_12 = g.gamma0;
  eps_11 = (g.F1p .* g.F1p + g.F1x .* g.F1x)/2;
  eps_22 = (g.F2p .* g.F2p + g.F2x .* g.F2x)/2;
end

if params.doPolarInjection
   ccSpec = polarInjection(det1, det2, GPSTimes, source, ccSpec, params, g, map);
end

% If sensInt=0 (masked data) then we will get infinities below when we 
% invert the sensInt.  So we assign NaN.
badindex=find(sensInt.data==0);
sensInt.data(badindex)=NaN;

% re-size some of the vectors into full maps
ccVar = repmat(ccVar,numFreqs,1);
g.gamma0 = repmat(g.gamma0,numFreqs,1);
g.tau = repmat(g.tau,numFreqs,1);
g.eta = repmat(g.eta,numFreqs,1);
f = repmat(f,1,N);

% calculate y, z, and sigma maps
map.y = real( 2 ./ (ccVar .* g.gamma0) .* ...
	     exp(-i*2*pi*f .* g.tau + i*g.eta) .* ccSpec.data ./ ...
	      sensInt.data );
map.sigma = 1 ./ abs(g.gamma0) ./ sqrt(sensInt.data*deltaF);

% Swap the two half of the sigma map for good SNR estimation of monochromatic
% signals.
if params.alternative_sigma
  % find times for which there is data (not every entry is NaN)
  idx = find(any(~isnan(sensInt.data), 1));

  % if there are an odd number of segments available, one will have to be left
  % out
  nsegs = length(idx);
  if mod(nsegs,2)==1
    idx(end) = [];
  end

  % perform the swap using a temporary variable
  tmp = map.sigma;
  % the first is the last
  tmp(:,idx(1:end/2)) = map.sigma(:,idx(end/2+1:end));
  % and the last is the first
  tmp(:,idx(end/2+1:end)) = map.sigma(:,idx(1:end/2));
  % port values of tmp to map.sigma
  map.sigma = tmp;
end

% Z-map for glitch rejection and double-checks uses imaginary part of CS
map.z = imag( 2 ./ (ccVar .* g.gamma0) .* ...
	     exp(-i*2*pi*f .* g.tau + i*g.eta) .* ccSpec.data ./ ...
	      sensInt.data );

% remove w^4 factor for correct normalization of sigma(f)
map.sigma = map.sigma/sqrt(pp.w1w2squaredbar/pp.w1w2bar^2);

% create efficiency maps
map.eps_11 = repmat(eps_11,size(map.y,1),1);
map.eps_12 = repmat(eps_12,size(map.y,1),1);
map.eps_22 = repmat(eps_22,size(map.y,1),1);

% record other data in map struct
map.g = g;
map.ccVar = ccVar;
map.ccSpec = ccSpec;
map.sensInt = sensInt;
% EHT on Dec 5, 2014: I propose the following changes:
%map.ccSpec = ccSpec.data;
%map.sensInt = sensInt.data;
map.pp = pp;
map.y_f = f;

return
