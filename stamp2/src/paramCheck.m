function params = paramCheck(params)
% function paramCheck(params)
% E Thrane.  Checks the params struct for self-consistency and completeness.
% See also: stampDefaults.m
%------------------------------------------------------------------------------
% Check basic options
%------------------------------------------------------------------------------
% check that the interferometer pair has been defined.
try
  params.ifo1;
  params.ifo2;
catch
  error('Please specify params.ifo1 and params.ifo2');
end

% check that the freqency range has been specified.
try
  params.fmin;
  params.fmax;
catch
  error('Please specify the frequency range: params.fmin and params.fmax');
end
  
% check that the search direction has been been specified and supplied with
% an appropriate range.
try
  params.ra;
  params.dec;
catch % If either one is not defined, give an error.
  error('Please specify the search direction: params.ra and params.dec');
end

if ~isnumeric(params.ra)
    params.ra = str2num(params.ra);
end

if ~isnumeric(params.dec)
params.dec = str2num(params.dec);
end


if sum((params.ra<0) | (params.ra>=24))
  error('params.ra must be between 0 and 24.');
elseif sum((params.dec<-90) | (params.dec>90))
  error('params.dec must be between -90 and +90.');
end

% check the random seed and set using the clock if necesssary
try
  % if the seed is greater than zero, the user-specified value is used
  if(params.seed>0)
    fprintf('Setting seed to %i.\n', params.seed);
  % if the seed is less than zero, it is set with the clock
  else
    params.seed = sum(clock*100) + params.startGPS;
    fprintf('Setting seed using clock.\n');
  end
% if the seed is not defined at all, we enter this catch statement
catch
  fprintf('Seed not defined: setting seed using the clock.\n');
  % add clock time to seed taking care to avoid exceed 2^32-1 = the seed limit
  params.seed = sum(clock*100) + params.startGPS;
end
% separately initialize randn and rand with the seed
randn('state', params.seed);
rand('state', params.seed);
% make sure the seed value is not too large
if params.seed>=2^32-1
  error('The random seed cannot exceed 2^32-1');
end

% set physical variables; do not allow these to be set by the user
params.G = 6.673e-11;       % gravitational constant in m^3/kg/s^2
params.c = 299792458;       % speed of light in m/s
params.ErgsPerSqCM = 1000;  % convert Joules/m^2 to ergs/cm^2

% Check for old glitch params.
% If set, switch to new glitch params struct and issue warning.
% Then remove old glitch params.
if isfield(params,'glitchCut')
  warning(['params.glitchCut is obsolete, please use params.glitch.doCut' ...
	   ' in the future.']);
  params.glitch.doCut = params.glitchCut;
  params = rmfield(params,'glitchCut');
end
if isfield(params,'doCoincidentCut')
  warning(['params.doCoincidentCut is obsolete, please use' ...
	   ' params.glitch.doCoincidentCut in the future.']);
  params.glitch.doCoincidentCut = params.doCoincidentCut;
  params = rmfield(params,'doCoincidentCut');
end

% check that frequency masks and glitch/DQ flags have been defined
try
  params.doStampFreqMask;
  params.StampFreqsToRemove;
  params.glitch.doCut;
  params.glitch.doCoincidentCut;
  params.glitch.numBands;
catch
  error('Data cleaning options missing from the params struct.');
end

% make sure params.glitch.doCut has a value of true or false
% (unlike old code, in which it could take on integer values
if ~(params.glitch.doCut==true || params.glitch.doCut==false)
  error('params.glitch.doCut must be true or false.');
end

% coarse grain the map
try
  params.doCoarseGrainMap;
catch
  params.doCoarseGrainMap = 0;
end

% coarse grain the map
try
  params.coarseGrainNotch;
catch
  params.coarseGrainNotch = 0;
end

% check that output and plotting options are defined
try
  params.debug;
  params.outputfilename;
  params.saveMat;
  params.savePlots;
  if params.savePlots
    try
      params.yMapScale;
    catch
      error('yMapScale must be defined when savePlots is true.');
    end
  end
catch
  error('Output options missing from the params struct.');
end

if params.saveMat
   if ~isfield(params, 'ftmapdir')
       params.ftmapdir = filepath;
   end
end

% background study flag used to stitch together contiguous stretches of data
try
  params.bkndstudy;
catch
  error('bkndstudy flag not defined');
end

% GPU options
try
  params.doGPU;
catch
  params.doGPU = false;
end

% fixedSkyPosition
try
  params.fixedSkyPosition;
catch
  params.fixedSkyPosition = false;
end

% gpu precision
try
  params.gpu.precision;
catch
  params.gpu.precision = 'double';
end

% if no gpu device is available, default to CPUs
%fprintf('GPU -> CPU catch is commented out for development.\n');
%if params.doGPU
%  try
%    gpuArray.rand(1, 1);
%  catch
%    fprintf('WARNING: GPU error detected...setting params.doGPU=false\n');
%    params.doGPU = false;
%  end
%end

%------------------------------------------------------------------------------
% Search algorithms
%------------------------------------------------------------------------------
% check if a flag is declared for all search options
try
  % definitely supported in STAMP 2
  params.doBurstegard;
  params.doZebragard;
  % the following may not be supported in STAMP 2
  params.doRadon;
  params.doRadonReconstruction;
  params.doBoxSearch;
  params.doClusterSearch;
  params.doLH;
  params.doStochtrack;
catch
  error('Search options missing from the params struct.');
end

% if running zebragard, make sure burstegard is activated.
if (params.doZebragard && ~params.doBurstegard)
  params.doBurstegard = true;
end

% make sure stochtrack has everything it needs
if params.doStochtrack
  try
    params.stochtrack.T;
    params.stochtrack.F;
    params.stochtrack.mindur;
    params.stochtrack.demo;
    params.stochtrack.norm;
    params.stochtrack.doSeed;
  catch
    error('missing stochtrack parameters.');
  end
end

% parallel options
params = initParallel(params);

% stochtrack options
if params.doStochtrack
  % the original/standard version of stocktrack uses Bezier curves
  try
    params.stochtrack.doBezier;
  catch
    params.stochtrack.doBezier = true;
  end

  % Boris Goncharov: exception for doExponential
  try
    params.stochtrack.doExponential;
  catch
    params.stochtrack.doExponential = false;
  end

  try
    params.stochtrack.doAntiChirp;
  catch
    params.stochtrack.doAntiChirp = false;
  end

  try
    params.stochtrack.doMSmagnetar;
  catch
    params.stochtrack.doMSmagnetar = false;
  end

  try
    params.stochtrack.doBurstegard;
  catch
    params.stochtrack.doBurstegard = false;
  end

  % option to flip the f and t axes before running stochtrack
  try
    params.stochtrack.doTranspose;
  catch
    params.stochtrack.doTranspose = false;
  end

  % stochsky: all-sky stochtrack assigns a random direction to each template
  try
    params.stochtrack.stochsky;
  catch
     params.stochtrack.stochsky = false;
  end

  % use CBC shaped templates in stochtrack
  try
    params.stochtrack.doCBC;
  catch
    params.stochtrack.doCBC = false;
    % set cbc.allsky false to avoid error in stochtrack
     params.stochtrack.cbc.allsky=false;
  end

  % use r-mode templates in stochtrack
  try
    params.stochtrack.doRModes;
  catch
    params.stochtrack.doRModes = false;
  end

  % eccentric binary templates
  try
    params.stochtrack.doECBC;
  catch
    params.stochtrack.doECBC = false;
  end

  % eccentric binary templates
  try
    params.stochtrack.extra_pixels;
  catch
    params.stochtrack.extra_pixels = 0;
  end

  % option to draw stochtrack Bezier control points from a ramp distribution 
  % instead of a uniform distribution.  results from preliminary studies are 
  % mixed.  sensitivity improves/worsens depending on the waveform.  the 
  % motivation is to emphasize high-frequency tracks for all-sky searches.
  try
    params.stochtrack.doRamp;
  catch
    params.stochtrack.doRamp = false;
  end

  try
    params.stochtrack.saveMat;
  catch
    params.stochtrack.saveMat = false;
  end

  try
    params.stochtrack.doSeed;
  catch
    params.stochtrack.doSeed = false;
  end

  % make sure stochtrack CBC has everything it needs
  if (params.stochtrack.doCBC) || (params.stochtrack.doECBC)
    % make sure the user has defined a mass range
    try
      params.stochtrack.cbc.min_mass;
      params.stochtrack.cbc.max_mass;
    catch
      error('missing stochtrack cbc parameters.');
    end
    % make sure min mass < max mass
    if params.stochtrack.cbc.min_mass>params.stochtrack.cbc.max_mass
      error('stochtrack.cbc.min_mass>params.stochtrack.cbc.max_mass = no.');
    end
    % make sure the masses aren't too big
    if params.stochtrack.cbc.max_mass>3
      fprintf('params.stochtrack.cbc.max_mass must be < 3\n');
      fprintf('Comment out this err message in paramCheck at your own risk.\n')
      warning('params.stochtrack.cbc.max_mass out of range.');
    end
    % minstop determines the minimum allowable chirp time.  The chirp *must* 
    % take place somewhere on the ft-map.  However, part of the beginning of 
    % the track may be cut off depending on the value of minstop and the chirp
    % mass.  The default value is chosen so that the 3-on-3 systems (lasting 
    % ~100s) produce tracks that are completely contained within a map.  Lower-
    % mass systems can begin before t=0.  We suggest: use the default value 
    % unless you understand meaning of this variable.  Contact M Coughlin for 
    % questions.  A note from MWC:
    % The calculation was just length(find(foft > 0)) - 2 length(xvals_extra).
    minstop_default = 141;
    try
      params.stochtrack.cbc.minstop;
    catch
      params.stochtrack.cbc.minstop = minstop_default;
    end
    % make sure that the map is big enough to fit the smallest necessary track
    if params.stochtrack.cbc.minstop>=minstop_default && ...
      params.endGPS-params.startGPS < 100
      warning('>=100s map required for doCBC');
    end
    % make sure the input mats are of the expected form
    if params.segmentDuration==1 && params.deltaF==1
      % do nothing: the parameters are set as expected
    else
      warning('For doCBC, please use segmentDuration=1 and deltaF=1.');
    end
    % option of running all-sky version of doCBC
    try
      params.stochtrack.cbc.allsky;
      fprintf('cbc.allsky==%i\n',params.stochtrack.cbc.allsky);
    catch
      params.stochtrack.cbc.allsky = false;
    end
    % if all-sky version is done, make sure the number of search directions is
    % defined
    if params.stochtrack.cbc.allsky==true 
      try
        params.stochtrack.cbc.ndirs;
      catch
        % default for LHO-LLO analysis; LV probably needs a larger value
        params.stochtrack.cbc.ndirs = 40;
        if ~(strcmp(params.ifo1, 'H1') && strcmp(params.ifo2, 'L1'))
          fprintf('paramCheck WARNING: ndirs=%i tuned for H1L1\n', ...
            params.stochtrack.cbc.ndirs);
        end
      end
      % and also check if params.stochtrack.norm is set appropriately since 
      % doCBC all-sky is only coded for params.stochtrack.norm='npix'
      if ~(strcmp(params.stochtrack.norm, 'npix') )
        warning('require params.stochtrack.norm=npix for cbc.allsky');
      end
      % and check that stochsky isnt on since that interferes with cbc.allsky
      if params.stochtrack.stochsky
        error('stochsky and cbc.allsky are mutally exclusive run options');
      end
    end
    % #########################################################################
  end

  % the doDF option uses CBC templates with >2 frequency bins per segment.
  % we showed that it's more sensitive than doDF=false, and comparably fast.
  try
    params.stochtrack.doDF;
  catch
    params.stochtrack.doDF = true;
  end

  % To be used in conjunction with trigger follow-ups
  try
    params.stochtrack.triggerGPS;
  catch
    params.stochtrack.triggerGPS = -1;
  end

  % option to remove the brightest pixels to reduce background
  try
    params.stochtrack.doPixelCut;
  catch
    params.stochtrack.doPixelCut = false;
  end 

  % option to remove burstegard clusters: no evidence that this helps
  try
    params.stochtrack.doBurstegardCut;
  catch
    params.stochtrack.doBurstegardCut = false;
  end

  % set NoTimeDelay=true to measure stochsky deltaT about zero
  if params.stochtrack.stochsky || params.stochtrack.cbc.allsky
    fprintf('Setting params.NoTimeDelay=true for stochsky.\n');
    params.NoTimeDelay=true;
  end

  % set GPU seeds for stochtrack
  if params.doGPU
    gpu_stream = parallel.gpu.RandStream('CombRecursive', 'Seed', params.seed);
    parallel.gpu.RandStream.setGlobalStream(gpu_stream);
  end

  % Bezier, doCBC, doECBC, and doRModes are all mutually exclusive.  The user
  % must pick no more (and no less) than one option when stochtrack is on.
  if sum([params.stochtrack.doBezier,params.stochtrack.doCBC,...
          params.stochtrack.doECBC, params.stochtrack.doRModes,...
          params.stochtrack.doExponential,params.stochtrack.doBurstegard,...
	  params.stochtrack.doAntiChirp]) > 1
    error('Only choose one stochtrack type');
  elseif sum([params.stochtrack.doBezier,params.stochtrack.doCBC,...
          params.stochtrack.doECBC, params.stochtrack.doRModes,...
          params.stochtrack.doExponential,params.stochtrack.doBurstegard,...
	  params.stochtrack.doAntiChirp]) == 0
    error('Choose a stochtrack type');
  end

  % optional time-dependent weighting for very-long (compressed) maps
  % note, that this option only runs if params.stochtrack.stochsky==true
  try
    params.stochtrack.vlong;
  catch
    params.stochtrack.vlong = false;
  end

  % Bezier pixel-based track generation
  try
    params.stochtrack.doBezierPixels;
  catch
    params.stochtrack.doBezierPixels = false;
  end

  try
    params.stochtrack.doMaxbandPercentage;
  catch
    params.stochtrack.doMaxbandPercentage = false;
  end

  % Lonetrack
  try
    params.stochtrack.lonetrack;
  catch
    params.stochtrack.lonetrack = false;
  end

end

% make sure burstegard has everything it needs
if params.doBurstegard
  try
    params.burstegard.NCN;
    params.burstegard.NR;
    params.burstegard.pixelThreshold;
    params.burstegard.tmetric;
    params.burstegard.fmetric;
    params.burstegard.debug;
    params.burstegard.weightedSNR;
    params.crunch_map;
  catch
    error('missing burstegard parameters.');
  end
end

% make sure that the box search has all the variables that it needs
if params.doBoxSearch
  try
    params.box;
    if params.box.freq_width<=0
      error('params.box.freq_width must be > 0.');
    end
    if params.box.time_width<=0
      error('params.box.time_width must be > 0.');
    end
    params.box.debug;
  catch
    error('missing box search parameters');
  end
end

% make sure (burstcluster) clustering algorithm has everything it needs
if params.doClusterSearch
  warning('Are you sure you want to use burstCluster and not burstegard?');
  try
    params.cluster.doCombineCluster; % cluster the clusters
    params.cluster.NN;  % minimum number of neighbours
    params.cluster.NR;  % neighbor radius as determined by metric
    params.cluster.NCN; % minimum number of pixels in a cluster
    params.cluster.NCR; % distance between neighbouring clusters
    params.cluster.pixelThreshold;
    params.cluster.tmetric;
    params.cluster.fmetric;
  catch
    error('You are missing clustering parameters. Please see clusterDefaluts.')
  end
  % check if parameters are sensibly defined
  if params.cluster.NCN <= params.cluster.NN
    warning('Typically NCN>NN so superclusters are bigger than clusters.');
  end
  if params.cluster.NCR <= params.cluster.NR
    warning('Typically NCR>NR so superclusters are bigger than clusters.');
  end
end

%------------------------------------------------------------------------------
% All-sky options
%------------------------------------------------------------------------------
% check if all-sky options are turned on
try
  params.skypatch;
  params.fastring;
catch
  error('All-sky options missing from the params struct.');
end

% make sure thetamax and dtheta are defined for the skypatch option
if params.skypatch
  try
    params.thetamax;
    params.dtheta;
  catch
    error(['skypatch requires additional fields: params.thetamax and theta.']);
  end
end

%------------------------------------------------------------------------------
% injection options
%------------------------------------------------------------------------------
% check if power injection flags are on
try
  params.powerinj;
  params.inj_trials;
catch
  error('Injection options missing from the params struct.');
end

% if there is an injection requested, make sure all parameters are defined
if params.powerinj
  % make sure injection file is defined
  try
    params.injfile;
  catch
    error('powerinj requires the definition of injfile.');
  end
  % injection time always the same (default is no)
  try
    params.fixedInjectionTime;
  catch
    % for backward compatibility, the default option is no
    params.fixedInjectionTime = false; 
    fprintf('fixedInjectionTime not defined; set false\n');
  end
  % fixed injection time requires an additional field specifying GPS time
  if params.fixedInjectionTime
    try
      params.fixedInjectionTimeGPS;
    catch
      error('fixedInjectionTime requires fixedInjectionTimeGPS');
    end
  end
  % make sure alpha_min and alpha_max are defined (min/max inj strength)
  try
    params.alpha_min;
    params.alpha_max;
  catch
    error('Specify params.alpha_min and alpha_max for params.powerinj.');
  end
  % alpha is an array of injection scale factors: how many values of alpha do
  % we want?
  if params.alpha_min==params.alpha_max
    % alpha_n is the number of injection trials
    params.alpha_n=1;
    params.alpha = params.alpha_min;
  else
    % check to make sure that alpha_min alpha_max are consistent
    if params.alpha_min<=0 || params.alpha_max<params.alpha_min
      error('params.alpha_min and alpha_max are inconsistent');
    end
    % if alpha_min and alpha_max are different, then we call get_alpha to
    % obtain an array of values for params.alpha
    params = get_alpha(params);
  end
  try 
    params.increment;
  catch
    % increment percentage between alpha values
    error('Specify params.increment');
  end
  try
    params.incrementType;
  catch
    % increment percentage between alpha values
    error('Specify params.incrementType (alpha or distance)');
  end

  % check if fixedLocationDirection is defined
  try
    params.fixedInjectionLocation;
  catch
    error('fixedInjectionLocation flag required if injections on.');
  end
  % check that a sky position is specified when fixedInjectionLocation is on
  if params.fixedInjectionLocation
    try
      params.inj_ra;
      params.inj_dec;
    catch
      error('inj_ra and inj_dec required for params.fixedInjectionLocation');
    end
  end
else
  % even if there is no injection, then there is still one injection strength
  % (zero) for the purposes of book-keeping
  params.alpha_n = 1;
end

%------------------------------------------------------------------------------
% parameter estimation options
%------------------------------------------------------------------------------
try
  params.doPE;
catch
  error('Parameter estimation options missing from the params struct.');
end

%------------------------------------------------------------------------------
% diagnostic options and other less common options
%------------------------------------------------------------------------------
% check if non-standard diagnostic flags are on
try
  params.phaseScramble;
  params.pixelScramble;
  params.fixAntennaFactors;
  params.bestAntennaFactors;
  params.NoTimeDelay;
  params.doRadiometer;
  params.doPolar;
  params.purePlus;
  params.pureCross;
  params.stamp_pem;
  params.circular;
catch
  error('diagnostic/other options missing from params struct.');
end

% check for mutally exclusive requests with polarization parameters
if params.doPolar+params.purePlus+params.pureCross+params.circular>1
  error('Mutually exclusive polarization requests.');
end

% make sure iota and psi are defined for the doPolar option
if params.doPolar
  warning('params.doPolar is not reviewed code.');
  try
    if params.psi>=2*pi
      error('psi must be in radians');
    end
    if params.iota>pi
      error('iota must be in radians');
    end
  catch
    error('doPolar requires iota and psi.');
  end
end

try
   params.doPolarInjection;
catch
   params.doPolarInjection = false;
end

% option to override gps times
try
  params.override_gps;
catch
  params.override_gps=0;
end

% check alt sigma option
try
  params.alternative_sigma;
catch
  error('alternative_sigma option missing from the param structure')
end

% if any non-standard parameters are set, the diagnostic flag is set
if (params.phaseScramble || params.pixelScramble ...
  || params.fixAntennaFactors || params.NoTimeDelay || params.doPolar ...
  || params.purePlus || params.pureCross || params.stamp_pem || ...
  params.bkndstudy || params.bestAntennaFactors)
  params.diagnostic=true;
else
  params.diagnostic=false;
end

% record matlab version for records
params.version = version;

% if calculating antenna factors for reference time, check that reference time is given
try
  params.useReferenceAntennaFactors;
catch
  params.useReferenceAntennaFactors = false;
end
if params.useReferenceAntennaFactors
  try
    params.referenceGPSTime;
    fprintf('Using reference GPS time to set antenna factors and detector time delay.\n')
  catch
    error('Attempted to use empty reference GPS time to set antenna factors and detector time delay')
  end
end

% mask a cluster from a .mat file
% if masking a cluster, check that file path has been defined
try
  params.maskCluster;
catch
  params.maskCluster = false;
end
if params.maskCluster
  try
    params.clusterFile;
  catch
    error('path to .mat file for maskCluster option not defined')
  end
end

% Check if singletrack parameters are set and set to defaul of singletrack.doSingletrack to false if not
if params.doStochtrack
  if (~isfield(params.stochtrack, 'singletrack')) || (~isfield(params.stochtrack.singletrack, 'doSingletrack'))
    params.stochtrack.singletrack.doSingletrack = false;
  else
    % Check if all-sky mode set. Default to false
    if (~isfield(params.stochtrack.singletrack, 'doAllsky'))
      params.stochtrack.singletrack.doAllsky = false;
    end
    % Check if positive SNR mode for single track is defined and set to false if not (these non-standard modes are usually meant for diagnostics, but can be used to constrain the sensitivity of the search in certain cases where different polarizations favor different signs of SNR)
    if (~isfield(params.stochtrack.singletrack, 'posSNRmode'))
      params.stochtrack.singletrack.posSNRmode = false;
    end
    % Check if negative SNR mode for single track is defined and set to false if not
    if (~isfield(params.stochtrack.singletrack, 'negSNRmode'))
      params.stochtrack.singletrack.negSNRmode = false;
    end
  end
end

% Check if flag to stop use of imaginary filter in csnr_v3 exists and default to false
if (~isfield(params, 'no_imaginary_polarized_filter'))
  params.no_imaginary_polarized_filter=false;
end

%------------------------------------------------------------------------------
% Options related to VLT
%------------------------------------------------------------------------------
try
   params.doVLT;
catch
  params.doVLT = 0;
  params.psdtype = 'default';
  params.nbThresh = 700;
end

try
   params.nbThresh;
catch
  params.nbThresh = 700;
end

return
