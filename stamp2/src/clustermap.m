  function stoch_out = clustermap(params, startGPS, endGPS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function stoch_out = clustermapmap2(params, startGPS, endGPS)
% E.g., 
%   params = stampDefaults;
%   clustermap(params, 816088197, 816089507);
% output (depending on what is requested in params struct):
%   diagnostic outputfile: map.mat
%   diagnostic plots: snr.eps, y_map.eps, sig_map.eps + .png versions
%   additional output if requested by search algorithm plug-ins
%   stoch_out: a struct containing information about injections and/or search
%     algorithms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% E. Thrane, S. Kandhasamy, S. Dorsher, S. Giampanis, M. Coughlin, 
% T. Prestegard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;

% make sure start and end GPS are defined 
if(nargin == 3)
  params.startGPS = strassign(startGPS);
  params.endGPS = strassign(endGPS);
else
  error('start and end GPS must be specified.');
end

% save psd in stoch_out
psd_generated=0;

% locate or create mats for the requested data
% if mat files containing ft-information already available, use that
% (for backward compatibility)
if params.anteproc.loadFiles
  if params.matavailable
    % Find and load anteproc'd .mat files.
    [matlist,params] = find_anteproc_mats(params);
    [map1, params1] = load_anteproc_mats(matlist,params,1);
    [map2, params2] = load_anteproc_mats(matlist,params,2);
  else
    % Pre-process data on-the-fly with anteproc.
    [map1, params1] = createmats_anteproc(params,1);
    [map2, params2] = createmats_anteproc(params,2);
  end

  if params.doTFNotch
    map1 = TFNotch(map1,params1);
    map2 = TFNotch(map2,params2);
  end

  % Check data and parameters.
  [params, map1, map2] = check_anteproc_data(map1,map2,params1, ...
					     params2,params);

  % Set segstarttimes to be equal (may need to be adjusted
  % due to time-shifts).
  map2.segstarttime = map1.segstarttime;

  % Combine data (calculate cross-correlation).
  [map, params, pp] = combine_anteproc_mats(map1,map2,params);

  % save one psd for each ifo for injection matched filtering computation
  if params1.stampinj & params2.stampinj || params.stampmdc
    psd_generated=1;
    psd1=[map1.f map1.P(:,round(size(map1.P,2)/2))];
    psd2=[map2.f map2.P(:,round(size(map1.P,2)/2))];
  end

  % Clear variables that aren't neeeded anymore.
  clear map1 map2 params1 params2;
else
  if params.matavailable
    % Load normal pre-processed STAMP .mat files (from preproc).
    [matlist, params] = findmats(params);
    [map, params, pp] = loadmats(matlist, params);
  else
    % Pre-process data on-the-fly with preproc.
    [map, params, pp] = createmats(params);
  end

  if params.doTFNotch;map = TFNotch(map,params);end
end

% check values of params for self-consistency
params = paramCheck(params);

% check to make there is meaningful data to analyze
if any(~isnan(map.cc(:))) == 0 
  error('map.cc is filled with nothing but NaN; exiting.');
end

% if we perform a background study, remove missing data; useful when using 
% *.mat files from preproc
if params.bkndstudy
  map = bkndstudy(map, params);
end

% apply mask
if params.doStampFreqMask
  map = maskmaps(map, params);
end

if params.doCoarseGrainMap
  map = coarseGrainMap(map,params);
end

if params.coarseGrainNotch
  % Making a notch with specified # of bins to notch
  map = notchmap(map, params);

  % calculating coarse graining parameters
  numaveT = ceil(params.coarseGrain_segDur / map.segDur);
  numaveF = ceil(params.coarseGrain_deltaF / map.deltaF);

  map.deltaF = map.f(2) - map.f(1);
  fprintf('Performing map coarse grain of a notched map ...\n');
  fprintf('Old map parameters: %.5f s x %.5f hz \n',map.segDur,map.deltaF);
  fprintf('New map parameters: %.5f s x %.5f hz \n',params.coarseGrain_segDur,params.coarseGrain_deltaF);

  % doing coarse graining - this can account for normalization changes when notches are done
  map.cc = coarseGrainNotchMap(map.cc,map.deltaF,map.f(1), params.coarseGrain_deltaF);
  map.P1 = coarseGrainNotchMap(map.P1, map.deltaF,map.f(1), params.coarseGrain_deltaF);
  map.P2 = coarseGrainNotchMap(map.P2,map.deltaF,map.f(1), params.coarseGrain_deltaF);
  map.fft1 = coarseGrainNotchMap(map.fft1,map.deltaF,map.f(1), params.coarseGrain_deltaF);
  map.fft2 = coarseGrainNotchMap(map.fft2,map.deltaF,map.f(1), params.coarseGrain_deltaF);
  map.naiP1 = coarseGrainNotchMap(map.naiP1,map.deltaF,map.f(1), params.coarseGrain_deltaF);
  map.naiP2 = coarseGrainNotchMap(map.naiP2,map.deltaF,map.f(1), params.coarseGrain_deltaF);
  map.sensInt = coarseGrainNotchMap(map.sensInt,map.deltaF,map.f(1), params.coarseGrain_deltaF);

  map.segDur = params.coarseGrain_segDur;
  map.deltaF = params.coarseGrain_deltaF;
  map.segstarttime = map.segstarttime(1:numaveT:end);
  map.f = map.f(1:numaveF:end);
end


% override gps times
if params.override_gps~=0
  fprintf('\n----------------OVERRIDING GPS TIMES!----------------\n\n');
  gps0 = params.override_gps;
  map.segstarttime = map.segstarttime - map.segstarttime(1) + gps0;
  endGPS = endGPS + (gps0 - startGPS);
  startGPS = gps0;
end

% initialize ccSpec struct and other radiometer variables
% eht: Note that cc and ccSpec are not identical in preproc--they differ by a 
% factor of Q.  The following definition of ccSpec yields the correct estimator
% for map.y downstream, but use caution comparing with variables in preproc.
ccSpec.data = map.cc;
ccSpec.flow = params.fmin;
ccSpec.deltaF = pp.deltaF;
sensInt.flow = params.fmin;
sensInt.deltaF = pp.deltaF;
sensInt.data = map.sensInt;
ccVar = map.ccVar;
midGPSTimes = map.segstarttime + params.segmentDuration/2;
[det1 det2] = ifo2dets(params);

% Adding det1 and det2 to map struct
map.det1 = det1;
map.det2 = det2;


% get sky location
if params.skypatch
  source = getskylocation(params);
else
  source = [params.ra params.dec];
end

% the number of searches executed
ss=0;

% loop over search directions
for kk=1:size(source,1)
  % initialize ft-map struct
  map = freshmap(map);

  % run the radiometer; calculate ft-maps
  [map] = ccSpecReadout_stamp(det1, det2, ... 
    midGPSTimes, source(kk,:), ccSpec, ccVar, sensInt, params, pp, map);

  if params.alternative_sigma
    [map,params] = altsigma(map,params);
  end

  % define SNR map
  map.snr = map.y ./ map.sigma;
  map.snrz = map.z ./map.sigma;

  if params.glitchinj
     map = glitchinj(params,map);
  end

  % mask cluster from .mat file
  if params.maskCluster
    map = maskCluster(params, map);
  end

  % initialize injection struct
  inj.init = [];

  % loop over injection trials
  % Note: if no injections are performed, then alpha_n is set to one and
  % inj_trials is set to one.
  for tt=1:params.inj_trials*params.alpha_n
%    fprintf('trial %i/%i\n', tt, params.inj_trials*params.alpha_n);
    % add injection if requested
    if params.powerinj
       [map0, inj] = powerinj3(params, map, source(kk,:), tt, inj);    
    else
       map0 = map;
    end

    % calculate Xi stastic
    map0 = calXi(params, map0);   

    % apply glitch cut if requested in params struct
    % cutcols records the ft-map columns (segments) that are cut
    [map0, stoch_out.cutcols{kk}, stoch_out.detcuts{kk}, stoch_out.xi_frac{kk}] = ...
    	glitchCut(params, map0);

    % special variables for use in PEM studies
    if params.stamp_pem
       map0 = stamp_pem_map_cuts(params,map0);
    end
    % save mat file(s) -- one for each search direction
    if params.saveMat
      if params.stamp_pem
        map_pem.fft1 = map0.fft1;
	map_pem.fft2 = map0.fft2;
	map_pem.f   = map0.f;
	map_pem.snr  = map0.snr;
	save([params.outputfilename '_' num2str(kk) '.mat'], ...
           'map_pem','params','pp');
      else
	save([params.ftmapdir '_' num2str(kk) '.mat'], ...
	     'map0','params','pp'); 
      end
    end

    % broadband radiometer comparison
    if params.doRadiometer
      y_rad = sum(map0.y.*map0.sigma.^-2, 1) ./ sum(map0.sigma.^-2, 1);
      y_sig = sum(map0.sigma.^-2, 1).^-0.5;
      fprintf('radiometer=\n');
      fprintf('%1.4e +/- %1.4e\n', y_rad, y_sig);
    end

    if params.doStochtrack && params.stochtrack.lonetrack
      % We do not need map ... only use map0 from here on.
      clear map;
    end
    % perform clustering algorithms if requested  
    [stoch_out, ss] = searches(map0, params, kk, ss, stoch_out, pp);
 
    % Add search direction to map struct.
    map0.source = source(kk,:);

    % Make sure max cluster is saved; compatible with searches that use a
    % cluster struct (burstegard, BurstCluster, Box search).
    if (params.doBurstegard | params.doBoxSearch | params.doClusterSearch ...
      | params.doStochtrack)
      if ~exist('max_cluster')
        max_cluster.snr_gamma = -1;
      end
      if (max_cluster.snr_gamma < stoch_out.cluster.snr_gamma)
        max_cluster = stoch_out.cluster;
	max_map = map0;
      elseif params.doStochtrack && params.stochtrack.singletrack.doSingletrack
        max_cluster = stoch_out.cluster;
        max_map = map0;
      end
    end

    if params.doPE
      stoch_out = stamp_pe(map0, params, stoch_out);
    end

  end
end

% Save 'max' structs in stoch_out struct.
% Also set 'map' to be 'max_map' for plotting purposes.
if (params.doBurstegard | params.doBoxSearch | params.doClusterSearch | ...
  params.doStochtrack)
  map0 = max_map;
  % Print search result summary to screen.
  fprintf(['\nSearch results:\nMax. SNR = %.3f, found at (ra,dec) =' ...
    ' (%.2f h, %.2f deg.)\n'], max(stoch_out.max_SNR), map0.source(1), ...
	  map0.source(2));
  stoch_out.cluster = max_cluster;
  stoch_out.cluster.source = max_map.source;
end
if params.returnMap
  stoch_out.map = map0;
end

% Plots for search direction corresponding to maximum SNR.
if params.savePlots
  saveplots(params, map0);
  if params.doBurstegard
    burstegard_plot(max_cluster, map0, params);
  end
end

% record overall map sensitivity
stoch_out.ccSigBar = sqrt(mean(ccVar(~isnan(ccVar))));

% store param values
stoch_out.params = params;

% store source locations %% Useful for all-sky analyses
stoch_out.sources = source;

% warn the user if diagnostic tools are on
if params.diagnostic
  fprintf('CAUTION: diagnostic tools on.\n'); 
end

% close all open plots
close all;
fprintf('Elapsed time = %f seconds.\n', toc);

if psd_generated==1
  stoch_out.psd1=psd1;
  stoch_out.psd2=psd2;
end

return
