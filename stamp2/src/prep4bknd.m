function prep4bknd(params)
% function prep4bknd(params)
% E. Thrane: create a "pseudo job file" containing jobs with a fixed duration
% of usable data by stitching together periods split by missing data.
%
% This code is similar to mkbckjobs.m.  Here is a description of the
% difference:
% > Does this code do anything different from prep4bknd.m?
%  Both mkbckjobs.m and prep4bknd.m produce a job file which when ran with
% clustermap.m, for each job, will produce maps with same number of time
% segments. But the way they produce that file is different and the new
% job files they produce themselves are different.
% (i) prep4bknd.m relies on findmats.m which in turn relies on the
% availability of mat files. So a complete set of mat files from already
% run preproc.m is a necessary condition for running prep4bknd.m while new
% mkbckjobs.m takes only the initial job file and produces a new job file
% (one of the main purpose of new clustermap -> preproc is not to produce
% any mat files).
%  (ii) The new job file is different. Since the mat files were already
% produced, with prep4bknd.m we don't have to worry about the boundaries
% of each job (to account neighboring segments, buffer seconds). But in
% clustermap -> preproc method, we run preproc for the first time and
% hence have to include seconds for neighboring segments and buffer. Also
% the jobs in the new job file will overlap (in contrast to jobfile
% produced byprep4bknd.m), because of the addition of neighboring and
% buffer seconds to each job.

% First make sure relevant parameter have been set.  Otherwise, use default 
% values
try
  % mat prefix
  params.inmats;
  % job start time
  params.startGPS;
  % job end time
  params.endGPS;
  % pseudo job duration
  params.jobdur;
  % output job file
  params.bkndout;
catch
  warning('Missing parameters: using Erics defaults.');
  params.inmats = '/archive/home/ethrane/stamp2/test/mats/map';
  params.startGPS = 816065659;
  params.endGPS = 877591542;
  params.jobdur = 500;
  params.bkndout = 'pseudojobs.txt';
end

% call findmats to get all mat files between job start-stop times
[matlist, params] = findmats(params);

% initialize pseudo job duration
tmpdur = 0;

%loop over files in the matlist to obtain start times and durations
for ii=1:length(matlist)
  [temp gps_string] = regexp(matlist{ii}, '.*-(.*)-.*.mat', 'match', 'tokens');
  matstart(ii) = str2num(char(gps_string{1}));
  [temp dur_string] = regexp(matlist{ii}, '.*-.*-(.*).mat', 'match', 'tokens');
  matdur(ii) = str2num(char(dur_string{1}));
end

% load one mat file to determine the segment duration
tmp = load(matlist{1});
segdur = tmp.map.segDur;

% open the output file
fid = fopen(params.bkndout, 'w+');

% initialize array of segment times
t=[];
% initialize counter for pseudo jobs
kk=1;
% determine the number of segments required for each pseudo job file
N = 2*params.jobdur-1;
% loop over mat files
for jj=1:length(matlist)
  % construct a list of segment start times
  tmat = matstart(jj):segdur/2:matstart(jj)+matdur(jj)-segdur;
  % add it to the running list
  t = [t tmat];
  % remove duplicate times from overlapping mat files
  t = unique(t);
  % if there are enough segments in the running list we can create a pseudo job
  if length(t)>=N
    % print job details to output file
    fprintf(fid, '%i %10.2f %10.2f %i\n', kk, ...
      t(1), t(N)+segdur/2, params.jobdur);
    % remove the times associated with that job file from the running list
    t(1:N)=[];
    % increment the pseudo job counter
    kk=kk+1;
  end
end

% close output file
fclose(fid);

return
