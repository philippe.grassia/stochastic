function [g, eps_12, eps_11, eps_22] = polarReadout(params, g)
% function [g, eps_12, eps11, eps_22] = polarReadout(params, g)
% E. Thrane - this code used to be part of ccSpecReadout_stamp.  However it is
% mostly used for diagnostic purposes, so I have moved it into a separate routine
% in order to make ccSpecReadout_stamp easier to read.
%
% NOTE: iota and phi should be in radians!

if params.doPolar
  Ap = (1/2)*(1 + (cos(params.iota))^2); % a_plus, from STAMP paper
  Ax = cos(params.iota);                 % a_cross, from STAMP paper
  term1 = (g.F1p .* g.F2p)*( (Ap^2)*(cos(2*params.psi))^2 + ...
			     (Ax^2)*(sin(2*params.psi))^2);
  term2 = ((g.F1p .* g.F2x) + (g.F1x .* g.F2p))*(Ap^2-Ax^2)* ...
	  cos(2*params.psi)*sin(2*params.psi);
  term3 = (g.F1x .* g.F2x)*( (Ap^2)*(sin(2*params.psi))^2 + ...
			     (Ax^2)*(cos(2*params.psi))^2 );
  term4 = i*(Ap*Ax)*((g.F1x .* g.F2p) - (g.F1p .* g.F2x));
  
  g.gamma0 = abs(term1 + term2 + term3 + term4)/( (Ap^2) + (Ax^2) );
  g.eta = -phase( (term1 + term2 + term3 + term4)/( (Ap^2) + (Ax^2) ) ); 
  
  % Calculate pair efficiencies.
  % See Eq. A50 in STAMP methods paper.  Note for eps_11 and eps_22,
  % the imaginary term cancels out.
  % TP: should be correct as of 8/12/2013.
  eps_12 = g.gamma0;
  eps_11 = (g.F1p .* g.F1p) * ...
	   ((Ap^2)*(cos(2*params.psi))^2 + (Ax^2)*(sin(2*params.psi))^2) + ...
	   2*(g.F1p .* g.F1x)*(Ap^2 - Ax^2) * ...
	   cos(2*params.psi) * sin(2*params.psi) + ...
	   (g.F1x .* g.F1x) * ...
	   ((Ap^2)*(sin(2*params.psi))^2 + (Ax^2)*(cos(2*params.psi))^2);
  eps_11 = abs(eps_11)/(Ap^2 + Ax^2);
  eps_22 = (g.F2p .* g.F2p) * ...
	   ((Ap^2)*(cos(2*params.psi))^2 + (Ax^2)*(sin(2*params.psi))^2) + ...
	   2*(g.F2p .* g.F2x)*(Ap^2 - Ax^2) * ...
	   cos(2*params.psi) * sin(2*params.psi) + ...
	   (g.F2x .* g.F2x) * ...
	   ((Ap^2)*(sin(2*params.psi))^2 + (Ax^2)*(cos(2*params.psi))^2);
  eps_22 = abs(eps_22)/(Ap^2 + Ax^2);

elseif params.purePlus % pure plus polarization
  g.gamma0 = (g.F1p .* g.F2p);
  eps_12 = g.gamma0;
  eps_11 = (g.F1p .* g.F1p);
  eps_22 = (g.F2p .* g.F2p);
elseif params.pureCross
  g.gamma0 = (g.F1x .* g.F2x);
  eps_12 = g.gamma0;
  eps_11 = (g.F1x .* g.F1x);
  eps_22 = (g.F2x .* g.F2x);
else
  % should never arrive here.
  error('Problem in polarReadout.');
end

return;
