function map = maskCluster(params, map)

% remove loudest cluster track (removing for upper limits)
% this is currently designed and tested for single direction searches only
% replaces pixels with NaN if the same pixels in the cluster array have non-zero values
%
% Written by R Quitzow-James

cluster_data = load(params.clusterFile);
if isfield(cluster_data, 'stoch_out')
  cluster_mask = cluster_data.stoch_out.cluster.reconMax;
elseif isfield(cluster_data, cluster)
  cluster_mask = cluster_data.cluster.reconMax;
elseif isfiled(reconmax)
  cluster_mask = cluster_data.reconMax;
else
  error('variable structure in file for constructing cluster mask not recognized')
end
if size(map.snr) == size(cluster_mask)
  fprintf('masking cluster from file\n')
  map.snr(cluster_mask > 0) = NaN;
  map.snrz(cluster_mask > 0) = NaN;
  map.y(cluster_mask > 0) = NaN;
  map.z(cluster_mask > 0) = NaN;
  map.sigma(cluster_mask > 0) = NaN;
else
  error('cluster mask size does not match size of snr or snrz array')
end
