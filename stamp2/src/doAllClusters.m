%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates quantities such as SNR, min and max
% frequencies etc.. of all the clusters found by burstegard, and store
% the data in the stoch_out structure. It also adds two other quantities:
% the frequencies of the first and last pixels of each cluster. This
% quantity is used when dealing with redundant clusters in overlapping ft-maps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function stoch_out = doAllClusters(stoch_out,params,map,max_cluster,ss)

if params.doAllClusters %%%% We can look only at the maxSNR cluster, to add the 
  uniClusters=unique(max_cluster.reconAll); %% new fields only
  nbClusters=size(uniClusters,1)-1;
elseif max(max(max_cluster.reconAll))>0
  nbClusters=1;
  uniClusters=[0 ; 1];
else
  nbClusters=0;
end

allClustersSnrs              = zeros(nbClusters,1);  %% New stoch_out fields prepared here
allClustersAP1               = zeros(nbClusters,1);
allClustersAP2               = zeros(nbClusters,1);
allClustersNE1.brut          = zeros(nbClusters,1);
allClustersNE1.norm          = zeros(nbClusters,1);
allClustersNE2.brut          = zeros(nbClusters,1);
allClustersNE2.norm          = zeros(nbClusters,1);
allClustersFreqs             = zeros(nbClusters,2);
allClustersTimes             = zeros(nbClusters,2);
allClustersFreqs_1st_pixel   = zeros(nbClusters,1);
allClustersFreqs_last_pixel  = zeros(nbClusters,1);
allClustersNbPix             = zeros(nbClusters,1);
allClustersSNRfracs.coherent = zeros(nbClusters,1);
allClustersSNRfracs.ifo1     = zeros(nbClusters,1);
allClustersSNRfracs.ifo2     = zeros(nbClusters,1);
allClustersSNRfracTimes      = zeros(nbClusters,1);


if params.doAllClusters
   if params.doZebragard
      mapY=abs(map.y);
      mapSig=map.sigma;
      mapSNR=abs(map.snr);
   else
      mapY=map.y;
      mapSig=map.sigma;
      mapSNR=map.snr;
   end
end


for clust0=1:nbClusters  %% Loop on each cluster in a given ft-map
   clust=uniClusters(clust0+1);
   if params.doAllClusters
      A=double(max_cluster.reconAll(:,:)==clust); %% Map with (clust) at the pixels belonging
   else                                           %% to the cluster studied, and 0 elsewhere
      A=double(max_cluster.reconMax(:,:));
   end 
   
   nbPixels=sum(sum(A));
   allClustersNbPix(clust0)=nbPixels;

   [r,c] = find(A == 1);
   % get min/max time and frequency.
   allClustersFreqs(clust0,1)=params.fmin-1+params.deltaF*min(r); %% Changing indexes to frequencies
   allClustersFreqs(clust0,2)=params.fmin-1+params.deltaF*max(r);
   allClustersTimes(clust0,1)=params.segmentDuration*(min(c)-1)/2; 
   allClustersTimes(clust0,2)=params.segmentDuration*(max(c)-1)/2; %%% Determine the time of beginning and ending
   % get minimum frequency in first and last columns.
   allClustersFreqs_last_pixel(clust0,1)=params.fmin-1+params.deltaF*min(r(c==min(c)));
   allClustersFreqs_1st_pixel(clust0,1)=params.fmin-1+params.deltaF*min(r(c==max(c)));


   if params.doAllClusters  %% Reconstitutes the SNR for every cluster on the map
      tt=map.segstarttime-map.start>allClustersTimes(clust0,1)&map.segstarttime-map.start<allClustersTimes(clust0,2);
      ff=map.f>allClustersFreqs(clust0,1)&map.f<allClustersFreqs(clust0,2);

      tempY=A.*(mapY);
      tempY(isnan(tempY))=0;
      tempSig=A.*(mapSig);
      tempSig(isnan(tempSig))=0;

      tempSig=1./tempSig;
      tempSig(isinf(tempSig))=0;
      tempSig=tempSig.^2;


      tempYOverSig2=tempY.*(tempSig);

      YOverSig2=sum(sum(tempYOverSig2));
      numer=YOverSig2/sum(sum(tempSig));

      denom=1/sqrt(sum(sum(tempSig)));

      clustSnr=numer/denom;

      allClustersSnrs(clust0)=clustSnr;

      % AP for each det
      % noise is estimated using median
      naiP=map.naiP1;
      naiP(isnan(naiP)|isinf(naiP))=0;
      AP1=map.naiP1./repmat(median(naiP,2),1,size(naiP,2));
      AP1(isnan(AP1)|isinf(AP1))=0;

      naiP=map.naiP2;
      naiP(isnan(naiP)|isinf(naiP))=0;
      AP2=map.naiP2./repmat(median(naiP,2),1,size(naiP,2));
      AP2(isnan(AP2)|isinf(AP2))=0;

      totalAP1 = sum(sum(A.*AP1));
      totalAP2 = sum(sum(A.*AP2));

      allClustersAP1(clust0) = totalAP1;
      allClustersAP2(clust0) = totalAP2;
      
      % Null energy
      NE=NullEnergy(map,logical(A),true);
      allClustersNE1.brut(clust0) = NE.ifo1.brut;
      allClustersNE1.norm(clust0) = NE.ifo1.norm;
      allClustersNE2.brut(clust0) = NE.ifo2.brut;
      allClustersNE2.norm(clust0) = NE.ifo2.norm;

      % SNR frac 
      colsSNR1 = sum(A.*AP1,1)/totalAP1;
      colsSNR2 = sum(A.*AP2,1)/totalAP2;

      allClustersSNRfracs.ifo1(clust0) = colsSNR1(find(colsSNR1==max(colsSNR1)));
      allClustersSNRfracs.ifo2(clust0) = colsSNR2(find(colsSNR2==max(colsSNR2)));

      tempSNR = A.*mapSNR;
      tempSNR(isnan(tempSNR)) = 0;
      totalSNR = sum(tempSNR(:));
      colsSNR = sum(tempSNR,1)/totalSNR;
      temp_idx = find(colsSNR==max(colsSNR));
      allClustersSNRfracs.coherent(clust0) = colsSNR(temp_idx);
      allClustersSNRfracTimes(clust0) = map.segstarttime(temp_idx) - ...
	  map.segstarttime(1);

   else
      allClustersSnrs(clust0)=max_cluster.snr_gamma;
   end
end

  stoch_out.allClustersSnrs{ss}=allClustersSnrs;  %% Fill and return the stoch_out structure
  stoch_out.allClustersAP1{ss}=allClustersAP1;  
  stoch_out.allClustersAP2{ss}=allClustersAP2;  
  stoch_out.allClustersNE1{ss}=allClustersNE1;
  stoch_out.allClustersNE2{ss}=allClustersNE2;
  stoch_out.allClustersFreqs{ss}=allClustersFreqs;
  stoch_out.allClustersFreqs_1st_pixel{ss}=allClustersFreqs_1st_pixel;
  stoch_out.allClustersFreqs_last_pixel{ss}=allClustersFreqs_last_pixel;
  stoch_out.allClustersTimes{ss}=allClustersTimes;
  stoch_out.allClustersNbPix{ss}=allClustersNbPix;
  stoch_out.nbClusters{ss}=nbClusters;
  stoch_out.allClustersSNRfracs{ss}=allClustersSNRfracs;
  stoch_out.allClustersSNRfracTimes{ss}=allClustersSNRfracTimes;

return
