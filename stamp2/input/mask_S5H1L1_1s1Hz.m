function params = mask_S5H1L1_1s1Hz(params)
% function params = mask_S5H1L1_1s1Hz(params)
% E. Thrane: frequency mask for S5H1L1 analyses
% for more see http://tinyurl.com/7lgaft2

params.doStampFreqMask=true;
params.StampFreqsToRemove = [57 58 59 60 61 62 63 118,119,120,121,122,178,179,180,181,182,239,240,241,328,329,330,331,341,342,343,344,345,346,348,349,599,685,686,687,688,689,690,691,692,693,694,695,696,697,959,1028,1029,1030,1031,1032,1033,1034,1035,1036,1039,1040,1041,1043,1044,1045,1143,1144,1145,1146,1150,1151,1152,1153];

% Remove notches outside the frequency range of interest.
if (isfield(params,'fmin') && isfield(params,'fmax'))
  params.StampFreqsToRemove = ...
      params.StampFreqsToRemove((params.StampFreqsToRemove>=params.fmin));
  params.StampFreqsToRemove = ...
      params.StampFreqsToRemove((params.StampFreqsToRemove<=params.fmax));
else
  error(['You must define your frequency range of interest (params.fmin and' ...
	 ' params.fmax) before defining a frequency mask.']);
end