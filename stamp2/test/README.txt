Instructions for running STAMP
by Eric Thrane on February 14, 2014
last updated: April 21, 2014

* Update on June 6, 2014: sometime in past several months, java has stopped 
playing nicely with stamp on the CIT and MIT clusters.  (It is necessary for
the doParallel option, which allows multi-core CPU processing.)  Today, it 
appears to have started working again for reasons unknown.

* I strongly recommend running on ldas-pcdev2.ligo.caltech.edu.  All the 
following instructions in this note assume that you are working in the 
directory where you find this README.txt.

* The zeroth step is to set up your environment to run matlab.  First, run
      stamp_setup.sh
Next, open
      /ldcg/matlab_r2013a/bin/matlab -nodisplay
At the time this is written, we recommend running with matlab 2013a.

* Now that matlab is open, set up your matlab paths:
      addpath(genpath('../src'));
      addpath(genpath('../input'));

* The first step for running stamp is to preprocess strain data to create the 
intermediate data required to run STAMP.  This step has already been done in 
order to create the mat files in input/.  However, if you wish to repeat this 
step, you can run run_preproc.m from the matlab command line.  The intermediate
mat files include 300s of Monte Carlo aLIGO noise in H1 and L1.  In the noise
is an injected ADI-D waveform, beginning 15s into the job.  The signal is 
injected at (ra,dec) = (6.4 hr, -39.30 deg).

* The next step is to run test_clustermap.m.  This is a wrapper that calls the
STAMP code.  The wrapper can be run with several search options:
      1 (default) = deep stochtrack running on 4 GPUs
      2 = default stochtrack with 4 CPUs
      3 = default stochtrack with 1 CPU
      4 = default stochtrack with 1 CPU
      5 = burstegard
At the time of writing, in order to use GPUs, you must run on the Caltech or 
MIT pcdev2 head node.  Burstegard and stochtrack are two different search 
algorithms.  Burstegard is very fast.  Stochtrack is, as a rule of thumb, more
sensitive for long-lived signals.  This wrapper script will create a number of
output files:

      snr.png: ft-map of cross-correlated data

      rmap.png: stochtrack reconstruction plot (not created by burstegard)

      all_clusters.png: all clusters found by burstegard (not created by
      stochtrack)

      large_cluster.png: loudest burstegard cluster (not created by stochtrack)

      map_1.mat: record of ft-maps

      test.mat: record of search results

There are some other image files created as well, but these are the important
ones.


TIPS FOR ADVANCED USERS========================================================

* In order to compile STAMP code to run on condor (or on XSEDE), the simplest
command is:

     mcc -R -nojvm -R -nodisplay -R -singleCompThread -m your_wrapper_here

If you compile the code like this, it can run on a single GPU or CPU with 4g of
memory.  However, if you use a very large ft-map, or if stochtrack.T and/or
stoctrack.F are very large, then you may experience memory problems.  Be 
prepared for the compilation to take a while (>15 minutes).

* If you want to run compiled code with doParallel on to take advantage of 
multiple CPU cores on one chip (or multiple GPUs on one node), you will need a
different compilation command that includes java:

    mcc -C -R -nodisplay -R -singleCompThread -m your_wrapper_here

Notice the -C flag; it is crucial to preventing mexa errors.  Also note that 
this method of compilation will produce a .ctf file and a mccExcludedFiles.log
file.  It's important not to move or remove either of these files.  The first 
time the executable is run, a directory will be created ending in _mcr.  This
should not be moved or removed either.  When running on an 8-core CPU with 
doParallel on, you will need 8g of memory.  In general, it is not efficient to
run doParallel code on condor with GPUs because the best performance is 
obtained using separate jobs for each GPU.  (If you insist on using doGPU with
doParallel, budget 16g per job.)  Also note, if you compile with this -C flag,
make sure to start your matlab session in a directory where the startup.m 
script will *not* be run.  Otherwise, the startup.m commands will be 
incorporated into the binary executable.  Instead, enter the necessary addpath 
commands on the matlab command line before compiling.

* Note that *single* GPU jobs require more memory with the -C flag, so you 
should not mix -C compilation with GPUs unless you want multiple GPUs, which 
is usually not the case.

* RECOMMENDED COMPILATION (added by E Thrane on 1 May, 2015)
This compilation is fast and produces a lean, low-memory executable that runs
on single cores and GPUs.

  mcc_fast('your_wrapper_goes_here');

Example code for mcc_fast is in this directory.  You will need to modify pieces
of it to point to your code.

* More tips for condor.  If you want to run stochtrack on condor with GPUs, put
the following lines in your .sub file:

    RequestMemory = 4000
    Requirements = TARGET.WantGPU =?= True

If you want to run with doParallel on multi-core CPUs, use the following lines 
instead:

   RequestMemory = 8000
   request_cpus = 8

If you submit jobs compiled without the -C flag, you must call the executable 
with cit-matlab-init.  If you compile with the -C flag, AND you are running 
from the command line, you must NOT call the exectuable with cit-matlab-init 
or you will get an error related to mex files in the parallel computing 
toolbox.  If you compile with the -C flag, AND you are submitting to condor, 
you must use cit-matlab-init.  In order to avoid getting burned by this issue,
I suggest adding the following lines to your condor wrapper:
  # determine if running on the head node, and if not, use cit-matlab-init
  computer=`hostname`
  if [ "$computer" = "ldas-pcdev2.mit.edu" ]
  then
    echo "running on the head node"
    init=""
  else
    echo "not running on the head node"
    init="/home/ethrane/cit-matlab-init"
  fi
  ...
  eval "$init /home/ethrane/verylong/bin/grand_stochtrack $@"

* Debugging tip.  If you receive this error

  Undefined function 'matlabpool' for input arguments of type 'char'.
  Error in initParallel (line 13)

it is probably caused by a failure to compile with the parallel toolbox.  Try
recompiling with the -C flag as described above.

* Debugging tip.  If you receive this error

  Error using matlabpool (line 134)
  Invalid MEX-file...distcomp/distcomp/dct_psfcns.mexa64: undefined symbol

it is probably because you are using the cit-matlab-init script in conjunction 
with -C compilation from the command line.  (This is a no-no; see above.)

EHT on 4 Mar, 2015: I got the same error today using -C and init.  I made the
following change to my cit-matlab-init script...

  #export MCR_ROOT=/ldcg/matlab_r2012a
  export MCR_ROOT=/ldcg/matlab_r2013a

...and the error went away.

* Debugging tip.  If you receive this error

  Could not access preferences directory: '/tmp/.matlab/mcr_v81/stuff

it may be beacuse you are running on condor without cit-matlab-init.

