# MATLAB SET-UP ###############################################################
# set LD_LIBRARY_PATH for matlab 2013a
export LD_LIBRARY_PATH=/ligotools/lib:/ldcg/matlab_r2013a/sys/opengl/lib/glnxa64:/ldcg/matlab_r2013a/sys/java/jre/glnxa64/jre/lib/amd64:/ldcg/matlab_r2013a/sys/java/jre/glnxa64/jre/lib/amd64/server:/ldcg/matlab_r2013a/sys/java/jre/glnxa64/jre/lib/amd64/native_threads:/ldcg/matlab_r2013a/bin/glnxa64:/ldcg/matlab_r2013a/sys/os/glnxa64:/ldcg/matlab_r2013a/runtime/glnxa64:/ldcg/matlab_r2013a/runtime/glnxa64:/ligotools/lib

export XAPPLRESDIR=/ldcg/matlab_r2013a/X11/app-defaults

eval `/ligotools/bin/use_ligotools`
###############################################################################
