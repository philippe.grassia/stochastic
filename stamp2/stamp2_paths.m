% Eric Thrane
%
% This is an example of a startup.m file which you can run automatically 
% every time you launch matlab to set your matlab paths.  These commands will
% set up the paths needed for STAMP code.  You must define the stamp_install
% variable to point to your svn copy of stamp.
% pwd uses the home directory.  if you want to hard-code a path, that's fine
% too.  Just un-comment out the subsequent line and modify for your own home
% directory.
stamp_install = pwd;
%------------------------------------------------------------------------------

% make sure that the stamp install exists
if ~exist(stamp_install)
  fprintf('stamp_install = %s does not exist.\n', stamp_install);
  fprintf('Please modify ex_startup.m\n');
  return;
end

% define STAMP paths
% include everything in the src/ directory including subdirectories
if ~isdeployed
  fprintf('loading stamp 2.0 packages...');
  addpath(genpath([stamp_install '/src']));
  addpath(genpath([stamp_install '/input']));
  fprintf('done.\n');
end
