function mcc_fast_4(fname)
% function mcc_fast_4(fname)
% example usage: mcc_fast_4('grand_stochtrack');

% change this to directory containing your copy of the stamp2 directory
stamp2parentDir = '/home/ethrane';

% define required paths for stamp2: 
% each subdirectory should be included separately
stamp2 = {strcat(stamp2parentDir,'/stamp2/src/'), ...
strcat(stamp2parentDir,'/stamp2/src/algorithms/stochtrack'), ...
strcat(stamp2parentDir,'/stamp2/src/img'), ...
strcat(stamp2parentDir,'/stamp2/src/gpu_tools'), ...
strcat(stamp2parentDir,'/stamp2/src/inj'), ...
strcat(stamp2parentDir,'/stamp2/src/tools'), ...
strcat(stamp2parentDir,'/stamp2/src/misc'), ...
strcat(stamp2parentDir,'/stamp2/src/anteproc'), ...
strcat(stamp2parentDir,'/stamp2/src/preproc'), ...
strcat(stamp2parentDir,'/stamp2/src/misc/utilities'), ...
strcat(stamp2parentDir,'/stamp2/src/misc/utilities/FTSeries'), ...
strcat(stamp2parentDir,'/stamp2/src/misc/utilities/ligotools'), ...
'/ldcg_server/ligotools/centos5_x86_64/matlab/', ...
'/ldcg/matlab_r2013a/toolbox/images/iptutils', ...
strcat(stamp2parentDir,'/stamp2/input/'), ...
strcat(stamp2parentDir,'/stamp2/src/crosscorr')
};

% start off by requiring stamp code
comp_cmd = 'mcc -v -N';
for ii=1:numel(stamp2)
  comp_cmd = [comp_cmd ' -I ' stamp2{ii}];
end

% path to signals toolbox
sigs = '/ldcg/matlab_r2013a/toolbox/signal/signal';
stats = '/ldcg/matlab_r2013a/toolbox/stats/stats';
% add parallel without java to run gather()
% this is needed for stochtrack on a single core
parallel = '/ldcg/matlab_r2013a/toolbox/distcomp/parallel/';
% add this to run stochtrack on a single gpu
gpu1 = '/ldcg/matlab_r2013a/toolbox/distcomp/gpu/';
gpu2 = '/ldcg/matlab_r2013a/toolbox/distcomp/array';
% in order to use padarray.m (Thanks, Michael)
images = '/ldcg/matlab_r2013a/toolbox/images/images/';

% java
%java = '/ldcg/matlab_r2013a/toolbox/javabuilder/javabuilder';

% -I flags
includes = { sigs stats parallel images gpu1 gpu2 };

% -R flags
options = { '-nodisplay','-singleCompThread' };

% compilation command includes
for ii=1:numel(includes)
  comp_cmd = [comp_cmd ' -I ' includes{ii}];
end

% ...options
for ii=1:numel(options)
  comp_cmd = [comp_cmd ' -R ' options{ii}];
end

% ...add -m flag
comp_cmd = [comp_cmd ' -m ' fname];

% compile
fprintf('%s\n',comp_cmd);
eval(comp_cmd);

% remove annoying files
system(['rm mccExcludedFiles.log readme.txt run_' fname '.sh']);

return;
