function [foldedInvCov, foldedStatistic, params, foldParams] = ...
    foldSID (paramsFile, firstSeg, lastSeg, matFile, writeFrames)
%
% Main routine to fold Stochastic Intermediate Data (SID) frames to one 
% sidereal day. The output can be written as both .mat and/or .gwf formats.
%
% Mathematical details can be found in the LIGO technical document: T0900093
%
%
% [foldedInvCov, foldedStatistic, params, foldParams] = ...
%     foldSID (paramFile, firstSeg, lastSeg, matFile, writeFrames);
%
% paramsFile      : String. The parameter file
% ------------ Optional parameters for parallelization ------------
% firstSeg        : Integer. First segment # to fold from list
%                   Default -1, equivalent to first available
% lastSeg         : Integer. Last segment # to fold from list
%                   Default -1, equivalent to last available
% matFile         : String. Save folded data to this .mat file
% writeFrames     : Logical. If exists, frame(s) will be written
% -----------------------------------------------------------------
%
% foldedInvCov    : Real matrix (t,f). Inverse covariance of folded statistic
% foldedStatistic : Complex matrix (t,f). Folded statistic
% params          : Structure. Parameters read from the first frame
% foldParams      : Structure. Parameters used for folding
%

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


%% Check arguments
if (nargin < 1 || nargin > 5)
  error(['Argument number mismatch. For usage info: help ' mfilename]);
end


%============================= Read Parameters =============================%

foldParams.paramsFile = paramsFile;

%% read in name/value pairs
[names,values] = ...
  ctextread(paramsFile, '%s %s', -1, 'commentstyle', 'matlab');

%% check that number of names and values are equal
if length(names)~=length(values)
  error('invalid parameter file');
end

%% loop over parameter names, assigning values to variables
for ii=1:length(names)
  switch names{ii}

  % Interferometer 1
  case 'ifo1'
    foldParams.ifo1 =  values{ii};
    ifo1 = foldParams.ifo1;

  % Interferometer 2
  case 'ifo2'
    foldParams.ifo2 =  values{ii};
    ifo2 = foldParams.ifo2;

  % Each line of this file should have:
  % Column 1: GPSStart, Column 2: path to the corresponding SID frame
  case 'SIDMetaDataFile'
    foldParams.SIDMetaDataFile = values{ii};
    SIDMetaDataFile = foldParams.SIDMetaDataFile;

  % Segment duration in SID frames (not folded SID frames)
  case {'segmentDuration', 'segDuration'}
    foldParams.segDuration = str2num(values{ii});
    segDuration = foldParams.segDuration;
    if ~(segDuration > 0)
      error('segDuration must be a positive number');
    end

  % Apply Sigma cuts, if necessary
  case 'maxASigma'
    maxASigma = str2num(values{ii}); % max
    SigmaCut.maxA = maxASigma;
  case 'maxDSigma'
    maxDSigma = str2num(values{ii}); % max
    SigmaCut.maxD = maxDSigma;
  case 'minDSigma'
    minDSigma = str2num(values{ii}); % min
    SigmaCut.minD = minDSigma;
  case 'smoothWinSize4SigmaCut'      % #bin for smoothing
    smoothSpan = -1;
    SigmaCut.smoothSpan = smoothSpan;
  case 'sigmaCutWeightFile'          % extra weight for broadband cuts
    sigmaCutWeightFile = values{ii};
    SigmaCut.weightFile = sigmaCutWeightFile;

  % (OPTIONAL) To save channels for compatibility with stochastic.m
  case 'explicit4isotropic' % Obsolete
    foldParams.backwardCompatible = true;
    backwardCompatible = foldParams.backwardCompatible;
    warning('Option explicit4isotropic is depreciated, use backwardCompatible');

  % (OPTIONAL) To save channels for compatibility with stochastic.m
  case 'backwardCompatible'
    foldParams.backwardCompatible = true;
    backwardCompatible = foldParams.backwardCompatible;

  % (OPTIONAL) Assume that noise PSDs of the neighbors are very close
  case 'identicalNeighbors'
    foldParams.identicalNeighbors = true;
    identicalNeighbors = foldParams.identicalNeighbors;
    
  % (OPTIONAL) Create a jobfile for folded data
  case 'FSIDJobFile'
    foldParams.FSIDJobFile = values{ii};
    FSIDJobFile = foldParams.FSIDJobFile;
     
  % (OPTIONAL) Prefix for FSID frames (suffixes are sidereal time, duration, ..)
  % Frames will be written iff this parameter is supplied
  case 'FSIDFramePrefix'
    foldParams.FSIDFramePrefix = values{ii};
    FSIDFramePrefix = foldParams.FSIDFramePrefix;

  % (OPTIONAL) Offset the sidereal GPSStart, too fool the pipeline!
  case 'FSIDGPSOffset'
    FSIDGPSOffsetReq = str2num(values{ii});
    foldParams.FSIDGPSOffset = FSIDGPSOffsetReq; % May be changed later

  % (OPTIONAL) Correct GPS offset to match the sidereal zero
  case 'FSIDGPSOffsetCorrect'
    FSIDGPSOffsetCorrect = str2num(values{ii});
    foldParams.FSIDGPSOffsetCorrect = FSIDGPSOffsetCorrect;

  % (OPTIONAL) Maximum number of segments per file, default 30 (26min)
  % If this is set to -1, whole folded data will be written to one frame
  case 'segmentsPerFrame'
    foldParams.segmentsPerFrame = round(str2num(values{ii}));
    segmentsPerFrame = foldParams.segmentsPerFrame;
    if (segmentsPerFrame < 1)
      error('segmentsPerFrame must be a positive integer');
    end
  
  % (OPTIONAL) Version of the Folded SID frame
  case 'version'
    foldParams.version = str2num(values{ii});
    FSIDVersion = foldParams.version;

  % (OPTIONAL) Sidereal day in seconds, default 86164
  case 'siderealDay'
    foldParams.siderealDay = str2num(values{ii});
    siderealDay = foldParams.siderealDay;
    if ~(siderealDay > 0)
      error('siderealDay must be a positive number');
    end

  % (OPTIONAL/DEBUG) Overlapping window correction, default true
  case 'ovlWinCorrection'
    foldParams.ovlWinCorrection = str2num(values{ii});
    ovlWinCorrection = foldParams.ovlWinCorrection;

  % (OPTIONAL) Log file, default stdout
  case 'logFile'
    foldParams.logFile = values{ii};
    logFile = foldParams.logFile;

  % (OPTIONAL) After this many SID frames progress will be written, default 1000
  case 'logStep'
    foldParams.logStep = str2num(values{ii});
    logStep = foldParams.logStep;
    if (logStep < 1)
      error('logStep must be a positive integer');
    end

  % (OPTIONAL) Write all, basic output will be written by default
  case 'verbose'
    foldParams.verbose = str2num(values{ii});
    verbose = foldParams.verbose;

  % Default warning
  otherwise
    fprintf(2,'WARNING: Unknown parameter %s\n',names{ii});

  end
end



%========================= Check Compulsory Inputs =========================%

if ~exist('SIDMetaDataFile','var')
  error('SIDMetaDataFile not found in file: %s',paramsFile);
end

if ~exist('segDuration','var')
  error('segmentDuration not found in file: %s',paramsFile);
end



%=============================== Log details ===============================%

if ~exist('logFile','var')
  logfp = 1;      % 1 = stdout
else
  logfp = fopen(logFile,'w');
end
foldParams.logfp = logfp;

if ~exist('logStep','var')
  logStep = 1000; % Write status after processing this many frames
end
foldParams.logStep = logStep;

if exist('verbose','var')
  verbose = true;
else
  verbose = false;
end
foldParams.verbose = verbose;



%======== Load and sort the list of GPS segments and the file names ========%

% Sorting can be slow. Most likely the list is already sorted, check it first
% If any other data query tool is used, modify this part

[listOfGPSStart, listOfSIDFile] = textread(SIDMetaDataFile,'%u %s');

if ~issorted(listOfGPSStart)
  [ listOfGPSStart, sortMatrix ] = sort(listOfGPSStart);
  listOfSIDFile = listOfSIDFile(sortMatrix);
  clear sortMatrix;
end

nSIDSeg = length(listOfGPSStart);
fprintf(logfp,'\nTotal number of segments available: %d\n',nSIDSeg);



%=========================== Default Parameters ============================%


if ~exist('maxASigma','var')
  SigmaCut.maxA = -1.0; % i.e. disable Absolute Sigma cut
end
if ~exist('maxDSigma','var')
  SigmaCut.maxD = -1.0; % i.e. disable Delta Sigma cut
end
if ~exist('minDSigma','var')
  SigmaCut.minD = -1.0; % i.e. disable Delta Sigma cut
end
if ~exist('smoothSpan','var')
  SigmaCut.smoothSpan = 1000; % Smooth PSDs before checking stationarity
end
if (SigmaCut.maxD <= 0.0) & (SigmaCut.minD <= 0.0)
  fprintf(logfp,'\n*** WARNING: NOT applying Delta Sigma cuts ***\n\n');
else
  fprintf(logfp,'\nDiscarding segments outside DSigma ratio [%f,%f]\n\n',...
    SigmaCut.minD,SigmaCut.maxD);
end
if (SigmaCut.maxA <= 0.0)
  fprintf(logfp,'\n*** WARNING: NOT applying absolute Sigma cut ***\n\n');
else
  fprintf(logfp,'\nDiscarding segments if Sigma exceeds %e\n\n',SigmaCut.maxA);
end
if ~exist('sigmaCutWeightFile','var')
  SigmaCut.weightFile = '';
end

SigmaCut.logfp = logfp;

if (SigmaCut.maxD > 0.0) | (SigmaCut.minD > 0.0)
  SigmaCut.applyDS = true;
else
  SigmaCut.applyDS = false;
end

if (SigmaCut.maxA > 0.0)
  SigmaCut.applyAS = true;
else
  SigmaCut.applyAS = false;
end

sigmaCutData.badSegment = [];

if ~exist('backwardCompatible','var')
  foldParams.backwardCompatible = false;
  backwardCompatible = foldParams.backwardCompatible;
end

if ~exist('identicalNeighbors','var')
  foldParams.identicalNeighbors = false;
  identicalNeighbors = foldParams.identicalNeighbors;
end
if identicalNeighbors
  fprintf(logfp,...
    '\n*** ASSUMING neighboring segments have the same noise PSD ***\n\n');
end

if ~exist('ovlWinCorrection','var')
  ovlWinCorrection = 1;
  foldParams.ovlWinCorrection = ovlWinCorrection;
end
if ovlWinCorrection == 0
  fprintf(logfp,...
    '\n*** WARNING: NOT correcting for overlapping windows ***\n\n');
end

if ~exist('siderealDay','var')
  siderealDay = 86164.1; %% SHOULD IT BE A COMPULSORY VARIABLE??
  foldParams.siderealDay = siderealDay;
end


if ~exist('firstSeg','var') | firstSeg == -1
  firstSeg = 1;
elseif ischar(firstSeg)
  firstSeg = round(str2num(firstSeg));
  if isempty(firstSeg)
    firstSeg = 1;
  end
end
if ((firstSeg < 1) | (firstSeg > nSIDSeg))
  error('first segment is out of range, should be in the range [1,%d]',nSIDSeg);
end
foldParams.firstSeg = firstSeg;

if ~exist('lastSeg','var') | lastSeg == -1
  lastSeg = nSIDSeg;
elseif ischar(lastSeg)
  lastSeg = round(str2num(lastSeg));
  if isempty(lastSeg)
    lastSeg = nSIDSeg;
  end
end
if ((lastSeg < 1) | (lastSeg > nSIDSeg))
  error('last segment is out of range, should be in the range [1,%d]',nSIDSeg);
end
foldParams.lastSeg = lastSeg;

if (firstSeg > lastSeg)
  error('first segment number should be less than last segment number');
end

nSIDSegments = lastSeg-firstSeg+1;

fprintf(logfp,'First segment to fold: %d\n',firstSeg);
fprintf(logfp,'Last segment to fold: %d\n',lastSeg);
fprintf(logfp,'=> Number of segments to be folded: %d\n',nSIDSegments);


if exist('FSIDFramePrefix','var') & ~exist('segmentsPerFrame','var')
  segmentsPerFrame = 1; % That is 26sec per file
  foldParams.segmentsPerFrame = segmentsPerFrame;
end

if ~isfield(foldParams,'FSIDGPSOffsetCorrect')
  FSIDGPSOffsetCorrect = 1;
  foldParams.FSIDGPSOffsetCorrect = 1;
end

if exist('FSIDFramePrefix')
  if isfield(foldParams,'FSIDGPSOffset') & (foldParams.FSIDGPSOffsetCorrect==1)
    foldParams.FSIDGPSOffset = FSIDGPSOffsetReq + siderealDay - ...
      siderealDay*GPStoGreenwichMeanSiderealTime(FSIDGPSOffsetReq)/24.0;
  else
    FSIDGPSOffsetReq = 0;
    foldParams.FSIDGPSOffset = 0;
    foldParams.FSIDGPSOffsetCorrect = 0;
  end
end

if exist('FSIDFramePrefix','var')
  if ~exist('writeFrames','var')
    fprintf(logfp, ...
        'WARNING: FSID frames will not be written, however FSIDFramePrefix\n');
    fprintf(logfp, ...
        'WARNING: variable will be stored in .mat file for combine routine\n');
  elseif (writeFrames)
    fprintf(logfp,'FSID frames will be written\n');
    fprintf(logfp,...
        'GPS offset for FSID frames requested: ~%dsec\n', FSIDGPSOffsetReq);
    fprintf(logfp,...
        'Actual GPS offset for FSID frames will be: ~%10.2fsec\n',...
        foldParams.FSIDGPSOffset);
  end
else
  if exist('writeFrames','var') & writeFrames
    error('FSIDFramePrefix is not set, though writeFrames is selected');
  end
end


%========================== Free some memory here ==========================%

% SID MetaData can take huge amount of memory, erase the unneccessary ones asap
% Keep some of the boundary elements, just in case
nExtraMetaDataElem2keep = 1; % MUST BE >= 1
if (firstSeg > 1 + nExtraMetaDataElem2keep)
  [listOfSIDFile(1:firstSeg - 1 - nExtraMetaDataElem2keep)] = deal(cell(1));
end
if (lastSeg < nSIDSeg - nExtraMetaDataElem2keep)
  [listOfSIDFile(lastSeg + 1 + nExtraMetaDataElem2keep:end)] = deal(cell(1));
end



%====== Load parameters & data from the 1st SID frame to be analyzed =======%

SIDFile = char(listOfSIDFile(firstSeg));
params  = loadSIDParams (SIDFile, ifo1, ifo2, segDuration);

% Sanity check
if (params.segmentDuration ~= segDuration)
  error('Segment duration mismatch in first frame %s',SIDFile);
end


% Parameters read from the first SID frame
fLow    = params.flow;
fHigh   = params.fhigh;
deltaF  = params.deltaF;

%%%%%%%%%%%%%%%%%%%%%%% FIX FACTORS HERE %%%%%%%%%%%%%%%%%%%%%%%
winFactor = params.w1w2bar*params.w1w2bar/params.w1w2squaredbar;
winRatio  = 0.5*params.w1w2ovlsquaredbar/params.w1w2squaredbar;
%%%%%%%%%%%%%%%%%%%%%%% FIX FACTORS HERE %%%%%%%%%%%%%%%%%%%%%%%

%%% DISABLE OVERLAPPING WINDOW CORRECTION, IF FORCED %%%
if (ovlWinCorrection == 0)
  winRatio  = 0.0;
  winFactor = 1.0;
end
%%% DISABLE OVERLAPPING WINDOW CORRECTION, IF FORCED %%%

% Some extra parameters are inserted in the SID params for easy saving!
params.winFactor = winFactor;
params.winRatio  = winRatio;

fprintf(logfp,'\nFrom the first SID frame:\n');
fprintf(logfp,'\tfLow = %f, fHigh = %f, deltaF = %f\n',fLow,fHigh,deltaF);
fprintf(logfp,'\twinRatio = %f, winFactor = %f\n',winRatio,winFactor);

% Sigma cut related parameters
if SigmaCut.applyDS
  if (SigmaCut.smoothSpan < 0)
    fprintf('\nApplying DSigma cut using integrated weight/(P_1P_2)\n');
  elseif (SigmaCut.smoothSpan >= floor((fHigh-fLow)/deltaF))
    fprintf('\nApplying DSigma cut using integrated PSD\n');
  else
    fprintf('\nApplying f wise DSigma cut by smoothing PSD over %d f bins\n',...
      SigmaCut.smoothSpan);
  end
end

if isempty(SigmaCut.weightFile)
  SigmaCut.w8 = 1;
else
  sigmaCutW8 = load(SigmaCut.weightFile);
  % Linear interpolation, zero outside interval for easy band limiting of sums
  % Log interpolation messes up zeros
  SigmaCut.w8 = interp1(sigmaCutW8(:,1),sigmaCutW8(:,2), ...
    [fLow:deltaF:fHigh],'linear',0.0)';
  %if (sigmaCutW8(1,1) > fLow)
  %  SigmaCut.w8(1:ceil((sigmaCutW8(1,1) - fLow)/deltaF)) = 0.0;
  %end
  %if (sigmaCutW8(end,1) < fHigh)
  %  SigmaCut.w8(floor((sigmaCutW8(end,1)-fLow)/deltaF):end) = 0.0;
  %end
  fprintf (logfp, ...
    'Using weights by interpolating data to apply Sigma Cuts from:\n%s\n',...
    SigmaCut.weightFile);
  fprintf (logfp, 'Frequency range considered: [%f, %f] Hz\n', ...
    min(sigmaCutW8(:,1)),max(sigmaCutW8(:,1)));
  clear sigmaCutW8;
end

% Save the final set of data quality cut parameters
foldParams.SigmaCut = SigmaCut;


%============================ Output parameters ============================%

% Final folded data sizes
foldedSegDuration = segDuration/2; % Factor 2 to acc for 50% overlap
nFreqBin = floor((fHigh-fLow)/deltaF) + 1; %!= ceil if (fHigh-flow)%deltaF==0
nSegment = round(siderealDay/foldedSegDuration);
foldParams.segDist = zeros(nSegment,1);

params.foldedSegmentDuration = foldedSegDuration;
foldParams.foldedSegDuration = foldedSegDuration;
foldParams.nSegment = nSegment;
foldParams.nFreqBin = nFreqBin;

fprintf(logfp,'\nNumber of folded segments will be: %d\n',nSegment);
fprintf(logfp,'Number of f bin in each segment will be: %d\n\n',nFreqBin);

fprintf(logfp,'Estimated volume of folded data (memory & disk): ~%dMB\n',...
    ceil(40*nFreqBin*nSegment/1024/1024));
if backwardCompatible
  fprintf(logfp,'Extra diskspace for backward campatibility: ~%dMB\n',...
  ceil(24*nFreqBin*nSegment/1024/1024));
end
fprintf(logfp,...
    'Total memory & disk usage will be more depending on other parameters\n');

% Allocate memory for output
foldedStatistic = complex(zeros(nSegment,nFreqBin));
foldedInvCov.Diag = zeros(nSegment,nFreqBin);
foldedInvCov.Prev = zeros(nSegment,nFreqBin);
foldedInvCov.Next = zeros(nSegment,nFreqBin);

% Store the so called the naive and theoretical sigmas
if SigmaCut.applyDS & (SigmaCut.smoothSpan < 0)
  sigmaCutData.GPSStart = zeros(nSIDSegments,1);
  sigmaCutData.naiSigma = zeros(nSIDSegments,1);
  sigmaCutData.thrSigma = zeros(nSIDSegments,1);
end


%================================ MAIN LOOP ================================%

% Before entering the main loop, read the first frame
GPSStart = listOfGPSStart(firstSeg);
SIDFile   = char(listOfSIDFile(firstSeg));
[ statistic, invVariance, isStationary, misc ] = ...
    loadSID(SIDFile, GPSStart, segDuration, SigmaCut, ifo1, ifo2);
varSigmaSqInv = winFactor * invVariance;
if ~any(isStationary)
  sigmaCutData.badSegment = [sigmaCutData.badSegment, GPSStart];
end

params.GPSFirstStart  = GPSStart;

% Store the so called the naive and theoretical sigmas
% Could move inside the main loop to have one such code block, but this is safer
if SigmaCut.applyDS & (SigmaCut.smoothSpan < 0)
  sigmaCutData.GPSStart(1) = GPSStart;
  sigmaCutData.naiSigma(1) = misc.naiSigma;
  sigmaCutData.thrSigma(1) = misc.thrSigma;
end


% Also, load the previous SID frame, if it exists
if (firstSeg > 1)

  GPSStartPrev = listOfGPSStart(firstSeg-1);
  SIDFile      = char(listOfSIDFile(firstSeg-1));
  [ statisticPrev, invVariance, isStationaryPrev ] = ...
    loadSID(SIDFile, GPSStartPrev, segDuration, SigmaCut, ifo1, ifo2);
  varSigmaSqInvPrev = winFactor * invVariance;
else

  % If the first frame analyzed is the first frame available
  % set evrything to zero, maintaining proper dimensions
  GPSStartPrev      = 0;
  statisticPrev     = 0.0 .* statistic;
  varSigmaSqInvPrev = 0.0 .* varSigmaSqInv;
  isStationaryPrev  = 1+0 .* isStationary; % Redundant
end


% Account for offset between siderealStart of FSID with GPSStart of SID frames
sumStartOffset = 0.0;

% Loop over the SID segments
fprintf(logfp,'\nReading and folding SID frames...\n');
for iSIDSeg = 1:nSIDSegments

  thisSeg = iSIDSeg - 1 + firstSeg;
  
  % Corresponding folded (sidereal day) segment
  % NOTE: floor(x) + 1 != ceil(x) when x is an integer
  siderealTime = GPStoGreenwichMeanSiderealTime(GPSStart);
  actualFoldSeg = siderealTime/24.0*nSegment;
  foldSeg = round(actualFoldSeg);
  sumStartOffset = sumStartOffset + (actualFoldSeg-foldSeg)*foldedSegDuration;
  foldSeg = mod(foldSeg,nSegment) + 1; % +1 because arrays start with index 1

  % Count number of good segment being folded in a given segment
  if any(isStationary)
    foldParams.segDist(foldSeg) = foldParams.segDist(foldSeg) + 1;
  end

  % Show which segment is going where
  if (verbose)
    fprintf(logfp,'GPS = %d, sidereal time = %f, folded segment # = %d\n', ...
        GPSStart, siderealTime, foldSeg);
  end


  % Load the NEXT segment here, unless the current segment is the last
  if (thisSeg < nSIDSeg)

    GPSStartNext = listOfGPSStart(thisSeg+1);
    SIDFile      = char(listOfSIDFile(thisSeg+1));
    [ statisticNext, invVariance, isStationaryNext, miscNext ] = ...
        loadSID(SIDFile, GPSStartNext, segDuration, SigmaCut, ifo1, ifo2);
    varSigmaSqInvNext = winFactor * invVariance;
    if ~any(isStationaryNext)
      sigmaCutData.badSegment = [sigmaCutData.badSegment, GPSStartNext];
    end

    % Store the so called the naive and theoretical sigmas
    if SigmaCut.applyDS & (SigmaCut.smoothSpan < 0)
      sigmaCutData.GPSStart (iSIDSeg+1) = GPSStartNext;
      sigmaCutData.naiSigma (iSIDSeg+1) = miscNext.naiSigma;
      sigmaCutData.thrSigma (iSIDSeg+1) = miscNext.thrSigma;
    end

  else

    % If the last frame analyzed is the last frame available
    % set evrything to zero, maintaining proper dimensions
    GPSStartNext      = 0;
    statisticNext     = 0.0 .* statistic;
    varSigmaSqInvNext = 0.0 .* varSigmaSqInv;
    isStationaryNext  = 1+0 .* isStationary;
  end

  
% If previous segment exists and adjacent (allow 1sec offset)
  prevExists = winRatio * ...
    ((thisSeg > 1) & ((GPSStart - GPSStartPrev) < foldedSegDuration+1));

  % If next segment exists and adjacent (allow 1sec offset)
  nextExists = winRatio * ...
    ((thisSeg < nSIDSeg) & ((GPSStartNext - GPSStart) < foldedSegDuration+1));


  % To check if the prev and next segments are recognized correctly
  %fprintf(logfp,'thisSeg=%d, GPSStart=%d, prevExists=%f, nextExists=%f\n',...
  %    thisSeg, GPSStart, prevExists, nextExists);
  


  %======================= Folding Here : Start =============================%
  
  if ~identicalNeighbors % The actual scenario
  
    foldedInvCov.Diag(foldSeg,:) = ...
      foldedInvCov.Diag(foldSeg,:) + isStationary' .* varSigmaSqInv';
    
    foldedInvCov.Prev(foldSeg,:) = foldedInvCov.Prev(foldSeg,:) + ...
      (0.5*prevExists) * isStationary' .* (varSigmaSqInv + varSigmaSqInvPrev)';

    foldedInvCov.Next(foldSeg,:) = foldedInvCov.Next(foldSeg,:) + ...
      (0.5*nextExists) * isStationary' .* (varSigmaSqInv + varSigmaSqInvNext)';

    foldedStatistic(foldSeg,:) = foldedStatistic(foldSeg,:) + ...
      isStationary' .* (varSigmaSqInv.*statistic - (...
      (0.5*prevExists)*(varSigmaSqInv + varSigmaSqInvPrev).*statisticPrev +...
      (0.5*nextExists)*(varSigmaSqInv + varSigmaSqInvNext).*statisticNext))';
      
  else % To match the current stochastic SpH pipeline
    
    foldedInvCov.Diag(foldSeg,:) = foldedInvCov.Diag(foldSeg,:) + ...
      (1.0-prevExists-nextExists) * isStationary'.* varSigmaSqInv';
    
    foldedStatistic(foldSeg,:) = foldedStatistic(foldSeg,:) + ...
      (1.0-prevExists-nextExists) * isStationary'.* (varSigmaSqInv.*statistic)';
    

    %prevFoldSeg = mod(foldSeg+nSegment-1,nSegment)+1;
    %nextFoldSeg = mod(foldSeg+1,nSegment)+1;

    %foldedInvCov.Diag(prevFoldSeg,:) = foldedInvCov.Diag(prevFoldSeg,:) - ...
    %  prevExists * isStationary' .* varSigmaSqInvPrev';

    %foldedInvCov.Diag(nextFoldSeg,:) = foldedInvCov.Diag(nextFoldSeg,:) - ...
    %  nextExists * isStationary' .* varSigmaSqInvNext';

    %foldedStatistic(foldSeg,:) = foldedStatistic(foldSeg,:) + ...
    %   isStationary' .* (varSigmaSqInv.*statistic)';

    %foldedStatistic(prevFoldSeg,:) = foldedStatistic(prevFoldSeg,:) - ...
    %  prevExists * isStationary' .* (varSigmaSqInvPrev.*statisticPrev)';

    %foldedStatistic(nextFoldSeg,:) = foldedStatistic(nextFoldSeg,:) - ...
    %  nextExists * isStationary' .* (varSigmaSqInvNext.*statisticNext)';  
      
  end
  
  %======================= Folding Here : End ===============================%
  

  % Now shift data elements to process the next segment
  % Current -> Previous
  GPSStartPrev      = GPSStart;
  statisticPrev     = statistic;
  varSigmaSqInvPrev = varSigmaSqInv;
  isStationaryPrev  = isStationary; % Redundant
  
  % Next -> Current
  GPSStart      = GPSStartNext;
  statistic     = statisticNext;
  varSigmaSqInv = varSigmaSqInvNext;
  isStationary  = isStationaryNext;

  if (rem(iSIDSeg,logStep) == 0)
    fprintf(logfp,'%d of %d done\n',iSIDSeg,nSIDSegments);
  end

end

params.GPSLastEnd = GPSStartPrev + foldedSegDuration;

foldParams.startOffset = sumStartOffset / nSIDSegments;



%======================= Write folded data to frames =======================%

if exist('FSIDFramePrefix') & exist('writeFrames') & writeFrames
  fprintf(logfp,'Writing folded SID frame files to %s... ', FSIDFramePrefix);
  writeFSID (foldParams, params, foldedInvCov, foldedStatistic);
  fprintf(logfp,'DONE\n');
end



%======== Save relevant data in a .mat file for parallel processing ========%

if exist('matFile','var')
  fprintf(logfp,'Writing folded SID to %s ...',matFile);
  save (matFile, 'firstSeg', 'lastSeg', 'foldParams', 'params', ...
    'foldedInvCov', 'foldedStatistic', 'sigmaCutData');
  fprintf(logfp,'DONE\n');
end



%================================= The End =================================%

if exist('logFile','var')
  fclose(logfp);
end

fprintf(logfp, ...
    '\n** Success: SID from GPS time %d to %d folded to 1 sidereal day **\n',...
    params.GPSFirstStart, params.GPSLastEnd);


return

