function [ statistic, invVariance, isStationary, misc ] = ...
    loadSID (SIDFile, GPSStart, segDuration, SigmaCut, ifo1, ifo2)
%
% Load SID from a frame 
%
% [ statistic, invVariance, isStationary ] = ...
%   loadSID (SIDFile, GPSStart, segDuration, SigmaCut, ifo1, ifo2);
%
% SIDFile      : String: File containing the SID
% GPSStart     : Interer: Start time
% segDuration  : Real/Integer: Suration of each segment (possibly 52sec)
% SigmaCut     : Structure: freq bin is tagged non-stationary, if its difference
%                with the local PSD and adjacent PSD differ by more (less) than
%                SigmaCut.maxD (-SigmaCut.minD). SigmaCut.smoothSpan is the
%                number of f-bin to integrate over for Delta Sigma comparison.
%                If it is larger than the total number of f-bin, only the sum
%                of PSD over all frequencies is compared. If it is negative,
%                the so called `sigma` of radiometer point esimate is compared.
%                SigmaCut.maxA is an absolute (non-relative) cut-off on the PSD.
%                SigmaCut.logfp is logfile pointer.
%                (It may be possible to introduce a frequency dependant cut)
% ifo1, ifo2   : Strings: Detectors in the baseline
%
% statistic    : Complex vector: CSD
% invVariance  : Real vector: inverse variance of the statistic
%                To be multiplied with winFactor
% isStationary : Integer vector: 1 if freq bin is stationary, 0 otherwise
% misc         : Structure. Return miscellaneous auxiliary data
%

% 
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


%% Check arguments
if (nargin < 6 | nargin > 6)
  error(['Argument number mismatch. For usage info: help ' mfilename]);
end

misc.readerror = 1;


%======================= Load frequency series data ========================%

% Main SID elements
P1      = frgetvect (SIDFile, [ifo1 ':AdjacentPSD'], GPSStart, segDuration);
P2      = frgetvect (SIDFile, [ifo2 ':AdjacentPSD'], GPSStart, segDuration);

%CSD   = frgetvect (SIDFile, [ifo1 ifo2 ':CSD'], GPSStart, segDuration);
%if length(CSD) <= 1
  reCSD = frgetvect (SIDFile, [ifo1 ifo2 ':ReCSD'], GPSStart, segDuration);
  imCSD = frgetvect (SIDFile, [ifo1 ifo2 ':ImCSD'], GPSStart, segDuration);
  CSD   = complex (reCSD, imCSD);
  clear reCSD, imCSD;
%end

nFreqBin = length(CSD);


% Return values
statistic   = CSD;
invVariance = 1.0 ./ (P1 .* P2); % So far we did not encounter zeros in P1,P2


%============================ Data Quality Cuts ============================%
% The cut is not really applied here, only what should be cut is returned

% Absoulute sigma cut
if SigmaCut.maxA > 0.0
  
  % If smooth range is -ve, use standard DSigma cut using integrated 1/(P_1 P_2)
  if SigmaCut.smoothSpan < 0

    misc.absSigma = sqrt(1.0/sum(SigmaCut.w8.^2 .* invVariance));

    isQuiet = ones(nFreqBin,1) * (misc.absSigma <= SigmaCut.maxA);
  
  % If smoothing window is larger than full f range, disable frequency wise cut
  % These are handwaving formulae, needs to be validated for production runs
  elseif SigmaCut.smoothSpan > nFreqBin

    misc.absSigma1 = sqrt(1.0/sum(SigmaCut.w8./P1));
    misc.absSigma2 = sqrt(1.0/sum(SigmaCut.w8./P2));

    isQuiet = ones(nFreqBin,1) * ( ...
      (misc.absSigma1 <= SigmaCut.maxA) & (misc.absSigma2 <= SigmaCut.maxA));
  
  % If smoothing window is larger than full f range, disable frequency wise cut
  else
    isQuiet = ((P1 <= SigmaCut.maxA) & (P2 <= SigmaCut.maxA));
  end
else
  isQuiet = ones(nFreqBin,1);
end

%% Optional control
%if any(isQuiet < 1)
%  % To reject the whole segment if even one frequency bin is non-stationary
%  %isQuiet(:) = 0;
%
%  fprintf(SigmaCut.logfp,...
%    'WARNING: ASigma cut applied to some f-bins of the segment at GPS %d\n',...
%    GPSStart);
%end


% Assumption: overlapping windows include neighboring segments failing the cut
if (SigmaCut.maxD > 0.0) | (SigmaCut.minD > 0.0)

  naiveP1 = frgetvect(SIDFile,[ifo1 ':LocalPSD'],GPSStart,segDuration);
  naiveP2 = frgetvect(SIDFile,[ifo2 ':LocalPSD'],GPSStart,segDuration);

  % If smooth range is -ve, use standard DSigma cut using integrated 1/(P_1 P_2)
  if SigmaCut.smoothSpan < 0

    misc.thrSigma = sqrt(1.0/sum(SigmaCut.w8.^2 .* invVariance));
    misc.naiSigma = sqrt(1.0/sum(SigmaCut.w8.^2 ./ (naiveP1.*naiveP2)));

    if SigmaCut.minD > 0.0
      isDSigmaSmallMin = (misc.naiSigma >= (SigmaCut.minD * misc.thrSigma));
    else
      isDSigmaSmallMin = 1;
    end
    
    if SigmaCut.maxD > 0.0
      isDSigmaSmallMax = (misc.naiSigma <= (SigmaCut.maxD * misc.thrSigma));
    else
      isDSigmaSmallMax = 1;
    end

    isStationary = (isDSigmaSmallMin & isDSigmaSmallMax) * isQuiet;

  % If smoothing window is larger than full f range, disable frequency wise cut
  elseif SigmaCut.smoothSpan > nFreqBin

    misc.thrSigma1 = sqrt(1.0/sum(SigmaCut.w8./P1));
    misc.thrSigma2 = sqrt(1.0/sum(SigmaCut.w8./P2));
    misc.naiSigma1 = sqrt(1.0/sum(SigmaCut.w8./naiveP1));
    misc.naiSigma2 = sqrt(1.0/sum(SigmaCut.w8./naiveP2));

    if SigmaCut.minD > 0.0
      isDSigmaSmallMin = ( ...
        (misc.naiveP1 >= SigmaCut.minD * misc.thrSigma1) & ...
        (misc.naiveP2 >= SigmaCut.minD * misc.thrSigma2));
    else
      isDSigmaSmallMin = 1;
    end
    
    if SigmaCut.maxD > 0.0
      isDSigmaSmallMax = ( ...
        (misc.naiveP1 <= SigmaCut.maxD * misc.thrSigma1) & ...
        (misc.naiveP2 <= SigmaCut.maxD * misc.thrSigma2));
    else
      isDSigmaSmallMax = 1;
    end

    isStationary = (isDSigmaSmallMin & isDSigmaSmallMax) * isQuiet;
    
  % Otherwise use f wise sigma cuts, but smooth over a given range
  % Incorporating weights here is possible, but should be done with care
  else
  
    % Compare naive and adjacent PSDs
    if SigmaCut.maxD > 0.0
      isDSigmaSmallMax = ...
        ((naiveP1-P1 <= SigmaCut.maxD*P1) & (naiveP2-P2 <= SigmaCut.maxD*P2));
    else
      isDSigmaSmallMax = ones(nFreqBin,1);
    end

    if SigmaCut.minD > 0.0
      isDSigmaSmallMin = ...
        ((P1-naiveP1 >=-SigmaCut.minD*P1) & (naiveP2-P2 >=-SigmaCut.minD*P2));
    else
      isDSigmaSmallMin = ones(nFreqBin,1);
    end

    % Smooth and round the checked values, ignore the end parts
    isStationary = round(smooth(isQuiet,SigmaCut.smoothSpan)) & ...
      round(smooth(isDSigmaSmallMax,SigmaCut.smoothSpan)) & ...
      round(smooth(isDSigmaSmallMin,SigmaCut.smoothSpan));
    isStationary(1:SigmaCut.smoothSpan) = 1;
    isStationary(end-SigmaCut.smoothSpan+1:end) = 1;

  end

else

  % Default is all stationary
  isStationary = isQuiet;
end


if any(isStationary == 0)
  % To reject the whole segment if even one frequency bin is non-stationary
  %isStationary(:) = 0;

  fprintf(SigmaCut.logfp,...
    'WARNING: Sigma cut applied to some f-bins of the segment at GPS %ds\n',...
    GPSStart);
end



%================================= Return ==================================%

clear P1 P2 CSD isQuiet ;

if ((SigmaCut.maxD > 0.0) | (SigmaCut.minD > 0.0))
  clear naiveP1 naiveP2 isDSigmaSmallMax isDSigmaSmallMin;
end

misc.readerror = 0;

return

