function [ param ] = loadSIDParams(SIDFile, ifo1, ifo2, segDuration)
%
% Load SID parameters from a frame
%
%
% param = loadSIDParams (SIDFile, ifo1, ifo2);
%
% SIDFile      : String: File containing the SID
% ifo1, ifo2   : Strings: Detectors in the baseline
% segDuration  : Real/Integer: Suration of each segment (possibly 52sec)
%
% param        : Structure: Different output parameters
%

% 
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


%% Check arguments
if (nargin < 4 | nargin > 4)
  error(['Argument number mismatch. For usage info: help ' mfilename]);
end


% Insert parameters in the output structure

param.segmentDuration = ...
  frgetvect(SIDFile,[ifo1 ifo2 ':segmentDuration'], 0, segDuration);

param.flow              = ...
  frgetvect(SIDFile,[ifo1 ifo2 ':flow'], 0, segDuration);
param.fhigh             = ...
  frgetvect(SIDFile,[ifo1 ifo2 ':fhigh'], 0, segDuration);
param.deltaF            = ...
  frgetvect(SIDFile,[ifo1 ifo2 ':deltaF'], 0, segDuration);

param.w1w2bar           = ...
  frgetvect(SIDFile,[ifo1 ifo2 ':w1w2bar'], 0, segDuration);
param.w1w2squaredbar    = ...
  frgetvect(SIDFile,[ifo1 ifo2 ':w1w2squaredbar'], 0, segDuration);
param.w1w2ovlsquaredbar = ...
  frgetvect(SIDFile,[ifo1 ifo2 ':w1w2ovlsquaredbar'], 0, segDuration);

param.bias              = ...
  frgetvect(SIDFile,[ifo1 ifo2 ':bias'], 0, segDuration);
param.naivebias         = ...
  frgetvect(SIDFile,[ifo1 ifo2 ':naivebias'], 0, segDuration);

% Or load all the stored parameters at once: Does it work?
%param = frgetparams(SIDFile,[ifo1 ifo2 ':Params']);


return

