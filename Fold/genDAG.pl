#! /usr/bin/perl

$USAGE ='
USAGE: genDAG.pl DAGFile nProc firstSeg lastSeg paramsFile matFilePrefix outFile

DAGFile       : DAG file name
nProc         : # of processes for the whole job
firstSeg      : First segment to process from the SID metadata file
                Use -1 for default (first available segment in metadata file)
lastSeg       : Last segment to process from the SID metadata file
                Use -1 for default (last available segment in metadata file)
paramsFile    : Parameter file
matFilePrefix : Prefix to the .mat output files for each parallel fold process
outFile       : (OPTIONAL) Combine output .mat files from parallel processes and
                write to .mat outFile and also frames if specified in paramsFile

NOTE: Output of each process will probably be ~1GB, that is, total ~nproc GB,
      choose nProc accordingly. (Rule of thumb: 1 process per month of data)

';

#
# Author: Sanjit Mitra <sanjit.mitra@ligo.org>
#


# Check number of arguments
if (@ARGV < 6) { die $USAGE; }
if (@ARGV > 6) { $doCombine = 1; }


# Read command line arguments
$DAGFile       = shift;
$nProc         = shift;
$firstSeg      = shift;
$lastSeg       = shift;
$paramsFile    = shift;
$matFilePrefix = shift;
if ($doCombine) { $outFile = shift; }


# Check arguments
if ($nProc < 1) { die "ERROR: nProc should be a positive integer\n"; }
if (! -r $paramsFile) { die "ERROR: Can't read $paramsFile\n"; }


# Extract parameters from parameter file
$SIDMetaDataFileParamSet=0;
open(par,"< $paramsFile") or die "ERROR: Can't read $paramsFile : $!";
while (<par>) {
  if (($_ !~ m/^\s*%/) && ($_ =~ m/SIDMetaDataFile/)) {
    $SIDMetaDataFile = $_;
    $SIDMetaDataFile =~ s/\s*SIDMetaDataFile\s*//;
    $SIDMetaDataFile =~ s/\s*$//;
  
    $SIDMetaDataFileParamSet=1;
    print "SID metadata file: $SIDMetaDataFile\n";
  }
}
close (par);
if (! $SIDMetaDataFileParamSet) { die "ERROR: SIDMetaDataFile is not set\n"; }


# Count lines which are not blank or commented by % or #
$SIDMetaDataFileRead=1;
open(meta,"< $SIDMetaDataFile") or $SIDMetaDataFileRead=0;
if ( $SIDMetaDataFileRead ) {
  $availSeg = 0;
  while (<meta>) {
    if (($_ !~ m/^\s*%/) && ($_ !~ m/^\s*#/) && ($_ !~ m/^\s*$/)) {
      $availSeg++;
    }
  }
  close(meta);

  if ( ! $availSeg ) { die "ERROR: SIDMetaDataFile is empty\n"; }
}


# Default segment numbers
# first
if ($firstSeg == -1) {
  $firstSeg = 1;
}
elsif ($firstSeg < 1) {
  die "ERROR: firstSeg should be an integer > 0, or -1 for default\n";
}
elsif ($firstSeg > $availSeg) {
  die "ERROR: firstSeg ($firstSeg) should be less than available ($availSeg)";
}

# last (requires the SID metadata file)
if ($lastSeg == -1) {
  if (! $SIDMetaDataFileRead) {
    die "ERROR: SIDMetaDataFile must exist to obtain default lastSeg\n";
  }
  else { $lastSeg = $availSeg; }
}
elsif ($lastSeg < 1) {
  die "ERROR: lastSeg should be an integer > 0, or -1 for default\n";
}
elsif ($lastSeg > $availSeg) {
  die "ERROR: lastSeg ($lastSeg) should be less than available ($availSeg)";
}


# Minimum # segments each process should handle
$nSeg = ($lastSeg - $firstSeg + 1);
$procSegs = int($nSeg/($nProc-1)) - 1;


# Verbose
if (! $SIDMetaDataFileRead) {
  print "NOTE: SIDMetaDataFile could not be read, believing user inputs!\n";
  print "The file is needed at runtime & must have at least $lastSeg entries\n";
}
print "First segment to process: $firstSeg\n";
print "Last segment to process: $lastSeg\n";
print "# processes: $nProc\n";
print "approx # segments per process: $procSegs\n";
print "Prefix to the output mat files: $matFilePrefix\n";
print "Common parameter file: $paramsFile\n";
print "Output DAG file: $DAGFile\n";
if ($doCombine) { print "Combined FSID will be written to: $outFile\n"; }

open(dag,">$DAGFile");

# Define environmental variables
$LD_LIBRARY_PATH = "$ENV{'LD_LIBRARY_PATH'}";
$HOME = "$ENV{'HOME'}";
$USER = "$ENV{'USER'}";



#======================= Fold by parallel processes =======================#

for ($proc = 1; $proc < $nProc; $proc++) {

  # First and last segment for this processor
  $procFirstSeg  = $lastSeg - ($nProc - $proc)*$procSegs + 1;
  $procLastSeg = $procFirstSeg + $procSegs - 1;

  # File to store output for this processor
  $matFile = sprintf("%s.%d.mat",$matFilePrefix,$proc);

  # Write to DAG file
  print dag "JOB $proc fold.sub\n";
  print dag "VARS $proc";
  print dag ' proc="'; print dag "$proc"; print dag '"';
  print dag ' paramsFile="'; print dag "$paramsFile"; print dag '"';
  print dag ' firstSeg="'; print dag "$procFirstSeg"; print dag '"';
  print dag ' lastSeg="'; print dag "$procLastSeg"; print dag '"';
  print dag ' matFile="'; print dag "$matFile"; print dag '"';
  print dag ' home="'; print dag "$HOME"; print dag '"';
  print dag ' user="'; print dag "$USER"; print dag '"';
  print dag ' ld_library_path="'; print dag "$LD_LIBRARY_PATH"; print dag '"';
  print dag "\n\n";
}


# Write the first job at last
# This process will have all the remaining process
# The reason is that one can not divide 100 segments among 11 processes and
# 99 processes among 10 processes using a same simple scheme
$proc = 0;

# First and last segment for this processor
$procFirstSeg = $firstSeg;
$procLastSeg  = $lastSeg - $procSegs*($nProc-1);

# File to store output for this processor
$matFile = sprintf("%s.%d.mat",$matFilePrefix,$proc);

# Write to DAG file
print dag "JOB $proc fold.sub\n";
print dag "VARS $proc ";
print dag ' proc="'; print dag "$proc"; print dag '"';
print dag ' paramsFile="'; print dag "$paramsFile"; print dag '"';
print dag ' firstSeg="'; print dag "$procFirstSeg"; print dag '"';
print dag ' lastSeg="'; print dag "$procLastSeg"; print dag '"';
print dag ' matFile="'; print dag "$matFile"; print dag '"';
print dag ' home="'; print dag "$HOME"; print dag '"';
print dag ' user="'; print dag "$USER"; print dag '"';
print dag ' ld_library_path="'; print dag "$LD_LIBRARY_PATH"; print dag '"';
print dag "\n\n";


# Write job graph
# MATLAB compiled binary should be run first by a single process
# This ensures that the directories are fully created by MATLAB binary
# Otherwise, a parallel process may think the directory exists,
# but it will not have all the files and crash
print dag "PARENT 0 CHILD ";
for ($proc=1; $proc<$nProc; $proc++) { print dag "$proc "; } print dag "\n\n";



#=========== (OPTIONAL) Combine results from parallel processes ===========#

if ($doCombine) {
  $proc = $nProc;

  # Write to DAG file
  print dag "JOB $proc combine.sub\n";
  print dag "VARS $proc ";
  print dag ' proc="'; print dag "$proc"; print dag '"';
  print dag ' outFile="'; print dag "$outFile"; print dag '"';
  print dag ' inFiles="'; print dag "$matFilePrefix*"; print dag '"';
  print dag ' home="'; print dag "$HOME"; print dag '"';
  print dag ' user="'; print dag "$USER"; print dag '"';
  print dag ' ld_library_path="'; print dag "$LD_LIBRARY_PATH"; print dag '"';
  print dag "\n\n";


  # Combine .mat files after every processor has written output
  print dag "PARENT ";
  for ($proc=0; $proc<$nProc; $proc++) { print dag "$proc "; }
  print dag "CHILD "; print dag $proc;
  print dag "\n\n";
}



#=============================== Finalize =================================#
close(dag);
print "DAG file written\n";

