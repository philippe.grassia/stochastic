#!/bin/sh +x
#

if [ $# -eq 2 ]
then
  dataDir=$2

elif [ $# -eq 3 ]
then
  GPSStart=$2
  GPSEnd=$3

else
  echo "USAGE: $0 <outputSIDMetaDataFile> <GPSStart GPSEnd> OR <directory>"
  echo " "
  echo "Zero byte files will be excluded from meta data and printed on screen"
  echo "If <GPSStart GPSEnd> is supplied ligo_data_find will be used, OR if"
  echo "<directory> is supplied the find command will be used to list frames."
  echo "The list of frames (metadata) is sorted on GPS start times."
  exit 1
fi

outputSIDMetaDataFile=$1

# Generate three temporary files
fileNames=`mktemp -p . -t SIDFileNames.XXXXXXXXXX` || exit 1
GPSTimes=`mktemp -p . -t GPSTimes.XXXXXXXXXX` || exit 1
unsorted=`mktemp -p . -t unsorted.XXXXXXXXXX` || exit 1


# LIGO Data Find way
if [ "x$dataDir" = "x" ]
then
  ligo_data_find -o HL -t SIDv1_H1L1_250mHz -s $GPSStart -e $GPSEnd \
  -u file -m localhost | cut -f4- -d'/' | sed 's/^/\//' | \
  while read filename; do
    if [ -s $filename ]
    then
      echo $filename >> $fileNames
    else
      ls -l $filename
    fi
  done

else
# If the data directory is known
  find $dataDir \( -perm -u+r \) -name "HL-SIDv1_H1L1_*.gwf" | sort -n |\
  while read filename; do
    if [ -s $filename ]
    then
      echo $filename >> $fileNames
    else
      ls -l $filename
    fi
  done
fi


# If the exact file paths across the nodes are known
#firstNode=1
#lastNode=331
#
#for ((  i = firstNode ;  i <= lastNode ;  i++  ))
#do
#  find /data/node${i}/ethrane \( -perm -u+r \) -name "HL-SIDv1_H1L1_250mHz*" | grep gwf >> $fileNames
#done



cut -f5 -d'-' $fileNames > $GPSTimes  
paste $GPSTimes $fileNames > $unsorted
#uniq -w 10 $unsorted | sort -n > $outputSIDMetaDataFile
sort -n $unsorted | uniq -w 10 > $outputSIDMetaDataFile

rm -f $fileNames $GPSTimes $unsorted

exit 0

