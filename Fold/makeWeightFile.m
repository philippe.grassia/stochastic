addpath('/usr/share/ligotools/matlab/');

% Read parameters from a frame
param = loadSIDParams('/archive/frames/S5/SGWB/LHO/HL-SIDv1_H1L1_250mHz-8608/HL-SIDv1_H1L1_250mHz-860840374-26.gwf', 'H1', 'L1', 52);


% Compulsory frequency array
f = [param.flow:param.deltaF:param.fhigh];
param.fhigh = f(end);

% Either use the SID range or use a band to limit integration
myflow  = 40.0;
myfhigh = 500.0;
%myflow  = param.flow;
%myfhigh = param.fhigh;


% Read overlap reduction function!
% Get the one being used in stochastic.m
gammadata = load('../input/isoGamma.mat');
f_gamma = gammadata.flow + gammadata.deltaF*(0:length(gammadata.isoGamma)-1);
gamma = interp1(f_gamma,gammadata.isoGamma,f,'linea',0.0);
clear gammadata f_gamma;
% The one that matches AllenRomano-2001
%gammadata = load('../input/ORF-HL.mat');
%gamma = (5/8/pi)*interp1(gammadata.freqBins,gammadata.gammaIso,f,'linea',0.0);
%clear gammadata;


% Source PSD
useHf = true;

if useHf
  % Read H(f) from a file
  Hdata = load('/home/sanjit.mitra/Fold/FSID/input/Hf.txt');
  H = exp(interp1(Hdata(:,1),log(Hdata(:,2)), f,'linear','extrap'));
else
  % Or, provide a power law
  alpha = 3;
  fRef  = 100.0;
  H = (f/fRef).^alpha ./ f.^3;
end

clear Hdata;


% Mask regions outside intended zone
if (myflow > param.flow)
  H(1:floor((myflow - param.flow - realmin)/param.deltaF)) = 0.0;
end

if (myfhigh < param.fhigh)
  H(ceil((myfhigh - param.flow + realmin)/param.deltaF):end) = 0.0;
end

% Array of frequency masks
% ** IMPORTANT: without this quality cuts are useless **
mask = [48.00,52.75,60.00,108.50,108.75,109.00,120.00,147.75,148.00,148.25,180.00,193.25,193.50,193.75,240.00,265.50,300.00,343.00,343.25,343.50,343.75,344.00,344.50,344.75,345.00,346.75,347.00,347.25,347.50,347.75,360.00,420.00,480.00];

% Indices to mask
masxidx = floor((mask-param.flow)/param.deltaF) + 1;

% Isotropic Gamma is being used to match stochastic pipeline
w = H .* gamma;
w(masxidx) = 0.0;

fid=fopen('../input/weights.txt','w');
fprintf(fid,'%f\t%e\n',[f;w]);
fclose(fid);

