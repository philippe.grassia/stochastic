#!/bin/bash +x
#

if [ $# -eq 3 ]
then
  cacheDir=$1
  jobStart=$2
  jobEnd=$3

else
  echo "USAGE: $0 <cachedir> <jobStart> <jobEnd>"
  echo " "
  echo "Make SIDMetaData file by joining cachefiles."
  echo "Output will be written to stdout."
  exit 1
fi

# Hardcoded variables
frameFilePrefix=frameFilesHL.
gpsFilePrefix=gpsTimesHL.
frameFileSuffix=.txt
gpsFileSuffix=.txt

for (( job=$jobStart; job<$jobEnd; job++ ))
do
  paste \
    ${cacheDir}/${gpsFilePrefix}${job}${gpsFileSuffix} \
    ${cacheDir}/${frameFilePrefix}${job}${frameFileSuffix}
done

exit 0

