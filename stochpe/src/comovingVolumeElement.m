function dVc_dz = comovingVolumeElement(z,CosmoParams);
% CALCULATING THE COMOVING TRANSVERSE DISTANCE
% SEE HOGG (ARXIV:ASTRO-PH/9905116) EQ.(16)

% PARSE COSMOLOGICAL PARAMETERS
H0= CosmoParams.H0;
Om = CosmoParams.Om;
Ol = CosmoParams.Ol;
Or = CosmoParams.Or;
Ok = 1.-Om-Ol-Or;
C = 3.e8;

% CALCULATE RELEVANT PARAMETERS
DH = C./(H0.*1e3);   % [Mpc]
Dm = comovingTransverseDistance(z, CosmoParams);
Ez = hubbleParameter(z,CosmoParams);

% CALCULATE COMOVING VOLUME ELEMENT
dVc_dz = 4.0*pi*DH*Dm^2/Ez;
