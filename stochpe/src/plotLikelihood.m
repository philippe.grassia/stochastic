function plotLikelihood(datafile)
% function plotLikelihood
%		input: datafile
%		output: plots the log likelihood in logL.png

%==============================================================================
% PERFORM MCMC EXPLORATION (METROPOLIS-HASTINGS) ON THE SGWB
%==============================================================================

addpath('/home/tgfli/projects/matapps/releases/utilities/detgeom/src')

%------------------------------------------------------------------------------
% USER INPUT
%------------------------------------------------------------------------------

cosmoParams.H0=70;
cosmoParams.Om=0.3;
cosmoParams.Ol=0.6;
cosmoParams.Or=0.0;
cosmoParams.Ok=0.0;

sfrParams.r0=5.0e-12;
sfrParams.W=45.0;
sfrParams.R=3.8;
sfrParams.Q=3.4;

massFuncParams.lambda_bbh = 1*2.2e-3; % []
massFuncParams.mu_bbh = 6.72; % [Msun]
massFuncParams.sigma_bbh = 0.05; % [Msun]
massFuncParams.lambda_bns = 1*2.2e-3; % []
massFuncParams.mu_bns = 1.05; % [Msun]
massFuncParams.sigma_bns = 0.05; % [Msun]

%------------------------------------------------------------------------------
% CALCULATE AND PLOT THE LIKELIHOOD
%------------------------------------------------------------------------------

% LOAD THE DATA (FROM SIMULATEDATA.M)
data = dlmread(datafile); % load strain spectral density
f = data(:,1).';
ogw_measured = data(:,2).';
sgw_measured = data(:,3).';

% PRINT THE LOGL FUNCTION TO FILE
n=10;
mcarray = linspace(0,10,n);
logLarray = zeros(0,n);
for i=1:n
	massFuncParams.mu_bbh=mcarray(i);
	ogw_trial = omegaGW(f,6.0,sfrParams, cosmoParams,massFuncParams);
	logLarray(i) =likelihood(ogw_trial, ogw_measured, sgw_measured);
end

% PLOT THE FUNCTION
figure('visible','off');
plot(mcarray, logLarray);
grid on;
xlabel('mu_bbh (Msun)');
ylabel('logL');
pretty;
print('-dpng','logL');

end
