function Dc = comovingLineOfSightDistance(z,CosmoParams);
% CALCULATING THE COMOVING LINE OF SIGHT DISTANCE
% SEE HOGG (ARXIV:ASTRO-PH/9905116) EQ.(15)

H0 = CosmoParams.H0; % [km/s/Mpc]
Om = CosmoParams.Om;
Ol = CosmoParams.Ol;
Or = CosmoParams.Or;
Ok = 1.-Om-Ol-Or;
C=3.e8; % [m/s]

DH = C./(H0.*1e3);   % [Mpc]

Fun = inline('1./hubbleParameter(z,CosmoParams)','z','CosmoParams');
Dc = DH.*quadv(Fun,0,z,[],[],CosmoParams); % [Mpc]
