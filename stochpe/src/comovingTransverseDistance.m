function Dm = comovingTransverseDistance(z,CosmoParams);
% CALCULATING THE COMOVING TRANSVERSE DISTANCE
% SEE HOGG (ARXIV:ASTRO-PH/9905116) EQ.(16)

% PARSE COSMOLOGICAL PARAMETERS
H0 = CosmoParams.H0; % [km/s/Mpc]
Om = CosmoParams.Om;
Ol = CosmoParams.Ol;
Or = CosmoParams.Or;
Ok = 1.-Om-Ol-Or;
C=3.e8; % [m/s]
eps = 1.e-7;

DH = C./(H0.*1e3);   % [Mpc]
sqrtOk = sqrt(abs(Ok));
Dc = comovingLineOfSightDistance(z,CosmoParams); % [Mpc]

if (Ok > eps)
	Dm = DH/sqrtOk * sinh(sqrtOk*Dc/DH); % [Mpc]
elseif (Ok < -1.0*eps);
	Dm = DH/sqrtOk * sin(sqrtOk*Dc/DH); % [Mpc]
else
	Dm = Dc; % [Mpc]
end

