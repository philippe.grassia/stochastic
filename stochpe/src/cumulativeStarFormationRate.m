function Rz = sfrRate(z,StarFormationRateParameters,CosmoParams);
% INTEGRATE THE DIFFERENTIAL STAR FORMATION RATE TO GET THE CUMULATIVE STAR
% FORMATION RATE

if z<=0
	Rz = 0
else
	Fun = inline('differentialStarFormationRate(z,StarFormationRateParameters,CosmoParams)','z','StarFormationRateParameters','CosmoParams');
	Rz = quadv(Fun,0,z,[],[],StarFormationRateParameters,CosmoParams);
end

