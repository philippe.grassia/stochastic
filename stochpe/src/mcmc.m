function mcmc(datafile,seed,numIterations)
% function mcmc SEED NUMITERATIONS
%		input: datafile to the random number generator
%		input: SEED to the random number generator
%		input: NUMITERATIONS number of samples to generate
%		output: samples are saved to state_SEED.txt

%==============================================================================
% PERFORM MCMC EXPLORATION (METROPOLIS-HASTINGS) ON THE SGWB
%==============================================================================

seed = strassign(seed);
numIterations = strassign(numIterations);

%------------------------------------------------------------------------------
% USER INPUT
%------------------------------------------------------------------------------
cosmoParams.H0=70;
cosmoParams.Om=0.3;
cosmoParams.Ol=0.6;
cosmoParams.Or=0.0;
cosmoParams.Ok=0.0;
sfrParams.r0=5.0e-12;
sfrParams.W=45.0;
sfrParams.R=3.8;
sfrParams.Q=3.4;
massFuncParams.lambda_bbh = 1*2.2e-3; % []
massFuncParams.mu_bbh = 6.72; % [Msun]
massFuncParams.sigma_bbh = 0.05; % [Msun]
massFuncParams.lambda_bns = 1*2.2e-3; % []
massFuncParams.mu_bns = 1.05; % [Msun]
massFuncParams.sigma_bns = 0.05; % [Msun]

rng(seed); % seed the random number generator with the passed in seed

%------------------------------------------------------------------------------
% CALCULATE MEASURED SPECTRUM AND STDEV
%------------------------------------------------------------------------------

data = dlmread(datafile); % load strain spectral density

f = data(:,1).';
ogw_measured = data(:,2).';
sgw_measured = data(:,3).';

% BURNIN
B = 10; % number of burnin steps
burnin=zeros(4,B);
t = 1; % counter

logLold = -flintmax;
while t<B+1
	% SAMPLE PROPOSAL DISTRIBUTIONS
	massFuncParams.mu_bbh = unifrnd(3,10); % sample proposal distribution
	massFuncParams.sigma_bbh = unifrnd(0,1); % [Msun]
	massFuncParams.mu_bns = unifrnd(0,3); % sample proposal distribution
	massFuncParams.sigma_bns = unifrnd(0,1); % [Msun]

	% CALCULATE LIKELIHOOD
	ogw_trial = omegaGW(f,6.0,sfrParams, cosmoParams,massFuncParams); % calculate spectrum
	logLnew =likelihood(ogw_trial, ogw_measured, sgw_measured); % calculate the likelihood

	% METROPOLIS HASTINGS
	loga = min([0 logLnew - logLold]); % calculate acceptance ratio
	logu = log(rand); % get random number to compare to acceptance ratio
	if logu<loga
		burnin(1,t) = massFuncParams.mu_bbh;
		burnin(2,t) = massFuncParams.sigma_bbh;
		burnin(3,t) = massFuncParams.mu_bns;
		burnin(4,t) = massFuncParams.sigma_bns;
		logLold = logLnew;
	else
		burnin(:,t) = burnin(:,t-1);
	end
	t = t+1;
end

% METROPOLIS-HASTINGS
T = numIterations; % maximum number of iterations
state = zeros(4,T);
t = 2; % counter
state(:,1) = burnin(:,B);
while t<T+1
	% SAMPLE PROPOSAL DISTRIBUTIONS
	massFuncParams.mu_bbh = unifrnd(3,10); % sample proposal distribution
	massFuncParams.sigma_bbh = unifrnd(0,1); % [Msun]
	massFuncParams.mu_bns = unifrnd(0,3); % sample proposal distribution
	massFuncParams.sigma_bns = unifrnd(0,0.2); % [Msun]

	% CALCULATE LIKELIHOOD
	ogw_trial = omegaGW(f,6.0,sfrParams, cosmoParams,massFuncParams); % calculate spectrum
	logLnew =likelihood(ogw_trial, ogw_measured, sgw_measured); % calculate the likelihood

	% METROPOLIS HASTINGS
	loga = min([0 logLnew - logLold]); % calculate acceptance ratio
	logu = log(rand); % get random number to compare to acceptance ratio
	if logu<loga
		state(1,t) = massFuncParams.mu_bbh;
		state(2,t) = massFuncParams.sigma_bbh;
		state(3,t) = massFuncParams.mu_bns;
		state(4,t) = massFuncParams.sigma_bns;
		logLold = logLnew;
	else
		state(:,t) = state(:,t-1);
	end
	t = t+1;
end

% PRINT OUTPUT
state = state';
filename = strcat('state_',num2str(seed),'.txt');
save(filename,'state','-ascii')

end
