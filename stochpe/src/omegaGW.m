function ogw = omegaGW(f,zmax, StarFormationRateParameters, CosmoParams,MassFuncParams);

McKG=1.9981e30; % covert Mc from solar mass to kg
G = 6.67384e-11; %[m^3/kg/s^2]
c=3.0e8; % [m/s]

%------------------------------------------------------------------------------
% CALCULATE FRACTIONAL ENERGY DENSITY AS A FUNCTION OF FREQUENCY
%------------------------------------------------------------------------------

% DETERMINE MAXIMUM REDSHIFT CONTRIBUTION
prefac = 8*pi*G*(McKG)^(1.66)*f/(3.0*(CosmoParams.H0*1000.0)^3*c^2);

% SETUP GRID INTEGRATION
zi = linspace(0,zmax,100);
mci = linspace(0.01,25,500);
[Z MC F] = meshgrid(zi,mci,f);
%[const MCfactor FMAX] = calculatePrefactor(F,MC,Z,CosmoParams);
% PIECES FOR THE INTEGRAND
zint = starFormationRateDensityObserverFrame(Z,StarFormationRateParameters, CosmoParams)./(hubbleParameter(Z,CosmoParams).*(1+Z).^(1/3));
ogw = 0;
for mfp = MassFuncParams
	[Egwf FMAX] = singleMergerEnergySpectrum(MC,F,mfp);

	% CREATE ZUP MASK TO REPRESENT INTEGRATION LIMITS
	ZFMAX = FMAX./F-1;
	MASK = Z < ZFMAX;
	% PERFORM SUM
	I = zint.*Egwf.*MASK;
	ogw_i = prefac.*squeeze(sum(squeeze(sum(I,2)),1))'*zi(2)*mci(2);
	ogw = ogw + ogw_i;
%ogw  = prefac.*squeeze(sum(squeeze(sum(I,2)),1))'*zi(2)*mci(2);

%ogw = sum(I(:))*zi(2)*mci(2);
end
