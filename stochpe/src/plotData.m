function plotData(datafile)
% function plotData DATAFILE
%		input: SEED to the random number generator
%		saves output file ogw_measured_SEED.dat
%		saves ogw_measured.png

%------------------------------------------------------------------------------
% LOAD DATA
%------------------------------------------------------------------------------
data = dlmread(datafile); % load strain spectral density

f = data(:,1).';
ogw_measured = data(:,2).';
sgw_measured = data(:,3).';
ogw_true = data(:,4).';

%------------------------------------------------------------------------------
% PLOT DATA
%------------------------------------------------------------------------------

figure('Visible','off');
loglog(f,abs(ogw_measured),f,sgw_measured,f,ogw_true);
legend('Ogw simulated', 'sigma_GW', 'Ogw true','Location','Best');
grid on;
axis([10 2048 1e-10 1e-2]);
xlabel('f (Hz)');
ylabel('sigma Ogw');
pretty;
[pathstr,name,ext] = fileparts(datafile);
print('-dpng',name);

end
