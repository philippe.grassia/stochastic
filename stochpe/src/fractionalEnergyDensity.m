function Ogw = fractionalEnergyDensity(f,zmax,StarFormationRateParameters,CosmoParams,MassFuncParams);
% CALCULATE THE FRACTIONAL ENERGY DENSITY OF A COMPACT BINARY (NS)

%------------------------------------------------------------------------------
% DEFINE SOME CONVERSIONS FACTORS
%------------------------------------------------------------------------------
McKG=1.9981e30; % covert Mc from solar mass to kg
G = 6.67384e-11; %[m^3/kg/s^2]
c=3.0e8; % [m/s]

%------------------------------------------------------------------------------
% CALCULATE FRACTIONAL ENERGY DENSITY AS A FUNCTION OF FREQUENCY
%------------------------------------------------------------------------------

% DETERMINE MAXIMUM REDSHIFT CONTRIBUTION
prefac = 8*(pi*G*McKG)^(1.66).*f.^(0.66)/(9.0*(CosmoParams.H0*1000.0)^3*c^2);

% INTEGRATE OVER THE CHIRPMASS SPACE
dm = 0.05;
mc = dm:dm:20;

% GET THE CHIRP MASS DISTRIBUTION
pmc = chirpMassProbability(mc,MassFuncParams);

McSec = mc*5.0e-6;
fmax = 1.0./(6.0^(1.5)*pi*McSec*0.25^(-0.6)); % fmax=flso, [/s]
cut = (f<fmax/(1.0+zmax));
z(cut) = zmax;
z(~cut) = fmax(~cut)./f-1.0;

% GET INTEGRAND
%volint = volumeIntegral(mc,f,zmax,StarFormationRateParameters,CosmoParams);
mintegrand = pmc.*mc.^(5/3);

% RIEMANN SUM INTEGRATION
Ogw = prefac*sum(mintegrand.*volint)*dm;

end
