function eht_run_all(seed)
% function eht_run_all(seed)

% first simulate data to create output dat file
% It will create an output file called ogw_measured_1.dat.  It consists of 
% four columns: f, \hat{Omega}, sigma, \Omega_true
% This data is H1L1, design sensitivity, one year of integration time.  It 
% contains a CBC background signal + simulated noise.
simulateData('ZERO_DET_high_P.txt', seed);

% Next, we run mcmc.  This code reads in the data file produced by 
% simulateData.  We will analyze it with only 1000 iterations so that it runs 
% quickly.  This will create a file called state_1.txt.  It consists of four 
% columns: mu_bbh, sigma_bbh, mu_bns, sigma_bns
% There are 1000 rows, one for every iteration. By producing a binned histogram
% of these ordered quadruplets, (mu1, sigma1, mu2, sigma2), we can produce a 
% probability density function.  In order to marginalize over, e.g., two 
% parameters, simply ignore them, when binning.
mcmc(['ogw_measured_' num2str(seed) '.dat'], seed, 1000);

% Armed with our list of ordered quadruplets, we can make some posterior plots.
% like so:
plotPosterior(['state_' num2str(seed) '.txt']);
% creating these plots: plotPosterior_mu.png  plotPosterior_sigma.png

% As a sanity check, plot the theoretical posterior:
plotLikelihood(['ogw_measured_' num2str(seed) '.dat']);
% creating this plot: logL.png

return