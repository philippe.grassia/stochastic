function logL = likelihood(ocalc, omeas, smeas);
logL = -1*sum(((omeas-ocalc)./smeas).^2);
end
