function sfrd = starFormationRateDensityObserverFrame(z, StarFormationRateParameters, CosmoParams);
% CODE UP PHEMENOLOGICAL MODEL OF THE STAR FORMATION RATE
% FROM COWARD AND BURMAN (ARXIV:ASTRO-PH/0505181)

% GET PHEMENOLOGICAL STAR FORMATION RATE PARAMETERS
r0 = StarFormationRateParameters.r0; % [/s/Mpc^3]
W = StarFormationRateParameters.W;
Q = StarFormationRateParameters.Q;
R = StarFormationRateParameters.R;

% CALCULATE SCALEFACTOR DESCRIBED IN EQ.(8)
hz = hubbleParameter(z,CosmoParams);
scalefactor = hz.*(1+z).^(-3/2);

% STAR FORMATION RATE DENSITIY
sfrd = r0*(1.0+W)*scalefactor.*exp(Q*z)./((1+z).*exp(R*z)+W); % extra factor of 1+z to calculate the observer rate, [/s/Mpc^3]
