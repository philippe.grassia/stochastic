function run_duty(gpsStart,gpsEnd,rate,T,doPSDCut)

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

fs = 256;
fref = 50;
fmin = 10;
fmax = 100;

frames = gpsStart:(T/2):gpsEnd;

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots/%.10f/%d/%d',rate,T,doPSDCut);
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

params.outputDir = outputDir;
params.spectraFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd.txt';
params.waveformFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/duty/waveforms/SEOBNRv2_30_30.dat';
params.T = T;
params.rate = rate;
params.gpsStart = gpsStart;
params.gpsEnd = gpsEnd;
params.fs = fs;
params.fref = fref;
params.fmin = fmin;
params.fmax = fmax;
params.doPlots = 1;
params.doPSDCut = doPSDCut;

spectra = load(params.spectraFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);
mc.transfer(:,1) = spectra_f;
mc.transfer(:,2) = spectra_h;

[t_all,r1_all,r2_all,r1_signal_all,r2_signal_all] = gen_data_bbh(params);

[psd1_tot,psd2_tot,psd1_signal_tot,psd2_signal_tot] = ...
   gen_psd(params,t_all,r1_all,r2_all,r1_signal_all,r2_signal_all);

highPassOrder = 6;
highPassFreq = 5; %Hz
highPassFreqNorm = highPassFreq/(fs/2);
[b,a] = butter(highPassOrder, highPassFreqNorm, 'high');
N = length(frames)-3;
times = frames(1:N);

count = 0;
for ii = 1:N

   if mod(ii,100) == 1
      fprintf('%d/%d\n',ii,N);
   end

   frameStart = frames(ii);
   frameEnd = frames(ii+2);

   indexes = find(t_all >= frameStart & t_all <= frameEnd);
   indexes = indexes(1:end-1);

   t = t_all(indexes);
   r1 = r1_all(indexes); r2 = r2_all(indexes);
   r1_signal = r1_signal_all(indexes); r2_signal = r2_signal_all(indexes);

   if params.doPSDCut
      r1_signal_tot = sum(r1_signal);
      r2_signal_tot = sum(r2_signal);

      if (r1_signal_tot == 0) && (r2_signal_tot == 0)
         continue
      end
   end

   window = hann(fs*T)';
   %window = ones(size(window));

   [w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar] = ...
  windowFactors(window, window);

   r1 = r1.*window;
   r2 = r2.*window;
   r1_signal = r1_signal.*window;
   r2_signal = r2_signal.*window;

   if highPassOrder > 6
      r1 = cascadefilter(r1, highPassOrder, ...
         highPassFreq, fs);
      r2 = cascadefilter(r2, highPassOrder, ...
         highPassFreq, fs);
      r1_signal = cascadefilter(r1_signal, highPassOrder, ...
         highPassFreq, fs);
      r2_signal = cascadefilter(r2_signal, highPassOrder, ...
         highPassFreq, fs);
   else
      r1 = filtfilt(b,a,r1);
      r2 = filtfilt(b,a,r2);
      r1_signal = filtfilt(b,a,r1_signal);
      r2_signal = filtfilt(b,a,r2_signal);
   end
   L = length(t);
   NFFT = 2^nextpow2(L); % Next power of 2 from length of y
   f = fs/2*linspace(0,1,NFFT/2+1);
   deltaF = f(2) - f(1);
   indexes = find(f >= fmin & f <= fmax);
   f = f(indexes);

   if count == 0
      r1_sum = zeros(length(f),1);
      r2_sum = zeros(length(f),1);
      psd1_sum = zeros(length(f),1);
      psd2_sum = zeros(length(f),1);
      r1r2_sum = zeros(length(f),1);

      r1_signal_sum = zeros(length(f),1);
      r2_signal_sum = zeros(length(f),1);
      psd1_signal_sum = zeros(length(f),1);
      psd2_signal_sum = zeros(length(f),1);
      r1r2_signal_sum = zeros(length(f),1);

      ys = zeros(N,1); sigmas2 = zeros(N,1);
      pgws = zeros(N,1);
   end

   sT = floor(NFFT/fs);

   y1 = fft(r1,NFFT);
   y1 = y1(1:NFFT/2+1);
   y1 = y1(indexes);
   y1 = y1 / fs;

   psd1 = 2*(abs(y1).^2)/(T*w1w2bar);

   y2 = fft(r2,NFFT);
   y2 = y2(1:NFFT/2+1);
   y2 = y2(indexes);
   y2 = y2 / fs;

   psd2 = 2*(abs(y2).^2)/(T*w1w2bar);

   %psd1 = interp1(spectra_f,spectra_h,f) * (L*fs*windowconst)/2;
   %psd2 = interp1(spectra_f,spectra_h,f) * (L*fs*windowconst)/2;

   psd1 = psd1_tot';
   psd2 = psd2_tot';

   r1r2 = y1 .* conj(y2);

   y1_signal = fft(r1_signal,NFFT);
   y1_signal = y1_signal(1:NFFT/2+1);
   y1_signal = y1_signal(indexes);
   y1_signal = y1_signal / fs;

   psd1_signal = 2*(abs(y1_signal).^2)/(T);
   psd1_signal = psd1_signal / w1w2bar;

   y2_signal = fft(r2_signal,NFFT);
   y2_signal = y2_signal(1:NFFT/2+1);
   y2_signal = y2_signal(indexes);
   y2_signal = y2_signal / fs;

   psd2_signal = 2*(abs(y2_signal).^2)/(T);
   psd2_signal = psd2_signal / w1w2bar;

   r1r2_signal = y1_signal .* conj(y2_signal);

   psd1_sum = psd1_sum + psd1'; 
   psd2_sum = psd2_sum + psd2';
   r1_sum = r1_sum + y1';
   r2_sum = r2_sum + y2';
   r1r2_sum = r1r2_sum + r1r2';

   psd1_signal_sum = psd1_signal_sum + psd1_signal';
   psd2_signal_sum = psd2_signal_sum + psd2_signal';
   r1_signal_sum = r1_signal_sum + y1_signal';
   r2_signal_sum = r2_signal_sum + y2_signal';
   r1r2_signal_sum = r1r2_signal_sum + r1r2_signal';

   [junk,index] = min(abs(f-fref));
   p_gw_50 = psd1_signal(index);
   if p_gw_50 == 0
      p_gw_50 = 1e-100; 
      pbar = ones(size(psd1_signal));
   else
      pbar = psd1_signal ./ p_gw_50;
   end
   pbar2 = (f/fref).^(2/3 - 3);

   if params.doPlots && ii == 1

      figure;
      loglog(f,pbar,'b')
      hold on
      loglog(f,pbar2,'r')
      hold off
      grid;
      xlim([10 1024]);
      %ylim([1e-50 1e-40]);
      xlabel('Frequency [Hz]');
      ylabel('PSD [strain/rtHz]');
      print('-dpng',[params.outputDir '/pbar.png'])
      print('-depsc2',[params.outputDir '/pbar.eps'])
      print('-dpdf',[params.outputDir '/pbar.pdf'])
      close;
   end
   pbar = pbar2;

   Q = (1/w1w2bar) * pbar ./ (psd1 .* psd2);
   n = (T/2) * deltaF * sum(pbar.^2 ./ (psd1 .* psd2));

   y = (1/n) * deltaF * sum(y1.*conj(y2).* Q);
   sigma2 = (w1w2squaredbar/w1w2bar^2) * 1/(2*n);

   ys(ii) = real(y);
   sigmas2(ii) = sigma2;
   pgws(ii) = p_gw_50;

   %fprintf('%.5e %.5e\n',y,sqrt(sigma));
   %fprintf('%.5e\n',p_gw_50);

   count = count + 1;
end

indexes = find(ys ~= 0);
ys = ys(indexes);
sigmas2 = sigmas2(indexes);
pgws = pgws(indexes);
times = times(indexes);

psd1_sum = psd1_sum / count;
psd2_sum = psd2_sum / count;

psd1_signal_sum = psd1_signal_sum / count;
psd2_signal_sum = psd2_signal_sum / count;

%psd1_sum = 2* psd1_sum / (L*fs*windowconst);
%psd2_sum = 2* psd2_sum / (L*fs*windowconst);
%psd1_signal_sum = 2* psd1_signal_sum / (L*fs*windowconst);
%psd2_signal_sum = 2* psd2_signal_sum / (L*fs*windowconst);

if params.doPlots
   figure;
   loglog(f,psd1_sum,'b')
   hold on
   loglog(f,psd2_sum,'r')
   loglog(spectra_f,spectra_h,'k--')
   hold off
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('PSD [strain/rtHz]');
   print('-dpng',[params.outputDir '/psd.png'])
   print('-depsc2',[params.outputDir '/psd.eps'])
   print('-dpdf',[params.outputDir '/psd.pdf'])
   close;
   
   figure;
   loglog(f,Q,'k--')
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('Q');
   print('-dpng',[params.outputDir '/Q.png'])
   print('-depsc2',[params.outputDir '/Q.eps'])
   print('-dpdf',[params.outputDir '/Q.pdf'])
   close;
   
   figure;
   loglog(f,psd1_signal_sum,'b')
   hold on
   loglog(f,psd2_signal_sum,'r')
   loglog(spectra_f,spectra_h,'k--')
   hold off
   grid;
   xlim([10 1024]);
   %ylim([1e-60 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('PSD [strain/rtHz]');
   print('-dpng',[params.outputDir '/psd_signal.png'])
   print('-depsc2',[params.outputDir '/psd_signal.eps'])
   print('-dpdf',[params.outputDir '/psd_signal.pdf'])
   close;

   figure;
   plot(times,ys,'bo');
   hold on
   plot(times,pgws,'kx');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Y');
   print('-dpng',[params.outputDir '/y.png'])
   print('-depsc2',[params.outputDir '/y.eps'])
   print('-dpdf',[params.outputDir '/y.pdf'])
   close;
   
   figure;
   errorbar(times,ys,sqrt(sigmas2),'bo');
   hold on
   plot(times,pgws,'kx');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Y');
   print('-dpng',[params.outputDir '/y_sigma.png'])
   print('-depsc2',[params.outputDir '/y_sigma.eps'])
   print('-dpdf',[params.outputDir '/y_sigma.pdf'])
   close;

   figure;
   plot(times,sqrt(sigmas2),'kx');
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Sigma');
   print('-dpng',[params.outputDir '/sigma.png'])
   print('-depsc2',[params.outputDir '/sigma.eps'])
   print('-dpdf',[params.outputDir '/sigma.pdf'])
   close;

end

%fprintf('No Noise: %.5e %.5e\n',sum(real(r1r2_signal_sum)),sum(abs(r1r2_signal_sum)));
%fprintf('Noise: %.5e %.5e\n',sum(real(r1r2_sum)),sum(abs(r1r2_sum)));

y = sum(ys./sigmas2) ./ sum(1./sigmas2);
%y = mean(ys);
sigma = sqrt(1/sum(1./sigmas2));
%sigma = mean(sqrt(sigmas));
pgw = mean(pgws);

std(ys)
sqrt(mean(sigmas2))
std(ys)/sqrt(mean(sigmas2))

%fprintf('y: %.5e, sigma: %.5e\n',y,sigma);
%fprintf('p_gw: %.5e, num sigma: %.5f\n',pgw,abs(y-pgw)/sigma);

matFile = [params.outputDir '/data.mat']; 
save(matFile,'ys','sigmas2','pgws');

