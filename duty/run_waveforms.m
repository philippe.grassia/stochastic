function run_waveform(m1,m2);

fs = 256;
fmin = 10;
fmax = 100;
fref = 40;

xi = 0;

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/bns/');
outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/bbh/%d_%d',m1,m2);
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

waveformDir = '/home/mcoughlin/Stochastic/Tania/waveforms';
waveforms = {'SEOBNRv2','IMRPhenomB','IMRPhenomC','IMRPhenomD'};
%waveforms = {'SEOBNRv2','IMRPhenomB','IMRPhenomC'};
waveforms = {'SEOBNRv2_ROM_DoubleSpin','IMRPhenomD'};

data = [];

figname = 'timeseries';
figure;
hold all
for i = 1:length(waveforms)

   if sum(ismember(waveforms{i},{'IMRPhenomD','SEOBNRv2_ROM_DoubleSpin'}))
      filename = [waveformDir '/' waveforms{i} '_' num2str(m1) '_' num2str(m2) '.dat.fft'];
   else
      filename = [waveformDir '/' waveforms{i} '_' num2str(m1) '_' num2str(m2) '.dat'];
   end

   if ~exist(filename)
      return
   end
   
   data_out = load(filename);
   
   if sum(ismember(waveforms{i},{'IMRPhenomD','SEOBNRv2_ROM_DoubleSpin'}))
 
      f = data_out(:,1);
      y1 = data_out(:,2)';
      T = 1;
      w1w2bar = 1;

      psd1 = 2*(abs(y1).^2)/(T*w1w2bar);
 
   else 
      waveform_t = data_out(:,1); waveform_hp = data_out(:,2); waveform_hc = data_out(:,3);
      waveform_t = waveform_t - waveform_t(1);
      waveform_dur = waveform_t(end) - waveform_t(1);
      waveform_fs = 1/(waveform_t(2) - waveform_t(1));
   
      tt = 0:(1/fs):waveform_dur;
      waveform_hp = decimate(waveform_hp,waveform_fs/fs);
      waveform_hc = decimate(waveform_hc,waveform_fs/fs);
      waveform_t = tt;
   
      %waveform_hp = [zeros(size(waveform_hp)); waveform_hp; zeros(size(waveform_hp))];
      %waveform_hc = [zeros(size(waveform_hc)); waveform_hc; zeros(size(waveform_hc))];
      %waveform_dur = length(waveform_hp)/fs;
   
      %tt = 0:(1/fs):(waveform_dur - 1/fs);
      %waveform_t = tt';
   
      T = waveform_t(end) - waveform_t(1);
   
      if mod(length(waveform_t),2) == 1
         window = hann(length(waveform_t) + 1);
         window = ones(size(window));
   
         [w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar] = ...
            windowFactors(window, window);
         window = window(1:end-1);
      else
         window = hann(length(waveform_t));
         window = ones(size(window));
   
         [w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar] = ...
            windowFactors(window, window);
      end
   
      data(i).tt = waveform_t;
      data(i).hp = waveform_hp;
      data(i).hc = waveform_hc;
   
      plot(data(i).tt,data(i).hp);
   
      L = length(waveform_t);
      NFFT = 2^nextpow2(L); % Next power of 2 from length of y
      f = fs/2*linspace(0,1,NFFT/2+1);
      sT = floor(NFFT/fs);
      y1 = fft(waveform_hp.*window,NFFT);
      y1 = y1(1:NFFT/2+1);
      y1 = y1 / fs;

      T = 1;
      psd1 = 2*(abs(y1).^2)/(T*w1w2bar);

   end

   indexes = find(f >= fmin & f <= fmax);

   data(i).f = f(indexes);
   data(i).psd = psd1(indexes);

end
hold off
%xlim([-10 0.1]);
legend(waveforms);
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;

LAL_MTSUN_SI = 4.9254923218988636432342917247829673e-6; %/**< Geometrized solar mass, s. = LAL_MSUN_SI / LAL_MPL_SI * LAL_TPL_SI */

Mtot = m1 + m2;
etha=m1*m2/(Mtot*Mtot);
nu1 = 404*((0.66389*etha*etha-0.10321*etha+0.10979)/0.125481)*(20./Mtot);
nu2 = 807.*((1.3278*etha*etha-0.20642*etha+0.21957)/0.250953)*(20./Mtot);
nu3 = 1153.*((1.7086*etha*etha-0.26592*etha+0.28236)/0.322668)*(20./Mtot);
sigma = 237.*((1.1383*etha*etha-0.177*etha+0.046834)/0.0737278)*(20./Mtot);
w1 = 1./nu1;
f = nu1;
v = (pi*Mtot*f*LAL_MTSUN_SI)^(1/3);
%w1 = w1 * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3).^2; 

w1 = f^(-1) * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3).^2  ./ (1 + (1.4547*xi - 1.8897)*v + (-1.8153*xi + 1.6557)*v*v).^2;
w2 = w1*nu2^(-4/3);
%f = nu2;
%v = (pi*Mtot*f*LAL_MTSUN_SI)^(1/3);
%w2 = w2 * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3).^2;
f = nu2;
v = (pi*Mtot*f*LAL_MTSUN_SI)^(1/3);
w2 = f^(5/3) * w1 * (1 + (1.4547*xi - 1.8897)*v + (-1.8153*xi + 1.6557)*v*v).^2 ./ (f*(f/(1.+((f-nu2)/(sigma/2.))*((f-nu2)/(sigma/2.))))^(2)); 

Kb = 7.8085e-20;

orientationFactor = 4/5;
orientationFactor = 1;

dL = 1;
A = (Mtot^(5/6) / pi^(2/3)) * sqrt(5*etha/24);
C = (1/(2*dL)) * sqrt(2);

ff = 10:1024;
psdgw = zeros(size(ff));
for ii = 1:length(ff)
   f = ff(ii);
   v = (pi*Mtot*f*LAL_MTSUN_SI)^(1/3);
   if (f<nu1)
      g = f^(2./3);
      g = f^(2./3) * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3).^2;
   elseif (f<nu2)
      g = w1*f^(5/3) * (1 + (1.4547*xi - 1.8897)*v + (-1.8153*xi + 1.6557)*v*v).^2;
      %g = w1*f^(5/3) * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3).^2;
      %g = w1*f^(5/3);
   elseif (f<=nu3)
      g = w2*f*(f/(1.+((f-nu2)/(sigma/2.))*((f-nu2)/(sigma/2.))))^(2);
   else
      g = 0;
   end
   Mc   = ((m1*m2)^(3./5))/Mtot^(1/5);
   fLSO = 4397 / Mtot;
   psdgw(ii) = 2*orientationFactor*((Mc^(5/3))/(dL*dL))*1*g/f^3;

   %psdgw(ii) = A*C* f.^(-7/6);

end

%dur = params.gpsEnd - params.gpsStart;
%numinj = floor(dur * params.rate);
%dur = T;
dur = 1;
numinj = 1;
psdgw = numinj * psdgw * Kb * Kb / dur;

fref = 40;
[junk,index] = min(abs(ff - fref));
fprintf('%.5e %.5e\n',ff(index),psdgw(index));
fref = 100;
[junk,index] = min(abs(ff - fref));
fprintf('%.5e %.5e\n',ff(index),psdgw(index));

figname = 'psd';
figure;
loglog(data(1).f,data(1).psd);
hold all
for i = 2:length(waveforms)
   loglog(data(i).f,data(i).psd);
end
loglog(ff,psdgw,'k--');
hold off
grid;
xlim([10 100]);
legend(waveforms);
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;

figname = 'ratio';
figure;
psdgw_interp = interp1(ff,psdgw,data(1).f);
ratio = data(1).psd./psdgw_interp';

norm = 1./data(1).f.^3;
norm = norm' ./ sum(norm);
mismatch = norm .* (data(1).psd - psdgw_interp')./min([psdgw_interp data(1).psd']');
mismatch = sum(abs(mismatch));

p=polyfit(log(data(1).f),log(ratio'),1);
m = p(1);
b = exp(p(2));

[junk,index] = min(abs(data(1).f - fref)); 
seobnrv2 = mismatch;

fprintf('m: %.5f, b: %.5f, fref: %.5f\n',m,b,seobnrv2);

semilogx(data(1).f,ratio,'b');
hold all
semilogx(data(1).f,b*data(1).f.^m,'b--');
for i = 2:length(waveforms)
   psdgw_interp = interp1(ff,psdgw,data(i).f);
   ratio = data(i).psd./psdgw_interp';
   norm = 1./data(i).f.^3;
   norm = norm' ./ sum(norm);
   mismatch = norm .* (data(i).psd - psdgw_interp')./min([psdgw_interp data(i).psd']');
   mismatch = sum(abs(mismatch));  
 
   p=polyfit(log(data(i).f),log(ratio'),1);
   m = p(1);
   b = exp(p(2));

   [junk,index] = min(abs(data(i).f - fref));
   %if i==2
   %   imrphenomb = mismatch(index);
   %   fprintf('m: %.5f, b: %.5f, fref: %.5f\n',m,b,imrphenomb);
   %end
   %if i==3
   %   imrphenomc = mismatch(index);
   %   fprintf('m: %.5f, b: %.5f, fref: %.5f\n',m,b,imrphenomc);
   %end
   if i==2
      imrphenomd = mismatch;
      fprintf('m: %.5f, b: %.5f, fref: %.5f\n',m,b,imrphenomd);
   end


   semilogx(data(i).f,ratio);
   semilogx(data(i).f,b*data(i).f.^m,'--');

end
hold off
xlim([10 100]);
grid;
legend(waveforms);
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;

save([outputDir '/data.mat'],'seobnrv2','imrphenomd','m1','m2','ff','psdgw','data');

keyboard
