function y = powerlaw(freq,a,k);

x = freq;
% calculate y-values
y = (10.^a)*(x.^k);

