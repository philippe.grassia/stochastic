function run_duty(gpsStart,gpsEnd,rate,T,doPSDCut,seed)

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

fs = 256;
fref = 50;
fmin = 10;
fmax = 100;

frames = gpsStart:(T/2):gpsEnd;

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots/%.10f/%d/%d/%d',rate,T,doPSDCut,seed);
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

params.outputDir = outputDir;
params.spectraFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd.txt';
params.waveformFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/duty/waveforms/SEOBNRv2_30_30.dat';
params.T = T;
params.rate = rate;
params.gpsStart = gpsStart;
params.gpsEnd = gpsEnd;
params.fs = fs;
params.fref = fref;
params.fmin = fmin;
params.fmax = fmax;
params.doPlots = 1;
params.doPSDCut = doPSDCut;

spectra = load(params.spectraFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);
mc.transfer(:,1) = spectra_f;
mc.transfer(:,2) = spectra_h;

[t_all,r1_all,r2_all,r1_signal_all,r2_signal_all] = gen_data_bbh(params);

data = gen_psd(params,t_all,r1_all,r2_all,r1_signal_all,r2_signal_all);

highPassOrder = 6;
highPassFreq = 5; %Hz
highPassFreqNorm = highPassFreq/(fs/2);
[b,a] = butter(highPassOrder, highPassFreqNorm, 'high');
N = length(frames)-3;
times = frames(1:N);

count = 0;
for ii = 1:N

   if mod(ii,100) == 1
      fprintf('%d/%d\n',ii,N);
   end

   frameStart = frames(ii);
   frameEnd = frames(ii+2);

   indexMin = ceil((frameStart-gpsStart)*fs) + 1; indexMax = ceil((frameEnd-gpsStart)*fs);

   t = t_all(indexMin:indexMax);
   r1 = r1_all(indexMin:indexMax); r2 = r2_all(indexMin:indexMax);
   r1_signal = r1_signal_all(indexMin:indexMax); r2_signal = r2_signal_all(indexMin:indexMax);

   if count == 0
      ys = zeros(N,1); sigmas2 = zeros(N,1);
      pgws = zeros(N,1); psdcut = zeros(N,1);
   end

   if params.doPSDCut
      r1_signal_tot = sum(r1_signal);
      r2_signal_tot = sum(r2_signal);

      if (r1_signal_tot == 0) && (r2_signal_tot == 0)
         psdcut(ii) = 0;
      else
         psdcut(ii) = 1;
      end
   end

   f = data.f;
   deltaF = f(2) - f(1);
   y1 = data.y1_all(:,ii);
   y2 = data.y2_all(:,ii);
   y1y2 = data.y1y2_all(:,ii);
   psd1 = data.psd1_all(:,ii);
   psd2 = data.psd2_all(:,ii);
   psd1_ave = data.psd1_ave;
   psd2_ave = data.psd2_ave;
   psd1_signal = data.psd1_signal_all(:,ii);
   psd2_signal = data.psd2_signal_all(:,ii);
   w1w2bar = data.w1w2bar;
   w1w2squaredbar = data.w1w2squaredbar;
   w1w2ovlsquaredbar = data.w1w2ovlsquaredbar;

   [junk,index] = min(abs(f-fref));
   p_gw_50 = psd1_signal(index);
   if p_gw_50 == 0
      p_gw_50 = 1e-100; 
      pbar = ones(size(psd1_signal));
   else
      pbar = psd1_signal ./ p_gw_50;
   end
   pbar2 = (f/fref).^(2/3 - 3);

   if params.doPlots && ii == 1

      figure;
      loglog(f,pbar,'b')
      hold on
      loglog(f,pbar2,'r')
      hold off
      grid;
      xlim([10 1024]);
      %ylim([1e-50 1e-40]);
      xlabel('Frequency [Hz]');
      ylabel('PSD [strain/rtHz]');
      print('-dpng',[params.outputDir '/pbar.png'])
      print('-depsc2',[params.outputDir '/pbar.eps'])
      print('-dpdf',[params.outputDir '/pbar.pdf'])
      close;
   end
   pbar = pbar2;

   Q = (1/w1w2bar) * pbar ./ (psd1_ave .* psd2_ave);
   n = (T/2) * deltaF * sum(pbar.^2 ./ (psd1_ave .* psd2_ave));

   y = (1/n) * deltaF * sum(y1y2' .* Q);
   sigma2 = (w1w2squaredbar/w1w2bar^2) * 1/(4*n);

   ys(ii) = real(y);
   sigmas2(ii) = sigma2;
   pgws(ii) = p_gw_50;

   %fprintf('%.5e %.5e\n',y,sqrt(sigma));
   %fprintf('%.5e\n',p_gw_50);

   count = count + 1;
end

%std(ys)
%sqrt(mean(sigmas2))
%std(ys)/sqrt(mean(sigmas2))

pgw = mean(pgws);

if params.doPSDCut
   indexes = find(psdcut);
   ys = ys(indexes);
   sigmas2 = sigmas2(indexes);
   times = times(indexes);
   pgwsplot = pgws(indexes);

   dutyfactor = length(indexes) / N;
   ys = ys * dutyfactor;
   sigmas2 = sigmas2 * dutyfactor^2;
else
   pgwsplot = pgws; 
end

y = sum(ys./sigmas2) ./ sum(1./sigmas2);
sigma = sqrt(1/sum(1./sigmas2));

if params.doPlots
   figure;
   loglog(f,Q,'k--')
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('Q');
   print('-dpng',[params.outputDir '/Q.png'])
   print('-depsc2',[params.outputDir '/Q.eps'])
   print('-dpdf',[params.outputDir '/Q.pdf'])
   close;
   
   figure;
   plot(times,ys,'bo');
   hold on
   plot(times,pgwsplot,'kx');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Y');
   print('-dpng',[params.outputDir '/y.png'])
   print('-depsc2',[params.outputDir '/y.eps'])
   print('-dpdf',[params.outputDir '/y.pdf'])
   close;
   
   figure;
   errorbar(times,ys,sqrt(sigmas2),'bo');
   hold on
   plot(times,pgwsplot,'kx');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Y');
   print('-dpng',[params.outputDir '/y_sigma.png'])
   print('-depsc2',[params.outputDir '/y_sigma.eps'])
   print('-dpdf',[params.outputDir '/y_sigma.pdf'])
   close;

   figure;
   plot(times,sqrt(sigmas2),'kx');
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Sigma');
   print('-dpng',[params.outputDir '/sigma.png'])
   print('-depsc2',[params.outputDir '/sigma.eps'])
   print('-dpdf',[params.outputDir '/sigma.pdf'])
   close;

   binranges = -5:0.1:5;
   vals = (ys - pgwsplot)./sqrt(sigmas2);
   bincounts = histc(vals,binranges);
   bincounts = bincounts / sum(bincounts);
   norm = normpdf(binranges,0,1);
   norm = norm / sum(norm);
   figure;
   plot(binranges,bincounts,'b');
   hold on
   plot(binranges,norm,'k--');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Y/sigma');
   ylabel('Counts');
   print('-dpng',[params.outputDir '/sigmahist.png'])
   print('-depsc2',[params.outputDir '/sigmahist.eps'])
   print('-dpdf',[params.outputDir '/sigmahist.pdf'])
   close;

end

%fprintf('y: %.5e, sigma: %.5e\n',y,sigma);
%fprintf('p_gw: %.5e, num sigma: %.5f\n',pgw,abs(y-pgw)/sigma);

matFile = [params.outputDir '/data.mat']; 
save(matFile,'ys','sigmas2','pgws','times','-v7.3');

