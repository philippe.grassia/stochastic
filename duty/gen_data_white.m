function [t,r1,r2,r1_signal,r2_signal] = gen_data(params);

fs = params.fs;
dur = params.gpsEnd - params.gpsStart;
t =  0:(1/fs):dur;
t = t(1:end-1);

spectra = load(params.spectraFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);
mc.transfer(:,1) = spectra_f;
mc.transfer(:,2) = spectra_h;

spectra = load(params.waveformFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);
mc_signal.transfer(:,1) = spectra_f;
mc_signal.transfer(:,2) = params.amp*spectra_h;

r1 = create_noise(fs,dur,mc);
r2 = create_noise(fs,dur,mc);

r1_signal = create_noise(fs,dur,mc_signal);
r2_signal = r1_signal;

r1 = r1 + r1_signal;
r2 = r2 + r2_signal;

if params.doPlots

   figure;
   plot(t,r1,'b');
   hold on
   plot(t,r2,'k--');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Timeseries [s]');
   ylabel('Strain');
   print('-dpng',[params.outputDir '/timeseries.png'])
   print('-depsc2',[params.outputDir '/timeseries.eps'])
   print('-dpdf',[params.outputDir '/timeseries.pdf'])
   close;
   
   figure;
   plot(t,r1_signal,'b');
   hold on
   plot(t,r2_signal,'k--');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Timeseries [s]');
   ylabel('Strain');
   print('-dpng',[params.outputDir '/timeseries_signal.png'])
   print('-depsc2',[params.outputDir '/timeseries_signal.eps'])
   print('-dpdf',[params.outputDir '/timeseries_signal.pdf'])
   close;
   
end 
     
