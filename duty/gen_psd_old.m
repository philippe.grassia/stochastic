function [psd1_sum,psd2_sum,psd1_signal_sum,psd2_signal_sum] = ...
   gen_psd(params,t_all,r1_all,r2_all,r1_signal_all,r2_signal_all)

T = params.T;
gpsStart = params.gpsStart;
gpsEnd = params.gpsEnd;
fs = params.fs;
fref = params.fref;
fmin = params.fmin;
fmax = params.fmax;

frames = gpsStart:(T/2):gpsEnd;

spectra = load(params.spectraFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);

highPassOrder = 6;
highPassFreq = 5; %Hz
highPassFreqNorm = highPassFreq/(fs/2);
[b,a] = butter(highPassOrder, highPassFreqNorm, 'high');
N = length(frames)-3;
times = frames(1:N);

count = 0;
for ii = 1:N

   if mod(ii,100) == 1
      fprintf('%d/%d\n',ii,N);
   end

   frameStart = frames(ii);
   frameEnd = frames(ii+2);

   indexes = find(t_all >= frameStart & t_all <= frameEnd);
   indexes = indexes(1:end-1);

   t = t_all(indexes);
   r1 = r1_all(indexes); r2 = r2_all(indexes);
   r1_signal = r1_signal_all(indexes); r2_signal = r2_signal_all(indexes);

   r1 = r1 - mean(r1); r2 = r2 - mean(r2);
   r1_signal = r1_signal - mean(r1_signal); r2_signal = r2_signal - mean(r2_signal);

   %if params.doPSDCut
   %   r1_signal_tot = sum(r1_signal);
   %   r2_signal_tot = sum(r2_signal);
   %
   %   if (r1_signal_tot == 0) && (r2_signal_tot == 0)
   %      continue
   %   end
   %end

   window = hann(fs*T)';

   %window = ones(size(window));

   [w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar] = ...
  windowFactors(window, window);

   r1 = r1.*window;
   r2 = r2.*window;
   r1_signal = r1_signal.*window;
   r2_signal = r2_signal.*window;

   if highPassOrder > 6
      r1 = cascadefilter(r1, highPassOrder, ...
         highPassFreq, fs);
      r2 = cascadefilter(r2, highPassOrder, ...
         highPassFreq, fs);
      r1_signal = cascadefilter(r1_signal, highPassOrder, ...
         highPassFreq, fs);
      r2_signal = cascadefilter(r2_signal, highPassOrder, ...
         highPassFreq, fs);
   else
      r1 = filtfilt(b,a,r1);
      r2 = filtfilt(b,a,r2);
      r1_signal = filtfilt(b,a,r1_signal);
      r2_signal = filtfilt(b,a,r2_signal);
   end

   L = length(t);
   NFFT = 2^nextpow2(L); % Next power of 2 from length of y
   f = fs/2*linspace(0,1,NFFT/2+1);
   deltaF = f(2) - f(1);
   indexes = find(f >= fmin & f <= fmax);
   f = f(indexes);

   if count == 0
      r1_sum = zeros(length(f),1);
      r2_sum = zeros(length(f),1);
      psd1_sum = zeros(length(f),1);
      psd2_sum = zeros(length(f),1);
      r1r2_sum = zeros(length(f),1);

      r1_signal_sum = zeros(length(f),1);
      r2_signal_sum = zeros(length(f),1);
      psd1_signal_sum = zeros(length(f),1);
      psd2_signal_sum = zeros(length(f),1);
      r1r2_signal_sum = zeros(length(f),1);

      ys = zeros(N,1); sigmas = zeros(N,1);
      pgws = zeros(N,1);
   end

   sT = floor(NFFT/fs);
   windowconst = 0.375;
   %windowconst = 1;

   y1 = fft(r1,NFFT);
   y1 = y1(1:NFFT/2+1);
   y1 = y1(indexes);
   y1 = y1 / fs;
   %y1 = y1 / sqrt(windowconst);

   psd1 = 2*(abs(y1).^2)/(L*fs*windowconst);
   psd1 = 2*(abs(y1).^2)/(T*w1w2bar);
   %psd1 = abs(y1).^2;
   %psd1 = T*(abs(y1).^2);

   y2 = fft(r2,NFFT);
   y2 = y2(1:NFFT/2+1);
   y2 = y2(indexes);
   y2 = y2 / fs;
   %y2 = y2 / sqrt(windowconst);

   psd2 = 2*(abs(y2).^2)/(L*fs*windowconst);
   psd2 = 2*(abs(y2).^2)/(T*w1w2bar);
   %psd2 = abs(y2).^2;
   %psd2 = T*(abs(y2).^2);

   %psd1 = interp1(spectra_f,spectra_h,f) * (L*fs*windowconst)/2;
   %psd2 = interp1(spectra_f,spectra_h,f) * (L*fs*windowconst)/2;

   %psd2 = abs(y2).^2;
   r1r2 = y1 .* conj(y2);

   y1_signal = fft(r1_signal,NFFT);
   y1_signal = y1_signal(1:NFFT/2+1);
   y1_signal = y1_signal(indexes);
   y1_signal = y1_signal / fs;
   %y1_signal = y1_signal / sqrt(windowconst);

   psd1_signal = 2*(abs(y1_signal).^2)/(L*fs*windowconst);
   psd1_signal = 2*(abs(y1_signal).^2)/(T*w1w2bar);
   %psd1_signal = (abs(y1_signal).^2);
   %psd1_signal = 2*(abs(y1_signal).^2)/T;

   y2_signal = fft(r2_signal,NFFT);
   y2_signal = y2_signal(1:NFFT/2+1);
   y2_signal = y2_signal(indexes);
   y2_signal = y2_signal / fs;
   %y2_signal = y2_signal / sqrt(windowconst);

   psd2_signal = 2*(abs(y2_signal).^2)/(L*fs*windowconst);
   psd2_signal = (abs(y2_signal).^2);
   psd2_signal = 2*(abs(y2_signal).^2)/(T*w1w2bar);

   r1r2_signal = y1_signal .* conj(y2_signal);

   psd1_sum = psd1_sum + psd1'; 
   psd2_sum = psd2_sum + psd2';
   r1_sum = r1_sum + y1';
   r2_sum = r2_sum + y2';
   r1r2_sum = r1r2_sum + r1r2';

   psd1_signal_sum = psd1_signal_sum + psd1_signal';
   psd2_signal_sum = psd2_signal_sum + psd2_signal';
   r1_signal_sum = r1_signal_sum + y1_signal';
   r2_signal_sum = r2_signal_sum + y2_signal';
   r1r2_signal_sum = r1r2_signal_sum + r1r2_signal';

   count = count + 1;
end

psd1_sum = psd1_sum / count;
psd2_sum = psd2_sum / count;

psd1_signal_sum = psd1_signal_sum / count;
psd2_signal_sum = psd2_signal_sum / count;

%psd1_sum = 2* psd1_sum / (L*fs*windowconst);
%psd2_sum = 2* psd2_sum / (L*fs*windowconst);
%psd1_signal_sum = 2* psd1_signal_sum / (L*fs*windowconst);
%psd2_signal_sum = 2* psd2_signal_sum / (L*fs*windowconst);

if params.doPlots
   figure;
   %loglog(f,psd1_sum,'b')
   loglog(f,psd1_sum,'b')
   %loglog(f,2*fs^2* psd1_sum / (L*fs*windowconst),'b')
   hold on
   loglog(f,psd2_sum,'r')
   %loglog(f,2*fs^2* psd2_sum / (L*fs*windowconst),'r')
   loglog(spectra_f,spectra_h,'k--')
   hold off
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('PSD [strain/rtHz]');
   print('-dpng',[params.outputDir '/psd_total.png'])
   print('-depsc2',[params.outputDir '/psd_total.eps'])
   print('-dpdf',[params.outputDir '/psd_total.pdf'])
   close;
   
   figure;
   loglog(f,psd1_signal_sum,'b')
   %loglog(f,2*fs^2* psd1_signal_sum / (L*fs*windowconst),'b')
   hold on
   loglog(f,psd2_signal_sum,'r')
   %loglog(f,2*fs^2* psd2_signal_sum / (L*fs*windowconst),'r')
   loglog(spectra_f,spectra_h,'k--')
   hold off
   grid;
   xlim([10 1024]);
   %ylim([1e-60 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('PSD [strain/rtHz]');
   print('-dpng',[params.outputDir '/psd_signal_total.png'])
   print('-depsc2',[params.outputDir '/psd_signal_total.eps'])
   print('-dpdf',[params.outputDir '/psd_signal_total.pdf'])
   close;
   
end

