
m1s = 5:5:100;
m2s = 5:5:100;

for ii = 1:length(m1s)
   for jj = 1:length(m2s)

      m1 = m1s(ii); m2 = m2s(jj);

      if m1 > m2
         continue
      end

      outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/bbh/%d_%d',m1,m2);

      filename = [outputDir '/data.mat'];
      if exist(filename)
         continue
      end

      run_waveforms(m1,m2)
   end
end

vals_imrphenomb = NaN*ones(length(m1s),length(m2s));
vals_imrphenomc = NaN*ones(length(m1s),length(m2s));
vals_imrphenomd = NaN*ones(length(m1s),length(m2s));
vals_seobnrv2 = NaN*ones(length(m1s),length(m2s));
for ii = 1:length(m1s)
   for jj = 1:length(m2s)
      m1 = m1s(ii); m2 = m2s(jj);

      if m1 > m2
         continue
      end

      outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/bbh/%d_%d',m1,m2);

      filename = [outputDir '/data.mat'];
      if ~exist(filename)
         continue
      end
      data_out = load(filename);

      %vals_imrphenomb(ii,jj) = abs(data_out.imrphenomb);
      %vals_imrphenomc(ii,jj) = abs(data_out.imrphenomc);
      vals_seobnrv2(ii,jj) = abs(data_out.seobnrv2);
      vals_imrphenomd(ii,jj) = abs(data_out.imrphenomd);      

   end
end

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/bbh/all/');
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

[X,Y] = meshgrid(m1s,m2s);

%figname = 'imrphenomb';
%figure;
%pcolor(X,Y,vals_imrphenomb);
%xlabel('Mass [solar masses]');
%ylabel('Mass [solar masses]');
%h = colorbar;
%ylabel(h, 'Mismatch');
%caxis([0 1]);
%print('-dpng',[outputDir '/' figname '.png']);
%print('-depsc2',[outputDir '/' figname '.eps']);
%print('-dpdf',[outputDir '/' figname '.pdf']);
%close;

%figname = 'imrphenomc';
%figure;
%pcolor(X,Y,vals_imrphenomc);
%xlabel('Mass [solar masses]');
%ylabel('Mass [solar masses]');
%h = colorbar;
%ylabel(h, 'Mismatch');
%caxis([0 1]);
%print('-dpng',[outputDir '/' figname '.png']);
%print('-depsc2',[outputDir '/' figname '.eps']);
%print('-dpdf',[outputDir '/' figname '.pdf']);
%close;

figname = 'imrphenomd';
figure;
pcolor(X,Y,vals_imrphenomd);
xlabel('Mass [solar masses]');
ylabel('Mass [solar masses]');
h = colorbar;
ylabel(h, 'Mismatch');
caxis([0 0.25]);
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;

figname = 'seobnrv2';
figure;
pcolor(X,Y,vals_seobnrv2);
h = colorbar;
caxis([0 0.25]);
ylabel(h, 'Mismatch');
xlabel('Mass [solar masses]');
ylabel('Mass [solar masses]');
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;
