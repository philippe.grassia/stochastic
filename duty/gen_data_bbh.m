function [t,r1,r2,r1_signal,r2_signal] = gen_data(params);

fs = params.fs;
dur = params.gpsEnd - params.gpsStart;
t =  0:(1/fs):dur;
t = t(1:end-1);

spectra = load(params.spectraFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);
mc.transfer(:,1) = spectra_f;
mc.transfer(:,2) = spectra_h;

data_out = load(params.waveformFile);
%data_out(:,2:3) = data_out(:,2:3)/1e100;
%data_out(:,2:3) = (data_out(:,2:3)/(10000))*sqrt(1/params.rate);
data_out(:,2:3) = (data_out(:,2:3)/(50000))*sqrt(1/params.rate);

waveform_t = data_out(:,1); waveform_hp = data_out(:,2); waveform_hc = data_out(:,3);
waveform_t = waveform_t - waveform_t(1);
waveform_dur = waveform_t(end) - waveform_t(1);
waveform_fs = 1/(waveform_t(2) - waveform_t(1));

tt = 0:(1/params.fs):waveform_dur;
%waveform_hp = interp1(waveform_t,waveform_hp,tt)';
%waveform_hc = interp1(waveform_t,waveform_hc,tt)';
waveform_hp = decimate(waveform_hp,waveform_fs/params.fs);
waveform_hc = decimate(waveform_hc,waveform_fs/params.fs);
waveform_t = tt;

T = waveform_t(end) - waveform_t(1);

if mod(length(waveform_t),2) == 1
   window = hann(length(waveform_t) + 1);
   [w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar] = ...
      windowFactors(window, window);
   window = window(1:end-1);
else
   window = hann(length(waveform_t));
   [w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar] = ...
  windowFactors(window, window);
end

r1 = create_noise(fs,dur,mc);
r2 = create_noise(fs,dur,mc);

r1_signal = zeros(size(r1));
r2_signal = zeros(size(r2));

numinj = floor(dur * params.rate);
if numinj == 0
   numinj = 1;
end
%injs = linspace(params.gpsStart,params.gpsEnd-waveform_dur-0.1,numinj);
injs = params.gpsStart + rand(numinj,1)*(params.gpsEnd-waveform_dur-0.1-params.gpsStart);

for jj = 1:numinj
   if mod(jj,100) == 1
      fprintf('%d/%d\n',jj,numinj);
   end

   start = injs(jj);
   %dist = 10 + randn;
   dist = 1;

   [junk,indexStart] = min(abs(t - start));
   indexEnd = indexStart + length(waveform_t) - 1;

   r1_signal(indexStart:indexEnd) = r1_signal(indexStart:indexEnd) + (1/dist)*waveform_hp';
   r2_signal(indexStart:indexEnd) = r2_signal(indexStart:indexEnd) + (1/dist)*waveform_hp';
end

r1 = r1 + r1_signal;
r2 = r2 + r2_signal;

if params.doPlots

   L = length(waveform_t);
   NFFT = 2^nextpow2(L); % Next power of 2 from length of y
   f = fs/2*linspace(0,1,NFFT/2+1);
   sT = floor(NFFT/fs);
   y1 = fft(waveform_hp.*window,NFFT);
   y1 = y1(1:NFFT/2+1);
   y1 = y1 / fs;
   psd1 = 2*(abs(y1).^2)/(T*w1w2bar);

   figure;
   plot(waveform_t,waveform_hp,'k--')
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-35]);
   xlabel('Time [s]');
   ylabel('Strain');
   print('-dpng',[params.outputDir '/waveform.png'])
   print('-depsc2',[params.outputDir '/waveform.eps'])
   print('-dpdf',[params.outputDir '/waveform.pdf'])
   close;
   
   figure;
   loglog(f,psd1,'b')
   grid;
   xlim([10 1024]);
   hold on
   loglog(spectra_f,spectra_h,'k--')
   hold off
   %ylim([1e-50 1e-35]);
   xlabel('Frequency [Hz]');
   ylabel('PSD [strain/rtHz]');
   print('-dpng',[params.outputDir '/waveform_psd.png'])
   print('-depsc2',[params.outputDir '/waveform_psd.eps'])
   print('-dpdf',[params.outputDir '/waveform_psd.pdf'])
   close;

   figure;
   plot(t,r1,'b');
   hold on
   plot(t,r2,'k--');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Timeseries [s]');
   ylabel('Strain');
   print('-dpng',[params.outputDir '/timeseries.png'])
   print('-depsc2',[params.outputDir '/timeseries.eps'])
   print('-dpdf',[params.outputDir '/timeseries.pdf'])
   close;
   
   figure;
   plot(t,r1_signal,'b');
   hold on
   plot(t,r2_signal,'k--');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Timeseries [s]');
   ylabel('Strain');
   print('-dpng',[params.outputDir '/timeseries_signal.png'])
   print('-depsc2',[params.outputDir '/timeseries_signal.eps'])
   print('-dpdf',[params.outputDir '/timeseries_signal.pdf'])
   close;
   
end 
     
