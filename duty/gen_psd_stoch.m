function [stoch_psd,stoch_ff] = gen_psd_stoch(params);

dataDir = [params.stochDir];
filenames = dir([dataDir '/output_psd1.job*.trial1.dat']);
data = [];
ff = [];
for ii = 1:length(filenames)
   filename = [dataDir '/' filenames(ii).name];
   data_out = load(filename);
   tt = data_out(:,1);
   freq = data_out(:,3);
   psdvals = data_out(:,4);

   tt_unique = unique(tt);

   for jj = 1:length(tt_unique)
      indexes = find(tt == tt_unique(jj));

      if ii == 1 && jj == 1
         stoch_ff = freq(indexes);
      end
      data = [data psdvals(indexes)]; 
   end
end

stoch_psd = mean(data');

