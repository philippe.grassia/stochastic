function run_duty(gpsStart,gpsEnd,amp,T,doPSDCut)

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

fs = 256;
fref = 50;
fmin = 10;
fmax = 100;

frames = gpsStart:(T/2):gpsEnd;

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_white/%.10f/%d/%d',amp,T,doPSDCut);
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

params.outputDir = outputDir;
params.spectraFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/flat.txt';
params.waveformFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/flat.txt';
params.T = T;
params.amp = amp;
params.gpsStart = gpsStart;
params.gpsEnd = gpsEnd;
params.fs = fs;
params.fref = fref;
params.fmin = fmin;
params.fmax = fmax;
params.doPlots = 1;
params.doPSDCut = doPSDCut;

spectra = load(params.spectraFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);
mc.transfer(:,1) = spectra_f;
mc.transfer(:,2) = spectra_h;

[t_all,r1_all,r2_all,r1_signal_all,r2_signal_all] = gen_data_white(params);

data = gen_psd(params,t_all,r1_all,r2_all,r1_signal_all,r2_signal_all);

highPassOrder = 6;
highPassFreq = 5; %Hz
highPassFreqNorm = highPassFreq/(fs/2);
[b,a] = butter(highPassOrder, highPassFreqNorm, 'high');
N = length(frames)-3;
times = frames(1:N);

count = 0;
for ii = 1:N

   if mod(ii,100) == 1
      fprintf('%d/%d\n',ii,N);
   end

   frameStart = frames(ii);
   frameEnd = frames(ii+2);

   indexMin = ceil((frameStart-gpsStart)*fs) + 1; indexMax = ceil((frameEnd-gpsStart)*fs);

   t = t_all(indexMin:indexMax);
   r1 = r1_all(indexMin:indexMax); r2 = r2_all(indexMin:indexMax);
   r1_signal = r1_signal_all(indexMin:indexMax); r2_signal = r2_signal_all(indexMin:indexMax);

   if params.doPSDCut
      r1_signal_tot = sum(r1_signal);
      r2_signal_tot = sum(r2_signal);

      if (r1_signal_tot == 0) && (r2_signal_tot == 0)
         continue
      end
   end

   if count == 0
      ys = zeros(N,1); sigmas2 = zeros(N,1);
      pgws = zeros(N,1);
   end

   f = data.f;
   deltaF = f(2) - f(1);
   y1 = data.y1_all(:,ii);
   y2 = data.y2_all(:,ii);
   y1y2 = data.y1y2_all(:,ii);
   psd1 = data.psd1_all(:,ii);
   psd2 = data.psd2_all(:,ii);
   psd1_ave = data.psd1_ave;
   psd2_ave = data.psd2_ave;
   psd1_signal = data.psd1_signal_all(:,ii);
   psd2_signal = data.psd2_signal_all(:,ii);
   w1w2bar = data.w1w2bar;
   w1w2squaredbar = data.w1w2squaredbar;
   w1w2ovlsquaredbar = data.w1w2ovlsquaredbar;

   [junk,index] = min(abs(f-fref));
   p_gw_50 = psd1_signal(index);
   if p_gw_50 == 0
      p_gw_50 = 1e-100; 
      pbar = ones(size(psd1_signal));
   else
      pbar = psd1_signal ./ p_gw_50;
   end
   pbar2 = (f/fref).^(0);

   if params.doPlots && ii == 1

      figure;
      loglog(f,pbar,'b')
      hold on
      loglog(f,pbar2,'r')
      hold off
      grid;
      xlim([10 1024]);
      %ylim([1e-50 1e-40]);
      xlabel('Frequency [Hz]');
      ylabel('PSD [strain/rtHz]');
      print('-dpng',[params.outputDir '/pbar.png'])
      print('-depsc2',[params.outputDir '/pbar.eps'])
      print('-dpdf',[params.outputDir '/pbar.pdf'])
      close;
   end
   pbar = pbar2;

   Q = (1/w1w2bar) * pbar ./ (psd1_ave .* psd2_ave);
   n = (T/2) * deltaF * sum(pbar.^2 ./ (psd1_ave .* psd2_ave));

   y = (1/n) * deltaF * sum(y1y2' .* Q);
   sigma2 = (w1w2squaredbar/w1w2bar^2) * 1/(4*n);

   ys(ii) = real(y);
   sigmas2(ii) = sigma2;
   pgws(ii) = p_gw_50;

   %fprintf('%.5e %.5e\n',y,sqrt(sigma));
   %fprintf('%.5e\n',p_gw_50);

   count = count + 1;
end

indexes = find(ys ~= 0);
ys = ys(indexes);
sigmas2 = sigmas2(indexes);
pgws = pgws(indexes);
times = times(indexes);

if params.doPlots
   
   figure;
   loglog(f,Q,'k--')
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('Q');
   print('-dpng',[params.outputDir '/Q.png'])
   print('-depsc2',[params.outputDir '/Q.eps'])
   print('-dpdf',[params.outputDir '/Q.pdf'])
   close;
   
   figure;
   plot(times,ys,'bo');
   hold on
   plot(times,pgws,'kx');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Y');
   print('-dpng',[params.outputDir '/y.png'])
   print('-depsc2',[params.outputDir '/y.eps'])
   print('-dpdf',[params.outputDir '/y.pdf'])
   close;
   
   figure;
   errorbar(times,ys,sqrt(sigmas2),'bo');
   hold on
   plot(times,pgws,'kx');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Y');
   print('-dpng',[params.outputDir '/y_sigma.png'])
   print('-depsc2',[params.outputDir '/y_sigma.eps'])
   print('-dpdf',[params.outputDir '/y_sigma.pdf'])
   close;

   figure;
   plot(times,sqrt(sigmas2),'kx');
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Sigma');
   print('-dpng',[params.outputDir '/sigma.png'])
   print('-depsc2',[params.outputDir '/sigma.eps'])
   print('-dpdf',[params.outputDir '/sigma.pdf'])
   close;

end

%fprintf('No Noise: %.5e %.5e\n',sum(real(r1r2_signal_sum)),sum(abs(r1r2_signal_sum)));
%fprintf('Noise: %.5e %.5e\n',sum(real(r1r2_sum)),sum(abs(r1r2_sum)));

y = sum(ys./sigmas2) ./ sum(1./sigmas2);
%y = mean(ys);
sigma = sqrt(1/sum(1./sigmas2));
%sigma = mean(sqrt(sigmas));
pgw = mean(pgws);

std(ys)
sqrt(mean(sigmas2))
std(ys)/sqrt(mean(sigmas2))

%fprintf('y: %.5e, sigma: %.5e\n',y,sigma);
%fprintf('p_gw: %.5e, num sigma: %.5f\n',pgw,abs(y-pgw)/sigma);

matFile = [params.outputDir '/data.mat']; 
save(matFile,'data','ys','sigmas2','pgws','times');

