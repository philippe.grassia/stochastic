function vector = stamp_create_noise(sampleRate,dataDuration,mc)

% number of data points
N = dataDuration * sampleRate;
% prepare for FFT
if ( mod(N,2)== 0 )
  numFreqs = N/2 - 1;
else
  numFreqs = (N-1)/2;
end
deltaF = 1/dataDuration;
f = deltaF*[1:1:numFreqs]';
flow = deltaF;
% Next power of 2 from length of y
NFFT = 2^nextpow2(N);

amp_values = mc.transfer(:,2);
f_transfer1 = mc.transfer(:,1);
Pf1 = interp1(f_transfer1,amp_values, f, 'spline');

%
deltaT = 1/sampleRate;
norm1 = sqrt(N/(2*deltaT)) * sqrt(Pf1);
%
re1 = norm1*sqrt(1/2).* randn(numFreqs, 1);
im1 = norm1*sqrt(1/2).* randn(numFreqs, 1);
z1  = re1 + 1i*im1;
% freq domain solution for htilde1, htilde2 in terms of z1, z2
htilde1 = z1;
% convolve data with instrument transfer function
otilde1 = htilde1*1;
% set DC and Nyquist = 0, then add negative freq parts in proper order
if ( mod(N,2)==0 )
  % note that most negative frequency is -f_Nyquist when N=even
  otilde1 = [ 0; otilde1; 0; flipud(conj(otilde1)) ];
else
  % no Nyquist frequency when N=odd
  otilde1 = [ 0; otilde1; flipud(conj(otilde1)) ];
end
% fourier transform back to time domain
o1_data = ifft(otilde1);
% take real part (imag part = 0 to round-off)
o1_data = real(o1_data);
vector = o1_data';
vectorError = 0;


