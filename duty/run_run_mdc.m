
seed = 2;
rng(seed);

gpsStart = 800000000;
gpsStart = 800000000;
gpsEnd = gpsStart + 60*2000;
gpsEnd = gpsStart + 5*20480;
%gpsEnd = gpsStart + 60*1000;
%gpsEnd = gpsStart + 60*100;
%gpsEnd = gpsStart + 60*10;

rates = [0.00001 0.0001 0.001 0.01 0.1 1];
rates = [0.00001 0.0001 0.001 0.01 0.1];
%rates = [0.000000001 0.00000001 0.0000001 0.000001 0.00001];
%rates = 0.1;
%rates = 1e-10; 
Ts = [30 60 120 240];
Ts = 60;
doPSDCuts = [0 1];
rates = 0;

%Ts = 60;
%rates = 0.1;

doPSDCuts = [0];
%doPSDCuts = 1;

for ii = 1:length(rates)
   rate = rates(ii);
   for jj = 1:length(Ts)
      T = Ts(jj);
      for kk = 1:length(doPSDCuts)
         doPSDCut = doPSDCuts(kk);
         outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_mdc/%.10f/%d/%d',rate,T,doPSDCut);
         matFile = [outputDir '/data.mat'];
         if exist(matFile)
            continue
         end
         fprintf('%.10f %d %d\n',rate,T,doPSDCut);
         run_mdc(gpsStart,gpsEnd,rate,T,doPSDCut);
      end
   end
end

for jj = 1:length(Ts)
   T = Ts(jj);
   for kk = 1:length(doPSDCuts)
      doPSDCut = doPSDCuts(kk);

      ys = []; sigmas = []; pgws = [];
      ys_mean = [];
      rates_plot = [];

      for ii = 1:length(rates)
         rate = rates(ii);
         %outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots/%d',rate);
         outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_mdc/%.10f/%d/%d',rate,T,doPSDCut);
         %outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_filter_psd_lowrate/%.10f/%d',rate,T);

         matFile = [outputDir '/data.mat'];
         if ~exist(matFile)
            continue
         end

         data_out = load(matFile,'ys','sigmas2','pgws');

         y = mean(data_out.ys);
         sigma = mean(sqrt(data_out.sigmas2));
         pgw = mean(data_out.pgws);
         %sigma = 1;

         y = sum(data_out.ys./data_out.sigmas2) ./ sum(1./data_out.sigmas2);
         sigma = sqrt(1/sum(1./data_out.sigmas2));
         pgw = mean(data_out.pgws);

         y_mean = mean(data_out.ys);

         ys = [ys y]; sigmas = [sigmas sigma]; 
         ys_mean = [ys_mean y_mean];
         pgws = [pgws pgw];
         rates_plot = [rates_plot rate];

         fprintf('%.10f %d %d\n',rate,T,doPSDCut);
         fprintf('y: %.5e, y_mean: %.5e, sigma: %.5e\n',y,y_mean,sigma);
         fprintf('p_gw: %.5e, num sigma: %.5f\n',pgw,abs(y-pgw)/sigma);

         outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_mdc/all/%d/%d',T,doPSDCut);
         %outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_filter_psd_lowrate/all/%d',T);
         if ~exist(outputDir)
            system(['mkdir -p ' outputDir])
         end
         
         errs = abs(ys - pgws)./sigmas;
         errs_mean = abs(ys_mean - pgws)./sigmas;
       
         figure;
         errorbar(rates_plot,ys,sigmas,'bo');
         hold on
         plot(rates_plot,pgws,'kx');
         hold off
         grid;
         set(gca,'xscale','log');
         %set(gca,'yscale','log');
         %xlim([10 1024]);
         %ylim([1e-50 1e-40]);
         xlabel('Rate');
         ylabel('Y');
         print('-dpng',[outputDir '/y_sigma_logx.png'])
         print('-depsc2',[outputDir '/y_sigma_logx.eps'])
         print('-dpdf',[outputDir '/y_sigma_logx.pdf'])
         close;
 
         figure;
         errorbar(rates_plot,ys,sigmas,'bo');
         hold on
         plot(rates_plot,pgws,'kx');
         hold off
         grid;
         set(gca,'xscale','log');
         %set(gca,'yscale','log');
         %xlim([10 1024]);
         %ylim([1e-50 1e-40]);
         xlabel('Rate');
         ylabel('Y');
         print('-dpng',[outputDir '/y_sigma.png'])
         print('-depsc2',[outputDir '/y_sigma.eps'])
         print('-dpdf',[outputDir '/y_sigma.pdf'])
         close;
 
         figure;
         loglog(rates_plot,errs,'kx');
         grid;
         %xlim([1 1e5]);
         %ylim([1e-50 1e-40]);
         xlabel('Rate');
         ylabel('Relative Errors');
         print('-dpng',[outputDir '/rel_errors.png'])
         print('-depsc2',[outputDir '/rel_errors.eps'])
         print('-dpdf',[outputDir '/rel_errors.pdf'])
         close;
        
      end
   end
end 
         
