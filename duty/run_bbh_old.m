function run_bbh

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

fs = 256;
fref = 50;
fmin = 10;
fmax = 100;

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/bbh');
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

params.outputDir = outputDir;
params.spectraFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd.txt';
params.waveformFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/duty/waveforms/SEOBNRv2_30_30.dat';
params.fs = fs;
params.fref = fref;
params.fmin = fmin;
params.fmax = fmax;
params.doPlots = 1;

data_out = load(params.waveformFile);

waveform_t = data_out(:,1); waveform_hp = data_out(:,2); waveform_hc = data_out(:,3);
waveform_t = waveform_t - waveform_t(1);
waveform_dur = waveform_t(end) - waveform_t(1);
waveform_fs = 1/(waveform_t(2) - waveform_t(1));

tt = 0:(1/params.fs):waveform_dur;
%waveform_hp = interp1(waveform_t,waveform_hp,tt)';
%waveform_hc = interp1(waveform_t,waveform_hc,tt)';
waveform_hp = decimate(waveform_hp,waveform_fs/params.fs);
waveform_hc = decimate(waveform_hc,waveform_fs/params.fs);
waveform_t = tt;

T = waveform_t(end) - waveform_t(1);
window = hann(length(waveform_t) + 1);

[w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar] = ...
  windowFactors(window, window);

L = length(waveform_t);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
ff = fs/2*linspace(0,1,NFFT/2+1);
sT = floor(NFFT/fs);
y1 = fft(waveform_hp.*window(1:end-1),NFFT);
y1 = y1(1:NFFT/2+1);
y1 = y1 / fs;
psd1 = 2*(abs(y1).^2)/(T*w1w2bar);

z = 0.1;
m1 = 30.0;
m2 = 30.0;

Mtot = m1 + m2;
etha=m1*m2/(Mtot*Mtot);
nu1 = 404*((0.66389*etha*etha-0.10321*etha+0.10979)/0.125481)*(20./Mtot);
nu2 = 807.*((1.3278*etha*etha-0.20642*etha+0.21957)/0.250953)*(20./Mtot);
nu3 = 1153.*((1.7086*etha*etha-0.26592*etha+0.28236)/0.322668)*(20./Mtot);
sigma = 237.*((1.1383*etha*etha-0.177*etha+0.046834)/0.0737278)*(20./Mtot);
w1 = 1./nu1;
w2 = nu2^(-4/3)/nu1;
Kb = 7.8085e-20; 

dL = 1;
psdgw = zeros(size(ff));
for ii = 1:length(ff)
   f = ff(ii);
   if (f<nu1)
      ii
      g = f^(2./3);
   elseif (f<nu2)
      g = w1*f^(5/3); 
   elseif (f<=nu3)
      g = w2*f*(f/(1.+((f-nu2)/(sigma/2.))*((f-nu2)/(sigma/2.))))^(2);
   else
      g = 0;
   end
   Mc   = ((m1*m2)^(3./5))/Mtot^(1/5);
   fLSO = 4397 / Mtot;
   psdgw(ii) = Mc^(5/3)/(dL*dL)*1*g/f^3;
end
psdgw = psdgw * Kb * Kb / T;

if params.doPlots

   figure;
   plot(waveform_t,waveform_hp,'k--')
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-35]);
   xlabel('Time [s]');
   ylabel('Strain');
   print('-dpng',[params.outputDir '/waveform.png'])
   print('-depsc2',[params.outputDir '/waveform.eps'])
   print('-dpdf',[params.outputDir '/waveform.pdf'])
   close;

   figure;
   loglog(ff,psd1,'b')
   grid;
   xlim([10 1024]);
   hold on
   %loglog(spectra_f,spectra_h,'k--')
   loglog(ff,psdgw,'k--');
   hold off
   %ylim([1e-50 1e-35]);
   xlabel('Frequency [Hz]');
   ylabel('PSD [strain/rtHz]');
   print('-dpng',[params.outputDir '/waveform_psd.png'])
   print('-depsc2',[params.outputDir '/waveform_psd.eps'])
   print('-dpdf',[params.outputDir '/waveform_psd.pdf'])
   close;
end

keyboard
