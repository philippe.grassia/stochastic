function run_duty(gpsStart,gpsEnd,rate,T,doPSDCut)

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

fs = 256;
%fs = 2048;
fref = 50;
fmin = 10;
fmax = 100;

frames = gpsStart:(T/2):gpsEnd;

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_mdc/%.10f/%d/%d',rate,T,doPSDCut);
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

params.outputDir = outputDir;
params.spectraFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd.txt';
params.waveformFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/duty/waveforms/SEOBNRv2_30_30.dat';
params.dataDir = '/home/stochastic/MDC/O1_MDC/RatesMassTest/TestSet_30_hRate_short';
%params.dataDir = '/home/stochastic/MDC/O1_MDC/RatesMassTest/TestSet_30_short';
%params.dataDir = '/home/stochastic/MDC/O1_MDC/RatesMassTest/TestSet_bns_hRate_short';
params.dataDir = '/home/stochastic/MDC/O1_MDC/RatesMassTest/TestSet_30_hRate_phenomB_short';
%params.dataDir = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk';

params.stochDir = '/home/mcoughlin/Stochastic/MDC/O1/ThirtyPlusThirtyHighRateShort/output/H1L1/10-100/0.666667/0.250000/';
%params.stochDir = '/home/mcoughlin/Stochastic/MDC/O1/BNSHighRateShort/output/H1L1/10-100/0.666667/0.250000/';
params.stochDir = '/home/mcoughlin/Stochastic/MDC/O1/ThirtyPlusThirtyHighRateShortIMRPhenomB/output/H1L1/10-100/0.666667/0.250000/';

params.T = T;
params.rate = rate;
params.gpsStart = gpsStart;
params.gpsEnd = gpsEnd;
params.fs = fs;
params.fref = fref;
params.fmin = fmin;
params.fmax = fmax;
params.doPlots = 1;
params.doPSDCut = doPSDCut;
params.ifo1 = 'H1';
params.ifo2 = 'L1';

params.site1 = getsitefromletter(params.ifo1(1));
params.site2 = getsitefromletter(params.ifo2(1));
params.detector1 = getdetector(params.site1);
params.detector2 = getdetector(params.site2);

spectra = load(params.spectraFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);
mc.transfer(:,1) = spectra_f;
mc.transfer(:,2) = spectra_h;

%omega = load([params.dataDir '/omega.txt']);
%omega = load([params.dataDir '/psd_gw.txt']);

omega = load(['/home/mcoughlin/MDC_Generation/MDC_Generation/trunk' '/psd_gw.txt']);

[stoch_psd,stoch_ff] = gen_psd_stoch(params);

[t_all,r1_all,r2_all,r1_signal_all,r2_signal_all] = gen_data_mdc(params);

data = gen_psd(params,t_all,r1_all,r2_all,r1_signal_all,r2_signal_all);

highPassOrder = 16;
highPassFreq = 9; %Hz
highPassFreqNorm = highPassFreq/(fs/2);
[b,a] = butter(highPassOrder, highPassFreqNorm, 'high');
N = length(frames)-3;
times = frames(1:N);

count = 0;
for ii = 1:N

   if mod(ii,100) == 1
      fprintf('%d/%d\n',ii,N);
   end

   frameStart = frames(ii);
   frameEnd = frames(ii+2);

   indexMin = ceil((frameStart-gpsStart)*fs) + 1; indexMax = ceil((frameEnd-gpsStart)*fs);

   t = t_all(indexMin:indexMax);
   r1 = r1_all(indexMin:indexMax); r2 = r2_all(indexMin:indexMax);
   r1_signal = r1_signal_all(indexMin:indexMax); r2_signal = r2_signal_all(indexMin:indexMax);

   if count == 0
      ys = zeros(N,1); sigmas2 = zeros(N,1);
      pgws = zeros(N,1); psdcut = zeros(N,1);
   end

   if params.doPSDCut
      r1_signal_tot = sum(r1_signal);
      r2_signal_tot = sum(r2_signal);

      if (r1_signal_tot == 0) && (r2_signal_tot == 0)
         psdcut(ii) = 0;
      else
         psdcut(ii) = 1;
      end
   end

   f = data.f;
   deltaF = f(2) - f(1);
   y1 = data.y1_all(:,ii);
   y2 = data.y2_all(:,ii);
   y1y2 = data.y1y2_all(:,ii);
   psd1 = data.psd1_all(:,ii);
   psd2 = data.psd2_all(:,ii);
   psd1_ave = data.psd1_ave;
   psd2_ave = data.psd2_ave;
   psd1_signal = data.psd1_signal_all(:,ii);
   psd2_signal = data.psd2_signal_all(:,ii);
   psd1_signal_ave= data.psd1_signal_ave;
   psd2_signal_ave= data.psd2_signal_ave;
   w1w2bar = data.w1w2bar;
   w1w2squaredbar = data.w1w2squaredbar;
   w1w2ovlsquaredbar = data.w1w2ovlsquaredbar;
   orf  = overlapreductionfunction(f, params.detector1, params.detector2);

   h0 = 0.7;
   omega_to_psd = 3.95364e-37*(h0/0.704)*(h0/0.704)*(1./f.^3);
   psd_to_omega = 1 ./ omega_to_psd';

   %psd1_signal = psd1_signal .* psd_to_omega;

   [junk,index] = min(abs(f-fref));
   p_gw_50 = psd1_signal(index);
   if p_gw_50 == 0
      p_gw_50 = 1e-100; 
      pbar = ones(size(psd1_signal));
   else
      pbar = psd1_signal' ./ p_gw_50;
   end
   pbar2 = (f/fref).^(2/3 - 3);

   if params.doPlots && ii == 1

      figure;
      loglog(f,pbar,'b')
      hold on
      loglog(f,pbar2,'r')
      hold off
      grid;
      xlim([10 1024]);
      %ylim([1e-50 1e-40]);
      xlabel('Frequency [Hz]');
      ylabel('PSD [strain/rtHz]');
      print('-dpng',[params.outputDir '/pbar.png'])
      print('-depsc2',[params.outputDir '/pbar.eps'])
      print('-dpdf',[params.outputDir '/pbar.pdf'])
      close;
   end
   %pbar = pbar2;

   Q = (1/w1w2bar) .* orf .* pbar ./ (psd1_ave .* psd2_ave);
   n = (T/2) * deltaF * sum( orf.^2 .* pbar.^2 ./ (psd1_ave .* psd2_ave));

   y = (1/n) * deltaF * sum(y1y2' .* Q);
   sigma2 = (w1w2squaredbar/w1w2bar^2) * 1/(2*n);

   ys(ii) = real(y);
   sigmas2(ii) = sigma2;
   pgws(ii) = p_gw_50;

   %fprintf('%.5e %.5e\n',y,sqrt(sigma));
   %fprintf('%.5e\n',p_gw_50);

   count = count + 1;
end

%std(ys)
%sqrt(mean(sigmas2))
%std(ys)/sqrt(mean(sigmas2))

pgw = mean(pgws);

if params.doPSDCut
   indexes = find(psdcut);
   ys = ys(indexes);
   sigmas2 = sigmas2(indexes);
   times = times(indexes);
   pgwsplot = pgws(indexes);

   dutyfactor = length(indexes) / N;
   ys = ys * dutyfactor;
   sigmas2 = sigmas2 * dutyfactor^2;
else
   pgwsplot = pgws; 
end

y = sum(ys./sigmas2) ./ sum(1./sigmas2);
sigma = sqrt(1/sum(1./sigmas2));

if params.doPlots
   figure;
   loglog(f,Q,'k--')
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('Q');
   print('-dpng',[params.outputDir '/Q.png'])
   print('-depsc2',[params.outputDir '/Q.eps'])
   print('-dpdf',[params.outputDir '/Q.pdf'])
   close;
   
   figure;
   plot(times,ys,'bo');
   hold on
   plot(times,pgwsplot,'kx');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Y');
   print('-dpng',[params.outputDir '/y.png'])
   print('-depsc2',[params.outputDir '/y.eps'])
   print('-dpdf',[params.outputDir '/y.pdf'])
   close;
   
   figure;
   errorbar(times,ys,sqrt(sigmas2),'bo');
   hold on
   plot(times,pgwsplot,'kx');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Y');
   print('-dpng',[params.outputDir '/y_sigma.png'])
   print('-depsc2',[params.outputDir '/y_sigma.eps'])
   print('-dpdf',[params.outputDir '/y_sigma.pdf'])
   close;

   figure;
   plot(times,sqrt(sigmas2),'kx');
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Time [s]');
   ylabel('Sigma');
   print('-dpng',[params.outputDir '/sigma.png'])
   print('-depsc2',[params.outputDir '/sigma.eps'])
   print('-dpdf',[params.outputDir '/sigma.pdf'])
   close;

   figure;
   %loglog(f,psd1_sum,'b')
   loglog(f,psd1_signal_ave,'b')
   %loglog(f,2*fs^2* psd1_sum / (L*fs*windowconst),'b')
   hold on
   loglog(f,psd2_signal_ave,'r')
   %loglog(f,2*fs^2* psd2_sum / (L*fs*windowconst),'r')
   %loglog(spectra_f,spectra_h,'k--')
   loglog(omega(:,1),omega(:,3),'k--')
   loglog(stoch_ff,stoch_psd,'c');
   hold off
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('PSD [strain/rtHz]');
   print('-dpng',[params.outputDir '/signal.png'])
   print('-depsc2',[params.outputDir '/signal.eps'])
   print('-dpdf',[params.outputDir '/signal.pdf'])
   close;

   omega_interp = interp1(omega(:,1),omega(:,3),f);
   omega_ratio = 2*omega_interp./psd1_signal_ave;
   stoch_interp = interp1(stoch_ff,stoch_psd,f);
   stoch_ratio = stoch_interp./psd1_signal_ave;

   figure;
   %loglog(f,psd1_sum,'b')
   semilogx(f,omega_ratio,'b')
   hold on
   loglog(f,stoch_ratio,'c')
   hold off
   grid;
   xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Frequency [Hz]');
   ylabel('PSD [strain/rtHz]');
   print('-dpng',[params.outputDir '/ratio.png'])
   print('-depsc2',[params.outputDir '/ratio.eps'])
   print('-dpdf',[params.outputDir '/ratio.pdf'])
   close;

end

%fprintf('y: %.5e, sigma: %.5e\n',y,sigma);
%fprintf('p_gw: %.5e, num sigma: %.5f\n',pgw,abs(y-pgw)/sigma);

matFile = [params.outputDir '/data.mat']; 
save(matFile,'data','ys','sigmas2','pgws','times');

