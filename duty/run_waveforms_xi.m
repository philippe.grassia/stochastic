function run_waveform(m1,m2);

fs = 256;
fmin = 10;
fmax = 100;
fref = 40;

xis = [0,0.5,0.95,-0.5,-0.95];

outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/bns/');
outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_waveforms/xi/%d_%d',m1,m2);
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

ff = 10:256;
legend_names = {};

LAL_MTSUN_SI = 4.9254923218988636432342917247829673e-6; %/**< Geometrized solar mass, s. = LAL_MSUN_SI / LAL_MPL_SI * LAL_TPL_SI */

for jj = 1:length(xis)
   xi = xis(jj);

   Mtot = m1 + m2;
   etha=m1*m2/(Mtot*Mtot);
   nu1 = 404*((0.66389*etha*etha-0.10321*etha+0.10979)/0.125481)*(20./Mtot);
   nu2 = 807.*((1.3278*etha*etha-0.20642*etha+0.21957)/0.250953)*(20./Mtot);
   nu3 = 1153.*((1.7086*etha*etha-0.26592*etha+0.28236)/0.322668)*(20./Mtot);
   sigma = 237.*((1.1383*etha*etha-0.177*etha+0.046834)/0.0737278)*(20./Mtot);
   w1 = 1./nu1;
   f = nu1;
   v = (pi*Mtot*f*LAL_MTSUN_SI)^(1/3);
   w1 = w1 * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3).^2; 
   w2 = w1*nu2^(-4/3);
   %f = nu2;
   %v = (pi*Mtot*f*LAL_MTSUN_SI)^(1/3);
   %w2 = w2 * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3).^2;
   
   Kb = 7.8085e-20;
   
   orientationFactor = 4/5;
   orientationFactor = 1;
   
   dL = 1;
   A = (Mtot^(5/6) / pi^(2/3)) * sqrt(5*etha/24);
   C = (1/(2*dL)) * sqrt(2);
   
   ff = 10:1024;
   psdgw = zeros(size(ff));
   for ii = 1:length(ff)
      f = ff(ii);
      v = (pi*Mtot*f*LAL_MTSUN_SI)^(1/3);
      if (f<nu1)
         g = f^(2./3);
         g = f^(2./3) * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3).^2;
      elseif (f<nu2)
         g = w1*f^(5/3) * (1 + (1.4547*xi - 1.8897)*v + (-1.8153*xi + 1.6557)*v*v).^2;
         %g = w1*f^(5/3) * (1 + (-323/224 + 451*etha/168)*v^2 + xi*(27/8 - 11*etha/6)*v^3).^2;
         g = w1*f^(5/3);
      elseif (f<=nu3)
         g = w2*f*(f/(1.+((f-nu2)/(sigma/2.))*((f-nu2)/(sigma/2.))))^(2);
      else
         g = 0;
      end
      Mc   = ((m1*m2)^(3./5))/Mtot^(1/5);
      fLSO = 4397 / Mtot;
      psdgw(ii) = 2*orientationFactor*((Mc^(5/3))/(dL*dL))*1*g/f^3;
   
      %psdgw(ii) = A*C* f.^(-7/6);
   
   end

   %dur = params.gpsEnd - params.gpsStart;
   %numinj = floor(dur * params.rate);
   %dur = T;
   dur = 1;
   numinj = 1;
   psdgw = numinj * psdgw * Kb * Kb / dur;

   fref = 40;
   [junk,index] = min(abs(ff - fref));
   fprintf('%.5e %.5e\n',ff(index),psdgw(index));
   fref = 100;
   [junk,index] = min(abs(ff - fref));
   fprintf('%.5e %.5e\n',ff(index),psdgw(index));

   data(jj).ff = ff;
   data(jj).psdgw = psdgw;
   legend_names{jj} = sprintf('%.2f',xi);
end

figname = 'psd';
figure;
loglog(data(1).ff,data(1).psdgw);
hold all
for i = 2:length(data)
   loglog(data(i).ff,data(i).psdgw);
end
hold off
grid;
xlim([10 100]);
legend(legend_names);
xlabel('Frequency [Hz]');
ylabel('PSD');
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;

figname = 'ratio';
figure;
ratio = data(1).psdgw./data(1).psdgw;
semilogx(data(1).ff,ratio,'b');
hold all
for i = 2:length(data)
   ratio = data(i).psdgw./data(1).psdgw;
   semilogx(data(1).ff,ratio);
end
hold off
xlim([10 100]);
grid;
legend(legend_names);
xlabel('Frequency [Hz]');
ylabel('Ratio');
print('-dpng',[outputDir '/' figname '.png']);
print('-depsc2',[outputDir '/' figname '.eps']);
print('-dpdf',[outputDir '/' figname '.pdf']);
close;

