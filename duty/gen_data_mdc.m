function [t,r1,r2,r1_signal,r2_signal] = gen_data(params);

fs = params.fs;
dur = params.gpsEnd - params.gpsStart;
t =  0:(1/fs):dur;
t = t(1:end-1);

ifo = 'H1';
channel = [];
channel(1).station = 'H1:STRAIN';
channel(1).samplef = 2048;
channel(1).calibration = 1;

dataDir = [params.dataDir '/' ifo];
framenames = dir([dataDir '/*.gwf']);
frames = {};
for ii = 1:length(framenames)
   frames{ii} = [dataDir '/' framenames(ii).name];
end
[data1,channel] = read_data(params,channel,params.gpsStart,params.gpsEnd,frames);
r1 = decimate(data1.samples,channel(1).samplef/params.fs);

ifo = 'L1';
channel = [];
channel(1).station = 'L1:STRAIN';
channel(1).samplef = 2048;
channel(1).calibration = 1;

dataDir = [params.dataDir '/' ifo];
framenames = dir([dataDir '/*.gwf']);
frames = {};
for ii = 1:length(framenames)
   frames{ii} = [dataDir '/' framenames(ii).name];
end
[data2,channel] = read_data(params,channel,params.gpsStart,params.gpsEnd,frames);
r2 = decimate(data2.samples,channel(1).samplef/params.fs);

r1 = r1';
r2 = r2';

r1_signal = r1;
r2_signal = r2;

if params.doPlots

   figure;
   plot(t,r1,'b');
   hold on
   plot(t,r2,'k--');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Timeseries [s]');
   ylabel('Strain');
   print('-dpng',[params.outputDir '/timeseries.png'])
   print('-depsc2',[params.outputDir '/timeseries.eps'])
   print('-dpdf',[params.outputDir '/timeseries.pdf'])
   close;
   
   figure;
   plot(t,r1_signal,'b');
   hold on
   plot(t,r2_signal,'k--');
   hold off
   grid;
   %xlim([10 1024]);
   %ylim([1e-50 1e-40]);
   xlabel('Timeseries [s]');
   ylabel('Strain');
   print('-dpng',[params.outputDir '/timeseries_signal.png'])
   print('-depsc2',[params.outputDir '/timeseries_signal.eps'])
   print('-dpdf',[params.outputDir '/timeseries_signal.pdf'])
   close;
   
end 
     
