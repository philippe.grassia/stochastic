
seed = 2;
rng(seed);

gpsStart = 800000000;
gpsStart = 0;
gpsEnd = gpsStart + 60*2000;
gpsEnd = gpsStart + 60*1000;
%gpsEnd = gpsStart + 60*100;
%gpsEnd = gpsStart + 60*10;

rates = [0.00001 0.0001 0.001 0.01 0.1 1];
rates = [0.00001 0.0001 0.001 0.01 0.1];
%rates = [0.000000001 0.00000001 0.0000001 0.000001 0.00001];
rates = 1;
%rates = 1e-10; 
%Ts = [60 120];
doPSDCuts = [0 1];

Ts = 60;
%rates = 0.1;

doPSDCuts = [0 1];
doPSDCuts = 0;

for ii = 1:length(rates)
   rate = rates(ii);
   for jj = 1:length(Ts)
      T = Ts(jj);
      for kk = 1:length(doPSDCuts)
         doPSDCut = doPSDCuts(kk);
         outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_bbh/%.10f/%d/%d',rate,T,doPSDCut);
         matFile = [outputDir '/data.mat'];
         if exist(matFile)
            continue
         end
         fprintf('%.10f %d %d\n',rate,T,doPSDCut);
         run_bbh(gpsStart,gpsEnd,rate,T,doPSDCut);
      end
   end
end

