
seed = 10;
rng(seed);

warning('off','all');

gpsStart = 800000000;
gpsStart = 0;
gpsEnd = gpsStart + 60*1000;
%gpsEnd = gpsStart + 60*100;
%gpsEnd = gpsStart + 60*10;

amps = [0.001 0.01 0.1 1 10];
amps = [0.00001 0.0001 0.001 0.01 0.1 1];
%amps = 0.0000000001;
%amps = 0.01;
amps = 0.1;
Ts = [60 120];
Ts = 30;
Ts = 60;
%Ts = 120;
%Ts = 240;
%Ts = 480;

%Ts = [60 120 240];
%amps = 0.1;

doPSDCuts = 0;

for ii = 1:length(amps)
   amp = amps(ii);
   for jj = 1:length(Ts)
      T = Ts(jj);

      for kk = 1:length(doPSDCuts)
         doPSDCut = doPSDCuts(kk);
         outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_white/%.10f/%d/%d',amp,T,doPSDCut);
         matFile = [outputDir '/data.mat'];
         if exist(matFile)
         %   continue
         end
         fprintf('%.10f %d %d\n',amp,T,doPSDCut);

         run_white(gpsStart,gpsEnd,amp,T,doPSDCut);
      end
   end
end

for jj = 1:length(Ts)
   T = Ts(jj);
   for kk = 1:length(doPSDCuts)
      doPSDCut = doPSDCuts(kk);

      ys = []; sigmas = []; pgws = [];
      ys_mean = [];
      amps_plot = [];

      for ii = 1:length(amps)
         amp = amps(ii);
         %outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots/%d',amp);
         outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_white/%.10f/%d/%d',amp,T,doPSDCut);
         %outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_filter_psd_lowamp/%.10f/%d',amp,T);

         matFile = [outputDir '/data.mat'];
         if ~exist(matFile)
            continue
         end

         data_out = load(matFile,'ys','sigmas2','pgws');

         y = mean(data_out.ys);
         sigma = mean(sqrt(data_out.sigmas2));
         pgw = mean(data_out.pgws);
         %sigma = 1;

         y = sum(data_out.ys./data_out.sigmas2) ./ sum(1./data_out.sigmas2);
         sigma = sqrt(1/sum(1./data_out.sigmas2));
         pgw = mean(data_out.pgws);

         y_mean = mean(data_out.ys);

         ys = [ys y]; sigmas = [sigmas sigma]; 
         ys_mean = [ys_mean y_mean];
         pgws = [pgws pgw];
         amps_plot = [amps_plot amp];

         fprintf('%.10f %d %d\n',amp,T,doPSDCut);
         fprintf('y: %.5e, y_mean: %.5e, sigma: %.5e\n',y,y_mean,sigma);
         fprintf('p_gw: %.5e, num sigma: %.5f\n',pgw,abs(y-pgw)/sigma);

         outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_white/all/%d/%d',T,doPSDCut);
         %outputDir = sprintf('/home/mcoughlin/Stochastic/duty/plots_filter_psd_lowamp/all/%d',T);
         if ~exist(outputDir)
            system(['mkdir -p ' outputDir])
         end
         
         errs = abs(ys - pgws)./sigmas;
         errs_mean = abs(ys_mean - pgws)./sigmas;

         std(data_out.ys) ./ sqrt(mean(data_out.sigmas2))

         figure;
         errorbar(amps_plot,ys,sigmas,'bo');
         hold on
         plot(amps_plot,pgws,'kx');
         hold off
         grid;
         set(gca,'xscale','log');
         %set(gca,'yscale','log');
         %xlim([10 1024]);
         %ylim([1e-50 1e-40]);
         xlabel('Amplitude');
         ylabel('Y');
         print('-dpng',[outputDir '/y_sigma_logx.png'])
         print('-depsc2',[outputDir '/y_sigma_logx.eps'])
         print('-dpdf',[outputDir '/y_sigma_logx.pdf'])
         close;

         figure;
         errorbar(amps_plot,ys,sigmas,'bo');
         hold on
         plot(amps_plot,pgws,'kx');
         hold off
         grid;
         set(gca,'xscale','log');
         set(gca,'yscale','log');
         %xlim([10 1024]);
         %ylim([1e-50 1e-40]);
         xlabel('Amplitude');
         ylabel('Y');
         print('-dpng',[outputDir '/y_sigma.png'])
         print('-depsc2',[outputDir '/y_sigma.eps'])
         print('-dpdf',[outputDir '/y_sigma.pdf'])
         close;
 
         figure;
         loglog(amps_plot,errs,'kx');
         grid;
         %xlim([1 1e5]);
         %ylim([1e-50 1e-40]);
         xlabel('Amplitude');
         ylabel('Relative Errors');
         print('-dpng',[outputDir '/rel_errors.png'])
         print('-depsc2',[outputDir '/rel_errors.eps'])
         print('-dpdf',[outputDir '/rel_errors.pdf'])
         close;
        
      end
   end
end 
         
