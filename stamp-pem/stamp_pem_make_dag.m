function stamp_pem_make_dag(pemParamsFile)
% function stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS)
% Given a STAMP-PEM parameter file, channel name, and start and end GPS times,
% creates condor .dag and .sub files for running the STAMP-PEM pipeline on
% multiple channels
% Routine written by Michael Coughlin.
% Modified: July 31, 2013
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Read in STAMP-PEM parameter file
params = readParamsFromFile(pemParamsFile);
params.darmChannelUnderscore = strrep(params.darmChannel,':','_');

darmChannelSplit = regexp(params.darmChannel,':','split');
params.darmChannelIfo = darmChannelSplit{1}; params.darmChannelName = darmChannelSplit{2};

% Read in channel list to determine channel sampling rates
[channelNames,channelSampleRates] = textread(params.channelList,'%s %f');
params.darmChannelSampleRate = channelSampleRates(find(strcmp(channelNames,params.darmChannel)));
params.pemChannelSampleRate = channelSampleRates(find(strcmp(channelNames,params.pemChannel)));

 
% Output path for run
params.path = ...
    [params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(params.startGPS) ...
     '-' num2str(params.endGPS) '/' params.darmChannelUnderscore '/' num2str(params.segmentDuration) '/' ...
    num2str(params.fmin) '-' num2str(params.fmax)];

params.channelNames = channelNames;
params.channelSampleRates = channelSampleRates;

stamp_pem_cache(params);
stamp_pem_segdb(params);
stamp_pem_createpath([params.path '/condor/logs']);
stamp_pem_createpath([params.path '/param_files']);

param_files = [];
for i=1:length(channelNames)
  % initialize
  param = [];
  param_value = [];
  % change colon to underscore in channel name for file naming
  channelNameUnderscore = strrep(channelNames{i},':','_');
  % read in param values from original params files
  [param param_value] = textread(pemParamsFile, '%s %s');
  % rename pem channel
  param_value{strcmp(param,'pemChannel')}=channelNames{i};
  % open new param file for this specific pem channel
  file = [params.path '/param_files/' channelNameUnderscore '_param_file.txt'];
  fid = fopen(file,'w+');
  for j=1:length(param)
    fprintf(fid,'%s %s\n',param{j},param_value{j});
  end %for j=...
  fclose(fid);
 if length(param_files)==0
   param_files{1} = file;
 else
  param_files = [param_files;file];
 end% if length...
end% for i=...
    

% Jobfile read by preproc/clustermap
params.jobsFile = [params.path '/jobsFile.txt'];

% Create job file
jobsFile = fopen(params.jobsFile,'w+');
fprintf(jobsFile,'1 %d %d %d\n',params.startGPS, ...
   params.endGPS,params.endGPS-params.startGPS);
fclose(jobsFile);

job_number=0;

% Open dag file for writing
fid=fopen([params.path '/condor/stamp_pem.dag'],'w+');

% Loop over remaining channels placing each in its own job
for i=1:length(channelNames)
    job_number=job_number+1;
    fprintf(fid,'JOB %d %s/condor/stamp_pem.sub\n',job_number,params.path)
    fprintf(fid,'RETRY %d 3\n',job_number)
    fprintf(fid,'VARS %d jobNumber="%d" pemParamsFile="%s" matappsPath="%s" pemChannel="%s" segmentDuration="%d" startGPS="%d" endGPS="%d" fmin="%d" fmax="%d"\n',job_number,job_number,param_files{i},params.matappsPath,channelNames{i},params.segmentDuration,params.startGPS,params.endGPS,params.fmin,params.fmax); 
    fprintf(fid,'\n\n');
end

% Final job runs summary page
job_number=job_number+1;
fprintf(fid,'JOB %d %s/condor/stamp_pem.sub\n',job_number,params.path)
fprintf(fid,'RETRY %d 3\n',job_number)
fprintf(fid,'VARS %d jobNumber="%d" pemParamsFile="%s" matappsPath="%s" pemChannel="%s" segmentDuration="%d" startGPS="%d" endGPS="%d" fmin="%d" fmax="%d"\n',job_number,job_number,pemParamsFile,params.matappsPath,'summary',params.segmentDuration,params.startGPS,params.endGPS,params.fmin,params.fmax);
fprintf(fid,'\n\n');

% Uncommenting out the code below runs the harvesting code and pem ranking page for the data produced.  (runs in vanilla universe for softlinking purposes)
%job_number=job_number+1;
%fprintf(fid,'JOB %d %s/condor/stamp_pem_vanilla.sub\n',job_number,params.path)
%fprintf(fid,'RETRY %d 3\n',job_number)
%fprintf(fid,'VARS %d jobNumber="%d" pemParamsFile="%s" pemChannel="%s" segmentDuration="%d" startGPS="%d" endGPS="%d"\n',job_number,job_number,pemParamsFile,'findCompareMats',params.segmentDuration,params.startGPS,params.endGPS);
%fprintf(fid,'\n\n');

job_number_last=job_number;

% Zeroth job ensures no condor issues
fprintf(fid,'JOB 0 %s/condor/stamp_pem.sub\n',params.path);
fprintf(fid,'VARS 0 jobNumber="0" pemParamsFile="0" matappsPath="0" pemChannel="0" segmentDuration="0" startGPS="0" endGPS="0" fmin="0" fmax="0"\n');
fprintf(fid,'\n\n');

% Only run jobs when zeroth job completes
fprintf(fid,'PARENT 0 CHILD');
for i=1:job_number_last
   fprintf(fid,' %d',i);
end
fprintf(fid,'\n\n');

% Only run final job when all others complete before
fprintf(fid,'PARENT');
for i=0:job_number_last-1
   fprintf(fid,' %d',i);
end
fprintf(fid, ' CHILD %d', job_number_last);

fprintf(fid,'\n\n');

fclose(fid);

LD_LIBRARY_PATH = '/ldcg/matlab_r2013a/runtime/glnxa64:/ldcg/matlab_r2013a/sys/os/glnxa64:/ldcg/matlab_r2013a/bin/glnxa64:/ldcg/matlab_r2013a/sys/java/jre/glnxa64/jre/lib/amd64/native_threads:/ldcg/matlab_r2013a/sys/java/jre/glnxa64/jre/lib/amd64/server:/ldcg/matlab_r2013a/sys/java/jre/glnxa64/jre/lib/amd64:/ldcg/matlab_r2013a/sys/opengl/lib/glnxa64:/ligotools/lib';

% Open standard universe sub file for writing
fid=fopen([params.path '/condor/stamp_pem.sub'],'w+');
fprintf(fid,'executable = %s/stamp2/condor-matlab-init\n',params.matappsPath);
fprintf(fid,'output = %s/condor/logs/out.$(jobNumber)\n',params.path);
fprintf(fid,'error = %s/condor/logs/err.$(jobNumber)\n',params.path);
fprintf(fid,'arguments = %s/stamp_pem_run $(pemParamsFile)\n',params.executableDir);
fprintf(fid,'request_memory = 1500\n');
fprintf(fid,'notification = never\n');
fprintf(fid,'getenv = False\n');
fprintf(fid,'environment = LD_LIBRARY_PATH=%s\n',LD_LIBRARY_PATH);
fprintf(fid,'log = /usr1/%s/%s-%s-%s-%s-standard.log\n',params.user,params.runName,num2str(params.startGPS),num2str(params.endGPS),num2str(params.segmentDuration));
fprintf(fid,'+MaxHours = 24\n');
%fprintf(fid,'universe = standard\n');
fprintf(fid,'queue 1\n');
fclose(fid);

% Submit dag file to condor
condor_submit_dag_command = sprintf('condor_submit_dag -maxjobs 20 %s/condor/stamp_pem.dag',params.path);
fprintf('Run: %s\n',condor_submit_dag_command);
junk = system(condor_submit_dag_command);
