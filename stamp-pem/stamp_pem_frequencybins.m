function map = stamp_pem_frequencybins(params, map)
% function saveplots(params, map)
% E. Thrane: saves .png and .eps files for key diagnostic plots

try
   params.coherenceThreshold;
catch
   params.coherenceThreshold = 0.5;
end

plotdir = [params.plotdir '/frequencybins'];
stamp_pem_createpath(plotdir);

xlabel_text = sprintf('t(s) [%.1f-%.1f]',map.segstarttime(1),map.segstarttime(end));
ylabel_text = 'PSD (arbitrary units)';

for i = 1:length(map.f)

   coherence = map.coherence(i);
   if coherence < params.coherenceThreshold
      continue
   end

   f = map.f(i);
   crosspower = map.snr(i,:); 
   %psd1 = log10(map.naiP1(i,:)); psd2 = log10(map.naiP2(i,:));
   psd1 = map.naiP1(i,:); psd2 = map.naiP2(i,:);

   crosspower = normalize(crosspower);
   psd1 = normalize(psd1);
   psd2 = normalize(psd2);

   plot_name = strrep(num2str(f),'.','_');

   figure;
   plot(map.xvals,crosspower + 2.5,'r')
   hold on
   plot(map.xvals,psd1 + 1.5,'b')
   plot(map.xvals,psd2 + 0.5,'k')
   hold off
   xlabel(xlabel_text)
   %ylabel(ylabel_text)
   legend({'Cross-power',strrep(params.darmChannel,'_','\_'),strrep(params.pemChannel,'_','\_')})
   ylim([0 5]);
   set(gca, 'YTick',[])
   pretty;
   print('-dpng',[plotdir '/' plot_name '_norm' ]);
   print('-depsc2',[plotdir '/' plot_name '_norm' ]);
   close;

   figure;
   hold on
   plot(map.xvals,psd1,'b')
   plot(map.xvals,psd2,'k')
   hold off
   xlabel(xlabel_text)
   ylabel(ylabel_text)
   legend({strrep(params.darmChannel,'_','\_'),strrep(params.pemChannel,'_','\_')})
   ylim([0 1.5]);
   set(gca, 'YTick',[])
   pretty;
   print('-dpng',[plotdir '/' plot_name '_overlay']);
   print('-depsc2',[plotdir '/' plot_name '_overlay']);
   close;
      
end

return

function y = normalize(x);
   minX = min(x); maxX = max(x);
   y = (1/(maxX - minX)) * (x - minX);
return

