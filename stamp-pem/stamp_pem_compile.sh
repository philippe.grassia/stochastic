#!/bin/bash

cp /home/mcoughlin/matlab_scripts/matlab_script_2012a.sh ${HOME}/
source ${HOME}/matlab_scripts/matlab_script_2012a.sh

mkdir -p ${HOME}/STAMP/STAMP-PEM/bin/
cd ${HOME}/STAMP/STAMP-PEM/bin/
rm -rf *
mkdir ${HOME}/matlab
cp ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/stamp_startup.m .
/ldcg/matlab_r2012a/bin/matlab <<EOF
 stamp_startup;
 mcc -C -R -nojvm -R -nodisplay -m stamp_pem_run
EOF
# mcc -C -R -nojvm -R -nodisplay -m stamp_pem_run
./stamp_pem_run 0 0 0 0 0 0 0

cd ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/

