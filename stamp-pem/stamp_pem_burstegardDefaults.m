function params = stamp_pem_burstegardDefaults(params)
% function params = stamp_pem_burstegardDefaults(params)
% Sets parameters for STAMP-PEM burstegard search.
% Routine written by Shivaraj Kandhasamy and modified by Michael Coughlin.
% Modified: August 4, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters and their values.
params.doBurstegard = true;
params.burstegard.NCN = 80;
params.burstegard.NR = 2;

% Change pixel threshold depending on segment duration as
% burstegard segdefaults with too many loud pixels
if params.segmentDuration == 1
   params.burstegard.pixelThreshold = 2.5;
elseif params.segmentDuration == 10
   params.burstegard.pixelThreshold = 2;
elseif params.segmentDuration == 100
   params.burstegard.pixelThreshold = 0.75;
else
   params.burstegard.pixelThreshold = 1;
end

params.burstegard.tmetric = 1;
params.burstegard.fmetric = 1;
params.burstegard.debug = 0;
params.burstegard.weightedSNR = false;

% The super-cluster is the sum of all clusters.
params.burstegard.super = false;

% findtrack is a cluster-the-cluster option.
params.burstegard.findtrack = true;

% rr is a variable for findtrack.
params.burstegard.rr = 50;


return;
