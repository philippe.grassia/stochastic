%% This is the L1PEM run with a 10s segment duration
%run_name corresponds to the params file we'll choose. see the
%/stamp-pem/input directory for available params file. 
run_name='L1PEM';
%concatenates params file for this run with your personal params file
pemParamsFile = stamp_pem_createParamsFile(run_name);
pemChannel = 'L1:PEM-EX_BSC4_ACCZ_OUT_DQ';
segmentDuration = 10;
startGPS = 1026409139;
endGPS = startGPS + 3600;
fmin= '10';
fmax='200';
stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS,fmin,fmax);
stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS,fmin,fmax);


