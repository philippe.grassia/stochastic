run_name='H1PEM';
pemParamsFile = stamp_pem_createParamsFile(run_name);
pemChannel = 'H1:PEM-MX_SEIS_VEA_FLOOR_Z_DQ';
%pemChannel = 'H1:PEM-MX_SEIS_VEA_FLOOR_QUAD_SUM_DQ';
segmentDuration = 10;
%startGPS = 1042936004;
startGPS = 1051879478;
endGPS = startGPS + 200;

%startGPS = 1048100000; 
%endGPS = 1048204573;

%endGPS = startGPS + 7200;
%endGPS = 1045785632;
%startGPS = endGPS - 3600;
fmin= '10';
%fmax='1023';
fmax = '256';
stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS,fmin,fmax);
stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS,fmin,fmax);

%stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS,fmin,fmax)


