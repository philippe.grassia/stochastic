
pemParamsFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_L1HIFOX.txt';
pemChannel = 'L1:ISI-HAM3_BLND_GS13RX_IN1_DQ';
run_name='L1HIFOX';
%concatenate params file for this run with your personal params file
pemParamsFile = stamp_pem_createParamsFile(run_name);
fmin= '0.1';
%fmax='200';
fmax = '128';
segmentDuration = 10;

fmax = '127';

startGPS = 1071187216;
endGPS = startGPS + 3600;

%stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, fmax);
stamp_pem_mod_dag(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, fmax);
%stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS,fmin, fmax);
%stamp_pem_run_all(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, fmax);
%stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS,fmin,fmax);


