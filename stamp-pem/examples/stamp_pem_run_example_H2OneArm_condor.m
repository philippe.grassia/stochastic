%run_name corresponds to the params file we'll choose. see the
%/stamp-pem/input directory for available params file. 
run_name='H2OneArm';
%concatenate params file for this run with your personal params file.
pemParamsFile = stamp_pem_CreateParamsFile(run_name);
segmentDuration = 1;
startGPS = 1027825216;
endGPS = startGPS + 600;
fmin= '10';
fmax='200';
stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS, fmin, fmax)


