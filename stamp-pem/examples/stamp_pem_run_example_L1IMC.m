
pemParamsFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_L1TRAIN.txt';
pemChannel = 'L1:HPI-HAM2_STSINF_A_Z_IN1_DQ';
%pemChannel = 'L1:OAF-CAL_IMC_F_DQ';
%pemChannel = 'L1:HPI-HAM2_BLND_L4C_Z_IN1_DQ';
%pemChannels = 'L1:ISI-HAM2_GS13INF_H1_IN1_DQ';
%pemChannel = 'L1:PEM-EX_BAYMIC_OUT_DQ';
%pemChannel = 'L1:IMC-F_OUT_DQ';
pemChannel = 'L1:HPI-BS_BLND_L4C_X_IN1_DQ';
pemChannel = 'L1:HPI-BS_STSINF_A_Z_IN1_DQ';
pemChannel = 'L1:OAF-CAL_MICH_CTRL_DQ';
pemChannel = 'L1:LSC-MICH_IN1_DQ';
run_name='L1IMC';
%concatenate params file for this run with your personal params file
pemParamsFile = stamp_pem_createParamsFile(run_name);
segmentDuration = 1;
%startGPS =  1053100000;
%startGPS = 1052000512;
%startGPS = 1052000000 - 256;
%endGPS = startGPS + 256;
startGPS =  1053100000;
endGPS = startGPS + 256;

endGPS =  1053100000;
startGPS = endGPS - 3600;

startGPS = 1057181416;
endGPS = startGPS + 3600;
%endGPS = startGPS + 750;

%startGPS = 1057181416 + 2250;
%endGPS = startGPS + 3600;
%endGPS = startGPS + 750;

fmin= '1';
%fmax='200';
fmax = '128';

fmin= '0.1';
%fmax='200';
fmax = '128';
segmentDuration = 10;

fmax = '127';

%startGPS = 1059686701;
%endGPS = 1059688211;

%startGPS = 1062615572;
%endGPS = 1062615572 + 1940;

%startGPS = 1062617656;
%endGPS = 1062617656 + 1240;

startGPS = 1062615572;
endGPS = 1062618896;

startGPS = 1057968016;
endGPS = 1057971616;

startGPS = 1066198636;
endGPS = 1066202236;

startGPS = 1065420016;
endGPS = startGPS + 3600;

startGPS = 1071187216;
endGPS = startGPS + 3600;

stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, fmax);
%stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS,fmin, fmax);
%stamp_pem_run_all(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, fmax);
%stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS,fmin,fmax);


