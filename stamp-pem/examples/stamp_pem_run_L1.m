function stamp_pem_run_L1(gpsStart,gpsEnd);


if isstr(gpsStart)
endGPS   = str2num(gpsEnd);
startGPS = str2num(gpsStart);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% MAKE PSL DAG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_name = 'L1PSL';
pemParamsFile   = stamp_pem_createParamsFile(run_name);
fmin            = '0.1';
segmentDuration = 10;
fmax            = '127';
stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, fmax);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% MAKE IMC DAG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_name = 'L1IMC';
pemParamsFile   = stamp_pem_createParamsFile(run_name);
fmin            = '0.1';
segmentDuration = 10;
fmax            = '127';
stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, fmax);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% MAKE PRMI DAG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_name = 'L1PRMI';
pemParamsFile   = stamp_pem_createParamsFile(run_name);
fmin            = '0.1';
segmentDuration = 10;
fmax            = '127';
stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, fmax);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% MAKE PRCL DAG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_name='L1PRCL';
pemParamsFile   = stamp_pem_createParamsFile(run_name);
fmin            = '0.1';
segmentDuration = 10;
fmax            = '127';
stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, fmax);

