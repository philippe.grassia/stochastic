#!/bin/bash

paramsFile=$1
segmentDuration=$2
startGPS=$3
endGPS=$4
fmin=$5
fmax=$6

source ${HOME}/matlab_script_2012a.sh
source ${HOME}/.bashrc
source ${HOME}/.bash_profile

cd /home/emacayeal/STAMP/STAMP-PEM/bin_all/
./stamp_pem_run_all ${paramsFile} ${segmentDuration} ${startGPS} ${endGPS} ${fmin} ${fmax}


