STAMP-PEM.php is the main summary page.

It links you to SP_Today.php, SP_Yesterday.php, SP_AllTime.php.

These three are identical 
except the file names they call. 
Comments are written only in SP_Today.php

Each of these calls individual events, 
which are in their own directory named
(for example) today1 for the top ranking today. All of these
individual events have identical code except, 
again, for relevant file names for plots and text files. 
Therefore, ONLY today1.php 
(in the sptoday1 directory) is commented.

Cheers,

Pat
