#! /bin/bash

for i in {1..10}
do

cd spyesterday$i

mv spyesterday_cluster_$i.png yesterday_cluster_$i.png

mv spyesterday_snr_$i.png yesterday_snr_$i.png

mv spyesterday$i.txt yesterday$i.txt

mv spyesterday$i.php yesterday$i.php

sed -i 's/spyesterday/yesterday/g' ./yesterday$i.php

cd ..

mv spyesterday$i yesterday$i

done

