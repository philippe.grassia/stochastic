#!/bin/bash

paramsFile=$1
segmentDuration=$2
startGPS=$3
endGPS=$4
fmin=$5
fmax=$6

source ${HOME}/.bashrc
source ${HOME}/.bash_profile

cd /home/emacayeal/matapps/packages/stochastic/trunk/stamp-pem/

/ldcg/matlab_r2012a/bin/matlab -q<<EOF
 stamp_startup;
 stamp_pem_make_dag('${paramsFile}','${segmentDuration}','${startGPS}','${endGPS}','${fmin}','${fmax}');
EOF

