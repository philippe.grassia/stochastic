function compareChannelLists(FramesList, pem_analysis_List)
%function compareChannelLists()
%List1='/home/meyers/channelList.txt';
%List2='./input/stamp_pem-H1-H2_R-channel_list.txt';
%
%This function checks two channel lists against one another to make sure
%that all channels used in pem analysis are available in the frames.
%

[ChannelListNames1, channelSampleRates1]=textread([FramesList], '%s %f');
[ChannelListNames2, channelSampleRates2]=textread([pem_analysis_List], '%s %f');
counter=0;
for ii=1:length(ChannelListNames1)
  for jj=1:length(ChannelListNames2)
    if strcmp(ChannelListNames2(jj), ChannelListNames1(ii))
      counter=counter+1;
    end
  end
end

if (counter>length(ChannelListNames2))
 error('More matched channel names than input channels? Error in use.');
end
if (counter<length(ChannelListNames2))
  fprintf('%s \n','missing channels!!')
end
if(counter == length(ChannelListNames2))
  fprintf('%s \n','all channels are present');
end