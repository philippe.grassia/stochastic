function pulsar_lines = pem_lines(params, map)

  [pulsar_names, gw_freq] = textread('pem_freq.txt','%s %f','delimiter',',');

  channel_data = NaN*ones(length(gw_freq),2);
  darm_data = NaN*ones(length(gw_freq),2);

  for j=1:length(pulsar_names)
     which_freq = find(min(abs(map.yvals-gw_freq(j))) ... 
        == abs(map.yvals-gw_freq(j)));
     freq_line = abs(map.snr(which_freq,:));
     which_vals = find(freq_line~=0);

     freq_snr(j) = mean(freq_line(which_vals));

     which_freq = find(abs(map.yvals-gw_freq(j)) <= 0.5);
     frequencies = map.yvals(which_freq);

     this_data = mean(abs(map.naiP1(which_freq,:)),2);
     which_freq_max = find(max(this_data) == this_data);
     darm_data(j,1) = frequencies(which_freq_max);
     darm_data(j,2) = this_data(which_freq_max);     

     this_data = mean(abs(map.naiP2(which_freq,:)),2);
     which_freq_max = find(max(this_data) == this_data);
     channel_data(j,1) = frequencies(which_freq_max);
     channel_data(j,2) = this_data(which_freq_max);

  end

  pulsar_lines.pulsar_names = pulsar_names;
  pulsar_lines.gw_freq = gw_freq;
  pulsar_lines.freq_snr = freq_snr;    
  pulsar_lines.channel_data = channel_data;
  pulsar_lines.darm_data = darm_data;

return
