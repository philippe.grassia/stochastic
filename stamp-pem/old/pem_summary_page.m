function [] = pem_summary_page(user,time,folder)

%Get All Files

CHANNELS = textread('pem_channel_list.txt','%s');

fid=fopen('list.txt','w+');
for it =1:length(CHANNELS)
   channel_split=split(CHANNELS{it},":");
   channel_split=cellstr(channel_split);
   channel_name=[channel_split{1,1} '_' channel_split{2,1}];
   fprintf(fid,'%s\n',channel_name);
end
fclose(fid);

CHANNELS = textread('list.txt','%s');

fid=fopen('pem_summary_page.html','w+');

% Create HTML file
fprintf(fid,'<html>\n<body>\n');
fprintf(fid,'<title>Radon Summary Page</title>\n');
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="1" cellspacing="1">\n')
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Radon Summary for %s</span></big></big></big></big>\n',folder);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');

% SEIS Table

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="1" cellspacing="1">\n')
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Daily Radon Run</span></big></big></big></big>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="1" cellspacing="1">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td>\n');
fprintf(fid,'Channel Names\n');
fprintf(fid,'</td>\n');

fprintf(fid,'<td>\n');
fprintf(fid,'Radon');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');

fprintf(fid,'<tr>\n');
for i=1:length(CHANNELS)
  fprintf(fid,'<tr>\n');
  fprintf(fid,'<td>\n');
  fprintf(fid,'%s',CHANNELS{i});
  fprintf(fid,'</td>\n');
  fprintf(fid,'<td>\n');
  fprintf(fid,'<a href="files/%s/%s_radon.html">Radon</a><br>',CHANNELS{i},CHANNELS{i});
  fprintf(fid,'</td>\n');
  fprintf(fid,'</tr>\n');
end

fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');

fprintf(fid,'Created by user %s on %s \n',user,time);
fprintf(fid,'<br>\n');
fprintf(fid,'Please E-mail Questions and Comments to coughlim@carleton.edu and scottcoughlin2014@u.northwestern.edu\n');

fclose(fid);

