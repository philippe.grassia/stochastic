function pem_lines_combine(channel_name,folder,user,ifo,frame_size)

plots.plcnt=1;
set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

gps_times_file=load('jobfile_all.txt');
gps_begin=gps_times_file(:,2);
gps_end=gps_times_file(:,3);

darm_channel_name = 'V1_Pr_B1_ACp_4000Hz';
channel_name='V1_Em_SEBDCE01_500Hz';
lineNumber='22';
folder='2011_06_04';
user='mcoughlin';
frame_size='4';
ifo='V1';

path=['/archive/home/' user '/STAMP/STAMP-PEM/' ifo '/daily/' ... 
      folder '/' num2str(frame_size)];

output=['/archive/home/' user '/public_html/STAMP/STAMP-PEM/' ifo '/daily/' ...
      folder '/' num2str(frame_size)];

folder_path=[path '/files/' channel_name];
output_folder_path = [folder_path '/Pulsar_Plots'];

mkdir_command = ['mkdir ' output_folder_path];
system(mkdir_command);

[pulsar_names, gw_freq] = textread('pem_freq.txt','%s %f','delimiter',',');

pulsar_line_stats = NaN*ones(length(gw_freq),length(gps_times_file));

for i=1:length(gps_times_file)
 try
   line_stats=load([folder_path '/' num2str(i) '/map.mat'],'stoch_out');
   pulsar_line_stats(:,i) = line_stats.stoch_out.pulsar_lines.freq_snr;   
 catch
 end
end

which_times = pem_tconvert(gps_begin);
for i=1:length(gps_begin)
   if which_times(i,2)<10
      month_str = ['0' num2str(which_times(i,2))];
   else
      month_str = num2str(which_times(i,2));
   end
   if which_times(i,3)<10
      day_str = ['0' num2str(which_times(i,3))];
   else
      day_str = num2str(which_times(i,3));
   end

   date_string{i} = strcat(num2str(which_times(i,1)),'_',month_str,'_',day_str);
end

[which_days,indexes] = unique(date_string,'first');

freq_fscan_channel = NaN*ones(length(gw_freq),length(which_days),3);
freq_fscan_darm = NaN*ones(length(gw_freq),length(which_days),3);

for i=1:length(which_days)

   data_output = pem_get_fscan(which_days{i},channel_name,gw_freq);
   freq_fscan_channel(:,i,:) = data_output;

   data_output = pem_get_fscan(which_days{i},darm_channel_name,gw_freq);
   freq_fscan_darm(:,i,:) = data_output;

end

correlation_values = NaN*ones(length(gw_freq),3);

for i = length(gw_freq)

   tt = (gps_begin - gps_begin(1))/86400;

   figure(plots.plcnt);
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   clf
   plot(tt,pulsar_line_stats(i,:))
   xlabel('Time [days]')
   ylabel('SNR')
   grid
   saveas(gcf,[output_folder_path '/' num2str(i) '_snr.jpeg']);
   close(plots.plcnt);
   plots.plcnt = plots.plcnt +1;

   tt_daily = tt(indexes);

   figure(plots.plcnt);
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   clf
   plot(tt_daily,freq_fscan_channel(i,:,1),tt_daily,freq_fscan_darm(i,:,1))
   xlabel('Time [days]')
   ylabel('Fscan SNR')
   grid
   hleg1 = legend({channel_name,darm_channel_name});
   set(hleg1,'Location','NorthEast')
   set(hleg1,'Interpreter','none')
   saveas(gcf,[output_folder_path '/' num2str(i) '_fscan_snr.jpeg']);
   close(plots.plcnt);
   plots.plcnt = plots.plcnt +1;

   figure(plots.plcnt);
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   clf
   plot(tt_daily,freq_fscan_channel(i,:,2),tt_daily,freq_fscan_darm(i,:,2))
   xlabel('Time [days]')
   ylabel('Peak Frequency')
   grid
   hleg1 = legend({channel_name,darm_channel_name});
   set(hleg1,'Location','NorthEast')
   set(hleg1,'Interpreter','none')
   saveas(gcf,[output_folder_path '/' num2str(i) '_fscan_freq_1Hz.jpeg']);
   close(plots.plcnt);
   plots.plcnt = plots.plcnt +1;
   
   figure(plots.plcnt);
   set(gcf, 'PaperSize',[10 6]);
   set(gcf, 'PaperPosition', [0 0 10 6]);
   clf
   plot(tt_daily,freq_fscan_channel(i,:,3),tt_daily,freq_fscan_darm(i,:,3))
   xlabel('Time [days]')
   ylabel('Peak Frequency Fscan SNR')
   grid
   hleg1 = legend({channel_name,darm_channel_name});
   set(hleg1,'Location','NorthEast')
   set(hleg1,'Interpreter','none')
   saveas(gcf,[output_folder_path '/' num2str(i) '_fscan_snr_1Hz.jpeg']);
   close(plots.plcnt);
   plots.plcnt = plots.plcnt +1;

   correlation_values(i,1) = pem_coherence(freq_fscan_channel(i,:,1), ...
      freq_fscan_darm(i,:,1));
   correlation_values(i,2) = pem_coherence(freq_fscan_channel(i,:,2), ...
      freq_fscan_darm(i,:,2));
   correlation_values(i,3) = pem_coherence(freq_fscan_channel(i,:,3), ...
      freq_fscan_darm(i,:,3));

end

%eht: no keyboard commands in svn code please
%keyboard

fid=fopen([path '/files/' channel_name '/segments.txt'],'w+');
for i=1:length(start_gps_times)
   fprintf(fid,'%.0f %.0f %.5f\n',start_gps_times(i),end_gps_times(i),radon_values(i)); 
end
fclose(fid);

fid=fopen([path '/files/' channel_name '/' channel_name '_radon.html'],'w+');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">%s Radon Page for %s</span></big></big></big></big><br>\n',channel_name,folder);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

for i=1:length(start_gps_times)
   fprintf(fid,'<table\n');
   fprintf(fid,'style="text-align: left; width: 1260px; height: 30px; margin-left:auto; margin-right: auto;"\n');
   fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
   fprintf(fid,'<tbody>\n');
   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"><big><big><big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Event %s</span></big></big></big></big><br>\n',num2str(i));
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');
   fprintf(fid,'</tbody>\n');
   fprintf(fid,'</table>\n');
   fprintf(fid,'<br>');

   fprintf(fid,'<table\n');
   fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
   fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
   fprintf(fid,'<tbody>\n');

   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">GPS Time of Block: %.1f - %.1f </span></big></big><br>\n',start_gps_times(i),end_gps_times(i));
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');

   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Max Radon Power: %.5f </span></big></big><br>\n',radon_values(i));
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');

   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Proposed Veto Times: %.1f - %.1f </span></big></big><br>\n',start_airplane_times(i),end_airplane_times(i));
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');

   fprintf(fid,'</tbody>\n');
   fprintf(fid,'</table>\n');
   fprintf(fid,'<br>');

   fprintf(fid,'<table\n');
   fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
   fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n')
   fprintf(fid,'<tbody>\n');
   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Veto Map </span></big></big><br>\n');
   fprintf(fid,'</td>\n');
   fprintf(fid,'<td\n');
   fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
   fprintf(fid,'style="font-weight: bold;">Radon Reconstruction </span></big></big><br>\n');
   fprintf(fid,'</td>\n');
   fprintf(fid,'</tr>\n');
   fprintf(fid,'<tr>\n');
   fprintf(fid,'<td style="vertical-align: top;">\n');
   fprintf(fid,'<a href="%s/snr.eps"><img alt="" src="%s/snr.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',num2str(folder_number(i)),num2str(folder_number(i)));
   fprintf(fid,'</td>');
   fprintf(fid,'<td style="vertical-align: top;">\n');
   fprintf(fid,'<a href="%s/large_cluster_plot.eps"><img alt="" src="%s/large_cluster_plot.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',num2str(folder_number(i)),num2str(folder_number(i)));
   fprintf(fid,'</td>');
   fprintf(fid,'</tr>\n');
   fprintf(fid,'</tbody>\n');
   fprintf(fid,'</table>\n');
   fprintf(fid,'<br>');

end
