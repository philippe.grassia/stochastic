function [time_begin_place, time_end_place] = ...
                 searchClusters(params, map)
%function [max_Y_gamma, max_sigma_gamma, max_snr_gamma] = ...
%                 searchClusters(params, map)
% clusterstamp Apply density based clustering algorithm developed by R.
% Khan (Class. Quantum Grav. 26 155009)
% INPUT :
%     params.cluster.NN              <- minimum number of pixels in each cluster
%     params.cluster.NR              <- neighbor radius as determined by metric 
%     params.cluster.pixelThreshold  <- threshold for pixels to be included in 
%                                       the cluster
%     parmas.cluster.tmetric         <- time metric of ft-map
%     parmas.cluster.fmetric         <- frequency metric of ft-map
%     map.xvals                      <- input GPS times array 
%     map.yvals                      <- input frequencies array 
%     map.snr                        <- snr map
%     map.Y                          <- Y map
%     map.sigma                      <- sigma map
%
% OUTPUT :
%     Details of the largest cluster
%       -> Y_gamma
%       -> sigma_gamma
%       -> snr_gamma
% shivaraj.kandhasamy (based on burstcluster.m by Peter Kalmus)
% shivaraj@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NN = params.cluster.NN;
NR = params.cluster.NR;
pixelThreshold = params.cluster.pixelThreshold;
tmetric = params.cluster.tmetric;
fmetric = params.cluster.fmetric;
t = map.xvals;
f = map.yvals;
map.snr=abs(map.snr);

% find all the pixels with snr > pixelThreshold
[rows, cols] = find(map.snr > pixelThreshold);

fprintf('num pixels: %d out of %d with snr > %.2f;  percentage: %1.2f \n', ...
         length(rows), numel(map.snr), pixelThreshold,...
         length(rows)/numel(map.snr)*100);

for ctr=1:length(rows)
  pixels.time(ctr) = t(cols(ctr));
  pixels.frequency(ctr) = f(rows(ctr));
  pixels.snr(ctr) = map.snr(rows(ctr), cols(ctr));
  pixels.y(ctr) = map.y(rows(ctr), cols(ctr));
  pixels.sigma(ctr) = map.sigma(rows(ctr), cols(ctr));
end  

% measure distances
tiles = [pixels.time' pixels.frequency' pixels.snr' pixels.y' pixels.sigma'];
distance = clusterDistancestamp(tiles, tmetric, fmetric);
distMat = squareform(distance);

% labelling each seed; general informations will be available on first seed only
seedpoint{1}.NN = NN; % 
N = length(distMat(:,1));
seedpoint{1}.N = N; % number of tiles or seeds

% diagnosing or debugging purpose 
if params.debug
 sortMat = sort(distMat, 'ascend');
 plot(sort((sortMat(NN+1,:)), 'descend'));
 xlabel('Tile Number');
 ylabel(sprintf('Distance to %i-distant-neighbor', NN));
 title('Distance graph');
end

for i = 1:N
    % Get index of neighbors, but not of self
    seedpoint{i}.neighbor.I = find(distMat(:,i)<=NR & distMat(:,i)~=0);
    % Get number of neighbors
    seedpoint{i}.neighbor.n = length(seedpoint{i}.neighbor.I);
    % Can it be a seed
    if seedpoint{i}.neighbor.n >= NN 
%        fprintf('%d has neibhours %d which is >= %d\n',i,seedpoint{i}.neighbor.n,NN);
        seedpoint{i}.ID = 0; % ID == 0 means it is a seed
        [zz, Index] = sort(tiles(seedpoint{i}.neighbor.I, 3), 'descend');
        seedpoint{i}.neighbor.I = seedpoint{i}.neighbor.I(Index);
    else
        seedpoint{i}.ID = NaN;
    end
end

% Index of tiles sorted along SNR
[zz,Index] = sort(tiles(:,3), 'descend');

% Set recursion limit
seedpoint{1}.rLimit = 490; % Default matlap limit is 500
set(0,'RecursionLimit', seedpoint{1}.rLimit);

% Initiate Cluster ID
clusterID = 0;
for i = 1:N
    ii = Index(i); % Pick higher significance seeds first
    if (seedpoint{ii}.ID == 0) % If seedpoint is possible seed and not in other cluster already
        clusterID  = clusterID + 1;
        seedpoint{ii}.ID = clusterID;
        [k, seedpoint] = expandCluster(ii,seedpoint,3);
    end
end

% looking for isolated island of (or single) pixel with snr > threshold
for i = 1:N
    if ~(seedpoint{i}.ID > 0)
        clusterID  = clusterID + 1;
        seedpoint{i}.ID = clusterID;
    end
end

%%%%%%%%%%%%% Clustering ends and reporting starts here %%%%%%%%%%%%%%%%%%%%%%%

% sort clusters
tempClusters = 0;
for i = 1:N
    tempClusters(i) = seedpoint{i}.ID;
end
uniqueClusters = sort(unique(tempClusters(tempClusters > 0)));
clusters.numberOfClusters = length(uniqueClusters);

fprintf('Total number of clusters: %i \n', clusters.numberOfClusters);

if clusters.numberOfClusters > 0
  % Read cluster info
  for clusterIndex = 1:clusters.numberOfClusters
    % Read time, frequency,and SNR info of each tile
    i = find(tempClusters == uniqueClusters(clusterIndex));
    t = pixels.time(i);
    f = pixels.frequency(i);
    z = pixels.snr(i);

    % Tile indices, total number of tiles and biggest cluster 
    clusters.indices{clusterIndex} = i;
    clusters.n{clusterIndex} = length(i);
    cluster_length(clusterIndex) = length(i); % just for finding largest cluster
  end

  % display clustering
  if params.savePlots %-----------------------------------------------
    figure
    axis([min(map.xvals) max(map.xvals) min(map.yvals) max(map.yvals)])
    xlabel('time [sec]');
    ylabel('Frequency [Hz]');
    hold on;
    
    % it should cycle through markers instead of relying
    % on a huge vector.  use mod...
    markers = {'ro', 'go', 'bo', 'co', 'mo', 'ko', ...
         'rs', 'gs', 'bs', 'cs', 'ms', 'ks', ...
         'rd', 'gd', 'bd', 'cd', 'md', 'kd', ...
         'rv', 'gv', 'bv', 'cv', 'mv', 'kv', ...
         'r^', 'g^', 'b^', 'c^', 'm^', 'k^', ...
         'r<', 'g<', 'b<', 'c<', 'm<', 'k<', ...
         'r>', 'g>', 'b>', 'c>', 'm>', 'k>', ...
         'rp', 'gp', 'bp', 'cp', 'mp', 'kp', ...
         'rh', 'gh', 'bh', 'ch', 'mh', 'kh'};
    
    for clusterIndex = 1 : length(uniqueClusters)
        indices = find(tempClusters == uniqueClusters(clusterIndex));
        handle = plot(tiles(indices, 1), tiles(indices, 2), markers{mod(clusterIndex, length(markers))+1 } );
        set(handle, 'MarkerSize', 10);
        set(handle, 'MarkerFaceColor', get(handle, 'Color'));
    end
    title(['SNR threshold = ' num2str(pixelThreshold) ' ,number of clusters = ' ...
           num2str(length(uniqueClusters))]) 
    hold off;
%    print -depsc clusters_plot.eps;
%    print -dpng clusters_plot.png;  
  end %-------if params.savePlots--------------------------------------

  % calculate Y_Gamma, sigma_Gamma, SNR_Gamma of all the clusters
  for clusterIndex = 1 : length(uniqueClusters)
    indices = find(tempClusters == uniqueClusters(clusterIndex));
    Y_gamma(clusterIndex) = ...
       sum(tiles(indices,4)./tiles(indices,5).^2)/sum(1./tiles(indices,5).^2);
    sigma_gamma(clusterIndex) = 1/sqrt(sum(1./tiles(indices,5).^2));
    snr_gamma(clusterIndex) = Y_gamma(clusterIndex)/sigma_gamma(clusterIndex);
  end
   
  % cluster with largest SNR
  [all_snr_gamma,cluster_index] = sort(snr_gamma,'descend');
  indices = find(tempClusters == uniqueClusters(cluster_index(1)));
  max_Y_gamma = Y_gamma(cluster_index(1));
  max_sigma_gamma = sigma_gamma(cluster_index(1));
  max_snr_gamma = snr_gamma(cluster_index(1));
  fprintf('The largest SNR is %.2f with %d number of pixel(s) \n',...
         max_snr_gamma, length(indices));
  if params.savePlots %-----------------------------------------------
    which_t_vals=zeros(size(map.xvals));
    t_vals = tiles(indices,1);
    f_vals = tiles(indices,2);
    temp_map = zeros(size(map.snr));
    for ii = 1:length(t_vals)
      cutt = map.xvals==t_vals(ii);
      cutf = map.yvals==f_vals(ii);
      temp_map(cutf,cutt) = map.snr(cutf,cutt);
      which_t_vals=which_t_vals+cutt;
    end
    printmap(temp_map,map.xvals+0.5,map.yvals+0.5,'time (s)','f (Hz)',...
            'SNR',[-5 5]) 
    print('-dpng',[params.which_folder 'large_cluster_plot.png']);
    print('-depsc2',[params.which_folder 'large_cluster_plot.eps']);
    which_t=find(which_t_vals>0);
    time_begin_place=min(which_t);
    time_end_place=max(which_t);
  end
end

return;
