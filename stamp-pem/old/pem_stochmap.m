function [rmax,time_begin,time_end]=stochmap(params, jobfile, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function stochmap(params, jobfile, varargin)
% E.g., 
%   params = stampDefaults;
%   jobfile='/archive/home/ethrane/preproc/S5H1L1_full_run.txt';
%   stochmap_dev(params, jobfile, 816088197, 816089507); % job 10
% or,
%   stochmap_dev(params, jobfile, 10); % job 10
% output (depending on what is requested in params struct):
%   diagnostic outputfile: map.mat
%   diagnostic plots: snr.eps, y_map.eps, sig_map.eps + .png versions
%   additional output if requested by search algorithm plug-ins
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% E. Thrane, S. Kandhasamy, S. Dorsher, S. Giampanis, M. Coughlin, 
% T. Prestegard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;

% For Monte Carlo studies we can call stochmap from within preproc to avoid
% writing to frames.  This happens when params.frompreproc==true.  Normal
% operating mode (reading data from frames) if params.frompreproc==false.
if (params.frompreproc)
  params.startGPS = strassign(varargin{1});
  params.endGPS = strassign(varargin{2});
  nSegs = length(params.pass.GPStime); % number of segments

else % read in data from frames
  % check values of params for self-consistency
  paramCheck(params);

  % read in and parse jobfile
  job_details = load(jobfile);
  AllStartTimes = job_details(:,2);
  AllEndTimes = job_details(:,3);
  AllJobDurations = job_details(:,4);

  % look at job=1 in order to determine the segment duration
  tmpFrameFile = [params.intFrameCachePath 'frameFiles' ...
      params.ifo1(1) params.ifo2(1) '.1.txt'];
  % create list of frame files
  try
    tmpframeFiles=ctextread(tmpFrameFile, '%s\n', -1, 'commentstyle','matlab');
  catch
    error('Cannot locate frame cachefile for job 1.\n');
  end
  temp_dur = frgetvect(tmpframeFiles{1}, ...
    [params.ifo1 params.ifo2 ':segmentDuration'], 0, 1000);
  params.segmentDuration = temp_dur(1);

  if(nargin==3) % job number specified
    params.jobNumber = strassign(varargin{1});
    params.startGPS = AllStartTimes(params.jobNumber);
    params.endGPS = AllEndTimes(params.jobNumber);
  elseif(nargin==4) % params.startGPS and params.endGPS are specified
    params.startGPS = strassign(varargin{1});
    params.endGPS = strassign(varargin{2});
  else
    error('Error: the number of arguments should be 3 or 4.');
  end

  % consider only segments between params.startGPS and params.endGPS
  cut = AllEndTimes-params.segmentDuration>=params.startGPS & ...
    AllStartTimes+params.segmentDuration<=params.endGPS;
  joblist = [1:length(AllStartTimes)];
  kept_jobs = joblist(cut);
  % select the frames needed for this interval
  k=1; % index for frame files of every segment
  for ii=1:length(kept_jobs)
    IM_CacheFile  = [params.intFrameCachePath 'frameFiles' params.ifo1(1) ...
      params.ifo2(1) '.' num2str(kept_jobs(ii)) '.txt'];
    frameFilesJobs = textread(IM_CacheFile, '%s\n', -1, 'commentstyle', ...
      'matlab');
    for j=1:length(frameFilesJobs)
      % record the GPS times
      [no_use1, no_use2, no_use3, segGPSstart] = ...
	frgetvect(frameFilesJobs{j}, [params.ifo1 ':AdjacentPSD']);
      GPStimes(k) = segGPSstart;
      % record the frame names
      if(segGPSstart>=params.startGPS && segGPSstart<=params.endGPS)
        frameFiles{k}=frameFilesJobs{j};
        k=k+1;
      end
    end
  end

  % check for data before continuing
  if(~exist('frameFiles'))
    error('No data between start and end times.\n');
    return;
  end

  nSegs = length(frameFiles); % number of segments

end % closes loop for case where params.frompreproc==false

for I=1:nSegs

  if (params.frompreproc) % get data from params struct.

    J=params.pass.which_segs(I);

    P1 = params.pass.P1(:,J);           P2 = params.pass.P2(:,J);
    naiP1 = params.pass.naiP1(:,J);     naiP2 = params.pass.naiP2(:,J);
    CC = params.pass.CC(:,J);
    pp.flow = params.pass.ppflow(:,J);  pp.fhigh = params.pass.ppfhigh(:,J);
    pp.deltaF = params.pass.ppdeltaF(:,J);
    pp.w1w2bar = params.pass.ppw1w2bar(:,J);
    pp.w1w2squaredbar = params.pass.ppw1w2squaredbar(:,J);
    pp.w1w2ovlsquaredbar = params.pass.ppw1w2ovlsquaredbar(:,J);
    try
      params.numSegmentsPerInterval = params.pass.nSPI(:,I);
    catch
      params.numSegmentsPerInterval = [];
    end
    params.resampleRate1 = params.pass.resampleRate1(:,J);
    params.resampleRate2 = params.pass.resampleRate2(:,J);
    params.nResample1 = params.pass.nResample1(:,J);
    params.nResample2 = params.pass.nResample2(:,J);
    params.betaParam1 = params.pass.betaParam1(:,J);
    params.betaParam2 = params.pass.betaParam2(:,J);
    GPStimes(I) = params.pass.GPStime(I);
    segGPSTime = GPStimes(I);


  else  % read in data from STAMP frames
    % read in STAMP data      
    inputfile = char(frameFiles{I});
    [P1, no_use1, no_use2, segGPSTime] = frgetvect(inputfile, ...
      [params.ifo1 ':AdjacentPSD'], 0, params.segmentDuration);
    P2 = frgetvect(inputfile,[params.ifo2 ':AdjacentPSD'], ...
      0, params.segmentDuration);
    naiP1 = frgetvect(inputfile,[params.ifo1 ':LocalPSD'], ...
      0, params.segmentDuration);
    naiP2 = frgetvect(inputfile,[params.ifo2 ':LocalPSD'], ...
      0, params.segmentDuration);
    CC = frgetvect(inputfile,[params.ifo1 params.ifo2 ':CSD'], ...
      0, params.segmentDuration);

    if I==1 % some variables are read in only once
      pp.flow = frgetvect(inputfile,[params.ifo1 params.ifo2 ':flow'], 0, ...
        params.segmentDuration);
      pp.fhigh = frgetvect(inputfile,[params.ifo1 params.ifo2 ':fhigh'], ...
        0, params.segmentDuration);
      pp.deltaF = frgetvect(inputfile,[params.ifo1 params.ifo2 ':deltaF'], ...
        0, params.segmentDuration);
      pp.w1w2bar = frgetvect(inputfile,[params.ifo1 params.ifo2 ...
        ':w1w2bar'], 0, params.segmentDuration);
      pp.w1w2squaredbar = frgetvect(inputfile, [params.ifo1 ...
        params.ifo2 ':w1w2squaredbar'], 0, params.segmentDuration);
      pp.w1w2ovlsquaredbar = frgetvect(inputfile, [params.ifo1 ...
        params.ifo2 ':w1w2ovlsquaredbar'], 0, params.segmentDuration);
      params.numSegmentsPerInterval = frgetvect(inputfile, ...
        [params.ifo1 params.ifo2 ':numSegmentsPerInterval'], 0, ...
        params.segmentDuration);

      % check that pp and params have reasonable and consistent values
        if (params.fmin<pp.flow | params.fmax>pp.fhigh)
        error('params.fmin and/or params.fmax are out of range of data!\n');
      end

      % these variables pertain to preproc ------------------------
      params.resampleRate1 = frgetvect(inputfile, [params.ifo1 ...
        params.ifo2 ':resampleRate1'], 0, params.segmentDuration);
      params.resampleRate2 = frgetvect(inputfile, [params.ifo1 ...
        params.ifo2 ':resampleRate2'], 0, params.segmentDuration);
      params.nResample1 = frgetvect(inputfile, ...
        [params.ifo1 params.ifo2 ':nResample1'], 0, params.segmentDuration);
      params.nResample2 = frgetvect(inputfile, ...
        [params.ifo1 params.ifo2 ':nResample2'], 0, params.segmentDuration);
      params.betaParam1 = frgetvect(inputfile, ...
        [params.ifo1 params.ifo2 ':betaParam1'], 0, params.segmentDuration);
      params.betaParam2 = frgetvect(inputfile, ...
        [params.ifo1 params.ifo2 ':betaParam2'], 0, params.segmentDuration);
      %
      parmchk = params.resampleRate1 + params.resampleRate2 + ...
        params.nResample1 + params.nResample2 + params.betaParam1 + ...
        params.betaParam2;
      if isempty(parmchk)
        params = preprocDefaults(params);
        fprintf(['Warning: default preproc variables not defined.  ' ...
          'Assuming default values.']);
      end
    end

  end % closes loop for params.frompreproc==false (data is read from frames)

  %---STAMP DATA FOR SEGMENT=I HAS NOW BEEN EITHER READ IN FROM FRAME OR---%
  %---PASSED TO STOCHMAP BY PREPROC.  NOW WE PROCESS THE DATA.-------------%

  % Sigma is scaled by a bias factor which depends on the number of segments 
  % per interval (nSPI) and the number of coarse-graining averages (N) which
  % is equal to segmentDuration*deltaF.  The bias factor is determined
  % numerically with separate code (contact M. Coughlin and E. Thrane for
  % details).  Here we get the bias factor using a look-up table which contains
  % bias factors for some combinations of N=1,2,13,15 and nSPI=9,13,17.  
  % If you are processing data that with a combination of (N, nSPI) that is not
  % in the table the bias factor will be set to 1 and you can deal with the
  % bias in post-processing.  For reference, the bias factor is 1.02 for 60s
  % segments with N=15 (deltaF=0.25 Hz).
  %
  if I==1
    if length(params.numSegmentsPerInterval)==0
      pp.wing_fac = 1;
      fprintf('Warning: Unable to determine numSegmentsPerInterval.\n');
      fprintf('Warning: wing_fac not defined; setting it to 1.\n');
    else
      bias_table = load('N_nSPI_Value.dat');
      which_N = find(pp.deltaF*params.segmentDuration==bias_table(:,1));
      which_nSPI = find(params.numSegmentsPerInterval==bias_table(:,2));
      which_value = intersect(which_N,which_nSPI);
      if length(which_value)==1
        pp.wing_fac=bias_table(which_value,3);
      else
        pp.wing_fac=1;
        fprintf('Warning: wing_fac is being set to 1.\n');
      end
    end
  end

  % calculate adjacent PSD structs for use in sigma calculation
  calPSD1_avg = constructFreqSeries(P1, pp.flow, pp.deltaF, 1);
  calPSD2_avg = constructFreqSeries(P2, pp.flow, pp.deltaF, 1);

  % create frequency mask
  dataFM = constructFreqMask(pp.flow, pp.fhigh, pp.deltaF, ...
    params.freqsToRemove, params.nBinsToRemove, params.doFreqMask);
  mask = constructFreqSeries(dataFM, pp.flow, pp.deltaF);

  % create arrays of frequency and (empty) overlap factor
  numFreqs = floor((pp.fhigh-pp.flow)/pp.deltaF)+1;
  f = pp.flow + pp.deltaF*transpose([0:numFreqs-1]);
  % from loadAuxiliarInput (if params.doDirectional)
  gamma = constructFreqSeries(ones(numFreqs,1), pp.flow, pp.deltaF, 1);

  % prepare to calculate Q filter
  numPoints1 = params.segmentDuration*params.resampleRate1;
  % the next line assumes hannDuration=segmentDuration
  params.fft1.dataWindow = tukeywin(numPoints1, 1);
  params.fft1.fftLength = 2*numPoints1;
  %
  numPoints2 = params.segmentDuration*params.resampleRate2;
  params.fft2.dataWindow = tukeywin(numPoints2, 1);
  params.fft2.fftLength = 2*numPoints2;
  %
  xf.data = ones(length(f),1);
  xf.flow = pp.flow;
  xf.deltaF = pp.deltaF;
  xf.symmetry = 1;
  %
  % calculate optimal filter, theoretical variance, and sensitivity
  % integrand using avg psds
  [Q, ccVar, sensInt] = calOptimalFilter(params.segmentDuration, gamma, ...
    xf, params.alphaExp, calPSD1_avg, calPSD2_avg, params.fft1.dataWindow, ...
    params.fft2.dataWindow, mask);

  % calculate the CSD, ccStat and ccSpec
  CSD = constructFreqSeries(CC /2 * params.segmentDuration ...
    * pp.w1w2bar, pp.flow, pp.deltaF, 1);
  maxCorrelationTimeShift=0.011;
  [ccStat, ccSpec] = processCSD(CSD, Q, maxCorrelationTimeShift);

  % call radiometer code (directional phase calculated in subroutine)
  [det1 det2] = ifo2dets(params);
  midGPSTime = segGPSTime + params.segmentDuration/2;
%eht  radmap = ccSpecReadout(det1, det2, midGPSTime, ...
%eht    [params.ra params.dec], ccSpec, ccVar,sensInt, 0);
% for glitch rejection we calculate the imaginary part of ccSpec as well
%eht  imag_radmap = ccSpecReadout_imag(det1, det2, midGPSTime, ...
%eht    [params.ra params.dec], ccSpec, ccVar,sensInt, 0);
  [radmap, imag_radmap] = ccSpecReadout_stamp(det1, det2, midGPSTime, ...
    [params.ra params.dec], ccSpec, ccVar,sensInt, 0, params);

  % calculate ft-maps using radiometer map
  map.y(:,I) = radmap(:,1);
  map.z(:,I) = imag_radmap(:,1);
  map.sigma(:,I) = radmap(:,2);
  map.segstarttime(I) = segGPSTime;

  % calculate fluence in [ergs/cm^2]
  map.F(:,I) = pp.deltaF .* f.^2 .* ...
    params.ErgsPerSqCM .* (pi*params.c^3/(4*params.G)) .* map.y(:,I);
  map.sigF(:,I) = pp.deltaF .* f.^2 .* ...
    params.ErgsPerSqCM .* (pi*params.c^3/(4*params.G)) .* map.sigma(:,I);

  % add autopower maps to map struct if requested
  if (params.saveAutopower)
    map.naiP1(:,I) = naiP1;
    map.naiP2(:,I) = naiP2;
  end

end  % loop over intervals I

% we have no sensitivity to masked data so set sigma=1 at these freqs
map.sigma(isnan(map.sigma)) = 1;
map.y(isnan(map.y)) = 0;
map.z(isnan(map.z)) = 0;
map.sigF(isnan(map.sigF)) = 1e99;
map.F(isnan(map.F)) = 0;

% apply bias factor
map.sigma = map.sigma * pp.wing_fac;
map.sigF = map.sigF * pp.wing_fac;

% define SNR map
map.snr = map.y ./ map.sigma;
map.snrz = map.z ./map.sigma;

% define map metadata
map.f = f;
map.deltaF = pp.deltaF;
map.segDur = params.segmentDuration;

% crop freqs
map = finishMap(params, map, GPStimes);

% number of independent measurements
map.nindep = sum(sum(map.sigma ~= 1));

params.which_folder=['files/' params.channelName2 '/' num2str(params.jobNumber) '/'];

if params.doRadon
  [rmax, time_begin_place, time_end_place] = pem_searchRadon(params, map);
end

if rmax >= 0

params.savePlots=1;
if params.savePlots,  pem_saveplots(params, map); end

params.saveMat=1;
if params.saveMat, save([params.which_folder 'map.mat'], 'map'); end

if params.debug, fprintf('std(snr)=\n'); fprintf('%2.2f\n', std(map.snr)); end

% broadband radiometer comparison
if params.doRadiometer
  y_rad = sum(map.y.*map.sigma.^-2, 1) ./ sum(map.sigma.^-2, 1);
  y_sig = sum(map.sigma.^-2, 1).^-0.5;
  fprintf('radiometer=\n');
  fprintf('%1.4e +/- %1.4e\n', y_rad, y_sig);
end

if params.doBoxSearch
  max_SNR = searchBox(params, map);
end
if params.doLH
  searchLH(params, map); 
end

which_x_values=time_begin_place:time_end_place;
map.snr=abs(map.snr(:,which_x_values));
map.y=map.y(:,which_x_values);
map.sigma=map.sigma(:,which_x_values);
map.segstarttime=map.segstarttime(which_x_values);
map.xvals=map.xvals(which_x_values);

params = clusterDefaults(params);
if params.doClusterSearch
  % results for the largest cluster
  [time_begin_place, time_end_place] = pem_searchClusters(params, map);
end

time_begin=map.segstarttime(time_begin_place);
time_end=map.segstarttime(time_end_place);
end

fprintf('elapsed_time = %f\n', toc);


return
