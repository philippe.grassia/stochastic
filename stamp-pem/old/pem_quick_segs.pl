#! /usr/bin/perl
#  Generate cachefiles and jobfiles for segments between t1 and t2.
#  There is only one job.

$ifo2 = "V";
$t1 = shift;
$t2 = shift;
$type1=shift;
$ifo1 = shift;
$end = shift;
#$t2 = $t1+1000;

#$type1="H1_LDAS_C02_L2";
#$type1="H1_RDS_R_L1";
#$type2=L1_LDAS_C02_L2;
#$type2="HrecOnline";
$out="temp_H1V1";

$list1=`ligo_data_find -o $ifo1 -t $type1 -s $t1 -e $t2 -u file`;
#$list1 =~ s/gsiftp.*\n//g;
#$list1 =~ s/.*data.*\n//g;
$list1 =~ s/file:\/\/localhost//g;
#$list1 =~ s/V-CW_RDS-93//g;
$gps1=$list1;
$gps1 =~ s/.*$type1-(.*)-$end\.gwf\n/\1\n/g;
$gps1 =~ s/.*$type1-(.*)-32\.gwf\n/\1\n/g;
$gps1 =~ s/.*$type1-(.*)-8880\.gwf\n/\1\n/g;

open(gpsfile1,">gpsTimes$ifo1.1.txt");
open(framefile1,">frameFiles$ifo1.1.txt");
print framefile1 $list1;
print gpsfile1 $gps1;
close(gpsfile1); close(gpsfile2);

open(jobfile,">jobfile.txt");
$diff=$t2-$t1;
print jobfile "%seg    start      stop    duration\n";
print jobfile "   1  $t1  $t2   $diff\n";
