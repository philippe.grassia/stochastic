#!/bin/bash

cd ${HOME}/STAMP/STAMP-PEM/compiled_radon_code/
cp /archive/home/mcoughlin/IM/svn/trunk/stamp/stamp_pem/* .

matlab -q<<EOF
 pem_startup
 mcc -R -nojvm -m pem_run_stamp
EOF

source ${HOME}/MatlabSetup_R2007a_glnxa64.sh

./pem_run_stamp 1 1 1 1 1 1

cd /archive/home/mcoughlin/IM/svn/trunk/stamp/stamp_pem/

