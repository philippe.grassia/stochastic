function [adcdata, dataOK, params] = ...
   readTimeSeriesData2(channelName, dataStartTime, dataDuration, ...
		      frameType, frameDuration, gpsTimesFile, ...
		      frameCacheFile, params, mc)
% function [adcdata, dataOK, params] = ...
%   readTimeSeriesData2(channelName, dataStartTime, dataDuration, ...
%                      frameType, frameDuration, gpsTimesFile, ...
%                      frameCacheFile, params, mc)
%   It reads in time-series data from frame files and if params.stampinj true, 
%     adds time domain injection to the data that was read from frames.
%
%  If channelName starts with 'matlab:' the data are assumed to be in .mat
%  files rather than frames, and the string following the colon is assumed
%  to be the name of the variable containing the time series.
%
%  dataOK is a boolean (=true or false) which indicates good or bad
%  (i.e., missing) data
%
%  Routine written by Joseph D. Romano and John T. Whelan.
%  and was modified by E. Thrane (for STAMP) 
%  Contact ethrane@physics.umn.edu and/or john.whelan@ligo.org
%
%  $Id: readTimeSeriesData.m,v 1.9 2006-04-14 21:05:42 whelan Exp $
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
persistent cache;% to keep the cache data in memory (used for new cache method)

% initially set dataOK to true
dataOK = true;

if(~isfield(params,'cacheFile') && ~params.largeShift && params.nargin ==3)% if old cache method
  % use a pre-fetched list of file locations to find and fetch the
  % frame data
  frameFiles = textread(frameCacheFile, '%s\n', -1, ...
                       'commentstyle', 'matlab');

  % parse the frame file names if necessary
  if ((frameDuration <= 0) ...
      || (strncmp(gpsTimesFile,'<auto>',length('<auto>'))))
    [gpsTimes,frameDurs]=decodeFrameNames(frameFiles);
  end;

  % if frame duration explicitly specified, use that instead
  if (frameDuration >= 0)
    frameDurs  = frameDuration*ones(length(frameFiles), 1);
  end;

  % if a GPS times file is specified instead of 'auto', read it
  if (strncmp(gpsTimesFile,'<auto>',length('<auto>')) == false)
    gpsTimes   = textread(gpsTimesFile, '%n', -1, ...
                         'commentstyle', 'matlab');
  end;
else % if new cache method
  if isempty(cache) % if it is not loaded already
    load(params.cacheFile); % mat file containing all cache data
  end

  % for doing large time shift
  if params.largeShift % if largeShift is true
    if strcmp(channelName(1:2), cache.site(3:4)) % if det2, change start time
       dataStartTime = dataStartTime + params.largeShiftTime2;
    end
  end

  useOne = 0; useTwo = 0;

  if isfield(cache,'pem') & strcmp(channelName,cache.channelName1)
    useOne = 1;
  elseif isfield(cache,'pem') & ~strcmp(channelName,cache.channelName1)
    useTwo = 1;
  elseif strcmp(channelName(1:2), cache.site(1:2))
    useOne = 1;
  elseif strcmp(channelName(1:2), cache.site(3:4))
    useTwo = 1;
  end

  % obtain frames files, gpsTimes for the corresponding period
  if ~params.doDetectorNoiseSim
  % When doDetectorNoiseSim is false, these lines define cachefiles
  % variables, that are used below to read data from frames.
  % When doDetecotrNoiseSim is true, we just simulate the noise, and
  % the following variables are not defined:
  % frameFiles, gpsTimes, frameDurs, startidx, endidx
    if useOne % if det1
      startidx = find(cache.gps1 == max(cache.gps1(cache.gps1 <= dataStartTime)));
      endidx = find(cache.gps1== ...
                  max(cache.gps1(cache.gps1 <= dataStartTime+dataDuration)));
      frameFiles = {cache.frames1{startidx:endidx}}';
      gpsTimes = cache.gps1(startidx:endidx);
      frameDurs = cache.dur1(startidx:endidx);
    elseif useTwo % if det2
      startidx = find(cache.gps2 == max(cache.gps2(cache.gps2 <= dataStartTime)));
      endidx = find(cache.gps2 == ...
                  max(cache.gps2(cache.gps2 <= dataStartTime+dataDuration)));
      frameFiles = {cache.frames2{startidx:endidx}}';
      gpsTimes = cache.gps2(startidx:endidx);
      frameDurs = cache.dur2(startidx:endidx);
    else
      error('Cache files are not available for the detector');
    end
    end
  end

magicLength = length('matlab:');
trimmedName = channelName(4:end);
if (strncmp(trimmedName,'matlab:',magicLength))
  varName = trimmedName((magicLength+1):end);
  [adcdata, dataOK] = ...
      readTSDataFromMatfile(varName, dataStartTime, dataDuration, ...
			    gpsTimes, frameFiles, frameDurs);
else
  % get the data
  if params.doDetectorNoiseSim
    % Fill with simulated Gaussian noise.
    sampleRate = params.sampleRate;
    % number of data points
    N = dataDuration * sampleRate;    
    % prepare for FFT
    if ( mod(N,2)== 0 )
      numFreqs = N/2 - 1;
    else
      numFreqs = (N-1)/2;
    end
    deltaF = 1/dataDuration;
    f = deltaF*[1:1:numFreqs]';
    flow = deltaF;
    % Next power of 2 from length of y
    NFFT = 2^nextpow2(N);
    amp_values = mc.transfer(:,2);
    f_transfer1 = mc.transfer(:,1);
    Pf1 = interp1(f_transfer1,amp_values, f, 'spline');
    %
    deltaT = 1/sampleRate;
    norm1 = sqrt(N/(2*deltaT)) * sqrt(Pf1);
    %
    re1 = norm1*sqrt(1/2).* randn(numFreqs, 1);
    im1 = norm1*sqrt(1/2).* randn(numFreqs, 1);
    z1  = re1 + 1i*im1;
    % freq domain solution for htilde1, htilde2 in terms of z1, z2
    htilde1 = z1;
    % convolve data with instrument transfer function
    otilde1 = htilde1*1;
    % set DC and Nyquist = 0, then add negative freq parts in proper order 
    if ( mod(N,2)==0 )
      % note that most negative frequency is -f_Nyquist when N=even
      otilde1 = [ 0; otilde1; 0; flipud(conj(otilde1)) ];
    else
      % no Nyquist frequency when N=odd
      otilde1 = [ 0; otilde1; flipud(conj(otilde1)) ];
    end
    % fourier transform back to time domain
    o1_data = ifft(otilde1);
    % take real part (imag part = 0 to round-off)
    o1_data = real(o1_data);
    vector = o1_data';
    vectorError = 0;
  else
    % Get data from frames.

    chanObject  = chanstruct(channelName);
    if isfield(cache,'pem') 
       dataRead = -4;
       while dataRead < 0
          [vector, sampleRate, vectorError] = chanvector(chanObject, ...
             dataStartTime, dataDuration, gpsTimes, frameFiles, frameDurs);
          dataRead = dataRead + 1;
          if vectorError == 0
             dataRead = 1;
          else
             pause(10)
          end
       end
    else
       [vector, sampleRate, vectorError] = chanvector(chanObject, ...
          dataStartTime, dataDuration, gpsTimes, frameFiles, frameDurs);
    end

    sampleRate2 = cache.channelSampleRates(find(strcmp(cache.channelNames,channelName)));

    if isfield(cache,'pem') & strcmp(channelName,cache.channelName1)
       N = dataDuration * cache.sampleRate1;     
    elseif isfield(cache,'pem') & ~strcmp(channelName,cache.channelName1)
       N = dataDuration * sampleRate2;
    end

    if isfield(cache,'pem') & N > length(vector)
       if strcmp(channelName,cache.channelName1)
          sampleRate = cache.sampleRate1;
       elseif ~strcmp(channelName,cache.channelName1)
          sampleRate = sampleRate2;
       end
       vector = padarray(vector,[0 N-length(vector)],1,'post');
       vectorError = 0;
       fprintf('Data load for %s failed, using zeros...\n',chanObject.name);
    end
  end

  % add time series injection if requested-------------------------------------
  if params.stampinj
    % load injection the first time that readTimeSeries is called
    if params.pass.stamp.firstpass 
      [params.pass.stamp.h1, params.pass.stamp.h2] = ...
        load_injection(params, params.pass.stamp, sampleRate);
      % scale injection by sqrt(alpha)
      params.pass.stamp.h1=sqrt(params.pass.stamp.alpha)*...
        params.pass.stamp.h1;
      params.pass.stamp.h2=sqrt(params.pass.stamp.alpha)*...
        params.pass.stamp.h2;
      % make a vector of times associated with the injection
      params.pass.stamp.t = ...
        (0:length(params.pass.stamp.h1)-1)/sampleRate + ...
        params.pass.stamp.startGPS;
      % compensate for time shift making sure that the shift time corresponds
      % to one a sampling time
      if ~params.doShift1
         params.ShiftTime1 = 0;
      end
      if ~params.doShift2
       	 params.ShiftTime2 = 0;
      end 
      params.ShiftTime1 = round(params.ShiftTime1 * sampleRate)/sampleRate;
      params.ShiftTime2 = round(params.ShiftTime2 * sampleRate)/sampleRate;
      params.pass.stamp.t1 =  params.pass.stamp.t - params.ShiftTime1;
      params.pass.stamp.t2 =  params.pass.stamp.t - params.ShiftTime2;
      % calculate the duration of the injection
      params.pass.stamp.dur = length(params.pass.stamp.h1)/sampleRate;
      % firstpass is over; do not repeat these calculations again
      params.pass.stamp.firstpass=false;
    end
    % Determine if injection overlaps with the frame data
    % In case of large time shift revert back to the original dataStartTime;
    % Since injection is based on the time stamps of the data, any shift 
    % larger than the signal duration in not possible; we just have to trick
    % the code.
    if params.largeShift % if largeShift is true
      if strcmp(channelName(1:2), cache.site(3:4)) % if det2, change start time
       dataStartTime = dataStartTime - params.largeShiftTime2;
      end
    end
    if dataStartTime + dataDuration >= params.pass.stamp.startGPS & ...
      dataStartTime <= params.pass.stamp.startGPS + params.pass.stamp.dur
      % make a vector of times associated with the frame data
      fr_t = (0:length(vector)-1)/sampleRate +  dataStartTime;
      % determine which IFO we are injecting into just now
      if strcmp(frameType, params.frameType1)
        % find the overlapping time indices for injection and frame data
        [tmp, idx_h1, idx_fr] = intersect(params.pass.stamp.t1, fr_t);
        % add the injection
        vector(idx_fr) = vector(idx_fr) + params.pass.stamp.h1(idx_h1)';
      elseif strcmp(frameType, params.frameType2)
        % find the overlapping time indices for injection and frame data
        [tmp, idx_h2, idx_fr] = intersect(params.pass.stamp.t2, fr_t);
        % add the injection
        vector(idx_fr) = vector(idx_fr) + params.pass.stamp.h2(idx_h2)';
      else
        error('frameType mismatch');
      end
    end
  end %------------------------------------------------------------------------

  % check that the data is OK
  if vectorError == 0
    dataOK = true;
  else
    fprintf(['READTIMESERIESDATA: missing %s frame data starting at %d, ' ...
      'ending at %d\n'], channelName(1), dataStartTime, ...
      dataStartTime+dataDuration);
    dataOK = false;
  end

  if dataOK 
    % fill time-series data structures
    adcdata.data   = transpose(vector);
    adcdata.tlow   = dataStartTime;
    adcdata.deltaT = 1/sampleRate;
  else
    % return all zeroes
    adcdata.data   = 0;
    adcdata.tlow   = 0;
    adcdata.deltaT = 0;
  end
end

return

