function stamp_pem_segdb(params)

try
   params.segmentFlag;
catch
   return
end

fprintf('Creating segment database file...\n');

segdb_command = sprintf('ligolw_segment_query --database --query-segments --include-segments %s --gps-start-time %d --gps-end-time %d | /usr/bin/ligolw_print -t segment:table -c start_time -c end_time -d " "',params.segmentFlag,params.startGPS,params.endGPS);

[junk,segments] = system(segdb_command);
segmentsSplit = regexp(segments,'\n','split');
segmentsSplit(strcmp('',segmentsSplit)) = [];
segments = [];
for i = 1:length(segmentsSplit)
   segment = segmentsSplit{i};
   segmentSplit = regexp(segment,' ','split');
   segments = [segments; str2num(segmentSplit{1}) str2num(segmentSplit{2})];
end

if length(segments) == 0
   fprintf('No segments for this time period... continuing.\n');
   return
end

stamp_pem_createpath([params.path '/segments/']);

% Create cache mat structure
segments.segments = segments;

% Save cache mat file
save([params.path '/segments/segments.mat'],'segments');

