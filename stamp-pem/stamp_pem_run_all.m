function stamp_pem_run_all(pemParamsFile,segmentDuration,startGPS,endGPS,fmin,fmax)
% function stamp_pem_run_all(pemParamsFile,segmentDuration,startGPS,endGPS)
% Given a STAMP-PEM parameter file, segment duration, and start and end 
% GPS times, runs the STAMP-PEM pipeline for all available channels. 
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isstr(startGPS)
   startGPS=str2num(startGPS);
end
if isstr(endGPS)
   endGPS=str2num(endGPS);
end

if isstr(segmentDuration)
   segmentDuration=str2num(segmentDuration);
end

if startGPS==0
   return
else
   % Read in STAMP-PEM parameter file
   params = readParamsFromFile(pemParamsFile);

   % Read in channel list
if exist([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo1 ...
                '-' params.frameType1 '-channel_list_' params.runName '.txt']);
   [channelNames,channelSampleRates] = ...
      textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo1 ...
		'-' params.frameType1 '-channel_list_' params.runName '.txt'],'%s %f');
else
  [channelNames,channelSampleRates] = ...
      textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo1 ...
                '-' params.frameType1 '-channel_list_.txt'],' %s %f');
end

   % Number of completed channels
   completedChannels = 0;

   % Number of channels for which completion is desired
   desiredChannels = length(channelNames);

   % Indexes of completed channels
   whichChannels = [];

   % Loop over channels
   for i = 1:length(channelNames)
      % Check that the sampling rate for the channel is large enough and
      % the desiredChannels limit has not been exceeded
      if channelSampleRates(i) >= 128 && completedChannels < desiredChannels
%	if ~(channelNames(i)==params.darmChannel)
%	  try
            % Run STAMP-PEM pipeline
            stamp_pem_run(pemParamsFile,channelNames{i},segmentDuration,startGPS,endGPS,fmin,fmax)
            completedChannels = completedChannels + 1;
            whichChannels = [whichChannels i];
            fprintf('%s completed\n',channelNames{i});
%         catch
%            fprintf('%s failed\n',channelNames{i});
%        end
%	end
      end
   end

   % Crop channel list to only use completed channels
   channelNames = channelNames(whichChannels);
   % Generate STAMP-PEM summary page for this run
   stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS,fmin,fmax);
   % Run STAMP-PEM ft-map comparison structure
%   stamp_pem_findCompareMats(pemParamsFile, startGPS, endGPS, segmentDuration, channelNames)
clear all;
end

