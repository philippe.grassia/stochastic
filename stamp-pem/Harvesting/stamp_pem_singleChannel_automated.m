function stamp_pem_singleChannel_automated(startGPS, endGPS, segmentDuration)
%Written to take in startGPS and endGPS every hour and run the pem code to
%analyze this pemChannel. h(t) or other pem channel for CC is
%defined by choice of pemParamsFile.
%Written by Pat Meyers
%----------------------------
pemParamsFile = '/home/meyers/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_H2OneArm.txt';
pemChannel = 'H2:PEM-EY_ACC_EBAY_FLOOR_Z_DQ';

%bash file that calls this inputs start and end GPS and segmentDuration in string form.
if isstr(startGPS)
startGPS=str2num(startGPS);
end

if isstr(endGPS)
endGPS=str2num(endGPS);
end

if isstr(segmentDuration);
segmentDuration=str2num(segmentDuration);
end
%call clustermap 
stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS);
%compare to ranked events today
stamp_pem_harvesting(pemParamsFile, startGPS, endGPS, segmentDuration, pemChannel, 'today');
%compare to ranked events of all time
stamp_pem_harvesting(pemParamsFile, startGPS, endGPS, segmentDuration, ...
		     pemChannel, 'alltime');
