#!/bin/bash
#written to automate analysis of single PEM channel each hour
source ${HOME}/MatlabSetup_R2007a_glnxa64.sh
source ${HOME}/.bashrc
source ${HOME}/.bash_profile


startGPS=`tconvert now - 2 hours`
endGPS=`tconvert now - 1 hour`


cd /home/meyers/matapps/packages/stochastic/trunk/stamp-pem

matlab -nodisplay<<EOF
stamp_startup;
stamp_pem_automated('${startGPS}', '${endGPS}');
EOF