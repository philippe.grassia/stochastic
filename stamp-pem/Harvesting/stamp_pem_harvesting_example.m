function stamp_pem_harvesting_example()
%Example of how to run harvesting script along with stamp_pem_run
%This sets a pemChanel, segment duration, h(t) and webpage path are 
%defined in choice of params file,these are located in /stamp-pem/input.
%Runs stamp_pem_run which calls clustermap.m
%
pemParamsFile = '/home/meyers/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_H2OneArm.txt';
pemChannel = 'H2:PEM-EY_ACC_EBAY_FLOOR_Z_DQ';
segmentDuration = 100;
startGPS=1026547217;
endGPS=1026550817;

if isstr(startGPS)
startGPS=str2num(startGPS);
end

if isstr(endGPS)
endGPS=str2num(endGPS);
end

stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS);
%'today' specifies that you'll ranke the files just produced against today's
%loudest events
stamp_pem_harvesting(pemParamsFile, startGPS, endGPS, segmentDuration, pemChannel, ...
		     'today');
%'alltime' specifies that you're comparing with the loudest events produced
stamp_pem_harvesting(pemParamsFile, startGPS, endGPS, segmentDuration, ...
		     pemChannel, 'alltime');
