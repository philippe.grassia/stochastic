function stamp_pem_reranking(params, ranked_files, index, ranklistname)

% This function reranks all of the previously ranked files because we have a new ranked file. 
%
% params.webpage_path ----------file path to where webpage files are stored 
% params.ranked_files_path -----file path to where .mat files of ranked files are stored
% ranked_files ----------list of the ranked .mat files
% index -----------------the rank of our new file, taken from for loop
%       -----------------in which we were checking challeneger against previous
%       -----------------ranked events
% ranklistname ----------tells whether we're calling this for today's or all
% -----------------------time's events
%
% Written by Pat Meyers
%------------------------------------------------------------



% If we already have 10 ranked files...
if length(ranked_files)==10
 delete([params.ranked_files_path, '/' ranklistname '_ranks/*rank10.mat'])
  for jj=10:-1:index+1

%start at the bottom. Delete tenth ranked, copy 9th ranked mat file down, etc.

   
    higher_rank = dir([params.ranked_files_path '/' ranklistname '_ranks/*rank', mat2str(jj-1), '.mat']);
    newname_root= higher_rank.name(1:length(higher_rank.name)-5);
    copyfile([params.ranked_files_path, '/' ranklistname '_ranks/', higher_rank.name],...
	     [params.ranked_files_path, '/' ranklistname '_ranks/', newname_root, mat2str(jj), '.mat']);
    delete([params.ranked_files_path, '/' ranklistname '_ranks/*rank', mat2str(jj-1), '.mat']);
%move text files and plots down the line
    copyfile([params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(jj-1),'/' ranklistname, mat2str(jj-1),'.txt'],...
             [params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(jj),'/' ranklistname, mat2str(jj),'.txt']);
    copyfile([params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(jj-1),'/' ranklistname '_snr_', mat2str(jj-1), '.png'],...
    	     [params.webpage_path '/SP_' ranklistname '_events/' ranklistname, ...
	      mat2str(jj),'/' ranklistname '_snr_',mat2str(jj),'.png']);
    copyfile([params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(jj-1),'/' ranklistname '_cluster_', mat2str(jj-1),'.png'],...
 	     [params.webpage_path '/SP_' ranklistname '_events/' ranklistname, ...
	      mat2str(jj),'/' ranklistname '_cluster_',mat2str(jj),'.png']);


  end %jj loop
end %length 10 option

%If we have fewer than ten files, but an event that displaces some of the
%already ranked ones, then...
if length(ranked_files)<10 & length(ranked_files)>0

%move bottom one to one lower rung

   for ii=length(ranked_files)+1:-1:index+1
     higher_rank = dir([params.ranked_files_path, '/' ranklistname '_ranks/*rank', mat2str(ii-1), '.mat']);
     newname_root= higher_rank.name(1:length(higher_rank.name)-5);
     copyfile([params.ranked_files_path, '/' ranklistname '_ranks/', higher_rank.name],...
                [params.ranked_files_path, '/' ranklistname '_ranks/', newname_root, mat2str(ii), '.mat']);
     delete([params.ranked_files_path, '/' ranklistname '_ranks/' higher_rank.name]);
     
%move text files and plots down the line

copyfile([params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(ii-1),'/' ranklistname , mat2str(ii-1),'.txt'],...
	 [params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(ii),'/' ranklistname ,mat2str(ii),'.txt']);
copyfile([params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(ii-1),'/' ranklistname '_snr_', mat2str(ii-1), '.png'],...
	 [params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(ii),'/' ranklistname '_snr_',mat2str(ii),'.png']);
copyfile([params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(ii-1),'/' ranklistname '_cluster_', mat2str(ii-1),'.png'],...
	 [params.webpage_path '/SP_' ranklistname '_events/' ranklistname, mat2str(ii),'/' ranklistname '_cluster_',mat2str(ii),'.png']);


   end %ii loop
end %length <10 option

