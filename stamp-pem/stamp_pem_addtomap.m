function [map,tt] = stamp_pem_addtomap(params,tt1,f1,map1,tt2,f2,map2)

if f1 ~= f2
   error('Attempting to combined maps with different frequency arrays\n')
end

if (length(tt1) == 0) && (length(tt2) == 0)
   map = []; tt = [];
   return
elseif (length(tt1) == 0)
   map = map2; tt = tt2;
   return
elseif (length(tt2) == 0)
   map = map1; tt = tt1;
   return
end

ttMin = min(min(tt1),min(tt2));
ttMax = max(max(tt1),max(tt2));
ttDur = tt1(2) - tt1(1);

tt = ttMin:ttDur:ttMax;
indexes = find(tt >= params.startGPSMap & tt <= params.endGPSMap);
tt = tt(indexes);

map = NaN*ones(length(f1), length(tt));
% find relevant time indices from new and old ft-maps
[C, new_t, old_t] = intersect(tt, tt1);
map(:,new_t) = map1(:,old_t);

% find relevant time indices from new and old ft-maps
[C, new_t, old_t] = intersect(tt, tt2);
map(:,new_t) = map2(:,old_t);


