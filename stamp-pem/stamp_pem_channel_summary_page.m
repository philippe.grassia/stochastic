function stamp_pem_channel_summary_page(pemParamsFile,matappsPath,segmentDuration,channelName)
% function stamp_pem_channel_summary_page(pemParamsFile,segmentDuration,channelName)
% Given a STAMP-PEM parameter file, segment duration, and channel name, creates a
% html table linking to all available STAMP-PEM runs for that channel and segment
% duration. The table is sortable as well as color-coded to indicate significance
% of a particular run.  
% Routine written by Michael Coughlin.
% Modified: July 31, 2012 
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Turn warnings off
warning off all;

if isstr(segmentDuration)
   segmentDuration = str2num(segmentDuration)
end

% Read in STAMP-PEM parameter file
params = readParamsFromFile(pemParamsFile);
params.segmentDuration = segmentDuration;
%read in stamp pem personal information
personal = readParamsFromFile([ matappsPath '/stamp-pem/input/stamp_pem_personal.txt');
params.dirPath           = personal.dirPath;
params.publicPath        = personal.publicPath;
params.matappsPath       = personal.matappsPath;
params.executableDir     = personal.executableDir;


% Folders of this run type
folders = dir([params.dirPath '/' params.ifo1 '/' params.runName '-*']);

% Path to summary folder
params.path = [params.dirPath '/' params.ifo1 '/' params.runName '-Summary'];
stamp_pem_createpath(params.path);

% Open HTML page for writing
fid=fopen([params.path '/' channelNameUnderscore '.html'],'w+');

% HTML Header
fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
fprintf(fid,'\n');
fprintf(fid,'<html>\n');
fprintf(fid,'\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<title>PEM Summary Page for %s</title>\n',channelName);
fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../../style/main.css">\n');
fprintf(fid,'<script src="../../../style/sorttable.js"></script>\n');
fprintf(fid,'</head>\n');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">PEM Summary Page for %s</span></big></big></big></big><br>\n',channelName);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

% HTML table header
fprintf(fid,'<table class="sortable" id="sort" style="text-align: center; width: 1260px; height: 67px; margin-left:auto; margin-right: auto; background-color: white;" border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<thead>\n');
fprintf(fid,'<tr><th>GPS Times</th><th>loudest SNR</th><th>loudest GPS</th><th>GPS SNR</th><th>loudest frequency</th><th>frequency SNR</th></tr>\n');
fprintf(fid,'</thead>\n');

% Generate arrays of ft-map SNRs for color-coding purposes
output = stamp_pem_compare_channel(params,channelName);

% Loop over available runs of this run type
for i=1:length(folders)

   % Check if there is data available for this channel
   if exist([params.dirPath '/' params.ifo1 '/' folders(i).name '/' num2str(params.segmentDuration) '/' channelNameUnderscore '/pem_' channelNameUnderscore '.html']); 

      % Read in mat file associated with this channel
      try
         mat_data = load([params.dirPath '/' params.ifo1 '/' folders(i).name '/' num2str(params.segmentDuration) '/' channelNameUnderscore '/' channelNameUnderscore '_cluster.mat']);
      catch
         mat_data.cluster_out.max_SNR = NaN;
      end

      % Return maximum gps and frequency SNRs
      try
         [gpsSNR,gpsSNRIndex] = max(mat_data.cluster_out.line_glitch.t_snr);
         loudestGPS = mat_data.cluster_out.line_glitch.t(gpsSNRIndex(1));
         [frequencySNR,frequencySNRIndex] = max(mat_data.cluster_out.line_glitch.f_snr);
         loudestFrequency = mat_data.cluster_out.line_glitch.f(frequencySNRIndex(1));
      catch
         loudestGPS = NaN; gpsSNR = NaN; loudestFrequency = NaN; frequencySNR = NaN;
      end

      % Generate html color code based on this run's SNR
      burstegardColor = stamp_pem_bgcolor(params,mat_data.cluster_out.max_SNR(1),output.burstegardSNR);
      gpsColor = stamp_pem_bgcolor(params,loudestGPS,output.gpsSNR); 
      frequencyColor = stamp_pem_bgcolor(params,loudestFrequency,output.frequencySNR);

      % Create HTML row containing channel and ft-map SNR information
      fprintf(fid,'<tr>');
      fprintf(fid,'<td><a href="../../%s/%d/%s/pem_%s.html">%s</a></td>',...
         folders(i).name,params.segmentDuration,channelNameUnderscore,...
         channelNameUnderscore,folders(i).name);
      fprintf(fid,'<td style="background-color: %s">%.2f</td>',burstegardColor,mat_data.cluster_out.max_SNR(1));
      fprintf(fid,'<td>%.2f</td>',loudestGPS);
      fprintf(fid,'<td style="background-color: %s">%.2f</td>',gpsColor,gpsSNR);
      fprintf(fid,'<td>%.2f</td>',loudestFrequency);
      fprintf(fid,'<td style="background-color: %s">%.2f</td>',frequencyColor,frequencySNR);
   end
   fprintf(fid,'</tr>\n');
end

% Close HTML Table
fprintf(fid,'</table>\n');

fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');

% Show colorbar indicating color significance
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 200px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Colorbar </span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./colorbar.png"><img alt="" src="./colorbar.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');

% Show user and time information
[junk,user] = system('echo $USER');
[junk,time] = system('date');

fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');
fprintf(fid,'Created by user %s on %s \n',user,time);

% Close HTML
fprintf(fid,'</html>\n');
fclose(fid);

% Public HTML output path
params.outputPath = ...
   [params.publicPath '/' params.ifo1 '/' params.runName '-Summary'];
stamp_pem_createpath(params.outputPath);

% Softlink HTML to output path
fid=fopen([params.path '/softlink.sh'],'w+');
fprintf(fid,'#! /usr/bin/env bash\n');
fprintf(fid,'ln -s %s/ %s\n',params.path,params.outputPath);
fclose(fid);
system(['source ' params.path '/softlink.sh']);

