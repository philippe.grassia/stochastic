function bgcolor = stamp_pem_bgcolor(params,snr,snrArray)
% bgcolor = stamp_pem_bgcolor(params,snr,snrArray)
% Given set of SNR values in snrArray and a reference SNR, returns html code 
% specifying a color indicating the reference SNR's significance
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Number of colors in array
N = 256;

% Create colormap array
color = 256 * colormap(jet(N));

try
   % Determine significance of snr (between 0 and 1)
   snrSig = length(find(snr >= snrArray)) / length(snrArray);
   % Determine color index of this significance
   index = max(1,floor(N * snrSig));
   % Return colors of this index
   thisColor = floor(color(index,:));
   % Return rgb string containing these colors
   bgcolor = sprintf('rgb(%d, %d, %d)',thisColor(1),thisColor(2),thisColor(3));
catch
   bgcolor = 'rgb(0, 0, 0)';
end

% Create colorbar to indicate color significance
if ~exist([params.path '/colorbar.png'])
   figure;
   set(0,'DefaultAxesFontSize',20);
   set(0,'DefaultTextFontSize',20);
   set(gcf, 'PaperSize',[5 5])
   hcb = colorbar('Location','South','XTickLabel',...
   {'Freezing','Cold','Cool','Neutral',...
   'Warm','Hot','Burning','Nuclear'});
   set(hcb,'XTickMode','manual')
   axis off
   print('-dpng',[params.path '/colorbar']);
   close;
end

