function stamp_pem_frame_check(frameFile,channelList)
% function stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS)
% Given a STAMP-PEM parameter file, PEM channel, segment duration, and
% start and end GPS times, runs the STAMP-PEM pipeline. This pipeline 
% generates ft-maps using STAMP, running various search algorithms to 
% search for structure. It creates an HTML page linking to the ft-maps and 
% search algorithm output.
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

channelList = '/home/mcoughlin/matapps/packages/stochastic/trunk/stamp-pem/frame_check/stamp_pem-L1-R-channel_list.txt';

[channels,samplefs] = textread(channelList,'%s %d');

channelListMod = '/home/mcoughlin/matapps/packages/stochastic/trunk/stamp-pem/frame_check/stamp_pem-L1-R-channel_list-MOD.txt';

fid = fopen(channelListMod,'w+');

for i = 1:length(channels)

   channel = channels{i}; samplef = samplefs(i);

   if isempty(strfind(channels{i},'GS13'))
      continue
   end

   if isempty(strfind(channels{i},'BLND'))
      continue
   end

   fprintf(fid,'%s %d\n',channel,samplef);
end
   
fclose(fid);
