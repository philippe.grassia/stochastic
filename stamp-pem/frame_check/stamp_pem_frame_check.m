function stamp_pem_frame_check(frameFile,channelList,type)
% function stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS)
% Given a STAMP-PEM parameter file, PEM channel, segment duration, and
% start and end GPS times, runs the STAMP-PEM pipeline. This pipeline 
% generates ft-maps using STAMP, running various search algorithms to 
% search for structure. It creates an HTML page linking to the ft-maps and 
% search algorithm output.
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

frchannels_command = sprintf('FrChannels %s',frameFile);

[junk,channels] = system(frchannels_command);

channels_split = regexp(channels,'\n','split');

samplef_min = 256;

try
   type;
catch
   type='all';
end

fid = fopen(channelList,'w+');

for i = 1:length(channels_split)
   channel_split = regexp(channels_split{i},' ','split');

   if length(channel_split) == 1
      continue
   end

   channel = channel_split{1}; samplef = str2num(channel_split{2});

   if samplef < samplef_min
      continue
   end

   data = frgetvect(frameFile,channel);
 
   if length(data) <= 1
      continue
   end

   if ~strcmp(type,'all')
      if isempty(strfind(channel,type))
         continue
      end
   end

   fprintf(fid,'%s %d\n',channel,samplef);
end
   
fclose(fid);
