
run_name='H1RDS';
pemParamsFile = stamp_pem_createParamsFile(run_name);

pemParamsFile

pemChannel = 'H1:PEM-MX_SEIS_VEA_FLOOR_Z_DQ';
%pemChannel = 'H1:PEM-MX_SEIS_VEA_FLOOR_QUAD_SUM_DQ';
segmentDuration = 10;
%startGPS = 1042936004;
startGPS = 1050000128;
endGPS =   1050000256;

fmin= '0.1';
fmax = '128';
%stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS,fmin, ...
%	      fmax);
%stamp_pem_summary_page(pemParamsFile,segmentDuration,startGPS,endGPS,fmin, ...
%		       fmax);

stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS,fmin,fmax)
