function stamp_pem_cache(params)

fprintf('Creating cache mat file...\n');

ligo_data_find_command = sprintf('ligo_data_find --server=tacc.ligo.org:443 -o %s -t %s -s %d -e %d -u file',...
                                 params.ifo1(1),params.frameType1,params.startGPS,params.endGPS);
[junk,frames1] = system(ligo_data_find_command);

% Only return non-empty frame lines
frames1 = regexp(frames1,'\n','split');
frames1 = frames1(find( ~strcmpi(frames1,'')));

if strcmp(frames1,'No files found!')
  error('ligo data find found no matching frames')
end
% Return arrays of frame gps and duration
stamp_pem_createpath([params.path '/cache/']);
gps1 = zeros(size(frames1));
dur1 = zeros(size(frames1));

for i = 1:length(frames1);
   frame1{i} = strrep(frames1{i},'file://localhost','');
   frame_split = regexp(strrep(frame1{i},'.gwf',''),'-','split');
   gps1(i) = str2num(frame_split{end-1});
   dur1(i) = str2num(frame_split{end});
end

ligo_data_find_command = sprintf('ligo_data_find --server=tacc.ligo.org:443 -o %s -t %s -s %d -e %d -u file',...
   params.ifo2(1),params.frameType2,params.startGPS,params.endGPS);
[junk,frames2] = system(ligo_data_find_command);

% Only return non-empty frame lines
frames2 = regexp(frames2,'\n','split');
frames2 = frames2(find( ~strcmpi(frames2,'')));

% Return arrays of frame gps and duration
stamp_pem_createpath([params.path '/cache/']);
gps2 = zeros(size(frames2));
dur2 = zeros(size(frames2));

for i = 1:length(frames2);
   frame2{i} = strrep(frames2{i},'file://localhost','');
   frame_split = regexp(strrep(frame2{i},'.gwf',''),'-','split');
   gps2(i) = str2num(frame_split{end-1});
   dur2(i) = str2num(frame_split{end});
end

% Create cache mat structure
cache.frames1 = frame1; cache.frames2 = frame2;
cache.gps1 = gps1; cache.gps2 = gps2;
cache.site = strcat(params.ifo1,params.ifo2);
cache.NJobs = 1;
cache.path1 = [params.path '/cache/']; cache.path2 = [params.path '/cache/'];
cache.dur1 = dur1; cache.dur2 = dur2;
cache.pem = 1; cache.channelName1 = params.darmChannel;
cache.channelNames=params.channelNames;
cache.channelSampleRates=params.channelSampleRates;
cache.sampleRate1 = params.darmChannelSampleRate; cache.sampleRate2 = params.pemChannelSampleRate;
cache.channelNames = params.channelNames; cache.channelSampleRates = params.channelSampleRates;

% Save cache mat file
save([params.path '/cache/cache.mat'],'cache');

