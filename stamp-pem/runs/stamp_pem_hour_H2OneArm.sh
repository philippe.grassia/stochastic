#!/bin/bash

source ${HOME}/.bashrc
source ${HOME}/.bash_profile

runName=H2OneArm

cat ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_${runName}.txt > ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_temp.txt

cat ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_personal.txt >> ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_temp.txt

segmentDuration=100
gpsStart=`tconvert now - 2 hours`
gpsEnd=`tconvert now - 1 hour`

${HOME}matapps/packages/stochastic/trunk/stamp-pem/stamp_pem.sh $paramsFile $segmentDuration $gpsStart $gpsEnd  > /home/mcoughlin/STAMP/STAMP-PEM/logs/${runName}-${gpsStart}-${gpsEnd}-${segmentDuration}.txt

segmentDuration=10
${HOME}/matapps/packages/stochastic/trunk/stamp-pem/stamp_pem.sh $paramsFile $segmentDuration $gpsStart $gpsEnd  > /home/mcoughlin/STAMP/STAMP-PEM/logs/${runName}-${gpsStart}-${gpsEnd}-${segmentDuration}.txt

segmentDuration=1
${HOME}/matapps/packages/stochastic/trunk/stamp-pem/stamp_pem.sh $paramsFile $segmentDuration $gpsStart $gpsEnd  > /home/mcoughlin/STAMP/STAMP-PEM/logs/${runName}-${gpsStart}-${gpsEnd}-${segmentDuration}.txt

