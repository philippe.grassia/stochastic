%run_name corresponds to the params file we'll choose. see the
%/stamp-pem/input directory for available params file.
run_name='H2OneArm'; 
pemParamsFile = stamp_pem_createParamsFile(run_name);
segmentDuration = 100;
startGPS = 1025636416;
endGPS = 1026590416;

GPS = startGPS:3600:endGPS;

for i = 1:length(GPS)-1
   try
      stamp_pem_make_dag(pemParamsFile,segmentDuration,GPS(i),GPS(i+1))
      system('sleep 5m')
   catch
      
   end
end


