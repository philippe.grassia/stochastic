#!/bin/bash

source ${HOME}/.bashrc
source ${HOME}/.bash_profile

runName=H2OneArm

cat ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_${runName}.txt > ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_temp.txt

cat ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_personal.txt >> ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/input/stamp_pem_params_temp.txt

segmentDuration=1
gpsStart=`tconvert now - 2 hours`
gpsEnd=$gpsStart+1800

${HOME}/matapps/packages/stochastic/trunk/stamp-pem/stamp_pem_condor.sh $paramsFile $segmentDuration $gpsStart $gpsEnd  > /home/meyers/STAMP/STAMP-PEM/logs/${runName}-${gpsStart}-${gpsEnd}-${segmentDuration}.txt

