#!/bin/bash

source ${HOME}/.bashrc
source ${HOME}/.bash_profile

gpsStart=`tconvert now - 2 hours`
gpsEnd=`tconvert now - 1 hour`

cd ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/

/ldcg/matlab_r2012a/bin/matlab <<EOF
 stamp_startup;
 stamp_pem_run_L1('${gpsStart}','${gpsEnd}')
EOF


