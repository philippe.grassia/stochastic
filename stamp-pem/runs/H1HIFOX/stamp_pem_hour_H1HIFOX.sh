#!/bin/bash

source ${HOME}/.bashrc
source ${HOME}/.bash_profile

gpsStart=`tconvert now - 2 hours`
gpsEnd=`tconvert now - 1 hour`
cd ${HOME}/matapps/packages/stochastic/trunk/stamp-pem/
touch "./runs/H1HIFOX/most_recent_times.txt";
echo "${gpsStart} ${gpsEnd}" > "./runs/H1HIFOX/most_recent_times.txt";
/ldcg/matlab_r2012a/bin/matlab -nodisplay -nojvm <<EOF
 stamp_startup; 
 H1HIFOX_make_dag('${gpsStart}','${gpsEnd}')
EOF


