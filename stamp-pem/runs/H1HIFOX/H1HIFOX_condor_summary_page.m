function H1HIFOX_condor_summary_page
% runs stamp_pem_mod_dag which is used to run the summary pages from the dag file that was already made and submitted previously.
[startGPS endGPS] = textread('most_recent_times.txt', '%d %d');

run_name        = 'H1HIFOX';
pemParamsFile   = stamp_pem_createParamsFile(run_name);
matappsPath     = '/home/meyers/matapps/packages/stochastic/trunk';
fmin            = '0.1';
fmax            = '127';
segmentDuration = 10;

stamp_pem_mod_dag(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin, fmax);


