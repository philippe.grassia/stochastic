function stamp_pem_run_example_H1HIFOX
% runs an example of H1HIFOX test. To be run from matlab command line. requires GPS time inputs. 
pemChannel = 'H1:ISI-HAM3_BLND_GS13RX_IN1_DQ';
startGPS2 = 1075564818;
endGPS2 = 1075568418;
startGPS = 1075575618;
endGPS = 1075579218;
if isstr(startGPS)
startGPS = str2num(startGPS);
end
if isstr(endGPS)
endGPS = str2num(endGPS);
end
run_name='H1HIFOX';
pemParamsFile = stamp_pem_createParamsFile(run_name);
fmin= '0.1';
fmax = '127';
segmentDuration = 10;
matappsPath = '/home/meyers/matapps/packages/stochastic/trunk';
keyboard
stamp_pem_run(pemParamsFile,matappsPath,pemChannel,segmentDuration,startGPS,endGPS,fmin, fmax);
%stamp_pem_mod_dag(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin,fmax);
%stamp_pem_summary_page(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin,fmax);

%stamp_pem_summary_compare(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin,fmax,startGPS2,endGPS2)
