function H1HIFOX_make_dag(startGPS,endGPS)
% makes dag for the H1HIFOX test frequency info is defined here; everything else is done in paramfiles, GPS times are passed from the shell script that calls this.

if isstr(startGPS)
startGPS = str2num(startGPS);
end
if isstr(endGPS)
endGPS = str2num(endGPS);
end
run_name='H1HIFOX';
pemParamsFile = stamp_pem_createParamsFile(run_name);
matappsPath = '/home/meyers/matapps/packages/stochastic/trunk';
fprintf('params file is: %s\n',pemParamsFile);
fmin= '0.1';
segmentDuration = 10;

fmax = '127';


%stamp_pem_make_dag(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin, fmax);
stamp_pem_make_dag_ER5(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin, fmax);

