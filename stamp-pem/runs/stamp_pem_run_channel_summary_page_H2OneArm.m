run_name='H2OneArm';
pemParamsFile = stamp_pem_createParamsFile(H2OneArm);
params = readParamsFromFile(pemParamsFile);
[channelNames,channelSampleRates] = ...
   textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo1 '-' params.frameType1 '-channel_list.txt'],'%s %f');
segmentDuration = 100;

for i = 1:length(channelNames)
   try
      stamp_pem_channel_summary_page(pemParamsFile,segmentDuration,channelNames{i})
   catch
   end
end


