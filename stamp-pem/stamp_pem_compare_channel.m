function output = stamp_pem_compare_channel(params,channelName)
% function output = stamp_pem_compare_channel(params,channelName)
% Given a STAMP-PEM parameters and channel name, returns a structure
% containing arrays of burstegard, frequency, and GPS SNRs available
% for this channel.
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

channelNameUnderscore = strrep(channelName,':','_');
darmChannelUnderscore = strrep(params.darmChannel, ':', '_');
% Folders of this run type
folders = dir([params.dirPath '/' params.ifo1 '/' params.runName '-*']);

% Arrays of search algorithm results
output.burstegardSNR = [];
output.gpsSNR = [];
output.frequencySNR = [];
output.coherence = [];
% Populate arrays with folder results
for i = 1:length(folders)

   % Check if there is data available for this channel   
   if exist([params.dirPath '/' params.ifo1 '/' folders(i).name '/' ...
	    darmChannelUnderscore '/' num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax) '/' channelNameUnderscore '/' channelNameUnderscore '_cluster.mat'])

      % Read in mat file associated with this channel      
      mat_data = load([params.dirPath '/' params.ifo1 '/' folders(i).name '/' ...
		       darmChannelUnderscore '/' num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax) '/' channelNameUnderscore '/' channelNameUnderscore '_cluster.mat']);

      % Populate arrays with SNR information
      try
         output.burstegardSNR = [output.burstegardSNR mat_data.cluster_out.max_SNR];
         output.gpsSNR = [output.gpsSNR max(mat_data.cluster_out.line_glitch.t_snr)];
         output.frequencySNR = [output.frequencySNR max(mat_data.cluster_out.line_glitch.f_snr)];
      catch
      end 
   end
end
% Remove NaN results from arrays
if ~isempty(output.burstegardSNR)
   output.burstegardSNR = output.burstegardSNR(~isnan(output.burstegardSNR));
end
if ~isempty(output.gpsSNR)
   output.gpsSNR = output.gpsSNR(~isnan(output.gpsSNR));
end
if ~isempty(output.frequencySNR)
   output.frequencySNR = output.frequencySNR(~isnan(output.frequencySNR));
end

