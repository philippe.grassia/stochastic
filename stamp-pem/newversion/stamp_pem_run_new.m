function stamp_pem_run(pemParamsFile)
% function stamp_pem_run(pemParamsFile,pemChannel,segmentDuration,startGPS,endGPS)
% Given a STAMP-PEM parameter file, PEM channel, segment duration, and
% start and end GPS times, runs the STAMP-PEM pipeline. This pipeline 
% generates ft-maps using STAMP, running various search algorithms to 
% search for structure. It creates an HTML page linking to the ft-maps and 
% search algorithm output.
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Turn warnings off
warning off all;

%set threshhold for interesting coherence information
%[gpsStart,gpsEnd]=stamp_pem_seg_check(startGPS, endGPS);

% For Condor purposes
if strcmp(pemParamsFile,'0')
   return
end

params = readParamsFromFile(pemParamsFile);

% Generates a summary page for an individual run linking to many channels
if strcmp(params.pemChannel,'summary')
   stamp_pem_summary_page(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin,fmax);
   % Compares results of channels in list at these GPS times
   % to ranked files for today and all time
elseif strcmp(params.pemChannel, 'findCompareMats')
   params = readParamsFromFile(pemParamsFile);
   params.coherenceThreshold=0.5;

   stamp_pem_findCompareMats(pemParamsFile, params.startGPS, params.endGPS, params.segmentDuration, ...
				      params.channelList, params.fmin, params.fmax)
else
%  for kk=1:length(gpsEnd)
%    startGPS=gpsStart(kk);
%    endGPS=gpsEnd(kk);
    % Read in STAMP-PEM parameter file 

    params = readParamsFromFile(pemParamsFile);

    params.startGPSMap = params.startGPS - 3600; params.endGPSMap = params.endGPS;
    params.darmChannelUnderscore = strrep(params.darmChannel,':','_');
    params.pemChannelUnderscore = strrep(params.pemChannel,':','_');
    
    darmChannelSplit = regexp(params.darmChannel,':','split');
    params.darmChannelIfo = darmChannelSplit{1}; params.darmChannelName = darmChannelSplit{2};
    
    pemChannelSplit = regexp(params.pemChannel,':','split');
    params.pemChannelIfo = pemChannelSplit{1}; params.pemChannelName = pemChannelSplit{2};

    % Read in channel list to determine channel sampling rates
    [channelNames,channelSampleRates] = textread(params.channelList,'%s %f');
    params.darmChannelSampleRate = channelSampleRates(find(strcmp(channelNames,params.darmChannel)));
    params.pemChannelSampleRate = channelSampleRates(find(strcmp(channelNames,params.pemChannel)));

    % Output path for run
    params.path = ...
	[params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(params.startGPS) ...
	 '-' num2str(params.endGPS) '/' params.darmChannelUnderscore '/' num2str(params.segmentDuration) '/' ...
	 num2str(params.fmin) '-' num2str(params.fmax)];
   
    params.channelNames = channelNames;
    params.channelSampleRates = channelSampleRates;
 
    fprintf('Running STAMP-PEM: %s\n',params.path);
    
    stamp_pem_createpath([params.path '/' params.pemChannelUnderscore]);
    % Parameter file read by preproc/clustermap
    params.paramsFile = ...
	[params.path '/' params.pemChannelUnderscore '/' params.pemChannelUnderscore ...
	 '.txt'];
    
    % Jobfile read by preproc/clustermap
    params.jobsFile = [params.path '/jobsFile.txt'];
 
    % Only generate jobfile and cache files if not already in existence
    if ~exist(params.jobsFile)
      
      stamp_pem_cache(params);  
      stamp_pem_segdb(params);

      % Create job file
      jobsFile = fopen(params.jobsFile,'w+');
	fprintf(jobsFile,'1 %d %d %d\n',params.startGPS, ...
		params.endGPS,params.endGPS-params.startGPS);
      fclose(jobsFile);
    end
  % Read in default clustermap parameters
  params = stampDefaults(params);
  
  % Output plot directory
  params.plotdir = [params.path '/' params.pemChannelUnderscore];
  params.outputfilename = [params.path '/' params.pemChannelUnderscore '/' params.pemChannelUnderscore];
  params.longsegs = true;
  
  % Ft-map parameters
   params.ra=6;   %right ascenion in hours (for search direction)
   params.dec=30; %declination in degrees (for search direction)
   % override default parameters--------------------------------------------------
   params.debug=true;
   params.saveMat=true;
   params.savePlots=true;
   params.doFreqMask=false;
   params.Autopower=true;
   params.yMapScale = 5e-42;
   minChannelSampleRate = min(params.darmChannelSampleRate,params.pemChannelSampleRate);
   fmax_max = (minChannelSampleRate/2)-1;
   if params.fmax > fmax_max
      params.fmax = fmax_max;
   end

   % Frequency Masking
   params.doStampFreqMask = true;
  FreqsToRemove = [];
  end_mask=floor(params.fmax/60);
  start_mask=ceil(params.fmin/60);
  for i = start_mask:end_mask;
    freqs_struct_counter=i-(start_mask-1);
    %FreqsToRemove(freqs_struct_counter)=60*i;
  end
  params.StampFreqsToRemove = FreqsToRemove;
  params.StampnBinsToRemove =10*ones(size(params.StampFreqsToRemove));
  params.Autopower=true;
 
  % search
  params = stamp_pem_burstegardDefaults(params);
%   params=boxDefaults(params);
%   params.box.freq_width=5;
%   params.box.time_width=200;

   % fixed sensitivity
   params.fixAntennaFactors = true;
   fprintf('params.fixAntennaFactors = %i\n', params.fixAntennaFactors);
 %  params.doBoxSearch=true;
   params.matavailable = 0;
   params.storemats = false;
   params.nogap = true;
   params.stamp_pem = true;

   params.pemPSD = false;
   params.writepemPSD = false;   
   params.pemPSDpath = ...
      [params.dirPath '/' params.ifo1 '/' params.runName '-PSD/' num2str(params.segmentDuration)];
   stamp_pem_createpath(params.pemPSDpath);

   if params.writepemPSD
      stamp_pem_pemPSD(params,params.darmChannel);
      stamp_pem_pemPSD(params,params.pemChannel);
   end

   if params.pemPSD
      pemP1 = load([params.pemPSDpath '/' params.darmChannelUnderscore]);
      params.pemP1 = pemP1.pemPSD;
      pemP2 = load([params.pemPSDpath '/' params.pemChannelUnderscore]);
      params.pemP2 = pemP2.pemPSD;

   end

   params.doBicoherence = false;
   params.doFrequencyBins = false;
   params.coherenceThreshold = 0.1;

   params.startGPSMap = params.startGPS; params.endGPSMap = params.endGPS;
   params.burstegard.weightedSNR = true;

   %%Define Params File sent to clustermap and preproc
   stamp_pem_params(params);
   % Run preproc/clustermap
   cluster_out=clustermap(params, params.startGPS, params.endGPS)
   % Save clustermap mat file
   save([params.path '/' params.pemChannelUnderscore '/' params.pemChannelUnderscore '_cluster.mat'],'cluster_out');

   % Generate HTML page for this run
   stamp_pem_page(params,cluster_out);

   close all;
%  end
end

clear all;
