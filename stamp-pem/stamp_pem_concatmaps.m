function map = stamp_pem_concatmaps(params, map)
% script to concatenate our new map to the old map
% inputs: params ----> parameters for STAMP analysis
%         map -------> old map onto which we will be concatenating the new
%                      map
% outputs: map ------> new map that has been added on to
%
% General procedure: search out the map most recently created by the STAMP
% analysis using the parameters for that analysis and the specific directory
% structure set up by the stamp-pem code. Concatenate the information of that
% new map onto the existing map (input) for that pem channel using stamp_pem_addtomap. 
%
% Code written by Michael Coughlin
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


try
   params.startGPSMap;
catch
   params.startGPS = params.startGPSMap;
end

try
   params.endGPSMap;
catch
   params.endGPS = params.endGPSMap;
end

% find all folders for this run name. 
runFolders = dir([params.dirPath '/' params.ifo1 '/' params.runName '-*']);
for i = 1:length(runFolders)
   runFile = [params.dirPath '/' params.ifo1 '/' runFolders(i).name '/' params.darmChannelUnderscore '/'  ...
      num2str(params.segmentDuration) '/' num2str(params.fmin) '-' num2str(params.fmax) '/'...
      params.pemChannelUnderscore '/' params.pemChannelUnderscore '_1.mat'];
   if ~exist(runFile)
      continue
   end
   %%% extract gps times for given folder
   folderSplit = regexp(runFolders(i).name,'-','split');
   mapStart = str2num(folderSplit{end-1}); mapEnd = str2num(folderSplit{end});
     % check if this is the folder we want...does it match the gps times from
     % the STAMP analysis just run.
   if (mapEnd < params.startGPSMap) || (mapStart > params.endGPSMap)
      continue
   end
      % load the file we want
   data = load(runFile);
% concatenate the new map onto the old map
   [map.snr,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.snr,...
      data.map0.segstarttime,data.map0.f,data.map0.snr);
   [map.y,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.y,...
      data.map0.segstarttime,data.map0.f,data.map0.y);      
   [map.Xi_snr,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.Xi_snr,...
      data.map0.segstarttime,data.map0.f,data.map0.Xi_snr);
   [map.sigma,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.sigma,...
      data.map0.segstarttime,data.map0.f,data.map0.sigma);
   [map.P1_log10,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.P1_log10,...
      data.map0.segstarttime,data.map0.f,data.map0.P1_log10);
   [map.P2_log10,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.P2_log10,...
      data.map0.segstarttime,data.map0.f,data.map0.P2_log10);
   [map.naiP1,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.naiP1,...
      data.map0.segstarttime,data.map0.f,data.map0.naiP1);
   [map.naiP2,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.naiP2,...
      data.map0.segstarttime,data.map0.f,data.map0.naiP2);
   [map.P1,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.P1,...
      data.map0.segstarttime,data.map0.f,data.map0.P1);
   [map.P2,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.P2,...
      data.map0.segstarttime,data.map0.f,data.map0.P2);
   [map.fft1,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.fft1,...
      data.map0.segstarttime,data.map0.f,data.map0.fft1);
   [map.fft2,tt] = stamp_pem_addtomap(params,map.segstarttime,map.f,map.fft2,...
      data.map0.segstarttime,data.map0.f,data.map0.fft2);

   map.segstarttime = tt;

end

map.xvals = map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);

map.snr(isnan(map.snr)) = 0;
map.y(isnan(map.y)) = 0;
map.Xi_snr(isnan(map.Xi_snr)) = 0;
map.sigma(isnan(map.sigma)) = 1e99;
map.P1_log10(isnan(map.P1_log10)) = 0;
map.P2_log10(isnan(map.P2_log10)) = 0;
map.naiP1(isnan(map.naiP1)) = 0;
map.naiP2(isnan(map.naiP2)) = 0;

return
