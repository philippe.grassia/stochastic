function stamp_pem_mod_dag(pemParamsFile,matappsPath,segmentDuration,startGPS,endGPS,fmin,fmax)
% function stamp_pem_make_dag(pemParamsFile,segmentDuration,startGPS,endGPS)
% Given a STAMP-PEM parameter file, channel name, and start and end GPS times,
% creates condor .dag and .sub files for running the STAMP-PEM pipeline on
% multiple channels
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if isstr(startGPS)
   startGPS=str2num(startGPS);
end
if isstr(endGPS)
   endGPS=str2num(endGPS);
end

if isstr(fmin)
   fmin=str2num(fmin);
end
if isstr(fmax)
   fmax=str2num(fmax);
end

if isstr(segmentDuration)
   segmentDuration=str2num(segmentDuration);
end

% Read in STAMP-PEM parameter file
params = readParamsFromFile(pemParamsFile);
params.segmentDuration = segmentDuration;
params.startGPS = startGPS; params.endGPS = endGPS;
params.fmin = fmin; params.fmax = fmax;

params.darmChannelUnderscore = strrep(params.darmChannel,':','_');
%read in stamp pem personal information
personal = readParamsFromFile([matappsPath '/stamp-pem/input/stamp_pem_personal.txt']);
	params.dirPath           = personal.dirPath;
	params.publicPath        = personal.publicPath;
        params.matappsPath       = personal.matappsPath;
        params.executableDir     = personal.executableDir;
        params.user              = personal.user;

darmChannelSplit = regexp(params.darmChannel,':','split');
params.darmChannelIfo = darmChannelSplit{1}; params.darmChannelName = darmChannelSplit{2};

% Read in channel list to determine channel sampling rates
if exist([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo1 '-' ...
            params.frameType1 '-channel_list_' params.runName '.txt']);
    [channelNames,channelSampleRates] = ...
        textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo1 '-' params.frameType1 '-channel_list_' params.runName '.txt'],'%s %f');
    params.darmChannelSampleRate = channelSampleRates(find(strcmp(channelNames,params.darmChannel)));
    [channelNames,channelSampleRates] = ...
        textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 ...
                  '-' params.frameType2 '-channel_list_' params.runName '.txt'],'%s %f');
    params.pemChannelSampleRate = channelSampleRates(1);
else
    fprintf('Running over default, non-run-specific, channel list\n');
    [channelNames,channelSampleRates] = ...
        textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo1 '-' params.frameType2 '-channel_list.txt'],'%s %f');
    params.darmChannelSampleRate = channelSampleRates(find(strcmp(channelNames,params.darmChannel)));
    [channelNames,channelSampleRates] = ...
        textread([params.matappsPath '/stamp-pem/input/stamp_pem-' params.ifo2 '-' params.frameType2 '-channel_list.txt'],'%s %f');
    params.pemChannelSampleRate = channelSampleRates(1);
end

% Output path for run
params.path = ...
    [params.dirPath '/' params.ifo1 '/' params.runName '-' num2str(params.startGPS) ...
     '-' num2str(params.endGPS) '/' params.darmChannelUnderscore '/' num2str(params.segmentDuration) '/' ...
    num2str(fmin) '-' num2str(fmax)];
job_number=0;
% Open dag file for writing
fprintf('%s \n', params.path);
fid=fopen([params.path '/condor/stamp_pem.dag.rescue001'],'w+');
% Loop over remaining channels placing each in its own job
fid
for i=1:length(channelNames)
    job_number=job_number+1;
    fprintf(fid,'RETRY %d 3\n',job_number)
    fprintf(fid,'DONE %d\n',job_number)
    fprintf(fid,'\n\n');
end

% Final job runs summary page
job_number=job_number+1;
fprintf(fid,'RETRY %d 3\n',job_number)
fprintf(fid,'\n\n');

% Uncommenting out the code below runs the harvesting code and pem ranking page for the data produced.  (runs in vanilla universe for softlinking purposes)
%job_number=job_number+1;
%fprintf(fid,'RETRY %d 3\n',job_number)
%fprintf(fid,'\n\n');

job_number_last=job_number;

% Zeroth job ensures no condor issues
fprintf(fid,'DONE 0\n');
fprintf(fid,'\n\n');

fclose(fid);

% Submit dag file to condor
condor_submit_dag_command = sprintf('condor_submit_dag -maxjobs 50 %s/condor/stamp_pem.dag',params.path);
fprintf('Run: %s\n',condor_submit_dag_command);
junk = system(condor_submit_dag_command);

