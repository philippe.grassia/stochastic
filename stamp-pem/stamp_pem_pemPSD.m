function stamp_pem_pemPSD(params,channelName)
% function stamp_pem_pemPSD(params,channelName)
% Given a STAMP-PEM parameters and channel name, writes average PSD 
% for that channel to file.
% Routine written by Michael Coughlin.
% Modified: August 1, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

channelNameUnderscore = strrep(channelName,':','_');

% Folders of this run type
folders = dir([params.dirPath '/' params.ifo1 '/' params.runName '-*']);

numberPSDs = 0;
pemPSD = [];

length(folders)

% Populate arrays with folder results
for i = 1:length(folders)

   % Check if there is data available for this channel   
   if exist([params.dirPath '/' params.ifo1 '/' folders(i).name '/' ...
	     params.darmChannelUnderscore '/' num2str(params.segmentDuration) '/' channelNameUnderscore '/' channelNameUnderscore '_psd.mat'])

      % Read in mat file associated with this channel      
      mat_data = load([params.dirPath '/' params.ifo1 '/' folders(i).name '/' ...
		       params.darmChannelUnderscore '/' num2str(params.segmentDuration) '/' channelNameUnderscore '/' channelNameUnderscore '_psd.mat']);

      % Remove NaNs from PSDs
      P2 = mat_data.pem.P2;
      P2(isnan(P2)) = 0;

      % Add PSDs to average PSD
      for j = 1:length(P2(1,:))
         if sum(P2(:,j)) ~= 0 
            if isempty(pemPSD)
               pemPSD = P2(:,j);
            else
               pemPSD = pemPSD + P2(:,j);
            end
            numberPSDs = numberPSDs + 1;
         end
      end
   end
end

% Average PSD
pemPSD = pemPSD / numberPSDs;

% Write PSD
save([params.pemPSDpath '/' channelNameUnderscore '.mat'],'pemPSD');

