This project contains code used by the LIGO Scientific Collaboration to search
for a stochastic background of gravitational waves.

The full repository includes a lot of test data and files and is quite large,
but to run the pipeline all that is needed are the directories

- CrossCorr
- Utilities
- PostProcessing

To export a tarball containing a minimal working copy of the necessary files to
run the isotropic pipeline, use 

```
git archive --prefix=stochastic/ --format tar.gz  \
  --remote git@git.ligo.org:stochastic-public/stochastic.git \
  HEAD CrossCorr PostProcessing Utilities -o stochastic.tar.gz
```

