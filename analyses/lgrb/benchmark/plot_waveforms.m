function plot_waveforms
% function plot_waveforms
% Eric Thrane: plots sqrt(hp^2 + hx^2) as a function of time for different
% waveforms

% reference distance (in Mpc) for which waveforms were created = 10 kpc
d = 0.01;

% distance to the source in Mpc
D = 10;

%b=load('adi_A.dat');
r=load('adi_B.dat'); % waveform c
g=load('adi_C.dat'); % waveform a
k=load('adi_D.dat'); % waveform b
%btot = sqrt(b(:,2).^2 + b(:,3).^2);
rtot = sqrt(r(:,2).^2 + r(:,3).^2) * (d/D);
gtot = sqrt(g(:,2).^2 + g(:,3).^2) * (d/D);
ktot = sqrt(k(:,2).^2 + k(:,3).^2) * (d/D);

% iterpolate to avoid an ugly looking plot
gx = g(2,1) : (g(end,1) - g(1,1))/20 : g(end,1);
gy = interp1(g(:,1), gtot, gx);
kx = k(2,1) : (k(end,1) - k(1,1))/20 : k(end,1);
ky = interp1(k(:,1), ktot, kx);
rx = r(2,1) : (r(end,1) - r(1,1))/20 : r(end,1);
ry = interp1(r(:,1), rtot, rx);

figure;
loglog(rx, ry, 'r-.', 'linewidth', 4)
hold on;
loglog(gx, gy, 'g--', 'linewidth', 4);
hold on;
loglog(kx, ky, 'k-', 'linewidth', 4)
axis([1e-1 3e2 1e-23 1e-20]);
xlabel('t (s)');
ylabel('(h_+(t)^2 + h_x(t)^2)^{1/2}');
pretty
%legend('A', 'B', 'C', 'D');
legend('a', 'b', 'c');
title(['d = ' num2str(D) ' Mpc'], 'fontsize', 20);
print('-dpng', 'model1_durations');
print('-depsc2', 'model1_durations');
