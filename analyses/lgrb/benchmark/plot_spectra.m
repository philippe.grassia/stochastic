function plot_spectra
% function plot_spectra

% waveform a
a = load('adi_B.dat');
ha = a(:,2);
Fsa = 1/(a(2,1) - a(1,1));
[hfa fa] = fft_eht(ha, Fsa);
Pfa = psd_eht(hfa, Fsa);

% waveform b
b = load('adi_C.dat');
hb = b(:,2);
Fsb = 1/(b(2,1) - b(1,1));
[hfb fb] = fft_eht(hb, Fsb);
Pfb = psd_eht(hfb, Fsb);

% waveform c
c = load('adi_D.dat');
hc = c(:,2);
Fsc = 1/(c(2,1) - c(1,1));
[hfc fc] = fft_eht(hc, Fsc);
Pfc = psd_eht(hfc, Fsc);

% plot
figure;
loglog(fa, Pfa, 'r');
hold on;
loglog(fb, Pfb, 'b');
hold on;
loglog(fc, Pfc, 'g');
legend('a', 'b', 'c');
xlabel('f (Hz)');
ylabel('PSD (Hz^{-1})');
axis([40 500 1e-48 1e-36]);
grid on;
pretty;
print('-dpng', 'plot_spectra');

return
