q = load('adi_B.dat');
hmax = max( sqrt(q(:,2).^2 + q(:,3).^2) );
% scale to 10 Mpc
hmax = hmax / 1000;
fprintf('adiB hmax = %1.1e\n', hmax);

q = load('adi_C.dat');
hmax = max( sqrt(q(:,2).^2 + q(:,3).^2) );
% scale to 10 Mpc
hmax = hmax / 1000;
fprintf('adiC hmax = %1.1e\n', hmax);

q = load('adi_D.dat');
hmax = max( sqrt(q(:,2).^2 + q(:,3).^2) );
% scale to 10 Mpc
hmax = hmax / 1000;
fprintf('adiD hmax = %1.1e\n', hmax);