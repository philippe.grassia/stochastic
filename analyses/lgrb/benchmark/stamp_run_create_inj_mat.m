function stamp_run_create_inj_mat
% function stamp_run_create_inj_mat
% This function creates .mat files for use in power injections
% by Michael Coughlin with comments from Eric Thrane
% Some lines are commented out because we decided to not use all the waveforms
% that were originally considered as options (because they are too short).

% The ADI waveforms
%create_inj_mat('M5a0.3eps0.2.dat','adi_A.mat');
%create_inj_mat('M5a0.3eps0.05.dat','adi_C.mat');
%create_inj_mat('M10a0.95eps0.2.dat','adi_B.mat');
%create_inj_mat('M10a0.95eps0.04.dat','adi_D.mat');
%
%create_inj_mat('adi_A.dat','adi_A.mat');
create_inj_mat('adi_B.dat','adi_B.mat');
create_inj_mat('adi_C.dat','adi_C.mat');
create_inj_mat('adi_D.dat','adi_D.mat');


% The Piro waveforms
%create_inj_mat('piroM2eta0.3fac0.2alpha0.01.dat','piro_B.mat');
%create_inj_mat('piroM2eta0.2fac0.2alpha0.01.dat','piro_D.mat');
%create_inj_mat('piroM10eta0.3fac0.2alpha0.01.dat','piro_C.mat');
%create_inj_mat('piroM10eta0.2fac0.2alpha0.01.dat','piro_A.mat');
%
%create_inj_mat('piro_A.dat','piro_A.mat');
%create_inj_mat('piro_B.dat','piro_B.mat');
%create_inj_mat('piro_C.dat','piro_C.mat');
%create_inj_mat('piro_D.dat','piro_D.mat');

return;
