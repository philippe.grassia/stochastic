function hrss_comparison2()
% function hrss_comparison2()
% E Thrane
% This script estimates hrss limits for the paper.
% The distances limits are coded by hand by looking at the paper.

% best fluece limits
F_adiB = 3.5;
F_adiC = 4.4;
F_adiD = 16;

% worst fluence limits
f_adiB = 1000;
f_adiC = 410;
f_adiD = 1200;

% calculate hrss for each waveform at 10 kpc
[F_B hrss_B] = cal_fluence('adi_B.dat', 1, false, false);
[F_C hrss_C] = cal_fluence('adi_C.dat', 1, false, false);
[F_D hrss_D] = cal_fluence('adi_D.dat', 1, false, false);

% scale according distance limits
hrss_best_B = hrss_B * sqrt(F_adiB/F_B);
hrss_best_C = hrss_C * sqrt(F_adiC/F_C);
hrss_best_D = hrss_D * sqrt(F_adiD/F_D);
%
hrss_worst_B = hrss_B * sqrt(f_adiB/F_B);
hrss_worst_C = hrss_C * sqrt(f_adiC/F_C);
hrss_worst_D = hrss_D * sqrt(f_adiD/F_D);

% print
fprintf('hrss for adiB: %1.1e - %1.1e\n', hrss_best_B, hrss_worst_B);
fprintf('hrss for adiC: %1.1e - %1.1e\n', hrss_best_C, hrss_worst_C);
fprintf('hrss for adiD: %1.1e - %1.1e\n', hrss_best_D, hrss_worst_D);
fprintf('compare with http://iopscience.iop.org/0004-637X/715/2/1438/pdf/0004-637X_715_2_1438.pdf\n');
fprintf('where hrss = 1.8e-22 - 3.8e-21\n');

return
