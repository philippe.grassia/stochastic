function chk_antenna_facs
% function chk_antenna_facs

% input parameters-------------------------------------------------------------
rawtriggerlist = '/home/ethrane/analyses/lgrb/triggers/s5vsr1_swift_lgrb.txt';
% format is: GRB GPS RA DEC DURATION
% Every line including the first should have five entries.
% The first line must be header information.
%------------------------------------------------------------------------------

% load raw trigger list
str = read_doc(rawtriggerlist);

% parse the data 
str = regexprep(str, '\n', ' ');
trigdata = regexp(str,' ','split');
trigdata = trigdata(2:end);

% number of columns in raw trigger list
ncol = 5;

% sanity check
fprintf('%1.1f lines in rawtriggerlist\n', length(trigdata) / ncol);

% put the data into variables
jj = 1;
for ii=1+ncol : ncol : length(trigdata)
  ID{jj} = trigdata{ii};
  gps(jj) = str2num(trigdata{ii+1});
  ra(jj) = str2num(trigdata{ii+2});
  % convert ra into hours from degrees
  ra(jj) = ra(jj) * (12/180);
  dec(jj) = str2num(trigdata{ii+3});
  dur(jj) = str2num(trigdata{ii+4});
  jj = jj + 1;
end

% the number of triggers
N = jj-1;

%------------------------------------------------------------------------------
% Identify and remove any triggers associated with bad antenna factor ratios.
% The antenna factor ratio is a measure of the cross-power to auto-power ratio
% expected if two of the eight neighboring segments are contaminated with
% signal.  We require that this factor be greater than one to avoid tracks of 
% near-zeros which can result from the CC signal being near zero.  It is 
% esentially impossible to detect such signals with a cross-correlation 
% statistic and therefore it is problematic to set upper limits.  Only one 
% trigger (3% of available triggers) are removed this way.
%------------------------------------------------------------------------------
params2 = stampDefaults;
params2.ifo1 = 'H1';
params2.ifo2 = 'L1';
% define physical constants
params2.G = 6.673e-11;
params2.c = 299792458;
params2.ErgsPerSqCM = 1000;
% proceed
[det1 det2] = ifo2dets(params2);
kk=1;
for tt=1:N
  source = [ra(tt) dec(tt)];
  g = calF(det1, det2, gps(tt), source, params2);
  epsilon = abs(g.F1p .* g.F2p + g.F1x .* g.F2x)/2;
  eps11 = abs(g.F1p.^2 + g.F1x.^2);
  eps22 = abs(g.F2p.^2 + g.F2x.^2);
  % not all triggers have upper limits
  try
    q = load(['/home/ethrane/stamp_results/upperlimits_v5/adi_D/' ...
      num2str(gps(tt)) '/limits_lgrb_v5.mat']);
    % save upper limit divided by epsilon
    x(kk) = q.ul * epsilon;
    kk=kk+1;
  catch
  end
end

% print diagnostic
fprintf('(xmin, xmax) = %1.1e, %1.1e\n', min(x), max(x));
fprintf('std(x)/mean(x) = %1.1e\n', std(x)/mean(x));

% histogram of log(x): note that x takes on discrete values due to spacing of 
% upper limit values in upper limit code.
figure;
hist(log10(x), 25);
xlabel('log(\alpha_{ul} \epsilon)');
pretty;
print('-dpng', 'chk_antenna_facs');

keyboard

return