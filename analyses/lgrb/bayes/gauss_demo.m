function gauss_demo
% function gauss_demo
% demonstrates how multiple posteriors can be combined to produce an average
% posterior

% number of trials
T = 100;

% theoretical normal distribution width
sigma = 1;

% generate random MC measurements
y = randn(T, 1);

% domain
x = -5:0.01:5;

% generate inferred posteriors
for ii=1:T
  py(ii,:) = exp(-(x-y(ii)).^2 / (2*sigma));
  py(ii,:) = py(ii,:) / sum(py(ii,:));
end

% combine posteriors using the previous one as the prior
prior = py(1,:);
for jj=2:T
  posterior = py(jj,:) .* prior;
  posterior = posterior / sum(posterior);
  prior = posterior;
  if jj==10
    posterior10 = posterior;
  end
end

% plot the results
figure;
for kk=1:T
  plot(x, py, 'b');
  hold on;
end
plot(x, posterior10, 'g');
hold on;
plot(x, posterior, 'r');
hold off;
pretty;
print('-dpng', 'gauss_demo');
