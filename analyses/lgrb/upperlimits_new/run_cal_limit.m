function run_cal_limit(matappspath)
% function run_cal_limit(matappspath)
% M Coughlin and Eric Thrane
% calls stamp_limits for every job and waveform

% load the list of jobs with no missing data
jobfile = load([matappspath 'analyses/lgrb/triggers/jobfile_0.txt']);

% parse jobfile
jobs = jobfile(:, 1);

% list of waveforms
waveforms = {'adi_B','adi_C','adi_D'};

% loop over jobs and wavefroms
for ii=1:length(jobs)
   for jj=1:length(waveforms)
      % call cal_limit
      try
        cal_limit(jobs(ii), waveforms{jj}, matappspath);
      catch
        fprintf('Error processing job=%i, waveform=%s\n', ...
          jobs(ii), waveforms{jj});
      end
   end
end

return
