function cal_limit(job, waveform, matappspath)
% function cal_limit(job, waveform, matappspath)
% by Eric Thrane and Michael Coughlin
% job = the trigger number (not to be confused with the trial number below 
%   denoted by ii)
% waveform = the name of the waveform, which is used to label the clustermap
%   output files; e.g., waveform = 'adi_D';
% matappspath = '/home/ethrane/matapps';
%
% This script harvests the output files from the sensitivity study and 
% calculates upper limits.

% make things condor-friendly
job = strassign(job);
if job==0
  return;
end

% set confidence limit
cl = 0.95;

% load trigger file for triggers with no missing data
q = load([matappspath '/analyses/lgrb/triggers/triggers_0.txt']);
% get trigger time from trigger file
trigger_gps = q(job, 2);

% define a prefix to label file names
%str = 'lgrb_v2';
str = 'lgrb_v3';

% print arguments to screen
fprintf('Running limits code for waveform %s and job %d\n', waveform, job);

% this is where the output .png files will go
% note, the summary .mat files will go with the other mat files (see below)
output_file_path_base = '/home/ethrane/stamp_results/upperlimits/';

% make sure output directories exist
% e.g., ~/stamp_results/upperlimits/plots/adi_D/829887393/
% first, check the 'base" directory named with the waveform
plotdir = [output_file_path_base 'plots/' waveform '/' num2str(trigger_gps)];
if ~exist(plotdir)
 system(['mkdir ' plotdir]);
end

% get a list of matfiles created from the injection study (run_clustermap_ul)
mat_files = dir( ['/home/ethrane/stamp_results/upperlimits/' waveform '/' ...
   num2str(trigger_gps) '/' str '_' num2str(trigger_gps) '_*.mat']);

% determine the number of mat files (the number of injection trials)
N = length(mat_files);

% loop over mat files to obtain an array or SNR for different trials and 
% for for different injection strengths (alpha)
for ii=1:N
  mat_file = ['/home/ethrane/stamp_results/upperlimits/' waveform '/' ...
    num2str(trigger_gps) '/' mat_files(ii).name];
  % load mat file
  q=load(mat_file);
  if ii==1
    % if this is the first file loaded keep all of stoch_out to save meta-data
    stoch_out = q.stoch_out;
    % record measured SNR with no injection
    snr = q.stoch_out.max_SNR(1);
    fprintf('The measured SNR is %1.2f.\n', snr);
  else
    % if this is not the first file, keep just max_SNR
    stoch_out.max_SNR = [stoch_out.max_SNR q.stoch_out.max_SNR];
    % sanity check: make sure that no-injection snr is the same for each file
    if abs(snr-q.stoch_out.max_SNR(1))>1e-5
      fprintf('problem with job=%i, waveform=%s, triall=%i\n', ...
        job, waveform, ii);
      error('Inconsistency in injection files.');
    end
  end
end

% reshape data from 1D array to a 2D array: the first dimension of data is
% trial number and the second is alpha index.  The first entry in the second 
% (alpha) dimension is always alpha=0.
data = reshape(stoch_out.max_SNR, stoch_out.params.alpha_n, N)';

% determine the round-off fluctuations in the zero-amplitude signal
numerr = std(data(:,1));

% check that the round-off fluctuations are small
if numerr/mean(data(:,1)) > 1e-8
  fprintf('problem with job=%i, waveform=%s\n', job, waveform);
  error('round-off fluctuations suspiciously large');
end

% loop over injection values
for kk=1:stoch_out.params.alpha_n
  % calculate the fraction of entries with SNR > the measured SNR (denoted
  % lower-case snr.  note: we multiply snr by 1+5*numerr to take into account
  % numerical rounding error
  frac(kk) = sum(data(:, kk)>snr*(1+5*numerr)) / length(data(:, kk));
end

% define a more convenient variable name for alpha
alpha = stoch_out.params.alpha(1:stoch_out.params.alpha_n);

% calculate upper limit: the value of alpha for which at least cl=95% of the
% injections exceed the measured value of SNR (denoted lower-case snr)
% This calculation is applicable when frac(alpha) is monotonically increasing.
% ul = min(alpha(frac>=cl));
%
% A more general calculation allows for downward dips in frac(alpha):
% Get an array of ones and zeros telling us which alpha values pass the
% frac>=cl cut.
flag = frac>=cl;
% Determine the largest alpha value to fail the cut.
midx = max(find(flag==0));
% Set the upper limit to be the next highest alpha value.
try
  ul = alpha(midx+1);
catch
  % make sure that the injections provided are sufficient to calculate the UL
  fprintf('problem with job=%i, waveform=%s\n', job, waveform);
  error('Unable to set upper limit.');
end

% make sure that the injection amplitudes included values that did not exceed
% the threshold more than cl=95% of the time.  alpha(1)=0 never passes this
% requirement so we demand that at least one other injection value passes.
if sum(frac<cl)<2
  fprintf('problem with job=%i, waveform=%s\n', job, waveform);
  error('Alpha values may be too conservative.');
end

% calculate the distance upper limit: distance goes like 1/sqrt(power)
ul_distance = sqrt(1/ul);

% waveforms created for 10 kpc -- convert to Mpc
% The units for ul_distance before conversion we can call dkpc for tens of kpc.
% To convert to Mpc we note that there are (10 kpc / 1 dkpc) and 
% (1 Mpc / 1000 kpc).
fprintf('scaling distance assuming 10 kpc waveform\n');
ul_distance = ul_distance * (10 / 1000);

% calculate fluence (units = ergs/cm^2)
F = cal_fluence([matappspath '/analyses/lgrb/benchmark/' waveform '.dat'], ul);

% save summary results
save([output_file_path_base waveform '/' num2str(trigger_gps) ...
  '/limits_' str '.mat']);

% make a diagnostic plot frac vs alpha
figure;
semilogx(alpha(2:end), frac(2:end), '-bo');
hold on;
semilogx([alpha(2) alpha(end)], [cl cl], 'r--');
hold on;
semilogx([ul ul], [0 1], 'g:');
xlabel('\alpha');
ylabel(['fraction exceeding th=' snr]);
axis([min(alpha) max(alpha) 0 1]);
title(['snr = ' num2str(snr) ', N = ' num2str(N)], 'FontSize', 20);
pretty;
print('-dpng', [plotdir '/alpha_' str]);

return;
