function ul_table_for_paper(matappspath)
% function ul_table_for_paper(matappspath)
% Eric Thrane
% This function reads in the upper limit results and creates a latex table file
% that can be pasted into the paper.  That file is called ul_table.tex.

% waveforms
waveforms = {'adi_B', 'adi_C', 'adi_D'};

% the names given to the waveforms in the paper
names = {'a', 'b', 'c'};

% top directory
top = '/home/stamp/upperlimits_new/lgrb/';

% prepare output file
fid = fopen('ul_table.tex', 'w+');

% load astro list in order to obtain GRB names, gps trig times, and job numbers
str = read_doc([matappspath 'analyses/lgrb/triggers/astro_0.txt']);
str = regexprep(str, '\n', ' ');
trigdata = regexp(str,' ','split');
trigdata = trigdata(2:end);
ncol = 5;
jj = 1;
for ii=1 : ncol : length(trigdata)
  ID{jj} = trigdata{ii};
  gps(jj) = str2num(trigdata{ii+1});
  jobs(jj) = jj;
  jj = jj + 1;
end

% loop over jobs, load data, print upper limits
for jj=1:length(waveforms)
  for ii=1:length(gps)
    waveform = waveforms{jj};
    name = names{jj};
    try
      % load upper limit data
      mat_data = load([top 'plots/' waveform '/' num2str(gps(ii)) ...
        '/limits_lgrb_v2.mat']);
      % use latex notation for fluence (obsolete)
%      F = latexstr(mat_data.F); % (obsolete)
      F = mat_data.F;
FF(ii,jj) = F;
D(ii,jj) = mat_data.ul_distance;
      % print results to screen
      fprintf('%i %s %1.1f %1.3f\n' , ...
        jobs(ii), waveform, mat_data.ul_distance, F);
      % print results for latex table
%EHT      fprintf(fid, '%s & %s & %1.1f & %1.3f \\\\\\hline\n' , ...
%EHT        ID{ii}, name, mat_data.ul_distance, F);
      fprintf('successfully loaded job=%i, waveform=%s\n', jobs(ii), waveform);
    catch
      % pause the script for debugging if any data is not available
      fprintf('cannot load job=%i, waveform=%s\n', jobs(ii), waveform);
      fprintf('  %s\n', [top 'plots/' waveform '/' num2str(gps(ii)) ...
        '/limits_lgrb_v2.mat'])
%      keyboard
    end
  end
end

% create latex file
for ii=1:length(gps)
  fprintf(fid, '%s', ID{ii});
  for jj=1:length(waveforms)
    fprintf(fid, ' & %1.1f', D(ii,jj));
  end
  for jj=1:length(waveforms)
    fprintf(fid, ' & %1.3f', FF(ii,jj));
  end
  fprintf(fid, ' \\\\\\hline\n');
end

% clean up
fclose(fid);

% save a mat file of the workspace
save('ul_table.mat');

return;
