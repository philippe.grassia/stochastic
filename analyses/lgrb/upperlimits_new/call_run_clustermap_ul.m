function call_run_clustermap_ul(matappspath, ii, trial)
% function call_run_clustermap_ul(matappspath, ii, trial)
% M Coughlin and Eric Thrane
% calls clustermap for each waveform and job for sensitivity studies

% condorize
trial = strassign(trial);
ii = strassign(ii);
if trial==0
  return;
end

% load the list of jobs with no missing data
jobfile = load([matappspath 'analyses/lgrb/triggers/jobfile_0.txt']);

% parse jobfile
jobs = jobfile(:, 1);

% list of waveforms
waveforms = {'adi_B','adi_C','adi_D'};

% loop over waveforms
for jj=1:length(waveforms)
  % call clustermap
  run_clustermap_ul(jobs(ii), trial, waveforms{jj}, matappspath);
end

return
