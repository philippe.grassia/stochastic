function stoch_out = example_clustermap(matappspath);
% function stoch_out = example_clustermap(matappspath);
% Eric Thrane: this script is a wrapper for clustermap.  It performs power
% injections.

% set values to specify example injection
trial = 1;
job = 3;
waveform = 'adi_D';
try
  matappspath;
catch
  matappspath = '/home/ethrane/matapps/';
end

% output file prefix
str ='example';

% initialize default parameters
params = stampDefaults;
if strcmp(waveform,'none')
   params.powerinj = false;
else
   params.powerinj = true;
   params.injfile = [matappspath '/analyses/lgrb/benchmark/' waveform '.mat'];
end

% initialize variables
stoch_out.start=true;
job = strassign(job);
trial = strassign(trial);
if trial==0
  return;
end

% jobfile and triggerfile for Switft triggers
jobsFile = [matappspath '/analyses/lgrb/upperlimits/jobfile_s5_swift_1000s_v2.txt'];
q = load([matappspath '/analyses/lgrb/upperlimits/triggers_s5_swift_1000s_v2.txt']);
% get ra and dec from trigger file
params.ra = q(job, 3);
params.dec = q(job, 4);

% input mat directory
% MC for the first 100 jobs of S5
%params.inmats = '/archive/home/ethrane/stamp2/test/mats/map';
% TS data from S5
params.inmats = '/home/stamp/stampmatfiles/HL-SID_nspi9_df1_dt1_ts1';

% frequency range--------------------------------------------------------------
params.fmin = 100;
% zoom in on signal region for nice plots
params.fmax = 300;

% override default parameters--------------------------------------------------
params.saveMat=false;
params.savePlots=true;
params.plotdir = './';

% notches
params = mask_S5H1L1_1s1Hz(params);

% set the seed
%params.seed = 1e9*job;
params.seed = trial*1e5;

% search
params = burstegardDefaults(params);
params.burstegard.findtrack = true;
params.burstegard.rr = 50;

% glitch cut
% turn off glitch cut to avoid missing data in example plot
%params.glitchCut = true;
params.glitchCut = false;

% make sure there is enough time to do the injection
f = load(jobsFile);
dur = f(job, 4);
start = f(job, 2);
% inject into just a small map
%stop = start+dur;
stop = start + 300;

load([matappspath '/analyses/lgrb/upperlimits/epsilon.mat']);

% multi-injection mode
params.inj_trials = 1;

params.alpha_max = (epsilon(3)/epsilon(job)) * 1.9e-5;
% inject at just one amplitude
%params.alpha_min = (epsilon(3)/epsilon(job)) * 6.4e-8;
params.alpha_min = params.alpha_max;

if strcmp(waveform,'adi_A')
   scale_factor = 2.5e0;
elseif strcmp(waveform,'adi_B')
   scale_factor = 2e1;
elseif strcmp(waveform,'adi_C')
   scale_factor = 1.5e-1;
elseif strcmp(waveform,'adi_D')
   scale_factor = 0.9e0;
elseif strcmp(waveform,'piro_A')
   scale_factor = 8e2;
elseif strcmp(waveform,'piro_B')
   scale_factor = 5e3;
elseif strcmp(waveform,'piro_C')
   scale_factor = 1.25e2;
elseif strcmp(waveform,'piro_D')
   scale_factor = 2.7e4;
else
   scale_factor = 1;
end
   
params.alpha_min = params.alpha_min * scale_factor;
params.alpha_max = params.alpha_max * scale_factor;

% distance
dist = 0.01 / sqrt(params.alpha_max);
fprintf('distance = %1.2f Mpc\n', dist);

% run clustermap 
fprintf('running clustermap with many trials...\n');
stoch_out=clustermap(params, start, stop);

output_file_path = './';

% save output files
save([output_file_path '/' str '_' num2str(job) '_' num2str(trial) '.mat']);

return
