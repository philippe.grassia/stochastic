function stoch_out = run_clustermap_noise(job)
% function test = run_clustermap_noise(job)
% performs clustering for pure noise for parameter tuning study
warning off;

% initialize variables
stoch_out.start=true;
job = strassign(job);
if job==0, return; end;

% initialize default parameters
params = lgrb_params(job);

% output file prefix-----------------------------------------------------------
% default settings
%str = 'default';
%
% cluster size test
%str = 'test_ncn_10';
%params.burstegard.NCN = 10;
%
% cluster size test
%str = 'test_ncn_120';
%params.burstegard.NCN = 120;
%
% cluster size test
%str = 'test_ncn_40';
%params.burstegard.NCN = 40;
%
% cluster size test
%str = 'test_ncn_160';
%params.burstegard.NCN = 160;
%
% cluster size test
%str = 'test_ncn_60';
%params.burstegard.NCN = 60;
%
% radius test
%str = 'test_nr_2.5';
%params.burstegard.NR = 2.5;
%
% radius test
%str = 'test_nr_1.5';
%params.burstegard.NR = 1.5;
%
% tmetric test
%str = 'test_tmetric_1.5';
%params.burstegard.tmetric = 1.5;
%
% tmetric test
%str = 'test_tmetric_0.5';
%params.burstegard.tmetric = 0.5;
%
% fmetric test
%str = 'test_fmetric_1.5';
%params.burstegard.fmetric = 1.5;
%
% fmetric test
str = 'test_fmetric_0.5';
params.burstegard.fmetric = 0.5;
%
% pixel SNR test
%str = 'test_pixel_0.7';
%params.burstegard.pixelThreshold = 0.7;
%
% pixel SNR test
%str = 'test_pixel_0.8';
%params.burstegard.pixelThreshold = 0.8;
%
% pixel SNR test
%str = 'test_pixel_0.6';
%params.burstegard.pixelThreshold = 0.6;
%
% pixel SNR test
%str = 'test_pixel_0.9';
%params.burstegard.pixelThreshold = 0.9;
%
% pixel SNR test
%str = 'test_pixel_1';
%params.burstegard.pixelThreshold = 1;
%
% pixel SNR test
%str = 'test_pixel_1.5';
%params.burstegard.pixelThreshold = 1.5;
%
% pixel SNR test
%str = 'test_pixel_1.3';
%params.burstegard.pixelThreshold = 1.3;
%
% pixel SNR test
%str = 'test_pixel_1.1';
%params.burstegard.pixelThreshold = 1.1;
%------------------------------------------------------------------------------

% override standard options
%params.savePlots=true;
%params.saveMat = true;
%
% deterministic random seed (does not matter when there are no injections)
% and better for reproducibility
params.seed = 1;
params.jobNumber=job;

% call clustermap
stoch_out=clustermap(params, params.hstart, params.hstop);

% save output files
save(['/usr1/ethrane/' str '_' num2str(job) '.mat'], 'stoch_out');
