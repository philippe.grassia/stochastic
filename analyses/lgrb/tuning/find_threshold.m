function snr = find_threshold
% function snr = find_threshold
warning off;

%------------------------------------------------------------------------------
% default settings
%str = 'default';
%
% minimum cluster size
%str = 'test_ncn_10';
%
% maximum cluster size
%str = 'test_ncn_120';
%
% varying cluster size
%str = 'test_ncn_40';
%
% varying cluster size
%str = 'test_ncn_160';
%
% varying cluster size
%str = 'test_ncn_60';
%
% varying cluster radius
%str = 'test_nr_2.5';
%
% varying cluster radius
%str = 'test_nr_1.5';
%
% varying tmetric
%str = 'test_tmetric_1.5';
%
% varying tmetric
%str = 'test_tmetric_0.5';
%
% varying fmetric
%str = 'test_fmetric_1.5';
%
% varying fmetric
str = 'test_fmetric_0.5';
%
% varying pixel threshold
%str = 'test_pixel_0.7';
%
% varying pixel threshold
%str = 'test_pixel_0.8';
%
% varying pixel threshold
%str = 'test_pixel_0.6';
%
% varying pixel threshold
%str = 'test_pixel_0.9';
%
% varying pixel threshold
%str = 'test_pixel_1';
%
% varying pixel threshold
%str = 'test_pixel_1.5';
%
% varying pixel threshold
%str = 'test_pixel_1.3';
%
% varying pixel threshold
%str = 'test_pixel_1.1';
%------------------------------------------------------------------------------

% number of trials
T = 1000;

% read in files
snr = [];
for ii=1:T
  try
    q = load(['./mats/' str '/' str '_' num2str(ii) '.mat']);
    snr = [snr q.stoch_out.max_SNR];
  catch
    fprintf('cannot load job %i\n', ii);
  end
end

% determine threshold
idx = length(snr) - floor(length(snr)/T);
tmp = sort(snr);
th = tmp(idx);
fprintf('threshold = %1.2f\n', th);

% save file
save(['mats/' str '_th.mat']);

% make a plot
figure;
hist(snr, [15:0.5:25]);
xlabel('SNR');
pretty;
print('-dpng', [str '_th.png']);
