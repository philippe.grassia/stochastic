function params = lgrb_params(job)
% function params = lgrb_params
% sets default parameters for LGRB sensitivity study

% condorize
job = strassign(job);

% initialize default parameters
params = stampDefaults;
params = burstegardDefaults(params);

% override GPS times
params.override_gps = 855882630;

% injection: GRB070219 @ GPS = 855882630
params.savePlots=false;
params.ra = 17.348;
params.dec = 69.345;

% input mat directory: Monte Carlo noise
params.inmats = ...
  '/home/ethrane/HL-SID_nspi9_df1_dt1_ts1/HL-SID_nspi9_df1_dt1_ts1';

% S5 jobfile
params.jobsFile = '/home/ethrane/analyses/lgrb/tuning/S5H1L1_full_run.txt';

% parse jobfile
h = load(params.jobsFile);

% work with a reduced list of jobs over 1500s + (12s buffer) long
DeltaT = 1500;
buffer = 6;
durs = h(:,3) - h(:,2);
cut = durs>=DeltaT+2*buffer;
h = h(cut,:);
% skip to the end of the run (last 1000 jobs) after the detector has stabilized
h = h(end-1000:end,:);
params.hstart = buffer + h(job, 2);
% each job is at least 1500s long
params.hstop = params.hstart + DeltaT;

% frequency range
params.fmin = 100;
params.fmax = 1200;

% save mat files and diagnostic plots
params.saveMat=false;

% notches
params = mask_S5H1L1_1s1Hz(params);

% notching is on
params.doStampFreqMask=true;
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove>=params.fmin);

% glitch cut
params.glitchCut = true;
params.doCoincidentCut = 1;

% random seed set with GPS time
params.seed = -1;
params.jobNumber=job;

return
