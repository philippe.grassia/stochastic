function detection_distance(str)
% function detection_distance(str)
% str is the prefix of the matfile created by find_threshold
tic;

% the number of trials
T=40;

% load in mat file created by find_threshold in order to get threshold
q = load(['mats/' str '_th.mat']);
th = q.th;

% get clustering parameter from one of the mat files
r = load(['mats/' str '/' str '_1.mat']);
R.NCN = r.stoch_out.params.burstegard.NCN;
R.NR = r.stoch_out.params.burstegard.NR;
R.tmetric = r.stoch_out.params.burstegard.tmetric;
R.fmetric = r.stoch_out.params.burstegard.fmetric;
R.pixelThreshold = r.stoch_out.params.burstegard.pixelThreshold;

% determine a range of injection strength values and associated distances
dmin = 1000;
dmax = 1728;
d = dmin;
% use logarithmic spacing
while 1~=0
  if max(d)>=dmax
    break;
  else
    % look at distance with 20% spacing
    d = [d 1.2*d(end)];
  end
end
% start from far away and work closer
d = fliplr(d);
alpha = d.^-2;

% printout information so we can see what we're getting into
fprintf('(dmin,dmax)=(%f,%f)\n', min(d), max(d));
fprintf('calculating SNR for %i distances x %i trials\n', length(d), T);

% loop over alpha values
for ii=1:length(d)
  % loop over trials
   for jj=1:T
     % clustermap parameters
     % job number taken from trial number
     params = lgrb_params(jj);
     params.powerinj = true;
     params.injfile = '/home/ethrane/lgrb/michal/adi_D.mat';
     params.inj_trials = 1;
     params.fmax = 250;
     params.StampFreqsToRemove = ...
     params.StampFreqsToRemove(params.StampFreqsToRemove<=params.fmax);
     % vary clustering parameter
     params.burstegard.NCN = R.NCN;
     params.burstegard.NR = R.NR;
     params.burstegard.tmetric = R.tmetric;
     params.burstegard.fmetric = R.fmetric;
     params.burstegard.pixelThreshold = R.pixelThreshold;
     % vary injection amplitude with ii
     params.alpha_min = alpha(ii);
     params.alpha_max = params.alpha_min;
     % call clustermap
     stoch_out = clustermap(params, params.hstart, params.hstop);
     snr(ii,jj) = stoch_out.max_SNR;
  end
  % calculate median snr
  snrbar(ii) = median(snr(ii,:));
  % does median snr exceed threshold?
  if snrbar(ii)>th
    % is the detection distance outside of the range?
    if d(ii)==max(d)
      fprintf('Detection distance exceeds %fe\n', d(ii));
    else
      fprintf('detection distance = %f\n', d(ii));
    end
    % no point continuing once median(snr) exceeds threshold
    save(['mats/' str '_d.mat']);
    return;
  end
end

% detection distance is out of range
fprintf('Detection distance is closer than %f\n', d(ii));

% save a record
save(['mats/' str '_d.mat']);

toc;
return
