function [pval snr bknd fluence] = cal_pvalue(bknd, gps0, search_str);
% function pval = cal_pvalue(bknd, gps, search_str);
% Eric Thrane: this function loads in data to calculate the p-value for data
% at a given gps time

% load background study data if it has not already been loaded
try
  load('background.mat');
catch
  bknd = load('/home/stamp/background/pvalues/TS1/background.mat');
end

% get SNR value for requested trigger time
try
  % The files in search/ are from adi_B trial one.  The first entry in out.snr
  % corresponds to no injection.
  X = load(['/home/ethrane/stamp_results/upperlimits_v5/search/' ...
     num2str(gps0) '/' search_str '_' num2str(gps0) '.mat']);
  snr = X.out.snr(1);
  % calculate p-value (bknd.snrs_sorted are sorted from low to high)
  [dummy snridx] = min(abs(snr - bknd.snrs_sorted));
  pval = 1-snridx/length(bknd.snrs_sorted);
  % determine the fluence of the most significant cluster
  fluence = X.stoch_out.fluence;
catch
  pval = -1;
  snr = -1;
  fluence = -1;
  fprintf('warning: no data available for %i\n', gps0);
end

return
