function find_triggers
% function find_triggers
% by Eric Thrane
% Modify the input parameters before running and make sure that your trigger
% list has the correct format (see below).
%
% This function reads in rawtriggerlist and generates three (sets of) output
% files:
%
%   astro_*.txt: trigger_name trigger_gps ra_hr dec_deg trigger_dur_sec
%   triggers_*.txt: trigger_number gps ra_hr dec_deg
%   jobfile_*.txt: trigger number start_gps end_gps search_dur
%
% The trigger files and job files are used by run_clustermap to tell STAMP
% when and where to perform the search.  The astro file keeps track of the
% proper trigger names, e.g., GRB051117B, which have characters in them so it
% is convenient to keep them in a separate file.
%
% This function calculates the amount of data available for each trigger and
% then cuts some triggers that have more than T missing data.  The T variable
% is defined in the input parameter section below.
%
% The jobfile start and stop times are determined by t1 and t2.
%
% The location of STAMP data is specified in params.inmats.
%
% The code applies a cut on the antenna factor ratio.

% input parameters-------------------------------------------------------------
rawtriggerlist = 's5vsr1_swift_lgrb.txt';
% format is: GRB GPS RA DEC DURATION
% Every line including the first should have five entries.
% The first line must be header information.

% location zero-lag STAMP mat files
params.inmats='/home/ethrane/HL-SID_nspi9_df1_dt1_zl/HL-SID_nspi9_df1_dt1_zl';

% specify the beginning and end of the on-source window in relation to the GRB
% trigger
t1 = -600;
t2 = 900;

% the amount of missing data allowed to keep a trigger in seconds; 
% (multiple cuts possible)
T = [0 500];

% epsilon cut
epsilon_min = 0.005;

% define string used to label the search
search_str = 'lgrb_v5';

% include p-values in the table?  this should be turned off when we are just
% creating trigger lists but no data has been analyzed yet.  once data has 
% been analyzed, this is set true
doPVal = true;
%------------------------------------------------------------------------------

% load raw trigger list
str = read_doc(rawtriggerlist);

% parse the data 
str = regexprep(str, '\n', ' ');
trigdata = regexp(str,' ','split');
trigdata = trigdata(2:end);

% number of columns in raw trigger list
ncol = 5;

% sanity check
fprintf('%1.1f lines in rawtriggerlist\n', length(trigdata) / ncol);

% put the data into variables
jj = 1;
for ii=1+ncol : ncol : length(trigdata)
  ID{jj} = trigdata{ii};
  gps(jj) = str2num(trigdata{ii+1});
  ra(jj) = str2num(trigdata{ii+2});
  % convert ra into hours from degrees
  ra(jj) = ra(jj) * (12/180);
  dec(jj) = str2num(trigdata{ii+3});
  dur(jj) = str2num(trigdata{ii+4});
  jj = jj + 1;
end

% the number of triggers
N = jj-1;

% sanity check
fprintf('the first GRB is: %s %i %1.2f %1.2f %1.0f\n', ID{1}, gps(1), ...
  ra(1), dec(1), dur(1));
fprintf('the last GRB is: %s %i %1.2f %1.2f %1.0f\n', ID{end}, gps(end), ...
  ra(end), dec(end), dur(end));

%------------------------------------------------------------------------------
% Identify and remove any triggers associated with bad antenna factor ratios.
% The antenna factor ratio is a measure of the cross-power to auto-power ratio
% expected if two of the eight neighboring segments are contaminated with
% signal.  We require that this factor be greater than one to avoid tracks of 
% near-zeros which can result from the CC signal being near zero.  It is 
% esentially impossible to detect such signals with a cross-correlation 
% statistic and therefore it is problematic to set upper limits.  Only one 
% trigger (3% of available triggers) are removed this way.
%------------------------------------------------------------------------------
params2 = stampDefaults;
params2.ifo1 = 'H1';
params2.ifo2 = 'L1';
% define physical constants
params2.G = 6.673e-11;
params2.c = 299792458;
params2.ErgsPerSqCM = 1000;
% proceed
[det1 det2] = ifo2dets(params2);
for tt=1:N
  source = [ra(tt) dec(tt)];
  g = calF(det1, det2, gps(tt), source, params2);
  epsilon = abs(g.F1p .* g.F2p + g.F1x .* g.F2x)/2;
  eps11 = abs(g.F1p.^2 + g.F1x.^2);
  eps22 = abs(g.F2p.^2 + g.F2x.^2);
  R(tt) = 4 * epsilon / sqrt(eps11*eps22);
end

%------------------------------------------------------------------------------
% The trigger data has been parsed.  Now we apply a cut on the amount of data
% available to analyze.
%------------------------------------------------------------------------------
% define metadata to use STAMP tool loadmats; the values do not actually matter
params.fmin = 100;
params.fmax = 1200;

% determine the amount of missing data for each trigger
for qq=1:N
  % beginning of on-source time
  params.startGPS = gps(qq) + t1;
  % end of on-source time
  params.endGPS = gps(qq) + t2;
  % find official data
  try
    [matlist, params] = findmats(params);
    % load official data
    [map, params, pp] = loadmats(matlist, params);
    % determine how much missing data there is in seconds
    missing(qq) = (sum(isnan(map.ccVar))/length(map.ccVar)) * (t2-t1);
    fprintf('  %1.0fs missing data for %s\n', missing(qq), ID{qq});
  catch
    fprintf('  WARNING: no data found for %s\n', ID{qq});
    missing(qq) = -1;
  end
end

% update on available data
fprintf('%i triggers have no missing data\n', sum(missing==0));

% triggers with no msising data
alldata = missing==0;

% trigger index number
idx = 1:N;

% initialize background estimation struct
bknd=0;

% apply requested cuts and print output files
for rr=1:length(T)
  cut = missing<=T(rr) & missing~=-1 & R>=1;
  cutidx = idx(cut);
  fid1 = fopen(['astro_' num2str(T(rr)) '.txt'], 'w+'); 
  fid2 = fopen(['jobfile_' num2str(T(rr)) '.txt'], 'w+'); 
  fid3 = fopen(['triggers_' num2str(T(rr)) '.txt'], 'w+'); 
  fid4 = fopen(['grb_' num2str(T(rr)) '.tex'], 'w+');
  row = 1;
  for tt=1:sum(cut)
    ss = cutidx(tt);
    fprintf(fid1, '%s %i %1.2f %1.2f %1.2f\n', ...
      ID{ss}, gps(ss), ra(ss), dec(ss), dur(ss));
    fprintf(fid2, '%i %i %i %i\n', tt, gps(ss)+t1, gps(ss)+t2, t2-t1);
    fprintf(fid3, '%i %i %1.3f %1.3f\n', tt, gps(ss), ra(ss), dec(ss));
    fprintf(fid4, '    %i & %s & %i & %1.2f & %1.2f & %1.1f & ', ...
      row, ID{ss}, gps(ss), ra(ss), dec(ss), dur(ss));
    row = row + 1;
    if alldata(ss)
      fprintf(fid4, 'yes');
    else
      fprintf(fid4, 'no');
    end
    % if requested include p-values in the table
    if doPVal
      [pval snr bknd fluence] = cal_pvalue(bknd, gps(ss), search_str);
%
%      fprintf(fid4, '& %5.2g & %1.0f & %1.1f', fluence, snr, 100*pval);
tmp = sprintf('%1.2e', fluence);
tmp = regexprep(tmp, 'e+', '\\times10^{');
tmp = regexprep(tmp, '+', '');
tmp = regexprep(tmp, '{0', '{');
tmp = ['$' tmp '}$'];
tmp = regexprep(tmp, '\\times10\^\{0\}', '');
fprintf(fid4, '& %s & %1.0f & %1.1f', tmp, snr, 100*pval);
%
      % record the data in arrays too
      PVAL(tt)=pval;
      SNR(tt)=snr;
      fluence(tt)=fluence;
    end
    fprintf(fid4, ' \\\\\\hline\n');
  end
  fclose(fid1);
  fclose(fid2);
  fclose(fid3);
  fclose(fid4);
end

% info about loudest event
fprintf('The loudest event was %s with SNR=%3.1f and p=%1.2e.\n', ...
  ID{SNR==max(SNR)}, max(SNR),  PVAL(SNR==max(SNR)));
trials = length(SNR);
p50 = 1 - (1-PVAL(SNR==max(SNR)))^trials;
fprintf('After trial factors: p=%1.2e.\n', p50);

% save the entire workspace as backup
save('find_triggers.mat');
