function stamp_pvalues_calc(results_path, output_dir)
% function stamp_pvalues_calc(results_path, output_dir)
% calculates the p-value vs SNR plot
% For time-shifts
%   results_path = '/home/stamp/background/results';
%   output_dir = '/home/stamp/background/pvalues/TS1'
% For Monte Carlo
%  results_path = '/home/stamp/background/results_mc'
%  output_dir = '/home/stamp/background/pvalues/MC'

% make sure the output file exists
if ~exist(output_dir)
   system(['mkdir ' output_dir]);
end

% find the input files (created by clustermap)
results_path_mats = dir([results_path '/*.mat']);

% initialize arrays
jobnumbers = [];
snrs = [];

% loop over mat files each of which is a noise realization trial
fprintf('Loading data...\n');
for i=1:length(results_path_mats)
   % load data
   map_stats=load([results_path '/' results_path_mats(i).name]);
   % parse job files to keep track of jobs
   file_name = strrep(results_path_mats(i).name,'.','_');
   file_name_split = regexp(file_name,'_','split');
   % record job number
   jobnumbers(i) = str2num(file_name_split{3});
   % record SNR of loudest cluster
   snrs(i) = map_stats.stoch_out.max_SNR;
   % keep track of progress
   if mod(i,1000)==0 | i == length(results_path_mats)
      fprintf('%d/%d load\n',i,length(results_path_mats));
   end
end

fprintf('Calculating p-values...\n');
% make a histogram of SNR
bins = min(snrs):1:max(snrs);
bar_graph=hist(snrs,bins);

% plot histogram of SNR
%figure;
%bar(bins,bar_graph,'r');
%xlabel('SNR')
%ylabel('Counts');
%pretty;
%print('-dpng',[output_dir '/background.png']);
%print('-depsc2',[output_dir '/background.eps']);
%close;

% sort SNR values
snrs_sorted = sort(snrs,'ascend');
% determine spacing of FAP plot
step = 1/length(snrs_sorted);
% calculate p-values in increments of step
pvals = step:step:1;
pvals = fliplr(pvals);

% plot p-value vs SNR
%figure;
%semilogy(snrs_sorted,pvals,'r');
%xlabel('SNR')
%ylabel('p');
%pretty;
%print('-dpng',[output_dir '/p_value.png']);
%print('-depsc2',[output_dir '/p_value.eps']);
close;

% save the results
save([output_dir '/background.mat']);
