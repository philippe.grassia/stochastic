===============================================================================
1. Pre-preproessing
===============================================================================
* First, run 

  run_prep4bknd.m

which calls prep4bknd (stamp SVN) to create a pseudo jobfile:

  pseudojobs.txt

This pseudo jobfile breaks S5H1L1 data into 1500s chunks.

* Now run preproc like this

  preproc('params_mc.txt', 'pseudojobs.txt', jobNumber)
  preproc('params_ts.txt', 'pseudojobs.txt', jobNumber)

for Monte Carlo and time-shift data respectively.  These jobs are run on 
condor.  The resulting output mat files are stored here:

  /home/stamp/stampmatfiles/

===============================================================================
2. Processing
===============================================================================
* Now that the mat files are created, run

  stamp_bknd_clustermap_wrapper.m

to analyze the data.  The resulting output files are stored here

  /home/stamp/background/results/

===============================================================================
3. Post-processing
===============================================================================

* First, run stamp_pvalues_calc to harvest SNR files and calculate p-value vs
SNR plot.  Then...

* stamp_pvalues_plot.m: calculates the p-value vs SNR plot.  It combines MC and
TS on a single plot.  Also, it calculates the detection threshold given T=5000
trials.

