function stamp_pvalues_plot
% function stamp_pvalues_plot
% code by Michael Coughlin with comments and edits by Eric Thrane
% This code should be run in ldas-pcdev1:~stamp/.  Before you run this code
% you must run stamp_pvalues_calc in order to generate background.mat files 
% that are read in by stamp_pvalues_plot.m.
%
% It makes two plots: background.png is a histogram of SNR and p_value.png is
% a plot of p-value vs. SNR.  Both plots indicate the FAP = 1/5000 threshold
% for an interesting event.  In order to change the FAP, modify the T variable.

% define number of trials for an interesting event FAP=1/T
T = 5000;

% define paths to the p-value data for time-shifts (TS1) and Monte Carlo (MC)
results_path{1} = '/home/stamp/background/pvalues/TS1';
results_path{2} = '/home/stamp/background/pvalues/MC';

% define path to output plots
output_dir = '/home/stamp/background/pvalues/plots';

% make sure output directory exists
if ~exist(output_dir)
   system(['mkdir ' output_dir]);
end

% loop over data files containing SNR and p-values
for i=1:length(results_path)
   % load data
   data(i) = load([results_path{i} '/background.mat']);
   % calculate FAP=1/5000 detection threshold
   L = length(data(i).snrs_sorted);
   idx = floor(L/T);
   th(i) = data(i).snrs_sorted(end-idx);
end

% plot a histogram of SNR
figure;
hold all;
bar(data(1).bins,data(1).bar_graph,'b');
bar(data(2).bins,data(2).bar_graph,'g');
plot([th(1) th(1)], [0 max(data(1).bar_graph)], 'r:');
plot([th(2) th(2)], [0 max(data(2).bar_graph)], 'k:');
hold off;
hleg1 = legend('TS1','MC');
set(hleg1,'Location','NorthWest');
set(hleg1,'Interpreter','none');
xlabel('SNR')
ylabel('Counts');
title(['SNR(fap=1/' num2str(T) ') = ' num2str(th(1))], 'fontsize', 20);
pretty;
print('-dpng', [output_dir '/background']);
print('-depsc2', [output_dir '/background']);
close;

% make a plot of p-value vs SNR
figure;
semilogy(data(1).snrs_sorted, data(1).pvals, 'b', 'linewidth', 2);
hold all;
semilogy(data(2).snrs_sorted, data(2).pvals, 'g--', 'linewidth', 2);
yval = 1/T;
plot(th(1), yval, 'bs', 'MarkerFaceColor', 'b');
plot(th(2), yval, 'go', 'MarkerFaceColor', 'g');
hold off;
hleg1 = legend('TS1','MC');
set(hleg1,'Location','NorthWest');
set(hleg1,'Interpreter','none');
xlabel('SNR')
ylabel('p');
title(['SNR(fap=1/' num2str(T) ') = ' num2str(th(1))], 'fontsize', 20);
pretty;
print('-dpng', [output_dir '/p_value']);
print('-depsc2', [output_dir '/p_value']);
print('-depsc2', ['./p_value']);
close;

return;
