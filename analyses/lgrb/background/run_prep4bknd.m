function run_prep4bknd
% function run_prep4bknd
% Michael Coughlin and Eric Thrane: prepares psuedo jobfile for background
% study using prep4bknd.

% the time-shift size in seconds (used to label output files)
shift_size = 1;

% define STAMP mat prefix:
%   HL = Hanford-Livingston
%   SID = stochastic intermediate data
%   nspi9 = 9 segements per interval
%   df1_dt1: deltaF = 1Hz and deltaT = 1s
%   ts = time-shifted data
filePrefix = ['HL-SID_nspi9_df1_dt1_ts' num2str(shift_size)];

% define where to put the STAMP mat files
params.inmats = ['/home/stamp/stampmatfiles/' filePrefix];

% the start-end times of the S5H1L1 jobfile
params.startGPS = 816065659;
params.endGPS = 877591542;

% the size of the on-source region
params.jobdur = 1500;

% the name of the "pseudo-job" file which is used when clustermap is run in
% params.bkndstudy==true mode
params.bkndout = 'pseudojobs.txt';

% call prep4bknd in STAMP 2 svn to create the pseudo jobfile.
prep4bknd(params);

return;
