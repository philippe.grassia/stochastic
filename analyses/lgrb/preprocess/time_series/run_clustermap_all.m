function stoch_out = run_clustermap_all(job)
% function stoch_out = run_clustermap_all(job)
%
% Eric Thrane: this script is a wrapper for clustermap.  It performs no
% injections.

% hard-code matapps path
matappspath = '/home/ethrane/matapps/';

% condorize
stoch_out.start=true;
job = strassign(job);

% output file prefix and location
% lgrb_v3 run out of ethrane/ after review changes
% lgrb_v4 uses time-domain injections with findtrack==false
% lgrb_v5 uses time-domain injections with findtrack==true
str ='lgrb_v5';
output_dir = '/home/ethrane/stamp_results/upperlimits_v5';

% initialize default parameters
params = stampDefaults;

% jobfile and triggerfile for Swift triggers even with missing data
jobsFile = [matappspath '/analyses/lgrb/triggers/jobfile_500.txt'];

% load trigger file for triggers even if data is missing
q = load([matappspath '/analyses/lgrb/triggers/triggers_500.txt']);

% get ra and dec from trigger file
params.ra = q(job, 3);
params.dec = q(job, 4);
trigger_gps = q(job, 2);

% zero-lag data
params.inmats = ...
  '/home/ethrane/HL-SID_nspi9_df1_dt1_zl/HL-SID_nspi9_df1_dt1_zl';

% frequency range--------------------------------------------------------------
params.fmin = 100;
params.fmax = 1200;

% do not save mat files or plots
params.saveMat=false;
params.savePlots=false;

% notches
params = mask_S5H1L1_1s1Hz(params);

% remove notches outside of observation band
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove>=params.fmin);
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove<=params.fmax);

% random seed set with GPS time
params.seed = -1;
params.jobNumber=job;

% search parameters
params = burstegardDefaults(params);

% add findtrack params
params.burstegard.findtrack = true;
params.burstegard.rr = 50;

% glitch cut
params.glitchCut = true;
params.doCoincidentCut = true;

% load and parse jobfile
f = load(jobsFile);
dur = f(job, 4);
start = f(job, 2);
stop = start+dur;

% prepare output file path
output_file_path_base = [output_dir '/search'];
if ~exist(output_file_path_base)
  system(['mkdir ' output_file_path_base]);
end
% next check the subdirectory named with the GPS time
output_file_path = [output_file_path_base '/'  num2str(trigger_gps)];
if ~exist(output_file_path)
  system(['mkdir ' output_file_path]);
end

% define output file and make sure it doesn't already exist
ofile = [output_file_path '/' str '_' num2str(trigger_gps) '.mat'];
if exist(ofile)
  fprintf('%s already exists; moving on\n', ofile); 
else
    % run clustermap
    fprintf('running clustermap...\n');
    stoch_out=clustermap0(params, start, stop);
    out.snr = stoch_out.max_SNR;

  % save output file            
  save(ofile, 'stoch_out', 'out');
end

return;
