function params = readParamsFromFile(paramsFile)
%
%  readParamsFromFile --- read in search parameters from a file
%
%  readParamsFromFile(paramsFile) reads in search parameters from a
%  file, returning the parameters in a structure.
%
%  Assumes parameters are given by name/value pair.
%
%  Routine written by Joseph D. Romano, John T. Whelan, Vuk Mandic.
%  Contact Joseph.Romano@astro.cf.ac.uk, john.whelan@ligo.org and/or
%  vmandic@ligo.caltech.edu.  Edited by Eric Thrane and Shivaraj Kandhasamy for
%  STAMP
%
%  Note: this file is organized into different sections:
%    * backward compatibility
%    * things for stochastic.m and STAMP
%    * things for stochastic.m only
%    * things for STAMP only
%  If you add additional parameters, please add them to the appropriate section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%EHT: stamp variables
global STAMP;
 
%% read in name/value pairs
[names,values] = ...
  textread(paramsFile, '%s %s\n', -1, 'commentstyle', 'matlab');

%% check that number of names and values are equal
if length(names)~=length(values)
  error('invalid parameter file');
end

%% loop over parameter names, assigning values to structure
for ii=1:length(names)

  switch names{ii}
    %--------------------------------------------------------------------------
    % for stochastic.m backward compatibility
    %--------------------------------------------------------------------------
    case 'doHighPass'
      params.doHighPass1 = str2num(values{ii});
      params.doHighPass2 = str2num(values{ii});

    case 'resampleRate'
      params.resampleRate1 = str2num(values{ii});
      params.resampleRate2 = str2num(values{ii});

    case 'bufferSecs'
      params.bufferSecs1 = str2num(values{ii});
      params.bufferSecs2 = str2num(values{ii});

    case 'ASQchannel'
      params.ASQchannel1 = values{ii};
      params.ASQchannel2 = values{ii};

    case 'framePath'
      params.framePath1 = values{ii};
      params.framePath2 = values{ii};

     case 'frameType'
      params.frameType1 = values{ii};
      params.frameType2 = values{ii};

    case 'frameDuration'
      params.frameDuration1 = str2num(values{ii});
      params.frameDuration2 = str2num(values{ii});

    case 'hannDuration'
      params.hannDuration1 = str2num(values{ii});
      params.hannDuration2 = str2num(values{ii});

    case 'nResample'
      params.nResample1 = str2num(values{ii});
      params.nResample2 = str2num(values{ii});

    case 'betaParam'
      params.betaParam1 = str2num(values{ii});
      params.betaParam2 = str2num(values{ii});

    case 'highPassFreq'
      params.highPassFreq1 = str2num(values{ii});
      params.highPassFreq2 = str2num(values{ii});

    case 'highPassOrder'
      params.highPassOrder1 = str2num(values{ii});
      params.highPassOrder2 = str2num(values{ii});

    case 'simOmegaRef'
      params.simOmegaRef1 = str2num(values{ii});
      params.simOmegaRef2 = str2num(values{ii});

    %--------------------------------------------------------------------------
    % for stochastic.m and STAMP
    %--------------------------------------------------------------------------
    case 'doShift1'
      params.doShift1 = str2num(values{ii});

    case 'doShift2'
      params.doShift2 = str2num(values{ii});

    case 'doBadGPSTimes'
      params.doBadGPSTimes = str2num(values{ii});

    case 'heterodyned'
      params.heterodyned = str2num(values{ii});

    case 'doOverlap'
      params.doOverlap = str2num(values{ii});

    case 'ifo1'
      params.ifo1 = values{ii};

    case 'ifo2'
      params.ifo2 = values{ii};

    case 'segmentDuration'
      params.segmentDuration = str2num(values{ii});

    case 'numSegmentsPerInterval'
      params.numSegmentsPerInterval = str2num(values{ii});

    case 'ignoreMidSegment'
      params.ignoreMidSegment = str2num(values{ii});

    case 'deltaF'
      params.deltaF = str2num(values{ii});

    case 'flow'
      params.flow = str2num(values{ii});

    case 'fhigh'
      params.fhigh = str2num(values{ii});

    case 'ShiftTime1'
      params.ShiftTime1 = str2num(values{ii});

    case 'ShiftTime2'
      params.ShiftTime2 = str2num(values{ii});

    case 'resampleRate1'
      params.resampleRate1 = str2num(values{ii});

    case 'resampleRate2'
      params.resampleRate2 = str2num(values{ii});

    case 'bufferSecs1'
      params.bufferSecs1 = str2num(values{ii});

    case 'bufferSecs2'
      params.bufferSecs2 = str2num(values{ii});

    case 'ASQchannel1'
      params.ASQchannel1 = values{ii};

    case 'ASQchannel2'
      params.ASQchannel2 = values{ii};

    case 'frameType1'
      params.frameType1 = values{ii};

    case 'frameType2'
      params.frameType2 = values{ii};

    case 'framePath1'
      params.framePath1 = values{ii};

    case 'framePath2'
      params.framePath2 = values{ii};

    case 'frameDuration1'
      params.frameDuration1 = str2num(values{ii});

    case 'frameDuration2'
      params.frameDuration2 = str2num(values{ii});

    case 'hannDuration1'
      params.hannDuration1 = str2num(values{ii});

    case 'hannDuration2'
      params.hannDuration2 = str2num(values{ii});

    case 'nResample1'
      params.nResample1 = str2num(values{ii});

    case 'nResample2'
      params.nResample2 = str2num(values{ii});

    case 'betaParam1'
      params.betaParam1 = str2num(values{ii});

    case 'betaParam2'
      params.betaParam2 = str2num(values{ii});

    case 'doHighPass1'
      params.doHighPass1 = str2num(values{ii});

    case 'doHighPass2'
      params.doHighPass2 = str2num(values{ii});

    case 'highPassFreq1'
      params.highPassFreq1 = str2num(values{ii});

    case 'highPassFreq2'
      params.highPassFreq2 = str2num(values{ii});

    case 'highPassOrder1'
      params.highPassOrder1 = str2num(values{ii});

    case 'highPassOrder2'
      params.highPassOrder2 = str2num(values{ii});

    case 'freqsToRemove'
      params.freqsToRemove = transpose(str2num(values{ii}));

    case 'nBinsToRemove'
      params.nBinsToRemove = transpose(str2num(values{ii}));

    case 'gpsTimesPath1'
      params.gpsTimesPath1 = values{ii};

    case 'gpsTimesPath2'
      params.gpsTimesPath2 = values{ii};

    case 'frameCachePath1'
      params.frameCachePath1 = values{ii};

    case 'frameCachePath2'
      params.frameCachePath2 = values{ii};

    case 'badGPSTimesFile'
      params.badGPSTimesFile = values{ii};

    case 'doFreqMask'
      params.doFreqMask = str2num(values{ii});

    %--------------------------------------------------------------------------
    % for stochastic.m only
    %--------------------------------------------------------------------------
    % directional analyses only ###############################################
    case 'doDirectional'
      params.doDirectional = str2num(values{ii});

    case 'doSphericalHarmonics'
      params.doSphericalHarmonics = str2num(values{ii});

    case 'doNarrowbandRadiometer'
      params.doNarrowbandRadiometer = str2num(values{ii});

    case 'doAllSkyComparison'
      params.doAllSkyComparison = str2num(values{ii});

    case 'SpHFreqIntFlag'
      params.SpHFreqIntFlag = str2num(values{ii});

    case 'SpHLmax'
      params.SpHLmax = str2num(values{ii});

    case 'useSkyPatternFile'
      params.useSkyPatternFile = str2num(values{ii});

    case 'SkyPatternFile'
      params.SkyPatternFile = values{ii};

    case 'SkyPatternRightAscensionNumPoints'
      params.SkyPatternRightAscensionNumPoints = str2num(values{ii});

    case 'SkyPatternDeclinationNumPoints'
      params.SkyPatternDeclinationNumPoints = str2num(values{ii});

    case 'maxCorrelationTimeShift'
      params.maxCorrelationTimeShift = str2num(values{ii});

    case 'gammaLM_coeffsPath'
      params.gammaLM_coeffsPath = values{ii};

    % intermediate data frames only ###########################################
    case 'intermediate'
       params.intermediate = str2num(values{ii});

    case 'intFrameCachePath'
       params.intFrameCachePath = values{ii};

    case 'doSidereal'
      params.doSidereal = str2num(values{ii});
    %##########################################################################

    % simulated data only #####################################################
    case 'doMonteCarlo'
      params.doMonteCarlo = str2num(values{ii});

    case 'doMCoffset'
      params.doMCoffset = str2num(values{ii});

    case 'doSimulatedPointSource'
      params.doSimulatedPointSource = str2num(values{ii});

    case 'doSimulatedSkyMap'
      params.doSimulatedSkyMap = str2num(values{ii});

    % NOTE: this option is not used with STAMP.
    % See instead: doDetectorNoiseSim
    case 'doSimulatedDetectorNoise'
      params.doSimulatedDetectorNoise = str2num(values{ii});

    case 'injChannel1'
      params.injChannel1 = values{ii};

    case 'injChannel2'
      params.injChannel2 = values{ii};

    case 'injPrefix1'
      params.injPrefix1 = values{ii};

    case 'injPrefix2'
      params.injPrefix2 = values{ii};

    case 'injFrameDuration1'
      params.injFrameDuration1 = str2num(values{ii});

    case 'injFrameDuration2'
      params.injFrameDuration2 = str2num(values{ii});

    case 'numTrials'
      params.numTrials = str2num(values{ii});

    case 'signalType'
      params.signalType = values{ii};

    case 'simOmegaRef1'
      params.simOmegaRef1 = str2num(values{ii});

    case 'simOmegaRef2'
      params.simOmegaRef2 = str2num(values{ii});

    case 'simulationPath'
      params.simulationPath = values{ii};

    case 'simulatedPointSourcesFile'
      params.simulatedPointSourcesFile = values{ii};

    case 'simulatedPointSourcesPowerSpec'
      params.simulatedPointSourcesPowerSpec = values{ii};

    case 'simulatedPointSourcesInterpolateLogarithmic'
      params.simulatedPointSourcesInterpolateLogarithmic = str2num(values{ii});

    case 'simulatedPointSourcesBufferDepth'
      params.simulatedPointSourcesBufferDepth = str2num(values{ii});

    case 'simulatedPointSourcesHalfRefillLength'
      params.simulatedPointSourcesHalfRefillLength = str2num(values{ii});

    case 'simulatedPointSourcesNoRealData'
      params.simulatedPointSourcesNoRealData = str2num(values{ii});

    case 'simulatedPointSourcesMakeIncoherent'
      params.simulatedPointSourcesMakeIncoherent = str2num(values{ii});

    case 'simulatedSkyMapFile'
      params.simulatedSkyMapFile = values{ii};

    case 'simulatedSkyMapFileType'
      params.simulatedSkyMapFileType = str2num(values{ii});

    case 'simulatedSkyMapFileNumBins'
      params.simulatedSkyMapFileNumBins = str2num(values{ii});

    case 'simulatedSkyMapInjectAsSpH'
      params.simulatedSkyMapInjectAsSpH = str2num(values{ii});

    case 'simulatedSkyMapConvertLmax'
      params.simulatedSkyMapConvertLmax = str2num(values{ii});

    case 'simulatedSkyMapConvertDeg'
      params.simulatedSkyMapConvertDeg = str2num(values{ii});

    case 'simulatedSkyMapInjectTimeDomain'
      params.simulatedSkyMapInjectTimeDomain = str2num(values{ii});

    case 'simulatedSkyMapPowerSpec'
      params.simulatedSkyMapPowerSpec = values{ii};

    case 'simulatedSkyMapInterpolateLogarithmic'
      params.simulatedSkyMapInterpolateLogarithmic = str2num(values{ii});

    case 'simulatedSkyMapBufferDepth'
      params.simulatedSkyMapBufferDepth = str2num(values{ii});

    case 'simulatedSkyMapHalfRefillLength'
      params.simulatedSkyMapHalfRefillLength = str2num(values{ii});

    case 'simulatedSkyMapNoRealData'
      params.simulatedSkyMapNoRealData = str2num(values{ii});

    case 'simulatedDetectorNoisePowerSpec1'
      params.simulatedDetectorNoisePowerSpec1 = values{ii};

    case 'simulatedDetectorNoisePowerSpec2'
      params.simulatedDetectorNoisePowerSpec2 = values{ii};

    case 'simulatedDetectorNoiseInterpolateLogarithmic'
      params.simulatedDetectorNoiseInterpolateLogarithmic=str2num(values{ii});

    case 'simulatedDetectorNoiseBufferDepth'
      params.simulatedDetectorNoiseBufferDepth = str2num(values{ii});

    case 'simulatedDetectorNoiseHalfRefillLength'
      params.simulatedDetectorNoiseHalfRefillLength = str2num(values{ii});

    case 'simulatedDetectorNoiseNoRealData'
      params.simulatedDetectorNoiseNoRealData = str2num(values{ii});

    case 'doInjFromFile1'
      params.doInjFromFile1 = str2num(values{ii});

    case 'doInjFromFile2'
      params.doInjFromFile2 = str2num(values{ii});

    case 'injGPSTimesPath1'
      params.injGPSTimesPath1 = values{ii};

    case 'injGPSTimesPath2'
      params.injGPSTimesPath2 = values{ii};

    case 'injFrameCachePath1'
      params.injFrameCachePath1 = values{ii};

    case 'injFrameCachePath2'
      params.injFrameCachePath2 = values{ii};

    case 'injScale1'
      params.injScale1 = str2num(values{ii});

    case 'injScale2'
      params.injScale2 = str2num(values{ii});
    %##########################################################################

    case 'doConstTimeShift'
      params.doConstTimeShift = str2num(values{ii});

    case 'doCombine'
      params.doCombine = str2num(values{ii});

    case 'writeResultsToScreen'
      params.writeResultsToScreen = str2num(values{ii});

    case 'writeStatsToFiles'
      params.writeStatsToFiles = str2num(values{ii});

    % "Naive" sigmas are those calculated from the PSD of the segment
    %  being analyzed, rather than with the sliding PSD estimator
    case 'writeNaiveSigmasToFiles'
      params.writeNaiveSigmasToFiles = str2num(values{ii});

    case 'writeSpectraToFiles'
      params.writeSpectraToFiles = str2num(values{ii});

    case 'writeSensIntsToFiles'
      params.writeSensIntsToFiles = str2num(values{ii});
      
    case 'writeCoherenceToFiles'
      params.writeCoherenceToFiles = str2num(values{ii});

    case 'writeCohFToFiles'
      params.writeCohFToFiles = str2num(values{ii});

    case 'writeOptimalFiltersToFiles'
      params.writeOptimalFiltersToFiles = str2num(values{ii});

    case 'writeOverlapReductionFunctionToFiles'
      params.writeOverlapReductionFunctionToFiles = str2num(values{ii});

    case 'writeCalPSD1sToFiles'
      params.writeCalPSD1sToFiles = str2num(values{ii});

    case 'writeCalPSD2sToFiles'
      params.writeCalPSD2sToFiles = str2num(values{ii});

    case 'doTimingTransientSubtraction1'
      params.doTimingTransientSubtraction1 = str2num(values{ii});

    case 'doTimingTransientSubtraction2'
      params.doTimingTransientSubtraction2 = str2num(values{ii});

    case 'TimingTransientFile1'
      params.TimingTransientFile1 = values{ii};

    case 'TimingTransientFile2'
      params.TimingTransientFile2 = values{ii};

    case 'site1'
      params.site1 = values{ii};

    case 'site2'
      params.site2 = values{ii};

    case 'azimuth1'
      params.azimuth1 = str2num(values{ii});

    case 'azimuth2'
      params.azimuth2 = str2num(values{ii});

    case 'alphaExp'
      params.alphaExp = str2num(values{ii});

    case 'fRef'
      params.fRef = str2num(values{ii});

    case 'maxSegmentsPerMatfile'
      params.maxSegmentsPerMatfile = str2num(values{ii});

    case 'useSignalSpectrumHfFromFile'
      params.useSignalSpectrumHfFromFile = str2num(values{ii});

    case 'IMScramblePhase'
      params.IMScramblePhase = str2num(values{ii});

    case 'HfFile'
      params.HfFile = values{ii};

    case 'HfFileInterpolateLogarithmic'
      params.HfFileInterpolateLogarithmic = str2num(values{ii});

    case 'UnphysicalTimeShift'
      params.UnphysicalTimeShift = str2num(values{ii});

    case 'minMCoff'
      params.minMCoff = str2num(values{ii});

    case 'maxMCoff'
      params.maxMCoff = str2num(values{ii});

    case 'ConstTimeShift'
      params.ConstTimeShift = str2num(values{ii});

    case 'maxDSigRatio'
      params.maxDSigRatio = str2num(values{ii});

    case 'minDSigRatio'
      params.minDSigRatio = str2num(values{ii});

    case 'minDataLoadLength'
      params.minDataLoadLength = str2num(values{ii});

    %% Begin KLUDGE because fshift not available in frames
    case 'fbase1'
      params.fbase1 = str2num(values{ii});

    case 'fbase2'
      params.fbase2 = str2num(values{ii});
    %% End KLUDGE

    case 'alphaBetaFile1'
      params.alphaBetaFile1 = values{ii};

    case 'alphaBetaFile2'
      params.alphaBetaFile2 = values{ii};

    case 'calCavGainFile1'
      params.calCavGainFile1 = values{ii};

    case 'calCavGainFile2'
      params.calCavGainFile2 = values{ii};

    case 'calResponseFile1'
      params.calResponseFile1 = values{ii};

    case 'calResponseFile2'
      params.calResponseFile2 = values{ii};

    % for STAMP see instead outputfiledir and outputfilename
    case 'outputDirPrefix'
      params.outputDirPrefix = values{ii};
    %
    case 'outputFilePrefix'
      params.outputFilePrefix = values{ii};

    case 'jobsFileCommentStyle'
      params.jobsFileCommentStyle = values{ii};
   
    % mat file created by master_cache.m with cache struct
    case 'cacheMat'
      params.cacheMat = values{ii};

    % mat file created by seg_list.m with segs struc
    case 'segsMat'
      params.segsMat = values{ii};  

    case 'verbose'
      params.verbose = str2num(values{ii});

    case 'saveGPS_diff'
      params.saveGPS_diff = str2num(values{ii});

    case 'fft1dataWindow'
      params.fft1dataWindow = str2num(values{ii});
 
    case 'fft2dataWindow'
      params.fft2dataWindow = str2num(values{ii});

    %--------------------------------------------------------------------------
    % for STAMP only
    %--------------------------------------------------------------------------
    case 'batch'
      params.batch = str2num(values{ii});

    case 'mapsize'
      params.mapsize = str2num(values{ii});

    case 'pp_seed'
      params.pp_seed = str2num(values{ii});

    % used for time-series injections in preproc
    case 'stampinj'
      params.stampinj = str2num(values{ii});

    case 'stamp.ra'
      params.pass.stamp.ra = str2num(values{ii});

    case 'stamp.decl'
      params.pass.stamp.decl = str2num(values{ii});

    case 'stamp.startGPS'
      params.pass.stamp.startGPS = str2num(values{ii});

    case 'stamp.file'
      params.pass.stamp.file = values{ii};

    case 'stamp.alpha'
      params.pass.stamp.alpha = str2num(values{ii});

    % used in preproc for STAMP studies
    case 'doDetectorNoiseSim'
      params.doDetectorNoiseSim = str2num(values{ii});

    case 'doDifferentNoise'
      params.doDifferentNoise = str2num(values{ii});

    case 'DetectorNoiseFile'
      params.DetectorNoiseFile = values{ii};
  
    case 'DetectorNoiseFile2'
      params.DetectorNoiseFile2 = values{ii};

    case 'sampleRate'
      params.sampleRate = str2num(values{ii});

    % more params for stochmap addition
    case 'stochmap'
      params.stochmap = str2num(values{ii});

    case 'startGPS'
      params.startGPS = str2num(values{ii});

    case 'endGPS'
      params.endGPS = str2num(values{ii});

    case 'fixAntennaFactors'
      params.fixAntennaFactors = str2num(values{ii});

    case 'storemats' % for storing mat files 
      params.storemats = str2num(values{ii});

    case 'outputfiledir'
      params.outputfiledir = values{ii};

    case 'outputfilename'
      params.outputfilename = values{ii};

    % for large time shift 
    case 'largeShift'
      params.largeShift = str2num(values{ii});
 
    case 'largeShiftTime2'
      params.largeShiftTime2 = str2num(values{ii});
  
    % new cache file method (must for large time shift and preproc with
    % start and end GPS)
    case 'cacheFile'
      params.cacheFile = values{ii};

    % STAMP-PEM
    case 'user'
      params.user = values{ii};

    case 'darmChannel'
      params.darmChannel = values{ii};

    case 'dirPath'
      params.dirPath = values{ii};

    case 'matappsPath'
      params.matappsPath = values{ii};

    case 'publicPath'
      params.publicPath = values{ii};

    case 'runName'
      params.runName = values{ii};

    case 'executableDir'
      params.executableDir = values{ii};

    case 'fscanPath'
      params.fscanPath = values{ii};

    otherwise
    % do nothing 

    end % switch

end %% loop over parameter names

%EHT: PASS GLOBAL VARIABLE
fprintf('CAUTION: BYPASSING PARAMETER FILE WITH STAMP GLOBAL VARIABLE\n');
params.pass.stamp.ra = STAMP.ra;
params.pass.stamp.decl = STAMP.decl;
params.pass.stamp.startGPS = STAMP.startGPS;
params.pass.stamp.file = STAMP.file;
params.pass.stamp.alpha =STAMP.alpha;

return
