function D = normalized_distance(F)
% function D = normalized_distance(F)
% Eric Thrane.  May 28, 2013
% This function takes as its input fluence F in ergs/cm^2.  The output is 
% distance in Mpc normalized for a face-on source with E = 0.1 msun.

% convert 1 Msun to ergs using E=mc^2 and (1e7 ergs/J) and msun=2e30 kg 
msun = 2e30 * (3e8)^2 * 1e7;

% Assume 0.1 msun is converted into GWs.
% Factor of 2.5 beaming for face-on emission (see note by Patrick Sutton) to
% convert energy to isotropic equivalent energy.
% http://tinyurl.com/boeqat5
EVP = 2.5 * 0.1 * msun;

% convert fluence from ergs/cm^2 to ergs/Mpc^2.
Mpc2cm = 3.09e24;
F0 = F * Mpc2cm^2;

% Calculate normalized distance limit.
%   F = E/4*pi*D^2 implies
%   D = sqrt(E/4*pi*F)
D = sqrt(EVP./(4*pi*F0));

return
