* First, we soft link to triggers/jobfile0.txt.  Then we run cachefiles.pl in
order to create cachefiles for this jobfile.  Since there are only 29 triggers
in jobfile0.txt, this creates a very limited number of cachefiles with
different numbering than ../S5H1L1_full_run_1605_v2.txt.

* In principle, we could use jobfile0.txt for our investigation.  However, to 
save time, we will run make_jobfiles.m, which creates 100 new jobfiles in 
jobfiles/ of the form jobfiles/jobfile_11.txt.  Each jobfile is equivalent to 
jobfile0.txt except each job has been cropped to be just 300s long.  The start
time of these little job files is chose randomly.

* Next we get the parameter file ready: params_zl_inj.txt.  We start by copying
over ../params_zl.txt.

     - We add six parameters under the heading of "injection."
     % injection
     stampinj true
     % job=14 example
     %stamp.ra 17.348
     %stamp.decl 69.345
     %stamp.startGPS 855882650
     %stamp.file /home/ethrane/analyses/lgrb/benchmark/adi_D.dat
     %stamp.alpha 5e-7

     - Note that some of these parameters are commented out because they're
     actually passed with the STAMP global struct (see below).

     - We changed the outputfilename to HL-SID_nspi9_df1_dt1_zl_inj and the
     outputfiledir is now 
     /usr1/ethrane/
     The mat files are actually saved in a subdirectory of /usr1/ethrane/ using
     the global STAMP variable.

     - We modified the cachefile paths to point to
     /home/ethrane/analyses/lgrb/preprocess/time_series/cachefiles/

* Next we created run_clustermap.m...

     - The code saves results to
     /home/ethrane/stamp_results/upperlimits_v5
     
     - It points to the mini jobfiles described above, e.g:
     jobfiles/jobfile_13.txt

     - It runs preproc before calling clustermap.  Preproc calls a modified 
     version of savemap that saves the matfiles here:
     injmats/1/1/HL-SID_nspi9_df1_dt1_zl_inj/
     It is modified to more easily pass the output file location using a global
     variable called GLOBALDIR.

     - Preproc also uses a modified version of readParamsFromFile, which gets
     stamp.pass variables from a global variable called STAMP.

* run_clustermap_all calculates SNR for the triggers for which not all data is 
available.  It uses a different jobfile from run_clustermap to get all 1500s of
data associated with the each trigger.

* local copy of clustermap0.m records stoch_out.fluence, 
