function cal_limit2(job, waveform)
% function cal_limit2(job, waveform)
% by Eric Thrane and Michael Coughlin
% job = the trigger number (not to be confused with the trial number below 
%   denoted by ii)
% waveform = the name of the waveform, which is used to label the clustermap
%   output files; e.g., waveform = 'adi_D';
% matappspath = '/home/ethrane/matapps';
%
% This script harvests the output files from the sensitivity study and 
% calculates upper limits.

% hard-code matappspath
matappspath = '/home/ethrane/matapps/';

% make things condor-friendly
job = strassign(job);
if job==0
  return;
end

% set confidence limit to 90% follow S6-VSR23 short GRB paper
cl = 0.90;

% load trigger file for triggers with no missing data
q = load([matappspath '/analyses/lgrb/triggers/triggers_0.txt']);
% get trigger time from trigger file
trigger_gps = q(job, 2);

% define a prefix to label file names
% lgrb_v3 run out of ethrane/ after review changes
% lgrb_v4 uses time-domain injections
% lgrb_v5 uses time-domain injections with findtrack==true
%str = 'lgrb_v4';
str = 'lgrb_v5';

% print arguments to screen
fprintf('Running limits code for waveform %s and job %d\n', waveform, job);

% this is where the output .png files will go
% note, the summary .mat files will go with the other mat files (see below)
%output_file_path_base = '/home/ethrane/stamp_results/upperlimits/';
%output_file_path_base = '/home/ethrane/stamp_results/upperlimits_v4/';
output_file_path_base = '/home/ethrane/stamp_results/upperlimits_v5/';

% make sure output directories exist
% e.g., ~/stamp_results/upperlimits/plots/adi_D/829887393/
% first, check the 'base" directory named with the waveform
plotdir = [output_file_path_base 'plots/' waveform '/' num2str(trigger_gps)];
if ~exist(plotdir)
 system(['mkdir ' plotdir]);
end

% get a list of matfiles created from the injection study (run_clustermap)
mat_files=dir( ['/home/ethrane/stamp_results/upperlimits_v5/' waveform '/' ...
  num2str(trigger_gps) '/' str '_' num2str(trigger_gps) '_*.mat']);

% determine the number of mat files (the number of injection trials)
N = length(mat_files);

% loop over mat files to obtain an array or SNR for different trials and 
% for for different injection strengths (alpha)
for ii=1:N
  try
    q = load(['/home/ethrane/stamp_results/upperlimits_v5/' waveform '/' ...
    num2str(trigger_gps) '/' mat_files(ii).name]);
    data(ii,:) = q.out.snr;
  catch
    fprintf('problem with %s\n', mat_files(ii).name);
    keyboard
  end
end
alpha = q.out.alpha;
alpha_n = length(alpha);

% get numerr (tolerance) from power injection files
tmp = load(['/home/ethrane/stamp_results/upperlimits/' waveform '/' ...
  num2str(trigger_gps) '/limits_lgrb_v3.mat' ]);
%snr = tmp.snr;
numerr = tmp.numerr;

% get snr from full 1500s on-source window with no injection
x = load(['/home/ethrane/stamp_results/upperlimits_v5/search/' num2str(trigger_gps) '/' str '_' num2str(trigger_gps) '.mat']);
snr = x.stoch_out.max_SNR;

% loop over injection values
for kk=1:alpha_n
  % calculate the fraction of entries with SNR > the measured SNR (denoted
  % lower-case snr.  note: we multiply snr by 1+5*numerr to take into account
  % numerical rounding error
  frac(kk) = sum(data(:, kk)>snr*(1+5*numerr)) / length(data(:, kk));
end

% calculate upper limit: the value of alpha for which at least cl=90% of the
% injections exceed the measured value of SNR (denoted lower-case snr)
% This calculation is applicable when frac(alpha) is monotonically increasing.
% ul = min(alpha(frac>=cl));
%
% A more general calculation allows for downward dips in frac(alpha):
% Get an array of ones and zeros telling us which alpha values pass the
% frac>=cl cut.
flag = frac>=cl;
% Determine the largest alpha value to fail the cut.
midx = max(find(flag==0));
% Set the upper limit to be the next highest alpha value.
try
  ul = alpha(midx+1);
catch
  % make sure that the injections provided are sufficient to calculate the UL
  fprintf('problem with job=%i, waveform=%s\n', job, waveform);
  error('Unable to set upper limit.');
end

% print results without saving outputfiles (and compare with previous results)
r = load(['/home/ethrane/stamp_results/upperlimits_v4/adi_D/' ...
  num2str(trigger_gps) '/limits_lgrb_v4.mat']);
fprintf('alpha < %1.1e (%1.0f%%)\n', ul, 100*(ul/r.ul));
fprintf('snr = %1.2f\n', snr);

% calculate the distance upper limit: distance goes like 1/sqrt(power)
ul_distance = sqrt(1/ul);

% waveforms created for 10 kpc -- convert to Mpc
% The units for ul_distance before conversion we can call dkpc for tens of kpc.
% To convert to Mpc we note that there are (10 kpc / 1 dkpc) and 
% (1 Mpc / 1000 kpc).
fprintf('scaling distance assuming 10 kpc waveform\n');
ul_distance = ul_distance * (10 / 1000);

% calculate fluence (units = ergs/cm^2)
F = cal_fluence([matappspath '/analyses/lgrb/benchmark/' waveform '.dat'], ul);

% save summary results
save([output_file_path_base waveform '/' num2str(trigger_gps) ...
  '/limits_' str '.mat']);

% make a diagnostic plot frac vs alpha
figure;
semilogx(alpha, frac, '-kx', 'linewidth', 2);
hold on;
semilogx(tmp.alpha(2:end), tmp.frac(2:end), '-bo');
hold on;
semilogx([tmp.alpha(2) tmp.alpha(end)], [cl cl], 'r--');
hold on;
semilogx([ul ul], [0 1], 'g:');
xlabel('\alpha');
ylabel(['fraction exceeding th=' snr]);
axis([tmp.alpha(2) tmp.alpha(end) 0 1]);
grid on;
title(['snr = ' num2str(snr) ', N = ' num2str(N)], 'FontSize', 20);
pretty;
print('-dpng', [plotdir '/alpha_' str]);

return
