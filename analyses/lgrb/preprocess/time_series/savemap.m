function savemap(map, params)
% function savemap(map, params)
% This function saves map, params and pp(defined inside) in a mat file.
% If params.startGPS and params.endGPS are provided, crop the data
% accordingly
%
% Routine by S. Kandhasamy, E. Thrane

%EHT: define global variable
global GLOBALDIR

% If startGPS and endGPS are provided select corresponding array indices
if(isfield(params,'startGPS') & isfield(params,'endGPS'))
  cut = map.segstarttime >= params.startGPS & ...
        map.segstarttime <= params.endGPS - params.segmentDuration;
else
  cut = true(length(map.segstarttime),1);
end

% crop the map, if necessary
map.segstarttime = map.segstarttime(cut);
map.cc = map.cc(:,cut);
map.sensInt = map.sensInt(:,cut);
map.ccVar = map.ccVar(cut);
map.naiP1 = map.naiP1(:,cut);
map.naiP2 = map.naiP2(:,cut);
map.P1 = map.P1(:,cut);
map.P2 = map.P2(:,cut);
map.start = map.segstarttime(1);
map.dur = map.segstarttime(end)+params.segmentDuration-map.segstarttime(1);

% other meta data
pp.flow = params.flow;
pp.fhigh = params.fhigh;
pp.deltaF = params.deltaF;
pp.w1w2bar = params.w1w2bar;
pp.w1w2squaredbar = params.w1w2squaredbar;
pp.w1w2ovlsquaredbar = params.w1w2ovlsquaredbar;

% define directory postfix
dirtime = floor(map.start/10000);
folderpostfix = num2str(dirtime);
%EHT: params.outputfilepath = [params.outputfiledir params.outputfilename ...
%EHT:                        '-' folderpostfix];
params.outputfilepath = [params.outputfiledir GLOBALDIR '/']; ...

% create a new directory if necessary
if ~exist(params.outputfilepath)
  system(['mkdir ' params.outputfilepath]);
end
params.outputfilepath = [params.outputfilepath params.outputfilename ...
                        '-' folderpostfix];
if ~exist(params.outputfilepath)
  system(['mkdir ' params.outputfilepath]);
end

% save the mat file
save([params.outputfilepath '/' params.outputfilename '-' ...
     num2str(map.start) '-' num2str(map.dur) '.mat'], 'map','params','pp');

return
