function quickpage
% function quickpage

% prefixes---------------------------------------------------------------------
% homedir is the path where the upper limit plots are
homedir = '/home/ethrane';
% webdir is the url extension pointing to the plots
webdir = '~ethrane/files';
% prefix navigates through additional subdirectories
prefix = [homedir '/stamp_results/upperlimits_v5/'];

% open summary html
%fid = fopen('quick_summary.html', 'w+');
fid = fopen('quick_summary_v5.html', 'w+');
fprintf(fid, '<HTML>');

% list of waveforms
waves = {'adi_B', 'adi_C', 'adi_D'};

% loop over waveforms
for ii=1:length(waves)
  % get a list of output directories
  list = dir([prefix 'plots/'  waves{ii} '/']);
  % the first two elements in list.name are . and ..
  % loop over list elements (GPS times)
  for jj=3:length(list)
    % reconstruct filename
    filename = [prefix 'plots/' waves{ii} '/' list(jj).name ...
      '/alpha_lgrb_v5.png'];
    % swap homedir and webdir for webpage
    filename = regexprep(filename, homedir, webdir);
    % print entry in the html file
%    fprintf(fid, '<a href="https://ldas-jobs.ligo.caltech.edu/%s"><img src="https://ldas-jobs.ligo.caltech.edu/%s", width=400></a><br>\n', filename, filename);
    fprintf(fid, '<a href="https://ldas-jobs.ligo-la.caltech.edu/%s"><img src="https://ldas-jobs.ligo-la.caltech.edu/%s", width=400></a><br>\n', filename, filename);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRINT ADDITIONAL DIAGNOSTIC INFORMATION SUCH AS R
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% read in results files and parse for useful information
%filename2 = [prefix waves{ii} '/' list(jj).name '/limits_lgrb_v5.mat'];
%q = load(filename2);
%trigger_gps = str2num(list(jj).name);
%R = cal_R(q.stoch_out.params, trigger_gps);
%fprintf(fid, '<br>R=%1.2f<br>\n', R);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % print the filename
    fprintf(fid, '<br>%s <br><br><br>\n', filename);
  end
end

% close summary html
fprintf(fid, '</HTML>');
fclose(fid);

return
