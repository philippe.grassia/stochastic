function marginalize_calerr(job, waveform)
% function marginalize_calerr(job, waveform)
% Marginalize over calibration error, lambda, modeled as a Gaussian with unit
% mean and width = 0.164 (see below).
% Then determine how the fraction of events surpassing snr0 changes after 
% marginalizing over lambda.
%   frac(alpha_true) = int dlambda frac(alpha_meas | lambda) p(lambda)

% calibration error = sqrt(err_H1^2 + err_L1^2)
% OLD ref: https://dcc.ligo.org/DocDB/0011/T1000227/001/T1000227-v1.pdf
% NEW ref: http://arxiv.org/pdf/1007.3973v1.pdf
% OLD: err = 0.164;
% NEW:
err = 0.1776;

% hard-code matappspath
matappspath = '/home/ethrane/matapps/';

% load trigger file for triggers with no missing data
qq = load([matappspath '/analyses/lgrb/triggers/triggers_0.txt']);
% get trigger time from trigger file
trigger_gps = qq(job, 2);

% define calibration error prior
lambda = 1-3*err : err/3 : 1+3*err;
p_lambda = exp(-(lambda-1).^2 / (2*err^2) );
p_lambda = p_lambda/sum(p_lambda);

% load mat file
q=load(['~/stamp_results/upperlimits_v5/adi_D/' num2str(trigger_gps) ...
  '/limits_lgrb_v5.mat']);

% loop over lambda
for ii=1:length(lambda)
  % calculate p(alpha|lambda): first shift alpha
  alpha = q.alpha * (1+err);

  % next, interpolate frac in the original alpha values and multiply by weight
  frac(ii,:) = p_lambda(ii) * interp1(alpha, q.frac, q.alpha);
end

% calculate average frac by summing (marginalizing)
fracbar = sum(frac, 1);

% calculate upper limit: the value of alpha for which at least cl=90% of the
% injections exceed the measured value of SNR (denoted lower-case snr)
% This calculation is applicable when frac(alpha) is monotonically increasing. 
% ul = min(alpha(frac>=cl));
%
% A more general calculation allows for downward dips in frac(alpha):
% Get an array of ones and zeros telling us which alpha values pass the 
% frac>=cl cut.
flag = fracbar>=q.cl;
% Determine the largest alpha value to fail the cut.
midx = max(find(flag==0));

% Set the upper limit to be the next highest alpha value.
try
  ul = alpha(midx+1);
catch
  % make sure that the injections provided are sufficient to calculate the UL
  fprintf('problem with job=%i, waveform=%s\n', job, waveform); 
  error('Unable to set upper limit.');
end

% print upper limit
fprintf('ul=%1.2e (%2.2fx change)\n', ul, ul/q.ul);

return
