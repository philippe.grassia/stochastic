function ul_table_for_paper
% function ul_table_for_paper
% Eric Thrane
% This function reads in the upper limit results and creates a latex table file
% that can be pasted into the paper.  That file is called ul_table.tex.

% calibration error factor and DC corrections
%EHT: May 13, 2013 
%EHT Ferr = 1.4;
Ferr = 1.4 * 1.074 * 0.960;
fprintf('################\n');
fprintf('Increasing fluence limits by %1.1fx according to results from marginalize_calerr and DC calibration factors.\n', Ferr);
fprintf('Decreasing distance limits by %1.1fx according to results from marginalize_calerr and DC calibration factors.\n', 1/sqrt(Ferr));
fprintf('################\n');

% hard-code matapps path
matappspath = '/home/ethrane/matapps/';

% waveforms
waveforms = {'adi_B', 'adi_C', 'adi_D'};

% the names given to the waveforms in the paper
names = {'a', 'b', 'c'};

% top directory
top = '/home/ethrane/stamp_results/upperlimits_v5/';

% prepare output file
fid = fopen('ul_table.tex', 'w+');

% load astro list in order to obtain GRB names, gps trig times, and job numbers
str = read_doc([matappspath 'analyses/lgrb/triggers/astro_0.txt']);
str = regexprep(str, '\n', ' ');
trigdata = regexp(str,' ','split');
trigdata = trigdata(2:end);
ncol = 5;
jj = 1;
for ii=1 : ncol : length(trigdata)
  ID{jj} = trigdata{ii};
  gps(jj) = str2num(trigdata{ii+1});
  jobs(jj) = jj;
  jj = jj + 1;
end

% loop over jobs, load data, print upper limits
for jj=1:length(waveforms)
  for ii=1:length(gps)
    waveform = waveforms{jj};
    name = names{jj};
    try
      % load upper limit data
      mat_data = load([top waveform '/' num2str(gps(ii)) ...
        '/limits_lgrb_v5.mat']);
      % use latex notation for fluence (obsolete)
%      F = latexstr(mat_data.F); % (obsolete)
      F = mat_data.F;
      FF(ii,jj) = F;
      D(ii,jj) = mat_data.ul_distance;
      % print results to screen
%EHT: incorporating calibration error
%EHT      fprintf('%i %s %1.1f %1.3f\n' , ...
%EHT        jobs(ii), waveform, mat_data.ul_distance, F);
      fprintf('%i %s %s %s\n' , ...
        jobs(ii), waveform, latex_str(mat_data.ul_distance), ...
        latex_str(Ferr*F));
      fprintf('successfully loaded job=%i, waveform=%s\n', jobs(ii), waveform);
    catch
      keyboard
      % pause the script for debugging if any data is not available
      fprintf('cannot load job=%i, waveform=%s\n', jobs(ii), waveform);
      fprintf('  %s\n', [top waveform '/' num2str(gps(ii)) ...
        '/limits_lgrb_v5.mat'])
    end
  end
end

% create latex file
for ii=1:length(gps)
  fprintf(fid, '%s', ID{ii});
  for jj=1:length(waveforms)
%EHT: incorporating calibration error
%EHT    fprintf(fid, ' & %1.1f', D(ii,jj));
%EHT: March 29, 2013 = excluding distance limits to focus on fluence.
%    fprintf(fid, ' & %s', latex_str(D(ii,jj)/sqrt(Ferr)));
  end
  for jj=1:length(waveforms)
%EHT: incorporating calibration error
%EHT    fprintf(fid, ' & %1.3f', FF(ii,jj));
    fprintf(fid, ' & %s', latex_str(Ferr*FF(ii,jj)));
  end
%EHT May 28, 2013--------------------------------------------------------------
  for jj=1:length(waveforms)
    d = normalized_distance(Ferr*FF(ii,jj));
    fprintf(fid, ' & %1.1f', d);
  end
%------------------------------------------------------------------------------
  fprintf(fid, ' \\\\\\hline\n');
end

% clean up
fclose(fid);

% save a mat file of the workspace
save('ul_table.mat');

return;
