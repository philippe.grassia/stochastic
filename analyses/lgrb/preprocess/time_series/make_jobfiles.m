function make_jobfiles
% function make_jobfiles

%------------------------------------------------------------------------------
% number of injection trials
T = 100;

% maximum injection duration
injdur = 300;
%------------------------------------------------------------------------------

% load in jobfile
f = load('jobfile_0.txt');
njobs = size(f,1);

% determine job length
joblength = f(1,4);
jobs = f(:,1);
starts = f(:,2);

% keep track of offsets
T0 = [];

% loop over trials
for ii=1:T
  % pick a random start time offset between 0 and joblength-injdur
  done = false;
  while ~done
    t0 = round(rand*(joblength-injdur));
    % has this time already used?
    if sum(t0==T0)>=1
      % yes: keep trying
    else
      % no: we have not already used this t0
      done=true;
      T0 = [T0 t0];
    end
  end
  % calculate new start times
  newstarts = starts + t0;
  % calculate new stop times
  newstops = newstarts + injdur;
  % print jobfile
  fid=fopen(['jobfiles/jobfile_' num2str(ii) '.txt'], 'w+'); 
  for jj=1:njobs
    fprintf(fid, '%i %i %i %i\n', jobs(jj), newstarts(jj), newstops(jj), ...
      injdur);
  end
  fclose(fid);
end

