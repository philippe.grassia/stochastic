function stoch_out = clustermap(params, startGPS, endGPS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function stoch_out = clustermapmap2(params, startGPS, endGPS)
% E.g., 
%   params = stampDefaults;
%   clustermap(params, 816088197, 816089507);
% output (depending on what is requested in params struct):
%   diagnostic outputfile: map.mat
%   diagnostic plots: snr.eps, y_map.eps, sig_map.eps + .png versions
%   additional output if requested by search algorithm plug-ins
%   stoch_out: a struct containing information about injections and/or search
%     algorithms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% E. Thrane, S. Kandhasamy, S. Dorsher, S. Giampanis, M. Coughlin, 
% T. Prestegard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;

% make sure start and end GPS are defined 
if(nargin == 3)
  params.startGPS = strassign(startGPS);
  params.endGPS = strassign(endGPS);
else
  error('start and end GPS must be specified.');
end

% locate or create mats for the requested data
% if mat files containing ft-information already available, use that
% (for backward compatibility)
if params.matavailable
  [matlist, params] = findmats(params);
  [map, params, pp] = loadmats(matlist, params);
else % otherwsie create ft-maps
  [map, params, pp] = createmats(params);
end

% check values of params for self-consistency
params = paramCheck(params);

% if we perform a background study, remove missing data; useful when using 
% *.mat files from preproc
if params.bkndstudy
  map = bkndstudy(map, params);
end

% apply mask
if params.doStampFreqMask
  map = maskmaps(map, params);
end

% override gps times
if params.override_gps~=0
  fprintf('\n----------------OVERRIDING GPS TIMES!----------------\n\n');
  gps0 = params.override_gps;
  map.segstarttime = map.segstarttime - map.segstarttime(1) + gps0;
  endGPS = endGPS + (gps0 - startGPS);
  startGPS = gps0;
end

% initialize ccSpec struct and other radiometer variables
ccSpec.data = map.cc;
ccSpec.flow = params.fmin;
ccSpec.deltaF = pp.deltaF;
sensInt.flow = params.fmin;
sensInt.deltaF = pp.deltaF;
sensInt.data = map.sensInt;
ccVar = map.ccVar;
midGPSTimes = map.segstarttime + params.segmentDuration/2;
[det1 det2] = ifo2dets(params);

% get sky location
if params.skypatch
  source = getskylocation(params);
else
  source = [params.ra params.dec];
end

% the number of searches executed
ss=0;

% loop over search directions
for kk=1:size(source,1)
  % initialize ft-map struct
  map = freshmap(map);

  % run the radiometer; calculate ft-maps
  [map] = ccSpecReadout_stamp(det1, det2, ... 
    midGPSTimes, source(kk,:), ccSpec, ccVar, sensInt, params, pp, map);

  % define SNR map
  map.snr = map.y ./ map.sigma;
  map.snrz = map.z ./map.sigma;

  if params.glitchinj
     map = glitchinj(params,map);
  end

  % initialize injection struct
  inj.init = [];

  % loop over injection trials
  % Note: if no injections are performed, then alpha_n is set to one and
  % inj_trials is set to one.
  for tt=1:params.inj_trials*params.alpha_n
    fprintf('trial %i/%i\n', tt, params.inj_trials*params.alpha_n);
    % add injection if requested
    if params.powerinj
       [map0, inj] = powerinj3(params, map, source(kk,:), tt, inj);    
    else
       map0 = map;
    end

    % calculate Xi stastic
    map0 = calXi(params, map0);
    
    % apply glitch cut if requested in params struct
    % cutcols records the ft-map columns (segments) that are cut
    [map0, stoch_out.cutcols{kk}] = glitchCut(params, map0);

    % special variables for use in PEM studies
    if params.stamp_pem
      map0.snr = abs(map0.snr); map0.y = abs(map0.y);
      pem.P1 = map0.P1; pem.P2 = map0.P2;
      pem.f = map0.f; pem.segstarttime = map0.segstarttime;
      save([params.outputfilename '_psd.mat'],'pem');
    end
   
    % save mat file(s) -- one for each search direction
    if params.saveMat
      save([params.outputfilename '_' num2str(kk) '.mat'], ...
	   'map0','params','pp'); 
    end

    % broadband radiometer comparison
    if params.doRadiometer
      y_rad = sum(map0.y.*map0.sigma.^-2, 1) ./ sum(map0.sigma.^-2, 1);
      y_sig = sum(map0.sigma.^-2, 1).^-0.5;
      fprintf('radiometer=\n');
      fprintf('%1.4e +/- %1.4e\n', y_rad, y_sig);
    end

    % perform clustering algorithms if requested  
    [stoch_out, ss] = searches(map0, params, kk, ss, stoch_out);
 
    % Add search direction to map struct.
    map0.source = source(kk,:);

    % Make sure max cluster is saved; compatible with searches that use a
    % cluster struct (burstegard, BurstCluster, Box search).
    if (params.doBurstegard | params.doBoxSearch | params.doClusterSearch ...
      | params.doStochtrack)
      if ~exist('max_cluster')
        max_cluster.snr_gamma = -1;
      end
      if (max_cluster.snr_gamma < stoch_out.cluster.snr_gamma)
        max_cluster = stoch_out.cluster;
	    max_map = map0;
      end
    end
  end
end

% Save 'max' structs in stoch_out struct.
% Also set 'map' to be 'max_map' for plotting purposes.
if (params.doBurstegard | params.doBoxSearch | params.doClusterSearch | ...
  params.doStochtrack)
  map = max_map;
  % Print search result summary to screen.
  fprintf(['\nSearch results:\nMax. SNR = %.3f, found at (ra,dec) =' ...
    '(%.2f h, %.2f deg.)\n'], max(stoch_out.max_SNR), map.source(1), ...
    map.source(2));
  stoch_out.cluster = max_cluster;
  stoch_out.cluster.source = max_map.source;
end
if params.returnMap
  stoch_out.map = map;
end

% Plots for search direction corresponding to maximum SNR.
if params.savePlots
  saveplots(params, map);
  if params.doBurstegard
    burstegard_plot(max_cluster, map, params);
  end
end

% record overall map sensitivity
stoch_out.ccSigBar = sqrt(mean(ccVar(~isnan(ccVar))));

% store param values
stoch_out.params = params;

% store source locations %% Useful for all-sky analyses
stoch_out.sources = source;

% calculate cluster fluence
stoch_out.fluence = cluster_fluence(map, stoch_out.cluster.reconMax, params);

% warn the user if diagnostic tools are on
if params.diagnostic
  fprintf('CAUTION: diagnostic tools on.\n'); 
end

% close all open plots
close all;
fprintf('Elapsed time = %f seconds.\n', toc);

return
