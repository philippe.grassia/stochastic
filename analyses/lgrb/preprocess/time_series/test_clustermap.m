function stoch_out = test_clustermap(waveform, job, trial);
% function stoch_out = test_clustermap(waveform, job, trial);
%
% Eric Thrane: this script is a wrapper for clustermap.  It performs power
% injections.  The output files are saved in a directory marked by the trigger
% GPS.

% hard-code matapps path
matappspath = '/home/ethrane/matapps/';

% condorize
stoch_out.start=true;
job = strassign(job);
trial = strassign(trial);
if trial==0
  return;
end

% define global variable
global GLOBALDIR;
GLOBALDIR = [num2str(job) '/' num2str(trial) '/'];
global STAMP;

% output file prefix and location
% lgrb_v3 run out of ethrane/ after review changes
% lgrb_v4 uses time-domain injections
%str ='lgrb_v4';
%output_dir = '/home/ethrane/stamp_results/upperlimits_v4';

% initialize default parameters
params = stampDefaults;

% jobfile and triggerfile for Swift triggers with no missing data
jobsFile=[matappspath '/analyses/lgrb/preprocess/time_series/jobfiles/jobfile_' num2str(trial) '.txt'];

% load trigger file for triggers with no missing data
q = load([matappspath '/analyses/lgrb/triggers/triggers_0.txt']);

% get ra and dec from trigger file
params.ra = q(job, 3);
params.dec = q(job, 4);
trigger_gps = q(job, 2);

% input mat directory created on the fly: this must agree with the value chosen
% in the preproc parameter file
%params.inmats = ['/home/ethrane/analyses/lgrb/preprocess/' ...
%  'time_series/injmats/' GLOBALDIR 'HL-SID_nspi9_df1_dt1_zl_inj'];
params.inmats = ['/usr1/ethrane/' GLOBALDIR 'HL-SID_nspi9_df1_dt1_zl_inj'];

% frequency range--------------------------------------------------------------
params.fmin = 100;
params.fmax = 1200;
%params.fmax = 300;

% do not save mat files or plots
params.saveMat=false;
params.savePlots=true;

% notches
params = mask_S5H1L1_1s1Hz(params);

% remove notches outside of observation band
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove>=params.fmin);
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove<=params.fmax);

% random seed set with GPS time
params.seed = -1;
params.jobNumber=job;

% search parameters
params = burstegardDefaults(params);

% glitch cut
params.glitchCut = true;
params.doCoincidentCut = true;

% load and parse jobfile
f = load(jobsFile);
dur = f(job, 4);
start = f(job, 2);
stop = start+dur;

% make sure preproc output directory exists
if exist(params.inmats)==0
  mkdir(params.inmats);
end

% get alpha values from power injection results
oresults = load(['/home/ethrane/stamp_results/upperlimits/' waveform '/' ...
  num2str(trigger_gps) '/limits_lgrb_v3.mat']);
% define alpha array to begin (one point before) first point that frac>=95%
%idx_start = min(find(oresults.flag==1)) - 1;
% alpha array ends after confirming three points with frac>=95%
%idx_stop = max(find(oresults.flag==0)) + 1 + 3;
% save to params so that they are recorded in the out struct
%out.alpha = oresults.alpha(idx_start:idx_stop);
%fprintf('%i injection values...\n', length(out.alpha));

% other option: use all values of alpha
out.alpha = oresults.alpha;
fprintf('%i injection values...\n', length(out.alpha));

%  out.alpha = 0.6371e-6;
  out.alpha = 0;
  % loop over injection values
  for ii=1:length(out.alpha)
    fprintf('# injection %i/%i #\n', ii, length(out.alpha));
    % pass global stamp variable
    STAMP.ra = params.ra;
    STAMP.decl = params.dec;
    STAMP.startGPS = start + 10;
    STAMP.file = ['/home/ethrane/analyses/lgrb/benchmark/' waveform '.dat'];
%    STAMP.alpha = 5e-7;
    STAMP.alpha = out.alpha(ii);

    % run preproc
    paramfile = ...
      '/home/ethrane/analyses/lgrb/preprocess/time_series/params_zl_inj.txt';
    preproc(paramfile, jobsFile, job)
    % run clustermap
    fprintf('running clustermap...\n');
    stoch_out=clustermap(params, start, stop);
    out.snr(ii) = stoch_out.max_SNR;
  end

return;
