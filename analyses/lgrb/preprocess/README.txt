* First, run limit_jobs.txt to make S5H1L1_full_run_1605.txt, which limits the 
maximum job duration to ~1605s.

Then, shorten the following jobs by hand, which were previously identified as
missing a few seconds of LLO data.  To see which jobs changed and how, 

   diff S5H1L1_full_run_1605.txt S5H1L1_full_run_1605_v2.txt

Then, 

   preproc('params_ts.txt', 'S5H1L1_full_run_1605_v2.txt', jobnumber)

on condor.

* use copyMats.pl to harvest mat files.

* To make sure that here are no missing/unexpected mat files, run validate.m.

* To make sure that there are no corrupted mat files, run corruption.mat.
It will take some time to run, so run it in the background.  When it's done,
check the output file corruption.mat to make sure that corrupt has size=0.
