function validate
% function validate
% This function determines all the expected mat files names based on a jobfile,
% the matfile duration, the overlap duration, and the buffer time (all user-
% defined parameters).  It then scans the disk to find the actual names of all
% mat files in some user-specified directory.  Finally, it compares the two 
% lists to determine if there are any missing files (expected but now found) 
% and if there any unexpected files.

% user-defiend parameters------------------------------------------------------
% load jobfile
f = load('S5H1L1_full_run_1605_v2.txt');

% location of mat files
%str = '/home/stamp/stampmatfiles/HL-SID_nspi9_df1_dt1_ts1';
str = '/home/ethrane/HL-SID_nspi9_df1_dt1_ts1/HL-SID_nspi9_df1_dt1_ts1';

% file duration
fdur=200;

% file overlap
folap = 1;

% 12s of buffer data required for each job:
% 2 buffer seconds + 4 overlapping segments
btime = 6;
%------------------------------------------------------------------------------

% parse jobfile to get start and stop times
starts = f(:,2);
stops = f(:,3);

% initialize file start and stop times
fstarts = [];
fstops = [];

% calculate expected file start times
for ii=1:length(starts)
%%%for ii=1:999
  % determine mat file start times
  tmp_start = [(starts(ii)+btime) : (fdur-folap) : (stops(ii) - btime - 1)];
  tmp_stop = [tmp_start(2:end)+1 stops(ii)-btime];
  fstarts = [fstarts tmp_start];
  fstops = [fstops tmp_stop];
end

% no single-segment matfiles
cut = fstops-fstarts==1;
fstarts(cut) = [];
fstops(cut) = [];

% status report
fprintf('finished parsing jobfile; now examining mat files...\n');

% first and last file prefixes from jobfile:
jmin = (starts(1) - mod(starts(1),10000) ) / 10000;
jmax = (starts(end) - mod(starts(end),10000) ) / 10000;

% initialize file index
index = 1;

% obtain a list of the actual mat files
for jj=jmin:jmax
%%%for jj=jmin:81893
  matdir = [str '-' num2str(jj)];
  all_files = dir([matdir '/*.mat']);
  for kk=1:length(all_files)
    file = all_files(kk).name;
    % determine GPS start time of mat file from file name
    [temp gps_string] = regexp(file, '.*-(.*)-[0-9]*\.mat', ...
      'match', 'tokens');
    matgps_start(index) = str2num(char(gps_string{1}));
    % determine duration
    [temp dur_string] = regexp(file, '.*-.*-([0-9])*\.mat', ...
      'match', 'tokens');
    matdur(index) = str2num(char(dur_string{1}));
    % increment index
    index = index + 1;
  end
end

% determine if there are any missing files
[mtimes, midx] = setdiff(fstarts, matgps_start);

% determine if there are any unexpected files
[utimes, uidx] = setdiff(matgps_start, fstarts);

% print summary
fprintf('%i missing files\n', length(mtimes));
fprintf('%i unexpected files\n', length(utimes));

% Create a single number beginning with the GPS start time, followed by a
% decimal, then followed by the duration.  We will use this to determine if
% the start times and durations are all in agreement.
% The calculation assumes that each mat file duration is less than 1000.
if any(fstops-fstarts >= 1000)
  error('validate.m assumes mat files with duration<1000.');
end
tmp1 = fstarts+(fstops-fstarts)/1000;
tmp2 = matgps_start+(matdur)/1000;
[mtmp, mtmpidx] = setdiff(tmp1, tmp2);
[utmp, utmpidx] = setdiff(tmp2, tmp1);
fprintf('%i missing files.durations\n', length(mtmp));
fprintf('%i unexpected files.durations\n', length(utmp));

% check for duplicates
if length(tmp1)~=length(unique(tmp1))
  fprintf('%i duplicate files expected\n', length(tmp1)-length(unique(tmp1)));
end
if length(tmp2)~=length(unique(tmp2))
  fprintf('%i duplicate files found\n', length(tmp2)-length(unique(tmp2)));
end

% save output file
save('validate');

return
