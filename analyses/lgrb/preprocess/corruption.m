function corruption
% function corruption
% This function tries loading every mat file to make sure it loads without any
% problems.

% record results on the fly
fid=fopen('./corruption.txt','w+'); 

% load jobfile
f = load('S5H1L1_full_run_1605_v2.txt');

% parse jobfile to get start and stop times
starts = f(:,2);
stops = f(:,3);

% location of mat files
%str = '/home/ethrane/HL-SID_nspi9_df1_dt1_ts1/HL-SID_nspi9_df1_dt1_ts1';
str = '/home/ethrane/HL-SID_nspi9_df1_dt1_zl/HL-SID_nspi9_df1_dt1_zl';

% index of corrupt files
bb=1;
% index of files that load with no problems
aa=0;

% first and last file prefixes from jobfile
jmin = (starts(1) - mod(starts(1),10000) ) / 10000;
jmax = (starts(end) - mod(starts(end),10000) ) / 10000;

% obtain a list of the actual mat files
for jj=jmin:jmax
%%%for jj=jmin:81893
%%%for jj=81650
  matdir = [str '-' num2str(jj)];
  all_files = dir([matdir '/*.mat']);
  for kk=1:length(all_files)
    file = all_files(kk).name;
    try
      load([matdir '/' file]);
       aa=aa+1;
    catch
      fprintf(fid, '%s is corrupt\n', file);
      corrupt{bb} = file;
      bb=bb+1;
    end
  end
end

% save output file
save('corruption.mat');

% clean up
fclose(fid)

return
