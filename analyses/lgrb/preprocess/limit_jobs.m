function limit_jobs
% function limit_jobs
% Rewrites a jobfile as a longer jobfile where jobs beyond a certain duration
% are broken into smaller jobs.

% user-defiend parameters------------------------------------------------------
% load jobfile
f = load('S5H1L1_full_run.txt');

% parse jobfile
starts = f(:,2);
stops = f(:,3);

% file duration
fdur=200;

% 12s of buffer data required for each job:
% 2 buffer seconds + 4 overlapping segments
btime = 6;

% overlap time
folap = 1;

% segment duration
segdur = 1;

% max job length
mdur = 8*(fdur-folap)+2*btime + segdur;

%------------------------------------------------------------------------------
% open new jobfile
fid=fopen(['S5H1L1_full_run_' num2str(mdur) '.txt'],'w+'); 

% initialize index
idx = 1;

% loop over jobs to create new job start times and durs
for ii=1:length(starts)
  % new start times
  nstarts = starts(ii) : mdur-2*btime-segdur : stops(ii);
  % new durations
  ndurs = [mdur*ones(1, length(nstarts)-1) stops(ii)-nstarts(end)];
  % new stop times
  nstops = nstarts + ndurs;
%-------------------------------------------------------------
% Make sure there are no very short jobs
  if ndurs(end) < 20
    nstarts(end) = [];
    nstops(end-1) = nstops(end);
    nstops(end) = [];
    ndurs(end) = [];
    ndurs(end) = nstops(end) - nstarts(end);
  end
%------------------------------------------------------------------------------
  % print to new jobfile
  for jj=1:length(nstarts)
    fprintf(fid, '%i %i %i %i\n', idx, nstarts(jj), nstops(jj), ...
      ndurs(jj));
    idx = idx + 1;
  end
end

fclose(fid);

return
