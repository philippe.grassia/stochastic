function params = default_params(params);
% function params = stochtrack_params

% injection: GRB070219 @ GPS = 855882630
params.ra = 17.348;
params.dec = 69.345;

if params.hstart < 1000000000
   params.fmin = 90;
   params.fmax = 256;
   params.fmax = 300;
else
   params.fmin = 10;
   params.fmax = 150;
end

params.yMapScale = 2e-46;
%EHT: use bestAntennaFactors
fprintf('Using bestAntennaFactors=false.\n');
params.bestAntennaFactors = false;

% notches
params = mask_S5H1L1_1s1Hz(params);

% remove notches outside of observation band
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove>=params.fmin);
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove<=params.fmax);

if params.hstart < 1000000000
%   params.StampFreqsToRemove = [];
%   params.StampFreqsToRemove = [];
    params.StampFreqsToRemove = [170 178 186];
else
   params.StampFreqsToRemove = [];
   params.StampFreqsToRemove = [];
end

% random seed set with GPS time
params.seed = -1;

if params.hstart < 1000000000
   params.glitch.doCut = true;
   %params.glitch.doCut = false;
else
   %params.glitch.doCut = true;
end

% define stochtrack parameters-------------------------------------------------
params.clustermap.doStochtrackBezier = 1;
params.clustermap.doStochtrackCBC =1;
params.clustermap.doStochtrackECBC =1;
params.clustermap.doBurstegard = 1;

% ANTEPROC PARAMS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use anteproc'd data?
params.anteproc.loadFiles = true;

% Time-shift amount (if doing a "time-based" time-shift).
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;

% Do a jobfile time-shift?
params.anteproc.jobFileTimeShift = false;

% Load data from which job for each detector?
params.anteproc.jobNum1 = params.jobNumber;
params.anteproc.jobNum2 = params.jobNumber;

% Remove gaps in data?
params.anteproc.bkndstudy = false;

% How long are the maps supposed to be (in seconds)? The code double-checks
% the map size after removing gaps.
params.anteproc.bkndstudydur = 100;

return
