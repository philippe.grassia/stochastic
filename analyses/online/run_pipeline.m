function run_pipeline(pipe_params)
% function run_pipeline(pipe_params)
% Given a STAMP online params struct, runs the pipeline. This pipeline
% generates mat files and ft-maps using STAMP, running various search algorithms to
% search for structure. 
% Routine written by Michael Coughlin and Patrick Meyers
% Modified: 6/15/2014
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

createpath(pipe_params.outputPath);

jobNumber = 1;
jobFile = [pipe_params.outputPath '/jobfile.txt'];
fid = fopen(jobFile,'w+');
fprintf(fid,'1 %d %d %d\n',pipe_params.gpsStart,pipe_params.gpsEnd,pipe_params.gpsEnd-pipe_params.gpsStart);
fclose(fid);

jobsdir = [pipe_params.outputPath '/jobs/'];
createpath(jobsdir);
paramsdir = [pipe_params.outputPath '/params/'];
createpath(paramsdir);
cachedir = [pipe_params.outputPath '/cache/'];
createpath(cachedir);

pipe_params_hold = pipe_params;
pipe_params.gpsStart = pipe_params.gpsStart - 6;
pipe_params.gpsEnd = pipe_params.gpsEnd + 6;

if pipe_params.doBknd
   pipe_params.gpsStart = pipe_params.gpsStart - 100;
   pipe_params.gpsEnd = pipe_params.gpsEnd + 100;
end

try
   pipe_params.segmentDuration;
catch
   pipe_params.segmentDuration = 1;
end
pipe_params.deltaF = 1/pipe_params.segmentDuration;
try
   pipe_params.nsegs;
catch
   pipe_params.nsegs = 9;
end
try
   pipe_params.highPassFreq;
catch
   pipe_params.highPassFreq = 9;
end

run_cache(pipe_params);

for i = 1:length(pipe_params.ifos)
   pipe_params.ifo1 = pipe_params.ifos{i};

   pipe_params.outputfiledir = [pipe_params.outputPath '/matfiles/' pipe_params.ifo1 '/'];
   createpath(pipe_params.outputfiledir);
   
   paramsFile = anteproc_params(pipe_params.outputfiledir);
   % prepare a new paramfile
   out = read_doc(paramsFile);
   out = regexprep(out, 'xyz_outputfiledir',pipe_params.outputfiledir);
   out = regexprep(out, 'xyz_cachedir',cachedir);
   out = regexprep(out, 'xyz_ifo1', pipe_params.ifos{i});
   out = regexprep(out, 'xyz_channel1', pipe_params.channels{i});
   out = regexprep(out, 'xyz_frametype1', pipe_params.frameTypes{i});
   out = regexprep(out, 'xyz_segmentDuration', num2str(pipe_params.segmentDuration));
   out = regexprep(out, 'xyz_deltaF', num2str(pipe_params.deltaF));
   out = regexprep(out, 'xyz_numSegmentsPerInterval', num2str(pipe_params.nsegs));
   out = regexprep(out, 'xyz_highPassFreq', num2str(pipe_params.highPassFreq));

   paramsFile = [paramsdir '/params_' pipe_params.ifo1 '.txt'];
   % write new paramfile
   fid=fopen(paramsFile,'w+');
   fprintf(fid, '%c', out);
   fclose(fid);

   gps1 = load(sprintf('%s/cache/gpsTotal%s.1.txt',pipe_params.outputPath,pipe_params.ifo1(1)));
   gpsStartnew = pipe_params.gpsStart;
   gpsEndnew = pipe_params.gpsEnd;

   if gps1(1) > gpsStartnew
      gpsStartnew = gps1(1);
   end
   if gps1(2) < gpsEndnew
      gpsEndnew = gps1(2);
   end

   jobFile = [jobsdir '/jobfile_' pipe_params.ifo1 '.txt'];
   fid = fopen(jobFile,'w+');
   fprintf(fid,'1 %d %d %d\n',gpsStartnew,gpsEndnew,gpsEndnew-gpsStartnew);
   fclose(fid);

   fprintf('For %s, Original: %d-%d, Cache: %d-%d\n',...
      pipe_params.ifos{i},pipe_params.gpsStart,pipe_params.gpsEnd,gpsStartnew,gpsEndnew);

   [matGPSStart,matGPSEnd] = getStartEnd(pipe_params);

   if ~((matGPSStart-10 <= pipe_params.gpsStart) && (matGPSEnd+10 >= pipe_params.gpsEnd))
      anteproc(paramsFile,jobFile,jobNumber);
   end
end

pipe_params = pipe_params_hold;

if pipe_params.doClustermap

   pipe_params_hold = pipe_params;
   for i = 1:length(pipe_params.ifos)
      for j = 1:length(pipe_params.ifos)
         if i>=j
            continue
         end

         pipe_params = pipe_params_hold;
         pipe_params.ifo1 = pipe_params.ifos{i}; pipe_params.ifo2 = pipe_params.ifos{j};

         gpsStartnew = pipe_params.gpsStart;
         gpsEndnew = pipe_params.gpsEnd;

         pipe_params.outputfiledir = [pipe_params.outputPath '/matfiles/' pipe_params.ifo1 '/'];
         [matGPSStart,matGPSEnd] = getStartEnd(pipe_params);
         gps1 = [matGPSStart matGPSEnd];

         pipe_params.outputfiledir = [pipe_params.outputPath '/matfiles/' pipe_params.ifo2 '/'];
         [matGPSStart,matGPSEnd] = getStartEnd(pipe_params);
         gps2 = [matGPSStart matGPSEnd];

         if gps1(1) > gpsStartnew
            gpsStartnew = gps1(1);
         end
         if gps1(2) < gpsEndnew
            gpsEndnew = gps1(2);
         end

         if gps2(1) > gpsStartnew
            gpsStartnew = gps2(1);
         end
         if gps2(2) < gpsEndnew
            gpsEndnew = gps2(2);
         end

         gpsEndnew = gpsEndnew - 1;
         pipe_params.jobsFile = [jobsdir '/jobfile_' pipe_params.ifo1 pipe_params.ifo2 '.txt'];
         fid = fopen(pipe_params.jobsFile,'w+');
         fprintf(fid,'1 %d %d %d\n',gpsStartnew,gpsEndnew,gpsEndnew-gpsStartnew);
         fclose(fid);

         pipe_params.gpsStart = gpsStartnew;
         pipe_params.gpsEnd = gpsEndnew;

         pipe_params.timeshift = 0;
         run_clustermap_online(pipe_params);
         if pipe_params.doBknd
            for k = 1:pipe_params.nBknd
               pipe_params.timeshift = k;
         %      run_clustermap_online(pipe_params);
            end
         end   

         trig_post(pipe_params);

      end
   end
end

