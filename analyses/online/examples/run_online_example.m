
outputPath = '/home/mcoughlin/STAMP/ER5/online';

gpsStart = 1074839138;
gpsEnd = gpsStart + 100;
gpsEnd = gpsStart + 10000;

gpsEnd = 2000000000;

pipe_params = [];
pipe_params.outputPath = outputPath;
pipe_params.ifos = {'H1','L1','V1'};
pipe_params.channels = {'FAKE-STRAIN','FAKE-STRAIN','FAKE_h_16384Hz_4R'};
pipe_params.frameTypes = {'H1_ER_C00_L1','L1_ER_C00_L1','V1Online'};

%pipe_params.ifos = {'H1'};
%pipe_params.channels = {'FAKE-STRAIN'};
%pipe_params.frameTypes = {'H1_ER_C00_L1'};
pipe_params.doClustermap = 0;

pipe_params.gpsStart = gpsStart;
pipe_params.gpsEnd = gpsEnd;

run_online_ifo(pipe_params);
%run_pipeline(pipe_params);

