
outputPath = '/home/mcoughlin/STAMP/ER5/trigs';

trigs_file = '/home/mcoughlin/matapps/packages/stochastic/trunk/analyses/online/gracedb/trigs.txt';
%trigs_file = '/home/mcoughlin/matapps/packages/stochastic/trunk/analyses/online/gracedb/trig.txt';
trigs = load(trigs_file);

pipe_params = [];
pipe_params.outputPath = outputPath;
pipe_params.ifos = {'H1','L1','V1'};
pipe_params.channels = {'FAKE-STRAIN','FAKE-STRAIN','FAKE_h_16384Hz_4R'};
pipe_params.frameTypes = {'H1_ER_C00_L1','L1_ER_C00_L1','V1Online'};

pipe_params.trigger = 1;

run_postprocessing(pipe_params);

