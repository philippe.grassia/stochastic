
outputPath = '/home/mcoughlin/STAMP/S6/test';

trigs_file = '/home/mcoughlin/matapps/packages/stochastic/trunk/analyses/online/s6/trigs.txt';
%trigs_file = '/home/mcoughlin/matapps/packages/stochastic/trunk/analyses/online/gracedb/trig.txt';
trigs = load(trigs_file);

pipe_params = [];
pipe_params.outputPath = outputPath;
pipe_params.ifos = {'H1','L1'};
pipe_params.channels = {'LDAS-STRAIN','LDAS-STRAIN'};
pipe_params.frameTypes = {'H1_LDAS_C02_L2','L1_LDAS_C02_L2'};

pipe_params.trigger = 1;

run_postprocessing(pipe_params);


