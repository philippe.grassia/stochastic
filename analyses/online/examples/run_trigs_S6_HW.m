
outputPath = '/home/mcoughlin/STAMP/S6/HW';

trigs_file = '/home/mcoughlin/matapps/packages/stochastic/trunk/analyses/online/s6/hardwareInj.txt';
%trigs_file = '/home/mcoughlin/matapps/packages/stochastic/trunk/analyses/online/gracedb/trig.txt';
trigs = load(trigs_file);

pipeParamsFile = 's6_params_cbc_short.mat';

trig_analysis(pipeParamsFile,outputPath,gps,ra,dec);


for i = 1:length(trigs(:,1))

   %if trigs(i,1) ~= 1076894532
   %   continue
   %end

   trig_analysis(pipeParamsFile,outputPath,trigs(i,1),trigs(i,2),trigs(i,3));
end


