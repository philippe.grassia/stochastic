
outputPath = '/home/mcoughlin/STAMP/ER6/anteproc';

trigs_file = '/home/mcoughlin/matapps/packages/stochastic/trunk/analyses/online/gracedb/trigs.txt';
%trigs_file = '/home/mcoughlin/matapps/packages/stochastic/trunk/analyses/online/gracedb/trig.txt';
trigs = load(trigs_file);

pipe_params = [];
pipe_params.outputPath = outputPath;
pipe_params.ifos = {'H1','L1','V1'};
pipe_params.channels = {'FAKE-STRAIN','FAKE-STRAIN','FAKE_h_16384Hz_4R'};
pipe_params.frameTypes = {'H1_ER_C00_L1','L1_ER_C00_L1','V1Online'};

%pipe_params.ifos = {'H1','L1'};
%pipe_params.channels = {'FAKE-STRAIN','FAKE-STRAIN'};
%pipe_params.frameTypes = {'H1_ER_C00_L1','L1_ER_C00_L1'};

pipe_params.doClustermap = 1;
pipe_params.clustermap.doStochtrackBezier = 0;
pipe_params.clustermap.doStochtrackCBC =0;
pipe_params.clustermap.doBurstegard = 1;

for i = 1:length(trigs(:,1))

   if trigs(i,1) ~= 1074925302
      continue
   end

   pipe_params.triggerGPS = trigs(i,1);
   pipe_params.gpsStart = trigs(i,1) - 250;
   pipe_params.gpsEnd = pipe_params.gpsStart  + 300;
   pipe_params.ra = trigs(i,2); pipe_params.dec = trigs(i,3);

   run_pipeline(pipe_params);
end
