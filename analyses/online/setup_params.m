function [params,pipe_params] = setup_params(params,pipe_params);

try
   params.ra = pipe_params.ra;
   fprintf('Setting ra to %.10f\n',pipe_params.ra);
catch
end

try
   params.dec = pipe_params.dec;
   fprintf('Setting dec to %.10f\n',pipe_params.dec);
catch
end

params.anteproc.inmats1 = [pipe_params.outputPath '/matfiles/' pipe_params.ifo1 '/' ...
   pipe_params.ifo1(1) '-' pipe_params.ifo1 '_mats'];
params.anteproc.inmats2 = [pipe_params.outputPath '/matfiles/' pipe_params.ifo2 '/' ...
   pipe_params.ifo2(1) '-' pipe_params.ifo2 '_mats'];

params.jobsFile = pipe_params.jobsFile;
% Jobfile to use.
params.anteproc.jobfile = params.jobsFile;

try
   pipe_params.triggerGPS;
catch
   pipe_params.triggerGPS = -1;
end

try
   params.clustermap.doStochtrackBezier = pipe_params.clustermap.doStochtrackBezier;
catch
end
try
   params.clustermap.doStochtrackCBC = pipe_params.clustermap.doStochtrackCBC;
catch
end
try
   params.clustermap.doBurstegard = pipe_params.clustermap.doBurstegard;
catch
end

try
   pipe_params.timeshift;
catch
   pipe_params.timeshift = 0;
end

params.anteproc.timeShift2 = pipe_params.timeshift;

if pipe_params.timeshift == 0
   params.saveMat=true;
   params.savePlots=true;
else
   params.saveMat=false;
   params.savePlots=false;
end

% notches
params = mask_S5H1L1_1s1Hz_lonetrack(params);
% remove notches outside of observation band
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove>=params.fmin);
params.StampFreqsToRemove = ...
  params.StampFreqsToRemove(params.StampFreqsToRemove<=params.fmax);

params.StampFreqsToRemove = [];

