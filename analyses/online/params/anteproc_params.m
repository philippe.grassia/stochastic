function [anteprocParamsFile] = anteproc_params(paramdir)
% function stamp_pem_params(params)
% Given a STAMP params struct, creates a STAMP parameter file used by
% preproc / clustermap
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
anteprocParamsFile = [paramdir 'anteproc-params.txt'];
% Open parameter file for writing
fid=fopen(anteprocParamsFile,'w+');
% Write STAMP parameters
fprintf(fid,'% parameters for stochastic search (name/value pairs)\n');
fprintf(fid,'%\n');
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'\n');
fprintf(fid,'% flags for optional operations\n');
fprintf(fid,'doFreqMask false\n');
fprintf(fid,'doHighPass1 true\n');
fprintf(fid,'doOverlap true\n');
fprintf(fid,'\n');
fprintf(fid,'doSidereal false\n');
fprintf(fid,'\n');
fprintf(fid,'minDataLoadLength 200\n');
fprintf(fid,' \n');
fprintf(fid,'doBadGPSTimes false\n');
fprintf(fid,'%badGPSTimesFile \n');
fprintf(fid,'\n');

fprintf(fid,'doShift1 false\n');
fprintf(fid,'ShiftTime1 1\n');
fprintf(fid,'doShift2 false\n');
fprintf(fid,'ShiftTime2 0\n');
fprintf(fid,'\n');

fprintf(fid,'% ifo names\n');
fprintf(fid,'ifo1 xyz_ifo1\n');
fprintf(fid,'\n');
fprintf(fid,'% segment duration (sec)\n');
fprintf(fid,'segmentDuration xyz_segmentDuration\n');

fprintf(fid,'\n');
fprintf(fid,'% parameters for sliding psd estimation:\n');
fprintf(fid,'% numSegmentsPerInterval should be odd; ignoreMidSegment is a flag \n');
fprintf(fid,'% that allows you to ignore (if true) or include (if false) the \n');
fprintf(fid,'% analysis segment when estimating power spectra\n');
fprintf(fid,'numSegmentsPerInterval xyz_numSegmentsPerInterval\n');
fprintf(fid,'ignoreMidSegment true\n');
fprintf(fid,'\n');
fprintf(fid,'% freq resolution and freq cutoffs for CC statistic sum (Hz)\n');
fprintf(fid,'flow 10\n');

fprintf(fid,'fhigh 2000\n');

fprintf(fid,'deltaF xyz_deltaF\n');
fprintf(fid,'\n');

fprintf(fid,'% params for Omega_gw (power-law exponent and reference freq in Hz)\n');
fprintf(fid,'alphaExp 0\n');
fprintf(fid,'fRef 100\n');
fprintf(fid,'\n');
fprintf(fid,'% resample rate (Hz)\n');
fprintf(fid,'resampleRate1 4096\n');
fprintf(fid,'\n');
fprintf(fid,'% buffer added to beginning and end of data segment to account for\n');
fprintf(fid,'% filter transients (sec)\n');
fprintf(fid,'bufferSecs1 2\n');
fprintf(fid,'\n');
fprintf(fid,'% ASQ channel\n');
fprintf(fid,'ASQchannel1 xyz_channel1\n');
fprintf(fid,'\n');
fprintf(fid,'% frame type and duration\n');
fprintf(fid,'frameType1 xyz_frametype1\n');
fprintf(fid,'\n');
fprintf(fid,'% duration of hann portion of tukey window \n');
fprintf(fid,'% (hannDuration = segmentDuration is a pure hann window)\n');
fprintf(fid,'hannDuration1 xyz_segmentDuration\n');
fprintf(fid,'\n');
fprintf(fid,'% params for matlab resample routine\n');
fprintf(fid,'nResample1 10\n');
fprintf(fid,'betaParam1 5\n');
fprintf(fid,'\n');
fprintf(fid,'% params for high-pass filtering (3db freq in Hz, and filter order) \n');
%fprintf(fid,'highPassFreq1 32\n');
%fprintf(fid,'highPassFreq2 32\n');
%fprintf(fid,'highPassOrder1 6\n');
%fprintf(fid,'highPassOrder2 6\n');
fprintf(fid,'highPassFreq1 xyz_highPassFreq\n');
fprintf(fid,'highPassOrder1 6\n');
fprintf(fid,'\n');
fprintf(fid,'% coherent freqs and number of freq bins to remove if doFreqMask=true;\n');
fprintf(fid,'% NOTE: if an nBin=0, then no bins are removed even if doFreqMask=true\n');
fprintf(fid,'% (coherent freqs are typically harmonics of the power line freq 60Hz\n');
fprintf(fid,'% and the DAQ rate 16Hz)\n');
fprintf(fid,'freqsToRemove ');
fprintf(fid,' \n');
fprintf(fid,'nBinsToRemove ');
fprintf(fid,' \n');
fprintf(fid,' \n');
fprintf(fid,'% calibration filenames\n');
fprintf(fid,'alphaBetaFile1 none\n');
fprintf(fid,'alphaBetaFile2 none\n');
fprintf(fid,'calCavGainFile1 none\n');
fprintf(fid,'calCavGainFile2 none\n');
fprintf(fid,'calResponseFile1 none\n');
fprintf(fid,'calResponseFile2 none\n');
fprintf(fid,'\n');
fprintf(fid,'% stochastic test\n');
fprintf(fid,'simOmegaRef 0\n');
fprintf(fid,'heterodyned false\n');

fprintf(fid,'\n');

fprintf(fid,'% path to cache files\n');

fprintf(fid,'gpsTimesPath1 xyz_cachedir\n');
fprintf(fid,'frameCachePath1 xyz_cachedir\n');

fprintf(fid,'outputfiledir xyz_outputfiledir\n');
fprintf(fid,'outputfilename mats\n');
fprintf(fid,'mapsize 200\n');
fprintf(fid,'stochmap 1\n');
fprintf(fid,'outputFilePrefix \n');

fprintf(fid,'storemats true\n');
fprintf(fid,'batch true\n');

% Close parameter file
fclose(fid);

