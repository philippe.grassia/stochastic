
pipe_params = [];
pipe_params.ifos = {'H1','L1','V1'};
pipe_params.channels = {'FAKE-STRAIN','FAKE-STRAIN','FAKE_h_16384Hz_4R'};
pipe_params.frameTypes = {'H1_ER_C00_L1','L1_ER_C00_L1','V1Online'};

pipe_params.doClustermap = 1;
pipe_params.clustermap.doStochtrackBezier = 0;
pipe_params.clustermap.doStochtrackCBC = 1;
pipe_params.clustermap.doBurstegard = 0;

pipe_params.segmentDuration = 1;
pipe_params.doBknd = 1;
pipe_params.nBknd = 100;

save('er5_params_cbc_bknd.mat','pipe_params');

