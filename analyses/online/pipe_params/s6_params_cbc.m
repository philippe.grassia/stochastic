
pipe_params = [];
pipe_params.ifos = {'H1','L1'};
pipe_params.channels = {'LDAS-STRAIN','LDAS-STRAIN'};
pipe_params.frameTypes = {'H1_LDAS_C02_L2','L1_LDAS_C02_L2'};

pipe_params.doClustermap = 1;
pipe_params.clustermap.doStochtrackBezier = 0;
pipe_params.clustermap.doStochtrackCBC = 1;
pipe_params.clustermap.doBurstegard = 0;

pipe_params.doBknd = 0;
pipe_params.segmentDuration = 1;
pipe_params.highPassFreq = 32;

save('s6_params_cbc.mat','pipe_params');

