
pipe_params = [];
pipe_params.ifos = {'H1','L1'};
pipe_params.channels = {'GDS-CALIB_STRAIN','GDS-CALIB_STRAIN'};
pipe_params.frameTypes = {'H1_HOFT_C00','L1_HOFT_C00'};

pipe_params.doClustermap = 1;
pipe_params.clustermap.doStochtrackBezier = 1;
pipe_params.clustermap.doStochtrackCBC = 1;
pipe_params.clustermap.doStochtrackECBC = 1;
pipe_params.clustermap.doBurstegard = 0;

pipe_params.segmentDuration = 1;
pipe_params.doBknd = 0;

save('er7_params_cbc.mat','pipe_params');

