function run_online(pipe_params)

minmatlength = 100;
generateMats = 1;
pipe_params_hold = pipe_params;

while generateMats

   pipe_params = pipe_params_hold;
 
   [junk,gpstime] = system('tconvert now');
   gpsNow = str2num(gpstime) - 10000000;
   %gpsNow = 1077069327;

   pipe_params.gpsEnd = gpsNow;
   pipe_params.gpsStart = gpsNow - 3600;

   pipe_params_hold = pipe_params;

   run_cache(pipe_params);

   for i = 1:length(pipe_params_hold.ifos)

      pipe_params = pipe_params_hold;

      pipe_params.ifo1 = pipe_params.ifos{i};
      pipe_params.outputfiledir = [pipe_params.outputPath '/matfiles/' pipe_params.ifo1 '/'];
      [matGPSStart,matGPSEnd] = getStartEnd(pipe_params);

      fprintf('%s %d %d\n',pipe_params.ifo1,matGPSStart,matGPSEnd);

      gps1 = [matGPSStart matGPSEnd];

      if gps1(2) > pipe_params.gpsStart
         pipe_params.gpsStart = gps1(2);
      end

      gps1 = load(sprintf('%s/cache/gpsTotal%s.1.txt',pipe_params.outputPath,pipe_params.ifo1(1)));
      gpsStartnew = pipe_params.gpsStart;
      gpsEndnew = pipe_params.gpsEnd;

      if gps1(1) > gpsStartnew
         gpsStartnew = gps1(1);
      end
      if gps1(2) < gpsEndnew
         gpsEndnew = gps1(2);
      end

      if gpsEndnew - gpsStartnew < minmatlength
         continue
      end

      pipe_params.gpsStart = gpsStartnew;
      pipe_params.gpsEnd = gpsEndnew;

      pipe_params.ifos = pipe_params.ifos(i);
      pipe_params.channels = pipe_params.channels(i);
      pipe_params.frameTypes = pipe_params.frameTypes(i);

      tic;
      run_pipeline(pipe_params); 
      elapsedTime = toc;

   end
end


