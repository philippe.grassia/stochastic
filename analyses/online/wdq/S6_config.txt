# Q Scan configuration file
# Automatically generated with wconfigure.sh
# by user bhughey on 2014-01-15 12:32:11 PST
# from sample frame files:
#   /archive/frames/ER5/L0/L1/L-L1_R-10738/L-L1_R-1073852224-64.gwf

[Context,Context]

[temp,temp]

{
  channelName:                 'H1:LDAS-STRAIN'
  frameType:                   'H1_LDAS_C02_L2'
  sampleFrequency:             16384
  searchTimeRange:             64
  searchFrequencyRange:        [0 Inf]
  searchQRange:                [4 64]
  searchMaximumEnergyLoss:     0.2
  whiteNoiseFalseRate:         1e-3
  searchWindowDuration:        0.5
  plotTimeRanges:              [1 4 16]
  plotFrequencyRange:          []
  plotNormalizedEnergyRange:   [0 25.5]
  alwaysPlotFlag:              1
}

{
  channelName:                 'L1:LDAS-STRAIN'
  frameType:                   'L1_LDAS_C02_L2'
  sampleFrequency:             16384
  searchTimeRange:             64
  searchFrequencyRange:        [0 Inf]
  searchQRange:                [4 64]
  searchMaximumEnergyLoss:     0.2
  whiteNoiseFalseRate:         1e-3
  searchWindowDuration:        0.5
  plotTimeRanges:              [1 4 16]
  plotFrequencyRange:          []
  plotNormalizedEnergyRange:   [0 25.5]
  alwaysPlotFlag:              1
}

