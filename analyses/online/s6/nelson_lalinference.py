
import os, sys, optparse, glob
import numpy as np

import matplotlib
#matplotlib.rc('text', usetex=True)
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 16})
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
import matplotlib.pyplot as plt

def get_results(file):

    dic = {}

    with open(file,'r') as f:

       for line in f:

           line_without_return = line.split("\n")
           line_split = line_without_return[0].split("\t")

           if not line_split[0] == "":
               dic[line_split[0]] = {}
               for i in xrange(1,len(line_split)):
                   try:
                       dic[line_split[0]][params[i]] = float(line_split[i])
                   except:
                       dic[line_split[0]][params[i]] = line_split[i]
           else:
               params = line_split

    fileSplit = file.split("/")
    gpsString = fileSplit[-3]
    gpsStringSplit = gpsString.split("-")

    dic["info"] = {}
    dic["info"]["gps"] = float(gpsStringSplit[0])
    dic["info"]["ifos"] = fileSplit[-2]
    dic["info"]["folder"] = fileSplit[-3]

    return dic

nelsonDirectory = "/home/nchriste/public_html/li/965174343-3369744/runB/"
folders = glob.glob(os.path.join(nelsonDirectory,"*-*"))

txt_file = "s6trigs.txt"
f = open(txt_file,"w")
gpss = []

mTotThresh = 10

for folder in folders:
    ifos = glob.glob(os.path.join(folder,"*"))
    filename = os.path.join(ifos[0],'summary_statistics.dat') 

    data_out = get_results(filename)

    if data_out["m1"]["median"] + data_out["m2"]["median"] > mTotThresh:
        continue
    
    ra = data_out["rightascension"]["median"] * 24.0 / (2*np.pi)
    dec = data_out["declination"]["median"] * 90.0
    gps = data_out["time"]["median"]

    f.write("%.0f %.10f %.10f\n"%(gps,ra,dec))

f.close()


