function trig_analysis(pipeParamsFile,outputPath,gps,ra,dec)

% For Condor purposes
if strcmp(pipeParamsFile,'0')
   return
end

if isstr(gps)
   gps = str2num(gps);
end
if isstr(ra)
   ra = str2num(ra);
end
if isstr(dec)
   dec = str2num(dec);
end

load(pipeParamsFile);

pipe_params.outputPath = outputPath;
pipe_params.triggerGPS = gps;

if pipe_params.segmentDuration < 1
   pipe_params.gpsStart = pipe_params.triggerGPS - 5;
   pipe_params.gpsEnd = pipe_params.triggerGPS + 5;
else
   pipe_params.gpsStart = pipe_params.triggerGPS - 250;
   pipe_params.gpsEnd = pipe_params.gpsStart  + 300;
end
pipe_params.ra = ra; pipe_params.dec = dec;

run_pipeline(pipe_params);

