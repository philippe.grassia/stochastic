function trig_post(pipe_params)

params.hstart = pipe_params.gpsStart;
params.hstop = pipe_params.gpsEnd;
params.plotdir = [pipe_params.outputPath '/maps/' ...
   num2str(pipe_params.gpsStart) '-' num2str(pipe_params.gpsEnd)...
   '/' pipe_params.ifo1 pipe_params.ifo2];

postdir = [params.plotdir '/post/'];
createpath(postdir);

fid = fopen([postdir '/trigger.html'],'w+');

% HTML Header
fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
fprintf(fid,'\n');
fprintf(fid,'<html>\n');
fprintf(fid,'\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<title>Trigger Page for %d</title>\n',pipe_params.triggerGPS);
fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../main.css">\n');
fprintf(fid,'<script src="../../sorttable.js"></script>\n');
fprintf(fid,'</head>\n');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Trigger Pages for %d</span></big></big></big></big><br>\n',pipe_params.triggerGPS);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');

types = {'bezier','CBC','ECBC','burstegard'};
for m = 1:length(types)
   type = types{m};
   folderName = [params.plotdir '/' type];
   if exist([folderName '/snr.png'])

      map_out = load([folderName '/map.mat']);

      if sum(strcmpi(type,{'bezier','CBC','ECBC'}))
         trig_pvalue_analytic(pipe_params,type);
         trig_pvalue_lonetrack(pipe_params,type);
         %trig_bknd(pipe_params,values{m});
      end

      % HTML table of channel plots
      fprintf(fid,'<table\n');
      fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
      fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
      fprintf(fid,'<tbody>\n');
      fprintf(fid,'<tr>\n');
      fprintf(fid,'<td\n');
      fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
      fprintf(fid,'style="font-weight: bold;">%s</span></big></big><br>\n',type);
      fprintf(fid,'</td>\n');
      fprintf(fid,'<td\n');
      fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
      fprintf(fid,'style="font-weight: bold;">SNR: %.3f </span></big></big><br>\n',map_out.stoch_out.max_SNR);
      fprintf(fid,'</td>\n');
      fprintf(fid,'</tr>\n');
      fprintf(fid,'<tr>\n');
      fprintf(fid,'<td\n');
      fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
      fprintf(fid,'style="font-weight: bold;">Cross-power spectrogram </span></big></big><br>\n');
      fprintf(fid,'</td>\n');
      fprintf(fid,'<td\n');
      fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
      fprintf(fid,'style="font-weight: bold;">Track recovery </span></big></big><br>\n');
      fprintf(fid,'</td>\n');
      fprintf(fid,'</tr>\n');
      fprintf(fid,'<tr>\n');
      fprintf(fid,'<td style="vertical-align: top;">\n');

      plotpath = sprintf('../%s/snr.png',type);
      fprintf(fid,'<a href="%s"><img alt="" src="%s" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plotpath,plotpath);
      if exist([folderName '/large_cluster.png'])
         plotpath = sprintf('../%s/large_cluster.png',type);
      else
         plotpath = sprintf('../%s/rmap.png',type);
      end
      fprintf(fid,'</td>');
      fprintf(fid,'<td style="vertical-align: top;">\n');
      fprintf(fid,'<a href="%s"><img alt="" src="%s" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plotpath,plotpath);
      fprintf(fid,'</td>');

      if exist([folderName '/pvalues_analytic.png'])
         plotpath = sprintf('../%s/pvalues_analytic.png',type);
         fprintf(fid,'</td>');
         fprintf(fid,'<td style="vertical-align: top;">\n');
         fprintf(fid,'<a href="%s"><img alt="" src="%s" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plotpath,plotpath);
         fprintf(fid,'</td>');
      end
      if exist([folderName '/pvalues_lonetrack.png'])
         plotpath = sprintf('../%s/pvalues_lonetrack.png',type);
         fprintf(fid,'</td>');
         fprintf(fid,'<td style="vertical-align: top;">\n');
         fprintf(fid,'<a href="%s"><img alt="" src="%s" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n',plotpath,plotpath);
         fprintf(fid,'</td>');
      end

      fprintf(fid,'</tr>\n');

      fprintf(fid,'</tbody>\n');
      fprintf(fid,'</table>\n');

   end
end

% Close HTML
fprintf(fid,'</html>\n');
fclose(fid);

return
