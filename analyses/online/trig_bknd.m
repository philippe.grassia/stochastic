function trig_bknd(pipe_params,type)

mats = dir([outpath '/bknd/*.mat']);

file = [outpath '/map.mat'];
q = load(file);
snr_zero = q.stoch_out.max_SNR;

% loop over mat files
snr = [];
for ii=1:length(mats)
  % load mat files
  file = [outpath '/bknd/' mats(ii).name];
  q = load(file);
  snr(ii) = q.stoch_out.max_SNR;
end

% make a histogram of SNR
figure;
hist(snr, 25);
hold on
title(sprintf('zero lag SNR: %.3f',snr_zero));
xlabel('SNR');
pretty;
print('-dpng',[outpath '/bknd']);
close;

th  = max(snr);

snr_sort = sort(snr,'ascend');
p_values = fliplr(1/length(snr_sort):1/length(snr_sort):1);

[junk,index] = min(abs(snr_sort - snr_zero));

figure;
semilogy(snr_sort,p_values,'r')

xlabel('SNR');
ylabel('p-value');
title(sprintf('zero lag p-value: %.3f',p_values(index)));
pretty;
print('-dpng',[outpath '/pvalues']);
close;

save([outpath '/bknd.mat'],'snr','th');


