function [params] = setClusteringParams(params,pipe_params,str)

if strcmp(str,'Bezier')
   params = stochtrackDefaults(params);
   params.doStochtrack = true;
   params.doGPU = true;
   params.doParallel = false;
   params.stochtrack.stochsky = true;
   params.stochtrack.doBezier = true;
   params.stochtrack.doCBC = false;
   params.stochtrack.doDF = false;
   params.stochtrack.mindur = 100;
   params.stochtrack.T = 10000;
   params.stochtrack.F = 20;
   params.stochtrack.cbc.allsky = false;

   if pipe_params.timeshift == 0
      params.plotdir = [pipe_params.outputPath '/maps/' ...
         num2str(params.hstart) '-' num2str(params.hstop)...
         '/' pipe_params.ifo1 pipe_params.ifo2 '/bezier/'];
   else
      params.plotdir = [pipe_params.outputPath '/maps/' ...
         num2str(params.hstart) '-' num2str(params.hstop)...
         '/' pipe_params.ifo1 pipe_params.ifo2 '/bezier/bknd/'];
   end
   createpath(params.plotdir);
   params.outputfilename = [params.plotdir '/map'];
   params.ftmapdir = params.outputfilename;

   if pipe_params.segmentDuration < 1
      %params.stochtrack.doTranspose = 1;
      %params.stochtrack.stochsky = false;
      %params.stochtrack.mindur = 10;
   end

elseif strcmp(str,'CBC')
   params = stochtrackDefaults(params);
   params.doStochtrack = true;
   params.doGPU = true;
   params.doParallel = false;
   params.stochtrack.stochsky = false;
   params.stochtrack.doBezier = false;
   params.stochtrack.doCBC = true;
   params.stochtrack.doDF = true;
   params.stochtrack.mindur = 0;

   if pipe_params.segmentDuration < 1
      params.stochtrack.cbc.min_mass = 10.0;
      params.stochtrack.cbc.max_mass = 100.0;
   else
      params.stochtrack.cbc.min_mass = 1.3;
      params.stochtrack.cbc.max_mass = 3.0;
   end

   if params.hstart < 1000000000
      params.stochtrack.cbc.minstop = 0;
      params.stochtrack.extra_pixels = 500;
   else
      params.stochtrack.cbc.minstop = 30;
      params.stochtrack.extra_pixels = 500;
   end

   params.stochtrack.cbc.allsky = true;
   %params.stochtrack.cbc.allsky = false;   
   
   params.stochtrack.T = 825;
   params.stochtrack.F = 50;
   if pipe_params.timeshift == 0
      params.plotdir = [pipe_params.outputPath '/maps/' ...
         num2str(params.hstart) '-' num2str(params.hstop)...
         '/' pipe_params.ifo1 pipe_params.ifo2 '/CBC/'];
   else
      params.plotdir = [pipe_params.outputPath '/maps/' ...
         num2str(params.hstart) '-' num2str(params.hstop)...
         '/' pipe_params.ifo1 pipe_params.ifo2 '/CBC/bknd/'];
   end
   createpath(params.plotdir);
   params.outputfilename = [params.plotdir '/map'];
   params.ftmapdir = params.outputfilename;

elseif strcmp(str,'ECBC')
   params = stochtrackDefaults(params);
   params.doStochtrack = true;
   params.doGPU = true;
   params.doParallel = false;
   params.stochtrack.stochsky = false;
   params.stochtrack.doBezier = false;
   params.stochtrack.doECBC = true;
   params.stochtrack.doDF = true;
   params.stochtrack.mindur = 0;
   params.stochtrack.cbc.min_eccentricity = 0;
   params.stochtrack.cbc.max_eccentricity = 0.5;
   params.stochtrack.cbc.n_eccentricity = 6;

   if pipe_params.segmentDuration < 1
      params.stochtrack.cbc.min_mass = 10.0;
      params.stochtrack.cbc.max_mass = 50.0;
   else
      params.stochtrack.cbc.min_mass = 1.3;
      params.stochtrack.cbc.max_mass = 3.0;
   end

   if params.hstart < 1000000000
      params.stochtrack.cbc.minstop = 16;
      params.stochtrack.extra_pixels = 250;
   else
      params.stochtrack.cbc.minstop = 100;
      params.stochtrack.extra_pixels = 250;
   end

   params.stochtrack.cbc.allsky = true;
   %params.stochtrack.cbc.allsky = false;

   params.stochtrack.T = 825;
   params.stochtrack.F = 50;

   if pipe_params.timeshift == 0
      params.plotdir = [pipe_params.outputPath '/maps/' ...
         num2str(params.hstart) '-' num2str(params.hstop)...
         '/' pipe_params.ifo1 pipe_params.ifo2 '/ECBC/'];
   else
      params.plotdir = [pipe_params.outputPath '/maps/' ...
         num2str(params.hstart) '-' num2str(params.hstop)...
         '/' pipe_params.ifo1 pipe_params.ifo2 '/ECBC/bknd/'];
   end
   createpath(params.plotdir);
   params.outputfilename = [params.plotdir '/map'];
   params.ftmapdir = params.outputfilename;

elseif strcmp(str,'Burstegard')
   params = burstegardDefaults(params);
   params.burstegard.pixelThreshold = 0.5;

   if pipe_params.timeshift == 0
      params.plotdir = [pipe_params.outputPath '/maps/' ...
         num2str(params.hstart) '-' num2str(params.hstop)...
         '/' pipe_params.ifo1 pipe_params.ifo2 '/burstegard/'];
   else
      params.plotdir = [pipe_params.outputPath '/maps/' ...
         num2str(params.hstart) '-' num2str(params.hstop)...
         '/' pipe_params.ifo1 pipe_params.ifo2 '/burstegard/bknd/'];
   end
   createpath(params.plotdir);
   params.outputfilename = [params.plotdir '/map'];
   params.ftmapdir = params.outputfilename;
else
   error('I do not recognize %s as a name for a clustering algorithm',str);
end

params.doGPU = false;
