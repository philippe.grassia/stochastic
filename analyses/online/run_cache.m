function run_cache(pipe_params)

for i = 1:length(pipe_params.ifos)
   frameType = pipe_params.frameTypes{i};
   ifo = pipe_params.ifos{i};
   ligo_data_find_command = ...
      sprintf('gw_data_find -t %s -o %s -s %.0f -e %.0f -u file > %s/cache/%s.cache',frameType,ifo(1),pipe_params.gpsStart,pipe_params.gpsEnd,pipe_params.outputPath,ifo);
   system(ligo_data_find_command);

   cacheFile = sprintf('%s/cache/%s.cache',pipe_params.outputPath,ifo);
   frameFiles = sprintf('%s/cache/frameFiles%s.1.txt',pipe_params.outputPath,ifo(1));
   gpsFiles = sprintf('%s/cache/gpsTimes%s.1.txt',pipe_params.outputPath,ifo(1));

   gpss = [];
   cache = textread(cacheFile,'%s','delimiter','\n');
   f = fopen(frameFiles,'w+');
   g = fopen(gpsFiles,'w+');
   for i = 1:length(cache)
      cacheSplit = regexp(cache{i},'file://localhost','split');
      cacheSplit = cacheSplit{2};
      fprintf(f,'%s\n',cacheSplit);
      cacheSplit = strrep(cacheSplit,'.','-');
      cacheSplit = regexp(cacheSplit,'-','split');
      gps = str2num(cacheSplit{end-2});
      dur = str2num(cacheSplit{end-1});
      fprintf(g,'%d\n',gps);
      gpss = [gpss gps];
   end

   fclose(f); 
   fclose(g);

   gpsFiles = sprintf('%s/cache/gpsTotal%s.1.txt',pipe_params.outputPath,ifo(1));
   g = fopen(gpsFiles,'w+');
   fprintf(g,'%d %d\n', min(gpss),max(gpss)+dur);
   fclose(g);
end

