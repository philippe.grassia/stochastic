#!/bin/bash

source ${HOME}/.bashrc
source ${HOME}/.bash_profile

gpsEnd=`tconvert now - 60 seconds`
gpsStart=`tconvert now`

gpsEnd=$(expr $gpsEnd - 10000000)
gpsStart=$(expr $gpsStart - 10000000)

executable="/home/mcoughlin/STAMP/online/bin/trig_analysis"
outputDirectory="/home/mcoughlin/STAMP/ER5/triganalysis"

export X509_USER_CERT=/home/mcoughlin/cert/stampfollowup-cert.pem
export X509_USER_KEY=/home/mcoughlin/cert/robot.key.pem

python /home/mcoughlin/matapps/packages/stochastic/trunk/analyses/online/gracedb/run_trig_analysis.py -s $gpsStart -e $gpsEnd -p $executable -o $outputDirectory

