
import os, sys, optparse
import numpy as np
import healpy as hp

import matplotlib
#matplotlib.rc('text', usetex=True)
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 16})
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
import matplotlib.pyplot as plt

from ligo.gracedb.rest import GraceDb, HTTPError

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser()

    parser.add_option("-x", "--executable", help="trigger analysis executable",\
        default="/home/mcoughlin/STAMP/online/bin/trig_analysis")
    parser.add_option("-o", "--outputDirectory", help="trigger analysis directory",\
        default="/home/mcoughlin/STAMP/ER5/triganalysis")
    parser.add_option("-p", "--pipeParamsFile", help="pipeline parameter mat file",\
        default="er5_params.mat")
    parser.add_option("-f", "--far", help="far threshold",default=1e-7)
    parser.add_option("-t", "--type", help="trigger type",default="LowMass")
    parser.add_option("-s", "--startGPS", help="start gps",type=int,default=1074925300)
    parser.add_option("-e", "--endGPS", help="end gps",type=int,default=1074925310)   

    opts, args = parser.parse_args()

    return opts

opts = parse_commandline()

# Instantiate client
g = GraceDb()

farString = "%s"%opts.far
farString = farString.replace("0","")
eventString = '%s far "<%s" %d..%d'%(opts.type,farString,opts.startGPS,opts.endGPS)
eventString = '%s %d..%d'%(opts.type,opts.startGPS,opts.endGPS)

# REST API returns an iterator
events = g.events('%s'%eventString)

rm_command = "rm skymap.fits.gz*"
os.system(rm_command)

keys = ['graceid','gpstime','extra_attributes','group','links','created','far','instruments','labels','nevents','submitter','analysisType','likelihood']

gpss = []

for event in events:

    eventinfo = {}
    for key in keys:
        eventinfo[key] = event[key]

    if eventinfo['far'] > opts.far:
        continue

    if len(gpss) > 0:
        dist = np.absolute(eventinfo['gpstime'] - np.array(gpss))
        if np.min(dist) < 5:
            continue
    gpss.append(eventinfo['gpstime'])

    ra = 0
    dec = 0
    trig_analysis_command = "%s %s %s %d %.5f %.5f"%(opts.executable,opts.pipeParamsFile,opts.outputDirectory,eventinfo['gpstime'],ra,dec)
    #f.write("%.0f %.10f %.10f\n"%(gps,ra,dec))
    print eventinfo['graceid'], eventinfo['gpstime'], eventinfo['far']

    skymapfile = open('skymap.fits.gz','w')
    r = g.files(eventinfo['graceid'], 'skymap.fits.gz')
    skymapfile.write(r.read())
    skymapfile.close()

    mask = hp.read_map('skymap.fits.gz')
    npix = len(mask)
    nside = hp.npix2nside(npix)
    index = np.argmax(mask)
    theta, phi = hp.pix2ang(nside, index)
    ra = phi
    dec = 0.5*np.pi - theta

    ra = (ra / (2*np.pi)) * 24.0
    dec = dec * (360.0/(2*np.pi))

    #plotName = os.path.join('.','mollview.png')
    #hp.mollview(mask)
    #plt.show()
    #plt.savefig(plotName,dpi=200)
    #plt.close('all')

    trig_analysis_command = "%s %s %s %d %.5f %.5f"%(opts.executable,opts.pipeParamsFile,opts.outputDirectory,eventinfo['gpstime'],ra,dec)
    os.system(trig_analysis_command)

