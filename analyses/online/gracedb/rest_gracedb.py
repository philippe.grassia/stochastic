
import os, sys, optparse
import numpy as np
import healpy as hp

import matplotlib
#matplotlib.rc('text', usetex=True)
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 16})
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
import matplotlib.pyplot as plt

from ligo.gracedb.rest import GraceDb, HTTPError

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser()

    parser.add_option("-f", "--far", help="far threshold",type=float,default=1e-7)
    parser.add_option("-t", "--type", help="trigger type",default="LowMass")
    parser.add_option("-s", "--startGPS", help="start gps",type=int,default=1070000000)
    parser.add_option("-e", "--endGPS", help="end gps",type=int,default=1080000000)    

    opts, args = parser.parse_args()

    return opts

opts = parse_commandline()

# Instantiate client
g = GraceDb()

farString = "%.5e"%opts.far
farString = farString.replace("0","")
eventString = '%s far "<%s" %d..%d'%(opts.type,farString,opts.startGPS,opts.endGPS)
eventString = '%s %d..%d'%(opts.type,opts.startGPS,opts.endGPS)

# REST API returns an iterator
events = g.events('%s'%eventString)

rm_command = "rm skymap.fits.gz*"
os.system(rm_command)

keys = ['graceid','gpstime','extra_attributes','group','links','created','far','instruments','labels','nevents','submitter','search','likelihood']

txt_file = "trigs.txt"
f = open(txt_file,"w")
gpss = []

for event in events:

    eventinfo = {}
    for key in keys:
        eventinfo[key] = event[key]
    eventinfo['gpstime'] = float(eventinfo['gpstime'])

    if eventinfo['far'] > opts.far:
        continue

    if len(gpss) > 0:
        dist = np.absolute(eventinfo['gpstime'] - np.array(gpss))
        if np.min(dist) < 5:
            continue
    gpss.append(eventinfo['gpstime'])

    ra = 0
    dec = 0
    #f.write("%.0f %.10f %.10f\n"%(gps,ra,dec))

    try:
        skymapfile = open('skymap.fits.gz','w')
        r = g.files(eventinfo['graceid'], 'skymap.fits.gz')
        skymapfile.write(r.read())
        skymapfile.close()

        mask = hp.read_map('skymap.fits.gz')
        npix = len(mask)
        nside = hp.npix2nside(npix)
        index = np.argmax(mask)
        theta, phi = hp.pix2ang(nside, index)
        ra = phi
        dec = 0.5*np.pi - theta

        ra = (ra / (2*np.pi)) * 24.0
        dec = dec * (360.0/(2*np.pi))
    except:
        print "Download of skymaps file for %s failed... using 0.0, 0.0"%eventinfo['graceid']

    #plotName = os.path.join('.','mollview.png')
    #hp.mollview(mask)
    #plt.show()
    #plt.savefig(plotName,dpi=200)
    #plt.close('all')

    rm_command = "rm skymap.fits.gz"
    os.system(rm_command)

    print eventinfo['graceid'], eventinfo['gpstime'], eventinfo['far'], ra, dec

    f.write("%.0f %.10f %.10f\n"%(eventinfo['gpstime'],ra,dec))

f.close()


