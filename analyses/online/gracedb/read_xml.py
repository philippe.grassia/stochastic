
import glue.ligolw.utils
import glue.ligolw.table
import glue.ligolw.lsctables
import sys
import numpy as np

inj_xml = "out.xml"

xmldoc = glue.ligolw.utils.load_filename(inj_xml, verbose=False)
injs = glue.ligolw.table.get_table(xmldoc,glue.ligolw.lsctables.CoincInspiralTable.tableName)

txt_file = "trigs.txt"

f = open(txt_file,"w")

for inj in injs:
    injtime = inj.end_time+1.e-9*inj.end_time_ns
    injfar = inj.combined_far
    injmchirp = inj.mchirp
    injifos = inj.ifos

    print injtime, injfar, injmchirp, injifos

    #if injfar > 1e-12:
    #    continue
    f.write("%.0f\n"%injtime)

f.close()

trigs = np.loadtxt(txt_file)
trigs = np.unique(trigs)

f = open(txt_file,"w")

for trig in trigs:
    f.write("%.0f\n"%trig)

f.close()

