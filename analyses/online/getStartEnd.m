function [matGPSStart,matGPSEnd] = getStartEnd(pipe_params)

matdirs = dir([pipe_params.outputfiledir '/*-*']);

if length(matdirs) == 0
   matGPSStart = 0;
   matGPSEnd = 0;
   return;
end

% record all the matlab files available
files = [];
paths = [];
gps = [];
index = 0;
% loop over possible directories containing mat files
for ii=1:length(matdirs)
  matdir = [pipe_params.outputfiledir '/' matdirs(ii).name];
  % list of mat files in the appropriate directories
  all_files = dir([matdir '/*.mat']);
  % for each file record its directory
  for jj = 1:length(all_files)
    index = index + 1;
    files{index} = all_files(jj).name;
    paths{index} = matdir;
    fileReplace = strrep(files{index},'.mat','');
    fileSplit = regexp(fileReplace,'-','split');
    gps = [gps; str2num(fileSplit{3}) str2num(fileSplit{3})+str2num(fileSplit{4})];
  end
end

if isempty(gps)
   matGPSStart = 0;
   matGPSEnd = 0;
   return;
end

% keep mat files that overlap with requested time
cut = pipe_params.gpsStart < gps(:,2) & pipe_params.gpsEnd > gps(:,1);

gps = gps(cut,:);
if ~any(cut)
   matGPSStart = 0;
   matGPSEnd = 0;
   return;
end
matGPSStart=min(gps(:,1)); matGPSEnd = max(gps(:,2));

