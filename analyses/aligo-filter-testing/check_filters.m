function run_filter(type, st)

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);
ST = st;

outputDir = ['./plots-corrected-filter-new-filter/' type];
if ~exist(outputDir)
   system(['mkdir -p ' outputDir])
end

fs = 4096; % Sampling rate
highPassOrder = 6;

if strcmp(type,'iligo')

   highpassFreqs = 32;

   spectraFile = '/home/mcoughlin/Stochastic//filter/lho4k_070318_strain.txt';
   spectra = load(spectraFile);
   spectra_f = spectra(:,1); spectra_h = spectra(:,2);
   channel = 'H1:LDAS-STRAIN';
   frameType = 'H1_LDAS_C02_L2';

elseif strcmp(type,'aligo')

   spectraFile = 'LHO-ALIGO-ASD-151006.txt';
   spectra = load(spectraFile);
   spectra_f = spectra(:,1); spectra_h = spectra(:,2).^2;
   highpassFreqs = [6, 9, 10, 11, 12, 14, 16];
   highPassOrder = 8;
   channel = 'H1:GDS-CALIB_STRAIN';
   frameType = 'H1_HOFT_C00';
   %highPassOrder = 6;
   %highPassOrder = 4;

elseif strcmp(type,'aligo2')

   highpassFreqs = 0.5:0.5:10;
   %highpassFreqs = 6;

   spectraFile = '/home/mcoughlin/Stochastic//filter/adv_spectra_phase1.txt';
   spectra = load(spectraFile);
   spectra_f = spectra(:,1); spectra_h = spectra(:,4);

   spectra_f_9_10_indexes = find(spectra >=9 & spectra <= 15);
   spectra_f_9_10 = spectra_f(spectra_f_9_10_indexes);
   spectra_h_9_10 = spectra_h(spectra_f_9_10_indexes);

   logx=log(spectra_f_9_10);
   logy=log(spectra_h_9_10);
   p=polyfit(logx,logy,1);

   k=p(1);
   loga=p(2);
   a=exp(loga);

   deltaF = spectra_f(2)-spectra_f(1);
   %spectra_f_low = deltaF:deltaF:(spectra_f(1)-deltaF);
   spectra_f_low = 2:deltaF:(spectra_f(1)-deltaF);
   spectra_h_low = a*spectra_f_low.^k;

   spectra_f = [spectra_f_low spectra_f'];
   spectra_h = [spectra_h_low spectra_h'];

   %spectra_f_new = (spectra_f(2)-spectra_f(1)):(spectra_f(2)-spectra_f(1)):spectra_f(end);
   %spectra_h_new = interp1(spectra_f,spectra_h,spectra_f_new,'spline','extrap');

   %spectra_f = spectra_f_new; spectra_h = spectra_h_new;

   fid = fopen('adv_spectra_interp.txt','w+');
   for i = 1:length(spectra_f)
      fprintf(fid,'%.10e %.10e\n',spectra_f(i),spectra_h(i)^2);
   end
   fclose(fid);

elseif strcmp(type,'et')

   highpassFreqs = 0.5:0.5:5;

   spectraFile = 'ET.txt';
   spectra = load(spectraFile);

   spectra_f = spectra(1:10000); spectra_h = spectra(10001:end);

   fid = fopen('et_spectra.txt','w+');
   for i = 1:length(spectra_f)
      fprintf(fid,'%.10e %.10e\n',spectra_f(i),spectra_h(i)^2);
   end
   fclose(fid);

   spectra_f = spectra_f(2:end); spectra_h = spectra_h(2:end);

   %indexes = find(spectra_f <= 2);
   %spectra_h(indexes) = spectra_h(indexes(end));

   spectra_f_2_6_indexes = find(spectra_f >=2 & spectra_f <= 6);
   spectra_f_2_6 = spectra_f(spectra_f_2_6_indexes);
   spectra_h_2_6 = spectra_h(spectra_f_2_6_indexes);

   logx=log(spectra_f_2_6);
   logy=log(spectra_h_2_6);
   p=polyfit(logx,logy,1);

   k=p(1);
   loga=p(2);
   a=exp(loga);

   deltaF = spectra_f(2)-spectra_f(1);
   %spectra_f_low = deltaF:deltaF:(spectra_f(1)-deltaF);
   spectra_f_low = 1:deltaF:(spectra_f(1)-deltaF);
   spectra_h_low = a*spectra_f_low.^k;

   spectra_f = [spectra_f_low; spectra_f'];
   spectra_h = [spectra_h_low; spectra_h'];

   fid = fopen('et_spectra_interp.txt','w+');
   for i = 1:length(spectra_f)
      fprintf(fid,'%.10e %.10e\n',spectra_f(i),spectra_h(i)^2);
   end
   fclose(fid);

end

highpassResponses10 = zeros(size(highpassFreqs));
highpassResponses5 = zeros(size(highpassFreqs));

T = 64;
mc = [];
transfer = load(spectraFile);

mc.transfer(:,1) = spectra_f;
mc.transfer(:,2) = spectra_h;

%vector = create_noise(fs,T,mc);
averages = 20;
t = 0:(1/fs):T;


for i = 1:length(highpassFreqs)
   highPassFreq = highpassFreqs(i); %Hz
   fprintf('running %f Hz\n',highPassFreq);
   highPassFreqNorm = highPassFreq/(fs/2);
   %passBandFreqNorm = 10/(fs/2);

   %[n,Wn] = buttord(passBandFreqNorm,highPassFreqNorm,3,60);
   % Returns n = 5; Wn=0.0810;
   %[b,a] = butter(n,Wn,'high');

   [b,a] = butter(highPassOrder, highPassFreqNorm, 'high');
   %[z, p, k] = butter(highPassOrder, highPassFreqNorm, 'high');
   %[sos,g]=zp2sos(z,p,k);
   %h=dfilt.df2sos(sos,g);
   %[h,f] = freqz(conv(b,b(end:-1:1)),conv(a,a(end:-1:1)),fs,fs);
   %[h,f] = freqz(b,a,fs,fs);
   %h=dfilt.df2sos(sos,g);

   window = hann(fs*T);
   st = ST;
   for j = 1:averages

      [vector times ok] = getFrameData(channel, frameType, st, T);
      vector = resample(vector, 1, 4, 10, 5);
      st = st + T;
      vector = vector(1:end-1)';
      vector = vector.*window';

      if highPassOrder > 6
%         y = cascadefilter(vector, highPassOrder, ...
%            highPassFreq, fs);
         y = sos_filter(vector, highPassOrder,...
            highPassFreq, fs);

      else
         y = filtfilt(b,a,vector);
      end
      L = length(t);
      NFFT = 2^nextpow2(L); % Next power of 2 from length of y
      f = fs/2*linspace(0,1,NFFT/2+1);

      if j == 1
         total_single_sided_filtered = zeros(length(f),1);
         total_single_sided_unfiltered = zeros(length(f),1);
         total_single_sided_filtered_fft = zeros(length(f),1);
         total_single_sided_unfiltered_fft = zeros(length(f),1);
         total_response = zeros(length(f),1);
      end

      sT = floor(NFFT/fs);
      windowconst = 0.375;

      Y = fft(vector,NFFT);
      single_sided_unfiltered_fft  = 2*(Y(1:NFFT/2+1));
%      single_sided_unfiltered = 2*(abs(Y(1:NFFT/2+1)).^2)/(L*fs*windowconst);
      %single_sided_unfiltered = sqrt(single_sided_unfiltered);
      Y = fft(y,NFFT);
      single_sided_filtered_fft = 2*(Y(1:NFFT/2+1));
%      single_sided_filtered = 2*(abs(Y(1:NFFT/2+1)).^2)/(L*fs*windowconst);
      %single_sided_filtered = sqrt(single_sided_filtered);

      %response = single_sided_filtered ./ single_sided_unfiltered;
      total_single_sided_filtered_fft = total_single_sided_filtered_fft + single_sided_filtered_fft';
      %total_single_sided_filtered = total_single_sided_filtered + single_sided_filtered';
      total_single_sided_unfiltered_fft = total_single_sided_unfiltered_fft + single_sided_unfiltered_fft';
      %total_single_sided_unfiltered = total_single_sided_unfiltered + single_sided_unfiltered';
      %total_response = total_response + response';
   end
   %single_sided_filtered = total_single_sided_filtered / averages;
   %single_sided_unfiltered = total_single_sided_unfiltered / averages;
   single_sided_filtered_fft = total_single_sided_filtered_fft / averages;
   single_sided_unfiltered_fft = total_single_sided_unfiltered_fft / averages;
   %response = total_response / averages;
   final_response = single_sided_filtered_fft ./ single_sided_unfiltered_fft;
   fprintf('setting up plotting...\n');

   indexes = find(f <= 1024 & f>=1);
   f = f(indexes);
   h = final_response(indexes);
   single_sided_filtered = abs(single_sided_filtered_fft(indexes)).^2 / (L*fs*windowconst);
   single_sided_unfiltered = abs(single_sided_unfiltered_fft(indexes)).^2 / (L*fs*windowconst);
   fprintf('ploting...\n');

   figure;
   plot(t(1:fs),vector(1:fs),'b')
   hold on
   plot(t(1:fs),y(1:fs),'r')
   hold off
   hleg1 = legend('unfiltered','filtered');
   set(hleg1,'Location','NorthWest')
   set(hleg1,'Interpreter','none')   

   %xlim([1,100])
   xlabel('Time [s]');
   ylabel('');
   print('-dpng',[outputDir '/' num2str(highPassFreq) '_' num2str(highPassOrder) '_time.png'])
   close;

   fprintf('ts plotted...\n');
   figure;
   subplot(2,1,1); 
   plot(f,20 * log10(abs(h)),'-');
   ylabel('|H(f)|');
   axis([0 1e2 -20 10]);
   grid on;
   subplot(2,1,2);
   plot(f,(180/pi)*unwrap(angle(h)),'-');
   xlim([0,100])
   xlabel('frequency (Hz)');
   ylabel('phase lag (degrees)');
   grid on;
   print('-dpng',[outputDir '/' num2str(highPassFreq) '_' num2str(highPassOrder) '_response.png'])
   close;

   fprintf('response plotted...\n');
   figure;
   loglog(f,single_sided_unfiltered,'b')
   hold on
   loglog(f,single_sided_filtered,'r')
   loglog(spectra_f,spectra_h,'g')
   hold off
   grid;
   hleg1 = legend('unfiltered','filtered','psd');
   %hleg1 = legend('unfiltered','filtered');
   set(hleg1,'Location','NorthEast')
   set(hleg1,'Interpreter','none')
   xlim([10,100])
   xlabel('Frequency [Hz]');
   ylabel('FFT');
   print('-dpng',[outputDir '/' num2str(highPassFreq) '_' num2str(highPassOrder) '_psd.png'])
   close;
   fid = fopen([outputDir '/log-' num2str(highPassFreq) '-' num2str(highPassOrder) '.log'],'w+');
   fprintf('psds plotted...\n');

   [f_10, f_10_index] = min(abs(f - 10));
   response = abs(h(f_10_index));
   fprintf(fid, 'Frequency: %f, Response at 10 Hz: %f\n',highPassFreq,response);

   highpassResponses10(i) = response;

   [f_5, f_5_index] = min(abs(f - 5));
   response = abs(h(f_5_index));
   fprintf(fid, 'Frequency: %f, Response at 5 Hz: %f\n',highPassFreq,response);
   highpassResponses10(i) = response;

   [f_20, f_20_index] = min(abs(f - 20));
   response = abs(h(f_20_index));
   fprintf(fid, 'Frequency: %f, Response at 20 Hz: %f\n',highPassFreq,response);
   highpassResponses20(i) = response;

   [f_40, f_40_index] = min(abs(f - 40));
   response = abs(h(f_40_index));
   fprintf(fid, 'Frequency: %f, Response at 40 Hz: %f\n',highPassFreq,response);
   highpassResponses40(i) = response;

   [f_9, f_9_index] = min(abs(f - 9));
   response = abs(h(f_9_index));
   fprintf(fid, 'Frequency: %f, Response at 9 Hz: %f\n',highPassFreq,response);
   highpassResponses9(i) = response;

   [f_13, f_13_index] = min(abs(f - 13));
   response = abs(h(f_13_index));
   fprintf(fid, 'Frequency: %f, Response at 13 Hz: %f\n',highPassFreq,response);
   highpassResponses13(i) = response;
   fclose(fid);
end

figure;
plot(highpassFreqs,highpassResponses10,'*')
xlabel('Corner Frequency [Hz]');
ylabel('Response at 10Hz');
print('-dpng',[outputDir '/responses10.png'])
close;

figure;
plot(highpassFreqs,highpassResponses5,'*')
xlabel('Corner Frequency [Hz]');
ylabel('Response at 5Hz');
print('-dpng',[outputDir '/responses5.png'])
close;

figure;
loglog(spectra_f,spectra_h)
xlabel('Frequency [Hz]');
ylabel('PSD');
xlim([1,10^3])
print('-dpng',[outputDir '/psd.png'])
close;


