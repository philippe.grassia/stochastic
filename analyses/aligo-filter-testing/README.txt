check_filters.m is used to check the aligo filters.

cascadefilter.m creates the cascade filter used for filter orders > 6. 
* In doing this analysis myself, I manually changed the cascade filter
function.

getFrameData.m is used to get frame data. check_filters.m is set up to
automatically look for aligo or iligo data depending on the 'type' you enter
into check_filters itself. No checks are done about the times that you enter,
so if you choose 'aligo' and give it 'iligo' times you're likely to get an
error. 

contact: patrick.meyers@ligo.org
