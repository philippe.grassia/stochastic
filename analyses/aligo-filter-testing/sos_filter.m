function [x2] = sos_filter(x, n, fknee, fs)
[z, p, k] = butter(n, fknee/(fs/2), 'high');
[sos, G] = zp2sos(z, p, k);
x2 = filtfilt(sos, G, x);

