psd1=load('LIGOsrdPSD_40Hz.txt');
psd2=load('aLIGOpsd_Early.txt');
psd3=load('aLIGO_O1PSD_6Hz.txt');

loglog(psd1(:,1),psd1(:,2),'b');
hold on
loglog(psd2(:,1),psd2(:,2),'r');
loglog(psd3(:,1),psd3(:,2),'g');
hold off

xlim([1 8000])
grid

legend('iLIGO 40 Hz','aLIGO Early 9Hz','aLIGO O1 6Hz');