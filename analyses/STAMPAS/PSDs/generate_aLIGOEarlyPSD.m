asd=load('aLIGO_EarlyASD.txt');

psd=asd;
psd(:,2)=asd(:,2).^2;

psd2=load('LIGOsrdPSD_40Hz.txt');

loglog(psd(:,1),psd(:,2));
hold on
loglog(psd2(:,1),psd2(:,2),'r');

freqs=asd(1,1):.1:asd(end,1);
freqs2=logspace(log10(1),log10(8000),350);

psd3=interp1(psd(:,1),psd(:,2),freqs2);
ext=find(isnan(psd3(1,:)));
psd3(1,ext(1:end-1))=psd3(1,ext(end-1)+1);
psd3(1,ext(end))=psd3(1,ext(end)-1);
loglog(freqs2,psd3,'g');
grid

xlim([1 8000])

asd2=load('aLIGO_O1ASD.txt');

psd4=asd2;
psd4(:,2)=asd2(:,2).^2;
loglog(psd4(:,1),psd4(:,2),'k');
hold off

legend('aLIGO\_EarlyPSD', 'LIGOsrdPSD\_40Hz.txt', 'aLIGO\_EarlyPSD interpolated','O1 PSD')
xlabel('Frequency [Hz]');
ylabel('PSD');

toto=[freqs2;psd3]';
dlmwrite('aLIGO_Early_psd.txt',toto,'delimiter',' ','precision','%8.4g');