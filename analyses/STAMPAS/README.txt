Modified April 2nd 2014
Contact: franco@lal.in2p3.fr

%%%%%%% WARNING !!!!! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

On Hanford, only S6 caches are available.

The STAMPAS pipeline works on Caltech, Hanford and ATLAS clusters

READ THIS FILE CAREFULLY: SEVERAL PATHS MUST BE CHANGED IN ORDER
TO RUN STAMPAS!!

%%%%%%%%% WHAT IS STAMPAS? %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

STAMPAS is a package which allows the user to perform a complete
STAMP All-Sky search, including background studies and injections.

It is meant to be self-consistent, and except for specific post-
processing studies, should allow any user to perform a search
without having to re-code something.

The user interact with STAMPAS using the config files, and the
submit_*_dag.sh scripts. 
They generate, given the necessary parameters, all the files
needed to perform the search. Tools provided in the post-processing
repertory should allow the user to exploit the data he obtained.

%%%%%%%% HOW IS STRUCTURED THE SEARCH? %%%%%%%%%%%%%%%%%%%%%%%%%

1. Background / zero-lag

Background studies works that way: for a given pair of ifos, you
provide a GPS start time and a GPS end time, which
correspond to the data you want to analyze. On this period, a 
STAMPAS search will be done according to the parameters you entered.
The period of time you chose is divided into shorter periods. For
each of these the STAMP analysis
is performed on a certain number of directions. Triggers found in the
data are then listed in the results repertory. 

2. Injections

Injections work that way: you first chose the number NumberOfInjections
of injections (NInj) you want to make per waveform and the waveforms you will
inject. You also select a list of multiplicative factors, to do several (up
to 20) injections  using the same waveform, but with different amplitudes.
Each waveform will be injected NInj times, and at each time, the injection will be 
performed on a different random sky location (the list of these locations 
is the same for each waveform) and at a random time inside each ft-map.

A STAMP All-Sky study is then performed around each injection, and the
results are stored in the same fashion than background results.

%%%%%%%% HOW TO? %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

READ CAREFULLY PLEASE

Preliminary steps

i. Adapt the startup.m file and the *.sub files as describe in the
corresponding files. 

ii. In the submit_*_dag.sh, edit the path to your STAMPAS directory.
Follow the example given.

iii. The cachefiles loaded with the pipeline are usable only on the
Caltech and Atlas clusters. If you run STAMPAS on another cluster, you will have to
create the required cachefiles to run your analysis.

HOW TO?

1. Depending on the type of analysis you want to run, open and edit
   the config_bkg.txt or config_inj.txt files. The options are:

   - pair: The pair of detectors you will use

   - cachePath: Path to the cachefiles. By default link to the files present
                in the STAMPAS/cachefiles directory

   - retry: number of tries Condor will make for each job.

   - matfilesLocation: repertory where the preprocessing files will be saved and/or
                       must be found (when already processed). 
                       Default option saves them in the search repertory

   - matfilesName: prefix given to the preprocessing files.

   - genCachefiles: if true, a cachefile containing the path of the preprocessing
                    files is generated as an output of the submit_preproc_dag.sh
                    script.

   - doNodes: if set to true, STAMPAS will look for preprocessed files on different
              nodes

   - nodes_dir: ...

   - doZeroLag : In the case of a background study, this option allows
                 you to analyze the zero-lag if set to 1.

   - doZebra : This parameters activates the zebragard option: the search
               will only be performed on 5 different sky-locations. Faster
               than regular all-sky analysis, but 10% less efficient, and 
               brings no information about sky position.

   - waveforms: path to the file containing the names of the waveforms files
   to use for your studies. 
 
   - nullWaveform: if 1, the segments on which you perform injections
                   are also analyzed withoyt any injection.
                   if 0, only injections are performed.

   - doDgPlots: if you want to save diagnostic ft-maps of the maximal 
                cluster found in a skymap, set to 1. Else set to 0.
 
   - doMC: if 0, you will use real data.
           if 1, you will use simulated data.

   - waveform_path: Path of the repertory in which the waveforms are
                    saved.

   - DetectorNoiseFileLIGO/Virgo: path to the PSD curves used when
                                  doing simulated noise, for LIGO and
                                  Virgo ifos.

   - window: Time length of ft-maps used (for memory consideration,
             it is best not to go beyond 600s for H1L1, and 300s when
             Virgo is implied, unless you change the resolution of the
             search.)

   - fmin/fmax: frequency band you want to investigate.

   - TShifts: the actual time-shift between each different lag, for 
              injections studies. (Tshifts in the background case
              is identical to the window duration by construction)

   - TOffset: introduces an offset of TOffset jobs in timeshifts.
              Useful to divide the timeshifts in two distincts dags.
              if you want to perform 100 shifts total, you can perform
              by set of 50 lags by setting TOffset respectively to 0 and 50
              in two distincts searches, with the other parameters identical.
              If you add a TOffset, the doZeroLag is automatically set to 0. 

   - overlap: overlap time between two consecutive ft-maps. When variable
              windows are one, its value should be small. 

   - FrequenciesToNotch: Path to the files containing the list of 
                         frequencies to notch for the analysis.

   - NumberOfInjections: number of injections per waveform you want to perform.
 
   - NShifts: number of timeslides, zero-lag excluded, you want to perform.

   - denseDAG: if 0, one Condor job is equivalent to one time-window
               if 1, one Condor job is equivalent to one science segment

   - doVariableWindows: if 1, denseDAG is set to 1. If a trigger is
                        detected at the very end of a window, the next window
                        begins at the beginning of the trigger.

   - windowLimit: Limit size of the windows duration one can achieve using
                  the variable windows

   - nbGroupsPerJob: number of science segments processed on each Condor job.

   - noStitching: if false, gaps in the data are removed and data from different
                  segments can be stitched together. If true, the gaps are taken into
                  account. Should be true when variable windows is on.

   - alternative_sigma: if true, the SNR is estimated using the alternative_sigma
                        code (studies on-going).

   - doRandomPolarization: when the signal injected is an ADI, its polarization will 
                           vary randomly for each injection.

   - altInjection: if true, a given injection is done at a random time within three
                   ft-maps. This way the signal can actually be found in-between two
                   maps, and can be treated like actual data.

   - randomInjections: if true, injections jobs are selected randomly over all
                       the possible jobs in the GPS range. If false, the first
                       jobs in this range are selected to do the injections. The 
                       end of the GPS range might not be covered by injections in this
                       case.


2. If need be, you will want to edit the freq_to_notch.txt and waveforms.txt
   files. The first contain a list of frequencies to be notched during the analysis.
   An example file is present in the STAMPAS directory. The second is the list of the
   name of the waveform files you want to use for your injections, along with the
   multiplicative (alpha) factors associated. The amplitude of your waveform will be 
   multiplied by the SQUARE ROOT of the factors you put there. An example is also
   given.

   *** In any case check that no empty lines are present in the file. This would result
   in adding empty jobs in the dag. ***

3. In the submit_*_dag.sh scripts (preproc and bkg OR inj), define GPS start and end of your
   study, as well an ID number used to identify your search. The ID is numerical
   and is used only in the naming of the search repertory.

4. Now you have to submit your dag files to the cluster. If you run 
   a background analysis, run the submit_preproc_dag.sh script first.
   Data will then be pre-processed. Once this is done, uses the submit_bkg_dag.sh
   script; if you run injections, run the submit_inj_dag.sh script. 

   NOTE: When preprocessed data are already available, edit consequently the 
   submit_preproc_dag.sh script, and use it first nonetheless. You'll be able
   to launch the submit_bkg_dag.sh script right after the script is done running.


A search directory, named either BKG/Start-(GPS_start)_Stop-(GPS_end)_ID-(ID)
or INJ/Start-(GPS_start)_Stop-(GPS_end)_ID-(ID), contains .dag files, logs files.... 
associated with your search.

5. Results are stored in txt files, with the following hierarchy:

BKG/Start-(GPS_start)_Stop-(GPS_end)_ID-(ID)/results/results_(lag number)/results_(GPS start of the segment).txt
for bkg studies

INJ/Start-(GPS_start)_Stop-(GPS_end)_ID-(ID)/results/results_(waveform)/results_(GPS start of the segment).txt
for injections

with shift number being different for each time-shift (0 corresponding to the
zero-lag).

The results files are ASCII files with 13 columns, and one line per trigger. 
Columns are:

1) GPS start of the ft-map 
2) RA coordinate of the map
3) DECL coordinate of the map
4) Minimal frequency of the trigger
5) Maximal frequency of the trigger
6) SNR of the trigger
7) Start time of the trigger within the map
   (comprised between 0 and (map_duration) seconds
8) End time of the trigger within the map
9) Max SNRfrac value
10) Time of the column of maximal SNRfrac value
11) Number of pixels in the trigger
12) True GPS start of the ft-map for the second interferometer
13) Lag number

