#! /usr/bin/env bash

path_val='/home/valentin.frey/o1-allsky/trunk/STAMPAS'
weeks[0]=Start-1126569617_Stop-1127174417_ID-3 
weeks[1]=Start-1127174417_Stop-1127779217_ID-1
weeks[2]=Start-1127779217_Stop-1128384017_ID-1
weeks[3]=Start-1128384017_Stop-1128988817_ID-1
weeks[4]=Start-1128988817_Stop-1129593617_ID-1
weeks[5]=Start-1129593617_Stop-1130198417_ID-1
weeks[6]=Start-1130198417_Stop-1130803217_ID-1
weeks_nb=$((${#weeks[@]}-1))

t1=`echo ${weeks[0]} | cut -d "-" -f 2 | cut -d "_" -f 1`
t2=`echo ${weeks[5]} | cut -d "-" -f 3 | cut -d "_" -f 1`

searchPath='Start-'${t1}'_Stop-'${t2}'_ID-1'
echo $searchPath ' ' $weeks_nb ' weeks'
TTime=0
TTimeZeroLag=0
cp -R ${path_val}/BKG/${weeks[0]} BKG/${searchPath}
rm -R BKG/${searchPath}/results/results_merged
for w in $(seq 1 1 ${weeks_nb});do
    echo $w ${searchPath} ' 100 time slides'
    for i in $(seq 1 1 100); do
#	ls ${path_val}/BKG/${weeks[$w]}/results/results_1/
	cp ${path_val}/BKG/${weeks[$w]}/results/results_${i}/* BKG/${searchPath}/results/results_${i}/.
	cp ${path_val}/BKG/${weeks[$w]}/results/glitchCut_${i}/* BKG/${searchPath}/results/glitchCut_${i}/.
    done
    a=`cat ${path_val}/BKG/${weeks[$w]}/tmp/TTime.txt`
    TTime=$(($TTime+$a))
    b=`cat ${path_val}/BKG/${weeks[$w]}/tmp/TTimeZeroLag.txt`
    TTimeZeroLag=$(($TTimeZeroLag+$b))
done

echo $TTime > BKG/${searchPath}/tmp/TTime.txt
echo $TTimeZeroLag > BKG/${searchPath}/tmp/TTimeZeroLag.txt