% Designed to set up the matlab environment for running the
% STAMP All-Sky pipeline.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EDIT THE FOLLOWING:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Path to your stamp2 directory.

ROOT_PATH=pwd;
STAMP2_PATH=strrep(ROOT_PATH,'STAMPAS','stamp2');

% Path to the functions subdirectory of the STAMPAS repertory.
FUNC_DIR = [ROOT_PATH '/functions'];
%FUNC_DIR = [ROOT_PATH '/post_processing'];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

verNum = str2num(version('-release'));
if(~isempty(verNum))
  verOld = verNum < 14;
else
  verOld = false;
end
if(verOld || (~isdeployed))
  
  addpath(genpath(STAMP2_PATH));
  addpath(FUNC_DIR);

  [no_use,h] = system('hostname -d');
  h = h(1:(end-1)); % remove endline
  
  if (strcmpi(h,'ligo.caltech.edu') | strcmpi(h,'ligo-wa.caltech.edu') | ...
      strcmpi(h,'ligo-la.caltech.edu'))
    %addpath('/ligotools/matlab');
    addpath(genpath('/usr/share/ligotools/matlab/'));
  elseif strcmpi(h,'atlas.aei.uni-hannover.de') || strcmpi(h,'atlas.local')
    addpath('/opt/lscsoft/ligotools/matlab');
  end
end
