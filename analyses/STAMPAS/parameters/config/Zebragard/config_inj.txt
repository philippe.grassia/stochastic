% INJECTIONS
inj 1
TSOffset 0
doZeroLag 0
noStitching 0
denseDAG 1

% The pair of ifos you choose. Choices are 'H1L1','H1V1' and 'V1L1'
pair H1L1

% Path to the real data cachefiles
cachePath cachefiles/

% Zebragard search. Set to 0 or 1.
doZebra 1

% Diagnostic plots. Set to 0 or 1.
doDgPlots 0

% Seedless search. Set to 0 or 1.
doSeedless 0

% Seedless all-sky search. Set to 0 or 1.
doSeedlessAllsky 0

% Seedless parameter file
seedlessParams seedless_params.txt

% Seedless search (Lonetrack). Set to 0 or 1.
doLonetrack 0

% Seedless search (Lonetrack post-processing). Set to 0 or 1.
doLonetrackPProc 0

% Seedless search (Lonetrack post-processing FAP). Set to < 1.
lonetrackFAP 0

% Turn on the use of GPUs. Set to 0 or 1
doGPU 0

% Turn on the use of parallel toolbox. Set to 0 or 1
doParallel 0

% Job memory size
jobMemory 2400

% Monte-Carlo data. Set to 0 or 1.
doMC 0

% PSD Files, for LIGO and Virgo.
DetectorNoiseFileLIGO PSDs/LIGOsrdPSD_40Hz.txt 
DetectorNoiseFileVirgo PSDs/VirgoDesign_PSD_only.txt

% FT-map size (Duration, and frequency limits)
window 500 
fmin 40    
fmax 1000

% Overlap between two consecutive ft-map
overlap 250

% File listing frequencies to notch during the pre-processing
FrequenciesToNotch freqs_ER8H1L1_1s_1Hz.txt

% alternative_sigma option
alternative_sigma 0

% Variable windows option (needs altInjection)
doVariableWindows 0

% Limit size of the windows (s)
windowLimit 600

% Randomize injection polarization? (for ADI waveforms only)
doRandomPolarization 1

% Number of injections per waveform
NumberOfInjections 180

% Number of seconds at the beginning and end of the group of jobs
blindZone 10

% Injections jobs are selected randomly in the GPS range
% selected by the user
% The first NumberOfInjections jobs are selected otherwise
randomInjections 1

%  In addition to the waveforms selected, the segments on which the
% injections are done are analyzed without any injection, if true.
nullWaveform 1

% Number of timeslides (excluding zero-lag) used, and time separation 
% (in seconds) between IFO streams. For injections only one lag is used. 
% If NTShifts is 0, injections will be done in the the zero-lag (use only 
% with MC), else the shift will be of TShiftInj.

NTShifts 1
TShiftInj 250

% Timeout between 2 injections
injectionSeparation 260

% File listing waveforms to be used
waveforms waveforms.txt

% Path to the waveforms' repository
waveform_path waveforms/

% Number of retries for each Condor job
retry 1

% ignore gaps between segments
ignoreGaps 0

% GPS start and end time of your search, ID
% Added after running the submission script
