classdef YAML
%YAML  Serialize a matlab variable to yaml format
%
%  [ X ] = YAML.load( S )
%  [ S ] = YAML.dump( X )
%
%  [ X ] = YAML.read( filepath )
%  YAML.write( filepath, X )
%
% YAML.LOAD takes YAML string S and returns matlab variable X.
% YAML.DUMP takes matlab variable X and converts to YAML string S.
% YAML.READ and YAML.WRITE are convenient methods to load and dump
% YAML format directly from a file.
% 
% Examples:
% To serialize matlab object
% 
%   >> X = struct('matrix', rand(3,4), 'char', 'hello');
%   >> S = YAML.dump(X);
%   >> disp(S);
%   matrix:
%   - [0.9571669482429456, 0.14188633862721534]
%   - [0.4853756487228412, 0.421761282626275]
%   - [0.8002804688888001, 0.9157355251890671]
%   char: hello
% 
% To decode yaml string
% 
%   >> X = YAML.load(S);
%   >> disp(X)
%     matrix: [3x2 double]
%       char: 'hello'
%
% See also: xmlread xmlwrite
%
% MODIFIED BY Valentin FREY (frey@lal.in2p3.fr)
%   - allow to load multi document yaml file
% date : fev 2016
%

  properties (Constant)
    JARFILE = classes.utils.yaml.YAML.jarfile
  end

  methods
    function delete(obj)
      javarmpath(classes.utils.yaml.YAML.JARFILE);
    end
  end
  
  methods (Static)
    function [ S ] = jarfile()
    %JARFILE path to the Snakeutils.misc.yaml.YAML jar file
      S = fileparts(mfilename('fullpath'));
      S = [S filesep 'java' filesep 'snakeyaml-1.9.jar'];
    end
    
    function varargout = load( S )
    %LOAD load matlab object from yaml string
      javaaddpath(classes.utils.yaml.YAML.JARFILE);
      
      % Load yaml into java obj
      yaml = org.yaml.snakeyaml.Yaml;
      
      if length(regexp(S,'---'))<2
        java_obj = yaml.load(S);
        varargout{1} = classes.utils.yaml.YAML.load_data(java_obj);
      else
        java_obj = yaml.loadAll(S);
        it=java_obj.iterator;
        i=1;
        while it.hasNext
          varargout{i} = classes.utils.yaml.YAML.load_data(it.next);
          i=i+1;
        end
      end
    end
    
    function [ S ] = dump( X )
    %DUMP serialize matlab object into yaml string
      javaaddpath(classes.utils.yaml.YAML.JARFILE);
      
      % Convert matlab obj to java obj
      yaml = org.yaml.snakeyaml.Yaml();
      java_obj = classes.utils.yaml.YAML.dump_data(X);
      
      % Dump into yaml string
      S = char(yaml.dump(java_obj));
    end
    
    function varargout = read( filepath )
    %READ read and decode yaml data from file
      fid = fopen(filepath,'r');
      S = fscanf(fid,'%c',inf);
      fclose(fid);
      [varargout{1:nargout}] = classes.utils.yaml.YAML.load( S );
    end
    
    function [] = write( filepath, X )
    %WRITE serialize and write yaml data to file
      S = classes.utils.yaml.YAML.dump( X );
      fid = fopen(filepath,'w');
      fprintf(fid,'%s',S);
      fclose(fid);
    end
  end
  
  methods(Static, Access=private)
    function result = load_data( r )
    %LOAD_DATA recursively convert java objects
      if isa(r, 'char')
        n = str2num(r);
        if(~isempty(n) && isnumeric(n))
          result = double(n);
        else
          result = char(r);
        end
      elseif isa(r, 'double')
        result = double(r);
      elseif isa(r, 'java.util.Date')
        result = DateTime(r);
      elseif isa(r, 'java.util.List')
        result = cell(r.size(),1);
        itr = r.iterator();
        i = 1;
        while itr.hasNext()
          result{i} = classes.utils.yaml.YAML.load_data(itr.next());
          i = i + 1;
        end
        result = classes.utils.yaml.YAML.merge_cell(result);
      elseif isa(r, 'java.util.Map')
        result = struct;
        itr = r.keySet().iterator();
        while itr.hasNext()
          key = itr.next();
          result.(char(key)) = classes.utils.yaml.YAML.load_data(...
              r.get(java.lang.String(key)));
        end
      elseif isa(r, 'logical')
        result = logical(r);
      else
        error('classes.utils.yaml.YAML:load_data:typeError',...
              ['Unknown data type: ' class(r)]);
      end
    end
    
    function result = merge_cell( r )
    %MERGE_CELL convert cell array to native matrix
      
    % Check eligibility
      merge = false;
      if all(cellfun(@isnumeric,r))
        merge = true;
      elseif all(cellfun(@isstruct,r))
        f = cellfun(@fieldnames,r,'UniformOutput',false);
        if isempty(f) || (numel(unique(cellfun(@numel,f)))==1 && ...
                          all(cellfun(@(x) all(strcmp(f{1},x)),f)))
          merge = true;
        end
      end
      
      % Merge if scalar or row vector
      result = r;
      if merge
        if all(cellfun(@isscalar,r))
          result = [r{:}];
        elseif all(cellfun(@isrow,r)) &&...
              length(unique(cellfun(@length,r)))==1
          result = cat(1,r{:});
        end
      end
    end
    
    function result = dump_data( r )
    %DUMP_DATA convert 
      if ischar(r)
        result = java.lang.String(r);
      elseif ~isscalar(r)
        result = java.util.ArrayList();
        if size(r,1)==1
          for i = 1:numel(r)
            if iscell(r)
              result.add(classes.utils.yaml.YAML.dump_data(r{i}));
            else
              result.add(classes.utils.yaml.YAML.dump_data(r(i)));
            end
          end
        else
          for i = 1:size(r,1)
            result.add(classes.utils.yaml.YAML.dump_data(r(i,:)));
          end
        end
      elseif isnumeric(r)
        result = java.lang.Double(r);
      elseif isstruct(r)
        result = java.util.LinkedHashMap();
        keys = fields(r);
        for i = 1:length(keys)
          result.put(keys{i},classes.utils.yaml.YAML.dump_data(r.(keys{i})));
        end
      elseif iscell(r)
        result = java.util.ArrayList();
        result.add(classes.utils.yaml.YAML.dump_data(r{1}));
      elseif isa(r,'DateTime')
        result = java.util.Date(datestr(r));
      else
        error('classes.utils.yaml.YAML:load_data:typeError',...
              ['Unsupported data type: ' class(r)]);
      end
    end
    
  end
  
end

