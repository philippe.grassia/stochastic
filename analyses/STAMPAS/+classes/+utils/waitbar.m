classdef (Sealed) waitbar < handle
% waitbar :
% DESCRIPTION :
%
% SYNTAX :
%   [] = waitbar ()
%
% INPUT : 
%
% OUTPUT :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE :
%
    properties(Access = private)
        str
        prefix = '\r'
        warn
    end
    
    methods
        function obj=waitbar(msg)
            persistent wb
            if isempty(wb) || ~isvalid(wb)
                wb = obj;
            end
            obj = wb;
            
            if nargin==1
                obj.print(msg);
            end
        end

        function delete(obj)
            fprintf(sprintf('\n'));
        end        
    end
    
    methods(Static)
        function warning(msg)
            wb=classes.utils.waitbar;
            wb.warn=[wb.warn '\n\t' msg];
            
            wb.print(wb.str);

            wb.prefix=sprintf('\033[%uA\r',length(regexp(wb.warn,'\\n','match')));
        end
    end
    
    methods (Access = private)        
        function print(obj,msg)            
            if ~ischar(msg),error('imput must be a string');end
            obj.str=msg;
            fprintf([obj.prefix, obj.str, obj.warn]);
        end
    end

end

