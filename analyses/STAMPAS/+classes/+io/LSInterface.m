classdef LSInterface  
% LSInterface :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = LSInterface ()
% 
% INPUT : 
%    
% OUTPUT :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  properties
    fragmented=false
    path='./'
    files={}
    instance
  end
  
  methods
    
    function obj=LSInterface()
    end

  end

  methods (Static)
    function obj=loadobj(s)
      if s.fragmented
        for i=1:numel(s.files)          
          m=matfile([s.path '/' s.files{i}]);
          t(i)=m.data;
        end
        obj=sum(t);
      end
    end
  end
end

