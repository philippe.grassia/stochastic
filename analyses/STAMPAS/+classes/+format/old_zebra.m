classdef old_zebra<claesse.format.formatInterface
% zebragard :
% DESCRIPTION :
%
% SYNTAX :
%   [] = zebragard ()
%
% INPUT : 
%
% OUTPUT :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE :
%
    properties
        glitchCut=struct(...
            'time',          {{1 'cell'}},...
            'det' ,          {{2 'cell'}},...
            'frac',          {{3 'cell'}})
        
        ra                  = { 1 'num'}
        dec                 = { 2 'num'}
        fmin                = { 3 'num'}
        fmax                = { 4 'num'}
        SNR                 = struct(...
            'coherent',      {{ 5 'num'}},...
            'ifo1',          {{ 6 'num'}},...
            'ifo2',          {{ 7 'num'}})
        tmin                = { 8 'num'}
        tmax                = { 9 'num'}
        SNRfrac             = {10 'num'}
        SNRfracTime         = {11 'num'}

        nbPix               = {12 'num'}
        lag                 = {13 'num'}
        flag                = {14 'num'}
        F1                  = {15 'num'}
        F2                  = {16 'num'}
        start               = struct(...
            'ifo1',          {{17 'num'}},...
            'ifo2',          {{18 'num'}})
        windowIdx           = {19 'num'}
        windowGroup         = {20 'num'}
    end
end

