classdef zebragard<classes.format.formatInterface
% zebragard :
% DESCRIPTION :
%
% SYNTAX :
%   [] = zebragard ()
%
% INPUT : 
%
% OUTPUT :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE :
%
    properties
        glitchCut=struct(...
            'time',          {{1 'cell'}},...
            'det' ,          {{2 'cell'}},...
            'frac',          {{3 'cell'}})
        
        ra                  = { 1 'num'}
        dec                 = { 2 'num'}
        fmin                = { 3 'num'}
        fmax                = { 4 'num'}
        SNR                 = struct(...
            'coherent',      {{ 5 'num'}},...
            'ifo1',          {{ 6 'num'}},...
            'ifo2',          {{ 7 'num'}})
        NE                  = struct(...
            'std',            struct(...
                'brut',       struct(...
                    'ifo1',  {{ 8 'num'}},...
                    'ifo2',  {{ 9 'num'}}),...
                'norm',       struct(...
                    'ifo1',  {{10 'num'}},...
                    'ifo2',  {{11 'num'}})),...
            'median',         struct(...
                'brut',       struct(...
                    'ifo1',  {{12 'num'}},...
                    'ifo2',  {{13 'num'}}),...
                'norm',       struct(...
                    'ifo1',  {{14 'num'}},...
                    'ifo2',  {{15 'num'}})))
        tmin                = {16 'num'}
        tmax                = {17 'num'}
        SNRfrac =             struct(...
            'coherent',      {{18 'num'}},...
            'ifo1',          {{19 'num'}},...
            'ifo2',          {{20 'num'}})
        SNRfracTime         = {21 'num'}
        nbPix               = {22 'num'}
        lag                 = {23 'num'}
        flag                = {24 'num'}
        F1                  = {25 'num'}
        F2                  = {26 'num'}
        windows             = struct(...
            'start',          struct(...
                'ifo1',      {{27 'num'}},...
                'ifo2',      {{28 'num'}}),...
            'stop',          {{29 'num'}},...
            'idx',           {{30 'num'}},...
            'duration',      {{31 'num'}},...
            'group',          struct(...
                'id',        {{32 'num'}},...
                'index',     {{33 'num'}},...
                'nbWindows', {{34 'num'}},...
                'start',     {{35 'num'}}))
    end
end

