classdef injections<classes.format.formatInterface
% injections :
% DESCRIPTION :
%
% SYNTAX :
%   [] = injections ()
%
% INPUT : 
%
% OUTPUT :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE :
%
    properties
        glitchCut = struct(...
            'time',  {{1 'cell'}},...
            'det',   {{2 'cell'}},...
            'frac',  {{3 'cell'}},...
            'ra',    {{4 'cell'}},...
            'dec',   {{5 'cell'}})

        ra         = { 1 'num'}
        decl       = { 2 'num'}
        startGPS   = { 3 'num'}
        iota       = { 4 'num'}
        psi        = { 5 'num'}
        F1         = { 6 'num'}
        F2         = { 7 'num'}
        SNR1       = { 8 'num'}
        SNR2       = { 9 'num'}
        SNR1s      = {10 'num'}
        SNR2s      = {11 'num'}
        hrss       = {12 'num'}
        start      = struct(...
            'ifo1', {{13 'num'}},...
            'ifo2', {{14 'num'}})
    end
end

