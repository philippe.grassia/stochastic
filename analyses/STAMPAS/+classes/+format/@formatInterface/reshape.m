function reshape(obj)  
% reshape :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = reshape ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

  id_mat  = 1;
  id_cell = 1;
  
  obj.sort;
  s=structfun(@f,obj.toStruct,'UniformOutput',false);  
  cellfun(@assign,fieldnames(s),'UniformOutput',false);


  function assign(x)
    obj.(x)=s.(x);
  end


  function o=f(s) 
    if isstruct(s)
      o=structfun(@f,s,'UniformOutput',false);
      return
    end
    
    if strcmp(s{2},'cell')
      o{1}    = id_cell;
      id_cell = id_cell+1;
    elseif strcmp(s{2},'num')
      o{1}    = id_mat;
      id_mat  = id_mat+1;
    end  
    o{2} = s{2};
  end
end


