function addField(obj,field,type)  
% addField :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = addField ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Mar 2016
%
  idx = obj.maxIdx(type)+1;
  
  if ~isprop(obj,field{1})
    addprop(obj,field{1});
  end
  
  setfield(obj,field{:},{idx type});
  
end

