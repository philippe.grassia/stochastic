function tf = check(obj,varargin)  
% check :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = check ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

tf=true;

switch nargin
  case 1
    h=obj;
    
  case 2
    h=varargin{1};
    
  otherwise
    error('wrong number of input');
end


for i=fieldnames(h)'
  if isstruct(h.(i{:}))
    tf=tf&obj.check(h.(i{:}));
  else
    if ~iscell(h.(i{:}))| ...
          size(h.(i{:}))~=[1 2]| ...
          ~isnumeric(h.(i{:}){1})| ...
          isempty(regexp(h.(i{:}){2},'^(num|cell)$','once'))
      tf=false;
    end
  end
end


return

