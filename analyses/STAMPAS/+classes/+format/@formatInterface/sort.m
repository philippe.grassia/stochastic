function sort(obj)
% sort :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = sort ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.1
% DATE : Fev 2016
%

for i=sort(fieldnames(obj)')
  obj.(i{:})=obj.(i{:});
end

return

