function s=toStruct(obj,varargin)
% toStruct :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = toStruct ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Fev 2016
%

if nargin ==1; h=obj; end
if nargin ==2; h=varargin{1}; end
for i=fieldnames(h)'
  if isstruct(h.(i{:}))
    s.(i{:})=obj.toStruct(h.(i{:}));
  else
    s.(i{:})=h.(i{:});
  end
end

return

