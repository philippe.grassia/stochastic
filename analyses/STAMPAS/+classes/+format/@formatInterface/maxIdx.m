function idx = maxIdx(obj,type,varargin)  
% maxIdx :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = maxIdx ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

idx=0;

if isempty(regexp(type,'^(cell|num)$','once'))
  error('type should be num or cell')
end

switch nargin
  case 2
    frmt=obj;
    
  case 3
    frmt=varargin{1};
    
  otherwise
    error('wrong number of input');
end

for i=fieldnames(frmt)'
  if isstruct(frmt.(i{:}))
    idx=max(idx,obj.maxIdx(type,frmt.(i{:})));
  elseif strcmp(frmt.(i{:}){2},type)
    idx=max(idx,frmt.(i{:}){1});
  end
end
    
return

