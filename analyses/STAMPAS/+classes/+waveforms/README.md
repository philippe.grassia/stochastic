# Dictionary

## Purpose 

the dictionary class is design to access in a easy way to the stampas
waveform. Each waveform have a header where the properties are
defined. The data can be accessible whatever they are generated on the
fly or by reading file

## Initialization
Using matlab you need first to import the dictionary

`>> import classes.waveforms.dictionary`

then you can instanciate the class by typing 

`>> dic = dictionary`

In a more direct way you can concatenate the two previous command into
a single one
`>> dic = classes.waveforms.dictionary`

By default, the dictionary class usr at the dic.yml file located at
the +classes/+waveforms/@dictionary/dic.yml. But you can specify your
own dic.yml file 
`>> dic = dictionary(path/to/myFile)`



## Header

If you want to get the information about a specific waveform you can
call the headers methods. the header methods take as argument the id of the waveforms (eg: adiA), the name of the file (eg: adi_A_tapered.dat). 
`>> dic.headers('adiA')`
Give the output
```
ans = 

  header with properties:

            id: 'adiA'
         group: 'adi'
          file: 'adi_A_tapered.dat'
          type: 'ascii'
        legend: 'ADI A'
        energy: 0.0214
          hrss: 9.2210e-21
          fmin: 135
    parameters: [1×1 struct]
      distance: 1
          fmax: 166
        xlabel: 'distance'
      duration: 38.7798
```

You can also get all waveforms information of a group of waveforms
using the group name
`>> dic.headers('adi')`
given you all the information from the adi waveforms
```
ans = 

  6×1 header array with properties:

    id
    group
    file
    type
    legend
    energy
```

## Data 
You can also get the data of a waveform using the wavefoms method. In
the same way as the header method, you can the id of the waveform file
name
`>> dic.waveforms('adiA')`

Next you can access to hp and hx method to the data of the waveform.
If you encounter trouble. Please check the waveform data file are
accessible for matlab (use the addpath to add the path of the .dat
file). If the waveform are generated on the fly. The function to
generate the data need also to bve accessible to matlab


## dic.yml 
The dic.yml is a config using the [yaml](http://yaml.org/)
syntax that summarize all the information about the stampas
waveforms. At any time you can add by hand new waveform.
