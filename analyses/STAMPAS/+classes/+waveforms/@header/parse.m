function parse(obj,s)  
% parse : parse the header obj
% DESCRIPTION :
%   parse the header object with the given struct
%
% SYNTAX :
%   parse (obj,s)
% 
% INPUT : 
%   obj : the waveform's headers
%     s : the struct containing the waveform information
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%
  cellfun(@(k) addHeaderField(obj,k,s.(k)),fieldnames(s),'UniformOutput',false);
end

function addHeaderField(obj,k,v)
  if ~isprop(obj,k)
    p=addprop(obj,k);
  end

  if ~strcmp(getfield(findprop(obj,k),'SetAccess'),'none')
    obj.(k)=v;
  end
end

