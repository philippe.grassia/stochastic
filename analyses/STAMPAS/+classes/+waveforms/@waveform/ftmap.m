function ftmap(obj,params)  
% plot : mahe a ftmap of the waveform
% DESCRIPTION :
%    plot the ftmap of the  waveforms ftmap
%
% SYNTAX :
%   [] = ftmap(obj)
% 
% INPUT : 
%    obj : the waveform
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%
  import classes.config
  

  %% GET DEFAULT VALUES
  if isprop(obj,'dt')
    dt = obj.dt;
  else
    dt = 1;
  end

  if isprop(obj,'df')
    df = obj.df;
  else
    df = 1;
  end

  if isprop(obj,'buffer')
    buffer = obj.buffer;
  else
    buffer = 2; %[s]
  end

  %% PARSE THE PARAMS 
  if nargin>1
    for k=fieldnames(params)'
      switch k{:}
        case 'dt'
          dt = params.(k{:});

        case 'df'
          df = params.(k{:});

        case 'buffer'
          buffer = params.(k{:});

      end
    end
  end

  if dt*df ~=1
    error(['wrong resolution [' num2str(dt) ' ' num2str(df) ...
          '] you should have df*dt=1'])
  end
  data = [obj.time obj.hx obj.hp];

  %% ADD BUFFER TIME
  data=[ [[-buffer:obj.deltaT:0]', ...
          zeros(floor(buffer/obj.deltaT)+1,1),...
          zeros(floor(buffer/obj.deltaT)+1,1)  ];data];

  data=[data; ...
        [[data(end,1):obj.deltaT:data(end,1)+buffer]', ...
         zeros(floor(buffer/obj.deltaT)+1,1), ...
         zeros(floor(buffer/obj.deltaT)+1,1)]];
  

  NT     = ceil(dt*1/obj.deltaT);
  NF     = 2^nextpow2(1/obj.deltaT/df-1);
  Max    = 2*ceil(size(data,1)/NT)-1;  % x 2 dur to overlap
  map    = zeros(NF/2,Max);
  time   = linspace(-buffer,data(end,1),Max);
  w      = hann(NF);

  % ----------------------------------------------------------
  %% FTmap
  % ----------------------------------------------------------
  %
  % loop over the segment, for each segment taper the data using a
  % hann windows then use fft.
  %
  for i=1:Max
    d=zeros(NT,1);
    dp = data(floor((i-1)*NT/2+1):min((i+1)*floor(NT/2),size(data,1)),2);
    dx = data(floor((i-1)*NT/2+1):min((i+1)*floor(NT/2),size(data,1)),3);
    d(1:min(NT,length(dx)),1)=dx+dp;

    [map(:,i),freq]=pwelch(d,w,NF/2,NF-1,ceil(1/obj.deltaT),'onesided');
  end

  %% save data and plot the FTmap
  % ----------------------------------------------------------
  %
  if isfield(params,'saturation')
    S = params.saturation;
  elseif isprop(obj,'saturation')
    S = obj.saturation;
  else
    S = max(map(:));
  end

  if obj.fmin ~= obj.fmax
    YMIN = max(obj.fmin-0.2*(obj.fmax-obj.fmin),0);
    YMAX = min(obj.fmax+0.2*(obj.fmax- obj.fmin), NF);
  else
    YMIN = obj.fmin-50 ;
    YMAX = obj.fmin+50;
  end
  XMIN = -buffer;
  XMAX = obj.duration+buffer;
  
  figure
  colormap autumn

  %% === MAIN PLOT === %%
  %% ================= %%
  ax0=gca;
  imagesc(time,freq,map,[0 S]);

  ax0_pos = get(ax0,'Position');
  ax0_pos = [ax0_pos(1) ax0_pos(2)+0.25*ax0_pos(4) 0.7*ax0_pos(3) 0.7*ax0_pos(4)];
  set(ax0,'Position',ax0_pos);
  set(gca,'YDir','normal');
  xlim([XMIN XMAX])
  ylim([YMIN YMAX])

  title(obj.id)
  ylabel('frequency [Hz]')
  
  set(gca,'XTick',[])
  set(gca,'FontSize',16);
  set(get(gca,'Title'),'FontSize',20);
  set(get(gca,'XLabel'),'FontSize',18);
  set(get(gca,'YLabel'),'FontSize',18);

  c  = colorbar();
  c_pos = get(c,'Position');
  set(c,'Position',[ax0_pos(1)+1.35*ax0_pos(3) ax0_pos(2) c_pos(3) ax0_pos(4)]);
  set(get(c,'YLabel'),'String','hrss');
  set(get(c,'YLabel'),'FontSize',18);



  %% === SUB PLOT 1 === %%
  %% ================== %%
  c = freq>YMIN &freq < YMAX;
  D = sum(map(c,:),2);
  BV=min(D);

  ax1 = axes('Position',[ax0_pos(1)+ax0_pos(3) ax0_pos(2)  ...
                      0.3*ax0_pos(3) ax0_pos(4)]);

  linkaxes([ax0,gca],'y')

  barh(freq(c),D,'FaceColor','r', 'EdgeColor','r','BaseValue',BV)
  xlim([BV 10*max(D)])
  xlabel('log_{10}(\int_{f} hrss)')

  grid minor

  set(gca,'Xscale','log')
  set(gca,'YTickLabel',[])
  set(gca,'XTickLabel',[])
  set(gca,'FontSize',16);
  set(get(gca,'Title'),'FontSize',20);
  set(get(gca,'XLabel'),'FontSize',14);
  

  %% === SUB PLOT 2 === %%
  %% ================== %%
  D = sum(map,1);
  BV=min(D(find(D>0)));

  ax2=axes('Position',[ax0_pos(1) ax0_pos(2)-0.3*ax0_pos(4) ...
            ax0_pos(3) 0.3*ax0_pos(4)]);
  linkaxes([ax0,gca],'x')

  bar(time,D, 'FaceColor','r', 'EdgeColor','r','BaseValue',BV);

  set(gca,'Yscale','log')
  xlabel('time [s]')
  ylabel('log_{10}(\int_{t} hrss)')
  ylim([BV 10*max(D)])

  grid minor

  set(gca,'YTickLabel',[]);
  set(gca,'FontSize',16);
  set(get(gca,'Title'),'FontSize',20);
  set(get(gca,'XLabel'),'FontSize',18);
  set(get(gca,'YLabel'),'FontSize',14);
    

  %% === TEXT INFO  === %%
  %% ================== %%
  ax3=axes('Position',[ax0_pos(1)+1.05*ax0_pos(3) ax0_pos(2)-0.3*ax0_pos(4) ...
                      0.2*ax0_pos(3) 0.2*ax0_pos(4)]);
  str=sprintf('fmin        : %.0f\nfmax       : %.0f\nduration \t: %.0f',...
              obj.fmin,obj.fmax,obj.duration);
  t=text(0,0,str);
  set(t,'FontSize',20);
  set(t,'EdgeColor','k');
  set(ax3,'Visible','off');

  if isfield(params,'file')
    print('-dpng',params.file);
  end

end

