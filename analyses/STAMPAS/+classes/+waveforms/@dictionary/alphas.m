function al = alphas(obj)  
% alphas :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = alphas ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  try
    al=arrayfun(@(h) h.alphas,obj.data,'UniformOutput',false);
  catch
    error(['no alphas saved in the dictionary. Use the parse methods ' ...
           'to include alpas']);
end


