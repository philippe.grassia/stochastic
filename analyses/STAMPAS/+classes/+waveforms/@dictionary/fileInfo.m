function [f] = fileInfo(obj,w)  
% fileInfo : return the waveform's file information
% DESCRIPTION :
%   return the waveform's file list for each waveform in the
%   dictionary or for some specified waveform
% SYNTAX :
%   [f] = fileInfo (obj,w)
% 
% INPUT : 
%   obj : the dictionary
%   w [OPTIONAL] : the name or the group of a specified waveforms   
%
% OUTPUT :
%   f : the waveform's file information
% 
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%  
  if nargin==1
    f={obj.data.file};
  else
    if sum(strcmp(obj.group,w))
      f = {obj.data(strcmp({obj.data.group},w),:).file};
    elseif sum(strcmp(obj.id,w))
      f = {obj.data(strcmp({obj.data.id},w),:).file};
    else
      error([w 'not found in the dictionary. Try waveform group input or a ' ...
             'waveform file input'])
    end
  end 
end
