function grp = group(obj,w)  
% group : return the waveforms group list 
% DESCRIPTION :
%    return the group of all the wavform present in the dictionary
%    or the group of some specified waveform.
%
% SYNTAX :
%   [grp] = group (obj,[w])
% 
% INPUT : 
%    obj : the dictionary
%    w [OPTIONAL] : the id or the file of a waveform eg : adiA or
%                   adi_A_tapered
%
% OUTPUT :
%    grp : the group string
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.2
% DATE : Jan 2016
%
  if nargin == 1
    grp={obj.data.group};
  else
    if sum(strcmp(obj.id,w))
      grp={obj.data(strcmp({obj.data.id},w)).group};
    elseif sum(strcmp(obj.fileInfo,w))      
      grp={obj.data(strcmp({obj.data.file},w)).group};
    else      
      error([w ' not found in the dictionary. Try to give a waveform ' ...
             'id or a waveform file']);
    end
  end
  grp=unique(grp);
end


