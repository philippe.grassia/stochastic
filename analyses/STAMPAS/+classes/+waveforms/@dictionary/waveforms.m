function [wvfs] = waveforms(obj,w)  
% waveforms : return the waveform
% DESCRIPTION :
%   get the waveform class from all waveform present in the
%   dictionary or for specified waveforms.
%   waveform are object of the class classes.waveforms.waveform
%   if you only need information on the waveform use the headers
%   methods
%
% SYNTAX :
%   [wvf] = waveforms (obj,w)
% 
% INPUT : 
%    obj : the dictionary
%    w [OPTIONAL] :waveform id, group, file
%
% OUTPUT :
%    wvf : the waveform class
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Jun 2016
%
  import classes.waveforms.waveform
  
  if nargin==1
    wvfs=arrayfun(@(h) waveform(h),obj.headers,'UniformOutput',false);
  else
    try
      wvfs=arrayfun(@(h) waveform(h),obj.headers(w),'UniformOutput',false);
    catch ME
      error(ME.message)
    end
  end  
  wvfs=[wvfs{:}];
end