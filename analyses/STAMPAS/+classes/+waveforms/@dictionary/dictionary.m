classdef dictionary < handle
% dictionary : class that manage stampas waveforms   
% DESCRIPTION :
%   provide all the information concerning the waveform. In order
%   to increase the spped of the code. this information are store
%   in a dic.yml file. this file can be at any time update or
%   regenerate using some appropriate methods  
%
% SYNTAX :
%   dic = dictionary (file)
% 
% INPUT : 
%    file [optional] : make a dictionary using the dic file
%                      if no input is given take the standard
%                      dic.yml file
%
% OUTPUT :
%    dic : dictionary object
%
% PROPERTIES :
%   PUBLIC :
%     file : dictionary file 
%     nbWvf : total number of waveforms
%     nbAlphas : total number of alphas 
%
%   PRIVATE :
%     data : dataset that store waveform information
% 
% METHODS :
%   PUBLIC :
%      waveforms : return array of waveform
%      id : return the id of the waveform
%      group : return the group of the waveform
%      alphas : return  array of waveform's alphas
%
%   STATIC : 
%      parse : parse the dictionay with waveform.txt file
%      dicfile : std dic file path
%     
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Mar 2016
%

  properties (Access = private)
    data=[];
  end
  
  properties
    file
    nbWvf    = 0
    nbAlphas = 0
  end
  

  methods
    
    function obj=dictionary(file)
      
      import classes.utils.yaml.YAML
      
      if nargin==0
        file=obj.dicfile;
      elseif ~exist(file)
        error([file ' doesn''t exist']);
      end
      
      obj.file  = file;
      obj.filldata(YAML.read(file));
      obj.nbWvf = size(obj.data,1);
      
    end      
  end
  
  methods(Static)
    function S = dicfile()
      S= fileparts(mfilename('fullpath'));
      S = [S '/dic.yml'];
    end
    
    function obj=instance()
      obj=classes.waveforms.dictionary;
      obj.data   = [];
      obj.nbWvf  = [];
      obj.nbAlphas = [];
      obj.file   = [];
    end
    
    function obj=parse(file)

      % assuming that the file is is a dag to get to the searchPath
      idxw = strfind(file, '/');
      searchPath = file(1:idxw(end)-1);

      % If continuum injection run then, we will load the temporarily generated dictionary instead
      dict_ci_path = [searchPath, '/ContWaveforms/dic_ci.yml'];
      if exist(dict_ci_path)==2
	  dictionary = classes.waveforms.dictionary(dict_ci_path);
      else
	  dictionary =  classes.waveforms.dictionary;
      end
      
      if ~exist(file)
        error([file ' doesn''t exist']);
      end
      
      dic  = dictionary();
      obj = dictionary.instance;
      
      fid = fopen(file);
      
      % extract waveforms used from the waveforms.txt
      l=fgetl(fid);
      while ischar(l)
        if ~isempty(regexp(l,'^%','match','once'))
          l=fgetl(fid);
          continue
        end
        
        if isempty(l)
          l=fgetl(fid);
          continue;
        end

        % extract wvf name & alphas
        wvf=strtrim(regexprep(l,'(\w*) [0-9]*.*','$1'));
        alphas = cell2mat(...
            cellfun(@str2num,...
                    strsplit(regexprep(l,'([0-9]*)%.*','$1')),...
                    'UniformOutput',false));
        
        % get the wvf info from dictionary
        t=dic.data(strcmp(dic.id,dic.id(wvf)));
        if ~isprop(t,'alphas')
          addprop(t,'alphas');
        end
        t.alphas=alphas;
        
        % append it to the new dictionary
        obj.data=[obj.data;t];

        % change line
        l=fgetl(fid);
        
      end
      fclose(fid);

      obj.nbWvf    = size(obj.data,1);
      obj.nbAlphas = sum(cellfun(@numel,obj.alphas));
    end
  end

  methods (Access = private)

    function filldata(obj,s)
      import classes.waveforms.header;
      for grp=fieldnames(s)'
        info=s.(grp{:}).info;
        m=rmfield(s.(grp{:}),'info');
        c=[];
        for id=fieldnames(m)'
          wvf=s.(grp{:}).(id{:});
          wvf.id=id{:};
          wvf.group=grp{:};
          wvf = cellfun(@(k) setfield(wvf,k,info.(k)),fieldnames(info),'UniformOutput',false);
          obj.data=[obj.data;header(wvf{:})];
        end
      end
    end

  end

end
