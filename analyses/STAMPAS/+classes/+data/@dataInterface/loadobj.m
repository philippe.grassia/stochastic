function obj = loadobj(s)  
% loadobj :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = loadobj ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

  try
    obj      = feval(s.instance,s.format);
  catch
    obj      = feval(s.class,s.format);
  end
  obj.data = s.data;
  obj.cell = s.cell;
  obj.numElements = size(s.data,1);

  if isfield(s,'lonetrack')
    obj.addprop('lonetrack');
    obj.lonetrack.ifo1=obj.loadobj(s.ifo1);
    obj.lonetrack.ifo2=obj.loadobj(s.ifo2);
  end
end

