classdef (Abstract) dataInterface<dynamicprops
% dataInterface :
% DESCRIPTION :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
   properties(Access = protected)
     cell        = {}
     data        = []
     idx_data    = 1 
     idx_cell    = 1
     numElements = 0
     format
   end

   methods(Abstract)
     cobj=getInstance(obj)
   end

   methods(Static)
     obj=loadobj(s);
   end

   methods

     function obj = set.format(obj,v)

     % check if input is a rigth format class
       if ~isa(v,'classes.format.formatInterface')
         error('format should be a classes.format.formatInterface obj');
       end
       
       % Check format input 
       try
         v.check();
       catch
         fprintf('cannot check format... abord');
         rethrow(lasterror);
         return
       end
       
       % assign th format to the input
       obj.format=v;
       
       % add properties from the format
       for i=fieldnames(obj.format)'
         p=addprop(obj,i{:});
         p.Dependent = true;
         p.GetMethod = @(obj) getProp(obj,obj.format.(i{:}));
       end

       % fill array&cell with empty value
       obj.data = ones(0,obj.format.maxIdx('num'));
       obj.cell = cell(1,obj.format.maxIdx('cell'));
     end

   end
end

