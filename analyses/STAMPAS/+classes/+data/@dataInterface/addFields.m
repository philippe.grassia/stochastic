function addFields(obj,s,varargin)  
% addFields :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = addFields ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%

  if ~isstr(s);error('fields need to be string');end
  s=strsplit(s,'.');    

  switch nargin
    case {1, 2}
      type   = 'num';
      values = zeros(obj.numElements,1);

    case 3
      v=varargin{1};
      switch class(v)
        case 'char'
          if strcmp(v,'num')
            type   = v;
            values = zeros(obj.numElements,1);
            
          elseif strcmp(v,'cell')
            type   = v;
            values = {zeros(obj.numElements,1)};
            
          else
            error('type must be num or cell');    
          end

        case {'double','logical'}
          if length(v)==1;v=repmat(v,obj.numElements,1);end
          if length(v)~=obj.numElements;
            error('wrong data size');
          end        
          type   = 'num';
          values = v;

        case 'cell'
          if length(v)~=obj.numElements;
            error('wrong data size');
          end
          type   = 'cell';
          values = v;

        otherwise
          error('bad input')
      end

    case 4
      type   = varargin{1};
      values = varargin{2};
      
  end

  if size(values,1)~=obj.numElements
    values=values';
  end

  % add data to the array 
  if strcmp(type,'num')
    try
      obj.data(:,getfield(obj.format,s{:},{1}))=values;
      return
    catch 
      obj.data=[obj.data,values];
      obj.idx_data=obj.idx_data+1;
    end
    
  elseif strcmp(type,'cell')
    try
      obj.cell(:,getfield(obj.format,s{:},{1}))=values;
    catch
      obj.cell=cat(2,obj.cell,values);
      obj.idx_cell=obj.idx_cell+1;
      return
    end
  end
  
  
  % add field to the object & format
  if ~isprop(obj,s{1})
    p = addprop(obj,s{1});
  end
  
  obj.format.addField(s,type);
  p = findprop(obj,s{1});
  p.GetMethod = @(obj) getProp(obj,obj.format.(s{1}));
end

