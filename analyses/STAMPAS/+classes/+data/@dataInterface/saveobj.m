function s = saveobj(obj)  
% saveobj : save object into mat file
% DESCRIPTION :
% 
% SYNTAX :
%   [] = saveobj ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Mar 2016
%  
  s.instance = class(obj);
  s.data     = obj.data;
  s.cell     = obj.cell;
  s.format   = obj.format;
  
  if isprop(obj,'lonetrack')
    s.lonetrack=true;
    s.ifo1=saveobj(obj.lonetrack.ifo1);
    s.ifo2=saveobj(obj.lonetrack.ifo2);   
  end

end

