function r = plus(a,b)  
% plus :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = plus ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  if a.numElements==0; r=b;return; end
  if b.numElements==0; r=a;return; end
  %if ~isequal(a.format,b.format);error('obj need to have the same format');end
  r=a.getInstance();

  r.data=[a.data;b.data];
  r.cell=cat(1,a.cell,b.cell);
  
  r.numElements=a.numElements+b.numElements;
return

