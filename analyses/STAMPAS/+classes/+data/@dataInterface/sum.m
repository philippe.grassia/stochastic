function r = sum(varargin)  
% sum :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = sum ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
if nargin==1;
  if length(varargin{1})==1
    r=varargin{1};
  else
    r=varargin{1}(1);
    r.data=cat(1,r.data,varargin{1}(2:end).data);
    r.cell=cat(1,r.cell,varargin{1}(2:end).cell);
    r.numElements=size(r.data,1);
  end
  return;
end

r=varargin{1}+sum(varargin{2:end});
return

