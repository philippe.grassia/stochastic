function [m] = memory(obj)  
% memory :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = memory ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

  props = properties(obj);
  m=sum(cellfun(@bytes,props));
    
  function m=bytes(p)
    tmp = getfield(obj,p); 
    s = whos('tmp'); 
    m = s.bytes; 
  end

end

