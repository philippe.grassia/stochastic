function [r] = plus(a,b)  
% plus :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = plus ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%
  if a.numElements==0; r=b;return; end
  if b.numElements==0; r=a;return; end

  switch class(a.format)
    case 'classes.format.seedless'
      r = plus@classes.data.dataInterface(a,b);
      
      if isprop(a,'lonetrack')
        r.lonetrack.ifo1 = plus@classes.data.dataInterface(a.lonetrack.ifo1,b.lonetrack.ifo2);
        r.lonetrack.ifo2 = plus@classes.data.dataInterface(a.lonetrack.ifo2,b.lonetrack.ifo2);
      end
      
    case 'classes.format.zebragard'
      r = plus@classes.data.dataInterface(a,b);
  end
end

