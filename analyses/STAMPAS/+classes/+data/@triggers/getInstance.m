function I = getInstance(obj)  
% getInstance :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = getInstance ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

  I=classes.data.triggers(obj.format);
  
  
  if isprop(obj,'lonetrack')
    addprop(I,'lonetrack');
    I.lonetrack.ifo1 = classes.data.triggers('seedless');
    I.lonetrack.ifo2 = classes.data.triggers('seedless');
  end
  
  
return

