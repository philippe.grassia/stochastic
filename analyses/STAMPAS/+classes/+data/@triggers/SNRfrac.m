function [v] = SNRfrac(obj,varargin)  
% SNRfrac : get SNRfrac of the trigger
% DESCRIPTION :
%   By default get the SNRfrac coherent of the triggers. if ifo1 or
%   ifo2 specified then return the corresponding ifo SNR
%  
% SYNTAX :
%   [] = SNRfrac (obj,varargin)
% 
% INPUT : 
%    obj : triggers
%    varargin : ifo pr ifo2
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jul 2016
%
if nargin==1
  if isstruct(obj.format.SNRfrac)
    v=obj.data(:,obj.format.SNRfrac.coherent{1});
  else
    v=obj.data(:,obj.format.SNRfrac{1});
  end
    
elseif nargin == 2
  if isstruct(obj.format.SNRfrac)
    if strcmp(varargin{1},'ifo1')
      v=obj.data(:,obj.format.SNRfrac.ifo1{1});
    elseif strcmp(varargin{1},'ifo2')
      v=obj.data(:,obj.format.SNRfrac.ifo2{1});
    elseif strcmp(varargin{1},'coherent')
      v=obj.data(:,obj.format.SNRfrac.coherent{1});
    else
      error('wrong ifo');
    end
  else
    error('no SNRfrac ifo1 or ifo2 for these triggers')
  end
end

end

