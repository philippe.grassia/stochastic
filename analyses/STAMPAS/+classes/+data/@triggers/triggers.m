classdef triggers<classes.data.dataInterface
% triggers :
% DESCRIPTION :
%
% SYNTAX :
%   [] = triggers ()
%
% INPUT : 
%
% OUTPUT :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE :
%
  methods
    
    function obj=triggers(s,f)        
      import classes.data.*

      %---------------------------------------------------------
      %% parse input 
      %---------------------------------------------------------
      % check input 
      % 
      switch nargin
        case 1 
          frmt=s;

        case 2 
          frmt=f;
          
        otherwise
          error('wrong number of input');
      end

      %---------------------------------------------------------
      %% set format 
      %---------------------------------------------------------
      % if frmt is a string -> get the classes.format corresponding
      % else check it's a good format
      %
      if ischar(frmt)
        switch frmt
          case 'seedless'
            obj.format=classes.format.seedless;
            
          case 'lonetrack'
            obj.format=classes.format.seedless;
            addprop(obj,'lonetrack');
            obj.lonetrack.ifo1=triggers('seedless');
            obj.lonetrack.ifo2=triggers('seedless');
            
          case 'zebragard'
            obj.format=classes.format.zebragard;
        end
        
      elseif isa(frmt,'classes.format.formatInterface')  
        obj.format=frmt;
        
      else
        error(['first input must be an '...
               'classes.format.formatInterface object']);
        
      end
          
      %---------------------------------------------------------
      %% parse data 
      %---------------------------------------------------------
      % add data to the object 
      %
      if nargin==2 
        try
          obj.addElements(s);
        catch
          error(['cannot parse data using first input. please ' ...
                 'read the doc']);
        end
      end

    end

    function v=numTriggers(obj)
      v=obj.numElements;
    end
    
  end
end

