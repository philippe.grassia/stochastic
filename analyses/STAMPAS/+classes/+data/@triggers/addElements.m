function addElements(obj,s,varargin)  
% addElements :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = addElements ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%  
  arg={obj,s};
  if nargin>2;arg=[arg,varargin{:}];end
  addElements@classes.data.dataInterface(arg{:});
  
  if isprop(obj,'lonetrack') && nargin==2
    obj.lonetrack.ifo1.addElements(s.lonetrack.ifo1);
    obj.lonetrack.ifo2.addElements(s.lonetrack.ifo2);
  end
end

