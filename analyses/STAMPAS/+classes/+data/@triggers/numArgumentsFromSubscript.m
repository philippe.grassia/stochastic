function n = numArgumentsFromSubscript(obj,S,indexingContext)  
% numArgumentsFromSubscript :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = numArgumentsFromSubscript ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%

  import matlab.mixin.util.IndexingContext
  if (indexingContext ~= IndexingContext.Assignment)
    n=1;
  end

end