function [dur] = duration(obj)  
% duration :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = duration ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  dur = obj.tmax-obj.tmin;
end

