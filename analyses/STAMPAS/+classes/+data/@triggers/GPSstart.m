function v = GPSstart(obj,varargin)  
% GPSstart :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = GPSstart ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

switch nargin
  case 1
    ifo='ifo1';

  case 2 
    if strcmp(varargin,'ifo1')|strcmp(varargin,'ifo2')
        ifo=varargin{1};
    else
        error('wrong number of input');
    end
    
  otherwise
    error('wrong number of input');
end

v=obj.windows.start.(ifo)+obj.tmin;

return

