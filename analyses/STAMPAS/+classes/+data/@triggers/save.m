function [] = save(obj,file)  
% save :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = save ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  import classes.io.LSInterface;

  MEM_MAX=2^31;

  if obj.memory>MEM_MAX
    N     = ceil(obj.memory/MEM_MAX);
    Ndata = ceil(obj.numTriggers/N);

    [path,file] = fileparts(file);
    if isempty(path);path='.';end

    file2load={};

    if ~exist([path '/' file])
      system(['mkdir ' path '/' file]);
    end

    for ii=1:N
      Nfile = [regexprep(file,'.mat','') '_' num2str(ii) '.mat'];
      file2load=[file2load,Nfile];
      
      data = obj.getInstance;
      data.data = obj.data(Ndata*(ii-1)+1:min(Ndata*ii,obj.numTriggers),:);
      data.cell = obj.cell(Ndata*(ii-1)+1:min(Ndata*ii,obj.numTriggers),:);
      data.numElements = Ndata;
      
      if isprop(obj,'lonetrack')
        data.lonetrack.ifo1 =  obj.lonetrack.ifo1.getInstance;
        data.lonetrack.ifo1.data = obj.lonetrack.ifo1.data(Ndata*(ii-1)+1:min(Ndata*ii,obj.numTriggers),:);
        data.lonetrack.ifo1.cell = obj.lonetrack.ifo1.cell(Ndata*(ii-1)+1:min(Ndata*ii,obj.numTriggers),:);
        data.lonetrack.ifo1.numElements = Ndata;

        data.lonetrack.ifo2 =  obj.lonetrack.ifo2.getInstance;
        data.lonetrack.ifo2.data = obj.lonetrack.ifo2.data(Ndata*(ii-1)+1:min(Ndata*ii,obj.numTriggers),:);
        data.lonetrack.ifo2.cell = obj.lonetrack.ifo2.cell(Ndata*(ii-1)+1:min(Ndata*ii,obj.numTriggers),:);
        data.lonetrack.ifo2.numElements = Ndata;
      end

      save([path '/' file '/' Nfile],'data');
    end
    
    data = LSInterface;
    data.fragmented = true;
    data.instance = class(obj);
    data.files    = file2load;
    data.path     = [pwd '/' path '/' file];
    save([path '/' file],'data');
    
    return
  end

  data = obj;
  save(file,'data');

end

