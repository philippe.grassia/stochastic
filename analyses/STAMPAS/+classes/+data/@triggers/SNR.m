function [v] = SNR(obj,varargin)  
% SNR : get the SNR of the triggers
% DESCRIPTION :
%   By default get the SNR coherent of the triggers. if ifo1 or
%   ifo2 specified then return the corresponding ifo SNR
%
% SYNTAX :
%   [v] = SNR (obj,varargin)
% 
% INPUT : 
%    obj : the triggers
%    varargin : ifo1 or ifo2
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%

if nargin==1
  if isstruct(obj.format.SNR)
    v=obj.data(:,obj.format.SNR.coherent{1});
  else
    v=obj.data(:,obj.format.SNR{1});
  end
    
elseif nargin == 2
  if isstruct(obj.format.SNR)
    if strcmp(varargin{1},'ifo1')
      v=obj.data(:,obj.format.SNR.ifo1{1});
    elseif strcmp(varargin{1},'ifo2')
      v=obj.data(:,obj.format.SNR.ifo2{1});
    end
  else
    error('no SNR ifo1 or ifo2 for these triggers')
  end
end

end