function r = plus(a,b)  
% plus :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = plus ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jan 2016
%

  if a.numElements == 0;r=b;return;end
  if b.numElements == 0;r=a;return;end
  
  if ~strcmp(a.name,b.name)||~strcmp(a.inj_type,b.inj_type)|| a.alpha~=b.alpha
    error(['cannot cat two injection that have not the same wvf, ' ...
           'alphas, and type'])
  end

  r=plus@classes.data.dataInterface(a,b);
  
  r.name     = a.name;
  r.inj_type = a.inj_type;
  r.alpha    = a.alpha;
  r.fmin     = a.fmin;
  r.fmax     = a.fmax;
  r.duration = a.duration;

end