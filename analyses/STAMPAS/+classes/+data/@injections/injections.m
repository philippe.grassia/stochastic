classdef injections<classes.data.dataInterface
% injections :
% DESCRIPTION :
%
% SYNTAX :
%   [] = injections ()
%
% INPUT : 
%
% OUTPUT :
%
% PROPERTIES:
%
% METHODS:
%
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE :
%
  properties
    name     = ''
    inj_type = ''
    fmin     = 0
    fmax     = 0
    duration = 0
    alpha    = 0
  end

  methods
    function obj=injections(s)
      obj.format=classes.format.injections;          
      
      if nargin>0&isstruct(s)
        obj.name     = s.name;
        obj.inj_type = s.inj_type;
        obj.alpha    = s.alpha;
        obj.fmin     = s.fmin;
        obj.fmax     = s.fmax;
        obj.duration = s.duration;
        obj.addElements(s);
      end
    end

    function obj=set.fmin(obj,v)
      if ~isnumeric(v);v=str2num(v);end
      obj.fmin=v;
    end

    function obj=set.fmax(obj,v)
      if ~isnumeric(v);v=str2num(v);end
      obj.fmax=v;
    end

    function obj=set.duration(obj,v)
      if ~isnumeric(v);v=str2num(v);end
      obj.duration=v;
    end

    function obj=set.alpha(obj,v)
      if ~isnumeric(v);v=str2num(v);end
      obj.alpha=v;
    end
  end

  methods (Static)
    obj=loadobj(s);
  end
end

