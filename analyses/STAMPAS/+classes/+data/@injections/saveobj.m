function s = saveobj(obj)  
% saveobj :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = saveobj ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  
  s = saveobj@classes.data.dataInterface(obj);
  s.name     = obj.name;
  s.inj_type = obj.inj_type;
  s.fmin     = obj.fmin;
  s.fmax     = obj.fmax;
  s.duration = obj.duration;
  s.alpha    = obj.alpha;

end

