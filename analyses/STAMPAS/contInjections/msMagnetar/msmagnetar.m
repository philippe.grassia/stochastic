function [hp, hc, t, f] = msmagnetar(f0,tau,nn,epsilon,duration,fs,T0,tstart,tend)

dt = 1/fs;
t = T0:dt:(T0+duration);
t = t';

f = f0 * (1.0+ t / tau).^(1.0/(1-nn));

II = 1e45;
%G = 6.67408*1e-11;
G = 6.67408*1e-8; % cgs
%c = 3*10.^8;
c = 3*10.^10; %cm/s
%dd = 40*3.0857e22; %m
dd = 40*3.0857e24; %cm
Phi0 = 0.0;
iota = 0.0;

consts = 4. * pi^2 * G / c.^4;
h_0 = consts * II * epsilon .* f.^2 / dd;

term1 = 2.0* pi .* tau .* f0 .* (1.0-nn) / (2.0-nn);
term2 = (1.+t / tau).^((2.0-nn) / (1.0-nn)) - 1;
Phi = Phi0 + term1 .* term2;

hp = h_0 * (1. + cos(iota).^2) / 2. .* cos(Phi);
hc = h_0 .* cos(iota) .* sin(Phi);

