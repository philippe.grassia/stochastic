function [duration,fmin,fmax,hrss,energy,distance,epsilon] = genmsmagnetar(f0,tau,nn);


%% The right way to calculate detection probability is to fix the amplitude parameter hrss.
%% We choose a value of 1e-2. Epsilon is degerate with it, so we will start with a value of
%% of 0.01, but will caluculate the right value presently.
epsilon_tmp = 0.01;



fs = 4096;
duration = 10000;

% We assume a distance in mpc to the source
distance = 40;


T0 = 0;
tstart = 0;
tend = duration;

[hp, hc, t, freq] = msmagnetar(f0,tau,nn,epsilon_tmp,duration,fs,T0,tstart,tend);

fmin = nanmin(freq);
fmax = nanmax(freq);
dt = t(2) - t(1);

hrss1 = sqrt(sum(hp.^2+hc.^2)*dt);


%%% We want all the waveforms to have the same hrss default values, which we will set to 1e-24 for all waveforms. 
hrss = 1e-24;

hp = hp*(hrss/hrss1); 
hc = hc*(hrss/hrss1);
epsilon = epsilon_tmp*hrss/hrss1;

% calc_energy needs distance in pc
energy = wvf_energy2(hp, hc, dt, distance*10^6);


