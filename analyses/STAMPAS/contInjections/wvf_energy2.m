function [E] = wvf_energy2(hp,hx,deltaT,r)  
% wvf_energy : compute the energy of a waveforms
% DESCRIPTION :
%   energy is define as following
%   E = r^2 1/4 c^3/G <dh+^2 dhx^2>
%   
%   dh+ : dh+ / dt
%   dhx : dhx / dt
%
% SYNTAX :
%   [E] = wvf_energy2(hp,hx, deltaT, distance);
% 
% INPUT : 
%    hp & hx: strains
%    r : source distance in pc
%
% OUTPUT :
%    E : energy
%
% AUTHOR : Valentin FREY
% 
% Sharan Banagiri (sharan.banagiri@ligo.org)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 

  msun   = 1.988e30;   % kg
  c      = 299792458;  % m s^-1
  G      = 6.67e-11;   %
  msunc2 = msun*c^2;   % kg m^2 s^-2
  pc     = 3.08e16;    % m
  
  r = r*pc;
 

  dhp = (diff(hp)/deltaT);
  dhx = (diff(hx)/deltaT);

  E=r^2*c^3/G*1/4*sum((9/16*dhp.^2+1/2*dhx.^2)*deltaT)/msunc2;
 
end

