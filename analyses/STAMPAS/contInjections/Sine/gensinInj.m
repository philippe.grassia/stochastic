function [hrss, fmin, fmax, energy, distance, h0] = gensinInj(f0,fdot,duration,h0,phi0);

% Default hrss of 1e-20 for all sin waveforms
hrss = 1e-20;

% define fmin and fmax
fend = f0 + duration*fdot;
fmin = min(f0, fend);
fmax = max(f0, fend);

% for energy calculation we assume an fs of 4096
fs = 4096;
dt = 1/fs;

t = [0:dt:duration];

% create hp and hx
phi = phi0 + 2*pi*(f0*t + 0.5*fdot*(t).^2);
hp = h0*cos(phi);
hx = h0*sin(phi);

% Normalize to the hrss of 1e-20
hrss1 = sqrt(sum(hp.^2+hx.^2)*dt);
hp = hp*hrss/hrss1;
hx = hx*hrss/hrss1;

h0 = h0*hrss/hrss1;

% assume a distance of 10 kpc for no physical waveforms
distance = 10^4;
energy = wvf_energy2(hp,hx,dt,distance);

% distance is zero for non physical waveforms
distance = 0;

end
