function [fmin,fmax,hrss, energy, distance,alpha] = genrmodes(f0,duration);


fs = 4096;

tstart = 0;
tend = duration;

alpha = 1;


[hp, hc, t, freq] = rmodes(f0,duration,fs,tstart,alpha);

fmin = nanmin(freq);
fmax = nanmax(freq);
dt = t(2) - t(1);
hrss =sqrt(sum(hp.^2+hc.^2)*dt);

% We assume a distance in mpc to the source
distance = 1;

% calc_energy needs distance in pc
energy = wvf_energy2(hp, hc, dt, distance*10^6);
