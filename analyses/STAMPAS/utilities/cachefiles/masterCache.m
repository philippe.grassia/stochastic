function masterCache(ifo1,ifo2)
% function masterCache()
% This script combines all the S5 cachefiles for H1 and L1 into a single struct
% called cache and saved as cache.mat
% Originally by E. Thrane and modified by S. Kandhasamy
% Contact shivaraj@physics.umn.edu
% Modified by M. Coughlim
% Modified by M.A. Bizouard (07/31/2015)

gpsTimesPath1 = './cache/';
gpsTimesPath2 = './cache/';
frameCachePath1 = './cache/';
frameCachePath2 = './cache/';

%jobfile = load(['jobfile-' ifo '.txt']);

%NJobs = length(jobfile);    % total number of in S5 H1L1 job file
NJobs = 1;
site = [ifo1 ifo2]; % sites for ifos to cross-correlate

% initialize cache struct
cache.frames1 = '';
cache.frames2 = '';
cache.gps1 = [];
cache.gps2 = [];
cache.dur1 = [];
cache.dur2 = [];

NFail = 0;        % keep track of number of failed jobs
for ii=1:NJobs
  try
    % load cachefiles
%    fprintf('job no %d \n',ii);
    frames1 = textread([frameCachePath1 'frameFiles' site(1) '.' num2str(ii) ...
					'.txt'],'%s');
    frames2 = textread([frameCachePath2 'frameFiles' site(3) '.' num2str(ii) ...
					'.txt'],'%s');
    gps1 = load([gpsTimesPath1 'gpsTimes' site(1) '.' num2str(ii) '.txt']);
    gps2 = load([gpsTimesPath2 'gpsTimes' site(3) '.' num2str(ii) '.txt']);

    % fill cache struct
    cache.frames1 = [cache.frames1 ; frames1];
    cache.frames2 = [cache.frames2 ; frames2];
    cache.gps1 = [cache.gps1 ; gps1];
    cache.gps2 = [cache.gps2 ; gps2];
  catch
    fprintf('Error with job %i; Check the cache files\n',ii);
    NFail = NFail + 1;
  end
end

% save metadata
cache.site = site;
cache.NJobs = NJobs;
cache.path1 = frameCachePath1;
cache.path2 = frameCachePath2;

% clean-up: now remove duplicate gps times so that the frame and gps arrays
% contain only unique gps times
[nouse1, cut1, nouse2] = unique(cache.gps1);
cache.gps1 = cache.gps1(cut1);
cache.frames1 = {cache.frames1{cut1}};

[nouse1, cut2, nouse2] = unique(cache.gps2);
cache.gps2 = cache.gps2(cut2);
cache.frames2 = {cache.frames2{cut2}};

for i = 1:length(cache.frames1)
   frameSplit = regexp(strrep(cache.frames1{i},'.gwf',''),'-','split');
   cache.dur1 = [cache.dur1 ; str2num(frameSplit{end})];
end

for i = 1:length(cache.frames2)
   frameSplit = regexp(strrep(cache.frames2{i},'.gwf',''),'-','split');
   cache.dur2 = [cache.dur2 ; str2num(frameSplit{end})];
end

% save to mat file
save(['cache/cache_' ifo1 ifo2 '.mat'],'cache');

quit

