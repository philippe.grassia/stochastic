#!/usr/bin/python

import os, sys, glob, optparse, warnings, numpy, datetime, time, matplotlib, math
from subprocess import Popen, PIPE, STDOUT
import numpy as np

__author__ = "Michael Coughlin <michael.coughlin@ligo.org>"
__date__ = "2012/8/26"
__version__ = "0.1"

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser(usage=__doc__,version=__version__)

    parser.add_option("-i", "--ifo", help="ifo", default ="H1")

    parser.add_option("-s", "--gpsStart", help="Start GPS Time.", default ="800000000")
    parser.add_option("-e", "--gpsEnd", help="End GPS Time.", default="800002048")

    parser.add_option("-v", "--verbose", action="store_true", default=False,
                      help="Run verbosely. (Default: False)")

    opts, args = parser.parse_args()

    # show parameters
    if opts.verbose:
        print >> sys.stderr, ""
        print >> sys.stderr, "running network_eqmon..."
        print >> sys.stderr, "version: %s"%__version__
        print >> sys.stderr, ""
        print >> sys.stderr, "***************** PARAMETERS ********************"
        for o in opts.__dict__.items():
          print >> sys.stderr, o[0]+":"
          print >> sys.stderr, o[1]
        print >> sys.stderr, ""

    return opts

def add_to_frame(frame,frameLocation,gpsStart,gpsEnd):

    frame_files = os.listdir(frameLocation)

    for frame_file in frame_files:

        frame_file_without_period = frame_file.split(".")
        frame_file_split = frame_file_without_period[0].split("-")
        frameGPSStart = int(frame_file_split[2])
        frameGPSEnd = frameGPSStart + int(frame_file_split[3])

        if gpsEnd >= float(frameGPSStart) and gpsStart <= float(frameGPSEnd):
            frame.append(os.path.join(frameLocation,frame_file))

    return frame

def create_cache(frameLocation,gpsStart,gpsEnd):

    frame = []

    for dirname, dirnames, filenames in os.walk(frameLocation):
        for subdirname in dirnames:
            frame = add_to_frame(frame,os.path.join(dirname, subdirname),gpsStart,gpsEnd)

    return frame


# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    warnings.filterwarnings("ignore")

    # Parse command line
    opts = parse_commandline()

    jobNumber = 1

    cacheLocation = "cache"
    cacheFile = "cache/%s.cache"%opts.ifo
    frame = [line.strip() for line in open(cacheFile)]

    #injectionDir = "/home/mcoughlin/Stochastic/MDC/Recolored/%s_long/"%opts.ifo
    #frame = [os.path.join(root, name)
    #    for root, dirs, files in os.walk(injectionDir)
    #    for name in files]
    GPSStart = []
    GPSEnd = []

    f = open(os.path.join(cacheLocation,"frameFiles%s.%d.txt"%(opts.ifo[0],jobNumber)),"w")
    g = open(os.path.join(cacheLocation,"gpsTimes%s.%d.txt"%(opts.ifo[0],jobNumber)),"w")
    h = open('jobfile-%s.txt'%opts.ifo,'w')
    count = 1

    for frame_file in frame:

            frame_file = frame_file.replace("file://localhost","")
            frame_file_without_period = frame_file.split(".")
            if frame_file_without_period[-1] == "txt":
                continue 
            frame_file_split = frame_file_without_period[0].split("/")

            frame_file_split = frame_file_split[-1]
            frame_file_split = frame_file_split.split("-")

            if not frame_file_split[0] == opts.ifo[0]:
                continue

            frameGPSStart = int(frame_file_split[-2])
            frameDur = int(frame_file_split[-1])

            if frameDur < 0:
                continue

            f.write("%s\n"%(frame_file))
            g.write("%d\n"%(frameGPSStart))
            GPSStart.append(frameGPSStart)
            GPSEnd.append(frameGPSStart+frameDur)

            h.write('%d %d %d %d\n'%(count,frameGPSStart,frameGPSStart+frameDur,frameDur))
            count = count + 1
    f.close()
    g.close()
    h.close()

    #f = open('jobfile.txt','w')
    #f.write('%d %d %d %d\n'%(1,np.min(GPSStart),np.max(GPSEnd),np.max(GPSEnd)-np.min(GPSStart)))
    #f.close()

