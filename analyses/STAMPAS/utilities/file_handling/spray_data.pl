#! /usr/bin/env perl
#
# Code for copying pre-processed STAMP data (.mat files)
# from the location they were saved in during pre-processing
# to the node filesystems. Currently set up to work at CIT.
# Run check_nodes_data.pl after this to check everything
# and make sure the copy is OK.
#
# Author: Tanner Prestegard (prestegard@physics.umn.edu)
###############################################################

use strict;
use warnings;
use POSIX;

# Parameters to modify. ----------------
# Location of .mat files currently.
my $dataLoc = "/mnt/zfs2/prestegard/matfilesS6";
# Node prefix and directory - .mat files will be copied
# from $dataLoc to $nodePrefXXX/$nodeDir where XXX is the
# number of the node.
my $nodePref = "/data/node";
my $nodeDir = "prestegard/matfilesS6/";

# Don't edit below here!
#---------------------------------------

# Setup: list of nodes at CIT (as of March 2015).
# Nodes 321, 328, 329, and 491-496 aren't usable.
my @nodeNums = (1..320);
push(@nodeNums,322..327);
push(@nodeNums,330..490);
push(@nodeNums,497..499);

# Get list of subdirectories.
opendir(my $dir,$dataLoc) or die("Cannot open directory ".$dataLoc."!");
my @subDirs = readdir($dir);
closedir($dir);
my @subDirSizes;

# Remove first two elements (. and ..)
shift(@subDirs);
shift(@subDirs);

# Get sizes of subdirectories of .mat files.
print "Getting subdirectory sizes in ".$dataLoc.".\n";
my $totDataSize = 0;
foreach my $DIR (@subDirs) {
    my $tempDir = $dataLoc."/".$DIR;
    my $cmd = "du -bs ".$tempDir;
    my $size = qx($cmd); chomp($size);
    $size =~ s/^(\d+).*$/$1/;
    push(@subDirSizes,$size);
    $totDataSize = $totDataSize + $size;
}
$totDataSize = $totDataSize/(1024*1024*1024); # convert from bytes to GB.

# Function checks nodes for free space and removes those which don't have enough space.
# It also gets the amount of data to copy to each node.
# Node array is passed by reference.
my $dataPerNode = rm_full_nodes($nodePref,\@nodeNums,$totDataSize);

# Convert $dataPerNode from GB to bytes.
$dataPerNode = $dataPerNode*(1024*1024*1024);


# Set up useful variables and arrays for copying the data.
my $sumSize = 0;
my @copyDirs;
my $nodeIdx = 0;
my @nodeSizes;

# Loop over subdirectories of .mat files and add up the total amount of data.
# Data is copied to the nodes once we have "enough"; i.e. the sum total size
# of a list of subdirectories exceeds the size specified by the user.
print "Beginning to copy data to the node filesystems.\n";
for (my $i=0; $i<scalar(@subDirs); $i=$i+1) {
    $sumSize = $sumSize + $subDirSizes[$i];

    # Add dir name to copy array.
    # This contains the list of directories that will be copied
    # to a particular node.
    push(@copyDirs,$subDirs[$i]);

    # If the total size of the current list of subdirectories exceeds the specified
    # amount of data to copy per node OR if we are on the last subdirectory,
    # we do the copy.
    if (($sumSize >= $dataPerNode) || ($i == (scalar(@subDirs)-1))) {

	# Print directories to be copied and their destination and the total size.
	my $node = $nodePref.$nodeNums[$nodeIdx]."/".$nodeDir;
	print "Copying the following directories to ".$node.".\n";
	foreach my $CDIR(@copyDirs) { print "\t".$CDIR."\n"; }
	my $gSize = $sumSize/(1024*1024*1024);
	printf("Total size: %.2f GB.\n",$gSize);

	# Make directory on node if it doesn't exist.
	if (!(-d $node)) {
	    my $mcmd = "mkdir -p ".$node;
	    qx($mcmd);
	}

	# Copy the directories.
	foreach my $CDIR(@copyDirs) {
	    my $copycmd = "cp -r ".$dataLoc."/".$CDIR." ".$node;
	    print $copycmd."\n";
	    qx($copycmd);
	}
	print "\n";

	# Track size assigned to each node.
	push(@nodeSizes,$gSize);

	# Clear sumSize and copyDirs.
	$sumSize = 0;
	undef(@copyDirs);

	# Increment nodeIdx.
	$nodeIdx = $nodeIdx + 1;
    }

}

# Print node sizes for cross-checking.
# Any nodes not printed means no data was assigned to them.
my $totData = 0;
for (my $i=0; $i<scalar(@nodeSizes); $i=$i+1) {
    printf("node %i: %.2f GB.\n",$nodeNums[$i],$nodeSizes[$i]);
    $totData = $totData + $nodeSizes[$i];
}
print "Total data check: ".$totData." GB.\n";

# Check original data location.
my $cmdcheck = "du -bs ".$dataLoc;
my $origData = qx($cmdcheck); chomp($origData);
$origData =~ s/^(\d+)\s+.*$/$1/;
$origData = $origData/(1024*1024*1024);
print "Total data in original location: ".$origData." GB.\n";
my $diffData = abs($origData - $totData)*(1024);
print "Difference: ".$diffData." MB.\n";

# End of main

# SUBROUTINES -----------------------------

# Removes nodes which don't have enough free space from list.
sub rm_full_nodes {

    # Get args.
    my $nodePref = $_[0];
    my $nodeNums = @{$_[1]};
    my $totDataSize = $_[2]; # GB

    # Check free space on nodes.
    print "Checking available space on nodes.\n";
    my @nodeSpace;
    for (my $i=0; $i<scalar(@nodeNums); $i++) {

	# Get free space.
	my $cmd = "df -k ".$nodePref.$nodeNums[$i]." | tail -1";
	my $freeSpace = `$cmd`;
	$freeSpace =~ s/^-\s+\d+\s+\d+\s+(\d+)\s+\d+.*$/$1/;
	chomp($freeSpace);
	
	# Convert from kB to GB.
	$freeSpace = $freeSpace/(1024*1024);

	# Add to @nodeSpace array.
	push(@nodeSpace,$freeSpace);
    }

    # Figure out how much data to copy to each node.
    # Remove nodes which don't have enough space.
    # Keep track of number of nodes removed.
    my $avgData;
    my $nRem = 1;
    while ($nRem > 0) {
	# Figure out how much data to copy to each node.
	#$avgData = ceil($totDataSize/scalar(@nodeNums));
	$avgData = $totDataSize/scalar(@nodeNums);
	
	# Loop over nodes.
	$nRem = 0;
	for (my $i=(scalar(@nodeSpace)-1); $i>=0; $i--) {

	    # Remove node from list if it doesn't have enough free space.
	    if ($nodeSpace[$i] < $avgData) {
		print "\tSkipping node ".$nodeNums[$i].", only ".$nodeSpace[$i]." GB free.\n";
		splice(@nodeNums,$i,1);
		splice(@nodeSpace,$i,1);
		$nRem++;
	    }
	}
    }

    return($avgData);

}


# EOF
