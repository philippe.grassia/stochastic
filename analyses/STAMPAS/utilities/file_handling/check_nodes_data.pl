B#! /usr/bin/env perl
#
# Code for looping over node directories and comparing the size
# of the .mat files on the nodes to those in the original location
# (where they were copied from).  Should run this after running
# spray_data.pl. Currently set up to run at CIT.
#
# Author: Tanner Prestegard (prestegard@physics.umn.edu)
###################################################################

use strict;
use warnings;

# Parameters to modify. ----------------
# Location of .mat files.
my $dataLoc = "/mnt/zfs2/prestegard/matfilesS6";

# Node prefix and directory - .mat files will be copied
# from $dataLoc to $nodePrefXXX/$nodeDir where XXX is the
# number of the node.
my $nodePref = "/data/node";
my $nodeDir = "prestegard/matfilesS6/";

# Don't edit below here!
#---------------------------------------

# Setup: list of nodes at CIT (as of March 2015).
# Nodes 321, 328, 329, and 491-496 aren't usable.
my @nodeNums = (1..320);
push(@nodeNums,322..327);
push(@nodeNums,330..490);
push(@nodeNums,497..499);

# GET INFO ON DATA IN ORIGINAL LOCATION ---------
# Get list of subdirectories.
print "Getting list of subdirectories in ".$dataLoc."\n";
opendir(my $dir,$dataLoc) or die("Cannot open directory ".$dataLoc."!");
my @subDirs = grep { !/^\.\.?$/ } readdir($dir);
closedir($dir);

# -----------------------------------------------

# Loop over nodes.
print "Looping over nodes.\n";
for (my $i=0; $i<scalar(@nodeNums); $i++) {

    # Full path to node directory containing .mat files.
    my $fullNodeDir = $nodePref.$nodeNums[$i]."/".$nodeDir;
    if (-d $fullNodeDir) {
	# Get list of subdirectories on this node.
	opendir(my $ndir,$fullNodeDir);
	my @nodeSubDirs = grep { !/^\.\.?$/ } readdir($ndir);
	closedir($ndir);

	# Loop over subdirectories.
	foreach my $SDIR (@nodeSubDirs) {

	    # Get size of this matfile directory in original location.
	    my $origDir = $dataLoc."/".$SDIR;
	    my $origSize = get_size($origDir);
	    
	    # Get size of this directory on the node.
	    my $nodefolder = $fullNodeDir."/".$SDIR;
	    my $nodeSize = get_size($nodefolder);

	    # Get size difference and fractional difference.
	    my $diff = $origSize - $nodeSize;
	    my $fdiff = $diff/$origSize;

	    # Print if fractional difference is significant.
	    if ($fdiff > 1e-5) {
		print $origDir."\t".$nodefolder."\t".($diff/(1024*1024))." MB\t".$fdiff."\n";
	    }

	    # Remove element from @subDirs array.
	    my $sdir_idx = 0;
	    ++$sdir_idx until $subDirs[$sdir_idx] =~ /$SDIR/ or $sdir_idx > $#subDirs;
	    splice(@subDirs, $sdir_idx, 1);

	}

    } else {
	print "Directory ".$fullNodeDir." does not exist.\n";
    }
}

# If there are any subdirectories that we missed, print them.
print "There are ".scalar(@subDirs)." directories still left in ".$dataLoc." that we didn't look at.\n";
foreach my $RDIR (@subDirs) {
    print "\t".$RDIR."\n";
}

# SUBROUTINES

# Gets directory size (in bytes)
sub get_size {

    # Get args.
    my $node_dir = $_[0];

    # Get free space.
    my $cmd = "du -bs ".$node_dir;
    my $cmdOut = `$cmd`;
    my $space_used = $cmdOut;
    $space_used =~ s/^(\d+)\s+.*$/$1/;
    chomp($space_used);

    return($space_used);
}

# EOF
