function generate_ftimes(searchPath, freqs, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generates times for specific frequency triggers  at each ifo in a stamp background study
% generate_freq_triggertimes('/home/sharan.banagiri/2Ibkg/BKG/Start-1126073529_Stop-1127245757_ID-1032', 7500, [196, 197, 198])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% load joblist for gps times
[~, e] = regexp(searchPath, 'ID-');
dagN = searchPath(e+1:end);
matname = ['triggers_', dagN, '.mat'];
system(['mkdir -p ', searchPath, '/trigger_plots']);
load([searchPath, '/', matname]);

% joblist = load([searchPath, '/tmp/jobfile_bknd.txt']);
% laglist = joblist(:, 1);
% joblist = joblist(:, 2);
% dj = joblist(2)  - joblist(1);

% length of thedata interval
T = length(joblist);

if nargin == 3
   snrCutoff = strassign(varargin{1});
else
    snrCutoff = 0;
end

nfreqs = length(freqs);

%initialize matrixes for frequency specific triggers
fmax_tmp = zeros(T, T);
fmin_tmp = zeros(T, T);

imgname = '_';

% collect triggers which are of the desired frequencies 
for ii = 1:nfreqs
  fmax_tmp = fmax_tmp | (triggers_fmax == freqs(ii));
  fmin_tmp = fmin_tmp | (triggers_fmin == freqs(ii));
  imgname = [imgname, num2str(freqs(ii)), '_'];
end

% collect triggers which belong to the tail of the distribution  if desired.
fmax_tmp = fmax_tmp & (snrlist > snrCutoff);
fmin_tmp = fmin_tmp & (snrlist > snrCutoff);

% sum over hanford times to make arrays for livingtston
fmax_L1 = sum(fmax_tmp, 1);
fmin_L1 = sum(fmin_tmp, 1);

% sum over livingston times to make arrays for hanford
fmax_H1 = sum(fmax_tmp, 2);
fmin_H1 = sum(fmin_tmp, 2);

%gpstimes = joblist(1):dj:joblist(end);

% plot fmin and fmax wiht GPS time
close all
plot(joblist, fmax_H1);
hold on
plot(joblist, -fmax_L1); 
title(['GPS times for bknd triggers with fmax at   ', num2str(freqs), ' Hz with snr > ',num2str(snrCutoff)]);
xlabel('GPS times')
ylabel('# of triggers')
legend('Livingston', 'Hanford')
saveas(gcf, [searchPath, '/trigger_plots/fmax', imgname, 'H1L1.png'])
close all


plot(joblist, fmin_H1);
hold on
plot(joblist, -fmin_L1);
title(['GPS times for bknd triggers with fmin at ', num2str(freqs), ' Hz with snr > ',num2str(snrCutoff)]);
xlabel('GPS times')
ylabel('# of triggers')
legend('Livingston','Hanford')
saveas(gcf, [searchPath, '/trigger_plots/fmin', imgname, 'H1L1.png'])
close all

keyboard
