function trigger_ftmap(searchPath,Toffset, flow, fhigh )

%%%%%%%%%%%%%
%Makes an ftmap of the trigger with a dt of window size. The frequency is the minimum frequency but can be easily changed in the code. Saves both ftmaps with time variation in L1 and time varation in H1

%Usage
  % trigger_ftmap('/home/sharan.banagiri/2Ibkg/BKG/Start-1126073529_Stop-1127245757_ID-1032', 5000, 20, 500)
%%%%%%%%%%%%%%

[~, e] = regexp(searchPath, 'ID-');
dagN = searchPath(e+1:end);
filename = ['triggers_', dagN, '.mat'];

% load the joblist
joblist = load([searchPath, '/tmp/jobfile_bknd.txt']);
laglist = joblist(:, 1);
joblist = joblist(:, 2);

% length of data arrays
T = length(joblist);

dj =joblist(2)  - joblist(1);
% length of thedata interval


% make an array for trigger frequencies
triggers_fmin  = NaN(T, T);
triggers_fmax  = NaN(T, T);

if exist([searchPath, '/', filename]) == 2
  load([searchPath, '/', filename]);

else
  for  ii = laglist(1):1:laglist(end)       
    results_dir = strcat([searchPath, '/results/results_'], num2str(ii+Toffset));

    for jj = laglist(1):1:laglist(end)
      % load the results mat file and store the fmin data of the trigger
      matname = strcat([results_dir,'/results_'], num2str(joblist(jj)), '.mat');
      
      if exist(matname) == 2
        result =  load(matname); 
	snrlist( ii, jj) = result.data.SNR;
        triggers_fmin( ii, jj ) = result.data.fmin; 
	triggers_fmax( ii, jj ) = result.data.fmax;
      else
	snrlist( ii, jj ) = -1;
	triggers_fmin( ii, jj ) = -1;
        triggers_fmax( ii, jj ) = -1;
      end

    end
    fprintf('Finished reading data for lagnumber = %i\n', ii+Toffset)
    % save([searchPath, '/', filename], 'triggers_fmin', 'triggers_fmax', 'snrlist', 'joblist', 'laglist');
  end
  save([searchPath, '/', filename], 'triggers_fmin', 'triggers_fmax', 'snrlist', 'joblist', 'laglist');
end

% Total time of the job file including all gaps

gpsTimes = joblist(1):dj:joblist(end);
TT = length(gpsTimes);


fsize = length(flow:5:(fhigh-5));
minfreqs_H1 = zeros(fsize, TT);
minfreqs_L1 = zeros(fsize, TT);
maxfreqs_H1 = zeros(fsize, TT);
maxfreqs_L1 = zeros(fsize, TT);


edges = (flow -2.5):5:(fhigh-2.5);
edgesX = flow:5:(fhigh -5);

% collecting trigger data with L1 time variation
for  ii = 1:T

          jj = 1 + round((joblist(ii) - joblist(1))/dj);
	  h1 = histogram(triggers_fmin(ii, :), edges);
          v1 = h1.Values;
          minfreqs_L1(:, jj) = v1;
          close all;

	  h1 = histogram(triggers_fmax(ii, :), edges);
          v1 = h1.Values;
          maxfreqs_L1(:, jj) = v1;
          close all;

end

% collection trigger data with L1 time variation
for ii = 1:T

           jj = 1 + round((joblist(ii) - joblist(1))/dj);
	   h1 = histogram(triggers_fmin(:,ii), edges);
           v1 = h1.Values;
	   minfreqs_H1(:, jj) = v1;
           close all;

	   h1 = histogram(triggers_fmax(:,ii), edges);
           v1 = h1.Values;
           maxfreqs_H1(:, jj) = v1;
           close all;
end


nums = [1, 20:20:160];
vs = log10(nums);

figure
surf(gpsTimes,flow:5:(fhigh-5),log10(minfreqs_H1), 'EdgeColor', 'None');
xlabel('H1 GPS Times')
ylabel('Trigger fmin freqs')
view(2);    
colormap jet
c1 = colorbar('Ticks', vs, 'TickLabels', {'1', '20', '40', '60', '80', '100', '120', '140', '160'});
c1.Label.String = '# of triggers'
caxis([0, 2.2])
grid off
saveas(gcf, [searchPath, '/trigger_plots/H1fmin_',dagN,'.png']);
close all

figure
surf(gpsTimes,flow:5:(fhigh-5),log10(minfreqs_L1),'EdgeColor', 'None');
xlabel('L1 GPS Times');
ylabel('Trigger fmin freqs');
view(2);
colormap jet
c = colorbar('Ticks', vs, 'TickLabels', {'1', '20', '40', '60', '80', '100', '120', '140', '160'});
c.Label.String  = '# of triggers'
caxis([0,2.2])
grid off
saveas(gcf, [searchPath, '/trigger_plots/L1fmin_',dagN,'.png']);
close all


figure
surf(gpsTimes,flow:5:(fhigh-5),log10(maxfreqs_H1), 'EdgeColor', 'None');
xlabel('H1 GPS Times')
ylabel('Trigger fmax freqs')
view(2);
colormap jet
c1 = colorbar('Ticks', vs, 'TickLabels', {'1', '20', '40', '60', '80', '100', '120', '140', '160'});
c1.Label.String = '# of triggers'
caxis([0, 2.2])
grid off
saveas(gcf, [searchPath, '/trigger_plots/H1fmax_',dagN,'.png']);
close all

figure
surf(gpsTimes,flow:5:(fhigh-5),log10(maxfreqs_L1),'EdgeColor', 'None');
xlabel('L1 GPS Times');
ylabel('Trigger fmax freqs');
view(2);
colormap jet
c = colorbar('Ticks', vs, 'TickLabels', {'1', '20', '40', '60', '80', '100', '120', '140', '160'});
c.Label.String  = '# of triggers'
caxis([0,2.2])
grid off
saveas(gcf, [searchPath, '/trigger_plots/L1fmax_',dagN,'.png']);
close all
keyboard
 


