function snrPlot(searchPath, mc)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plots the trigger background
  %eg snrPlot('/home/sharan.banagiri/2Ibkg/STAMPAS/BKG/Start-1126073529_Stop-1128830762_ID-15119', 0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if nargin ~= 2
  error('Need two inputs - path to dag and is it with montecarlo noise?')
end

% make a directory for plots
trigplots = [searchPath, '/trigger_plots'];
system(['mkdir -p ', trigplots]);


firstpass = 1;

for i = 7501:1:7971

	  dir_name = strcat(searchPath,'/results/results_', num2str(i));
          file_list = dir(dir_name);

           if firstpass
            realsnrlist = NaN(600,length(file_list));
            fminlist = NaN(600,length(file_list));
            fmaxlist = NaN(600,length(file_list));
            firstpass = 0;
           end

            for j = 1:1:length(file_list)
		if ~file_list(j).isdir
                matname = strcat( dir_name,'/', file_list(j).name);
                D = load(matname);
                realsnrlist(i-7500, j) = D.data.SNR;
                fminlist(i-7500, j)  = D.data.fmin;
                fmaxlist(i-7500, j) = D.data.fmax;
                clear D
           end
      end

		fprintf('Finished reading data with lagnumber = %i\n', i)

   end

[~, e] = regexp(searchPath, 'ID-');
dagN = searchPath(e+1:end);   

if mc
   filename = [trigplots, '/snr_BKG_MC_', dagN];
   freqfilename = [trigplots, '/f_BKG_MC_', dagN];
else
   filename = [trigplots,'/snr_BKG_', dagN];
   freqfilename = [trigplots, '/f_BKG_',dagN];  
end

freqfilename  = [freqfilename, '.png'];
filename = [filename, '.png'];


% Making background histograms
a = max(max(realsnrlist));
edges = -0.05:0.1:(a+a/10);
h_real = histogram(realsnrlist, edges, 'Normalization', 'probability');
v_real = h_real.Values ;
ZeroIndex = v_real == 0;
v_real(ZeroIndex) = 10^-6;
close all
edgesX = edges(1:end-1);
figure
stairs(edgesX + 0.05,log10(v_real));  % cut SNr
legend('real data')
title('Background of triggers')
xlabel('SNR')
ylabel('log10(#)')
fprintf('Saving background histogram in %s \n', filename)
saveas(gcf, filename);
close all


%Making frequency plots
fs = 19.5:1:500.5;
fsx = fs(1:end -1) +0.5;
favglist = 0.5*(fmaxlist+fminlist);
hmin = histogram(fminlist, fs);
vmin = hmin.Values;
close all
hmax = histogram(fmaxlist, fs);
vmax = hmax.Values;
close all
havg = histogram(favglist, fs);
vavg = havg.Values;
close all


figure
stairs(fsx, log10(vmin), 'r');
hold on
stairs(fsx, log10(vavg), 'b');
hold on
stairs(fsx, log10(vmax), 'g');
legend('fmin', 'fmax', 'favg')
title('Frequency dist of triggers')
xlabel('frequency')
ylabel('log10(#)')
fprintf('Saving freq histogram in %s \n', freqfilename)
saveas(gcf, freqfilename);



keyboard
close all;
end
