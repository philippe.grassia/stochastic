#!/bin/bash  
# shell script to initialize interactive run 
# ------------------------------------------------------------------
# Syntax:
#   -init.sh 
# ------------------------------------------------------------------
# Purpose: 
#    this script export all RUN parameters needed with respect to 
#    your env update also LD_LIBRARY_PATH
# ------------------------------------------------------------------
# author:                         FREY valentin
# EMail Address:                  valentin.frey@gmail.com
# version                         1.1
# Comment:                        
# ------------------------------------------------------------------

script=init.sh

### check if an history file exist & get the saved variable
historyFile=${interactive}/.interactive_history
if [[ -f ${historyFile} ]];then
    export htype=`grep TYPE ${historyFile} | \
	awk '{print $2}'`
    export hstart=`grep START ${historyFile} | \
	awk '{print $2}'`
    export hstop=`grep STOP ${historyFile} | \
	awk '{print $2}'`
    export hid=`grep ID ${historyFile} | \
	awk '{print $2}'`
    if [[ ${htype} == 'INJ' ]];then
	export hwvf=`grep WVFID ${historyFile} | \
	    awk '{print $2}'`
	export hlagNumber=''
    elif [[ ${htype} == 'BKG' ]];then
	export hlagNumber=`grep LAGNUMBER ${historyFile} | \
	    awk '{print $2}'`
	export hwvfId=''
    else
	echo ${historyFile} " corrupted, delete it and restart "
    fi
    export hgpsWindows=`grep GPSWINDOWS ${historyFile} | \
	awk '{print $2}'`
    export hwebRep=`grep WEBREP ${historyFile} | \
	awk '{print $2}'`
fi

### set STAMPAS & stamp2 direcetory
RUN=`pwd | sed 's|/STAMPAS.*||'`

if [[ $RUN == `pwd` ]];then
    echo -e "\e[1;31m $script : you need to be in a STAMPAS directory\e[0m"
    echo -e "\t\e[1;31m eg. ~/s5-allsky/STAMPAS/\e[0m"
    exit 1
fi

export STAMPAS=$RUN/STAMPAS
export STAMP2=$RUN/stamp2


### host distinction for web repository
### and matlab initialisation                       
HOST=`hostname -d | cut -d "." -f 1`
if [[ $HOST == "atlas" ]]; then
    source $STAMPAS/matlab_script_2013a.sh || exit $?
    export web_repo='WWW/LSC' 
    export web_repo_suffix='/LSC'
    export webHost='https://atlas1.atlas.aei.uni-hannover.de'
elif [[ $HOST == "ligo" ]];then
    source $STAMPAS/matlab_script_2015a.sh || exit $?
    export web_repo='public_html'
    export web_repo_suffix=''
    export webHost='https://ldas-jobs.ligo.caltech.edu'
else
    echo -e "\e[1;31m $script : Only designed to work on LIGO and ATLAS clusters. Exiting...\e[0m"
    exit
fi


### display header
echo -e "****************************************************"
echo -e "****************************************************"
echo -e "**   ______   __                                  **"    
echo -e "**  |   ___) |  |__                               **"
echo -e "**  |  |__  (    __)  __  __  __    ___ _   ____  **" 
echo -e "** (    __)  |  |    |  \|  \|  \  / _ ' | | _  \ **"
echo -e "**  |  |     |  |_   |          | | (_)  | |  __/ **"
echo -e "**  |__|     |____)  |__|\__|\__|  \___'_| |__|   **"
echo -e "**                                                **"
echo -e "****************************************************"
echo -e "****************************************************\n"



##################################
###  RUN PARAMETERS FIND STEP  ###
##################################


### try some run parameters acoording to the working path
type=`pwd | sed 's|/Start.*||' | sed -e 's|.*STAMPAS/||'`
start=`pwd | sed -e 's/.*Start-//'| sed 's/_Stop.*//'`
stop=`pwd | sed -e 's/.*Stop-//g'| sed 's/_ID.*//'`
id=`pwd | sed -e 's/.*ID-//g'`


### check if we are on a Start-*_Stop-*_ID-* folder
### else ask type, start, stop, id parameters
if [[ $type ==  `pwd` ]] ||  [[ $start ==  `pwd` ]] ||\
   [[ $stop ==  `pwd` ]] ||  [[ $id ==  `pwd` ]]; then
    
    typeStep=0
    startStep=0
    stopStep=0
    idStep=0
    folderStep=0

    # type step
    echo -e "\n\e[0;33mTYPE : \e[0m"
    while [[ $typeStep == 0 ]]; do
	echo -e " \e[0;33m|\e[0m Choose a type INJ/BKG"
	read -p "$(echo -e " \e[0;33m| [${htype}]>\e[0m")" type
	if [[ ${type} == '' ]];then
	    type=${htype}
	fi

	if [[ "${type}" != "INJ" ]] && [[ "${type}" != "BKG" ]];then
	    echo -e " \e[0;33m|\e[1;31m Bad type, please write INJ or BKG\e[0m"
	else
	    typeStep=1
	fi
    done

    # start step
    echo -e "\n\e[0;33mGPS START TIME : \n"
    while [[ $startStep == 0 ]]; do
	echo -e " \e[0;33m|\e[0m Choose a start GPStime"
	read -p "$(echo -e " \e[0;33m| [${hstart}]>\e[0m")" start
	if [[ ${start} == '' ]];then
	    start=${hstart}
	fi

	if ! [ $start -eq  $start 2> /dev/null ] || [[ $start == '' ]];then
	    echo -e " \e[0;33m|\e[1;31m Bad start GSP time, it must be a number.\e[0m"
	else
	    startStep=1
	fi
    done

    # stop step
    echo -e "\n\e[0;33mGPS STOP TIME : \e[0m"
    while [[ $stopStep == 0 ]]; do
	echo -e " \e[0;33m|\e[0m Choose a stop GPStime"
	read -p "$(echo -e " \e[0;33m| [${hstop}]>\e[0m")" stop
	if [[ ${stop} == '' ]];then
	    stop=${hstop}
	fi

	if ! [ $stop -eq $stop 2> /dev/null ] || [[ $stop == '' ]];then
	    echo -e " \e[0;33m|\e[1;31m Bad stop GSP time, it must be a number.\e[0m"
	else
	    stopStep=1
	fi
    done

    # id step
    echo -e "\n\e[0;33mID : \e[0m"
    while [[ $idStep == 0 ]]; do
	echo -e " \e[0;33m|\e[0m Choose a run id"
	read -p "$(echo -e " \e[0;33m| [${hid}]>\e[0m")" id
	if [[ ${id} == '' ]];then
	    id=${hid}
	fi

	if ! [ $id -eq $id 2> /dev/null ] || [[ $id == '' ]];then
	    echo -e " \e[0;33m|\e[1;31m Bad id run, it must be a number.\e[om"
	else
	    idStep=1
	fi
    done
    
    ### check if the run already exist
    if [[ -d ${STAMPAS}/${type}/Start-${start}_Stop-${stop}_ID-${id} ]]; then
	echo -e "\n\e[1;34mSUMMARY : \e[0m"
	echo -e " \e[1;34m|\e[0m Find one directory with the following parameters :"
	echo -e " \e[1;34m| TYPE  : \e[0m" $type
	echo -e " \e[1;34m| START : \e[0m" $start
	echo -e " \e[1;34m| STOP  : \e[0m" $stop
	echo -e " \e[1;34m| ID    : \e[0m" $id
	echo -e " \e[1;34m| folder name :\e[0m"
	echo -e " \e[1;34m|\e[0m "${STAMPAS}/${type}/Start-${start}_Stop-${stop}_ID-${id}
	echo -e " \e[1;34m|\e[0m "
	while [[ ${folderStep} == 0 ]]; do
	    echo -e " \e[1;34m|\e[0m Do you want to use this folder y/n"
	    read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" useFolder
	    if [[ ${useFolder} == '' ]];then
		useFolder=y
	    fi 

	    if [[ ${useFolder} != 'n' ]] && [[ ${useFolder} != 'y' ]]; then
		echo -e " \e[1;34m|\e[1;31m Please write y or n\e[0m"
	    elif [[ "${useFolder}" == "n" ]]; then
		echo -e " \e[1;34m|\e[1;31m You write n, then abording... Please check  why information are wrong\e[0m"
		exit 0
	    else
		folderStep=1
		newFolder=false
	    fi
	done

    ### no folder found with this parameters 
    ### ask if create this folder
    else
	echo -e "\e[1;34\nSUMMARY : \e[0m"
	echo " \e[1;34m|\e[0m No folder found with the following parameters : "
        echo -e " \e[1;34m| TYPE  : \e[0m" $type
        echo -e " \e[1;34m| START : \e[0m" $start
        echo -e " \e[1;34m| STOP  : \e[0m" $stop
        echo -e " \e[1;34m| ID    : \e[0m" $id
	echo -e " \e[1;34m|\e[0m"
	while [[ ${folderStep} == 0 ]]; do
	    echo -e " \e[1;34m|\e[0m Do you wan't to create this folder y/n"
	    read -p "$(echo -e  "\e[1;34m| [y]>\e[0m")" useFolder
	    if [[ ${useFolder} == '' ]];then
		useFolder=y
	    fi

	    if [[ ${useFolder} != 'n' ]]  && [[ ${useFolder} != 'y' ]]; then
		echo -e " \e[1;34m|\e[1;31m Please write y or n\e[0m"
	    elif [[ ${useFolder} == 'n' ]]; then
		echo -e " \e[1;34m|\e[1;31m You write n then abording...\e[0m"
		exit 1
	    else
		folderStep=1
		mkdir ${STAMPAS}/${type}/Start-${start}_Stop-${stop}_ID-${id} || exit $?
		newFolder=true
	    fi
	done
    fi
else
    echo -e "\e[1;34mRUN PARAMETERS :\e[0m"
    echo -e " \e[1;34m|\e[0m You are in the following folder : "
    echo -e " \e[1;34m|\e[0m" ${STAMPAS}/${type}/Start-${start}_Stop-${stop}_ID-${id}
    echo -e " \e[1;34m|\e[0m with the following parameters :"
    echo -e " \e[1;34m| TYPE  :\e[0m " $type
    echo -e " \e[1;34m| START :\e[0m " $start
    echo -e " \e[1;34m| STOP  :\e[0m " $stop
    echo -e " \e[1;34m| ID    :\e[0m " $id
    echo -e " \e[1;34m|\e[0m"

    confirmStep=0
    echo -e " \e[1;34m|\e[0m Do you confirm y/n"
    while [[ ${confirmStep} == 0 ]];do
	read -p "$(echo -e " \e[1;34m| [y]>")" confirm
	
	if [[ ${confirm} == '' ]];then
	    confirm=y
	fi

	if [[ ${confirm} != "n" ]] && [[ ${confirm} != "y" ]]; then
	    echo -e " \e[1;34m|\e[1;31m Please write y or n\e[0m"
        elif [[ ${confirm} == 'n' ]];then
	    echo -e "\e[1;31m You enter no then abording... Please check why run parameters are not good\e[0m"
            exit 0
        else
            confirmStep=1
	    newFolder=false
        fi
    done
fi 

### Get the search 
if [ ${start} -lt 900000000 ]; then
    search='s5-allsky'
elif [ ${start} -lt 1000000000 ]; then
    search='s6-allsky'
elif [ ${start} -lt 1124004000 ]; then
    search='er7-allsky'
elif [ ${start} -lt 1126073340 ]; then
    search='er8-allsky'
else
    search='o1-allsky'
fi


### export the run parameters
export type
export start
export stop
export id
export search

### write the history file
echo "TYPE " ${type} > "${historyFile}"
echo "START " ${start} >> "${historyFile}"
echo "STOP " ${stop} >> "${historyFile}"
echo "ID " ${id} >> "${historyFile}"

### export other parameters use in ftmap script
export RUNFolder=${STAMPAS}/${type}/Start-${start}_Stop-${stop}_ID-${id}
export Mcheck=$RANDOM # number to check if matlab run work fine
export newFolder
export historyFile
