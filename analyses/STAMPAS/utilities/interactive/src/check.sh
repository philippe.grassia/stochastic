#!/bin/bash  
# script to configure and check parameters for the interactive run
# ------------------------------------------------------------------
# Syntax:
#   -$0 
# ------------------------------------------------------------------
# Purpose: 
#    check if all the file request foran interactive run are 
#    present
# ------------------------------------------------------------------
# author:                         FREY valentin
# EMail Address:                  valentin.frey@gmail.com
# version                         1.0
# Comment:                        
# ------------------------------------------------------------------

script=check.sh
echo $0 

### check file from src
if ! [[ -f ${interactive}/src/init.sh ]]; then
    echo "$script : file src/init.sh missing, abord ..."
    exit 1
fi

if ! [[ -f ${interactive}/src/setup.sh ]]; then
    echo "$script : file src/setup.sh missing, abord ..."
    exit 1
fi

### check plot2html folder
if ! [[ -d ${interactive}/plot2html ]]; then
    echo "$script : folder plot2html missing, abord ..."
    exit 1
fi

if ! [[ -f ${interactive}/plot2html/plot2html.m ]]; then
    echo "$script : file plot2html/plot2html.m missing, abord ..."
    exit 1
fi

if ! [[ -f ${interactive}/plot2html/main.js ]]; then
    echo "$script : file plot2html/main.js missing, abord ..."
    exit 1
fi

if ! [[ -f ${interactive}/plot2html/style.css ]]; then
    echo "$script : file plot2html/style.css missing, abord ..."
    exit 1
fi

