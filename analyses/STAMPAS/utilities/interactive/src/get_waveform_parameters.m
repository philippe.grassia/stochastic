function parameters = get_waveform_parameters(searchPath)
% Gets the main parameters of the waveforms used in the current INJ
% dag. It assumes:
% - the list of waveforms is stored in the waveforms.txt file found
% in the search folder. 
% - the waveforms are stored in the STAMPAS local copy folder
% (STAMPAS/waveforms). 
%
% Inputs:
% - searchPath: path to the search folder
%
% Outputs:
% - parameters structure containing all parameters: alphas,
% waveform names, freq min, freq max, distance and duration
%
% To be done:
% - Currently not working properly for the eBBH waveforms
% - Save parameters in a file & add a check at the beginning if the
% parameters file exist. If yes, load the parameters from the file

parameters=[];

% Check whether searchPath exists
if exist(searchPath,'dir')==0
  error([searchPath ' does not exist']);
  return
end

% Get the list of waveforms
B=readtext([searchPath '/waveforms.txt'], ' ', '%', '', 'textual');

% The waveform name is stored in the first column
nb_wvfs=size(B,1);

% If no waveforms are found, exit
if nb_wvfs==0
  error(['No waveform found in ' searchPath 'waveforms.txt']);
  return
end

waveform_names=[];
for i=1:nb_wvfs
  waveform_names=[waveform_names; B(i,1)];
end

%% get rid of the "/" for ebbh waveforms
for i=1:nb_wvfs
  if ~isempty(regexp(waveform_names{i},'.*/$'))
    waveform_names{i}=waveform_names{i}(1:end-1);  
  end
end

% Get the alpha values
for i=1:nb_wvfs
  % non-empty elements of row i of B
  B_i = B(i,~cellfun(@isempty,B(i,:)));
  alphas_i = B_i(2:end);
  alphas{i} = cellfun(@str2num, alphas_i);
  %alphas{i} = arrayfun(@(x) num2str(x,'%.6f'),double_alphas{i}, ...
  %		       'UniformOutput',false);
end
% Get other parameters 
fmin=zeros(nb_wvfs,1);
fmax=zeros(nb_wvfs,1);
distance=zeros(nb_wvfs,1);
duration=zeros(nb_wvfs,1);

for i=1:nb_wvfs
  filename=strcat('../../waveforms/', waveform_names(i));
  [void,s_fmin]=system(['head -2 ' char(filename) ' | awk ''{print $2}'' ']);
  [void,s_fmax]=system(['head -2 ' char(filename) ' | awk ''{print $3}'' ']);
  [void,s_distance]=system(['head -2 ' char(filename) ' | awk ''{print $4}'' ']);
  [void,s_hrss]=system(['head -2 ' char(filename) ' | awk ''{print $5}'' ']);

  if strfind(s_fmin,'fmin')
    if size(s_fmin)<5
      error(['Cannot find the fmin information this waveform: ' ...
	     s_fmin]);
    end
    fmin(i)=str2num(s_fmin(5:end));
  end
  if strfind(s_fmax,'fmax')
    if size(s_fmax)<5
      error(['Cannot find the fmax information this waveform: ' ...
	     s_fmax]);
    end
    fmax(i)=str2num(s_fmax(5:end));
  end
  if strfind(s_distance,'dist');
    if size(s_distance)<10
      error(['Cannot find the distance information this waveform: ' ...
	     s_distance]);
    end
    distance(i)=str2num(s_distance(10:end));
  end
  if strfind(s_hrss,'hrss')
    if size(s_hrss)<5
      error(['Cannot find the hrss information this waveform: ' ...
	     s_hrss]);
    end
    hrss(i)=str2num(s_hrss(5:end));
  end
  
  [void,s_duration]=system(['tail -1 ' char(filename) ' | cut -d " " -f 1']);
  tmp=str2num(s_duration);
  duration(i)=tmp(1);
end

% TP 4/15/2015- nb_alphas is no longer used since
% this can be different for different waveforms.
parameters.nb_wvfs=nb_wvfs;
parameters.alphas=alphas;
parameters.waveform_names=waveform_names;
parameters.fmin=fmin;
parameters.fmax=fmax;
parameters.distance=distance;
parameters.duration=duration;
parameters.hrss=hrss;
parameters.searchPath=searchPath;
%parameters.double_alphas=double_alphas;

return;