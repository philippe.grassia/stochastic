#!/bin/bash  
# script to check all matfile, params file and config file 
# ------------------------------------------------------------------
# Syntax:
#   -$0 [args...]
# ------------------------------------------------------------------
# Purpose: 
#    check if matfile, params file and config file request to run 
# run_clustermap are present in the Start-*_Stop-*_ID-* folder
# In the case of a new Start-*_Stop-*_ID-* folder, just copy file 
# from STAMPAS directory 
#    
# ------------------------------------------------------------------
# author:                         FREY valentin
# EMail Address:                  valentin.frey@gmail.com
# version                         1.2
# Comment:                        
# ------------------------------------------------------------------

script=setup.sh

########################
### run setup_config ###
########################

### first check config file
configFile=config_$(echo $type  | tr '[:upper:]' '[:lower:]').txt
if [[ ${newFolder} == false ]] && \
    ( ! [[ -f ${RUNFolder}/${configFile} ]] ); then
    echo "$script : ${configFile} not found, abording ..." 
    exit 1
elif [[ ${newFolder} == true ]]; then
    cp ${STAMPAS}/${configFile} ${RUNFolder}/
else
    : # do nothing
fi


### check/copy waveform.txt file if type = INJ
if [[ ${type} == INJ ]]; then
    if [[ ${newFolder} == false ]] && \
       ( ! [[ -f ${RUNFolder}/waveforms.txt ]] ); then
	echo "$script : waveforms.txt not found, abording ..." 
	exit 1
    elif [[ ${newFolder} == true ]]; then
	cp ${STAMPAS}/waveforms.txt ${RUNFolder}
    else
	: # do nothing
    fi
fi


### In the case on new folder, run setup config 
### to generate all file needed
if [[ ${newFolder} == true ]]; then
    echo "GPS_start ${start}" >> ${RUNFolder}/${configFile}
    echo "GPS_end ${stop}" >> ${RUNFolder}/${configFile}
    echo "ID ${id}" >> ${RUNFolder}/${configFile}
    echo "Hostname $( hostname -d )" >> ${RUNFolder}/${configFile}
    echo "firstAnteproc 1" >> ${RUNFolder}/${configFile}

    cd ${STAMPAS} || exit $?
    ${MATLAB_ROOT}/bin/matlab -nodisplay <<EOF || exit $?
       run('${STAMPAS}/startup');
       setup_config('${RUNFolder}/${configFile}');
EOF

    ### run another time for bkg
    if [[ ${type} == 'BKG' ]];then	
	echo "firstAnteproc 0" >> ${RUNFolder}/${configFile}
	${MATLAB_ROOT}/bin/matlab -nodisplay <<EOF || exit $?
           run('${STAMPAS}/startup');
           setup_config('${RUNFolder}/${configFile}');
EOF
    fi

    cd - || exit $?
fi



####################
###  check file  ###
####################

### check/copy run_clustermap matfile
if [[ ${newFolder} == false ]] && \
    ( ! [[ -f ${RUNFolder}/run_clustermap.m ]] ); then
    echo "$script : run_clustermap.m not found, abording ..." 
    exit 1
elif [[ ${newFolder} == true ]]; then
    cp ${STAMPAS}/functions/run_clustermap.m ${RUNFolder}/
else
    : # do nothing
fi

### check dagman
dagman=stamp_allsky_$(echo $type  | tr '[:upper:]' '[:lower:]').dag
if ! [[ -f ${RUNFolder}/${dagman} ]]; then    
    echo "$script : dagman not found, abording ..." 
    exit 1
fi



### copy some other file 

### Line removal files
doLineRemoval=`cat ${STAMPAS}/params_0.txt | grep doLineRemoval | awk '{print $2}'`
if [[ ${doLineRemoval} == true ]];then
    linefile1=`cat ${STAMPAS}/params_0.txt | grep lineFile1 | awk '{print $2}'`
    linefile2=`cat ${STAMPAS}/params_0.txt | grep lineFile2 | awk '{print $2}'`
  
    if [[ ${newFolder} == false ]] && \
	( ( ! [[ -f ${RUNFolder}/${linefile1} ]] ) || \
	( ! [[ -f ${RUNFolder}/${linefile2} ]] ) ); then
	echo "$script : ${linefile1} & ${linefile2} missing abording ..."
	exit 1
    elif [[ ${newFolder} == true ]]; then
	cp ${STAMPAS}/${linefile1} ${RUNFolder} || exit $?
	cp ${STAMPAS}/${linefile2} ${RUNFolder} || exit $?
    else
	: # do nothing
    fi
fi


### seedlessParam
doSeedless=`cat ${STAMPAS}/${configFile} | grep doSeedless | awk '{print $2}'`
if [[ ${newFolder} == 1 ]] && \
    ( ! [[ -f ${RUNFolder}/seedless_params.txt ]] ); then
    echo "$script : seedless_param not found, abording ..." 
    exit 1
elif [[ ${newFolder} == true ]]; then
    cp ${STAMPAS}/seedless_params.txt ${RUNFolder}/
else
    : # do nothing
fi
