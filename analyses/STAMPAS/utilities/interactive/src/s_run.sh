#!/bin/bash  
#  run run_clustermap into a screen process 
# ------------------------------------------------------------------
# Syntax:
#   ./s_run.sh
# ------------------------------------------------------------------
# Purpose: 
#    execute getFtmap.m function with all parameter define by the 
#    ftmap.sh script
#    
# ------------------------------------------------------------------
# author:                         FREY valentin
# EMail Address:                  valentin.frey@gmail.com
# version                         1.1
# Comment:                        
# ------------------------------------------------------------------

logFile=${RUNFolder}/logs/ftmap_${gpsWindows}.txt

if [[ $type == 'INJ' ]];then
    args=$wvfId','$injIdx','$doMat
fi

### remove all possible old plots
if [[ $type == 'INJ' ]];then
    plotFolder="DiagPlots/plots_"${wvfName}"/plots_"${gpsWindows}
    if [ -d ${plotFolder} ]; then
	echo "Deleting " ${plotFolder}
	rm -R ${plotFolder}
    fi
fi

### run run_clustermap with all parameter define from ftmap.sh

$MATLAB_ROOT/bin/matlab -nodisplay -logfile ${logFile} <<EOF
   addpath('${interactive}/src')
   addpath('${STAMPAS}/waveforms');                                             
   addpath('${STAMPAS}/cachefiles');                                            
   run('${STAMPAS}/startup');
   getFtmap('${RUNFolder}',$args);   
   display('${Mcheck}');
   exit                 
EOF


### check if matlab run work fine                                               
output=`tail -1 ${logFile}`
if [[ $output != $Mcheck ]];then
    echo -e "\nProblem happening when using matlab " >> ${logFile}
    echo "Please check the log file for" >> ${logFile}
    echo "more detail" >> ${logFile}
    exit 1
fi

### make webfolder for the plot                                                 
webFolder=~/$web_repo/${search}/Start-${start}_Stop-${stop}_ID-${id}/DiagPlots/$webRep
if [ -d ${webFolder} ]; then
    rm -R ${webFolder} || exit $?
fi
mkdir -p ${webFolder} || exit $?

### generate th web page
matlab -nodisplay -logfile ${logFile}_2<<EOF
    addpath(genpath('${interactive}'));
    run('${STAMPAS}/startup');
    plot2html('$webFolder',$wvfId, $injIdx)
EOF
cp ${interactive}/plot2html/style.css $webFolder/style.css || exit $?
cp ${interactive}/plot2html/main.js $webFolder/main.js || exit $?

echo -e "\n**************************************************" >> $logFile
echo -e "**                                              **" >> $logFile
echo -e "** Please before run another time this script   **" >> $logFile
echo -e "** make sure you rename the DiagPlots & result  **" >> $logFile
echo -e "** folder. You don't want to overwrite this     **" >> $logFile
echo -e "** folder                                       **" >> $logFile
echo -e "**                                              **" >> $logFile
echo -e "**************************************************" >> $logFile

exit 0

