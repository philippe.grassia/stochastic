function [] = getFtmap(searchPath,varargin)  
% getFtmap : launch clustermap to have ftmap
% DESCRIPTION :
%    find & compute parameters to launch clustermap in
%    interactive. work for both BKG & INJ
% SYNTAX :
%   [] = getFtmap (searchPath,varargin)
% 
% INPUT : 
%   searchPath : path for the search directory 
%   varargin :
%    for INJ :
%        wvfId  : Id of the waveform
%        injIdx : index of the injection
%    
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.1
% DATE : mov 2015
%

%%% Set Env
setenv('LD_LIBRARY_PATH','');

%%% get Params
params=getParams(searchPath);

%%% get argins
if params.stampinj
    wvfId=varargin{1};
    injIdx=varargin{2};
    if nargin==4
        params.saveMat=varargin{3};
    end
    %%% wvf parameters
    wvfs=get_waveform_parameters(searchPath);
    alphas=wvfs.alphas{wvfId};
    
    %%% Ra Dec
    radec=load([searchPath '/tmp/radec.txt']);
    params.stamp.ra=num2str(radec(injIdx,1));
    params.stamp.decl=num2str(radec(injIdx,2));
    params.stamp.startGPS=num2str(radec(injIdx,2+ ...
                                        wvfId));

    %%% joblist
    jobs=load([searchPath '/tmp/joblist_injections.txt']);
    injStart=radec(injIdx,wvfId+2);
    injStop=radec(injIdx,wvfId+2)+wvfs.duration(wvfId);
    ext=injStop>jobs(:,2) & injStart<jobs(:,3);
    joblist=jobs(ext,:);

    %%% additional parameters
    params.nameInj=wvfs.waveform_names{wvfId};
    currentLag=1;
end


for AA=1:length(alphas);
    params.paramsFile = [searchPath '/tmp/params_inj_' num2str(wvfId) '_' num2str(injIdx) '_' num2str(AA) '.txt'];
    for jobNumber=1:size(joblist,1)
        params.injectionWindowIndex=joblist(jobNumber,1);

        %%% put the seed in order to recover parameters
        rng(joblist(jobNumber,2)+100*(AA-1)+ ...
            wvfId,'twister');
        if params.doZebragard
            if jobNumber==1 | jobNumber~=1 & joblist(jobNumber,5)~=joblist(jobNumber-1,5)
                params.ra=rand(5,1)*24;
                params.dec=(180/pi)*(acos(1-2*rand(5,1))-pi/2);
            end
        else
            params.ra=rand(1,1)*24;
            params.dec=(180/pi)*(acos(1-2*rand(1,1))-pi/2);   
        end
        
        t_start=joblist(jobNumber,2);
        t_end=joblist(jobNumber,3);

        %%% make folder for Plots
        params.savePlots=true;
        system(['mkdir -p ' searchPath '/DiagPlots/plots_' params.nameInj ...
                '/plots_' num2str(params.stamp.startGPS,'%d') ...
                '/plots_' num2str(alphas(AA),'%6f') '/plots_' num2str(t_start) ]);
        params.plotdir=[searchPath '/DiagPlots/plots_' params.nameInj ...
                        '/plots_' num2str(params.stamp.startGPS,'%d') ...
                        '/plots_' num2str(alphas(AA),'%6f') ...
                        '/plots_' num2str(t_start)];
        
        %%% make folder for Mats
        if params.saveMat
            system(['mkdir -p ' searchPath '/DiagMats/plots_' params.nameInj ...
                    '/plots_' num2str(params.stamp.startGPS,'%d') ...
                    '/plots_' num2str(alphas(AA),'%6f') '/plots_' ...
                    num2str(t_start) ]);
            params.ftmapdir=[searchPath '/DiagPlots/plots_' params.nameInj ...
                             '/plots_' num2str(params.stamp.startGPS,'%d') ...
                             '/plots_' num2str(alphas(AA),'%6f') ...
                             '/plots_' num2str(t_start)];
        end
        
        %%% launch clustermap 
        stoch_out=clustermap(params, t_start, t_end); 
    end
end

 

return

