#! /usr/bin/env bash
# run interactively clustermap and make plot in a html web page
# ------------------------------------------------------------------
# Syntax:
#   ./ftmaps.sh
# ------------------------------------------------------------------
# Purpose:
#   This script runs clustermap and generates web page that displays
#   Diagplot. It works for both injection & background. 
#   Just request injection id (injection), lagnumber (background) and
#   a gsp time. Matlab output are written in an logs/ftmaps.txt file, 
#   ftmaps are stored in the DiagPlots folder.
#   plot2html use DiagPlots folder to generate an index.html in a
#   specifique folder. Can be run independently of this script
#   This script just call sub routine in order to put right ENV 
#   variable
#
#   see STAMPAS README for more information about clustermap 
# ------------------------------------------------------------------
# author:                         FREY valentin
# EMail Address:                  valentin.frey@gmail.com 
# version:                        1.3
# Comment:                        Modified by MAB
# ------------------------------------------------------------------ 


### find where is the interactive folder        
if [[ -L $0 ]];then
    export interactive=`readlink $0 | sed 's|/ftmaps.sh||'`
else
    export interactive=`echo $0 |sed 's|/ftmaps.sh||'`
fi

### configure the ftmap parameters & check every file request for 
### ftmap are present
if ! [[ -d ${interactive}/src ]]; then
    echo -e "\e[1;31m $0 : folder ${interactive}/src missing, abording ...\e[0m"
    exit 1
fi
source ${interactive}/src/check.sh || exit $?


### Run the init.sh 
### this script give the parameters of the run to study
### search, type, start, stop, id ...
source ${interactive}/src/init.sh || exit $?


### check if all file need to run run_clustermap are accesible
### in Start-*_Stop-*_ID-*
### if this folder doesn t exist just copy the request file 
### from STAMPAS
source ${interactive}/src/setup.sh


### we make the distinction between INJ & BKG
### for INJ main variable is Injection idx
### for BKG main variable is lag idx             
cd ${RUNFolder} || exit $?

### for INJ
if [[ "$type" == 'INJ' ]]; then
    ###  Choose an injection in the list of possible injection  
    wvfStep=0
    echo -e "\n\e[0;33mWAVEFORM : \e[0m"
    while [ $wvfStep == 0 ]; do
	echo -e "\e[0;33m | \e[0m Choose a waveform between the following"
	grep -v % waveforms.txt | awk 'BEGIN{a=1;}{if($1 != ""){printf "\033[0;33m | \033[0m [%i] \t %s\n",a++, $1}}'
	nbWvf=`grep -v % waveforms.txt | wc -l` 
	read -p "$(echo -e "\e[0;33m | [ ${hwvf} ]> \e[0m")" wvfId

	### if empty get the history value
	if [[ ${wvfId} == '' ]];then
	    wvfId=${hwvf}
	fi	
	
	### check the wvfId values
	if [[ ${wvfId} == '' ]] || [[ ${wvfId} -le 0 ]] || [[ ${wvfId} -gt ${nbWvf} ]];then
	    echo -e "\e[0;33m | \e[1;31m Bad waveform id, please write the [id] of the waveform\e[0m\n"
	else
	    wvfStep=1
	fi
    done

    ### save wvfId in the history for a future use
    echo "WVFID " ${wvfId} >> ${historyFile}

    ### find the GPS time
    validGPSTime=(`cat tmp/radec.txt | awk -v x=$((wvfId+2)) '{printf "%10i\n", $x}'`)

    ### find the wvf name
    wvfName=`grep -v % waveforms.txt | awk -v id=${wvfId} '{if (NR==id) print $1}'`

    ### save wvfName in the history for a future use
    echo "WVFNAME " ${wvfName} >> ${historyFile}

### for BKG
elif [[ "$type" == 'BKG' ]];then
    ###  Choose an lag number between 1 & NTShifts
    lagStep=0
    echo -e "\n\e[0;33mLAG NUMBER : \e[0m"
    while [ $lagStep == 0 ]; do
        nbLag=` grep '^NTShifts ' config_bkg.txt | awk '{print $2}'`
	echo -e " \e[0;33m|\e[0m Choose a lag number between [1-"$nbLag"]"
        read -p "$(echo -e " \e[0;33m| [${hlagNumber}]>\e[0m ")" lagNumber
	
	### if empty take the history value
	if [[ ${lagNumber} == '' ]];then
	    lagNumber=${hlagNumber}
	fi

	### Check th lagNumber value
        if [[ $lagNumber == '' ]] || [[ $lagNumber -le 0 ]] && [[ $lagNumber -gt $nbLag ]]; then
            echo -e " \e[0;33m|\e[1;31m Bad lag number, please retry\e[0m\n"
        else
            lagStep=1
        fi
    done
    
    ### save lagNumber in the history for a future use
    echo "LAGNUMBER " ${lagNumber} >> ${historyFile}

    ### make job_idx array
    nb_job_idx=`awk '{print $5}' tmp/jobfile_bknd.txt | uniq | tail -1 | awk '{printf "%i", $0}'`
    for (( ii=1;ii<=$nb_job_idx;ii=$ii+12 ));do
	job_idx[ii]=$ii
    done

# if not BKG & INJ 
else
    echo -e "\e[1;31m Jobs need to be injection or background\e[0m"
    exit 1
fi # type condition


### Choose the map beginning gps in the possible list
gpsStep=0
echo -e "\n\e[0;33mGPS TIME :  \e[0m"
while [ $gpsStep == 0 ]; do
    echo -e " \e[0;33m|\e[0m Choose a GPS time for the map between $start & $stop"
    ### ask if the user wants to see the possible gps time
    if [[ $type = 'INJ' ]];then
	echo -e " \e[0;33m|\e[0m \e[0;33m/!\ this GPS time must be in radec.txt file\e[0m" 
	echo -e " \e[0;33m|\e[0m do you want to see the possible GPS time y/n"
	read -p "$(echo -e "\e[0;33m | [n]> \e[0m")" seeGpsTime
	
        ### If the user wants to see the possible gps time
	if [[ "$seeGpsTime" == 'y' ]];then
	    for ((gps=0;gps<=${#validGPSTime[@]};gps=gps+5)); do
		echo -e  "${validGPSTime[$gps]}\t${validGPSTime[$((gps+1))]}\t${validGPSTime[$((gps+2))]}\t${validGPSTime[$((gps+3))]}\t${validGPSTime[$((gps+4))]}"
	    done
	    echo -e " \e[0;33m|\e[0m Choose a GPS time for the map between the previous"
	fi
	
        ### read the gsp time
	echo -e " \e[0;33m|\e[0m Choose a GPS time for the map"
    fi 
    read -p "$(echo -e " \e[0;33m| [ ${hgpsWindows} ]>\e[0m")" gpsWindows

    if [[ "${gpsWindows}" == '' ]];then
	gpsWindows=${hgpsWindows}
    fi

    ### check the gps time
    if [[ "${gpsWindows}" == '' ]];then
    	echo -e ' \e[0;33m|\e[1;31m Bad gps time entry, please retry\e[0m'
    elif [[ $type == 'INJ' ]];then
	injIdx=`grep -hnT $gpsWindows tmp/radec.txt | awk '{print $1}'`
	if [[ "${injIdx}" == '' ]];then
	    echo -e ' \e[0;33m|\e[1;31m No windows index found for gsp time '$gpsWindows.0'\e[0m' 
	else
	    gpsStep=1
	fi     # end if inj
    else
        gpsStep=1
    fi
    # check gps time
    
done           # end of the gpstime step

### save wvfId in the history for a future use
echo "GPSWINDOWS " ${gpsWindows} >> ${historyFile}


### Choose a web repository name
webStep=0
echo -e "\n\e[0;33mWEB REPOSITORY :\e[0m"
echo -e " \e[0;33m|\e[0m Other file in " ~/$web_repo/${search}/Start-${start}_Stop-${stop}_ID-${id}/Diagplot/
ls -l ~/$web_repo/${search}/Start-${start}_Stop-${stop}_ID-${id}/Diagplot/ 2> /dev/null | \
    awk '{printf "\033[0;33m|\033[0m %s", $1}'
while [ $webStep == 0 ]; do
    echo -e " \e[0;33m|\e[0m Choose a repository name for the DiagPlots"
    read -p "$(echo -e " \e[0;33m| [ ${gpsWindows} ]>\e[0m")" webRep

    if [[ ${webRep} == '' ]];then
	webRep=${gpsWindows}
    fi

    if [[ ${webRep} == '' ]];then
	echo -e " \e[0;33m|\e[1;31m Bad web repository name please retry\e[0m"
    else
	webStep=1
    fi
done

### save wvfId in the history for a future use
echo "WEBREP " ${webRep} >> ${historyFile}



### save  matfile
matStep=0
echo -e "\n\e[0;33mMATFILES : \e[0m"
while [ ${matStep} == 0 ];do
    echo -e " \e[0;33m|\e[0m Do you want to save matfile y/n"
    read -p "$(echo -e " \e[0;33m| [y]>\e[0m")" doMat

    ### if empty set doMat to n
    if [[  ${doMat} == '' ]];then
	doMat=n;
    fi

    ### Check doMat values
    if [[ ${doMat} != 'n' ]] && [[ ${doMat} != 'y' ]];then
	echo -e " \e[0;33m|\e[1;31m Bad entry please write n or y\e[0m"
    else
	matStep=1
	if [[ $doMat == 'y' ]];then
	    doMat=true
	else
	    doMat=false    
	fi
    fi
done

### move into the initial folder
cd - || exit $?

### Print Summary of the Job
echo -e "\n\e[1;34mSUMMARY :\e[0m"
echo -e " \e[1;34m|\e[0m You are allowed to launch a job with the following parameters"
echo -e " \e[1;34m|\e[0m Please check that every thing it's OK"
echo -e " \e[1;34m| TYPE   :\e[0m " $type
echo -e " \e[1;34m| START  :\e[0m " $start
echo -e " \e[1;34m| STOP   :\e[0m " $stop
echo -e " \e[1;34m| ID     :\e[0m " $id
if [[ ${type} == 'INJ' ]];then
    echo -e " \e[1;34m| WVFID  :\e[0m " $wvfId
    echo -e " \e[1;34m| WVFNAME  :\e[0m " $wvfName
    echo -e " \e[1;34m| GPSINJ :\e[0m " $gpsWindows
else
    echo -e " \e[1;34m| LAG    :\e[0m " $lagNumber
    echo -e " \e[1;34m| GPSWIN :\e[0m " $gpsWindows
fi
echo -e " \e[1;34m| WEBREP :\e[0m " $webRep
echo -e " \e[1;34m| DOMAT  :\e[0m " $doMat
echo -e " \e[1;34m|\e[0m"
echo -e " \e[1;34m|\e[0m Launch the jobs "
launchStep=0
while [[ ${launchStep} == 0 ]];do 
    read -p "$(echo -e " \e[1;34m| [y]>\e[0m ")" launch
    if [[ ${launch} == '' ]];then
	launch=y
    fi

    if [[ ${launch} != 'n' ]] && [[ ${launch} != 'y' ]];then 
	echo -e " \e[1;34m|\e[1;31m Please write y or n\e[0m"
    elif [[ ${launch} == 'n' ]];then
	echo -e " \e[1;34m|\e[1;31m You enter n, then abording...\e[0m"
	exit 0
    else
	launchStep=1
    fi
done

echo -e " \e[1;34m|\e[0m"
echo -e " \e[1;34m|\e[0m Diagplot will be put in ~/$web_repo/${search}/Start-${start}_Stop-${stop}_ID-${id}/Diagplot/$webRep"
echo -e " \e[1;34m|\e[0m ${webHost}/~${USER}${web_repo_suffix}/${search}/Start-${start}_Stop-${stop}_ID-${id}/Diagplot/$webRep"


### export variables before launch clustermap
export webRep
export doMat
export gpsWindows
if [[ ${type} == 'INJ' ]];then
    export wvfId
    export injIdx
    export wvfName
elif [[ ${type} == 'BKG' ]];then
    export lagNumber
else
    echo -e '\e[1;31m type can only be INJ or BKG\e[0m'
    exit 1
fi

### start new screen & run matlab 
screen -d -m -S "interactive-${Mcheck}" || exit $?
screen -p 0 -S "interactive-${Mcheck}" -X stuff $'${interactive}/src/s_run.sh\n' || exit $?

### kill the screen once all process are over
#screen -p 0 -S "interactive-${Mcheck}" -X stuff $'exit\n'


echo -e "\n**************************************************"
echo -e "**                                              **" 
echo -e "** clustermap launch in a screen process        **"
echo -e "** You can see the progress of the job with the **"
printf "** %44s **\n" "screen -r -S \"interactive-${Mcheck}\""
echo -e "**                                              **"
echo -e "** You can detach this screen with ^a d command **"
echo -e "** see screen for more information              **"
echo -e "**                                              **"
echo -e "** You can also see the logs file if the run    **"
echo -e "** crashes :                                    **"
echo -e "** more logs/ftmap_${gpsWindows}.txt                          **"
echo -e "**                                              **"
echo -e "**************************************************" 

exit 0
