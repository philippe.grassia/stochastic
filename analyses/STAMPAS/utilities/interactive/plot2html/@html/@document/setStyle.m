function css=setStyle(obj,css)  
% setStyle :
% DESCRIPTION :
% 
% SYNTAX :
%   setStyle ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
    l=obj.createElement('link');
    l.setAttribute('rel','stylesheet');
    l.setAttribute('href',sprintf('%s',css));
    obj.head.appendChild(l);

end

