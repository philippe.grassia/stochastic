function node = createNode(obj,tag,varargin)  
% createNode :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = createNode ()
% 
% INPUT : 
%
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jan 2016
%

%------------------------------------------------
%% INPUT PARSER
%------------------------------------------------
p=inputParser;
addRequired(p,'obj');
addRequired(p,'tag');
if mod(nargin,2);addOptional(p,'text','',@ischar);end
if verLessThan('matlab','8.2')
    addParamValue(p,'Parent','');
    addParamValue(p,'Sibling','');
    addParamValue(p,'Attribute','',@isstruct);
else
    addParameter(p,'Parent','');
    addParameter(p,'Sibling','');
    addParameter(p,'Attribute','',@isstruct);
end
parse(p,obj,tag,varargin{:});
obj       = p.Results.obj;
tag       = p.Results.tag;
parent    = p.Results.Parent;
sibling   = p.Results.Sibling;
attribute = p.Results.Attribute;
if mod(nargin,2)
    text = p.Results.text;
else
    text='';
end

node=obj.createElement(tag);
if ~isempty(text)
    node.appendChild(obj.createTextNode(text));
end
if ~isempty(attribute)
    f=fieldnames(attribute);
    for i=1:numel(f)
        if strcmp(f{i},'id')
            obj.putIdentifier(attribute.(f{i}),node);
        else
            node.setAttribute(f{i},attribute.(f{i}));
        end
    end
end
if ~isempty(parent)
    parent.appendChild(node);
elseif ~isempty(sibling)
    sibling.getParentNode().insertBefore(node, sibling.getNextSibling());
end

return

