function schema
    HTMLPackage = findpackage('html');
    javaPackage = findpackage('javahandle');
    javaClass = findclass(javaPackage, 'org.apache.xerces.dom.DocumentImpl');
    documentClass = schema.class(HTMLPackage, 'document', javaClass);  

    %%% add properties
    p=schema.prop(documentClass,'body','org.apache.xerces.dom.DeepNodeListImpl');
    p.GetFunction=@(s,v,p) getBody(s);
    p=schema.prop(documentClass,'title','string');
    p.SetFunction=@(s,v,p) setTitle(s,v); 
    p=schema.prop(documentClass,'head','org.apache.xerces.dom.DeepNodeListImpl');
    p.getFunction=@(s,v,p) getHead(s);
    p=schema.prop(documentClass,'style','string');
    p.setFunction=@(s,v,p) setStyle(s,v);
    p=schema.prop(documentClass,'script','string');
    p.setFunction=@(s,v,p) setScript(s,v);
end
