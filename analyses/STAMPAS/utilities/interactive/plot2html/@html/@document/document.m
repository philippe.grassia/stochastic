function obj = document(dtd)  
% document :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = document ()
% 
% INPUT : 
%    
% OUTPUT :


%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
    
    if nargin == 0
        di=org.apache.xerces.dom.DocumentImpl();
        dtd=di.createDocumentType('html',...
                                  '-//W3C//DTD XHTML 1.0 Strict//EN', ...
                                  'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd');
    end
    obj=html.document(dtd);
    
    %%% create html,head,body etc ...
    h=obj.createElementNS('http://www.w3.org/1999/xhtml', 'html');
    obj.appendChild(h);
    
    h.appendChild(obj.createElement('head'));
    h.appendChild(obj.createElement('body'));

end

