function title=setTitle(obj,title)  
% setTitle :
% DESCRIPTION :
% 
% SYNTAX :
%   title ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
    t=obj.createElement('title');
    t.appendChild(obj.createTextNode(sprintf('%s',title)));
    obj.head.appendChild(t);

end

