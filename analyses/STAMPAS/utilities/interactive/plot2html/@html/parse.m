function doc=parse(f)  
% parse :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = parse ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Jan 2016
%

import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.parsers.DocumentBuilder
import org.w3c.dom.Document

%%% parse file f
dbFactory = DocumentBuilderFactory.newInstance();
dBuilder = dbFactory.newDocumentBuilder();
d = dBuilder.parse(java.io.File(f));

%%% instancea new html.document
doc = html.document();

%%% fill the new document with the file nodes 
doc.documentElement.appendChild(doc.adoptNode(d.getDocumentElement));

return

