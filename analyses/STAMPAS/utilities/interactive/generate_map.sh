#! /usr/bin/env bash

usage(){
    echo 'Arg 1: window duration'
    echo 'Arg 2: waveform name [adi_A_tapered]'
    echo 'Arg 3: minimal frequency'
    echo 'Arg 4: maximal frequency'
    echo 'Arg 5: amplitude factor'
    exit
}

# main
if [[ $# -lt 5 ]]; then
    usage
fi

GPSSTART=1134567890
DURATION=$1
WVF_NAME=$2
FMIN=$3
FMAX=$4
FACTOR=$5

HOST=`hostname -d`
if [[ ${HOST} =~ "atlas" ]]; then
    OUTPUT='/home/${USER}/WWW/LSC/simulation'
else
    OUTPUT='/home/${USER}/public_html/simulation'
fi


matlab -nodisplay -r "addpath src; run('../../startup'); generate_map_simulation ${GPSSTART} ${DURATION} ${WVF_NAME} ${FMIN} ${FMAX} ${FACTOR} ${OUTPUT}"