function [map,time,freq]=plotWvfs(name,out)  
% plotWvfs : plot SATMPAS waveform model 
% DESCRIPTION :
%    make a FTmap of a waveform model and save the .png plot & save the mat
%    file containing the data.
%
% SYNTAX :
%   [map,time,freq] = plotWvfs (name,out)
% 
% INPUT : 
%    name : waveform name (eg adiA, maXgnetarD etc...)
%    out  : name of the .mat & .png file
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%
  import classes.waveforms.dictionary

  % ----------------------------------------------------------
  %% INIT
  % ----------------------------------------------------------
  %
  dic    = dictionary;
  buffer = 10; % [s]
  dt     = 1;  % [s]
  df     = 1;  % [Hz]

  %% GET WVF INFO FROM THE DICTIONARY
  % ----------------------------------------------------------
  %
  try
    wvf=dic.waveforms(name);
  catch
    fprintf('waveform : %s not found in the dictionary\n', name);
    fprintf('try one of the following waveform :\n');
    for i=dic.id;
      fprintf('\t%s\n',i{:});
    end
  end

  %% READ DATA FROM THE DATAFILE 
  % ----------------------------------------------------------
  %
  fid=fopen(['../../waveforms/' wvf.file]);
  l=fgetl(fid);
  while ischar(l)
    if ~isempty(regexp(l,'^%','match','once'))
      if ~isempty(regexp(l,'^% [0-9]* [0-9]*','match','once'))
        c=strsplit(l,' ');
        hrss_file=str2num(c{end});
      end
      l=fgetl(fid);
      continue
    end
    data=fscanf(fid, '%f %f %f\n');
    break
  end
  data=reshape(data,3,ceil(length(data)/3))';

  %% GET RESOLUTION & PREPARE DATA TO FTmap
  % ----------------------------------------------------------
  %
  deltaT = data(2,1)-data(1,1);
  N      = dt*ceil(1/deltaT);

  % add buffer time defor & after the data
  data=[ [[-buffer:deltaT:0]', ...
          zeros(floor(buffer/deltaT)+1,1),...
          zeros(floor(buffer/deltaT)+1,1)  ];data];

  data=[data; ...
        [[data(end,1):deltaT:data(end,1)+buffer]', ...
         zeros(floor(buffer/deltaT)+1,1), ...
         zeros(floor(buffer/deltaT)+1,1)]];

  Max    = 2*ceil(size(data,1)/N)-1;  % x 2 dur to overlap
  map    = zeros(floor(N/2)+1,Max);
  time   = linspace(-buffer,data(end,1),Max);
  freq   = linspace(1,floor(N/2),N/2);

  % ----------------------------------------------------------
  %% FTmap
  % ----------------------------------------------------------
  %
  % loop over the segment, for each segment taper the data using a
  % hann windows then use fft.
  %
  for i=1:Max
    d=zeros(N,1);

    dp = data((i-1)*floor(N/2)+1:min((i+1)*floor(N/2),size(data,1)),2);
    dx = data((i-1)*floor(N/2)+1:min((i+1)*floor(N/2),size(data,1)),3);
    d(1:min(N,length(dx)),1)=dx+dp;

    w = hann(min(N,size(d,1)));
    
    a = fft(d.*w);
    map(:,i)=deltaT/length(a)*sqrt(abs(a(1:floor(length(a)/2)+1)).^2);
  end


  %% save data and plot the FTmap
  % ----------------------------------------------------------
  %
  fprintf('save data into %s.mat file\n',out)
  save([regexprep(out,'.png|.mat','') '.mat'],'map','freq','time');
  
  figure
  %  colormap gray
  %  colormap(flipud(gray))
  colormap autumn
  imagesc(time,freq,map,[0 max(map(:))]);
  ylim([wvf.fmin-0.2*(wvf.fmax-wvf.fmin) wvf.fmax+0.6*(wvf.fmax-wvf.fmin)])
  set(gca,'YDir','normal');
  title(wvf.id)
  xlabel('Time [s]')
  ylabel('Frequency [Hz]')
  c= colorbar;

  set(gca,'FontSize',16);
  set(get(gca,'Title'),'FontSize',20);
  set(get(gca,'XLabel'),'FontSize',18);
  set(get(gca,'YLabel'),'FontSize',18);
  set(get(c,'YLabel'),'Interpreter','Latex');
  set(get(c,'YLabel'),'String','$|\tilde{h}(f,t)|$');
  set(get(c,'YLabel'),'FontSize',18);
  
  fprintf('save plot into %s.png file\n',out)
  print('-dpng',[regexprep(out,'.png|.mat','') '.png']);
end
