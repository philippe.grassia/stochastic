function plots(m, as, epsilon, dist)

%m=3;
%as=.7;
%epsilon=0.035;
%dist=1;

root_filename=['M' num2str(m) 'a' num2str(as) 'eps' num2str(epsilon) ...
	      'dist' num2str(dist)];
filename1=[root_filename '.dat'];
filename2=[root_filename '_tapered.dat'];
a=load(filename1);
b=load(filename2);

dt=a(2,1)-a(1,1);
sampling=1./dt;
fft_size=2^nextpow2(size(a,1));

psd=load('psd1.dat');
freq=psd(:,1);

%% Interpolate the onesided PSD. We assume the PSD is a onesided PSD
%% The interpolation is done for frequencies of the current PSD 
f = sampling/2*linspace(0,1,fft_size/2+1);

fmin=40;
fmax=sampling/2.;
%ext_freq=find(f>=fmin & f<=fmax);
%f=f(ext_freq);
f=transpose(f);

onesided_psd=interp1(freq,psd,f);

% Fix NaN that may happen at the edges. If PSD(f1)==NaN, assign the
% value of the next frequency bin (assuming it is different from NaN)
x=isnan(onesided_psd);
ext=find(x==1);

if size(ext,1)>0
  ext2=ext+1;
  if ext(end)==size(onesided_psd,1)
    ext2(end)=size(onesided_psd,1)-1;
  end
  onesided_psd(ext)=onesided_psd(ext2);
  
  if size(find(isnan(onesided_psd)==1))>0
    display(['PSD is still containing NaNs after correction with next' ...
	     ' values. Please check the PSD'])
    SNR=NaN;
    return;
  end
end


figure (1)
loglog(psd(:,1), sqrt(psd(:,2)));
hold on
loglog(onesided_psd(:,1), sqrt(onesided_psd(:,2)),'r');
hold off
xlim([10 1000])
grid

figure(2)
ext_beg=1:4096;
plot(a(ext_beg,1), a(ext_beg,3));
hold on
plot(b(ext_beg,1), b(ext_beg,3),'r');
hold off

xlabel ('time [s]');
ylabel ('strain amplitude');
legend('original', 'tapered 1 x frequency')

figure(5)
plot(a(:,1), a(:,3));
hold on
plot(b(:,1), b(:,3),'r');
hold off

xlabel ('time [s]');
ylabel ('strain amplitude');
legend('original', 'tapered 1 x frequency')

figure(4)
ext_end=size(a,1)-4096:size(a,1);
plot(a(ext_end,1), a(ext_end,2));
hold on
plot(b(ext_end,1), b(ext_end,2),'r');
hold off

xlabel ('time [s]');
ylabel ('strain amplitude');
legend('original', 'tapered')

% hrss calculation

hrss_o=sqrt(sum(a(:,2).^2+a(:,3).^2)*dt);
hrss=sqrt(sum(b(:,2).^2+b(:,3).^2)*dt);

% SNR calculation
hpptilde=1./sampling*fft(a(:,2),fft_size);
hcptilde=1./sampling*fft(a(:,3),fft_size);

hptilde=1./sampling*fft(b(:,2),fft_size);
hctilde=1./sampling*fft(b(:,3),fft_size);

figure(3)
loglog(f(:,1), abs(hpptilde(1:fft_size/2+1,1)))
hold on
loglog(f(:,1), abs(hcptilde(1:fft_size/2+1,1)),'r')
hold off
grid
legend('original', 'tapered')
xlim([1 sampling/2])

ss=0;
for (i=1:fft_size/2)
  ss = ss+hpptilde(i,1)*conj(hpptilde(i,1))/onesided_psd(i,2);
  ss = ss+hcptilde(i,1)*conj(hcptilde(i,1))/onesided_psd(i,2);
end

snr_o=2*sqrt(ss*sampling/fft_size);

ss=0;
for (i=1:fft_size/2)
  ss = ss+hptilde(i,1)*conj(hptilde(i,1))/onesided_psd(i,2);
  ss = ss+hctilde(i,1)*conj(hctilde(i,1))/onesided_psd(i,2);
end

snr=2*sqrt(ss*sampling/fft_size);
diff=(snr_o-snr)/snr_o;

display (['Original SNR=' num2str(snr_o)])
display ([' Tapered SNR=' num2str(snr)])
display (['    SNR loss=' num2str(diff*100) '%']);
display (['Original hrss=' num2str(hrss_o)])
display ([' Tapered hrss=' num2str(hrss)])

fid=fopen('snr1.txt', 'a');

fprintf(fid,'%s\n', root_filename);
fprintf(fid,'%s\n', ['Original SNR=' num2str(snr_o)]);
fprintf(fid,'%s\n', [' Tapered SNR=' num2str(snr)]);
fprintf(fid,'%s\n\n', ['    SNR loss=' num2str(diff*100) '%']);
fprintf(fid,'%s\n', ['Original hrss=' num2str(hrss_o)]);
fprintf(fid,'%s\n', [' Tapered hrss=' num2str(hrss)]);

%exit