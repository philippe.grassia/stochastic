# PMVP waveform generator
# van Putten-based model for SN GW radiation
# theta: colatitude
# phi: azimuth

#!/usr/bin/python
import sys
import numpy
import pylab
from pylab import *
from numpy import loadtxt

# constants
msun = 1.98892e33
clite = 2.9979e10
ggrav = 6.6726e-8
factor = 1.0 * ggrav / clite**4
pi = 3.14159265358979e0
trd = 1./3.

def nearestEvenInt (x):
    return int(round(x/2.)*2)


def taper(h, f_low, f_high, dt):
    sampling=1.0/dt

    taper_scale=5.             # number of half-cycles to taper

    n_taper_start=ceil(taper_scale * sampling / f_low)
    n_taper_end=ceil(taper_scale * sampling / f_high)

    n_taper_start=nearestEvenInt(n_taper_start)
    n_taper_end=nearestEvenInt(n_taper_end)

    n_taper_start_half=int(n_taper_start/2)
    n_taper_end_half=int(n_taper_end/2)

#    print "n_taper_start: ", n_taper_start
#    print "n_taper_end: ", n_taper_end

    out=h

    window=np.hanning(n_taper_start)
#    print "window ", len(window) 
#    for i in range(0,n_taper_start):
#        print window[i]
        
    for i in range(0, n_taper_start_half):
        out[i]=out[i]*window[i]

    window=np.hanning(n_taper_end)
#    print "window ", len(window) 
#    for i in range(0,n_taper_end):
#        print window[i]

    for i in range(n_taper_end_half, n_taper_end):
        out[len(out)-n_taper_end+i]=out[len(out)-n_taper_end+i]*window[i]

    return out


# physical parameters
Mbh = 3. * msun  # mass of the central BH (3-10 Msun)
astar = 0.7      # spin of the central BH; astar = Jbh/Mbh^2 (0.3-0.99)
Jbh = astar * Mbh**2. * ggrav/clite  # In cgs units. astar is dimensionless
Mdisk = 1.5 * msun    # mass of the accretion disk/torus (how big?)
epsilon = 0.035    # fraction of Mdisk that goes into the clumps (0.01-0.5 ?)
mch = epsilon * Mdisk  # mass of the chunks forming the 'binary'
rct = 10000000.    # radius of the thin disk. Binary is at rct + Risco
dfactor = 3.08568e18*1000*1000 # conversion factor (Mpc to cm)
dist = 1 # distance in Mpc
D = dfactor*dist  # Distance (converted to cm)

filename = 'M' + str(int(ceil(Mbh/msun))) + 'a' + str(astar) + 'eps' + str(epsilon) + 'dist' + str(dist) + '.dat' 
filename_t = 'M' + str(int(ceil(Mbh/msun))) + 'a' + str(astar) + 'eps' + str(epsilon) + 'dist' + str(dist) + '_tapered.dat' 

x=loadtxt(filename);

print x.shape
print x[0,1]

hplus=x[:,1]
hcross=x[:,2]

dt=x[1,0]-x[0,0]
n_taper=nearestEvenInt(1./dt)
window=np.hanning(n_taper)
wvf_len=len(hplus)

hplus_t=hplus
hcross_t=hcross
  
for i in range(0, n_taper/2):
    hplus_t[i]=hplus_t[i]*window[i]

for i in range(n_taper/2, n_taper):
    hplus_t[wvf_len-n_taper+i]=hplus_t[wvf_len-n_taper+i]*window[i]

for i in range(0, n_taper/2):
    hcross_t[i]=hcross_t[i]*window[i]

for i in range(n_taper/2, n_taper):
    hcross_t[wvf_len-n_taper+i]=hcross_t[wvf_len-n_taper+i]*window[i]

f2 = open(filename_t,'w')

time=0
f2.write('\t'.join(str(col) for col in [time, hplus_t[0], hcross_t[0]]))
f2.write('\n')

for i in range(1, wvf_len):
    time += dt
    f2.write('\t'.join(str(col) for col in [time, hplus_t[i], hcross_t[i]]))
    f2.write('\n')

f2.close()

