python vanPuttengw.py -M 5 -a .3 -e 0.05 -t 2
python vanPuttengw.py -M 10 -a .95 -e 0.2 -t 1
python vanPuttengw.py -M 10 -a .95 -e 0.04 -t 1
python vanPuttengw.py -M 3 -a .7 -e 0.035 -t 2
python vanPuttengw.py -M 8 -a .99 -e 0.065 -t 1
python vanPuttengw.py -M 30 -a .8 -e 0.07 -t 2


matlab -nodisplay -r "plots 5 .3 .05 1"
matlab -nodisplay -r "plots 10 .95 .2 1"
matlab -nodisplay -r "plots 10 .95 .04 1"
matlab -nodisplay -r "plots 3 .7 .035 1"
matlab -nodisplay -r "plots 8 .99 .065 1"
matlab -nodisplay -r "plots 30 .8 .07 1"

#cat adi_A_header.txt > adi_A_tapered.dat
#cat M5a0.3eps0.05dist1_tapered.dat >> adi_A_tapered.dat

#cat adi_B_header.txt > adi_B_tapered.dat
#cat M10a0.95eps0.2dist1_tapered.dat >> adi_B_tapered.dat

#cat adi_C_header.txt > adi_C_tapered.dat
#cat M10a0.95eps0.04dist1_tapered.dat >> adi_C_tapered.dat

#cat adi_D_header.txt > adi_D_tapered.dat
#cat M3a0.7eps0.035dist1_tapered.dat >> adi_D_tapered.dat

#cat adi_E_header.txt > adi_E_tapered.dat
#cat M8a0.99eps0.065dist1_tapered.dat >> adi_E_tapered.dat

#cat adi_F_header.txt > adi_F_tapered.dat
#cat M30a0.8eps0.07dist1_tapered.dat >> adi_F_tapered.dat
