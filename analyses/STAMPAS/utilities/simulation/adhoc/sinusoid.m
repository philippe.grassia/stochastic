function sinusoid(f0,ff,dur,alpha)
% function sinusoid()
%
% Produces a waveform with frequency dependence
% phi(t) = f0*t + (1/2)fdot*t^2 + (1/6)*fddot*t^3
% Waveforms can be monochromatic or have linear or
% quadratic frequency dependence on time.
%
% Based on polarizer.m by Eric Thrane.
% Adapted by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set these parameters:
%f0 = 100;           % initial frequency in Hz
%ff = 200;           % ending frequency in Hz
%dur = 50;           % injection duration in s
%alpha = 1;          % alpha determines the linearity of the frequency
                    % dependence.  alpha = 0 is purely quadratic; alpha = 1
                    % is purely linear.
fdot = alpha*(ff-f0)/dur; % df/dt in Hz/s
fddot = 2*((ff-f0)/dur^2 - fdot/dur); % d^2 f/dt^2 in Hz/s^2
hrssNorm = 1e-20;   % hrss normalization
iota = 0*(pi/180);  % iota in degrees (source inclination)
psi = 0*(pi/180);   % psi in degrees (polarization angle)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print to screen.
fprintf('fdot = %0.3g, fddot = %0.3g\n',fdot,fddot);

% Calculate phase evolution.
fs=4096;
t=[0:1/fs:dur];
Phit = 2*pi*(f0 + 0.5*fdot*t + (1/6.0)*fddot*t.^2).*t;

% Calculate hp and hx.
Ap = (1+cos(iota)^2)/2;
Ax = cos(iota);
hp = Ap*cos(2*psi)*cos(Phit) - Ax*sin(2*psi)*sin(Phit);
hx = Ap*sin(2*psi)*cos(Phit) + Ax*cos(2*psi)*sin(Phit);

% Taper the waveform.
hp = apply_window(hp,fs,1,'both');
hx = apply_window(hx,fs,1,'both');

% Normalize.
hrss = sqrt(trapz(t,(hp.^2 + hx.^2)));
hp = hp .* (hrssNorm/hrss);
hx = hx .* (hrssNorm/hrss);

% Save waveform.
out = [t; hp; hx;];
fname = sprintf('pol_%i_%i_%i.dat',(180/pi)*iota,(180/pi)*psi,dur);
fid = fopen(fname,'w+');
fprintf(fid,'%% fmin fmax dist(Mpc) hrss\n');
fprintf(fid,'%% %d  %d  0 %.2e\n',f0-3,f0+3,hrssNorm);
fprintf(fid,'%% \n');
fprintf(fid,'%% t h+ hx\n');
fprintf(fid,'%.12f\t%.8e\t%.8e\n',out);
fclose(fid);

return;
