function h_out = apply_window(h_in,fs,taper_dur,type)

% Get injection sample times.
N = numel(h_in);
t = (0:N)/fs;

% Define window (factor of two comes from fact that we only use
% 1/2 of a Hann window.
n_taper = ceil(2*taper_dur*fs);
w = (1/2)*(1-cos(2*pi*(0:(n_taper-1))/(n_taper-1)));
if iscolumn(h_in)
  w = w';
end

% Get taper indices
taper_idx_start = 1:ceil(n_taper/2);
wf_idx_start = taper_idx_start;
taper_idx_end = (floor(n_taper/2)+1):n_taper;
wf_idx_end = (N-numel(taper_idx_end)+1):N;

% Determine which ones to use.
if strcmpi(type,'start')
  taper_idx = taper_idx_start;
  wf_idx = wf_idx_start;
elseif strcmpi(type,'end')
  taper_idx = taper_idx_end;
  wf_idx = wf_idx_end
elseif strcmpi(type,'both')
  taper_idx = [taper_idx_start taper_idx_end];
  wf_idx = [wf_idx_start wf_idx_end];
else
  error('type should be ''start'', ''end'', or ''both''.');
end

% Apply taper.
h_out = h_in;
h_out(wf_idx) = h_out(wf_idx) .* w(taper_idx);

return;