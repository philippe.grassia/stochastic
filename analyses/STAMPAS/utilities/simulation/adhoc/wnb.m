function wnb(flow,fhigh,wf_dur)
% function wnb()
%
% Generates band-limited white noise.
% hp and hx components are independent.
%
% Based on code from burst matapps SVN.
% Adapted by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Seed random number generator with clock.
rng(sum(clock*1e6),'twister');

%--- Default parameters --------------
fname = 'wnb.dat'; %-- Filename.
fs = 4096;         %-- Sampling rate (Hz).
hrssNorm = 1e-20;  %-- Default h_rss normalization.
%wf_dur = 100.0;    %-- Length of waveform (seconds).
bp_order = 6;      %-- Order of bandpass filter.
%flow = 700;        %-- Minimum frequency (Hz).
%fhigh = 750;       %-- Maximum frequency (Hz).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate length of time series in samples.
durationSamp = floor(wf_dur*fs);

% Calculate time at each sample.
t = (0:1:durationSamp) ./ fs;

% Make white noise.
hp = randn(size(t));
hx = randn(size(t));

% Construct bandpass filter.
% Note: butter() automatically doubles the order
% of the filter when doing a bandpass
% (vs. high-pass or low-pass).
[b,a] = butter(bp_order/2,[flow fhigh]/(fs/2));

% Aply bandpass filter.
hp = filtfilt(b,a,hp);
hx = filtfilt(b,a,hx);

% Taper the waveform
hp = apply_window(hp,fs,1,'both');
hx = apply_window(hx,fs,1,'both');

% Normalize.
hrss = sqrt(trapz(t,(hp.^2 + hx.^2)));
normFactor = hrssNorm/hrss;
hp = hp .* normFactor;
hx = hx .* normFactor;

% Organize wf array.
wf = [t; hp; hx;];

% Write to file.
fid = fopen(fname,'w+');
fprintf(fid,'%% fmin fmax dist(Mpc) hrss\n');
fprintf(fid,'%% %d  %d  0 %.2e\n',f0-3,f0+3,hrssNorm);
fprintf(fid,'%% \n');
fprintf(fid,'%% t h+ hx\n');
fprintf(fid,'%.12e\t%.8e\t%.8e\n',wf);
fclose(fid);

return;
