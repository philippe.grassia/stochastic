dist = 0;

% sinusoidA
h0 = 1.0e-20;
f0 = 200.00;
phi0 = 0;
fdot = 200/(3600);
duration = 3000; 
fmin = min([f0,f0+fdot*duration]);
fmax = max([f0,f0+fdot*duration]);
hrss = h0;
save('sinusoidA.mat');

% sinusoidB
h0 = 1.0e-20;
f0 = 1700.00;
phi0 = 0;
fdot = -250/(3600);
duration = 3500;
fmin = min([f0,f0+fdot*duration]);
fmax = max([f0,f0+fdot*duration]);
hrss = h0;
save('sinusoidB.mat');

% sinusoidC
h0 = 1.0e-20;
f0 = 200.00;
phi0 = 0;
fdot = 0;
duration = 3000;
fmin = min([f0,f0+fdot*duration]);
fmax = max([f0,f0+fdot*duration]);
hrss = h0;
save('sinusoidC.mat');

% sinusoidD
h0 = 1.0e-20;
f0 = 1700.00;
phi0 = 0;
fdot = -250/(3600);
duration = 0;
fmin = min([f0,f0+fdot*duration]);
fmax = max([f0,f0+fdot*duration]);
hrss = h0;
save('sinusoidD.mat');

% sinusoidE
h0 = 1.0e-20;
f0 = 200.00;
phi0 = 0;
fdot = 800/(7200);
duration = 7000;
fmin = min([f0,f0+fdot*duration]);
fmax = max([f0,f0+fdot*duration]);
hrss = h0;
save('sinusoidE.mat');

% sinusoidF
h0 = 1.0e-20;
f0 = 1700.00;
phi0 = 0;
fdot = -900/(7200);
duration = 7500;
fmin = min([f0,f0+fdot*duration]);
fmax = max([f0,f0+fdot*duration]);
hrss = h0;
save('sinusoidF.mat');

% sinusoidE
h0 = 1.0e-20;
f0 = 200.00;
phi0 = 0;
fdot = 800/(30000);
duration = 25000;
fmin = min([f0,f0+fdot*duration]);
fmax = max([f0,f0+fdot*duration]);
hrss = h0;
save('sinusoidG.mat');

% sinusoidH
h0 = 1.0e-20;
f0 = 1700.00;
phi0 = 0;
fdot = -900/(30000);
duration = 30000;
fmin = min([f0,f0+fdot*duration]);
fmax = max([f0,f0+fdot*duration]);
hrss = h0;
save('sinusoidH.mat');



