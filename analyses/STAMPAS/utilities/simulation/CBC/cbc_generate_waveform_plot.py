#!/usr/bin/python

"""
%prog

Michael Coughlin (michael.coughlin@ligo.org)

This program does waveform comparisons.

"""

import os, math
import numpy as np
import matplotlib
matplotlib.rc('text', usetex=True)
matplotlib.use('agg')
import matplotlib.pyplot as plt

import cbc_generate_waveform_utils

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def plot_name(waveform):
    params = ["approximant","mass1","mass2"]
    string = ""
    for param in params:
        if string == "":
            string = str(waveform["args"][param])
        else:
            string = "_".join([string,str(waveform["args"][param])])
    return string

def plot_waveform_approximant(waveforms,type,plotName):

    plt.loglog(waveforms["waveform"]["data"]["f"], waveforms["waveform"]["data"][type], label="Waveform")
    plt.loglog(waveforms["approximant"]["data"]["f"], waveforms["approximant"]["data"][type], label="Approximant")
    plt.xlabel("Frequency [Hz]")
    plt.xlim([10,2048])
    plt.legend()
    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_waveform_deltas(waveforms,type,plotName):

    for waveform in ["neg","zero","pos"]:
        plt.loglog(waveforms[waveform]["data"]["f"], waveforms[waveform]["data"][type], label=waveform)
    plt.xlabel("Frequency [Hz]")
    plt.xlim([10,2048])
    plt.legend()
    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_waveform_derivative(derivative1,derivative2,type,plotName):

    plt.loglog(derivative1["f"], derivative1[type], label="1st Derivative")
    plt.loglog(derivative2["f"], derivative2[type], label="2nd Derivative")
    plt.xlabel("Frequency [Hz]")
    plt.xlim([10,2048])
    plt.legend()
    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_overlap(f,htilde,stilde,psd,plotName):

    plt.loglog(f, htilde, label="htilde")
    plt.loglog(f, stilde, label="stilde")
    plt.loglog(f, psd, label="psd")
    plt.xlabel("Frequency [Hz]")
    plt.xlim([10,2048])
    plt.legend()
    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_waveform(waveform,plotDirectory):

    plt.plot(waveform["timedata"]["t"], waveform["timedata"]["hplus"], label="h+")
    plt.plot(waveform["timedata"]["t"], waveform["timedata"]["hcross"], label="hx")
    plt.legend()
    plt.show()
    plotName = os.path.join(plotDirectory,"plus_cross.png")
    plt.savefig(plotName,dpi=256)
    plt.close('all')

    plt.plot(waveform["data"]["f"], waveform["data"]["hamp"], label="hamp")
    plt.xlabel("Frequency [Hz]")
    plt.xlim([1,256])
    plt.legend()
    plt.show()
    plotName = os.path.join(plotDirectory,"amp.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')

    plt.plot(waveform["data"]["f"], waveform["data"]["hphase"], label="hphase")
    plt.xlabel("Frequency [Hz]")
    plt.xlim([1,256])
    plt.legend()
    plt.show()
    plotName = os.path.join(plotDirectory,"phase.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_waveform_distance_error(waveforms):

    plotDirectory = "/home/mcoughlin/PE/wavecomp/plots/%s_%s/waveform_distance_error"%(waveforms["approximant"]["args"]["approximant"],waveforms["approximant_errors"]["args"]["approximant"])
    if not os.path.isdir(plotDirectory):
        pe_waveform_comparison_utils.mkdir(plotDirectory)

    keys = ["approximant","approximant_errors"]
    for key in keys:
        plt.semilogy(waveforms[key]["data"]["f"], waveforms[key]["data"]["hamp"], label=key.replace("_","\_"))
    plt.xlabel("Frequency [Hz]")
    plt.legend()
    plt.xlim([0,2000])
    plt.show()
    plotName = os.path.join(plotDirectory,"amp.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')

    for key in keys:
        plt.plot(waveforms[key]["data"]["f"], waveforms[key]["data"]["hphase"], label=key.replace("_","\_"))
    plt.xlabel("Frequency [Hz]")
    plt.legend()
    plt.xlim([0,2000])
    plt.show()
    plotName = os.path.join(plotDirectory,"phase.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')

    for key in keys:
        plt.semilogy(waveforms[key]["data"]["f"], np.real(waveforms[key]["data"]["hamp"] * np.exp(1j* waveforms[key]["data"]["hphase"])), label=key.replace("_","\_"))
    plt.xlabel("Frequency [Hz]")
    plt.legend()
    plt.xlim([0,2000])
    plt.show()
    plotName = os.path.join(plotDirectory,"total.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_waveform_distance_error_psd(waveforms,vector,psd):

    plotDirectory = "/home/mcoughlin/PE/wavecomp/plots/%s_%s/waveform_distance_error"%(waveforms["approximant"]["args"]["approximant"],waveforms["approximant_errors"]["args"]["approximant"])
    if not os.path.isdir(plotDirectory):
        pe_waveform_comparison_utils.mkdir(plotDirectory)

    key = "approximant"
    plt.semilogy(waveforms[key]["data"]["f"], vector * np.ma.conjugate(vector), label="vector")
    plt.semilogy(waveforms[key]["data"]["f"], psd, label="psd")
    plt.xlabel("Frequency [Hz]")
    plt.legend()
    plt.xlim([0,2000])
    plt.show()
    plotName = os.path.join(plotDirectory,"psd.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')


def plot_derivatives(derivatives):

    plotDirectory = "/home/mcoughlin/PE/wavecomp/plots/%s/%s"%(derivatives[0]["approximant"],derivatives[0]["parameter"])
    if not os.path.isdir(plotDirectory):
        pe_waveform_comparison_utils.mkdir(plotDirectory)

    for derivative in derivatives:
        plt.loglog(derivative["derivative"]["zero"]["f"], derivative["derivative"]["zero"]["hamp"], label=str(derivative["delta"]))

    plt.xlabel("Frequency [Hz]")
    plt.legend()
    plt.show()
    plotName = os.path.join(plotDirectory,"amp_derivative.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')

    for derivative in derivatives:
        plt.semilogx(derivative["derivative"]["zero"]["f"], derivative["derivative"]["zero"]["hphase"], label=str(derivative["delta"]))

    plt.xlabel("Frequency [Hz]")
    plt.legend()
    plt.show()
    plotName = os.path.join(plotDirectory,"phase_derivative.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')

    for derivative in derivatives:
        plt.semilogx(derivative["derivative"]["zero"]["f"], derivative["derivative"]["zero"]["total"], label=str(derivative["delta"]))

    plt.xlabel("Frequency [Hz]")
    plt.legend()
    plt.show()
    plotName = os.path.join(plotDirectory,"total_derivative.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')

    for derivative in derivatives:
        plt.plot(derivative["derivative"]["zero"]["t"], derivative["derivative"]["zero"]["total_ifft"], label=str(derivative["delta"]))

    plt.xlabel("Time [s]")
    plt.legend()
    plt.show()
    plotName = os.path.join(plotDirectory,"total_derivative_ifft.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')


def plot_distance(f,amp):

    plotDirectory = "/home/mcoughlin/PE/wavecomp/plots"
    if not os.path.isdir(plotDirectory):
        pe_waveform_comparison_utils.mkdir(plotDirectory)

    plt.semilogy(f,amp)
    plt.xlim([1,2048])
    plt.ylim([10**(-29),10**(-21)])
    plt.xlabel("Frequency [Hz]")
    plt.legend()
    plt.show()
    plotName = os.path.join(plotDirectory,"test.png")
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_error(x,y,matrix,xlabel,ylabel,plotName):

    vmin = np.min(matrix)
    vmax = np.max(matrix)

    X,Y = np.meshgrid(x, y)
    ax = plt.subplot(111)
    #im = plt.pcolor(X,Y,matrix.transpose(), cmap=matplotlib.cm.jet, vmin=vmin, vmax=vmax)
    im = plt.pcolor(X,Y,matrix.transpose(), cmap=matplotlib.cm.jet)
    #ax.set_xscale('log')
    #im.set_interpolation('bilinear')
    #plt.xlim([min(x),max(x)])

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    cbar=plt.colorbar()
    cbar.set_label('Error')
    plt.grid
    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_error_single(x,y,xlabel,ylabel,type,plotName):

    ax = plt.subplot(111)

    if type == "linear":
        plt.plot(x,y,'*')
    elif type == "semilogx":
        plt.semilogx(x,y,'*')
    elif type == "semilogy":
        plt.semilogy(x,y,'*')
    elif type == "loglog":
        plt.loglog(x,y,'*')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid
    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_psd(data,type,plotName):

    for key, value in data.items():
        plt.loglog(value["freq"],value[type],label=key)
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Amplitude")
    plt.xlim([10,1024])
    plt.legend()
    plt.grid
    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_phase(data,type,plotName):

    for key, value in data.items():
        plt.semilogx(value["freq"],value[type],label=key)
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Amplitude")
    plt.xlim([10,1024])
    plt.legend()
    plt.grid
    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')


