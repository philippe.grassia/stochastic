% run_params
% code to create params files to create mat files with injections

base_waveformsdir = './waveforms/';

% waveforms to create 
waveforms = {'vlm-a','vlm-b','vlm-c','vlm-d','vlm-e','vlm-f','bns-a','bbh-a'};

% basis cbwaves file
cbwavesFile = './cbwgen.pl';

% loop over waveforms
xyz_pp_seed = 1;
for w = 1:length(waveforms)

   waveform = waveforms{w};

   fmin = 40;
   if strcmp(waveform,'bns-a')
      mass1 = 1.4; mass2 = 1.4;
      spin1 = 0; spin2 = 0;
   elseif strcmp(waveform,'nsbh-a')
      mass1 = 3.0; mass2 = 1.4;
      spin1 = 0; spin2 = 0;
   elseif strcmp(waveform,'nsbh-b')
      mass1 = 3.0; mass2 = 1.4;
      spin1 = 0.95; spin2 = 0;
   elseif strcmp(waveform,'bbh-a')
      mass1 = 3.0; mass2 = 3.0;
      spin1 = 0; spin2 = 0;
   elseif strcmp(waveform,'bbh-b')
      mass1 = 3.0; mass2 = 3.0;
      spin1 = 0.95; spin2 = 0;
   elseif strcmp(waveform,'bbh-c')
      mass1 = 3.0; mass2 = 3.0;
      spin1 = 0.95; spin2 = 0.95;
   elseif strcmp(waveform,'imbbh-a')
      mass1 = 5.0; mass2 = 5.0;
      spin1 = 0; spin2 = 0;
   elseif strcmp(waveform,'imbbh-b')
      mass1 = 5.0; mass2 = 5.0;
      spin1 = 0.95; spin2 = 0;
   elseif strcmp(waveform,'imbbh-c')
      mass1 = 5.0; mass2 = 5.0;
      spin1 = 0.95; spin2 = 0.95;
   elseif strcmp(waveform,'imbbh-d')
      mass1 = 10.0; mass2 = 1.4;
      spin1 = 0; spin2 = 0;
   elseif strcmp(waveform,'smbbh-a')
      mass1 = 10.0; mass2 = 10.0;
      spin1 = 0; spin2 = 0;
   elseif strcmp(waveform,'smbbh-b')
      mass1 = 10.0; mass2 = 10.0;
      spin1 = 0.95; spin2 = 0;
   elseif strcmp(waveform,'smbbh-c')
      mass1 = 10.0; mass2 = 10.0;
      spin1 = 0.95; spin2 = 0.95;
   elseif strcmp(waveform,'ebns-a')
      mass1 = 2079.0; mass2 = 2079.0;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.2;
   elseif strcmp(waveform,'ebns-b')
      mass1 = 2079.0; mass2 = 2079.0;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.4;
   elseif strcmp(waveform,'ebns-c')
      mass1 = 2079.0; mass2 = 2079.0;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.6;
   elseif strcmp(waveform,'ensbh-a')
      mass1 = 4455.7; mass2 = 2079.0;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.2;
   elseif strcmp(waveform,'ensbh-b')
      mass1 = 4455.7; mass2 = 2079.0;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.4;
   elseif strcmp(waveform,'ensbh-c')
      mass = 4455.7; mass2 = 2079.0;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.6;
   elseif strcmp(waveform,'ebbh-a')
      mass1 = 4455.7; mass2 = 4455.7;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.2;
   elseif strcmp(waveform,'ebbh-b')
      mass1 = 4455.7; mass2 = 4455.7;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.4;
   elseif strcmp(waveform,'ebbh-c')
      mass1 = 4455.7; mass2 = 4455.7;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.6;
   elseif strcmp(waveform,'eimbbh-a')
      mass1 = 7425; mass2 = 7425;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.2;
   elseif strcmp(waveform,'eimbbh-b')
      mass1 = 7425; mass2 = 7425;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.4;
   elseif strcmp(waveform,'eimbbh-c')
      mass1 = 7425; mass2 = 7425;
      spin1 = 0; spin2 = 0;
      eccentricity = 0.6;
   elseif strcmp(waveform,'vlm-a')
      mass1 = 1.25; mass2 = 1.25;
      spin1 = 0; spin2 = 0;
      fmin = 15;
   elseif strcmp(waveform,'vlm-b')
      mass1 = 1.0; mass2 = 1.0;
      spin1 = 0; spin2 = 0;
      fmin = 15;
   elseif strcmp(waveform,'vlm-c')
      mass1 = 0.75; mass2 = 0.75;
      spin1 = 0; spin2 = 0;
      fmin = 15;
   elseif strcmp(waveform,'vlm-d')
      mass1 = 0.5; mass2 = 0.5;
      spin1 = 0; spin2 = 0;
      fmin = 15;
   elseif strcmp(waveform,'vlm-e')
      mass1 = 0.25; mass2 = 0.25;
      spin1 = 0; spin2 = 0;
      fmin = 20;
   elseif strcmp(waveform,'vlm-f')
      mass1 = 0.10; mass2 = 0.10;
      spin1 = 0; spin2 = 0;
      fmin = 40;
   end

   alphas = 1;

   psi = 0;
   iota = 0;

   waveformFileName = sprintf('%s/waveform_%s.txt',base_waveformsdir,waveform);
   if strcmp(waveform,'ebns-a') || strcmp(waveform,'ebns-b') || strcmp(waveform,'ebns-c') || strcmp(waveform,'ensbh-a') || strcmp(waveform,'ensbh-b') || strcmp(waveform,'ensbh-c') || strcmp(waveform,'ebbh-a') || strcmp(waveform,'ebbh-b') || strcmp(waveform,'ebbh-c') || strcmp(waveform,'eimbbh-a') || strcmp(waveform,'eimbbh-b') || strcmp(waveform,'eimbbh-c')
      out = read_doc(cbwavesFile);

      out = regexprep(out, 'xyz_mass1', num2str(mass1));
      out = regexprep(out, 'xyz_mass2', num2str(mass2));
      out = regexprep(out, 'xyz_spin1z',num2str(spin1));
      out = regexprep(out, 'xyz_spin2z',num2str(spin2));
      out = regexprep(out, 'xyz_epsilon',num2str(eccentricity));
      out = regexprep(out, 'xyz_psi',num2str(psi));
      out = regexprep(out, 'xyz_iota',num2str(iota));
      out = regexprep(out, 'xyz_outfile',waveformFileName);

      cbwavesFileName = sprintf('%s/waveform_%s_alpha_%e_job_%d.txt',base_cbwavesdir,waveform,alpha,job);
      % write new paramfile
      fid=fopen(cbwavesFileName,'w+');
      fprintf(fid, '%c', out);
      fclose(fid);

      fprintf('%s\n',waveformFileName);
      system_call = sprintf('perl %s; cp CBwaves.ini cbwaves.ini; /home/mcoughlin/cbwaves-1.2.0-1/cbwaves/bin/cbwaves cbwaves.ini',cbwavesFileName);
      system(system_call);
           
   else
      system_call = sprintf('source /home/mcoughlin/LAL/master/etc/lscsoftrc; /home/mcoughlin/STAMP/Lonetrack/mc_inj_grb/1/params/cbc_generate_waveform --mass1 %f --mass2 %f --spin1z %f --spin2z %f --inclination %f --polarization %f --f_lower %f --output %s',mass1,mass2,spin1,spin2,iota,psi,fmin,waveformFileName);
      system(system_call);
   end

   hdata = load(waveformFileName);
   t_in = hdata(:,1); hp_in = hdata(:,2); hx_in = hdata(:,3);

   fs = 1/(t_in(2) - t_in(1));
   dt = 1/fs;

   taper = linspace(0,4-dt,fs);
   N = length(taper);
   mask = ones(size(t_in'));
   mask(1:N)=sin(pi/2.*taper).^2;
   mask(end-N+1:end) = fliplr(mask(1:N));

   mask = mask';

   hp_out = hp_in .* mask;
   hx_out = hx_in .* mask;

   % write new paramfile
   fid=fopen(waveformFileName,'w+');
   for j = 1:length(t_in)
      fprintf(fid,'%.10e %.10e %.10e\n',t_in(j),hp_out(j),hx_out(j));
   end
   fclose(fid);      
end



