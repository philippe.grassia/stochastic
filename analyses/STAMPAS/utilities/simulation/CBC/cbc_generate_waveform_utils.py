#!/usr/bin/python

"""
%prog

Michael Coughlin (michael.coughlin@ligo.org)

This program does waveform comparisons.

"""

import os, sys, pickle, math, optparse
import pycbc.waveform, pycbc.fft, pycbc.types, pycbc.filter
import numpy as np

#import matchedfilter

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def mkdir(path):

    pathSplit = path.split("/")
    pathAppend = "/"
    for piece in pathSplit:
        if piece == "":
            continue
        pathAppend = os.path.join(pathAppend,piece)
        if not os.path.isdir(pathAppend):
            os.mkdir(pathAppend)

def create_data(waveform,htilde):

    htilde_real = np.real(htilde)
    htilde_imag = np.imag(htilde)
    hamp = np.sqrt(htilde_real**2 + htilde_imag**2)
    #hphase = np.arctan(np.divide(htilde_imag,htilde_real))

    factor = 0
    valOld = []

    hphase = []
    for htilde_real_val,htilde_imag_val in zip(htilde_real,htilde_imag):
        if htilde_real_val == 0:
            if htilde_imag_val == 0:
                val = 0
            elif htilde_imag_val > 0:
                val = np.pi / 2
            elif htilde_imag_val < 0:
                val = - np.pi / 2
        else:
            val = np.arctan(np.divide(htilde_imag_val,htilde_real_val))

        if not valOld == []:
            if (valOld > 0) and (val < 0):
                factor = factor + np.pi
        val = val + factor
        hphase.append(val)

        valOld = val

    f = []
    for freq in htilde.sample_frequencies:
        f.append(freq)
    f = np.array(f)

    waveform["data"] = {}
    waveform["data"]["f"] = f
    waveform["data"]["htilde_real"] = htilde_real
    waveform["data"]["htilde_imag"] = htilde_imag
    waveform["data"]["hamp"] = hamp
    waveform["data"]["hphase"] = hphase

    fmax_new = 16384
    #fmax_new = 2048
    fmax = max(f)

    fnew = np.arange(waveform["args"]["delta_f"],fmax_new,waveform["args"]["delta_f"])

    waveform["data"]["f"] = fnew
    for key,value in waveform["data"].items():
        if key in ["htilde_real","htilde_imag","hamp"]:
            waveform["data"][key] = np.interp(fnew,f,value,left=0,right=0)
        elif key in ["hphase"]:
            waveform["data"][key] = np.interp(fnew,f,value,left=0,right=factor)

    return waveform

def create_waveform(waveform):

    #print pycbc.waveform.td_approximants()
    td_approximants = ['SEOBNRv1', 'SpinTaylorT4', 'TaylorET', 'IMRPhenomB', 'EOBNRv2HM', 'TaylorT1', 'TaylorT3', 'TaylorT2', 'EOBNRv2', 'TaylorT4', 'IMRPhenomA', 'PhenSpinTaylorRD', 'SEOBNRv3','IMRPhenomPv2']
    #print pycbc.waveform.fd_approximants()
    fd_approximants = ['IMRPhenomA', 'TaylorF2', 'TaylorF2RedSpinTidal', 'IMRPhenomB','IMRPhenomBspin','TaylorF2RedSpin']
    # NINJA2 approximants
    ninja2_approximants = ['BAM-D11spp50_96', 'GATech-MayaKranc_D12_a0.20_m103_nj', 'Llama-d5q2', 'SpEC-EqualMassAntiAlignedSpins', 'BAM-um3_88', 'GATech-MayaKranc_D10_a0.20_m77_nj', 'Llama-d4d4-q1--D10-h64-r250', 'LEAN-dq4', 'BAM-D10spp75_80', 'BAM-D125smm50Nep_80', 'BAM-R6_PN_80', 'BAM-D12spp25_96', 'FAU-hybrid_q3S0.4S0.6aligned_long', 'UIUC-q1_spin_0.85_om0.0536_22', 'GATech-MayaKranc_D10_a0.40_m90_nj', 'SpEC-EqualMassAlignedSpins', 'GATech-MayaKranc_Sp02py0935th90_gr', 'GATech-MayaKranc_D12_a0.00_q2_m90_nj', 'BAM-q2a0a025_T_96_344', 'GATech-MayaKranc_D12_a0.80_m103_nj', 'BAM-um2_88', 'GATech-MayaKranc_D11_a0.20_q2_m90_nj', 'GATech-MayaKranc_D10_a0.80_m90_nj', 'SpEC-EqualMassNonspinning', 'BAM-D13smm85Nep_88', 'GATech-MayaKranc_D12_a0.60_m103_nj', 'GATech-MayaKranc_D12_a0.40_m103_nj', 'SpEC-q1s0.96950', 'GATech-MayaKranc_D10_a0.60_m77_nj', 'BAM-D10spp85_80', 'Llama-d550', 'SpEC-q1s-0.95', 'SpEC-q4s0', 'Llama-u4u4-q1--D8-h64-r250', 'GATech-MayaKranc_D10_a0.90_m129_nj', 'BAM-D13smm75Nep_96', 'SpEC-q3s0', 'SpEC-q6s0', 'RIT-10to1', 'BAM-D12smm25Nep_80', 'GATech-MayaKranc_D12_a0.00_m129_nj', 'SpEC-q2s0', 'BAM-EP_um4_D10-n96', 'Llama-d2d2-q1--D10-h64-r250']

    if waveform["args"]["approximant"] in ["EOBNRv2"]:
        waveform["args"]["spin1z"] = 0.0
        waveform["args"]["spin2z"] = 0.0

    if waveform["args"]["approximant"] in fd_approximants:
        waveform = create_fd_waveform(waveform)
    elif waveform["args"]["approximant"] in td_approximants:
        waveform = create_td_waveform(waveform)
    elif waveform["args"]["approximant"] in ninja2_approximants:
        waveform = create_ninja2_waveform(waveform)
    elif waveform["args"]["approximant"] == "NR":
        waveform = create_textfile_waveform(waveform)
    else:
        print "Waveform %s not supported"%waveform["args"]["approximant"]

    return waveform

def generate_fplus_fcross(latitude,longitude,polarization):
    f_plus = - (1.0/2.0) * (1.0 + np.cos(latitude)*np.cos(latitude)) * np.cos (2.0 * longitude) * np.cos (2.0 * polarization) - np.cos(latitude) * np.sin(2.0*longitude) * np.sin (2.0 * polarization)
    f_cross=  (1.0/2.0) * (1.0 + np.cos(latitude)*np.cos(latitude)) * np.cos (2.0 * longitude) * np.sin (2.0* polarization) - np.cos (latitude) * np.sin(2.0*longitude) * np.cos (2.0 * polarization)
    return f_plus, f_cross

def generate_detector_strain_td(waveform, h_plus, h_cross):

    f_plus, f_cross = generate_fplus_fcross(waveform["args"]["latitude"], waveform["args"]["longitude"], waveform["args"]["polarization"])

    return (f_plus*h_plus+f_cross*h_cross)

def generate_detector_strain_fd(waveform, hplustilde, hcrosstilde):

    f_plus, f_cross = generate_fplus_fcross(waveform["args"]["latitude"], waveform["args"]["longitude"], waveform["args"]["polarization"])

    return (f_plus*hplustilde+f_cross*hcrosstilde)

def create_textfile_waveform(waveform):

    data = np.loadtxt(waveform["waveformFile"])
    f = data[:,0]
    hplustilde = data[:,1]
    hcrosstilde = data[:,2]

    delta_f = f[1] - f[0]

    fmax_new = 16384
    fmax = max(f)

    fnew = np.arange(waveform["args"]["delta_f"],fmax_new,waveform["args"]["delta_f"])

    htilde = np.interp(fnew,f,hplustilde,left=0,right=0) + 1j* np.interp(fnew,f,hcrosstilde,left=0,right=0)
    htilde = pycbc.types.FrequencySeries(htilde, delta_f=waveform["args"]["delta_f"], dtype=pycbc.types.complex128)

    waveform = create_data(waveform,htilde)

    return waveform

def create_fd_waveform(waveform):

    if waveform["args"]["approximant"] == "IMRPhenomBspin":
        hplustilde, hcrosstilde = pycbc.waveform.get_fd_waveform(approximant="IMRPhenomB", mass1=waveform["args"]["mass1"], mass2=waveform["args"]["mass2"], f_lower=waveform["args"]["f_lower"], delta_f=waveform["args"]["delta_f"], distance=waveform["args"]["distance"], phi0=waveform["args"]["phi0"], spin1z=waveform["args"]["spin1z"], spin2z=waveform["args"]["spin2z"], spin1x=waveform["args"]["spin1x"], spin2x=waveform["args"]["spin2x"], inclination=waveform["args"]["inclination"])
    else:
        hplustilde, hcrosstilde = pycbc.waveform.get_fd_waveform(approximant=waveform["args"]["approximant"], mass1=waveform["args"]["mass1"], mass2=waveform["args"]["mass2"], f_lower=waveform["args"]["f_lower"], delta_f=waveform["args"]["delta_f"], distance=waveform["args"]["distance"], phi0=waveform["args"]["phi0"], spin1z=waveform["args"]["spin1z"], spin2z=waveform["args"]["spin2z"], spin1x=waveform["args"]["spin1x"], spin2x=waveform["args"]["spin2x"], spin1y=waveform["args"]["spin1y"], spin2y=waveform["args"]["spin2y"], inclination=waveform["args"]["inclination"])

    htilde = generate_detector_strain_fd(waveform, hplustilde, hcrosstilde)
    htilde = pycbc.types.FrequencySeries(htilde, delta_f=waveform["args"]["delta_f"], dtype=pycbc.types.complex128)

    waveform = create_data(waveform,htilde)

    return waveform

def create_td_waveform(waveform):

    hplus, hcross = pycbc.waveform.get_td_waveform(approximant=waveform["args"]["approximant"], mass1=waveform["args"]["mass1"], mass2=waveform["args"]["mass2"], f_lower=waveform["args"]["f_lower"], delta_t=waveform["args"]["delta_t"], distance=waveform["args"]["distance"], phi0=waveform["args"]["phi0"], spin1z=waveform["args"]["spin1z"], spin2z=waveform["args"]["spin2z"], spin1x=waveform["args"]["spin1x"], spin2x=waveform["args"]["spin2x"], spin1y=waveform["args"]["spin1y"], spin2y=waveform["args"]["spin2y"], inclination=waveform["args"]["inclination"])

    t = hplus.sample_times
    #hamp = np.sqrt(hplus**2 + hcross**2)
    #hphase = np.arctan(np.divide(hcross,hplus))

    #h = hplus + 1j * hcross
    h = generate_detector_strain_td(waveform,hplus,hcross)

    waveform["timedata"] = {}
    waveform["timedata"]["t"] = np.array(t)
    waveform["timedata"]["hplus"] = np.array(hplus)
    waveform["timedata"]["hcross"] = np.array(hcross) 

    ts = pycbc.types.TimeSeries(h,delta_t=waveform["args"]["delta_t"],dtype=pycbc.types.float64)
    htilde = pycbc.types.FrequencySeries(np.zeros(len(h)/2 + 1), delta_f=1, dtype=pycbc.types.complex128)
    pycbc.fft.fft(ts, htilde)

    waveform = create_data(waveform,htilde)

    return waveform

def create_ninja2_waveform(waveform):

    injxml = '/home/mcoughlin/git-repo/PE/WaveformComparison/ninja2catalog.xml'
    injlocation = '/home/mcoughlin/git-repo/PE/WaveformComparison/NINJA2'

    injTable=SimInspiralUtils.ReadSimInspiralFromFiles([injxml])
    injections = {}
    for inj in injTable:
        numrel_data_split = inj.numrel_data.split("/")
        key = str("%s-%s"%(numrel_data_split[-4],numrel_data_split[-3]))
        frame = '/%s'%("/".join(numrel_data_split[3:]))
        injections[key] = {}
        injections[key]["key"] = key
        injections[key]["frame"] = frame
        for parameter in dir(inj):
            injections[key][parameter] = inj.__getattribute__(parameter)

    waveform["injection"] = injections[waveform["args"]["approximant"]]

    # Channels
    # channels = ['hcross_l2_mp2', 'hplus_l2_mp2', 'hcross_l2_mn2', 'hplus_l2_mn2']
    channels = ['hplus_l2_mp2','hcross_l2_mp2']

    data = {}
    for channel in channels:

        print "Loading data for %s"%(channel)
        injfile = "%s/%s/%s-001.dat"%(injlocation,waveform["args"]["approximant"],channel)

        dataOut = np.loadtxt(injfile)
        data[channel] = {}
        data[channel]["t"] = dataOut[:,0]
        data[channel]["h"] = dataOut[:,1]

    t = data["hplus_l2_mp2"]["t"]
    hplus = data["hplus_l2_mp2"]["h"]
    hcross = data["hcross_l2_mp2"]["h"]

    h = generate_detector_strain_td(waveform,hplus,hcross)

    # Scale time spacing
    t = t * (waveform["args"]["mass1"] + waveform["args"]["mass2"])
    delta_t = t[1] - t[0]

    ts = pycbc.types.TimeSeries(h,delta_t=delta_t,dtype=pycbc.types.float64)
    htilde = pycbc.types.FrequencySeries(np.zeros(len(h)/2 + 1), delta_f=1, dtype=pycbc.types.complex128)
    pycbc.fft.fft(ts, htilde)

    waveform = create_data(waveform,htilde)

    return waveform

def compute_overlap(htilde, stilde, psd):

    d_h = matchedfilter.overlap(htilde, stilde, psd=psd, low_frequency_cutoff=None,\
        high_frequency_cutoff=None, normalized=False)

    return d_h

def compute_fisherinv(derivatives):

    parameters = []
    for key, value in derivatives["waveforms"].items():
        parameters.append(key)

    s = (len(parameters),len(parameters))
    fisher = np.zeros(s)

    for i in xrange(len(parameters)):
        for j in xrange(len(parameters)):
            derivative1 = derivatives["first"][parameters[i]]
            derivative2 = derivatives["first"][parameters[j]]

            delta_f = derivative1["f"][1] - derivative1["f"][0]
            htilde = pycbc.types.FrequencySeries(derivative1["total"], delta_f=delta_f, dtype=pycbc.types.complex128)
            stilde = pycbc.types.FrequencySeries(derivative2["total"], delta_f=delta_f, dtype=pycbc.types.complex128)
            psd = generate_psd(htilde.sample_frequencies)

            d_h = compute_overlap(htilde, stilde, psd)

            fisher[i][j] = d_h

    fisherinv = np.linalg.inv(fisher)

    return fisherinv

def generate_psd(f):

    z = np.loadtxt("/home/mcoughlin/git-repo/PE/WaveformComparison/noiseModels/ZERO_DET_high_P_PSD.dat")
    z_f = z[:,0]
    z_amp = z[:,1]

    z_interp = np.interp(f,z_f,z_amp,right=0,left=0)

    delta_f = f[1] - f[0]
    psd = pycbc.types.FrequencySeries(z_interp, delta_f=delta_f, dtype=pycbc.types.complex128)

    return psd

def compute_waveform_distance(waveforms,derivatives,fisherinv,plotLocation):

    deltaA = waveforms["waveform"]["data"]["hamp"] - waveforms["approximant"]["data"]["hamp"]
    deltaPhi = waveforms["waveform"]["data"]["hphase"] - waveforms["approximant"]["data"]["hphase"]

    # (dA + A*i*dp) e^(i*p)

    difference = (deltaA + 1j * waveforms["approximant"]["data"]["hamp"] * deltaPhi) * np.exp(1j * waveforms["approximant"]["data"]["hphase"])

    difference = np.absolute(difference)

    #difference = deltaA  * np.exp(1j* deltaPhi)

    parameters = []
    for key, value in derivatives["waveforms"].items():
        parameters.append(key)

    s = (len(parameters),1)
    first_order = np.zeros(s)
    # compute first order
    for i in xrange(len(parameters)):

        delta_f =  waveforms["waveform"]["args"]["delta_f"]
        htilde = pycbc.types.FrequencySeries(difference, delta_f=delta_f, dtype=pycbc.types.complex128)
        stilde = pycbc.types.FrequencySeries(derivatives["first"][parameters[i]]["total"], delta_f=delta_f, dtype=pycbc.types.complex128)

        psd = generate_psd(htilde.sample_frequencies)
        d_h = compute_overlap(htilde, stilde, psd)
        first_order[i] = d_h

        plotName = os.path.join(plotLocation,"first_order_%s.png"%parameters[i])
        pe_waveform_comparison_plot.plot_overlap(htilde.sample_frequencies,difference,derivatives["first"][parameters[i]]["total"],psd,plotName)

    second_order = np.zeros(s)
    # compute second order
    for i in xrange(len(parameters)):

        delta_f =  waveforms["waveform"]["args"]["delta_f"]
        htilde = pycbc.types.FrequencySeries(difference, delta_f=delta_f, dtype=pycbc.types.complex128)
        stilde = pycbc.types.FrequencySeries(derivatives["second"][parameters[i]]["total"], delta_f=delta_f, dtype=pycbc.types.complex128)
        psd = generate_psd(htilde.sample_frequencies)
        d_h = compute_overlap(htilde, stilde, psd)
        second_order[i] = d_h * first_order[i] * fisherinv[i][i]

        htilde = pycbc.types.FrequencySeries(derivatives["second"][parameters[i]]["total"], delta_f=delta_f, dtype=pycbc.types.complex128)
        stilde = pycbc.types.FrequencySeries(derivatives["first"][parameters[i]]["total"], delta_f=delta_f, dtype=pycbc.types.complex128)
        psd = generate_psd(htilde.sample_frequencies)
        d_h = compute_overlap(htilde, stilde, psd)
        second_order[i] = second_order[i] - d_h * (first_order[i] * fisherinv[i][i])**2

        htilde = pycbc.types.FrequencySeries(derivatives["first"][parameters[i]]["total"], delta_f=delta_f, dtype=pycbc.types.complex128)
        stilde = pycbc.types.FrequencySeries(derivatives["second"][parameters[i]]["total"], delta_f=delta_f, dtype=pycbc.types.complex128)
        psd = generate_psd(htilde.sample_frequencies)
        d_h = compute_overlap(htilde, stilde, psd)
        second_order[i] = second_order[i] - (1/2) * d_h * (first_order[i] * fisherinv[i][i])**2

        plotName = os.path.join(plotLocation,"second_order_%s.png"%parameters[i])
        pe_waveform_comparison_plot.plot_overlap(htilde.sample_frequencies,difference,derivatives["second"][parameters[i]]["total"],psd,plotName)
 
    #pe_waveform_comparison_plot.plot_distance(derivatives[approximant]["mass1"]["waveforms"]["zero"]["data"]["f"],difference)

    return first_order, second_order

def compute_waveform_distance_error(waveforms,derivatives,first_errors,second_errors):

    # < h_m(theta) - h_m(theta_0) - \partial h_m/\partial theta_i \Delta \theta_i | h_m(theta) - h_m(theta_0) - \partial h_m/\partial theta_i \Delta \theta_i>

    delta_f = waveforms["approximant"]["args"]["delta_f"]
    #h_m_theta = waveforms["approximant_errors"]["data"]["hamp"] * np.exp(1j * waveforms["approximant_errors"]["data"]["hphase"])
    #h_m_theta0 = waveforms["approximant"]["data"]["hamp"] * np.exp(1j * waveforms["approximant"]["data"]["hphase"])
    h_m_theta = waveforms["approximant"]["data"]["hamp"] * np.exp(1j * waveforms["approximant"]["data"]["hphase"])  
    h_m_theta0 = waveforms["waveform"]["data"]["hamp"] * np.exp(1j * waveforms["waveform"]["data"]["hphase"])


    pe_waveform_comparison_plot.plot_waveform_distance_error(waveforms)
    vector = h_m_theta0 - h_m_theta

    htilde = pycbc.types.FrequencySeries(vector, delta_f=delta_f, dtype=pycbc.types.complex128)
    psd = generate_psd(htilde.sample_frequencies)
    d_h = compute_overlap(htilde, htilde, psd)

    htilde = pycbc.types.FrequencySeries(h_m_theta0, delta_f=delta_f, dtype=pycbc.types.complex128)
    norm = compute_overlap(htilde, htilde, psd)

    error_0 = d_h / norm

    parameters = []
    for key, value in derivatives["waveforms"].items():
        parameters.append(key)

    print np.sum(vector)

    for i in xrange(len(parameters)):
        print np.sum(derivatives["first"][parameters[i]]["total"] * derivatives["first"][parameters[i]]["delta"])
        #vector = vector - derivatives["first"][parameters[i]]["total"] * first_errors[i] 
        vector = vector - derivatives["first"][parameters[i]]["total"] * derivatives["first"][parameters[i]]["delta"]

    htilde = pycbc.types.FrequencySeries(vector, delta_f=delta_f, dtype=pycbc.types.complex128)
    #pe_waveform_comparison_plot.plot_waveform_distance_error_psd(waveforms,htilde,psd)
    d_h = compute_overlap(htilde, htilde, psd)

    htilde = pycbc.types.FrequencySeries(h_m_theta0, delta_f=delta_f, dtype=pycbc.types.complex128)
    norm = compute_overlap(htilde, htilde, psd)

    error_1 = d_h / norm

    for i in xrange(len(parameters)):
        print derivatives["second"][parameters[i]]["delta"]
        print np.sum(derivatives["second"][parameters[i]]["total"] * derivatives["second"][parameters[i]]["delta"])
        vector = vector - derivatives["second"][parameters[i]]["total"] * derivatives["second"][parameters[i]]["delta"]

    htilde = pycbc.types.FrequencySeries(vector, delta_f=delta_f, dtype=pycbc.types.complex128)
    #pe_waveform_comparison_plot.plot_waveform_distance_error_psd(waveforms,htilde,psd)
    d_h = compute_overlap(htilde, htilde, psd)

    htilde = pycbc.types.FrequencySeries(h_m_theta0, delta_f=delta_f, dtype=pycbc.types.complex128)
    norm = compute_overlap(htilde, htilde, psd)

    error_2 = d_h / norm

    #pe_waveform_comparison_plot.plot_distance(derivatives[approximant]["mass1"]["waveforms"]["zero"]["data"]["f"],difference)

    return error_0, error_1, error_2

def compute_derivative_first(waveform):

    derivative_dic = {}
    derivative_dic["f"] = waveform["zero"]["data"]["f"]
    derivative_dic["delta"] = waveform["delta"]
    derivative_dic["hamp"] = (waveform["pos"]["data"]["hamp"] - waveform["neg"]["data"]["hamp"]) / derivative_dic["delta"]

    derivative_dic["hamp"] = np.absolute(derivative_dic["hamp"])

    derivative_dic["hphase"] = (waveform["pos"]["data"]["hphase"] - waveform["neg"]["data"]["hphase"]) / derivative_dic["delta"]

    derivative_dic["hphase"] = np.absolute(derivative_dic["hphase"])

    # (dA + A*i*dp) e^(i*p)
    derivative_dic["total"] = (derivative_dic["hamp"] + waveform["zero"]["data"]["hamp"]*1j*derivative_dic["hphase"]) * np.exp(1j*waveform["zero"]["data"]["hphase"])

    derivative_dic["total"] = np.absolute(derivative_dic["total"])

    #derivative_dic["total"] = derivative_dic["hamp"] * np.exp(1j*waveform["zero"]["data"]["hphase"])

    delta_f = waveform["zero"]["args"]["delta_f"]
    htilde = pycbc.types.FrequencySeries(derivative_dic["total"], delta_f=delta_f, dtype=pycbc.types.complex128)
    ts = pycbc.types.TimeSeries(np.zeros(2*len(htilde)-1),delta_t=1,dtype=pycbc.types.float64)
    pycbc.fft.ifft(htilde,ts)

    derivative_dic["t"] = ts.sample_times
    derivative_dic["total_ifft"] = ts

    return derivative_dic

def compute_derivative_second(waveform,derivative):

    derivative_dic = {}
    derivative_dic["f"] = waveform["zero"]["data"]["f"]
    derivative_dic["delta"] = np.power(waveform["delta"],2)
    derivative_dic["hamp"] = (waveform["pos"]["data"]["hamp"] + waveform["neg"]["data"]["hamp"] - 2*waveform["zero"]["data"]["hamp"])/derivative_dic["delta"]

    derivative_dic["hamp"] = np.absolute(derivative_dic["hamp"])
 
    derivative_dic["hphase"] = (waveform["pos"]["data"]["hphase"] + waveform["neg"]["data"]["hphase"] - 2*waveform["zero"]["data"]["hphase"])/derivative_dic["delta"]
    # (d^2 A + 2*i*dA*dp + A * i * d^2 p - A * (dp^2)) * e^(i*p)

    derivative_dic["hphase"] = np.absolute(derivative_dic["hphase"])

    derivative_dic["total"] = (derivative_dic["hamp"] + 2*1j*derivative["hamp"]*derivative["hphase"] + waveform["zero"]["data"]["hamp"] * 1j * derivative_dic["hphase"] - waveform["zero"]["data"]["hamp"] * np.power(derivative["hphase"],2)) * np.exp(1j*waveform["zero"]["data"]["hphase"])

    derivative_dic["total"] = np.absolute(derivative_dic["total"])

    #derivative_dic["total"] = (derivative_dic["hamp"] + waveform["zero"]["data"]["hamp"]*1j*derivative_dic["hphase"]) * np.exp(1j*waveform["zero"]["data"]["hphase"])

    #derivative_dic["total"] = derivative_dic["hamp"] * np.exp(1j*derivative_dic["hphase"])

    delta_f = waveform["zero"]["args"]["delta_f"]
    htilde = pycbc.types.FrequencySeries(derivative_dic["total"], delta_f=delta_f, dtype=pycbc.types.complex128)
    ts = pycbc.types.TimeSeries(np.zeros(2*len(htilde)-1),delta_t=1,dtype=pycbc.types.float64)
    pycbc.fft.ifft(htilde,ts)

    derivative_dic["t"] = ts.sample_times
    derivative_dic["total_ifft"] = ts

    return derivative_dic

def compute_derivative(waveforms,delta):

    derivative = {}
    derivative["waveforms"] = {}
    derivative["waveforms"]["neg"] = waveforms[0]
    derivative["waveforms"]["zero"] = waveforms[1]
    derivative["waveforms"]["pos"] = waveforms[2]

    derivative["derivative"] = {}
    derivative["derivative"]["neg"] = compute_derivative_first(derivative,"neg","neg","zero",delta)
    derivative["derivative"]["zero"] = compute_derivative_first(derivative,"zero","pos","neg",2*delta)
    derivative["derivative"]["pos"] = compute_derivative_first(derivative,"pos","pos","zero",delta)

    derivative["derivative_second"] = {}
    derivative["derivative_second"]["zero"] = compute_derivative_second(derivative,"zero","pos","neg",2*delta)

    return derivative

def waveform_struct(approximant):

    waveform = {}
    waveform["args"] = {'spin1x':0,'spin1y':0,'spin1z':0,
        'spin2x':0,'spin2y':0,'spin2z':0,'lambda1':0, 'lambda2':0,
        'inclination':0,'distance':10e7,'fmax':0,'phi0':0,
        'amplitude_order':-1,'phase_order':-1,
        'mass1':10,'mass2':5,'f_lower':15,'approximant':approximant,
        'latitude':0,'longitude':0,'polarization':0,
        'delta_f':1/4.0,'delta_t':1/256.0}
        #'delta_f':1/4.0,'delta_t':1/2048.0}


    return waveform

