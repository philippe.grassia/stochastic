#!/usr/bin/python
# usage: always run with (-o outputfile) option
# Eric Thrane: udpated on May 9, 2016 to handle non-aligned spin

"""
%prog

Michael Coughlin (michael.coughlin@ligo.org)

This program does waveform comparisons.

"""

import os, sys, pickle, math, optparse
import pycbc.waveform, pycbc.fft, pycbc.types, pycbc.filter
import numpy as np
import cbc_generate_waveform_utils, cbc_generate_waveform_plot

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser()

    parser.add_option("-w", "--waveform", help="Waveform",default="SpinTaylorT4")
    parser.add_option("-m", "--mass1", help="Mass 1",type="float",default=1.4)
    parser.add_option("-n", "--mass2", help="Mass 2",type="float",default=1.4)
    parser.add_option("-s", "--spin1z", help="Spin 1",type="float",default=0.0)
    parser.add_option("-t", "--spin2z", help="Spin 2",type="float",default=0.0)
#
    parser.add_option("-S", "--spin1x", help="Spin 1 x",type="float",default=0.0)
    parser.add_option("-T", "--spin2x", help="Spin 2 x",type="float",default=0.0)
    parser.add_option("-U", "--spin1y", help="Spin 1 y",type="float",default=0.0)
    parser.add_option("-V", "--spin2y", help="Spin 2 y",type="float",default=0.0)
#
    parser.add_option("-i", "--inclination", help="Inclination",type="float",default=0.0)
    parser.add_option("-d", "--distance", help="Distance",type="float",default=1.0)
    parser.add_option("-p", "--polarization", help="polarization",type="float")
    parser.add_option("-f", "--f_lower", help="frequency",type="float")
    parser.add_option("-o", "--output", help="output file")

    parser.add_option("-v", "--verbose", action="store_true", default=False,
                      help="Run verbosely. (Default: False)")

    opts, args = parser.parse_args()

    return opts

def file_name(waveform,parameters):
    params = ["mass1","mass2"]
    string = ""
    for key in parameters.iterkeys():
        if string == "":
            string = "_".join([str(key),str(parameters[key]["value"])])
        else:
            string = "_".join([string,str(key),str(parameters[key]["value"])])
    return string

# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    #parameters = ["phi0","delta_t","mass1","mass2","spin1x","spin1y","spin1z","spin2x","spin2y","spin2z","f_lower","distance","inclination","lambda1","lambda2","amplitude_order","phase_order","approximant"]

    opts = parse_commandline()

    parameters = {}
    for opt, value in opts.__dict__.items():
        if not value == None and not opt in ["verbose","waveform","waveformFile","errorParams","approximant","mass1Delta","mass2Delta","event","output"]:
            parameters[opt] = {}
            parameters[opt]["value"] = float(value)

    print "Calculating waveform : ", opts.waveform
    # Generate waveform
    waveform = cbc_generate_waveform_utils.waveform_struct(opts.waveform)
    for key in parameters.iterkeys():
        waveform["args"][key] = parameters[key]["value"]
    waveform = cbc_generate_waveform_utils.create_waveform(waveform)

    fileName = file_name(opts.waveform,parameters)

    f = open(opts.output,"w+")
    for i in xrange(len(waveform["timedata"]["t"])):
        f.write("%.15e %.15e %.15e\n"%(waveform["timedata"]["t"][i],waveform["timedata"]["hplus"][i],waveform["timedata"]["hcross"][i]))
    f.close()

    #cbc_generate_waveform_plot.plot_waveform(waveform,outputDir)

