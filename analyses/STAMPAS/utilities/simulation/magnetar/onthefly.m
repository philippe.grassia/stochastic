dist = 0;

eB = 0.001;
f0 = 1700;
duration = 3500;
[duration,fmin,fmax,hrss] = genmagnetar(eB,f0,duration);
save('magnetarA.mat');

eB = 0.01;
f0 = 1700;
duration = 3500;
[duration,fmin,fmax,hrss] = genmagnetar(eB,f0,duration);
save('magnetarB.mat');

eB = 0.1;
f0 = 1700;
duration = 3500;
[duration,fmin,fmax,hrss] = genmagnetar(eB,f0,duration);
save('magnetarC.mat');


