function conditioning(name)

x=load(['pt_' name '.dat']);

fs = 1/(x(2,1) - x(1,1));
dt = 1/fs;

taper = linspace(0,1-dt,fs);
N = length(taper);
mask = ones(size(x,1),1);
mask(1:N,1)=sin(pi/2.*taper).^2;
mask(end-N+1:end,1) = flipud(mask(1:N,1));

clear taper
y=x;
y(:,2) = x(:,2).* mask;
y(:,3) = x(:,3).* mask;

dlmwrite(['PT_' name '_tapered.dat'],y,'delimiter','\t','precision','%.8g');
clear mask


% Diagnostic
figure(1)
x_end=20000;
plot(x(1:x_end,1),x(1:x_end,2))
hold on
plot(y(1:x_end,1),y(1:x_end,2),'r')
hold off


xlabel ('time [s]');
ylabel ('strain amplitude');
legend('original', 'tapered')

figure(2)
ext_end=size(x,1)-x_end:size(x,1);
plot(x(ext_end,1), x(ext_end,2));
hold on
plot(y(ext_end,1), y(ext_end,2),'r');
hold off

xlabel ('time [s]');
ylabel ('strain amplitude');
legend('original', 'tapered')

% hrss calculation

hrss_o=sqrt(sum(x(:,2).^2+x(:,3).^2)*dt);
hrss=sqrt(sum(y(:,2).^2+y(:,3).^2)*dt);

display(['hrss original: ' num2str(hrss_o)]);
display(['hrss: ' num2str(hrss)]);
clear x;
% Frequency domain studies
fft_size=2^nextpow2(size(y,1));
hptilde=1./fs*fft(y(:,2),fft_size);
hxtilde=1./fs*fft(y(:,3),fft_size);


clear y;

figure(3)
f = fs*linspace(0,1,fft_size/2+1);
f=f';
loglog(f(1:fft_size/2+1,1), abs(hptilde(1:fft_size/2+1,1)))
hold on
loglog(f(1:fft_size/2+1,1), abs(hxtilde(1:fft_size/2+1,1)),'r')
hold off
grid
legend('hp', 'hx')
if strcmp(name,'A')==1
  xlim([2000 3500])
else
  xlim([1 fs/2])
end

