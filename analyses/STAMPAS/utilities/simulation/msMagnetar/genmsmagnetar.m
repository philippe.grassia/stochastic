function [duration,fmin,fmax,hrss] = genmsmagnetar(f0,tau,nn,epsilon);

fs = 4096;
duration = 10000;

T0 = 0;
tstart = 0;
tend = duration;

[hp, hc, t, freq] = msmagnetar(f0,tau,nn,epsilon,duration,fs,T0,tstart,tend);

fmin = nanmin(freq);
fmax = nanmax(freq);
dt = t(2) - t(1);
hrss =sqrt(sum(hp.^2+hc.^2)*dt);

fprintf('%.5f %.5f %.5e\n',duration,fmin,hrss);

