
### Paul Lasky
# paul.lasky@monash.edu

# simple code that uses msMagnetarWaveform to create waveform data files for use in
# searches for finding millisecond magnetar remnants

import numpy as np
import msMagnetarWaveform as WF

import pandas as pd

# set up parameters:
f0 = [1000, 2000]          # starting GW frequency [Hz]
tau = [10**2, 10**3, 10**4]      # spindown timescale [s]
nn = [2.5, 3.0, 5.]              # braking index
epsilon = [10**-2, 10**-3]       # stellar ellipticity

dd = 40.                         # distance [Mpc]
iota = 0.

fs = 8192.   # sampling frequency
#fs = 4096.
#fs = 1.

tmin = 0.
tmax = 1e4

time = np.arange(tmin, tmax, 1./fs)

out_dir = 'waveform_files/Fs4096/'

def waveform_and_save(time, f0, tau, nn, eps):
    ## calculate plus and cross polarisations
    hp, hx = WF.ht(time, f0, tau, nn, eps, dd, iota)

    ## make output file name
    out_file = 'msMagnetar_f0'+str(f0)+'_tau'+'{:04d}'.format(tau)+'_n'+str(nn)+'_eps'+'{:4f}'.format(eps)

    ## save file
    #save as an ascii text file (too big...)
    np.savetxt(out_dir+out_file+'.dat', np.c_[time, hp, hx])

    #save as an hdf5 file
    df = pd.DataFrame({'time': time,
                      'hp': hp,
                      'hx': hx})
    
    df.to_hdf(out_dir+out_file+'.h5', 'df')

# loop over all waveform parameters
for ii in np.arange(len(f0)):
    for jj in np.arange(len(tau)):
        for kk in np.arange(len(nn)):
            for ll in np.arange(len(epsilon)):
                waveform_and_save(time, f0[ii], tau[jj], nn[kk], epsilon[ll])

