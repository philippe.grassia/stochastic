
%f0s = [1000, 2000];          % starting GW frequency [Hz]
%taus = [10.^2, 10.^3, 10.^4];      % spindown timescale [s]
%nns = [2.5, 3.0, 5.];              % braking index
%epsilons = [10.^-2, 10.^-3];       % stellar ellipticity

f0s = [1000, 2000];          % starting GW frequency [Hz]
taus = [10.^2, 10.^4];      % spindown timescale [s]
nns = [2.5, 5.];              % braking index
epsilons = [10.^-2, 10.^-3];       % stellar ellipticity

Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
cnt = 1
for ii = 1:length(f0s)
   f0 = f0s(ii);
   for jj = 1:length(taus)
      tau = taus(jj);
      for kk = 1:length(nns)
         nn = nns(kk);
         for ll = 1:length(epsilons)
             epsilon = epsilons(ll); 
             [duration,fmin,fmsax,hrss] = genmsmagnetar(f0,tau,nn,epsilon);
             filename = sprintf('msmagnetar%s.mat',Alphabet(cnt));
             save(filename);
             cnt = cnt + 1; 
         end
      end
   end
end
