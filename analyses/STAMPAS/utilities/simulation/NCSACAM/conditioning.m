function conditioning(name,outname)

x=load(['waveforms/' name]);

fs = 1/(x(2,1) - x(1,1));
dt = 1/fs;

fsnew = 4096;
r = fs/fsnew;
hp_resamp = decimate(x(:,2),r);
hc_resamp = decimate(x(:,3),r);
tt_resamp = x(1:r:end,1);

x = [tt_resamp hp_resamp hc_resamp];
fs = 1/(x(2,1) - x(1,1));
dt = 1/fs;

taper = linspace(0,1-dt,fs);
N = length(taper);
mask = ones(size(x,1),1);
mask(1:N,1)=sin(pi/2.*taper).^2;
mask(end-N+1:end,1) = flipud(mask(1:N,1));

clear taper
y=x;
y(:,2) = x(:,2).* mask;
y(:,3) = x(:,3).* mask;

dlmwrite([outname '_tapered.dat'],y,'delimiter','\t','precision','%.8g');

