

files = dir(['waveforms/*BNS*']);
outnames = {'NCSACAM_A','NCSACAM_B','NCSACAM_C','NCSACAM_D','NCSACAM_E','NCSACAM_F'};

fid = fopen('mapping.txt','w+')
for ii = 1:length(files)
    name = files(ii).name;
    %conditioning(name,outnames{ii})
    fprintf(fid,'%s %s\n',name,outnames{ii});
end

filenames = {'f_10Hz_m1-5.0000000e+00_m2-5.0000000e+00_ecc-2.0000000e-01','f_10Hz_m1-5.0000000e+00_m2-5.0000000e+00_ecc-4.0000000e-01','f_10Hz_m1-5.0000000e+00_m2-5.0000000e+00_ecc-6.0000000e-01'};
outnames = {'NCSACAM_G','NCSACAM_H','NCSACAM_I'};
for ii = 1:length(filenames)
    name = filenames{ii};
    conditioning(name,outnames{ii})
    fprintf(fid,'%s %s\n',name,outnames{ii});
end

fclose(fid);

