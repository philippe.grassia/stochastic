

mass1 = 3.0;
mass2 = 3.0;
iota = 0;
dist = 1;
[duration,fmin,fmax,hrss] = geninspiral(mass1,mass2,iota,dist);

save('inspiralA.mat');

mass1 = 1.4;
mass2 = 1.4;
iota = 0;
dist = 1;
[duration,fmin,fmax,hrss] = geninspiral(mass1,mass2,iota,dist);
save('inspiralB.mat');

mass1 = 1.25;
mass2 = 1.25;
iota = 0;
dist = 1;
[duration,fmin,fmax,hrss] = geninspiral(mass1,mass2,iota,dist);
save('inspiralC.mat');

mass1 = 1.0;
mass2 = 1.0;
iota = 0;
dist = 1;
[duration,fmin,fmax,hrss] = geninspiral(mass1,mass2,iota,dist);
save('inspiralD.mat');

mass1 = 0.75;
mass2 = 0.75;
iota = 0;
dist = 1;
[duration,fmin,fmax,hrss] = geninspiral(mass1,mass2,iota,dist);
save('inspiralE.mat');

mass1 = 0.5;
mass2 = 0.5;
iota = 0;
dist = 1;
[duration,fmin,fmax,hrss] = geninspiral(mass1,mass2,iota,dist);
save('inspiralF.mat');

mass1 = 0.25;
mass2 = 0.25;
iota = 0;
dist = 1;
[duration,fmin,fmax,hrss] = geninspiral(mass1,mass2,iota,dist);
save('inspiralG.mat');

mass1 = 0.1;
mass2 = 0.1;
iota = 0;
dist = 1;
[duration,fmin,fmax,hrss] = geninspiral(mass1,mass2,iota,dist);
save('inspiralH.mat');

