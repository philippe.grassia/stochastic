function conditioning(name,out)

theta = 0;

x=load(name);

fs = 4096;
t = 0:1/fs:max(x(:,1));

[junk,idx] = unique(x(:,1));
x = x(idx,:);
h = interp1(x(:,1),x(:,3),t);
f = interp1(x(:,1),x(:,2),t);
h(isnan(h)) = 0;
f(isnan(f)) = 0;
dt = 1/fs;
phi = 2*pi*cumsum(f)*dt;

taper = linspace(0,1-dt,fs);
N = length(taper);
mask = ones(length(t),1);
mask(1:N,1)=sin(pi/2.*taper).^2;
mask(end-N+1:end,1) = flipud(mask(1:N,1));

clear taper
y=zeros(length(t),3);
y(:,1) = t;
y(:,2) = 1e-21 * -h .* cos(phi) * ((1+cos(theta)^2)/2) .* mask';
y(:,3) = 1e-21 * -h .* sin(phi) * cos(theta) .* mask';

dlmwrite(out,y,'delimiter','\t','precision','%.8g');
clear mask

