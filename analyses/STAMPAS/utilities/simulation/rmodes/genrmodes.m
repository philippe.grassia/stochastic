function [duration,fmin,fmax,hrss] = genrmodes(alpha,f0,duration);

fmin = 10;
fs = 16384;

T0 = 0;
tstart = 0;
tend = duration;

[hp, hc, t, freq] = rmodes(alpha,f0,duration,fs,T0,tstart,tend);

fmin = nanmin(freq);
fmax = nanmax(freq);
dt = t(2) - t(1);
hrss =sqrt(sum(hp.^2+hc.^2)*dt);

fprintf('%.5f %.5f %.5e\n',duration,fmin,hrss);

