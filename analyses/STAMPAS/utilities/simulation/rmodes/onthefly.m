dist = 0;

alpha = 0.01;
f0 = 1700;
duration = 3500;
[duration,fmin,fmax,hrss] = genrmodes(alpha,f0,duration);
save('rmodesA.mat');

alpha = 0.1;
f0 = 1700;
duration = 3500;
[duration,fmin,fmax,hrss] = genrmodes(alpha,f0,duration);
save('rmodesB.mat');

alpha = 1;
f0 = 1700;
duration = 3500;
[duration,fmin,fmax,hrss] = genrmodes(alpha,f0,duration);
save('rmodesC.mat');


