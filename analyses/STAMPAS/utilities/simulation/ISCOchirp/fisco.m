
function [out1] = fisco(t,y,flag)
%----------------------------------------------------------------------------
%
%   ODEs of suspended accretion 
%
%   References
%   van Putten, M.H.P.M., & Ostriker, E.C., 2001, ApJ, 552, L31 
%   van Putten, M.H.P.M., 2001, PRL, 87, 091101
%   - 2008, ApJ, 681, L91 
%   - 2009, MNRAS, 396, L81
%   - 2012, Prog. Theor. Phys., 127, 331
%   - 2015, ApJ, 810, 7
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%  (c)2008,2010,2012,2014,2016 Maurice H.P.M. van Putten
%	This program is distributed under the GNU General Public Licence
%   <http://www.gnu.org/licenses/>
%----------------------------------------------------------------------------
if nargin < 3 | isempty(flag)
   fC=0.5;  %feedback strength (variance of poloidal magnetic field)
   fz=1.0;  %R=R_{ISCO}
   M=y(1);J=y(2);a=J/M^2;
   Z1=1+(1-a.^2).^(1/3).*((1+a).^(1/3)+(1-a).^(1/3));Z2=(3*a.^2+Z1.^2).^0.5;
   z=3+Z2-((3-Z1).*(3+Z1+2*Z2)).^0.5;z=z*fz;E=sqrt(1-2/3/z);C=fC*E;
   lambda=asin(a);OmegaH=tan(lambda/2)/2/M;OmegaT=1/M/(z^(3/2)+a);
   C=C*(M*z*OmegaT)^2*3;eta=OmegaT/OmegaH;
   out1 = [-C*(1-eta)*eta*OmegaH^2;-C*(1-eta)*OmegaH];
else
  switch(flag)
  case 'init'              
    out1 = [0; 12];
  otherwise
    error(['Unknown flag ''' flag '''.']);
  end
end