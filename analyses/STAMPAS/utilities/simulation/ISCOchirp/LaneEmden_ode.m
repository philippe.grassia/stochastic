%-------------------------------------------------------
%    
%   (M(r),J(r)) in a Lane-Emden approximation of rho(r)
%    
%   Used by ode23 in LandeEmden.m
%-------------------------------------------------------

function [out1,out2,out3,out4]=mass_ode(t,y,flag,n)

if nargin < 3 | isempty(flag)
   
  out1=[y(2)/t^2; -t^2*y(1)^n;4*pi*t^2*y(1)^n;4*pi*t^4*y(1)^n];  
  
else
  switch(flag)
  case 'init'               % Used only if TSPAN or Y0 is empty.
    % Return default TSPAN, Y0, and OPTIONS.
    out1 = [0; 1];
    out2 = [0; 1; 1];
    out3 = [0; 1; 1];
    out4 = [0; 1; 1];
    
  otherwise
    error(['Unknown flag ''' flag '''.']);
  end
end