%----------------------------------------------------------------------------
%  
%	======================================================================
%	Authors:	Maurice H.P.M. van Putten
%	======================================================================
%	Code name:                  ISCOchirp.m
%	Language:                   MatLab
%	Code tested by compilers/OS:n/a
%	Description of input data:	none
%	Description of output data: figures (eps,jpg) and ascii output
%	System requirements:		none
%	Calls to external routines: none
%	Dependencies:				none
%	Additional comments:		output originally published in ApJ, 681,L91
%	=======================================================================
%	The AAS gives permission to anyone who wishes to use these subroutines
%	to run their own calculations. Permission to republish or reuse these
%	routines should be directed to permissions@aas.org.
%	Note that the AAS does not take responsibility for the content of the
%	source code. Potential users should be wary of applying the code to
%	conditions that the code was not written to model and the accuracy of
%	the code may be affected when compiled and executed on different systems.
%	========================================================================
%
%	DESCRIPTION AND BACKGROUND
%
%  This MatLab program calculates a light curve of a long duration 
%  gravitational wave chirp from quadrupole ISCO waves [1,2] powered by 
%  rotating black holes described by the Kerr metric [3,4]. 
%    Matter at the ISCO receives fedback frm the black hole (at maximal 
%  horizon flux in its lowest energy state) by relativistic frame dragging 
%  acting on an inner torus magnetosphere [5]. At sufficient heating thus 
%  provided, it develops non-axisymmetric instabilities and becomes
%  luminous in gravitational radiation [2,6-8]. 
%    Sustained feedback is described by equations of suspended accretion 
%  [1,9], balancing input from the black hole with output in GWs and, to 
%  a lesser degree, in MeV neutrinos and magnetic winds [6]. The GWs are 
%  hereby solved self-consistently following a Hopf bifurcation [8].
%    Derived from spin angular momentum, the GWs shows a descending chirp 
%  in the relaxation of the black hole spacetime to that of a slowly 
%  spinning black hole with associated expanding ISCO. Spin down seen 
%  normalized light curves of the BATSE catalogue [8,10] point to initially
%  near-extremal black holes [11].
%  
%  Selected references: 
%    1. van Putten, M.H.P.M., 2001, Phys. Rev. Lett., 87, 091101;
%    2. van Putten, M.H.P.M., 2002, ApJ, 575, L71; Kerr, R.P., 1963, 
%    Phys. Rev. Lett., 11, 237; 4. Bardeen, J.M., Press, W.H., & 
%    Teukolsky, S.A., 1972, Phys. Rev. D, 178, 347; 5. van Putten, 
%    M.H.P.M., Science, 1999, 284, 115; 6. van Putten, M.H.P.M., & 
%    Levinson, A., 2003, ApJ, 584, 937; 7. van Putten, M.H.P.M., 2008, 
%    ApJ, 684, L91; 8. ibid. 2012, Prog. Theor. Phys., 127, 331;  
%    9. van Putten, M.H.P.M., & Ostriker, E., 2001, ApJ, 552, L31; 
%    10. van Putten, M.H.P.M., & Gupta, A., 2009, 394, 2238; 11. van 
%    Putten, M.H.P.M., 2015, ApJ, 810, 7; See further Cutler, C., & 
%    Thorne, K.S., 2002, arXiv:gr-qc/0204090v1.
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%  (c)2008,2010,2012,2014,2016 Maurice H.P.M. van Putten
%	This program is distributed under the GNU General Public Licence
%   <http://www.gnu.org/licenses/>
%----------------------------------------------------------------------------
clear all;
%SCALING OF MASS AND DURATION
fM=5;        %Initial black hole mass set to 10 MSolar
fC=0.5;fz=1.0;%Scaling of feedback (fz also in fisco.m) and R_Torus=R_ISCO
TTot = 120/fC;

%ISCO FORMULAS
a=0.0001:0.0001:0.9999;la=asin(a);
ZZ1=1+(1-a.^2).^(1/3).*((1+a).^(1/3)+(1-a).^(1/3));ZZ2=(3*a.^2+ZZ1.^2).^0.5;
z1=3+ZZ2-((3-ZZ1).*(3+ZZ1+2*ZZ2)).^0.5;z2=3+ZZ2+((3-ZZ1).*(3+ZZ1+2*ZZ2)).^0.5;
z1=fz*z1;z2=fz*z2;

%TEMPLATES a/M in the range (0.36,1)
figure(40*fM+1)
kkmx=60;
for kk=1:kkmx
   
   %MATLAB ODE45 INTEGRATION (M-INIT=1MSolar) 
   clear T Y;ak=0.359999+0.64*(kk-1)/(kkmx-1);a_k(kk)=ak;a_initial=ak
   inc=[1,ak];eps=1e-12;tol=eps;          
   options = odeset('RelTol',eps,'AbsTol',tol);
   [T,Y]=ode45('fisco',[0 TTot],inc,options);L=length(T);
   t=T;
   %OUTPUT PARAMETERS 
   Egw(kk)=1-Y(length(T),1);M=Y(:,1);J=Y(:,2);aa=J./M.^2;
     Z1=1+(1-aa.^2).^(1/3).*((1+aa).^(1/3)+(1-aa).^(1/3));Z2=(3*aa.^2+Z1.^2).^0.5;
     z=3+Z2-((3-Z1).*(3+Z1+2*Z2)).^0.5;E=sqrt(1-2/3./z);C1=fC*E;                    
     lambda=asin(aa);OmegaH =tan(lambda/2)/2./M;OmegaT =1./M./(z.^(3/2)+aa);
     C=3*C1.*(M.*z.*OmegaT).^2; 
   eta=OmegaT./OmegaH;
     Erot=2*M.*sin(lambda/4).^2;Erot_k(kk)=Erot(1); 
     A=C.*eta.*(1-eta).*OmegaH.^2;  
     f=OmegaT/pi*3e10/(1.5e5*fM);       %f scaling by fM
     EkTorus=C1.*(M.*z.*OmegaT).^2;Lj=(OmegaH.*z).^2.*EkTorus;
   
   %PLOT RESULTS (SCALING BY fM WHEN NEEDED)
   f0(kk)=f(1);f1(kk)=f(L);dT(1)=T(1);df(1)=f(2)-f(1);
   dM(1)=M(2)-M(1);mf(1)=f(1);
   L=length(T);
   for i=2:L-1
       dT(i)=(T(i+1)-T(i-1))/2;df(i)=(f(i+1)-f(i-1))/2;
       dM(i)=(M(i+1)-M(i-1))/2;dE(i)=dM(i)/df(i);
       mf(i)=(f(i+1)+f(i-1))/2;
   end;
   dT(L)=T(L)-T(L-1);df(L)=f(L)-f(L-1);
   dM(L)=M(L)-M(L-1);                   %mf(L)=f(L);
   N_Phi(kk)=sum(f(1:L).*transpose(dT(1:L)));%total nr of periods
   dk=kkmx/10;
   if(kk==kkmx)
      IP=1:100:L;l=length(IP),IP(l+1)=L;
      FS=14;
      subplot(321);
        plot(T(IP),Y(IP,1));hold on;grid on;
        plot(T(IP),Y(IP,2),'LineWidth',3);
        plot(T(IP),Erot(IP),'-o');
        xlabel('time [s]','fontsize',FS);legend('M(t)','J(t)','E_{rot}^H(t)');
      subplot(322);
        plot(T,eta);hold on;plot(T,OmegaT/OmegaH(1),'LineWidth',3);grid on;
        legend('\Omega_T/\Omega_H','\Omega_T/\Omega_{H,i}');xlabel('time [s]','fontsize',FS);    
      subplot(323);
        plot(T,A*fM);grid on;hold on;
        ylabel('L_{H}','fontsize',FS);xlabel('time [s]','fontsize',FS);axis([0 60 0 0.15]);
      subplot(324);
        y=Egw./Erot_k;x=a_k;plot(x,y);grid on;
        ylabel('{\Delta E /E_{rot}}','FontSize',FS);xlabel('Initial spin-parameter J/M^2','FontSize',FS);axis([0 1 0 1]);
      subplot(325);
        plot(T,(fM*dM)./df);grid on; %fM already in f and df
        ylabel('{dE/df}','FontSize',FS);xlabel('time [s]','FontSize',FS);
      subplot(326);
        plot(f,(fM*dM)./df);grid on; %fM already in f and df
        clear dEdf;dEdf(:,1)=f;dEdf(:,2)=(fM*dM)./df; %dEdf scaled by fM
        ylabel('{dE/df}','FontSize',FS);xlabel('frequency [Hz]','FontSize',FS);  
      if(kk==kkmx)
        print -djpeg ISCOchirpa.jpg;print -depsc ISCOchirpa.eps;
      end
   end
  
end

%PLOT GRB LIGHT CURVE CHARACTERISTICS
figure(40*fM+2);
subplot(221);
  Lj=Lj;Lj=Lj/max(Lj);[p,q]=max(Lj);
  plot(T,Lj);grid on; hold on;box on;plot(T(q),Lj(q),'o');
  legend(['M_H=' num2str(fM) 'M_{sun}'],['a/M = ' num2str(aa(q),4)]);
  ylabel('L_{GRB} (a.u.)','FontSize',14);xlabel('time [s]','FontSize',14);
subplot(222);
  thetaH=sqrt(z);thetaH=thetaH/max(thetaH);
  plot(T,thetaH);grid on;hold on;box on;ylabel('\theta_H/max(\theta_H)','FontSize',14);xlabel('time [s]','FontSize',14);
subplot(223);
  plot(aa,Lj);grid on;hold on;xlabel('a/M','FontSize',14);ylabel('L_{GRB} (a.u.)','FontSize',14);
subplot(224);
  plot(thetaH,Lj);grid on;hold on;xlabel('(\theta_H/max(\theta_H)','FontSize',14);ylabel('L_{GRB} (a.u.)','FontSize',14);
  print -djpeg ISCOchirpb.jpg;print -depsc ISCOchirpb.eps;

%NORMALIZED OUTPUT (M-INIT=1MSolar)
clear out;out(:,1)=T;out(:,2)=M;out(:,3)=J;out(:,4)=A;out(:,5)=f*fM;
save fort.102 out -ascii;

%PLOT ORIENTATION AVERAGED HCHAR(f)
hchar(:,1)=dEdf(:,1);
hchar(:,2)=sqrt(2)/pi/3.09e26*sqrt(dEdf(:,2)*1.5e5*3e10);
figure(40*fM+3)
plot(hchar(:,1),hchar(:,2));grid on;legend('h_{char}(f) (D=100 Mpc)');
xlabel('frequency [Hz]','FontSize',18);ylabel('orientation averaged strain','FontSize',18);
print -djpeg ISCOchirpc.jpg;print -depsc ISCOchirpc.eps;
save hchar.txt hchar -ascii;

%----------------------------------------------------------------------------
%   Generate chirp injection signal
%----------------------------------------------------------------------------
load fort.102;out=fort;L=length(out(:,1));
   T=out(1:L-1,1);M=out(1:L-1,2);J=out(1:L-1,3);A=out(1:L-1,4);f=out(1:L-1,5);
   M=M*fM;A=A*fM;f=f/fM;
   
%INSTANTANEOUS STRAIN h(t) INCLUDE 1/sqrt(5) for ORIENTATION AVERAGING
   D=1e8*3.1e18;c=3e10;Omega=pi*mf; %(Omega=Omega_ISCO=0.5*Omega_GW);fm scaled 
   Lgw=A*1.5e5                      %[cm/s]
   h=sqrt(Lgw')./(Omega*D)*sqrt(c);
   mxh=[mean(h) max(h) max(Lgw) mean(mf)]
   
   %INTERPOLATE AT SAMPLING FREQUENCY fS  
   fS=4096;                         %LIGO downsampled
   clear t_i h_i f_i s;
   Ns=1;Nf=TTot*fS;i_1(1)=1;i_2(1)=length(T);
   for k=1:Ns
     i1=i_1(k);i2=i_2(k);ti=T(i1)+(1:Nf)*(T(i2)-T(i1))/Nf;
     hi=spline(T(i1:i2),h(i1:i2),ti);fi=spline(T(i1:i2),f(i1:i2),ti);
     t_i(:,k)=transpose(ti);h_i(:,k)=transpose(hi);f_i(:,k)=transpose(fi);
   end
   for k=1:Ns
       dt=t_i(2,k)-t_i(1,k);
       phi=cumsum(2*pi*f_i(:,k)*dt);
       s(:,k)=h_i(:,k).*sin(phi);

       w = 0;
       hp(:,k) = h_i(:,k) .* (1+cos(w).^2) .* cos(phi);
       hc(:,k) = 2 * h_i(:,k) .* cos(w) .* sin(phi);

   end
   tt = 0:(1/fS):max(t_i);
   hp = interp1(t_i,hp,tt);
   hp(isnan(hp)) = 0.0;
   hc = interp1(t_i,hc,tt);
   hc(isnan(hc)) = 0.0;

   fs = 1/(tt(2) - tt(1));
   dt = 1/fs;

   taper = linspace(0,1-dt,fs);
   N = length(taper);
   mask = ones(length(hp),1);
   mask(1:N,1)=sin(pi/2.*taper).^2;
   mask(end-N+1:end,1) = flipud(mask(1:N,1));

   hp = hp .* mask';
   hc = hc .* mask';

   fid = fopen('ISCOchirp.dat','w+');
   for ii = 1:length(tt)
      fprintf(fid,'%.10f %.10e %.10e\n',tt(ii),hp(ii),hc(ii));
   end
   fclose(fid);

   save fort.1101 s -ascii;save fort.1201 t_i -ascii;save fort.1301 f_i -ascii;
figure(40*fM+4);
  plot(t_i,s);
  xlabel('time [s]','FontSize',18);ylabel('Instantaneous strain','FontSize',18);
  legend('h(t) (M=10M_\o, D=100 Mpc)');axis([0 TTot -1.5e-23 1.5e-23]);
  print -djpeg ISCOchirpd.jpg; print -depsc ISCOchirpd.eps
 
%----------------------------------------------------------------------------
%   Plot chirp templates for time slicing at tau=1  
%----------------------------------------------------------------------------
load fort.1101;S=fort;load fort.1201;T=fort;load fort.1301;F=fort;
  Nf=4096;Ns=64;
  K=floor(length(T)/Ns);clear s t_i f_i;
  for i=1:Ns
    i1=1+(i-1)*K;i2=i*K;
    s(:,i)=S(i1:i2);t_i(:,i)=T(i1:i2);f_i(:,i)=F(i1:i2);
  end
  s=s/sqrt(5); %Fig. 12.1 in Book2012, now scaled to 100 Mpc

figure(40*fM+5);  
  dt=1/Nf;df=1;clear X Y;
  subplot(221);
    for k=1:Ns,sm(k)=max(s(:,k));fm(k)=mean(f_i(:,k));tm(k)=mean(t_i(:,k));end;
    plot(tm,sm);
    grid on;hold on;xlabel('time [s]');ylabel('orientation averaged strain');
    for k=2:Ns/4
       X(1)=tm(4*(k-1));Y(1)=0;X(2)=X(1);Y(2)=sm(4*(k-1));
       X(3)=X(1)+1;Y(3)=sm(4*(k-1)+1);X(4)=X(1)+1;Y(4)=0;
       fill(X,Y,'k');
    end
    axis([0 60 0 1e-23]);legend('D=100 Mpc');  
  subplot(222);
    plot(tm,fm);
    grid on;hold on;xlabel('time [s]');ylabel('frequency [Hz]');
    for k=2:Ns/4
       X(1)=tm(4*(k-1));Y(1)=0;X(2)=X(1);Y(2)=fm(4*(k-1));
       X(3)=X(1)+1;Y(3)=fm(4*(k-1)+1);X(4)=X(1)+1;Y(4)=0;
       fill(X,Y,'k');
    end
    clear u v;u=1:12:72;v(1:6)=2000;
    plot(u,v,'r--','linewidth',3); %max freq in LIGO downsampled data
    axis([0 60 0 3000]);legend('M=10_\o');
  
  for k=1:Ns
    ch(:,k)=fft(s(:,k))/Nf;
  end;
  L=length(ch(:,1));
  subplot(212); 
    for k=1:2:Ns
      if(max(f_i(:,k))<2000)semilogy(abs(ch(1:L/2,k)));grid on;hold on;end;
    end;
    axis([0 2000 1e-26 5e-24]);xlabel('frequency [Hz]');ylabel('Fourier coefficient');
  print -djpeg ISCOchirpe.jpg;print -depsc ISCOchirpe.eps

  %system(['rm *.pdf *.eps *.jpg fort.* *.txt'])
