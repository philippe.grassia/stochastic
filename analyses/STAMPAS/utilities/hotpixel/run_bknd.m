
set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

plotLocation = 'plots';

bins = -10:0.01:10;

[z,int_vals] = fit_dist();
int_vals = int_vals * 2;
int_vals = int_vals / sum(int_vals);
int_vals = 1 - cumsum(int_vals);

figure;
set(gcf, 'PaperSize',[10 8])
set(gcf, 'PaperPosition', [0 0 10 8])
clf
semilogy(z,int_vals,'k');
xlabel('SNR');
ylabel('Probability Density Function');
ylim([1e-10 1]);
xlim([0 15]);
print('-dpng',[plotLocation '/coherent.png']);
print('-depsc2',[plotLocation '/coherent.eps']);
close;

bins = 0:0.01:20;
chi2vals = chi2pdf(bins,2);
chi2vals = chi2vals / sum(chi2vals);
chi2vals = 1 - cumsum(chi2vals);

figure;
semilogy(bins,chi2vals,'k')
xlim([0 20]);
print('-dpng',[plotLocation '/autopower.png']);
print('-depsc2',[plotLocation '/autopower.eps']);
close;

fap1 = 1e-3; 
fap2 = 1e-4;
fap3 = 1e-5;
fap4 = 1/370;

coh1 = interp1(int_vals,z,fap1);
coh2 = interp1(int_vals,z,fap2);
coh3 = interp1(int_vals,z,fap3);
fprintf('FAP: %.3e, SNR: %.5f\n',fap1,coh1);
fprintf('FAP: %.3e, SNR: %.5f\n',fap2,coh2);
fprintf('FAP: %.3e, SNR: %.5f\n',fap3,coh3);

auto1 = interp1(chi2vals,bins,fap1);
auto2 = interp1(chi2vals,bins,fap2);
auto3 = interp1(chi2vals,bins,fap3);
fprintf('FAP: %.3e, SNR: %.5f\n',fap1,auto1);
fprintf('FAP: %.3e, SNR: %.5f\n',fap2,auto2);
fprintf('FAP: %.3e, SNR: %.5f\n',fap3,auto3);

