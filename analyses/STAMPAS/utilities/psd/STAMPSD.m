%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STAMPSD : quick PSD analysis for STAMPAS
% PURPOSE : 
%    Take N time in the time interval and estimated a PSD
%    compute the mean and the std using these estimation. 
%    then plot the PSD. a zoomed PSD can be used using the FMIN
%    .FMAX parameters   
%
% AUTHOR  : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE    : May 2016
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER's PARAMETERS
JOBFILE = '../../O2_joblists/jobsH1L1.txt';
START   = 1164067217;
STOP    = 1164412817;                        
FMIN    = [0 200 400 600 800 1000 1200 1400 1600 1800] ;
FMAX    = [200 400 600 800 1000 1200 1400 1600 1800 2000];
N       = 10; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

% add path
STAMPAS = regexprep(pwd,'(.*/STAMPAS)/.*','$1');
stamp2  = regexprep(pwd,'(.*)/STAMPAS/.*','$1/stamp2/src/anteproc');
addpath(STAMPAS);
addpath(stamp2);
addpath([STAMPAS '/functions']);
addpath([STAMPAS '/post_processing/background_estimation/src']);

% loop over the frequency range
for i=1:numel(FMIN)
  fprintf('%f %f\n',FMIN(i),FMAX(i));
  if ~exist(['mkdir -p figures/' num2str(FMIN(i)) '-' num2str(FMAX(i))])
    system(['mkdir -p figures/' num2str(FMIN(i)) '-' num2str(FMAX(i))]);
  end

  PSDplots(JOBFILE,...
           'Start',START, ...
           'Stop',STOP, ...
           'Fmin', FMIN(i)-50, ...
           'Fmax', FMAX(i)+50, ...
           'Folder',['figures/' num2str(FMIN(i)) '-' num2str(FMAX(i))],...
           'Ntime',N);
end
