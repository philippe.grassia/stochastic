segexpr 'union( H1-VETOTIME_CAT1_A.txt,  H1-VETOTIME_CAT4_A.txt)' | sort > H1-VETOTIME_A.txt
segexpr 'union( H1-VETOTIME_CAT1_B.txt,  H1-VETOTIME_CAT4_B.txt)' | sort > H1-VETOTIME_B.txt
segexpr 'union( H1-VETOTIME_CAT1_C.txt,  H1-VETOTIME_CAT4_C.txt)' | sort > H1-VETOTIME_C.txt
segexpr 'union( H1-VETOTIME_CAT1_D.txt,  H1-VETOTIME_CAT4_D.txt)' | sort > H1-VETOTIME_D.txt

segexpr 'union( L1-VETOTIME_CAT1_A.txt,  L1-VETOTIME_CAT4_A.txt)' | sort > L1-VETOTIME_A.txt
segexpr 'union( L1-VETOTIME_CAT1_B.txt,  L1-VETOTIME_CAT4_B.txt)' | sort > L1-VETOTIME_B.txt
segexpr 'union( L1-VETOTIME_CAT1_C.txt,  L1-VETOTIME_CAT4_C.txt)' | sort > L1-VETOTIME_C.txt
segexpr 'union( L1-VETOTIME_CAT1_D.txt,  L1-VETOTIME_CAT4_D.txt)' | sort > L1-VETOTIME_D.txt

segexpr 'union(H1-VETOTIME_A.txt,H1-VETOTIME_B.txt)' | sort > tmp1
segexpr 'union(H1-VETOTIME_C.txt,H1-VETOTIME_D.txt)' | sort > tmp2
segexpr 'union(tmp1,tmp2)' | sort > H1-VETOTIME.txt  
segexpr 'union(L1-VETOTIME_A.txt,L1-VETOTIME_B.txt)' | sort > tmp1
segexpr 'union(L1-VETOTIME_C.txt,L1-VETOTIME_D.txt)' | sort > tmp2
segexpr 'union(tmp1,tmp2)' | sort > L1-VETOTIME.txt  

segexpr 'union(H1-VETOTIME.txt,../ByHand_vetoes.txt)' | sort > tmp1
mv tmp1 H1-VETOTIME.txt

segexpr 'union(L1-VETOTIME.txt,../ByHand_vetoes.txt)' | sort > tmp1
mv tmp1 L1-VETOTIME.txt

segexpr 'union(H1-SCIENCE_A.txt,H1-SCIENCE_B.txt)' | sort > tmp1
segexpr 'union(H1-SCIENCE_C.txt,H1-SCIENCE_D.txt)' | sort > tmp2
segexpr 'union(tmp1,tmp2)' | sort > H1-SCIENCE.txt  
segexpr 'union(L1-SCIENCE_A.txt,L1-SCIENCE_B.txt)' | sort > tmp1
segexpr 'union(L1-SCIENCE_C.txt,L1-SCIENCE_D.txt)' | sort > tmp2
segexpr 'union(tmp1,tmp2)' | sort > L1-SCIENCE.txt  

segexpr 'intersection(H1-SCIENCE.txt,not(H1-VETOTIME.txt))' | sort > H1-SCIENCE_afterveto.txt
segexpr 'intersection(L1-SCIENCE.txt,not(L1-VETOTIME.txt))' | sort > L1-SCIENCE_afterveto.txt

segexpr 'intersection(H1-SCIENCE_afterveto.txt, L1-SCIENCE_afterveto.txt)' | sort | awk '{if ($2-$1>200) print "1 " $1,$2,$2-$1}'> H1L1_afterveto.txt

rm tmp*
rm H1-VETOTIME_A.txt  H1-VETOTIME_B.txt  H1-VETOTIME_C.txt  H1-VETOTIME_D.txt
rm L1-VETOTIME_A.txt  L1-VETOTIME_B.txt  L1-VETOTIME_C.txt  L1-VETOTIME_D.txt
