export S6_SEGMENT_SERVER=https://segdb.ligo.caltech.edu

S6A_start=931035615 
S6A_end=935798415

S6B_start=938004408
S6B_end=947023216

S6C_start=947023216
S6C_end=961545615

S6D_start=961545615
S6D_end=971622015 

#-s 931035296 -e 935798487
#-s 937526415 -e 947344275
#-s 948444884 -e 961545687
#-s 961545688 -e 971654415

#ligolw_segments_from_cats -s $S6A_start -e $S6A_end -d -p -v https://www.lsc-group.phys.uwm.edu/bursts/public/runs/s6/dqv/category_definer/H1L1V1-S6A_BURST_ALLSKY_OFFLINE-930960015-5011200.xml
#ligolw_segments_from_cats -s $S6B_start -e $S6B_end -d -p -v https://www.lsc-group.phys.uwm.edu/bursts/public/runs/s6/dqv/category_definer/H1L1V1-S6B_BURST_ALLSKY_OFFLINE-937526415-9496800.xml
#ligolw_segments_from_cats -s $S6C_start -e $S6C_end -d -p -v https://www.lsc-group.phys.uwm.edu/bursts/public/runs/s6/dqv/category_definer/H1L1-S6C_BURST_ALLSKY_OFFLINE-947344275-20000000.xml
#ligolw_segments_from_cats -s $S6D_start -e $S6D_end -d -p -v https://www.lsc-group.phys.uwm.edu/bursts/public/runs/s6/dqv/category_definer/H1L1V1-S6D_BURST_ALLSKY_OFFLINE-961545615-0.xml

ligolw_segment_query -t https://segdb.ligo.caltech.edu -q -s ${S6A_start} -e ${S6D_end} -a H1:DCH-INJECTION_STOCHASTIC:3 | ligolw_print -t segment -c start_time -c end_time -d " " > H1:DCH-INJECTION_STOCHASTIC.txt

ligolw_segment_query -t https://segdb.ligo.caltech.edu -q -s ${S6A_start} -e ${S6D_end} -a L1:DCH-INJECTION_STOCHASTIC:3 | ligolw_print -t segment -c start_time -c end_time -d " " > L1:DCH-INJECTION_STOCHASTIC.txt


ligolw_print H1-VETOTIME_CAT1-${S6A_start}-*.xml -t segment -c start_time -c end_time -d " " > H1-VETOTIME_CAT1_A.txt
ligolw_print H1-VETOTIME_CAT1-${S6B_start}-*.xml -t segment -c start_time -c end_time -d " " > H1-VETOTIME_CAT1_B.txt
ligolw_print H1-VETOTIME_CAT1-${S6C_start}-*.xml -t segment -c start_time -c end_time -d " " > H1-VETOTIME_CAT1_C.txt
ligolw_print H1-VETOTIME_CAT1-${S6D_start}-*.xml -t segment -c start_time -c end_time -d " " > H1-VETOTIME_CAT1_D.txt

ligolw_print H1-VETOTIME_CAT4-${S6A_start}-*.xml -t segment -c start_time -c end_time -d " " > H1-VETOTIME_CAT4_A.txt
ligolw_print H1-VETOTIME_CAT4-${S6B_start}-*.xml -t segment -c start_time -c end_time -d " " > H1-VETOTIME_CAT4_B.txt
ligolw_print H1-VETOTIME_CAT4-${S6C_start}-*.xml -t segment -c start_time -c end_time -d " " > H1-VETOTIME_CAT4_C.txt
ligolw_print H1-VETOTIME_CAT4-${S6D_start}-*.xml -t segment -c start_time -c end_time -d " " > H1-VETOTIME_CAT4_D.txt


ligolw_print L1-VETOTIME_CAT1-${S6A_start}-*.xml -t segment -c start_time -c end_time -d " " > L1-VETOTIME_CAT1_A.txt
ligolw_print L1-VETOTIME_CAT1-${S6B_start}-*.xml -t segment -c start_time -c end_time -d " " > L1-VETOTIME_CAT1_B.txt
ligolw_print L1-VETOTIME_CAT1-${S6C_start}-*.xml -t segment -c start_time -c end_time -d " " > L1-VETOTIME_CAT1_C.txt
ligolw_print L1-VETOTIME_CAT1-${S6D_start}-*.xml -t segment -c start_time -c end_time -d " " > L1-VETOTIME_CAT1_D.txt

ligolw_print L1-VETOTIME_CAT4-${S6A_start}-*.xml -t segment -c start_time -c end_time -d " " > L1-VETOTIME_CAT4_A.txt
ligolw_print L1-VETOTIME_CAT4-${S6B_start}-*.xml -t segment -c start_time -c end_time -d " " > L1-VETOTIME_CAT4_B.txt
ligolw_print L1-VETOTIME_CAT4-${S6C_start}-*.xml -t segment -c start_time -c end_time -d " " > L1-VETOTIME_CAT4_C.txt
ligolw_print L1-VETOTIME_CAT4-${S6D_start}-*.xml -t segment -c start_time -c end_time -d " " > L1-VETOTIME_CAT4_D.txt

segexpr 'union( H1-VETOTIME_CAT1_A.txt, H1:DCH-INJECTION_STOCHASTIC.txt)' | sort > tmp
mv tmp H1-VETOTIME_CAT1_A.txt
segexpr 'union( H1-VETOTIME_CAT1_B.txt, H1:DCH-INJECTION_STOCHASTIC.txt)' | sort > tmp
mv tmp H1-VETOTIME_CAT1_B.txt
segexpr 'union( H1-VETOTIME_CAT1_C.txt, H1:DCH-INJECTION_STOCHASTIC.txt)' | sort > tmp
mv tmp H1-VETOTIME_CAT1_C.txt
segexpr 'union( H1-VETOTIME_CAT1_D.txt, H1:DCH-INJECTION_STOCHASTIC.txt)' | sort > tmp
mv tmp H1-VETOTIME_CAT1_D.txt

segexpr 'union( L1-VETOTIME_CAT1_A.txt, L1:DCH-INJECTION_STOCHASTIC.txt)' | sort > tmp
mv tmp L1-VETOTIME_CAT1_A.txt
segexpr 'union( L1-VETOTIME_CAT1_B.txt, L1:DCH-INJECTION_STOCHASTIC.txt)' | sort > tmp
mv tmp L1-VETOTIME_CAT1_B.txt
segexpr 'union( L1-VETOTIME_CAT1_C.txt, L1:DCH-INJECTION_STOCHASTIC.txt)' | sort > tmp
mv tmp L1-VETOTIME_CAT1_C.txt
segexpr 'union( L1-VETOTIME_CAT1_D.txt, L1:DCH-INJECTION_STOCHASTIC.txt)' | sort > tmp
mv tmp L1-VETOTIME_CAT1_D.txt

#rm V1-VETOTIME*