S6A_start=931035615 
S6A_end=935798415

S6B_start=938004408
S6B_end=947023216

S6C_start=947023216
S6C_end=961545615

S6D_start=961545615
S6D_end=971622015 

segexpr 'union(H1-SCIENCE_A.txt, H1-SCIENCE_B.txt)' | sort > tmp1
segexpr 'union(H1-SCIENCE_C.txt, H1-SCIENCE_D.txt)' | sort > tmp2
segexpr 'union(tmp1,tmp2)' | sort > H1_SCIENCE.txt

segexpr 'union(L1-SCIENCE_A.txt, L1-SCIENCE_B.txt)' | sort > tmp1
segexpr 'union(L1-SCIENCE_C.txt, L1-SCIENCE_D.txt)' | sort > tmp2
segexpr 'union(tmp1,tmp2)' | sort > L1_SCIENCE.txt

segexpr 'intersection(H1_SCIENCE.txt,L1_SCIENCE.txt)' | sort > H1L1.txt


segexpr 'union( H1-VETOTIME_CAT1_A.txt,  H1-VETOTIME_CAT4_A.txt)' | sort > H1-VETOTIME_A.txt
segexpr 'union( H1-VETOTIME_CAT1_B.txt,  H1-VETOTIME_CAT4_B.txt)' | sort > H1-VETOTIME_B.txt
segexpr 'union( H1-VETOTIME_CAT1_C.txt,  H1-VETOTIME_CAT4_C.txt)' | sort > H1-VETOTIME_C.txt
segexpr 'union( H1-VETOTIME_CAT1_D.txt,  H1-VETOTIME_CAT4_D.txt)' | sort > H1-VETOTIME_D.txt

segexpr 'union( L1-VETOTIME_CAT1_A.txt,  L1-VETOTIME_CAT4_A.txt)' | sort > L1-VETOTIME_A.txt
segexpr 'union( L1-VETOTIME_CAT1_B.txt,  L1-VETOTIME_CAT4_B.txt)' | sort > L1-VETOTIME_B.txt
segexpr 'union( L1-VETOTIME_CAT1_C.txt,  L1-VETOTIME_CAT4_C.txt)' | sort > L1-VETOTIME_C.txt
segexpr 'union( L1-VETOTIME_CAT1_D.txt,  L1-VETOTIME_CAT4_D.txt)' | sort > L1-VETOTIME_D.txt

segexpr 'intersection(H1-SCIENCE_A.txt,not(H1-VETOTIME_A.txt))' | sort > H1-SCIENCE_A_afterveto.txt
segexpr 'intersection(H1-SCIENCE_B.txt,not(H1-VETOTIME_B.txt))' | sort > H1-SCIENCE_B_afterveto.txt
segexpr 'intersection(H1-SCIENCE_C.txt,not(H1-VETOTIME_C.txt))' | sort > H1-SCIENCE_C_afterveto.txt
segexpr 'intersection(H1-SCIENCE_D.txt,not(H1-VETOTIME_D.txt))' | sort > H1-SCIENCE_D_afterveto.txt

segexpr 'intersection(L1-SCIENCE_A.txt,not(L1-VETOTIME_A.txt))' | sort > L1-SCIENCE_A_afterveto.txt
segexpr 'intersection(L1-SCIENCE_B.txt,not(L1-VETOTIME_B.txt))' | sort > L1-SCIENCE_B_afterveto.txt
segexpr 'intersection(L1-SCIENCE_C.txt,not(L1-VETOTIME_C.txt))' | sort > L1-SCIENCE_C_afterveto.txt
segexpr 'intersection(L1-SCIENCE_D.txt,not(L1-VETOTIME_D.txt))' | sort > L1-SCIENCE_D_afterveto.txt

segexpr 'intersection(H1-SCIENCE_A_afterveto.txt,L1-SCIENCE_A_afterveto.txt)' > H1L1_A_ini.txt
segexpr 'intersection(H1-SCIENCE_B_afterveto.txt,L1-SCIENCE_B_afterveto.txt)' > H1L1_B_ini.txt
segexpr 'intersection(H1-SCIENCE_C_afterveto.txt,L1-SCIENCE_C_afterveto.txt)' > H1L1_C_ini.txt
segexpr 'intersection(H1-SCIENCE_D_afterveto.txt,L1-SCIENCE_D_afterveto.txt)' > H1L1_D_ini.txt

segexpr 'intersection(H1L1_A_ini.txt,not(../ByHand_vetoes.txt))' | awk '{if ($2-$1>200) print $1,$2,$2-$1}' > H1L1_A_av.txt
segexpr 'intersection(H1L1_B_ini.txt,not(../ByHand_vetoes.txt))' | awk '{if ($2-$1>200) print $1,$2,$2-$1}' > H1L1_B_av.txt
segexpr 'intersection(H1L1_C_ini.txt,not(../ByHand_vetoes.txt))' | awk '{if ($2-$1>200) print $1,$2,$2-$1}' > H1L1_C_av.txt
segexpr 'intersection(H1L1_D_ini.txt,not(../ByHand_vetoes.txt))' | awk '{if ($2-$1>200) print $1,$2,$2-$1}' > H1L1_D_av.txt

segexpr 'union(H1L1_A_av.txt,H1L1_B_av.txt)' > tmp1
segexpr 'union(H1L1_C_av.txt,H1L1_D_av.txt)' > tmp2

segexpr 'union(tmp1,tmp2)' > H1L1_aftercat1.txt

cat H1L1.txt | awk '{if ($2-$1>500) x=x+$2-$1}END{print x " s (" x/86400 " days)"}'
cat H1L1_aftercat1.txt | awk '{if ($2-$1>500) x=x+$2-$1}END{print x " s (" x/86400 " days)"}'

rm tmp1
rm tmp2
rm H1L1_*_ini.txt
rm H1L1_*_av.txt
rm H1_*_afterveto.txt
rm L1_*_afterveto.txt
