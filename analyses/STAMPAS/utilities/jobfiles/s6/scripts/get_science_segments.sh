export S6_SEGMENT_SERVER=https://segdb.ligo.caltech.edu

S6A_start=931035615 
S6A_end=935798415

S6B_start=938004408
S6B_end=947023216

S6C_start=947023216
S6C_end=961545615

S6D_start=961545615
S6D_end=971622015 

#-s 931035296 -e 935798487
#-s 937526415 -e 947344275
#-s 948444884 -e 961545687
#-s 961545688 -e 971654415

ligolw_segment_query -t $S6_SEGMENT_SERVER -q -s $S6A_start -e $S6A_end -a H1:DMT-SCIENCE:4 -o H1_masterA.xml
ligolw_segment_query -t $S6_SEGMENT_SERVER -q -s $S6B_start -e $S6B_end -a H1:DMT-SCIENCE:4 -o H1_masterB.xml
ligolw_segment_query -t $S6_SEGMENT_SERVER -q -s $S6C_start -e $S6C_end -a H1:DMT-SCIENCE:4 -o H1_masterC.xml
ligolw_segment_query -t $S6_SEGMENT_SERVER -q -s $S6D_start -e $S6D_end -a H1:DMT-SCIENCE:4 -o H1_masterD.xml

ligolw_segment_query -t $S6_SEGMENT_SERVER -q -s $S6A_start -e $S6A_end -a L1:DMT-SCIENCE:4 -o L1_masterA.xml
ligolw_segment_query -t $S6_SEGMENT_SERVER -q -s $S6B_start -e $S6B_end -a L1:DMT-SCIENCE:4 -o L1_masterB.xml
ligolw_segment_query -t $S6_SEGMENT_SERVER -q -s $S6C_start -e $S6C_end -a L1:DMT-SCIENCE:4 -o L1_masterC.xml
ligolw_segment_query -t $S6_SEGMENT_SERVER -q -s $S6D_start -e $S6D_end -a L1:DMT-SCIENCE:4 -o L1_masterD.xml

ligolw_print H1_masterA.xml -t segment -c start_time -c end_time -d " " > H1-SCIENCE_A.txt
ligolw_print H1_masterB.xml -t segment -c start_time -c end_time -d " " > H1-SCIENCE_B.txt
ligolw_print H1_masterC.xml -t segment -c start_time -c end_time -d " " > H1-SCIENCE_C.txt
ligolw_print H1_masterD.xml -t segment -c start_time -c end_time -d " " > H1-SCIENCE_D.txt

ligolw_print L1_masterA.xml -t segment -c start_time -c end_time -d " " > L1-SCIENCE_A.txt
ligolw_print L1_masterB.xml -t segment -c start_time -c end_time -d " " > L1-SCIENCE_B.txt
ligolw_print L1_masterC.xml -t segment -c start_time -c end_time -d " " > L1-SCIENCE_C.txt
ligolw_print L1_masterD.xml -t segment -c start_time -c end_time -d " " > L1-SCIENCE_D.txt