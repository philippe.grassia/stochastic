#! /usr/bin/env bash

# Options
usage(){
    echo 'Usage ./get_segments.sh 1178678567 H1'
    echo 'Arg 1: GPS start time'
    echo 'Arg 2: GPS end time'
    echo 'Arg 3: name'
    exit
}


# main
[[ $# -ne 3 ]] && usage

GPS_START=$1
GPS_END=$2
DQ_NAME=$3
echo $GPS_START
echo $GPS_END
echo $DQ_NAME

ligolw_segment_query_dqsegdb --segment-url=https://segments.ligo.org --query-segments --include-segments ${DQ_NAME} --gps-start-time ${GPS_START} --gps-end-time ${GPS_END} | ligolw_print -t segment:table -c start_time -c end_time -d " " > ${DQ_NAME}.txt
