#! /usr/bin/env bash

# Options
usage(){
    echo 'Usage ./generate_jobfile.sh 1178678567 H1'
    echo 'Arg 1: GPS time'
    echo 'Arg 2: ifo'
    exit
}


# main
[[ $# -ne 2 ]] && usage

GPS=$1
IFO=$2

ligolw_dq_query_dqsegdb -t https://segments-dev.ligo.org -b -a -f ${GPS} | grep ${IFO}

