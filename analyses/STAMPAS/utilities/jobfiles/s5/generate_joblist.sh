#!/usr/bin/env bash

# merge all cat1 vetoes for H1 and L1

test=0

echo 'Generating the union of cat 1 flags ...'
if [ $test == 0 ]; then
    if [ -e tmp ]; then
	rm tmp
    fi
    for i in `cat stoch_H1.txt`; do
#	echo $i
	if [ ! $i == "H1:MASTER_OVERFLOW_LSC" ]; then
	    segexpr "union(tmp, stoch/science/${i}.txt)" >> tmp
	    sort tmp | uniq > tmp2
	    mv tmp2 tmp
	else
	    echo "excluded: " $i
	fi
    done
    mv tmp stoch_H1.veto
    
    for i in `cat stoch_L1.txt`; do
#	echo $i
	if [ ! $i == "L1:MASTER_OVERFLOW_LSC" ]; then
	    segexpr "union(tmp, stoch/science/${i}.txt)" >> tmp
	    sort tmp | uniq > tmp2
	    mv tmp2 tmp
	else
	    echo "excluded: " $i
	fi
    done
    mv tmp stoch_L1.veto
fi

echo 'Intersection Science with cat1 flags for each ifo ...'
# intersec SCIENCE with cat1 vetoes for H1 and L1
segexpr "intersection(stoch/science/H1:Science.txt, not(stoch_H1.veto))" > tmp
sort tmp > H1_science_after_cat1.txt
segexpr "intersection(stoch/science/L1:Science.txt, not(stoch_L1.veto))" > tmp
sort tmp > L1_science_after_cat1.txt

echo 'Coincidence SCIENCE H1L1 ...'
# coincidence SCIENCE H1 L1
segexpr "intersection(stoch/science/H1:Science.txt, stoch/science/L1:Science.txt)" > tmp
segexpr "intersection(tmp, boundaries.txt)" > H1L1.txt

echo 'Coincidence SCIENCE H1L1 after cat1 ...'
# coincidence H1 L1 after cat1
segexpr "intersection(H1_science_after_cat1.txt, L1_science_after_cat1.txt)" > tmp
segexpr "intersection(tmp, boundaries.txt)" > H1L1_aftercat1.txt

cat H1L1_aftercat1.txt | awk 'BEGIN{i=1}{if ($2-$1>181) print i,$1,$2,$2-$1;i=i+1}' > jobsH1L1.txt

echo 'Live time computation ...'
echo ' ' 
# livetime
cat stoch_H1.veto | awk 'BEGIN{x=0}{x=x+$2-$1}END{print "H1 veto: " x}'
cat stoch_L1.veto | awk 'BEGIN{x=0}{x=x+$2-$1}END{print "L1 veto: " x}'

cat H1L1.txt | awk 'BEGIN{x=0}{if ($2-$1>500) x=x+$2-$1}END{print "H1L1 science: " x}'
cat H1L1_aftercat1.txt | awk 'BEGIN{x=0}{if ($2-$1>500) x=x+$2-$1}END{print "H1L1 science after cat1:" x " " x-25923491}'

if [ -e tmp ]; then
    rm tmp
fi
