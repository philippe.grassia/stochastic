# This script is 
# 1/ getting the list of all flags for a given run/time period
# 2/ download flags' segments
#
# S6 time periods and run name are hard-coded for S6
#
# Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
#

if [ $# != 1 ]; then
    echo 'Arg 1: run name [S5/S6]'
    exit
fi

run=$1
shift

if [ $run == S5 ]; then
    GPS_START=814743552
    GPS_END=875232014
    filename=S5.xml
    DATABASE=https://metaserver.phy.syr.edu
else 
    if [$run == S6 ]; then
	GPS_START=931035313
	GPS_END=971557962
	filename=S6A-S6B-S6C-S6D.xml
	DATABASE=https://segdb.ligo.caltech.edu
    else
	echo 'run name is not recohnized'
	exit
    fi 
fi
    

#ligolw_segment_query --show-types -t $DATABASE --gps-start-time $GPS_START --gps-end-time $GPS_END > allDQs/${filename}

#ligolw_print -t show_types_result:table -c ifos -c name -d " " allDQs/${filename} | awk '{if ($1!= "V1") print $0}' | awk '{if ($1!= "G1") print $0}' | grep -v ame | sort | uniq > allDQs/${run}_H1L1_names.txt

for i in `cat stoch_burst_H1L1_names.list | awk '{print $1":"$2}'`; do
    echo $i
    ligolw_segment_query --query-segments -t $DATABASE --gps-start-time $GPS_START --gps-end-time $GPS_END --include-segments $i | ligolw_print -t segment:table -c start_time -c end_time -d " " > stoch/$i.txt
done

