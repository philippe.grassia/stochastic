#!/usr/bin/env bash

## This script loops over all the data quality flags files
## and interesect them with the science segments
## (useful to calculate dead time)
## Currently set for H1 L1.
##
## Modified by Marie Anne Bizouard (mabizoua@lal.in2p3.fr) 08/11/2014
##########################################################

eval `/ligotools/bin/use_ligotools` 

if [ $# != 1 ]; then
    echo 'Missing arguments:'
    echo 'Arg 1: run [S5/S6]'
    exit
fi

run=$1
shift


if [ ! -d stoch/science ]; then
    mkdir stoch/science
fi

for ifo in 'H' 'L'; do
    echo $ifo
    if [ ${run} == S5 ]; then
	science_dq_name=${ifo}1\:Science
    else
	science_dq_name=${ifo}1\:DMT-SCIENCE
    fi
    
    DQFiles=`ls stoch/${ifo}[0-1]*`

    for dq in $DQFiles
    do
#
#	echo $dq
	dqRep=`echo $dq | cut -d '/' -f 2`
#	echo $dqRep
	segexpr 'intersection('stoch/${science_dq_name}.txt','$dq')' > stoch/science/$dqRep
	cat stoch/science/$dqRep | awk -v file=$dqRep 'BEGIN{x=0}{x=x+$2-$1}END{printf("%s \t%d \n", file, x)}'
    done


done

