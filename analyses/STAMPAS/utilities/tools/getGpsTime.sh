#!/bin/bash  
# print the Gps time of a given week in a given Run 
# ------------------------------------------------------------------
# Syntax:
#   -getGpsTime RUN [Week]
# ------------------------------------------------------------------
# Purpose: 
#    convert the gps time of a run into unix time in order to use 
#    the date command. the run is assume to begin first midnight
#    before. unix-gps convertion code is based on the original php
#    code (http://www.andrews.edu/~tzs/timeconv/timealgorithm.html)
# ------------------------------------------------------------------
# author:                         FREY valentin
# EMail Address:                  valentin.frey@gmail.com
# version                         1.0
# Comment:                        
# ------------------------------------------------------------------

### PLEASE CHANGE HERE OFFICIAL GPS TIME ###
### write Inf if the run is not end
case $1 in
'S5')
	start=815155213
	stop=875232014
	;;
'S6')
	start=931035615
	stop=971622015
	;;
'ER7')
	start=1116700672
	stop=1118331000
	;;
'ER8')
	start=1124064017
#	stop=1126623617
	stop=1126051217
	;;
'O1')
#	start=1126623617
	start=1126051217
	stop=1137283217
#	stop=1136649617
	;;
'O2')
	start=1164067217
	stop=1187733618
	;;
'O3')
	start=1235433618
	stop=9999999999
	;;
.*)
	echo 'unknow run'
	exit 1
	;;
esac
################## END #####################

### variable
gpsWeek=604800

### usage function
function usage(){
    echo -e "\nGive the gps time of a given week in a given run"
    echo "usage : ./getGpsTime RUN [week]"
    echo "       RUN  : the run name "
    echo "       week : [optional] the week number"
    echo ""
    echo -e "eg. ./getGpsTime ER7 1\n"    
}

###############################################################
### Funtion to convert GPS <-> Unix time                    ###
### this part of code is based on the original at :         ###
### http://www.andrews.edu/~tzs/timeconv/timealgorithm.html ###
###############################################################

leaps=(46828800 78364801 109900802 173059203 252028804 \
    315187205 346723206 393984007 425520008 457056009 504489610 \
    551750411 599184012 820108813 914803214 1025136015 1119744016 \
    1341118800)

### Test to see if a GPS second is a leap second
function isleap() {
    isLeap=0
    for i in ${leaps[@]}; do
	if [[ $1 -eq $i ]];then
            isLeap=1
            break
	fi
    done
}

function getNbLeap() {
    nbleap=0
    for i in ${leaps[@]}; do
	if [[ $1 -gt $i ]];then
	    nbleap=`echo "$nbleap+1" | bc`
	fi
    done
}

### Test to see if a unixtime second is a leap second
function isunixtimeleap() {    
    unix2gps $1
    isleap $gpsTime
    isunixleap=$isLeap
}

### Convert GPS Time to Unix Time
function gps2unix() {    
    getNbLeap $1
    unixTime=$(($1 + 315964800 - $nbleap))

    isunixtimeleap $unixTime
    if [[ $? == 1 ]];then
	unixTime=$unixTime + 0.5
    fi

    
}

### Convert Unix Time to GPS Time
function unix2gps() {
    unixTime=$1

    fpart=`echo "scale=1; $1%1" | bc`
    if [[ $fpart != 0 ]];then
	unixTime=`echo "scale=1; $unixTime - 0.5" | bc`
	isLeaps=1
    else
	isLeaps=0
    fi

    gpsTime=$(($unixTime - 315964800))
    getNbLeap $gpsTime
    gpsTime=$(($gpsTime + $isLeaps + $nbleap))
}

############################################
###  --  END GPS <-> UNIX CONVERTER  --  ###
############################################



##############################
### GET GPS TIME FUNCTIONS ### 
##############################

### get the gps time of the first Midnight
### before the gps time given
### [gpsTime] = getFirstMidnightBefore gps 
function getFirstMidnightBefore(){
    gps2unix $1
    dt=`date -ud @$unixTime +"%Y-%m-%d"`
    dt=$dt" 00:00:00"
    dt=`date -ud "$dt" +%s`
    unix2gps $dt
}

### print formatted gps time of the RUN and the givenm week
### [] = printGpsTime gspStart gpsStop [gpsWeekStart] 
function printGpsTime(){
    ostart=$1
    ostop=$2

    ### if stop == Inf take actual time
    if [[ $ostop == 'Inf' ]];then
	unix2gps $(date -u +"%s")
	ostop=$gpsTime
    fi

    ### --- OFFICIAL GPS TIME --- ###
    # convert gps time to unix time 
    gps2unix $ostart
    unixTimeStart=$unixTime
    dtStart=`date -ud @$unixTimeStart +"%Y-%m-%d[%T]"`

    gps2unix $ostop
    unixTimeStop=$unixTime
    dtStop=`date -ud @$unixTimeStop +"%Y-%m-%d[%T]"` 
    
    # display official time
    echo -e "\n"${run}" Official Gps Times"
    echo "------------------------------"
    printf "%20s %20s\n" $dtStart $dtStop
    printf "%20s %20s\n" $ostart $ostop
    ### --- END OFFICIAL GPS TIME --- ###


    ### --- RUN GPS TIME --- ###
    # variable
    if [[ "$#" == 3 ]];then
	pstart=$3
    else
	pstart=$1
    fi
    
    # convert start gps time into unix time
    getFirstMidnightBefore $pstart
    gpsStart=$gpsTime
    gps2unix $gpsTime
    unixTimeStart=$unixTime
    dtStart=`date -ud @$unixTimeStart +"%Y-%m-%d[%T]"`
    
    # convert stop gps time into unix time
    if [[ "$#" == 3 ]] && [[ $(($gpsStart+$gpsWeek)) -lt $ostop ]];then
	gpsStop=$(($gpsStart+$gpsWeek))
	gps2unix $gpsStop
	unixTimeStop=$unixTime
	dtStop=`date -ud @$unixTimeStop +"%Y-%m-%d[%T]"` 
	echo -e "\n"${run}" week n" ${week} " STAMPAS Gps Times"
    else
	echo -e "\n"${run}" STAMPAS Gps Times"
	gpsStop=$ostop
	gps2unix $ostop
        unixTimeStop=$unixTime
        dtStop=`date -ud @$unixTimeStop +"%Y-%m-%d[%T]"`
    fi
    
    echo "------------------------------"
    printf "%20s %20s\n" $dtStart $dtStop
    printf "%20s %20s\n" $gpsStart $gpsStop

}

###################################
###  END GET GPS TIME FUNCTION  ###
###################################


### MAIN FUNCTION
if [[ "$#" != 1 ]] && [[ "$#" != 2 ]];then
    usage
    exit 0
fi

run=$1
week=$2
if [[ "$#" == 1 ]];then
    printGpsTime $start $stop
else

    ### compute the number of week available
    if [[ $stop == 'Inf' ]];then 
	unix2gps $(date -u +"%s")
	maxWeek=`echo "scale=0;($gpsTime - $start)/$gpsWeek+1" | bc` 
    else
	maxWeek=`echo "scale=0;($stop - $start)/$gpsWeek+1" | bc` 
    fi

    ### check the the given week is available 
    if [[ $2 -gt $maxWeek ]];then
	echo $1 "only contain " $maxWeek "weeks"
	exit 1
    fi
    
    ### print the gpe time
    Wstart=$(($start + ($2-1)*$gpsWeek))
    printGpsTime $start $stop $Wstart 
fi

exit 0

