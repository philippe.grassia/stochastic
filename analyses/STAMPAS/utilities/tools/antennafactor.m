%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% antennafactor :
% PURPOSE :
%
%
% AUTHOR  : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE    : 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER's PARAMETERS
PSI   = 0:0.1:2*3.1415;
THETA = 0:0.1:3.1415;
IOTA  = 0:0.1:2*3.1415;
IOTA = 0;
X = 0:0.1:1;
Y = 0:0.1:1;
Z = 0:0.1:1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

COLOR = parula(100);

[THETA,PSI] = meshgrid(THETA,PSI);

%[X,Y,Z] = meshgrid(X,Y,Z);

Fp = @(THETA,PSI) 0.5*(1+cos(THETA).^2).*cos(2*PSI).*cos(2*IOTA) + ...
          cos(THETA).*sin(2*PSI).*sin(2*IOTA);
Fx = @(THETA,PSI) 0.5*(1+cos(THETA).^2).*cos(2*PSI).*sin(2*IOTA) + ...
     cos(THETA).*sin(2*PSI).*cos(2*IOTA);

R = abs(Fp(THETA,PSI));
Fp_X = R.*sin(THETA).*cos(PSI);
Fp_Y = R.*sin(THETA).*sin(PSI);
Fp_Z = R.*cos(THETA);

R = abs(Fx(THETA,PSI));
Fx_X = R.*sin(THETA).*cos(PSI);
Fx_Y = R.*sin(THETA).*sin(PSI);
Fx_Z = R.*cos(THETA);

R = sqrt(Fx(THETA,PSI).^2+Fp(THETA,PSI).^2);
Fxp_X = R.*sin(THETA).*cos(PSI);
Fxp_Y = R.*sin(THETA).*sin(PSI);
Fxp_Z = R.*cos(THETA);

%C = COLOR(max(1,ceil(R*100)),:);
figure('Color',[0 0 0],...
       'Units','centimeters',...
       'Position',[0 0 29.7 21],...
       'PaperType','a4',...
       'PaperPositionMode','auto');



RA = 0:0.1:24;
DEC = -90:0.1:90;
%{
for r=1:length(RA)
  for d=1:length(DEC)
    Hp(r,d) = antenna(getdetector('LHO'),7,RA(r),DEC(d),0);
    Hx(r,d) = antenna(getdetector('LHO'),7,RA(r),DEC(d),-pi/4);
    Hxp(r,d) = sqrt(Hp(r,d).^2+Hx(r,d).^2);

    Lp(r,d) = antenna(getdetector('LLO'),7,RA(r),DEC(d),0);
    Lx(r,d) = antenna(getdetector('LLO'),7,RA(r),DEC(d),-pi/4);
    Lxp(r,d) = sqrt(Lp(r,d).^2+Lx(r,d).^2);

    Vp(r,d) = antenna(getdetector('VIRGO'),7,RA(r),DEC(d),0);
    Vx(r,d) = antenna(getdetector('VIRGO'),7,RA(r),DEC(d),-pi/4);
    Vxp(r,d) = sqrt(Vp(r,d).^2+Vx(r,d).^2);
  end
end
%}
fprintf('Compute Antenna Factor ... done\n');


ax11 = axes('Position',[0.05 0.76 0.30 0.23]);
axesm('MapProjection', 'hammer', 'AngleUnits', 'radians');
surfacem(DEC/180*pi,RA/12*pi,Hp,'Parent',ax11);

ax11.Box = 'off';
ax11.XAxisLocation = 'origin';
ax11.YAxisLocation = 'origin';
ax11.Layer = 'top';
ax11.XTick = [-12:6:12]/12*pi;
ax11.XTickLabel = arrayfun(@num2str,mod([12:6:36],24),'UniformOutput',false);
ax11.YTick = [-90:60:90]/180*pi;
ax11.YTickLabel = arrayfun(@num2str,[-90:60:90],'UniformOutput',false);
ax11.Title.Interpreter = 'latex';
ax11.Title.FontSize = 20;
ax11.Title.String = '$F_{+}$';
ax11.Title.Color = 'k';


ax12 = axes('Position',[0.35 0.76 0.30 0.23]);
axesm('MapProjection', 'hammer', 'AngleUnits', 'radians');
surfacem( DEC/180*pi,RA/12*pi,Hx,'Parent',ax12);

ax12.Box = 'off';
ax12.XAxisLocation = 'origin';
ax12.YAxisLocation = 'origin';
ax12.Layer = 'top';
ax12.XTick = [-12:6:12]/12*pi;
ax12.XTickLabel = arrayfun(@num2str,mod([12:6:36],24),'UniformOutput',false);
ax12.YTick = [-90:60:90]/180*pi;
ax12.YTickLabel = arrayfun(@num2str,[-90:60:90],'UniformOutput',false);
ax12.Title.Interpreter = 'latex';
ax12.Title.FontSize = 20;
ax12.Title.String = '$F_{\times}$';
ax12.Title.Color = 'k';

ax13 = axes('Position',[0.65 0.76 0.30 0.23]);
axesm('MapProjection', 'hammer', 'AngleUnits', 'radians');
surfacem( DEC/180*pi,RA/12*pi,Hxp,'Parent',ax13);

ax13.Box = 'off';
ax13.XAxisLocation = 'origin';
ax13.YAxisLocation = 'origin';
ax13.Layer = 'top';
ax13.XTick = [-12:6:12]/12*pi;
ax13.XTickLabel = arrayfun(@num2str,mod([12:6:36],24),'UniformOutput',false);
ax13.YTick = [-90:60:90]/180*pi;
ax13.YTickLabel = arrayfun(@num2str,[-90:60:90],'UniformOutput',false);
ax13.Title.Interpreter = 'latex';
ax13.Title.FontSize = 20;
ax13.Title.String = '$F_{+} + F_{\times}$';
ax13.Title.Color = 'k';



ax21 = axes('Position',[0.05 0.51 0.30 0.23]);
axesm('MapProjection', 'hammer', 'AngleUnits', 'radians');
surfacem( DEC/180*pi,RA/12*pi,Lp,'Parent',ax21);

ax21.Box = 'off';
ax21.XAxisLocation = 'origin';
ax21.YAxisLocation = 'origin';
ax21.Layer = 'top';
ax21.XTick = [-12:6:12]/12*pi;
ax21.XTickLabel = arrayfun(@num2str,mod([12:6:36],24),'UniformOutput',false);
ax21.YTick = [-90:60:90]/180*pi;
ax21.YTickLabel = arrayfun(@num2str,[-90:60:90],'UniformOutput',false);



ax22 = axes('Position',[0.35 0.51 0.30 0.23]);
axesm('MapProjection', 'hammer', 'AngleUnits', 'radians');
surfacem( DEC/180*pi,RA/12*pi,Lx,'Parent',ax22);

ax22.Box = 'off';
ax22.XAxisLocation = 'origin';
ax22.YAxisLocation = 'origin';
ax22.Layer = 'top';
ax22.XTick = [-12:6:12]/12*pi;
ax22.XTickLabel = arrayfun(@num2str,mod([12:6:36],24),'UniformOutput',false);
ax22.YTick = [-90:60:90]/180*pi;
ax22.YTickLabel = arrayfun(@num2str,[-90:60:90],'UniformOutput',false);


ax23 = axes('Position',[0.65 0.51 0.30 0.23]);
axesm('MapProjection', 'hammer', 'AngleUnits', 'radians');
surfacem(DEC/180*pi,RA/12*pi,Lxp,'Parent',ax23);

ax23.Box = 'off';
ax23.XAxisLocation = 'origin';
ax23.YAxisLocation = 'origin';
ax23.Layer = 'top';
ax23.XTick = [-12:6:12]/12*pi;
ax23.XTickLabel = arrayfun(@num2str,mod([12:6:36],24),'UniformOutput',false);
ax23.YTick = [-90:60:90]/180*pi;
ax23.YTickLabel = arrayfun(@num2str,[-90:60:90],'UniformOutput',false);




ax31 = axes('Position',[0.05 0.27 0.30 0.23]);
axesm('MapProjection', 'hammer', 'AngleUnits', 'radians');
surfacem( DEC/180*pi,RA/12*pi,Vp,'Parent',ax31);

ax31.Box = 'off';
ax31.XAxisLocation = 'origin';
ax31.YAxisLocation = 'origin';
ax31.Layer = 'top';
ax31.XTick = [-12:6:12]/12*pi;
ax31.XTickLabel = arrayfun(@num2str,mod([12:6:36],24),'UniformOutput',false);
ax31.YTick = [-90:60:90]/180*pi;
ax31.YTickLabel = arrayfun(@num2str,[-90:60:90],'UniformOutput',false);



ax32 = axes('Position',[0.35 0.27 0.30 0.23]);
axesm('MapProjection', 'hammer', 'AngleUnits', 'radians');
surfacem( DEC/180*pi,RA/12*pi,Vx,'Parent',ax32);

ax32.Box = 'off';
ax32.XAxisLocation = 'origin';
ax32.YAxisLocation = 'origin';
ax32.Layer = 'top';
ax32.XTick = [-12:6:12]/12*pi;
ax32.XTickLabel = arrayfun(@num2str,mod([12:6:36],24),'UniformOutput',false);
ax32.YTick = [-90:60:90]/180*pi;
ax32.YTickLabel = arrayfun(@num2str,[-90:60:90],'UniformOutput',false);




ax33 = axes('Position',[0.65 0.27 0.30 0.23]);
axesm('MapProjection', 'hammer', 'AngleUnits', 'radians');
surfacem( DEC/180*pi,RA/12*pi,Vxp,'Parent',ax33);

ax33.Box = 'off';
ax33.XAxisLocation = 'origin';
ax33.YAxisLocation = 'origin';
ax33.Layer = 'top';
ax33.XTick = [-12:6:12]/12*pi;
ax33.XTickLabel = arrayfun(@num2str,mod([12:6:36],24),'UniformOutput',false);
ax33.YTick = [-90:60:90]/180*pi;
ax33.YTickLabel = arrayfun(@num2str,[-90:60:90],'UniformOutput',false);




% ----

ax41 = axes('Position',[0.1 0.03 0.20 0.23]);
hold all
fill3(Fp_X',Fp_Y',Fp_Z',[.6 .6 .6]);

l=line([0 1],[0 1],[0 0],'LineWidth',3,'Color','b');
l=line([0 1],[0 -1],[0 0],'LineWidth',3,'Color','b');

xlim([-1 1])
ylim([-1 1])
zlim([-1 1])

box
view(45, 45)


ax42 = axes('Position',[0.40 0.03 0.20 0.23]);
hold all
fill3(Fx_X',Fx_Y',Fx_Z',[.6 .6 .6]);

l=line([0 1],[0 1],[0 0],'LineWidth',3,'Color','b');
l=line([0 1],[0 -1],[0 0],'LineWidth',3,'Color','b');

xlim([-1 1])
ylim([-1 1])
zlim([-1 1])

box
view(45, 45)

ax43 = axes('Position',[0.70 0.03 0.2 0.23]);
hold all
fill3(Fxp_X',Fxp_Y',Fxp_Z',[.6 .6 .6]);

l=line([0 1],[0 1],[0 0],'LineWidth',3,'Color','b');
l=line([0 1],[0 -1],[0 0],'LineWidth',3,'Color','b');

xlim([-1 1])
ylim([-1 1])
zlim([-1 1])
box
view(45, 45)


a=annotation('textbox',[0.01 0.97 0.01 0.01],...
             'FontSize',10,...
             'String','LIGO Hanford',...
             'FitBoxToText','on');

annotation('textbox',[0.01 0.73 0.01 0.01],...
           'FontSize',10,...
           'String','LIGO Livingston',...
           'FitBoxToText','on');

annotation('textbox',[0.01 0.45 0.01 0.01],...
           'FontSize',10,...
           'String','Virgo',...
           'FitBoxToText','on');

annotation('textbox',[0.01 0.27 0.01 0.01],...
           'FontSize',10,...
           'String','Antenna factors',...
           'FitBoxToText','on');

annotation('textbox',[0.3 0.97 0.0001 0.0001],...
           'FontSize',20,...
           'String','$F_{+}$',...
           'Interpreter','latex',...
           'FitBoxToText','off');

annotation('textbox',[0.6 0.97 0.0001 0.0001],...
           'FontSize',20,...
           'String','$F_{\times}$',...
           'Interpreter','latex',...
           'FitBoxToText','off');

annotation('textbox',[0.9 0.97 0.0001 0.0001],...
           'FontSize',20,...
           'String','$F$',...
           'Interpreter','latex',...
           'FitBoxToText','off');



fprintf('save figure ...\n');
print('-dpng','../figures/antennafactor.png')