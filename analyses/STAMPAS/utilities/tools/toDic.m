function [] = toDic(fname)  
% toDic :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = toDic ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%
fs = 4096;
w = load(fname);

file = regexprep(fname,'waveforms/','');
name = strsplit(file,'.mat');
letter = regexprep(file,'.*_([a-z]).mat','$1');


%% COMPUTE ENERGY
[hp, hx] = msmagnetar(w.f0,w.tau,w.nn,w.epsilon,w.duration,fs,0,0,w.duration);
msun   = 1.988e30;   % kg
c      = 299792458;  % m s^-1
G      = 6.67e-11;   %
msunc2 = msun*c^2;   % kg m^2 s^-2
pc     = 3.08e16;    % m
r      = 1e6*pc;
dhp    = (diff(hp)*fs);
dhx    = (diff(hx)*fs);
E =  r^2*c^3/G*1/4*sum((9/16*dhp.^2+1/2*dhx.^2)/fs)/msunc2;


fprintf('%s:\n',name{1})
fprintf('  legend: %s\n',['MS Magnetar ' letter])
fprintf('  file: %s\n',file)
fprintf('  fmin: %.2f\n',w.fmin)
fprintf('  fmax: %.2f\n',w.fmsax)
fprintf('  duration: %.2f\n',w.duration)
fprintf('  hrss: %g\n',w.hrss)
fprintf('  energy: %g\n',E)
fprintf('  distance: 1\n')
fprintf('  constructor: msmagnetar_injection\n')
fprintf('  type: onthefly\n')


  function energy(w)
  end
end

