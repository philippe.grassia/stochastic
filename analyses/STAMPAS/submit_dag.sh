#! /usr/bin/env bash

##############################################################
# Script which generates all inputs of a dag for ANTEPROC, BKG, 
# INJ and MDC dags. 
# The dag is launched at the end
#
# 
##############################################################

usage(){
    echo 'Arg 1: GPS start time'
    echo 'Arg 2: GPS end time'
    echo 'Arg 3: ID'
    echo 'Arg 4: Type (PREPROC/BKG/INJ/LONETRACKPPROC)'
    echo 'Arg 5: maxjobs to be used for dag submission'
    echo 'Arg 6: matfilesAvailable (0/1) for PREPROC only'
    exit
}

# Set a trap for error handling
trap "echo -ne \"\nerror $?\n\"; exit $?" ERR

# Path to stamp2 library
ROOT_PATH=`echo $PWD | sed 's/\/STAMPAS//g'`
STAMP2_PATH=${ROOT_PATH}/stamp2

# Hostname
HOST=`hostname -d`

# main
if [[ $# -lt 5 ]]; then
    usage
fi

GPSSTART=$1
GPSEND=$2
ID=$3
TYPE=$4
MAXJOBS=$5
if [[ $# -eq 6 ]];then
    MATFILESAVAILABLE=$6
fi

if [[ ${TYPE} != "INJ" && ${TYPE} != "BKG" &&  ${TYPE} != "PREPROC" && ${TYPE} != "LONETRACKPPROC" ]]; then
    echo 'The 4th option must be INJ, PREPROC, LONETRACKPPROC or BKG'
    exit
fi

if [[ ${TYPE} == "PREPROC" && $# -ne 6 ]]; then
    usage
fi

if [[ ${TYPE} == "INJ" ]]; then
    CONFIGFILE="config_inj.txt"
    FOLDER="INJ"
elif [[ ${TYPE} == "BKG" ]] || [[ ${TYPE} == "PREPROC" ]]; then
    CONFIGFILE="config_bkg.txt"
    FOLDER="BKG"
elif [[ ${TYPE} == "LONETRACKPPROC" ]]; then
    CONFIGFILE="config_lonetrackpproc.txt"
    FOLDER="BKG"
fi

SEARCHPATH=${FOLDER}/Start-${GPSSTART}_Stop-${GPSEND}_ID-${ID}

# Prepare setup_config
if [[ ${TYPE} == "BKG" ]] || [[ ${TYPE} == "LONETRACKPPROC" ]]; then
    if [[ -e "$SEARCHPATH"/APok.txt ]]; then
	firstAnteproc=0
	echo "firstAnteproc $firstAnteproc" >> $SEARCHPATH/${CONFIGFILE}
    else
	echo "You must run with PREPROC option first. Aborting..."
	exit
    fi
else # INJ, PREPROC
    if [[ ! -d ${SEARCHPATH} ]]; then
	mkdir ${SEARCHPATH}
    fi

    # Copy config file
    cp ${CONFIGFILE} ${SEARCHPATH}
    echo "GPS_start ${GPSSTART}" >> ${SEARCHPATH}/${CONFIGFILE}
    echo "GPS_end ${GPSEND}" >> ${SEARCHPATH}/${CONFIGFILE}
    echo "ID $ID" >> ${SEARCHPATH}/${CONFIGFILE}
    echo "Hostname $HOST" >> $SEARCHPATH/${CONFIGFILE}

    if [[ ${TYPE} == "PREPROC" ]]; then   
	if [[ -e "$SEARCHPATH"/APok.txt ]] || [[ -e "$SEARCHPATH"/ok.txt ]]; then
	    echo "PREPROC already completed. Run with BKG option now. Aborting..."
	    exit
	else
	    firstAnteproc=1
	    echo "firstAnteproc $firstAnteproc" >> $SEARCHPATH/${CONFIGFILE}
	fi
    fi
fi

# Seedless algorithm (usefull for compilation but doParallel should be used instead)
SEEDLESS=`grep doSeedless ${CONFIGFILE} | cut -d " " -f 2- | awk 'BEGIN{x=0}{x=x+$1}END{print x}'`

if [[ ${SEEDLESS} == "2" ]]; then
    SEEDLESS=1
else
    SEEDLESS=0
fi

PARALLEL=`grep doParallel ${CONFIGFILE} | cut -d " " -f 2-`

# Matlab initilization
if [[ ${HOST} =~ "atlas" ]]; then
    source matlab_script_2013a.sh
elif [[ ${HOST} =~ "caltech" ]] || [[ ${HOST} =~ "cit" ]]; then
    source matlab_script_2015a.sh
else
    echo "Only designed to work on LIGO and ATLAS clusters. Exiting..."
    exit
fi

# Dag configuration and nitialization
$MATLAB_ROOT/bin/matlab -nodisplay<<EOF
  setup_config('${SEARCHPATH}/${CONFIGFILE}');
EOF


if [[ -e ${SEARCHPATH}/ok.txt ]]; then
    echo "Compilation starts."
    rm "${SEARCHPATH}"/ok.txt
else
    echo "Configuration failed. Submission aborted"
    exit
fi

# Matlab compilation 
if [[ ${TYPE} == "PREPROC" ]]; then #PREPROC (anteproc.m)
    if [[ ${MATFILESAVAILABLE} == 0 ]]; then
	$MATLAB_ROOT/bin/matlab -nodisplay<<EOF
mcc -N -I $STAMP2_PATH -I $MATLAB_ROOT/toolbox/signal/signal -I $MATLAB_ROOT/toolbox/stats/stats -R -nodisplay -R -nojvm -R -singleCompThread -m $STAMP2_PATH/src/anteproc/anteproc.m
EOF
	echo $MATLABPATH
	cp $STAMP2_PATH/src/anteproc/anteproc.m $SEARCHPATH
	cp condor-matlab-init $SEARCHPATH/stampas
	mv anteproc $SEARCHPATH
	rm run_anteproc.sh mccExcludedFiles.log readme.txt
        LD_LIBRARY_PATH=''
# Dag submission
	condor_submit_dag -maxjobs ${MAXJOBS} ${SEARCHPATH}/stamp_allsky_anteproc.dag
    fi
else #BKG & INJ (run_clustermap.m)
    if [[ $SEEDLESS == 1 ]]; then
	if [[ ${PARALLEL} == 1 ]]; then
	    echo 'compilation for stochtrack with doParallel option'
	    $MATLAB_ROOT/bin/matlab -nodisplay<<EOF
mcc -C -I $STAMP2_PATH -I $MATLAB_ROOT/toolbox/signal/signal -I $MATLAB_ROOT/toolbox/stats/stats -I $MATLAB_ROOT/toolbox/images/images -I $MATLAB_ROOT/toolbox/distcomp/parallel -R -nodisplay -R -singleCompThread -a +classes/+waveforms/@dictionary/dic.yml -a +classes/+utils/+yaml/java/snakeyaml-1.9.jar -m functions/run_clustermap.m
mcc -C -I $STAMP2_PATH -I $MATLAB_ROOT/toolbox/signal/signal -I $MATLAB_ROOT/toolbox/stats/stats -I $MATLAB_ROOT/toolbox/images/images -I $MATLAB_ROOT/toolbox/distcomp/parallel -R -nodisplay -R -singleCompThread -a +classes/+waveforms/@dictionary/dic.yml -a +classes/+utils/+yaml/java/snakeyaml-1.9.jar -m functions/run_lonetrackpproc.m
EOF
	    mv run_clustermap.ctf ${SEARCHPATH} 
	else
	    echo 'compilation for stochtrack without doParallel'
	    $MATLAB_ROOT/bin/matlab -nodisplay<<EOF
mcc -N -I $STAMP2_PATH -I $MATLAB_ROOT/toolbox/signal/signal -I $MATLAB_ROOT/toolbox/stats/stats -I $MATLAB_ROOT/toolbox/images/images -I $MATLAB_ROOT/toolbox/distcomp/parallel -R -nodisplay -R -singleCompThread -a +classes/+waveforms/@dictionary/dic.yml -a +classes/+utils/+yaml/java/snakeyaml-1.9.jar -m functions/run_clustermap.m
mcc -N -I $STAMP2_PATH -I $MATLAB_ROOT/toolbox/signal/signal -I $MATLAB_ROOT/toolbox/stats/stats -I $MATLAB_ROOT/toolbox/images/images -I $MATLAB_ROOT/toolbox/distcomp/parallel -R -nodisplay -R -singleCompThread -a +classes/+waveforms/@dictionary/dic.yml -a +classes/+utils/+yaml/java/snakeyaml-1.9.jar -m functions/run_lonetrackpproc.m
EOF
	fi
    else
	echo 'compilation for non stochtrack'
	$MATLAB_ROOT/bin/matlab -nodisplay<<EOF
mcc -N -I $STAMP2_PATH -I $MATLAB_ROOT/toolbox/signal/signal -I $MATLAB_ROOT/toolbox/stats/stats -R -nodisplay -R -singleCompThread -a +classes/+waveforms/@dictionary/dic.yml -a +classes/+utils/+yaml/java/snakeyaml-1.9.jar -m functions/run_clustermap.m
mcc -N -I $STAMP2_PATH -I $MATLAB_ROOT/toolbox/signal/signal -I $MATLAB_ROOT/toolbox/stats/stats -R -nodisplay -R -singleCompThread -a +classes/+waveforms/@dictionary/dic.yml -a +classes/+utils/+yaml/java/snakeyaml-1.9.jar -m functions/run_lonetrackpproc.m
EOF
    fi    

    if [[ ${TYPE} == "INJ" ]]; then
	cp waveforms.txt ${SEARCHPATH}
    fi
    
    cp functions/run_clustermap.m ${SEARCHPATH}
    cp condor-matlab-init ${SEARCHPATH}/stampas
    mv run_clustermap ${SEARCHPATH} 
    rm run_run_clustermap.sh mccExcludedFiles.log readme.txt
    ln -s ../../utilities/interactive/ftmaps.sh ${SEARCHPATH}/ftmap
    cp functions/run_lonetrackpproc.m ${SEARCHPATH}
    mv run_lonetrackpproc ${SEARCHPATH}
    rm run_run_lonetrackpproc.sh
    cp move_results.sh ${SEARCHPATH}   
# Dag submission
    # to avoid error GCLIB message on atlas, LD_LIBRARY_PATH should 
    # be cleared before condor submission
    LD_LIBRARY_PATH=''
    condor_submit_dag -maxjobs ${MAXJOBS} ${SEARCHPATH}/stamp_allsky_${TYPE,,}.dag
fi

# SVN VERSION EXTRACTION
./svnversion.sh $STAMP2_PATH ${SEARCHPATH}
