function js = setScript(obj,js)  
% setScript :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = setScript ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

    s=obj.createElement('script');
    s.setAttribute('type','text/javascript');
    s.setAttribute('charset','utf-8');
    s.setAttribute('src',sprintf('%s',js));
    s.appendChild(obj.createTextNode(''));
    obj.head.appendChild(s);

end

