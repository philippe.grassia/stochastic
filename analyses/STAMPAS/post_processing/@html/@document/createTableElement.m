function table=createTableElement(obj,data,option)  
% createTableElement :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = createTableElement ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

if nargin<3;option='';end

table=obj.createElement('table');

%%% table header
tr=obj.createElement('tr');
for col=1:size(data,2)
    th=obj.createElement('th');
    th.appendChild(obj.createTextNode(sprintf('%s',data(1,col).name)));
    tr.appendChild(th);
end
table.appendChild(tr);

for row=2:size(data,1)
    tr=obj.createElement('tr');
    for col=1:size(data,2)
        td=obj.createElement('td');
        if strcmp(option,'IMG')
            a=obj.createElement('a');            
            a.setAttribute('href',sprintf('%s',data(row,col).name));
            img=obj.createElement('img');
            img.setAttribute('src',sprintf('%s',data(row,col).name));
            a.appendChild(img);
            td.appendChild(a);
        else
            td.appendChild(obj.createTextNode(sprintf('%s',data(row,col).name)));
        end
        tr.appendChild(td);
    end
    table.appendChild(tr);
end


return

