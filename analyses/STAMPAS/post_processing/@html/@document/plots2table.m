function plots2table(html,plot,varargin)  
% plots2table :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = plots2table ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

%------------------------------------------------
%% INPUT PARSER                                                                                         
%------------------------------------------------
p=inputParser;
addRequired(p,'html');
addRequired(p,'plot');
if verLessThan('matlab','8.2')
    addParamValue(p,'Table','default');
    addParamValue(p,'MaxPlots',3,@isnumeric);
    
else
    addParameter(p,'Table','default');
    addParameter(p,'MaxPlots',3,@isnumeric);
    
end
parse(p,html,plot,varargin{:});

html=p.Results.html;
plot=p.Results.plot;
table=p.Results.Table;
maxPlots=p.Results.MaxPlots;

if isempty(html.getElementById(table))            
    table=html.createNode('table','Attribute',...
                          struct('id',table));
    tr=html.createNode('tr','Parent',table);
    td=html.createNode('td','Parent',tr);
    a=html.createNode('a','Attribute',...
                      struct('href',regexprep(plot,'_thumbnail','')),...
                      'Parent',td);
    html.createNode('img','Attribute',...
                    struct('src',plot),...
                    'Parent',a);
else
    table=html.getElementById(table);
    tr=table.getElementsByTagName('tr');
    tr=tr.item(tr.getLength-1);
    td=tr.getElementsByTagName('td');
    if td.getLength>=maxPlots
        tr=html.createNode('tr','Parent',table);
        td=html.createNode('td','Parent',tr);
    else
        td=html.createNode('td','Parent',tr);
    end
    a=html.createNode('a','Attribute',...
                      struct('href',regexprep(plot,'_thumbnail','')),...
                             'Parent',td);
    html.createNode('img','Attribute',...
                    struct('src',plot),...
                    'Parent',a);
end

return

