function [] = setMathJax(obj)  
% setMathJax : add MathJax lib for use latex formula 
% DESCRIPTION :
%    setMathJax is an external library that allow us to use latex
%    syntax directly in html
%
% SYNTAX :
%   [] = setMathJax ()
% 
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jul 2016
%

  s=obj.createElement('script');
  s.setAttribute('type','text/x-mathjax-config');
  s.setAttribute('charset','utf-8');
  s.appendChild(obj.createTextNode(['MathJax.Hub.Config({tex2jax: ' ...
                      '{inlineMath: [[''$'',''$'']]}});']));
  obj.head.appendChild(s);
  
  s=obj.createElement('script');
  s.setAttribute('type','text/javascript');
  s.setAttribute('charset','utf-8');
  s.setAttribute('src',sprintf('https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'));
  s.appendChild(obj.createTextNode(''));
  obj.head.appendChild(s);

end




