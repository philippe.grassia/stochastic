function head = getHead(obj)  
% getHead :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = getHead ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
    head=obj.getElementsByTagName('head');
    head=head.item(0);

end

