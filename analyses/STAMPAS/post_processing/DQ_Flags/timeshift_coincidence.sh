#! /usr/bin/env bash

# functions
usage(){
    echo 'Arg 1: RUN'
    echo 'Arg 2: DAG path '
    echo 'Arg 3: DAG ID '
    echo 'Arg 4: IFO1'
    echo 'Arg 5: IFO2'
    exit
}

# main
[[ $# -ne 5 ]] && usage

RUN=$1
DAG=$2
ID=$3
IFO1=$4
IFO2=$5


echo 'Generating report page for run ' $RUN $ID $IFO1 $IFO2 ' dag ' $DAG 

folder=${RUN}_ID${ID}
filename_prefix=${RUN}_ID${ID}
results=${RUN}'_ID'${ID}'_results'

if [ ! -d ${folder} ]; then
    mkdir ${folder}
fi

#
echo 'generating the loudest events list ...'
#../../background_estimation/get_loudest_events.sh ${DAG} --SNRfrac .5 -o loudest.txt
cp loudest.txt ${folder}/${folder}.txt

#
echo 'shifting H1 and L1 GPS times ...'
#matlab -nodisplay -r "timeshift_triggers ${IFO1} ${IFO2} ${folder}/${folder};exit"

#
echo 'intersecting each time shifted triggers list with all DQ flags ...'
for ifo in $IFO1 $IFO2; do
    ifo_nomatching=0
    for shift in -400 -350 -300 -250 0 250 300 350 400; do 
        echo $ifo $shift
        
        DQFiles=`ls allDQs_Science/${RUN}/$ifo*`
        FILENAME=${folder}/${filename_prefix}'_'${ifo}'_'$shift'.txt'
        
        for dq in $DQFiles; do
#	    echo $dq
            INT=`segexpr 'intersection('$FILENAME','$dq')'`
            if [ -n "$INT" ]
            then
                dqRep=`echo $dq | cut -d '/' -f 3 | cut -d "." -f 1`
                if [ ! -f $results/$dqRep ]; then
 		    mkdir -p $results/$dqRep
                fi
# 		echo $results/$dqRep/shift_$shift.txt
                segexpr 'intersection('$FILENAME','$dq')' > $results/$dqRep/shift_$shift.txt
                rc=`grep "No matching file" $results/$dqRep/shift_$shift.txt`
                if [[ ${#rc} -gt 0 ]]; then
		    ifo_nomatching=$((${ifo_nomatching} + 1))
		    echo $ifo ' segexpr pbs found: ' ${ifo_nomatching}
                fi
            fi
        done
        
    done
done

rc=`grep matching $results/*/*`
if [ ${#rc} -gt 0 ]; then
    echo 'Some of the DQ files or the shifted lodeust events files are not found. Please check in '$results
else
    echo 'Coincidences seem OK'
fi
