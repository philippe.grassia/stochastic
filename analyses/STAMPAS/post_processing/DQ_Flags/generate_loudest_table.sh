#! /usr/bin/env bash

# functions
usage(){
    echo 'Missing arguments'
    echo 'Arg 1: run [S5/S6]'
    echo 'Arg 2: ID'
    exit
}

[[ $# -ne 2 ]] && usage

RUN=$1
ID=$2

FOLDER=${RUN}_ID${ID}

LOUDEST_NB=`wc -l ${FOLDER}/${FOLDER}.txt | awk '{print $1}'`

cat ${FOLDER}/${FOLDER}.txt  | awk '{printf("%d %7.2f %12.2f %12.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %5.3f\n",NR,$6, $1+($7+$8)/2.,$12+($7+$8)/2,$8-$7,$13,$4,$5,$2,$3,$9)}' > tmp.txt

cat ${FOLDER}/${FOLDER}.txt | awk '{printf("%f %f \n", $3, $3+$5)}' > H1_times.txt
cat ${FOLDER}/${FOLDER}.txt | awk '{printf("%f %f \n", $4, $4+$5)}' > L1_times.txt

echo '| *ID* | *SNR* | *GPS H1* | *GPS L1* | *Dur. [s]* | *Lag #* | *f_min [Hz]* | *f_max [Hz]* | *RA [hr]* | *dec [deg.]* | *tau [ms]* | *SNR_frac* | *DQ H1* | *DQ L1* |' > ${FOLDER}_table.txt

cat ${FOLDER}/${FOLDER}.txt | awk '{print "|"$1 "|"$2 "|"$3 "|"$4 "|"$5 "|"$6 "|"$7 "|"$8 "|"$9 "|"$10 "|"$11 "|" $12 "|["$3"DQH1]|["$4"DQL1]|"}' >> ${FOLDER}_table.txt 

mv ${FOLDER}_table.txt table_tmp1

echo 'extracting DQ information for H1 ...'
cp ${FOLDER}/H1_veto.txt vetofile
for i in $(seq 1 $LOUDEST_NB); do
    cat H1_times.txt | awk -v ind=$i '{if (NR==ind) print $0}' > tmp
    gps=`cat tmp | awk '{printf("%10.2f\n", $1)}'`
    a=`segexpr 'intersection(tmp, vetofile)'`

    if [ ${#a} -gt 0 ]; then
#	echo $gps 'YES'
	sed -e "s|\[${gps}DQH1\]|YES|g" table_tmp1 > table_tmp2
	cp table_tmp2 table_tmp1
    else
#	echo $gps ' '
	sed -e "s|\[${gps}DQH1\]| |g" table_tmp1 > table_tmp2
	cp table_tmp2 table_tmp1
    fi
done


echo 'extracting DQ information for L1 ...'
cp ${FOLDER}/L1_veto.txt vetofile
for i in $(seq 1 $LOUDEST_NB); do
    cat L1_times.txt | awk -v ind=$i '{if (NR==ind) print $0}' > tmp
    gps=`cat tmp | awk '{printf("%10.2f\n", $1)}'`
    a=`segexpr 'intersection(tmp, vetofile)'`

    if [ ${#a} -gt 0 ]; then
#	echo $gps ' YES'
	sed -e "s|\[${gps}DQL1\]|YES|g" table_tmp1 > table_tmp2
	cp table_tmp2 table_tmp1
    else
#	echo $gps ' '
	sed -e "s|\[${gps}DQL1\]| |g" table_tmp1 > table_tmp2
	cp table_tmp2 table_tmp1
    fi
done

mv table_tmp1 ${FOLDER}/loudest_events.table


rm H1_times.txt
rm L1_times.txt
rm table_tmp2
rm tmp.txt
rm tmp
rm vetofile


