#!/usr/bin/env bash

###########################################
# This script identifies which events are vetoed
# by a given flag. It takes as input a 
# list of DQ files and generates a table that 
# can be inserted in the wiki
#
# Contact: M. A. Bizouard (mabizoua@lal.in2p3.fr)
###########################################

# functions
usage(){
    echo 'Missing arguments'
    echo 'Arg 1: run [S5/S6/O1/O2]'
    echo 'Arg 2: ID'
    echo 'Arg 3: IFO [H1/L1]'
    echo 'Arg 4: threshold on eff/deadtime'
    echo 'Arg 5: threshold on sigma number'
    echo 'Arg 6: threshold on 1000*eff/segment#'
    exit
}

[[ $# -ne 6 ]] && usage

RUN=$1
ID=$2
IFO=$3
THR_ED=$4
THR_SIG=$5
THR_ESN=$6

FOLDER=${RUN}_ID${ID}

filename=${RUN}_${IFO}_bestFlags.txt
output=${FOLDER}/${RUN}_${IFO}.table

LOUDEST_NB=`wc -l ${FOLDER}/${FOLDER}.txt | awk '{print $1}'`

echo 'Applying the final selection criteria on the pre-selected' $RUN ' ' $IFO ' flags ...'

### INPUT LIST

triggers=${FOLDER}/${FOLDER}.txt
if [ $IFO == "H1" ]; then
    cat $triggers | awk '{printf("%f %f \n", $3, $3+$5)}' > H1_times.txt
fi
if [ $IFO == "L1" ]; then
    cat $triggers | awk '{printf("%f %f \n", $4, $4+$5)}' > L1_times.txt
fi

if [ ${RUN} == "S5" ]; then
    SCIENCE_FILENAME='allDQs_Science/'${RUN}'/'${IFO}':Science.txt'
elif [ ${RUN} == "S6" ]; then
    SCIENCE_FILENAME='allDQs_Science/'${RUN}'/'${IFO}':DMT-SCIENCE.txt'
elif [ ${RUN} == "O1" ]; then
    SCIENCE_FILENAME='allDQs_Science/'${RUN}'/'${IFO}':DMT-ANALYSIS_READY.txt'
elif [ ${RUN} == "O2" ]; then
    SCIENCE_FILENAME='allDQs_Science/'${RUN}'/'${IFO}':DCH-CLEAN_SCIENCE_C02.txt'
else
    echo 'Add science flag names for this run'
fi

science_time=`cat ${SCIENCE_FILENAME} | awk 'BEGIN{sum=0}{sum=sum+$2-$1}END{print sum}'`
declare -a array_triggers
if [ -f tmp ]; then
    rm tmp
fi

col_number=`cat ${FOLDER}/$filename | awk '{print NF}' | tail -1`
if [ $col_number -lt 10 ]; then
    echo ${FOLDER}/$filename ' does not contain 10 columns but only ' $col_number
    echo 'Is that the right file?'
    exit 1
fi

if [ -f $FOLDER/${RUN}_${IFO}_DQ_1.txt ]; then
    rm $FOLDER/${RUN}_${IFO}_DQ_1.txt
fi

if [ -f $FOLDER/${RUN}_${IFO}_DQ_2.txt ]; then
    rm $FOLDER/${RUN}_${IFO}_DQ_2.txt
fi

if [ -f $FOLDER/${RUN}_${IFO}_DQ_3.txt ]; then
    rm $FOLDER/${RUN}_${IFO}_DQ_3.txt
fi

if [ -f $FOLDER/${RUN}_${IFO}_DQ_4.txt ]; then
    rm $FOLDER/${RUN}_${IFO}_DQ_4.txt
fi

cat ${FOLDER}/$filename | sort -k 6 -g -r | awk '{print $10}' > tmp.txt
cat ${FOLDER}/$filename | sort -k 6 -g -r > tmp_full.txt

LIST=`cat tmp.txt`
nb=`wc -l tmp.txt | awk '{print $1}'`
k=1

echo '| *DQ flag* | *dt[%]* | *eff/dt* | *seg #* | *dt/seg #* | * 1000*eff/seg #* | *sigma #* | *Events vetoed* |' > $output
for dq in $LIST
do
    echo $k "/" $nb ":" $dq
    veto_time=`cat allDQs_Science/${RUN}/${dq}.txt | awk 'BEGIN{sum=0}{sum=sum+$2-$1}END{print sum}'`
    dt=`echo "scale=2; $veto_time / $science_time * 100" | bc`
    j=1
    
    for i in $(seq 1 $LOUDEST_NB); do
 	cat ${IFO}_times.txt | awk -v ind=$i '{if (NR==ind) print $0}' > tmp
 	INT=`segexpr "intersection(allDQs_Science/${RUN}/${dq}.txt,tmp)"`
 	if [ -n "$INT" ]; then
 	    gps=`cat tmp | awk '{printf("%10.1f\n", ($1+$2)/2)}'`
 	    array_triggers[j]=$i
 	    j=$(($j+1))
#	    echo 'trigger ' $i ' ' $gps
 	fi
    done

    values=`awk -v thr_ed=${THR_ED} -v thr_sig=${THR_SIG} -v thr_esn=${THR_ESN} -v line=$k '{
	if (NR==line)
	    if ($6 < thr_ed)
      	        print " %BLUE%"$5"%ENDCOLOR% | %BLUE%"$6"%ENDCOLOR% | %BLUE%"$7"%ENDCOLOR% | %BLUE%"$9"%ENDCOLOR% | %BLUE%"$8"%ENDCOLOR% | %BLUE%"$11"%ENDCOLOR% " ;
	    else
                if ( $11 < thr_sig )
                    print " %RED%"$5"%ENDCOLOR% | %RED%"$6"%ENDCOLOR% | %RED%"$7"%ENDCOLOR% | %RED%"$9"%ENDCOLOR% | %RED%"$8"%ENDCOLOR% | %RED%"$11"%ENDCOLOR% " ;
		else
	            if ($8 < thr_esn)
      		        print " %GREEN%"$5"%ENDCOLOR% | %GREEN%"$6"%ENDCOLOR% | %GREEN%"$7"%ENDCOLOR% | %GREEN%"$9"%ENDCOLOR% | %GREEN%"$8"%ENDCOLOR% | %GREEN%"$11"%ENDCOLOR% " ;
		    else
	      	        print $5 " | " $6 " | " $7 " | " $9 " | " $8 " | " $11;
                    fi
		fi
            fi
	fi
	}' tmp_full.txt`

    echo $dq >> $FOLDER/${RUN}_${IFO}_DQ_1.txt

    if [[ "$values" == *"BLUE"* ]]; then
	echo '| %BLUE%' $dq '%ENDCOLOR% |' $values '| %BLUE%' ${array_triggers[*]} '%ENDCOLOR% |' >> $output
    else 
	echo $dq >> $FOLDER/${RUN}_${IFO}_DQ_2.txt

	if [[ $values == *"RED"* ]]; then
	    echo '| %RED%' $dq '%ENDCOLOR% |' $values '| %RED%' ${array_triggers[*]} '%ENDCOLOR% |' >> $output
	else
	    echo $dq >> $FOLDER/${RUN}_${IFO}_DQ_3.txt

	    if [[ $values == *"GREEN"* ]]; then
		echo '| %GREEN%' $dq '%ENDCOLOR% |' $values '| %GREEN%' ${array_triggers[*]} '%ENDCOLOR% |' >> $output
	    else
		echo '|' $dq '|' $values '|' ${array_triggers[*]} '|' >> $output
		echo $dq >> $FOLDER/${RUN}_${IFO}_DQ_4.txt
	    fi
	fi
    fi

    unset array_triggers
    k=$(($k+1))
done

rm tmp.txt
rm tmp_full.txt
rm ${IFO}_times.txt


