DQ studies
Written by: S. Franco (franco@lal.in2p3.fr)

Last modified: 08/11/2014 by Marie Anne Bizouard (mabizoua@lal.in2p3.fr)

PRINCIPLE

In this repertory are found all the necessary scripts to
select the DQ flags that are useful for the all-sky stamp search.
The principle is to compare the number of triggers vetoed by a DQ flag
to the number of time-shifted triggers vetoed by the DQ flag. 
Time shifts introduced in the triggers' lists allow to estimate the
number of accidental coincidences. Low accidental coincidences vetoes 
are selected.

These script helps the user to classify the DQ flags by plotting
the number of coincidences between the DQ on times, and both genuine
and time-shifted trigger lists.

NOTE: The DQ lists for S5 & S6 are stored in a tarball in SVN. The tarball 
is prepared by hand using the script allDQs/get_DQ.sh.

HOW T0

1. Download DQ flags (get_DQ.sh) or untar allDQs/S5.tar or allDQs/S6.tar

1.bis: get UPV vetoes with get_UPV_veto.sh (see explanation inside script)

2. Run get_DQ_in_science.sh script to intersect DQ segments with 
SCIENCE segments

3. Run timeshift_coincidence.sh. The script is:
i/   extracting the SNR>30 loudest background triggers
ii/  extracting H1 and L1 GPS times
iii/ generating different time-shifted triggers lists for H1 and L1
iv/  intersect time shifted triggers lists with all DQ flags for both IFOs.

It makes use of the timeshift_triggers.m script to extract an time ordered 
lists of GPS for both IFOs.

4. Run select_DQs.sh
That script will select the flags, write them in a file and generate the veto lists
At that level, one can add some more criteria to select the flags that are used for vetoes.
The script can be thus run many times. 
i/   creates the coincidences vs. timeshfit plots for each DQ. 
ii/  writes in <folder>/results_all/loose/tight.txt a line for each "good" DQ flag according to different criteria
iii/ copies figures in ~public_html

5. Run generate_wiki.sh
The script runs:
i/   downselect_flags.sh for all IFOs
ii/  compute deadtimes for all selection criteria
iii/ generate the table for loudest events
iv/  generates veto lists

In the end all results are in folder <run>_<ID>

Especially 

- 3 tables:
loudest_events.table
O2_H1.table
O2_L1.table

- 1 wiki:
index.wiki

2 veto files:
<IFO1>_veto.txt
<IFO2>_veto.txt


