#!/usr/bin/env bash

## This script loops over all the data quality flags files
## and interesect them with the science segments
## (useful to calculate dead time)
## Currently set for H1 L1.
##
## Written by: S. Franco (franco@lal.in2p3.fr)
## Modified by Marie Anne Bizouard (mabizoua@lal.in2p3.fr) 08/11/2014
##########################################################

eval `/ligotools/bin/use_ligotools` 

usage(){
    echo 'Arg 1: run name [S5/S6/O1/O2]'
    exit
}

[[ $# -ne 1 ]] && usage

run=$1

if [ ! -d allDQs_Science/${run} ]; then
    mkdir -p allDQs_Science/${run}
fi

for ifo in 'H' 'L'; do
    echo $ifo
    if [ ${run} == S5 ]; then
        science_dq_name=${ifo}1\:Science
    elif [ ${run} == S6 ]; then
        science_dq_name=${ifo}1\:DMT-SCIENCE
    elif [ ${run} == O1 ]; then
        science_dq_name=${ifo}1\:DMT-ANALYSIS_READY
    elif [ ${run} == O2 ]; then
#        science_dq_name=${ifo}1\:DCS-ANALYSIS_READY_C02   #C02
        science_dq_name=${ifo}1\:DCH-CLEAN_SCIENCE_C02   #C02 cleaned
    fi
    
    DQFiles=`ls allDQs/${run}/${ifo}[0-1]*`

    for dq in $DQFiles
    do
        echo $dq
        dqRep=`echo $dq | cut -d '/' -f 3`
        segexpr 'intersection('allDQs/${run}/${science_dq_name}.txt','$dq')' > allDQs_Science/${run}/$dqRep
    done
done

