#!/usr/bin/env bash

###########################################
# This script creates the total list of
# GPS times to veto. It takes as input a 
# list of DQ files, located for instance in
# the allDQs repertory.
#
# Written by: S. Franco (franco@lal.in2p3.fr)
###########################################

if [ $# -lt 3 ]; then
    echo 'Mssing arguments'
    echo 'Arg 1: run [S5/S6/O1/O2]'
    echo 'Arg 2: IFO [H1/L1]'
    echo 'Arg 3: input file that contains the selected flags'
    echo '[Arg 4: veto segment filename]'
    exit
fi
RUN=$1
IFO=$2
filename=$3
veto_save=0

if [ $# -eq 4 ]; then
   veto_filename=$4
   veto_save=1
fi

#echo 'Generating veto list for ' $RUN ' ' $IFO ' ...'

### INPUT LIST
LIST=`cat $filename`

### OUTPUT LIST
OUTPUT=${RUN}'_'${IFO}'_veto.txt'

if [ ${RUN} == "S5" ]; then
    SCIENCE_FILENAME='allDQs_Science/'${RUN}'/'${IFO}':Science.txt'
elif [ ${RUN} == "S6" ]; then
    SCIENCE_FILENAME='allDQs_Science/'${RUN}'/'${IFO}':DMT-SCIENCE.txt'
elif [ ${RUN} == "O1" ]; then
    SCIENCE_FILENAME='allDQs_Science/'${RUN}'/'${IFO}':DMT-ANALYSIS_READY.txt'
elif [ ${RUN} == "O2" ]; then
    SCIENCE_FILENAME='allDQs_Science/'${RUN}'/'${IFO}':DCH-CLEAN_SCIENCE_C02.txt'
else
    echo 'Add science flag names for this RUN'
fi

science_time=`cat ${SCIENCE_FILENAME} | awk 'BEGIN{sum=0}{sum=sum+$2-$1}END{print sum}'`


j=1
for dq in $LIST
do
    if [ $j == 1 ]; then
	cp allDQs_Science/${RUN}/${dq}.txt tmp1
    else
	segexpr "union(allDQs_Science/${RUN}/${dq}.txt,tmp1)" | sort > tmp
	cp tmp tmp1
    fi
    veto_time=`cat tmp1 | awk 'BEGIN{sum=0}{sum=sum+$2-$1}END{print sum}'`
    dt=`echo "scale=5; $veto_time / $science_time" | bc`
#    echo $dq $veto_time $dt

    j=$(($j+1))
done

sort tmp | uniq > tmp1
#cp tmp1 ${OUTPUT}_nounion
segexpr "union(tmp1,tmp1)" | sort > tmp

sort tmp | uniq > $OUTPUT

veto_time=`cat $OUTPUT | awk 'BEGIN{sum=0}{sum=sum+$2-$1}END{print sum}'`
dt=`echo "scale=5; $veto_time / $science_time" | bc`
seg_nb=`wc -l $OUTPUT | cut -d " " -f 1`
echo 'Veto duration= '$veto_time 's. Science duration= ' $science_time 's. Deadtime=' $dt ' segments nb: ' $seg_nb

if [ $veto_save == "1" ]; then
    echo $veto_time $science_time $dt $seg_nb > ${veto_filename}
fi

rm tmp
rm tmp1



