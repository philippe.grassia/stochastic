# This script is 
# 1/ getting the list of all flags for a given run/time period
# 2/ download flags' segments
#
# S6 time periods and run name are hard-coded for S6
#
# Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
#

# functions
usage(){
    echo 'Arg 1: run name [S5/S6/O1/O2]'
    exit
}

[[ $# -ne 1 ]] && usage

run=$1

if [ $run == S5 ]; then
    GPS_START=814743552
    GPS_END=875232014
    filename=S5.xml
    DATABASE=https://metaserver.phy.syr.edy
elif [ $run == S6 ]; then
    GPS_START=931035313
    GPS_END=971557962
    filename=S6A-S6B-S6C-S6D.xml
    DATABASE=https://segdb.ligo.caltech.edu
elif [ $run == O1 ]; then
    GPS_START=1126073342
    GPS_END=1136649617
    filename=O1.xml
    DATABASE=https://segments.ligo.org
elif [ $run == O2 ]; then
    GPS_START=1164067217           
    GPS_END=1187733618
    filename=O2.xml
    DATABASE=https://segments.ligo.org
else
    echo 'run name is not recohnized'
    exit
fi

if [ ! -d allDQs ]; then    
    mkdir allDQs
    mkdir allDQs/${run}
fi
echo $DATABASE
echo $GPS_START
echo $GPS_END

echo 'Get DQ names'
ligolw_segment_query_dqsegdb --show-types --segment-url=$DATABASE --gps-start-time $GPS_START --gps-end-time $GPS_END > allDQs/${filename}

echo 'Get DQ names in txt'
ligolw_print -t show_types_result:table:table -c ifos -c name -d " " allDQs/${filename} | awk '{if ($1!= "V1") print $0}' | awk '{if ($1!= "G1") print $0}' | grep -v ame | sort | uniq > allDQs/${run}_H1L1_names.txt

echo 'Loop over names'
for i in `cat allDQs/${run}_H1L1_names.txt | awk '{print $1":"$2}'`; do
    echo $i
    ligolw_segment_query_dqsegdb --query-segments --segment-url=$DATABASE --gps-start-time $GPS_START --gps-end-time $GPS_END --include-segments $i | ligolw_print -t segment:table -c start_time -c end_time -d " " | sort > allDQs/${run}/$i.txt
done

