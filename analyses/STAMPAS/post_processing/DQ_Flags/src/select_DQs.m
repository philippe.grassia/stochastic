function select_DQs (folder_suffix, run, verbose)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For each DQ fshifts studies, plot the number
% of coincidences with triggers list with respect
% to the time shift applied on them, centered on 
% the original list.
%
% M A Bizouard (mabizoua@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Directory where the intersection between DQ
%% lists and trigger lists are saved
directory=[folder_suffix '_results'];

%% Shifts to use forr the plots
shifts=[-400,-350,-300,-250,0,250,300,350,400];

%% Figures are saved in:
saveDir=[folder_suffix '_figures'];

if (exist(saveDir,'dir')~=7)
  mkdir(saveDir);
else
  rmdir(saveDir,'s');
  mkdir(saveDir); 
end


switch run
 case 'S5'
  L1_science=load(['allDQs_Science/' char(run) '/L1:Science.txt']);
  H1_science=load(['allDQs_Science/' char(run) '/H1:Science.txt']);
 case 'S6'
  L1_science=load(['allDQs_Science/' char(run) '/L1:DMT-SCIENCE.txt']);
  H1_science=load(['allDQs_Science/' char(run) '/H1:DMT-SCIENCE.txt']);
 case 'O1'
  L1_science=load(['allDQs_Science/' char(run) '/L1:DMT-ANALYSIS_READY.txt']);
  H1_science=load(['allDQs_Science/' char(run) '/H1:DMT-ANALYSIS_READY.txt']);
 case 'O2'
  L1_science=load(['allDQs_Science/' char(run) '/L1:DCH-CLEAN_SCIENCE_C02.txt']);
  H1_science=load(['allDQs_Science/' char(run) '/H1:DCH-CLEAN_SCIENCE_C02.txt']);
 otherwise
  display('Add science flag names for this run')
  exit
end

sum_L1_science=sum(L1_science(:,2)-L1_science(:,1));
sum_H1_science=sum(H1_science(:,2)-H1_science(:,1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dqs=dir(directory);

fid1 = fopen([folder_suffix '/results_loose.txt'], 'w');
fid2 = fopen([folder_suffix '/results_tight.txt'], 'w');
fid3 = fopen([folder_suffix '/results_all.txt'], 'w');

display ('Selecting DQs ....');
for (i=3:size(dqs,1))
  display(dqs(i).name);
  if size(strfind(dqs(i).name,'DMT-SCIENCE'),1)>0 | ...
	size(strfind(dqs(i).name,'Science'),1)>0 | ...
	size(strfind(dqs(i).name,'DMT-ANALYSIS_READY'),1)>0 | ...
	size(strfind(dqs(i).name,'CLEAN_SCIENCE'),1)>0
    continue
  end

  ifo=dqs(i).name(1:2);
  dq=load(['allDQs_Science/' char(run) '/' dqs(i).name '.txt']);
  if size(dq,1)==0
    display([dqs(i).name ' is empty']);
    continue
  end
  sum_dq=sum(dq(:,2)-dq(:,1));
  dq_seg_nb=size(dq,1);
  
  if strcmp(ifo,'L1')==1
    deadtime=sum_dq/sum_L1_science;
  else
    deadtime=sum_dq/sum_H1_science;
  end

  numTrigs=zeros(1,length(shifts));   
  for (j=1:length(shifts))
    prefix='';
    filename=[directory '/' dqs(i).name '/shift_' prefix num2str(shifts(j)) '.txt'];
    if exist(filename)~=2
      numTrigs(j)=0;
    else
      [a,b]=system(['wc -l ' filename '| cut -d " " -f 1']);
      numTrigs(j)=str2num(b);
    end
  end

  % Plots
  plot(shifts,numTrigs,'+-b');
  shortName=dqs(i).name(1:end);
  title(strrep(shortName,'_','\_')); 
  xlabel('Time shift [s]');
  ylabel('Evts nb');

  
  % Save all results
  fprintf (fid3, '%d \t %7.2f \t %7.2f \t %d \t %5.2f \t %5.2f \t %8d \t %5.2f \t %5.2f \t %s \n', ...
	   numTrigs(5), mean(numTrigs([1,2,3,4,6,7,8,9])), std(numTrigs([1,2,3,4,6,7,8,9])), ...
	   max(numTrigs([1,2,3,4,6,7,8,9])), deadtime*100, ...
	   numTrigs(5)/deadtime/100., dq_seg_nb, 1000*numTrigs(5)/dq_seg_nb, ...
	   1000000*deadtime/dq_seg_nb, shortName);
  
  % DQs selection
  % Loose
  if numTrigs(5)>0 & (numTrigs(5)>mean(numTrigs([1,2,3,4,6,7,8,9]))+2) & (numTrigs(5)>max(numTrigs([1,2,3,4,6,7,8,9]))) ...
  | numTrigs(5)>0 & (mean(numTrigs([1,2,3,4,6,7,8,9]))==0)
  
    fprintf (fid1, '%d \t %7.2f \t %7.2f \t %d \t %5.2f \t %5.2f \t %8d \t %5.2f \t %5.2f \t %s \n', ...
	     numTrigs(5), mean(numTrigs([1,2,3,4,6,7,8,9])), std(numTrigs([1,2,3,4,6,7,8,9])), ...
	     max(numTrigs([1,2,3,4,6,7,8,9])), deadtime*100, ...
	     numTrigs(5)/deadtime/100., dq_seg_nb, 1000*numTrigs(5)/dq_seg_nb, ...
	     10000000*deadtime/dq_seg_nb, shortName);
    
    
    if verbose==1
      display ([shortName ' ' num2str(numTrigs(5)) ' ' ...
		num2str(numTrigs(5)-sqrt(numTrigs(5))) ' ' ...
		num2str(mean(numTrigs([1,2,3,4,6,7,8,9]))) ' ' ...
		num2str(max(numTrigs([1,2,3,4,6,7,8,9]))) ' ' ...
		num2str(deadtime*100) ' ' ...
		num2str(10000000*deadtime/dq_seg_nb)]);
    end
  end
  % Tight
  if numTrigs(5)>0 & numTrigs(5)>mean(numTrigs([1,2,3,4,6,7,8,9])) ...
	+ std(numTrigs([1,2,3,4,6,7,8,9])) & deadtime < .20
    if std(numTrigs([1,2,3,4,6,7,8,9]))>0
      fprintf (fid2, '%d \t %7.2f \t %7.2f \t %d \t %5.2f \t %5.2f \t %8d \t %5.2f \t %5.2f \t %s \t %5.2f\n', ...
	       numTrigs(5), mean(numTrigs([1,2,3,4,6,7,8,9])), std(numTrigs([1,2,3,4,6,7,8,9])), ...
	       max(numTrigs([1,2,3,4,6,7,8,9])), deadtime*100, ...
	       numTrigs(5)/deadtime/100., dq_seg_nb, 1000*numTrigs(5)/dq_seg_nb, ...
	       10000000*deadtime/dq_seg_nb, shortName, ...
	       (numTrigs(5)-mean(numTrigs([1,2,3,4,6,7,8,9])))/std(numTrigs([1,2,3,4,6,7,8,9])));
    else
      fprintf (fid2, '%d \t %7.2f \t %7.2f \t %d \t %5.2f \t %5.2f \t %8d \t %5.2f \t %5.2f \t %s \t %5.2f\n', ...
	       numTrigs(5), mean(numTrigs([1,2,3,4,6,7,8,9])), std(numTrigs([1,2,3,4,6,7,8,9])), ...
	       max(numTrigs([1,2,3,4,6,7,8,9])), deadtime*100, ...
	       numTrigs(5)/deadtime/100., dq_seg_nb, 1000*numTrigs(5)/dq_seg_nb, ...
	       10000000*deadtime/dq_seg_nb, shortName, 0);
    end

    saveas(gcf,[saveDir '/' shortName],'png');

  end
  
end

fclose(fid1);
fclose(fid2);
fclose(fid3);
exit

