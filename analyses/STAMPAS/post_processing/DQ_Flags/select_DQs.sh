#! /usr/bin/env bash

# functions
usage(){
    echo 'Arg 1: dag name'
    echo 'Arg 2: RUN'
    echo 'Arg 3: search name [<run>_allsky]'
    echo 'Arg 4: folder suffix [<run>_ID<ID>]'
    echo 'Arg 5: folder name of the final destination of the figures in $USER/public_html/<search>/<dag>/]'
    echo '       if NULL, no copy made'
    exit
}

# main
[[ $# -ne 5 ]] && usage

DAG=$1
RUN=$2
SEARCH=$3
folder_suffix=$4
folder_copy=$5

if [[ ${folder_suffix} != *"_ID"* ]]; then
    echo ' The third argument must be follow the syntax: [<run>_ID<ID>_<loudest triggers nb>]'
    exit
fi


matlab -nodisplay -r "select_DQs ${folder_suffix} ${RUN} 0"

path=/home/$USER/public_html/${SEARCH}/${DAG}/${folder_copy}

if [ ${folder_copy} != "NULL" ]; then
    if [ ! -e ${path} ]; then
	mkdir -p ${path}
    else
	[ "$(ls -A ${path})" ] && echo 'Folder ' ${path} ' will be cleaned'; rm ${path}/* || echo "Do nothing"
    fi
    cp ${folder_suffix}_figures/* ${path}/.
fi

cat ${folder_suffix}/results_tight.txt | grep "L1:" > ${folder_suffix}/${RUN}_L1_bestFlags.txt
cat ${folder_suffix}/results_tight.txt | grep "H1:" > ${folder_suffix}/${RUN}_H1_bestFlags.txt

