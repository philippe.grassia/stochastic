function hist33 (x, y, nbin_x, nbin_y, varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AUTHOR: MAB
% DATE: 12/03/2006
% SUBJECT: 3D histograms
% LAST RELEASE:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INPUTS:
% x - vector of x inputs
% y - vector of y inputs
% OUTPUTS:
% none - Graph only
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  x = EVTSASS(1:end,4);
%  y = EVTSASS(1:end,3);
%  nbin_x = 100;
%  nbin_y = 100;

k=1;
intbinsFlagX = 0;
intbinsFlagY = 0;

while k <= nargin-4
  if ischar(varargin{k})
    switch lower(varargin{k})
     case {'intbins'}
      intbinsFlagX = 1;
      intbinsFlagY = 1;
    
     case {'intbinsx'}
      intbinsFlagX = 1;
      intbinsFlagY = 0;

     case {'intbinsy'}
      intbinsFlagY = 1;
    end
  end
  k=k+1;
end

  x_max = max(x);
  x_min = min(x);
  y_max = max(y);
  y_min = min(y);
  
  x_range = x_max - x_min;
  y_range = y_max - y_min;
  if intbinsFlagX
    x_delta = ceil(x_range / (nbin_x-1));
    nbin_x=ceil((x_max-x_min)/x_delta);
  else
    x_delta = x_range / (nbin_x-1);
  end
  
  if x_delta==0
    warning('Not enough x bins');
    return;
  end
  
  if intbinsFlagY
    y_delta = ceil(y_range / (nbin_y-1));
    nbin_y=ceil((y_max-y_min)/y_delta);
  else
    y_delta = y_range / (nbin_y-1);
  end

  if y_delta==0
    warning('Not enough y bins');
    return;
  end
  
  clear map
  map (nbin_x, nbin_y) = 0;

  for (i=1:length(x))
%    display(num2str(y(i)))
    x_bin = ceil((x(i)-x_min)/x_delta);
    if x_bin<nbin_x
      x_bin=x_bin+1;
    end
    y_bin = ceil((y(i)-y_min)/y_delta);
    if y_bin<nbin_y
      y_bin=y_bin+1;
    end

    map (x_bin,y_bin) = map (x_bin,y_bin) +1;
  
  end
  
  map = log10(map);
%  map = flipud(map);
  
  tx = x_min:x_delta:x_max;
  ty = y_min:y_delta:y_max;

%  [x_grid, y_grid] = meshgrid (tx, ty);

%  ty = fliplr(ty);  
  

  imagesc(tx, ty, map');

  set (gca, 'YDir', 'normal')
  c=colorbar;
  s=get(c,'YTick');

  ytick=[];
  yticklabel=[];
  for i=1:size(s,2)
    if mod(s(1,i),1)==0
      ytick=[ytick s(1,i)];
      yticklabel=[yticklabel 10^(s(1,i))];
    end
  end
  set(c, 'YTick', ytick);
  set(c, 'YTickLabel', yticklabel);  
  cc=colormap(jet);
  set(gca(), 'Color', cc(1,:));
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%END%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

