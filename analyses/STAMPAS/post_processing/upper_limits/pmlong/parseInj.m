function parseInj(searchPath, ninj)

% read in the injection matfiles
wvfDir = [searchPath, '/ContWaveforms'];

% arrays for necessayr quanities
tauVal = [];
fVal = [];
epsVal = [];
hrssVal = [];

% loop over and read from all injection matfiles
for ii  = 1:ninj;
 
  wvfFile = ['msmagnetar', num2str(ii), '.mat'];

  wvf = load([wvfDir, '/', wvfFile]);
  
  tauVal = [tauVal,wvf.tau];
  
  fVal = [fVal, wvf.f0];
  epsVal = [epsVal, wvf.epsilon];
  hrssVal = [hrssVal, wvf.hrss];
  clear wvf;

end

% find unique values
f_un = unique(fVal);
tau_un = unique(tauVal);

Egw_ul = zeros(length(f_un)*length(tau_un), 1);
f0s = zeros(length(f_un)*length(tau_un), 1);
taus = zeros(length(f_un)*length(tau_un), 1);
cnt = 1;

for ii = 1:length(f_un)
    for jj = 1:length(tau_un)

 	idx = 1:ninj;
	idx = idx(fVal==f_un(ii) & tauVal == tau_un(jj));
	eps = epsVal(fVal==f_un(ii) & tauVal == tau_un(jj));
	
	% calculate upper limits in energy
	Egw_ul(cnt) = energyUL(searchPath, 'msmagnetar',idx, f_un(ii), eps, tau_un(jj));
	f0s(cnt)  = f_un(ii);
	taus(cnt) = tau_un(jj);
	cnt = cnt + 1;
    end
end
% save		 
save([searchPath, '/Egw_UL.mat'], 'f0s', 'taus', 'Egw_ul')

