function Egw_UL = energyUL(searchPath,wvfname,wvfidx,  f0, eps, tau)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Script for calculating energy based upper limits
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% directory where output files are stored from the efficiency post-processing step
effDir = [searchPath, '/figures/'];

if ~(exist(effDir) == 7)
  error('Need to run the Injection post processing first');
end
 
effValues = [];
hValues = [];
count = 0;

for ii  = 1:length(wvfidx);
    % load in in the output files
    effFile = ['eff_msmagnetar', num2str(wvfidx(ii)), '.mat'];

    load([effDir, '/', effFile]);
    effValues = [effValues,eff(:, 8)];

    % Some reference hrss to check all injections have the same values
    if isempty(hValues);
       href = eff(:, 2);
    end

    if ~isequal(href, eff(:,2))
       error('something wrong. Injections dont have the same hrss amplitude.')
    end
       
    hValues = [hValues, eff(:, 2)];
    
end

% Sort to make sure that amplitudes are in the right order.
[href, idx] = sort(href);

  
%% We will fit the data to a sigmoidal curve. 
close all; 
fsig = fittype( '1/(1+exp(-a*(x-b)))', 'independent', 'x', 'dependent', 'y' );


% Define array for the h values
hs = linspace(href(1), href(end), 500);
dhs = hs(2) - hs(1);

% initialize
Egw_UL = 0;

% For the common inejction set wvfidx will be 2

for ii = 1:length(wvfidx)
  % fit to the sigmoid
  feff = fit(log10(href), effValues(:, ii), fsig, 'StartPoint', [4.6, log10(5e-21)]);
  mEff = feff(log10(hs));

  % We get 90% UL by setting efficiency == 0.9
  h_UL = interp1(mEff, hs, 0.9);

  % Calculate the scaling factor -  all magnetar wave forms start out wiht h = 1e-24 
  alp_UL = h_UL/1e-24;

  % Convert to energies - we will use the same fiducial values are in the injection generation script
  II = 1e45; %cgs
  G = 6.67408*1e-8; % cgs
  c = 3*10.^10; %cm/s
  dd = 40*3.0857e24; %cm

  %consts = 32 * (pi^6) * G * (II^2) * (fstart^6) / (10*c^5);

  msun = 1.988e33; % mass in grams
  Esun = msun*c^2; % energy in ergs
  
  energy_constant = 64 * (pi^6) * G * (II^2) * (f0^6) * tau.* eps(ii).^2 / (5*c^5);

  Egw_UL  = Egw_UL + energy_constant.*alp_UL.^2/Esun; % Egw in solar mass energy units
end

% average
Egw_UL = Egw_UL/length(wvfidx);

return 
