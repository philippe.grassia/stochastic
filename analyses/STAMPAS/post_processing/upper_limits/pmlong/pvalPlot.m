function pvalPlot(searchPath, loadfromfile, varargin)

%%%
%%% Script to make false alarm probability curves accounting for trials factor
%%% searchPath : The path to the background run directoru

%% Either make a curve with or without trials factor and zero-lag trigger
if nargin == 2
  Nzl = 1;
  dozl = 0;
elseif nargin == 4
  Nzl = strassign(varargin{1});
  zl_snr = strassign(varargin{2});
  dozl = 1;
else
    error('Takes only 2 or 4 inputs')
end

% load in joblist files
joblist = load([searchPath, '/tmp/jobfile_bknd.txt']);
T = size(joblist, 1);
  
% make a directory for plots
trigplots = [searchPath, '/trigger_plots'];
system(['mkdir -p ', trigplots]);

% load pval plot if already made a stored from a previous time
if loadfromfile == 1
  load([trigplots, '/pval.mat']);
else
  %addpath to the stampas directory. 
  idx = regexp(searchPath, '/BKG');
  addpath(genpath(searchPath(1:idx)));

  % we will use parfor to speed things up
  parpool(25);
  result_dir = strcat(searchPath,'/results');
  filenum = dir(result_dir);
  
  snrlist = NaN(T,T);
  
  for ii = 3:length(filenum)
   
    dir_name = [result_dir, '/', filenum(ii).name];
    matlist = dir(dir_name);
    
    parfor jj = 3:1:length(matlist)
    
    if ~matlist(jj).isdir
       % load result matfiles and extract SNR vals
      matname = strcat( dir_name,'/', matlist(jj).name);
       D = load(matname);
       snr_tmp(jj-2) = D.data.SNR;
     end
   end
  
  snrlist(ii-2, 1:length(snr_tmp)) = snr_tmp;  
  clear snr_temp;
  
  fprintf('Finished reading data with lagnumber = %i\n', ii)
end
end

[~, e] = regexp(searchPath, 'ID-');
dagN = searchPath(e+1:end);
pval_filename = [trigplots,'/snr_pval.png'];

snrmax = max(snrlist(:));

snrVals = 0.1:0.1:snrmax;
Nfap = [];
for ii = 1:length(snrVals)
  Nfap =[Nfap,  sum(snrlist(:)>snrVals(ii))];
end
Tbkg_zl =  sum(~isnan(snrlist(:)))/Nzl;

% This is pval for one trigger greater than some snr
pval = 1 - exp(-Nfap./Tbkg_zl);

% Making FAP histogram
figure
loglog(snrVals,pval);

if dozl
  hold on
  zl_pval = interp1(snrVals, pval, zl_snr);
  fprintf(['The pvalue of the zero lag trigger is ' num2str(zl_pval) '\n'])
  yvals  = 1e-8:0.01:1.5;
  plot(zl_snr*ones(length(yvals), 1), yvals)
  legend('pvalue', 'zerolag')
end

title('False Alarm Probility/P-Value');
xlabel('SNR');
ylabel('P-Value');
xlim([1 1e2]);
ylim([0.5*min(pval(pval ~= 0)) 1.1]);
grid minor
fprintf('Saving plot in %s \n', pval_filename)
saveas(gcf,pval_filename);
close all

save([trigplots, '/pval.mat'],'pval','snrVals', 'snrlist');


close all;
end




