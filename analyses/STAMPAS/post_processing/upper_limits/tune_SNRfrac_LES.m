function tune_SNRfrac_LES()
% function tune_SNRfrac_LES()
%
% Runs UL_LES.m for specified values of the SNRfrac threshold.
% Plots upper limits (normalized by minimum upper limits)
% as a function of SNRfrac.  Results are saved in a .mat
% file for quick plotting in the future.
%
% Users may wish to change the SNRfrac_range and matfile
% name below, as well as the waveform names.
%
% Written by Tanner Prestegard (prsetegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% SNRfrac ranges
% Note: thresholds lower than 0.1 tend to cause problems
%       by removing basically all of the background events.
SNRfrac_range = 0.1:0.05:0.95;

% Name of matfile.
matfile_name = 'S6_LES_tuning.mat';

wf_names = {'adiA','adiB','adiC','adiD','adiE', ...
	    'lineA','lineB','monoA','monoB','quadA', ...
	    'quadB','sgA','sgB','wnbA','wnbB','wnbC'};

m = {'-b','-r','-k','-g','-m','-c', ...
     '--b','--r','--k','--g','--m','--c', ...
     ':b',':r',':k',':g',':m',':c'};

if (exist(matfile_name) ~= 2)
  % If matfile doesn't exist, calculate
  % upper limits for different SNRfrac values.
  fprintf('Calculating upper limits.\n');

  % Loop over SNRfrac values.
  for ii=1:numel(SNRfrac_range)
    fprintf('Testing SNRfrac = %f.\n',SNRfrac_range(ii));
    [hrss,all_ULs{ii}] = UL_loudest_event(true,SNRfrac_range(ii));
  end
  
  % Save matfile with SNRfrac values and upper limit results.
  save(matfile_name,'hrss','all_ULs','SNRfrac_range');
else
  % Otherwise, load matfile.
  fprintf('Loading results of SNRfrac tuning.\n');
  load(matfile_name);
end

% Get number of waveforms
N_wf = numel(hrss);

% Loop over waveforms.
for ii=1:N_wf
  fprintf('Plotting waveform %i of %i.\n',ii,N_wf);

  % Loop over SNRfracs and get results for waveform ii.
  for jj=1:numel(SNRfrac_range)
    for kk=1:numel(all_ULs{jj}{ii})
      curve(kk,jj) = all_ULs{jj}{ii}(kk);
    end
  end

  % Plot UL/UL_min as a function of SNRfrac.
  % Each plot contains many curves of different hrss values
  % for a single waveform.
  close all;
  figure;
  for jj=1:size(curve,1)
    norm_curve_jj = curve(jj,:)/min(curve(jj,:));
    plot(SNRfrac_range,norm_curve_jj,m{jj},'LineWidth',2);
    if (jj==1)
      hold on;
    end
  end
  hold off;
  xlabel('SNRfrac');
  ylabel('UL/UL_{min}');
  hl = legend(cellstr(num2str(hrss{ii}(:))),'Location','NorthEastOutside');
  set(get(hl,'title'),'string','h_{rss}','FontSize',20);
  title([wf_names{ii} ' loudest event statistic UL/UL_{min} vs. SNRfrac']);
  largePlot(15,12);
  pretty();
  ylim([0.95 1.25]);
  print('-dpng',['SNRfrac_tuning_LE_' wf_names{ii} '.png']);

end

return;