function params = UL_params()
% function params = UL_params()
%
% Information for searches to be combined into upper limit
% calculation. Users can specify one search or multiple searches
% by adding elements to the parameters that are cell arrays.
% Example:
%   One search: params.GPS_start = {10};
%   Multiple:   params.GPS_start = {10, 20, 30};
% Note: you would need to specify GPS end times, ID numbers, and
%       SNRfrac cuts for each search, as well.
%
% Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The following 8 parameters MUST BE CELL ARRAYS.
% Label for each search.
params.search_label={'O1'};
% GPS start and end of the search.
params.GPS_start = {1126073342};
params.GPS_end = {1137283217};
% ID number for injection study, zero-lag, and background study.
params.ID_inj = {2};
params.ID_zl = {7};
params.ID_bkg = {11};
% SNRfrac threshold to apply.
params.SNRfracCut = {0.5};
params.webFolder = {'Up'};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use DQ flags?
params.useDQflags = true;
% Use SNRfrac?
params.useSNRfrac = true;
% Use TFveto
params.useTFveto = true;
% Use Rveto
params.useRveto = true;
% Make plots?
params.doPlots = true;

% Interpolation method used for all instances of interp1 function.
% Users should calculate results with multiple interpolation
% methods to ensure that interpolation does not significantly
% affect the results.
% Options: 'nearest', 'linear', 'spline', 'cubic'.
% See Matlab help for interp1 for more information.
params.interp_method = 'linear';

% Parameters for ad hoc waveforms for FAD calculation.
% The FAD should ONLY be used for ranking of background
% events across multiple searches/networks for ad hoc
% waveforms, NOT for final upper limit calculations.
% The code calculates the hrss where the search has
% 50% efficiency for the ad hoc waveform and assigns
% it a fiducial distance specified by the user below.
params.adhoc_dist = 1; % Mpc

% Don't edit below here! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up calibration error.
for ii=1:numel(params.search_label)
  if (strcmpi(params.search_label{ii},'S5'))
    % S5 - J. Abadie et al. Nucl. Instrum. Meth. A624 223 (2010)
    % Table 1, 40-2000 Hz magnitude error.
    % H1 = 10.4%, L1 = 14.4%.
    params.calib_error{ii} = 0.1776;
  elseif (strcmpi(params.search_label{ii},'S6'))
    % S6 - LIGO DCC T1100071

    % Used highest error from all S6 epochs
    % listed in Table 1, 40-2000 Hz band.
    % H1 = 4%, L1 = 7%
    params.calib_error{ii} = 0.0806;
  elseif (strcmpi(params.search_label{ii},'O1'))
     params.calib_error{ii} = 0;
    continue
  else
    error('Search label should be S5 or S6.');
  end
end


% get the web folder
[~,h]=system('hostname -d');
h = h(1:(end-1));
switch h
  case {'ligo.caltech.edu','ligo-wa.caltech.edu',['ligo-' ...
                        'la.caltech.edu']}
    params.webFolder=strcat(['/home/' getenv('USER') '/public_html/'],...
                            params.webFolder);
    
  case {'atlas.aei.uni-hannover.de','atlas.local'}
    params.webFolder=strcat(['/home/' getenv('USER') '/WWW/LSC/'],...
                            params.webFolder);

  otherwise
    params.webFolder=strcat('./WEB/',params.webFolder);
end

return;

% End of file