function par_split = split_params(params)
% function par_split = split_params(params)
%
% Split params struct into an array of structs
% where each element in the array is a parameter struct
% corresponding to a particular search.
%
% Example:
%  Input params has params.a = {1,2,3}; params.b = 10;
%  Output par_split has par_split(1).a = 1; par_split(1).b = 10;
%                       par_split(2).a = 2; par_split(2).b = 10;
%                       par_split(3).a = 3; par_split(3).b = 10;
%
% Useful for passing to sub-routines that process data
% on a search-by-search basis and don't have an index
% number passed to them.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get fieldnames of params struct.
fn = fieldnames(params);

% Use search_label field as representative of how many searches
% are included in the params struct.
% Loop over searches.
for ii=1:numel(params.search_label)
  % Loop over fields.
  for jj=1:numel(fn)
    % If the field is a cell array, take the iith element.
    if iscell(params.(fn{jj}))
      par_split(ii).(fn{jj}) = params.(fn{jj}){ii};
    else
      % Otherwise, it should be only a single element.
      par_split(ii).(fn{jj}) = params.(fn{jj});
    end
  end
end


return;