function largePlot(w,h)
% Makes plot area larger.

set(gcf,'PaperUnits','inches');
set(gcf,'PaperSize',[w h]);
set(gcf,'PaperPositionMode','manual');
set(gcf,'PaperPosition',[0 0 w h]);

% make legend larger
%{
set(get(gca(),'XLabel'),'FontSize',20);
set(get(gca(),'YLabel'),'FontSize',20);
set(get(gca(),'Title'),'FontSize',24);
set(gca(),'FontSize',18);
%}
return;