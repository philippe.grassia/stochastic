function out = calc_vvis(wf,bg_snr,params)
% function out = calc_vvis(in_wf,bg_snr,params)
%
% Calculates visible volume for a particular waveform
% given a list of injection triggers at different distances
% and a list of background SNR triggers.  Also calculates
% visible volume uncertainty due to statistical and
% systematic effects.  Amplitude calibration error
% is the primary cause of systematic uncertainty.
% This function is called during calc_FAD.m
%
% Inputs:
%   wf          - array of structs. Each struct corresponds to a
%                 particular alpha value for this waveform and
%                 contains a trigger list, an alpha value, an hrss,
%                 the number of injections performed at this alpha,
%                 and the distance (if applicable).
%   bg_snr      - list of SNRs of all background triggers.
%   params      - params struct.
%
% Outputs:
%   out - waveform struct, including visible volume and
%         uncertainty as a function of SNR.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Have to loop over background SNRs here.
for ii=1:numel(bg_snr)
  % Loop over alphas and calculate efficiency at each alpha.
  for jj=1:numel(wf)
    eff(jj) = sum(wf(jj).data.SNR > bg_snr(ii))/wf(jj).Ninj;
  end

  % Get list of all hrsses.
  hrss = [wf.hrss];

  % Get list of all distances.
  % Handle ad hoc waveforms using parameters specified in
  % UL_params.m.
  if (sum([wf.dist]) == 0)
    % ad hoc waveforms.
    % Find hrss of 50% efficiency (interpolate if needed)
    % and assign it the distance specified by the user.
    temp = (eff-0.5);
    idx1 = find(temp==min(temp(temp>0)));
    idx2 = find(temp==max(temp(temp<0)));
    hrss_50 = interp1(eff([idx1 idx2]), hrss([idx1 idx2]), 0.5, ...
		      params.interp_method);
    dists = params.adhoc_dist.*(hrss_50./hrss);
  else
    dists = [wf.dist];
  end

  % Sort by distance.
  [dists,d_idx] = sort(dists);
  eff = eff(d_idx);

  % Calculate visible volume (discrete method)
  % N_of_r is cumulative number of injections
  % as a function of distance.
  N_of_r = (1:numel(dists))*wf(1).Ninj;
  dNdr = gradient(N_of_r,dists);

  % Number of signals recovered vs. distance.
  Nrec = eff*wf(1).Ninj;
  rho_inv = (4*pi)*(dists.^2)./dNdr;
  out.v_vis(ii) = sum(Nrec.*rho_inv);
  out.dv_stat(ii) = sqrt(sum(Nrec.*rho_inv.^2));
end

% Systematic uncertainty (due to calibration error).
out.dv_sys = (params.calib_error*3).*out.v_vis;

% Add uncertainties in quadrature.
out.dv_tot = sqrt(out.dv_stat.^2 + out.dv_sys.^2);

return;