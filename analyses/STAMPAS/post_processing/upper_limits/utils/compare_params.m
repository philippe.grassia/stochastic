function compare_params(params1,params2)
% function compare_params(params1,params2)
%
% Function that compares two parameter structs to check
% for equivalance.  If anything doesn't match up, an
% error is thrown.
%
% INPUT:
%   params1 - first params struct.
%   params2 - second params struct.
%
% Written by Tanner Prestegard (prestegard@physics.umn.edu) 9/21/2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Params to ignore (don't affect the analysis).
% Example: doPlots is false when you first calculate upper limits,
% so that is saved as false in the FAD .mat file.  But then you
% want to make plots later; you shouldn't have to reproduce the FAD
% file to do that.
ignore = {'search_label','doPlots','interp_method'};

% Get fieldnames.
field1 = fieldnames(params1);
field2 = fieldnames(params2);

% Make sure both sets of parameters have the same fieldnames.
diff1 = setdiff(field1,field2);
diff2 = setdiff(field2,field1);
if (~isempty(diff1) | ~isempty(diff2))
  error('Saved parameters don''t match');
end

% Loop over fieldnames and compare the two parameter sets.
% Throw an error if something doesn't match up.
for ii=1:numel(field1)

  % Shorthand variable.
  field_ii = field1{ii};

  % If the fieldname matches one of the params we ignore,
  % jump to the next iteration of the ii for loop.
  if sum(strcmpi(field_ii,ignore) > 0)
    continue;
  end

  % Different comparison methods depending if the
  % parameter is a string or a number.
  if ischar(params1.(field_ii))
    if ~strcmpi(params1.(field_ii),params2.(field_ii))
      error(['Field ' field_ii ' doesn''t match.']);
    end
  elseif (isnumeric(params1.(field_ii)) | islogical(params1.(field_ii)))
    if (params1.(field_ii) ~= params2.(field_ii))
      error(['Field ' field_ii ' doesn''t match.']);
    end
  else   
    error('Parameter fields should be strings numeric.');
  end

end

return;