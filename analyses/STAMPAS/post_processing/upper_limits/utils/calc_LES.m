function [search,tot] = calc_LES(params,SNR_thresh,search)
% function wf = calc_LES(params,SNR_thresh)
%
% Loudest event statistic:
% Calculates efficiency as a function of hrss (and distance for
% physical waveforms).  Returns an array of structs where each
% struct corresponds to a particular waveform.
%
% Inputs:
%   params     - struct specifying params for handling data.
%   SNR_thresh - SNR threshold for injection recovery.
%   search     - search array; each element is a struct corresponding
%                
%
% Outputs:
%   search - same as input, but with waveform information (efficiency,
%            waveforms, etc.) added to it.
%   tot    - array of structs combining results from multiple searches.
%            Each element in the array corresponds to a waveform.
%   wf - an array of structs where each struct corresponds to
%        to a particular waveform. Struct contains all injected
%        hrss/distance values along with number of injections made
%        and recovered at each hrss/distance.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import classes.waveforms.dictionary

% Number of searches.
nSearches = numel(params.search_label);

% Split parameters struct into array of structs
% for ease of passing to subroutines.
par = split_params(params);

% Loop over searches.
for ii=1:nSearches

  % Set up search path.
  searchPath_inj = generate_searchPath('INJ',par(ii).GPS_start, ...
				       par(ii).GPS_end, ...
				       par(ii).ID_inj);
  
  % Get number of injections.
  [names,values] = textread(['../../' searchPath_inj '/config_inj.txt'], '%s %s\n',-1,'commentstyle','matlab');
  Ninj = values(find(strcmp(names,'NumberOfInjections')));
  Ninj = str2num(cell2mat(Ninj)); % Convert from cell -> string -> number.
  
  dic  = dictionary.parse(['../../' searchPath_inj '/waveforms.txt']);
  wvfs = dic.headers;

  % Get waveform names and check search compatibility by comparing the names
  % of the waveforms used in each search (if using multiple searches).
  search(ii).wf_names = {wvfs.id};

  if (ii > 1)
    % Compare with wf_names from search(1) to make sure they are the same.
    for jj=1:numel(search(ii).wf_names)
      if ~strcmp(search(1).wf_names(jj),search(ii).wf_names(jj))
	error(['Waveforms from search ' num2str(ii) ' don''t match.  Check' ...
	       ' your waveforms.txt files.']);
      end
    end
  end

  
  % If SNR_thresh is an array, that means each waveform has
  % a corresponding threshold which may be unique.
  % Thus, when it is just one number, we make it into an array
  % for compatibility purposes. This is done within the for loop
  % because we don't know the number of waveforms until this point.
  if (numel(SNR_thresh) == 1)
    SNR_thresh = ones(size(wf_names))*SNR_thresh;
  end
  
  % Loop over waveforms.
  currentWvfNb=0;
  for w=wvfs'
    currentWvfNb=currentWvfNb+1;

    % Print message for user.
    fprintf('Processing results for waveform %s (%i/%i).\n',w.legend, ...
            currentWvfNb,dic.nbWvf);
  
    % Load injection study results.
    % Data is organized in a struct containing substructs.
    % Each substruct corresponds to a particular distance
    % for that injection and contains the results
    % for that distance and some metadata.
    inj_data = get_inj_data(searchPath_inj,par(ii),w);
    
    % Loop over alphas and reorganize the data into a new struct.
    for kk=1:numel(w.alphas)
      % Add number of injections to wf struct.
      search(ii).wf(currentWvfNb).Ninj(kk) = Ninj;
      
      % Number of injections recovered.
      search(ii).wf(currentWvfNb).Nrec(kk) = sum(inj_data(kk).data.SNR > SNR_thresh(currentWvfNb));

      % Poisson uncertainty on number of recovered injections.
      search(ii).wf(currentWvfNb).dNrec(kk) = sqrt(search(ii).wf(currentWvfNb).Nrec(kk));
      
      % Distance, accounting for calibration error.
      search(ii).wf(currentWvfNb).dist(kk) = inj_data(kk).dist / ...
	  (1 + params.calib_error{ii});
      
      % hrss, accounting for calibration error.
      search(ii).wf(currentWvfNb).hrss(kk) = inj_data(kk).hrss * ...
	  (1 + params.calib_error{ii});;

    end

    % Efficiency.
    search(ii).wf(currentWvfNb).eff = search(ii).wf(currentWvfNb).Nrec ./ search(ii).wf(currentWvfNb).Ninj;
    
    % Waveform name.
    search(ii).wf(currentWvfNb).name = w.legend;
  end

end


for ii=1:numel(search(1).wf_names)
  % Get min and max hrss after calibration error is accounted for.
  hrss_min = min(search(1).wf(ii).hrss);
  hrss_max = max(search(1).wf(ii).hrss);
  for jj=2:nSearches
    hrss_min = max(min(search(jj).wf(ii).hrss),hrss_min);
    hrss_max = min(max(search(jj).wf(ii).hrss),hrss_max);
  end

  % Interpolate at all hrss points falling between min and max.
  hrss_int = [];
  for jj=1:nSearches
    hrss_idx = (search(jj).wf(ii).hrss >= hrss_min & search(jj).wf(ii).hrss <= hrss_max);
    hrss_int = [hrss_int search(jj).wf(ii).hrss(hrss_idx)];
  end
  hrss_int = sort(hrss_int);

  for jj=1:nSearches
    search(jj).wf(ii).hrss_int = hrss_int;
    search(jj).wf(ii).eff_int = interp1(search(jj).wf(ii).hrss, ...
					search(jj).wf(ii).eff,hrss_int,...
					params.interp_method);
  end

  % Get hrss and distance.
  tot(ii).hrss = hrss_int;
  dist_hrss = search(1).wf(ii).dist(1).*search(1).wf(ii).hrss(1);
  tot(ii).dist = dist_hrss./hrss_int;

  % Calculate total sum of efficiency * observation time for each hrss value.
  tot(ii).eff_time = search(1).wf(ii).eff_int*search(1).zl.T_obs/(86400*365.25);
  for jj=2:nSearches
    tot(ii).eff_time = tot(ii).eff_time + ...
	search(jj).wf(ii).eff_int*search(jj).zl.T_obs/(86400*365.25);
  end
end

return;