function [] = prepareWebReport(params)  
% prepareWebReport :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = prepareWebReport ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%

addpath('../')
addpath('../..');
addpath('../../functions');

import classes.waveforms.dictionary

% Get number of searches.
nSearches = numel(params.search_label);

% Loop over searches.
for ii=1:nSearches
  
  % mkdir the web folder
  if ~exist(params.webFolder{ii})
    system(['mkdir ' params.webFolder{ii}]);
    system(['mkdir ' params.webFolder{ii} '/fad/']);
    system(['mkdir ' params.webFolder{ii} '/fad/figures']);

    system(['mkdir ' params.webFolder{ii} '/les/']);
    system(['mkdir ' params.webFolder{ii} '/les/figures']);
  end

  % get path
  injPath = generate_searchPath('INJ',params.GPS_start{ii},...
                                params.GPS_end{ii},params.ID_inj{ii});

  % get the waveforms used in the search
  dic = dictionary.parse(['../../' injPath '/waveforms.txt']);

  for w=dic.headers'
    if ~exist([params.webFolder{ii} '/fad/' w.id])
      system(['mkdir ' params.webFolder{ii} '/fad/' w.id]);
      system(['mkdir ' params.webFolder{ii} '/fad/' w.id '/figures']);
    end
  end
end

end