function out_data = clean_data(data, params)
% function out_data = clean_data(data, params)
%
% Function for vetoing triggers using DQ flags and the
% SNRfrac veto.
%
% Inputs:
%   data   - array of triggers.  Each row is a trigger
%            and each column is a different parameter.
%   params - params for handling the data.
%
% Outputs:
%   out_data - cleaned array of triggers.

% Set input data to output data.

  cut={};
  
  if params.useDQflags
    cut=[cut 'DQFlags'];
  end
  
  if params.useTFveto
    cut=[cut 'TFveto'];
  end

  if params.useRveto
    cut=[cut 'Rveto'];
  end

  if params.useSNRfrac
    cut=[cut ['SNRfrac:' num2str(params.SNRfracCut)]];
  end

  cut=strcat(cut,'+');
  cut=[cut{:}];
  
  [~,~,idx] = decode(cut,data);
  out_data=data(idx);

end