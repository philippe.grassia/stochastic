function R90 = bayes_UL(nu,dnu,doPlots,wf_name,folder)
% function R90 = bayes_UL(nu,dnu,doPlots,wf_name,folder)
%
% Takes as input the time-volume productivity for a search
% and its associated uncertainty (both in units of Mpc^3*yr)
% and calculates the 90% rate upper limits.  Uses a Bayesian
% formalism assuming a Poisson distribution on the number
% of events observed and a Gaussian distribution for the
% time-volume productivity.  Used as part of UL_FAD.m
%
% Inputs:
%   nu      - time-volume productivity of a search in units
%             of Mpc^3*yr.
%   dnu     - uncertainty on time-volume productivity (same units).
%   doPlots - true if plots should be produced, false otherwise.
%   wf_name - name of waveform for which the upper limit
%             is being calculated.
%   folder  - webFolder
%
% Outputs:
%   R90 - 90% rate upper limit (units of Mpc^{-3}yr^{-1}).
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Number of points to use for likelihood calculation.
% Users can modify this if needed, but 5000 seems to produce
% the same results as larger numbers of points, so it doesn't
% seem necessary to sample the likelihood more finely.
Npts = 5e3;

% Set up priors.  We use flat priors in R and nu.
% The boundaries of the priors are chosen such that all significant
% contributions to the likelihood should be contained within the priors.
% Log spacing is used for R because there are significant contributions
% to the likelihood over several orders of magnitude.
logR_min = -10;
logR_max = log10((2.3/nu)*1e2);
nu_min = 0;
nu_max = nu*10;

% Make grids of points to sample the likelihood.
R_pts = logspace(logR_min,logR_max,Npts);
nu_pts = linspace(nu_min,nu_max,Npts);
R_array = repmat(R_pts,size(nu_pts'));
nu_array = repmat(nu_pts',size(R_pts));

% Convert from R to mu because the likelihood can only be defined in
% terms of mu and nu, not R.
mu_array = R_array.*nu_array;
mu_pts = R_pts.*nu_pts;

% Likelihood is formulated in terms of:
%  1. The probability of observing zero events for a Poisson distribution
%     with mean mu. P(N=0 | mu) = exp(-mu).
%  2. A Gaussian distribution with mean nu and sigma dnu.
poiss_like = exp(-mu_array);
gauss_like = exp(-((nu_array-nu).^2)./(2*dnu.^2))./(sqrt(2*pi)*dnu);
like = poiss_like.*gauss_like;

% Because we use flat priors and can ignore the evidence, the 
% posterior is the same as the likelihood.
post = like;

% Transform likelihood to be a function of two new variables, R and Q.
% R = mu/nu (rate).
% Q = nu (we choose this for simplicity)
Q_pts = nu_pts;
Q_array = nu_array;

% Calculate Jacobian determinant of the transformation.
% mu = R*Q = R*nu
%   dmu/dR = nu
%   dmu/dQ = R
% nu = Q
%   dnu/dR = 0
%   dnu/dQ = 1
% |J| = (dmu/dR)*(dnu/dQ) - (dnu/dR)*(dmu/dQ)
%     = nu
%%%%%%%%%%%%%%%%%%%
det_J = nu_array;

% Apply the transformation to get a new posterior distribution
% in terms of R and Q.
post_RQ = post .* det_J;

% Marginalize over Q and normalize to get the 1D posterior on R.
post_R = trapz(Q_pts,post_RQ);
post_R = post_R/trapz(R_pts,post_R);

% Find the 90% limit on R.
c_post = cumtrapz(R_pts,post_R);
temp = find(c_post > 0.9);
R90 = R_pts(temp(1));

% Make plots (if requested).
if doPlots

  % Filename.
  fname = [folder '/' wf_name '_rate_PDF.png'];

  % Replace _ with \_ for plotting purposes.
  wf_name = strrep(wf_name,'_','\_');

  close all;
  figure;
  % Area plot to show 90% contour.
  area(R_pts(1:temp),post_R(1:temp),'FaceColor',[0.5 0.5 0.5]);
  hold on;
  % Line plot to show full curve.
  plot(R_pts,post_R,'-k','LineWidth',2);
  % Add line at 90% limit with text.
  line([R90 R90],ylim,'LineStyle','--','Color','k','LineWidth',2);
  t_str = sprintf('R_{90%%} = %.5g Mpc^{-3}-yr^{-1}',R90);
  text(R90*1.1,mean(ylim),t_str,'FontSize',14);
  hold off;
  % Set x-scale.
  xlim([0 max(R_pts)*2]);
  % Axis labels.
  xlabel('Rate (Mpc^{-3}yr^{-1})');
  % Plot title.
  t = [wf_name ' rate PDF'];
  title(t);
  largePlot(15,12);
  set(gca,'XScale','log');
  set(gca,'layer','top');
  pretty(26);
  % Print file.
  print('-dpng',fname);
  close all;
end

return;