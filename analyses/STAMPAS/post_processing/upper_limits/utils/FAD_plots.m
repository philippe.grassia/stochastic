function FAD_plots(search, params)
% function FAD_plots()
%
% Code for making diagnostic plots for the FAD upper limit calculations.
% Not used in actual upper limits calculations.
%
% Inputs:
%   search - search struct.
%   params - params struct.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Plotting markers and line types.
m = {'-b','-r','-k','-g','-m','-c','-y', ...
     '--b','--r','--k','--g','--m','--c','--y', ...
     ':b',':r',':k',':g',':m',':c',':y',...
    '-.b','-.r','-.k','-.g','-.m','-.c','-.y'};
m_zl = {'ob','or','ok','og','om','oc','oy', ...
	'xb','xr','xk','xg','xm','xc','xy', ...
	'sb','sr','sk','sg','sm','sc','sy',...
	'hb','hr','hk','hg','hm','hc','hy'};

% Number of searches
nSearches = numel(search);

% Calculate combined time-volume product
% as a function of FAD for each waveform.
% First get min and max FAD of all searches.
for jj=1:numel(search(1).inj)
  min_FAD = 1e6;
  max_FAD = 0;
  for ii=1:nSearches
    % Get min FAD larger than 0 and max FAD.
    FAD_gt0 = search(ii).inj(jj).FAD(search(ii).inj(jj).FAD>0);
    min_FAD_ii = min(FAD_gt0);
    max_FAD_ii = max(search(ii).inj(jj).FAD);
    if (min_FAD_ii < min_FAD)
      min_FAD = min_FAD_ii;
    end
    if (max_FAD_ii > max_FAD)
      max_FAD = max_FAD_ii;
    end
  end
  
  % FADs for combined search.
  comb.FAD(:,jj) = logspace(log10(min_FAD),log10(max_FAD),100)';
  comb.nu(:,jj) = zeros(size(comb.FAD(:,jj)));
  for ii=1:nSearches
    comb.search(ii).v_vis(:,jj) = interp1(search(ii).inj(jj).FAD, ...
					  search(ii).inj(jj).v_vis,comb.FAD(:,jj), ...
					  params.interp_method);
    comb.search(ii).nu(:,jj) = comb.search(ii).v_vis(:,jj)*search(ii).zl.T_obs/(86400*365.25);
    comb.nu(:,jj) = comb.nu(:,jj) + comb.search(ii).nu(:,jj);
  end
end

% Calculate FAD and v_vis for all ZL triggers for each waveform.
for ii=1:numel(search(1).wf_names)
  for jj=1:nSearches

  zl_snr = sort(search(jj).zl.data.SNR);
  zl_snr = zl_snr(1:end);
  search(jj).zl.FAD(:,ii) = interp1(search(jj).inj(ii).snr, ...
				    search(jj).inj(ii).FAD,zl_snr, ...
				    params.interp_method);
  search(jj).zl.v_vis(:,ii) = interp1(search(jj).inj(ii).snr, ...
				      search(jj).inj(ii).v_vis,zl_snr, ...
				      params.interp_method);
  search(jj).zl.nu(:,ii) = interp1(comb.FAD(:,ii),comb.search(jj).nu(:,ii), ...
				   search(jj).zl.FAD(:,ii),params.interp_method);
  search(jj).zl.snr = zl_snr;
  end
end

% Diagnostic plots
fprintf('Plotting.\n');

% Replace _ with \_ for plotting purposes.
wf_names = strrep(search(1).wf_names,'_','\_');

% FAD vs SNR
% Plot all waveforms for each search.
for ii=1:nSearches
  close all;
  figure;
  for jj=1:numel(search(ii).wf_names)
    semilogy(search(ii).inj(jj).snr,search(ii).inj(jj).FAD,m{jj},'LineWidth',2);
    if (jj==1)
      hold on;
    end
  end

  % Add ZL triggers.
  for jj=1:numel(search(ii).wf_names)
    semilogy(search(ii).zl.snr,search(ii).zl.FAD(:,jj),m_zl{jj},'MarkerEdgeColor',m_zl{jj}(2),'MarkerFaceColor',m_zl{jj}(2));
  end

  hold off;
  xlabel('SNR_\Gamma');
  ylabel('FAD (Mpc^{-3}yr^{-1})');
  legend('Location','SouthWest',wf_names);
  title([params.search_label{ii} ' FAD vs. SNR']);
  largePlot(15,12); 
  pretty();
  set(gcf,'Renderer','painters');
  print('-dpng',[params.webFolder{ii} '/fad/figures/'...
                 params.search_label{ii} '_FAD_vs_SNR.png']);

end

% Visible volume vs. SNR.
% Plot all waveforms for each search.
for ii=1:nSearches
  close all;
  figure;
  for jj=1:numel(search(ii).wf_names)
    semilogy(search(ii).inj(jj).snr,search(ii).inj(jj).v_vis,m{jj},'LineWidth',2);
    if (jj==1)
      hold on;
    end
  end

  % Add ZL triggers.
  for jj=1:numel(search(ii).wf_names)
    semilogy(search(ii).zl.snr,search(ii).zl.v_vis(:,jj),m_zl{jj},'MarkerEdgeColor',m_zl{jj}(2),'MarkerFaceColor',m_zl{jj}(2));
  end

  hold off;
  xlabel('SNR_\Gamma');
  ylabel('Visible volume (Mpc^{3})');
  legend('Location','SouthWest',wf_names);
  title([params.webFolder{ii} '/' params.search_label{ii} ' V_{vis} vs. SNR']);
  largePlot(15,12);
  pretty();
  set(gcf,'Renderer','painters');
  print('-dpng',[params.webFolder{ii} '/fad/figures/'...
                 params.search_label{ii} '_vvis_vs_SNR.png']);

end

% Visible volume vs. FAD.
% Plot all waveforms for each search.
for ii=1:nSearches
  close all;
  figure;
  for jj=1:numel(search(ii).wf_names)
    loglog(search(ii).inj(jj).FAD,search(ii).inj(jj).v_vis,m{jj},'LineWidth',2);
    if (jj==1)
      hold on;
    end
  end

  % Add ZL triggers.
  for jj=1:numel(search(ii).wf_names)
    loglog(search(ii).zl.FAD(:,jj),search(ii).zl.v_vis(:,jj),m_zl{jj},'MarkerEdgeColor',m_zl{jj}(2),'MarkerFaceColor',m_zl{jj}(2));
  end

  hold off;
  xlabel('FAD (Mpc^{-3}yr^{-1})');
  ylabel('Visible volume (Mpc^{3})');
  legend('Location','NorthEast',wf_names);
  title([params.search_label{ii} ' V_{vis} vs. FAD']);
  largePlot(15,12);
  set(gca,'XDir','reverse');
  pretty();
  set(gcf,'Renderer','painters');
  print('-dpng',[params.webFolder{ii} '/fad/figures/' ...
                 params.search_label{ii} '_vvis_vs_FAD.png']);

end

% Repeat same plots but do one for each waveform to compare the searches.
% Visible volume vs. FAD.
% Plot all searches for each waveform.
for jj=1:numel(search(1).wf_names)
  close all;
  figure;
  for ii=1:nSearches
    loglog(search(ii).inj(jj).FAD,search(ii).inj(jj).v_vis,m{ii}, ...
	   'LineWidth',2);
    if (ii==1)
      hold on;
    end
  end

  % Add ZL triggers.
  for ii=1:nSearches
    loglog(search(ii).zl.FAD(:,jj),search(ii).zl.v_vis(:,jj),m_zl{ii}, ...
	   'MarkerEdgeColor',m_zl{ii}(2),'MarkerFaceColor',m_zl{ii}(2));
  end

  hold off;
  xlabel('FAD (Mpc^{-3}yr^{-1})');
  ylabel('Visible volume (Mpc^{3})');
  legend('Location','NorthEast',params.search_label);
  title([wf_names{jj} ' V_{vis} vs. FAD']);
  largePlot(15,12);
  set(gca,'XDir','reverse');
  pretty();
  set(gcf,'Renderer','painters');
  print('-dpng',[params.webFolder{ii} '/fad/' search(1).wf_names{jj} ...
                 '/figures/' search(1).wf_names{jj} '_vvis_vs_FAD.png']);
  
end

% Visible volume vs. SNR
% Plot all searches for each waveform.
for jj=1:numel(search(1).wf_names)
  close all;
  figure;
  for ii=1:nSearches
    semilogy(search(ii).inj(jj).snr,search(ii).inj(jj).v_vis,m{ii}, ...
	     'LineWidth',2);
    if (ii==1)
      hold on;
    end
  end

  % Add ZL triggers.
  for ii=1:nSearches
    semilogy(search(ii).zl.snr,search(ii).zl.v_vis(:,jj),m_zl{ii}, ...
	     'MarkerEdgeColor',m_zl{ii}(2),'MarkerFaceColor',m_zl{ii}(2));
  end

  hold off;
  xlabel('SNR_\Gamma');
  ylabel('Visible volume (Mpc^{3})');
  legend('Location','SouthWest',params.search_label);
  title([wf_names{jj} ' V_{vis} vs. SNR']);
  largePlot(15,12);
  pretty();
  set(gcf,'Renderer','painters');
  print('-dpng',[params.webFolder{ii} '/fad/' search(1).wf_names{jj}...
                 '/figures/' search(1).wf_names{jj} '_vvis_vs_SNR.png']);

end

% FAD vs. SNR
% Plot all searches for each waveform.
for jj=1:numel(search(1).wf_names)
  close all;
  figure;
  for ii=1:nSearches
    semilogy(search(ii).inj(jj).snr,search(ii).inj(jj).FAD,m{ii}, ...
	     'LineWidth',2);
    if (ii==1)
      hold on;
    end
  end

  % Add ZL triggers.
  for ii=1:nSearches
    semilogy(search(ii).zl.snr,search(ii).zl.FAD(:,jj),m_zl{ii}, ...
	     'MarkerEdgeColor',m_zl{ii}(2),'MarkerFaceColor',m_zl{ii}(2));
  end
  
  hold off;
  xlabel('SNR_\Gamma');
  ylabel('FAD (Mpc^{-3}yr^{-1})');
  legend('Location','SouthWest',params.search_label);
  title([wf_names{jj} ' FAD vs. SNR']);
  largePlot(15,12);
  pretty();
  set(gcf,'Renderer','painters');
  print('-dpng',[params.webFolder{ii} '/fad/' search(1).wf_names{jj} ...
                 '/figures/' search(1).wf_names{jj} '_FAD_vs_SNR.png']);

end

% Plot nu vs FAD (combined and individual)
% Plot all searches for each waveform.
if (nSearches > 1)
  labs{1} = 'Combined';
  for ii=1:nSearches
    labs{ii+1} = params.search_label{ii};
  end
else
  labs{1} = params.search_label{1};
end

for jj=1:numel(search(1).wf_names)
  close all;
  figure;

  if (nSearches > 1)
    loglog(comb.FAD(:,jj),comb.nu(:,jj),'--k','LineWidth',2);
    hold on;
  end
  for ii=1:nSearches
    loglog(comb.FAD(:,jj),comb.search(ii).nu(:,jj),m{ii},'LineWidth',2);
  end

  % Put hold on here if only doing one search.
  if (nSearches == 1)
    hold on;
  end

  for ii=1:nSearches
    loglog(search(ii).zl.FAD(:,jj),search(ii).zl.nu(:,jj),m_zl{ii}, ...
	     'MarkerEdgeColor',m_zl{ii}(2),'MarkerFaceColor',m_zl{ii}(2));
  end

  hold off;
  xlabel('FAD (Mpc^{-3}yr^{-1})');
  ylabel('\nu (Mpc^3yr)');
  set(gca,'XDir','reverse');
  largePlot(15,12);
  legend(labs);
  title([search(1).wf_names{jj} ' \nu vs. FAD']);
  pretty();
  set(gcf,'Renderer','painters');
  print('-dpng',[params.webFolder{ii} '/fad/' search(1).wf_names{jj} ...
                 '/figures/' search(1).wf_names{jj} '_nu_vs_FAD.png']);
  close all;
end


return;