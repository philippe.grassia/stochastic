function inj = get_inj_data(searchPath,params,wvf)
% function inj = get_inj_data(params,wvf)
%
% Code for loading injection data.  Called by calc_FAD.m
% for calculating STAMPAS upper limits.
%
% Inputs:
%   params    - params struct
%   wvf       - classes.waveforms.header obj
%
% Outputs:
%   inj - a struct containing data for all injection triggers
%        at all alpha values for the waveform in question.
%
% Author: Tanner Prestegard (prestegard@physics.umn.edu)
%         Frey Valentin (frey@lal.in2p3.fr) Modifed by
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Loop over alphas.
% Load each file, determine distance from alpha factors,
% and assign results to a struct.
for ii=1:numel(wvf.alphas)  

  % Load file.
  fname = ['../../' searchPath ...
           '/results/results_merged/results_' wvf.file '_' ...
	   num2str(wvf.alphas(ii),'%0.10f') '_filtered.mat'];
  temp_data = load(fname);
  
  % Calculate distance from alpha factors.
  dist = wvf.distance/sqrt(wvf.alphas(ii));
  
  % Calculate hrss from alpha factors.
  hrss = wvf.hrss*sqrt(wvf.alphas(ii));

  % Assign data to wf struct.
  inj(ii).alpha = wvf.alphas(ii);
  inj(ii).dist = dist;
  inj(ii).hrss = hrss;
  inj(ii).data = clean_data(temp_data.data,params);
  
end


return;