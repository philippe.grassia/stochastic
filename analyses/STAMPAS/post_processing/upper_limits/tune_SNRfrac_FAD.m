function tune_SNRfrac_FAD()
% function tune_SNRfrac_FAD()
%
% Runs UL_FAD.m for specified values of the SNRfrac threshold.
% Plots upper limits (normalized by minimum upper limits)
% as a function of SNRfrac.  Results are saved in a .mat
% file for quick plotting in the future.
%
% Users may wish to change the SNRfrac_range and matfile
% name below, as well as the legend entries.
%
% Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% SNRfrac ranges
% Note: thresholds lower than 0.1 tend to cause problems
%       by removing basically all of the background events.
SNRfrac_range = 0.1:0.05:0.95;

% Name of matfile.
matfile_name = 'S6_FAD_tuning.mat';

% legend entries
legend_entries = {'ADI-A','ADI-B','ADI-C','ADI-D','ADI-E','LINE-A','LINE-B', ...
		  'MONO-A','MONO-B','QUAD-A','QUAD-B','SG-A','SG-B', ...
		  'WNB-A','WNB-B','WNB-C'};

% Line color and styles for plotting.
m={'-b','-r','-k','-g','-m','-c', ...
   '--b','--r','--k','--g','--m','--c', ...
   ':b',':r',':k',':g',':m',':c'};


if (exist(matfile_name) ~= 2)
  % If matfile doesn't exist, calculate
  % upper limits for different SNRfrac values.
  fprintf('Calculating upper limits.\n');

  % Loop over SNRfrac values.
  for ii=1:numel(SNRfrac_range)
    fprintf('Testing SNRfrac = %f.\n',SNRfrac_range(ii));
    all_ULs(ii,:) = UL_FAD(true,SNRfrac_range(ii));
  end
  
  % Save matfile with SNRfrac values and upper limit results.
  save(matfile_name,'all_ULs','SNRfrac_range');
else
  % Otherwise, load matfile.
  fprintf('Loading results of SNRfrac tuning.\n');
  load(matfile_name);
end

% Plot upper limits vs SNRfrac values.
close all;
figure;
for ii=1:size(all_ULs,2)
  norm_ULs = all_ULs(:,ii)/min(all_ULs(:,ii));
  plot(SNRfrac_range,norm_ULs,m{ii},'LineWidth',2);
  if (ii==1)
    hold on;
  end
end
hold off;
xlabel('SNRfrac');
ylabel('UL/UL_{min}');
legend(legend_entries);
largePlot(15,12);
ylim([0.95 1.25]);
pretty();
print('-dpng','SNRfrac_tuning_FAD.png');
close all;

return;