function [] = UL_REPORT()  
% UL_REPORT :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = UL_REPORT ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Jun 2016
%

  import classes.waveforms.dictionary

  % Load parameters for making report 
  params = UL_params();

  % Get number of searches.
  nSearches = numel(params.search_label);


  % ===================================================================
  %% FAD REPORT
  % ===================================================================
  % make a FAD report web page if FAD results exist
  %
    
  % Loop over searches.
  for ii=1:nSearches
    if exist([params.webFolder{ii} '/fad.txt']);
      report{ii}.FAD='fad/index.html';
    else
      continue
    end

    % get path
    injPath = generate_searchPath('INJ',params.GPS_start{ii},...
                                  params.GPS_end{ii},params.ID_inj{ii});
    
    % get the waveforms used in the search
    dic = dictionary.parse(['../../' injPath '/waveforms.txt']);
    
    % create a new html doc
    % and add style
    doc = html.document;
    doc.style = 'style.css';
    
    % add title
    doc.title = [params.search_label{ii} ' FAD upper limit report'];
    doc.createNode('h1',[params.search_label{ii} ' FAD upper limit report'],'Parent',doc.body);    
    
    % make a Breadcrumbs
    d=doc.createNode('div','','Attribute',struct('class','ariane'));
    doc.createNode('a','main','Attribute',struct('href','../'), ...
                   'Parent',d);
    d.appendChild(doc.createTextNode(' > FAD'));

    addToc(doc,dic,'');
    
    % -------------------------------------------------------------------
    %% Global plot
    % -------------------------------------------------------------------
    % simply add the global plot in the main page
    %
    doc.createNode('h2','Main plots','Parent',doc.body);
    div = doc.createNode('div','Attribute',struct('class','waveforms'),'Parent',doc.Body);
    t = doc.createNode('table','Parent',div);
    
    tr = doc.createNode('tr','Parent',t);
    addplot(doc,['figures/' params.search_label{ii} '_FAD_vs_SNR.png'],tr,'');
    addplot(doc,['figures/' params.search_label{ii} '_vvis_vs_FAD.png'],tr,'');
    addplot(doc,['figures/' params.search_label{ii} '_vvis_vs_SNR.png'],tr,'');


    % -------------------------------------------------------------------
    %% WAVEFORM REPORT
    % -------------------------------------------------------------------
    % for each waveform make a specific report page
    %
    for w = dic.headers'
      % create new html document
      wdoc = html.document;
      wdoc.style = 'style.css';
      wdoc.title = [w.legend ': FAD upper limit report page'];
      wdoc.createNode('h1',[w.legend ' FAD upper limit'],'Parent', ...
                      wdoc.body);

      % make a Breadcrumbs
      d=wdoc.createNode('div','','Attribute',struct('class','ariane'));
      wdoc.createNode('a','main','Attribute',struct('href','../../'), ...
                      'Parent',d);
      d.appendChild(wdoc.createTextNode(' > '));
      wdoc.createNode('a','FAD','Attribute',struct('href','../'), ...
                      'Parent',d);
      d.appendChild(wdoc.createTextNode([' > ' w.legend]));
      wdoc.createNode('h2','Plots','Parent',wdoc.body);
      
      % add plot
      div = wdoc.createNode('div','Attribute',struct('class','waveforms'),'Parent',wdoc.body);
      t = wdoc.createNode('table','Parent',div);
      tr = wdoc.createNode('tr','Parent',t);

      addplot(wdoc,['figures/' w.id '_FAD_vs_SNR.png'],tr,'');
      addplot(wdoc,['figures/' w.id '_nu_vs_FAD.png'],tr,'');
      addplot(wdoc,['figures/' w.id '_rate_PDF.png'],tr,'');
      
      tr = wdoc.createNode('tr','Parent',t);
      addplot(wdoc,['figures/' w.id '_vvis_vs_SNR.png'],tr,'');
      addplot(wdoc,['figures/' w.id '_vvis_vs_FAD.png'],tr,'');
      
      wdoc.write([params.webFolder{ii} '/fad/' w.id '/index.html'])
      system(['cp template/style.css ' params.webFolder{ii} '/fad/' w.id]);

    end

    % write down the web report page
    doc.write([params.webFolder{ii} '/fad/index.html']);
    system(['cp template/style.css ' params.webFolder{ii} '/fad/']);
  end


  % ===================================================================
  %% LES REPORT
  % ===================================================================
  %
  
  % Loop over searches.
  for ii=1:nSearches
    
    if exist([params.webFolder{ii} '/les.txt']);
      report{ii}.LES='les/index.html';
    else
      continue
    end
    
    % get path
    injPath = generate_searchPath('INJ',params.GPS_start{ii},...
                                  params.GPS_end{ii},params.ID_inj{ii});
    
    % get the waveforms used in the search
    dic = dictionary.parse(['../../' injPath '/waveforms.txt']);
    
    % create a new html doc
    % and add style
    doc = html.document;
    doc.style = 'style.css';
    
    % add title
    doc.title = [params.search_label{ii} ' LES upper limit report'];
    doc.createNode('h1',[params.search_label{ii} ' LES upper limit report'],'Parent',doc.body);    

    % make a Breadcrumbs
    d=doc.createNode('div','','Attribute',struct('class','ariane'));
    doc.createNode('a','main','Attribute',struct('href','../'), ...
                   'Parent',d);
    d.appendChild(doc.createTextNode(' > LES'));

    addToc(doc,dic,'#');

    % -------------------------------------------------------------------
    %% Global plot
    % -------------------------------------------------------------------
    % simply add the global plot in the main page
    %
    doc.createNode('h2','Main plots','Parent',doc.body);
    div = doc.createNode('div','Attribute',struct('class','waveforms'),'Parent',doc.Body);
    t = doc.createNode('table','Parent',div);

    currentCol=1;
    tr=doc.createNode('tr','Parent',t);
    for w = dic.headers'
      if currentCol>3
        tr=doc.createNode('tr','Parent',t);
        currentCol=1;
      else
        currentCol=currentCol+1;
      end
      addplot(doc,['figures/' w.id '_LES_upper_limits.png'],tr,w.id);
    end
    
    % write down the web report page
    doc.write([params.webFolder{ii} '/les/index.html']);
    system(['cp template/style.css ' params.webFolder{ii} '/les']);    
  end


 
  % ===================================================================
  %% UPPER LIMIT REPORT
  % ===================================================================
  %
  par = split_params(params);
  for ii=1:nSearches
    doc = html.document;
    doc.title = [params.search_label{ii} 'upper limit'];
    doc.style = 'style.css';
    
    doc.createNode('h1',[params.search_label{ii} 'upper limit']);
    
    doc.createNode('h2','Parameters','Parent',doc.body);
    div = doc.createNode('div','Attribute',struct('id','dic'),'Parent',doc.body);
    t = doc.createNode('table','Parent',div);
    addParam(t,par(ii));
    
    fn=fieldnames(report{ii});
    for jj = 1:numel(fn)
      tr = doc.createNode('tr','Parent',t);
      doc.createNode('th',fn{jj},'Parent',tr);
      td = doc.createNode('td','Parent',tr);
      doc.createNode('a','link','Attribute',struct('href',report{ii}.(fn{jj})),'Parent',td);
    end

    % write down the web report page
    doc.write([params.webFolder{ii} '/index.html']);
    system(['cp template/style.css ' params.webFolder{ii} '/']);    
  end
    

  % =====================================================================
  %% TOOL FUNCTION
  % =====================================================================
  %
  function addplot(doc,name,parent,id)
    if isempty(id)
      td=doc.createNode('td','Parent',parent);
    else
      td=doc.createNode('td','Attribute',struct('id',id),'Parent',parent);
    end
    a=doc.createNode('a','Attribute',...
                     struct('href',name),...
                     'Parent',td);
    doc.createNode('img','Attribute',...
                   struct('src',name),...
                   'Parent',a);
  end


  function addToc(doc,dic,link)
  % use the dic to create the ToC. the ToC is divide in two part :
  %   - physical waveform
  %   - ad-hoc waveform

    % get physical wavefoms & adhoc waveforms
    wvfs = dic.headers;
    phys_wvfs  = wvfs(arrayfun(@(h) strcmp(h.xlabel,'distance'),wvfs));
    adhoc_wvfs = wvfs(arrayfun(@(h) strcmp(h.xlabel,'hrss'),wvfs));

    % get physical & adhoc group
    phys_grp  = unique({phys_wvfs.group});
    adhoc_grp = unique({adhoc_wvfs.group});

    % add toc 
    doc.createNode('h2','dictionary','Parent',doc.body);
    div = doc.createNode('div','Attribute',struct('id','dic'),'Parent',doc.body);
    
    % deal with physical waveforms first
    t  = doc.createNode('table','Parent',div);
    tr = doc.createNode('tr','Parent',t);
    doc.createNode('td','Physical waveform',...
                   'Attribute',struct('class','tableTitle',...
                                      'colspan',num2str(numel(phys_wvfs))),...
                   'Parent',t);
    
    tr = doc.createNode('tr','Parent',t);
    for w=phys_grp
      doc.createNode('td',w{:},'Parent',tr);
    end
    
    currentWvfNb=1;
    while true
      tr = doc.createNode('tr','Parent',t);
      emptyWvf=0;
      for grp=phys_grp
        gwvf = phys_wvfs(strcmp({phys_wvfs.group},grp));
        if currentWvfNb<=numel(gwvf)
          td = doc.createNode('td','Parent',t);
          doc.createNode('a',gwvf(currentWvfNb).id,...
                         'Attribute',struct('href',[link gwvf(currentWvfNb).id]), ...
                         'Parent',td);
        else
          doc.createNode('td','','Parent',t);
          emptyWvf=emptyWvf+1;
        end
      end
      
      if emptyWvf == numel(phys_grp)
        break
      else
        currentWvfNb=currentWvfNb+1;
      end
    end

    % deal with adhoc waveforms
    t   = doc.createNode('table','Parent',div);
    tr = doc.createNode('tr','Parent',t);
    doc.createNode('td','Ad-hoc waveform',...
                   'Attribute',struct('class','tableTitle',...
                                      'colspan',num2str(numel(phys_wvfs))),...
                   'Parent',t);
    
    tr = doc.createNode('tr','Parent',t);
    for w=adhoc_grp
      doc.createNode('td',w{:},'Parent',tr);
    end
    
    currentWvfNb=1;
    while true
      tr = doc.createNode('tr','Parent',t);
      emptyWvf=0;
      for grp=adhoc_grp
        gwvf = adhoc_wvfs(strcmp({adhoc_wvfs.group},grp));
        if currentWvfNb<=numel(gwvf)
          td = doc.createNode('td','Parent',t);
          doc.createNode('a',gwvf(currentWvfNb).id,...
                         'Attribute',struct('href',[link gwvf(currentWvfNb).id]), ...
                         'Parent',td);
        else
          doc.createNode('td','','Parent',t);
          emptyWvf=emptyWvf+1;
        end
      end
      
      if emptyWvf == numel(adhoc_grp)
        break
      else
        currentWvfNb=currentWvfNb+1;
      end
    end

  end
  
  
  function addParam(table,p)
    fn=fieldnames(p);
    for i=1:length(fn)
      tr = doc.createNode('tr','Parent',table);
      doc.createNode('th',fn{i},'Parent',tr);
      if isnumeric(p.(fn{i}))|| islogical(p.(fn{i}))
        doc.createNode('td',num2str(p.(fn{i})),'Parent',tr);
        
      else
        doc.createNode('td',p.(fn{i}),'Parent',tr);          
      end
    end
  end
end

