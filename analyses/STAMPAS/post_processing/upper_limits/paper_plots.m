function paper_plots()
% function paper_plots()
%
% Makes plots of efficiency-based loudest event statistic
% upper limit curves vs. h_rss for each waveform family
% used in the S5/S6 STAMP all-sky search.
%
% Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% add utils
addpath('utils/');

% get upper limit curves
[hrss, ULs] = UL_LES(false,false);

% indices
adi_idx = [1 2 3 5];
sine_idx = [8 9 6 7 10 11];
sg_idx = 12:13;
wnb_idx = 14:16;
idx_cell = {adi_idx sine_idx sg_idx wnb_idx};

% legend titles
adi_leg = {'ADI-A','ADI-B','ADI-C','ADI-E'};
sine_leg = {'MONO-A','MONO-B','LINE-A','LINE-B','QUAD-A','QUAD-B'};
sg_leg = {'SG-A','SG-B'};
wnb_leg = {'WNB-A','WNB-B','WNB-C'};
leg_cell = {adi_leg sine_leg sg_leg wnb_leg};

% line types
adi_line = {'-b','--r','-.k','-g'};
sine_line = {'-b','-g','--r','--y','-.k','-.m'};
sg_line = {'-b','--r'};
wnb_line = {'-b','--r','-.k'};
line_cell = {adi_line sine_line sg_line wnb_line};

% family names
fam_name = {'ADI','Sinusoid','Sine-Gaussian','White Noise Burst'};
fam_abbrev = {'ADI','sine','sg','wnb'};

% ranges
xrange = {[2e-22 5e-19], [4e-22 2e-18], [1e-21 2e-18], [1e-21 2e-18]};

% useful variables
nFamilies = numel(idx_cell);

% Loop over families and make a plot for each family.
for ii=1:nFamilies
  close all;
  figure;

  % get family information
  idx_ii = idx_cell{ii};
  leg_ii = leg_cell{ii};
  line_ii = line_cell{ii};

  % number of waveforms in the family.
  nWfs = numel(idx_ii);

  % Loop over waveforms and add to plot.
  for jj=1:nWfs
    loglog(hrss{idx_ii(jj)},ULs{idx_ii(jj)},line_ii{jj},'LineWidth',2);
    
    if (jj==1)
      hold on;
    end
  end
  hold off;

%  xlabel('h$_{rss}$ [strain/Hz$^{1/2}$]','Interpreter','latex','FontName','Helvetica');
  set(gca,'XMinorTick','on','YMinorTick','on')
  xlim(xrange{ii});
  %title([fam_name{ii} ' upper limits vs.h_{rss}']);
  xlabh = get(gca,'XLabel');
  set(xlabh,'Position',get(xlabh,'Position') - [0 0.22 0])
  %largePlot(15,12);
  largePlot(9.33,7);
  grid on;
  legend(leg_ii);
  pretty(20);

  % Do axis labels after pretty() so we can set the font size individually.
  xlabel('h_{rss} [strain/Hz^{1/2}]','FontSize',22);
  %ylabel('Rate upper limit [yr^{-1}]','FontSize',22);
  ylabel('R_{90%,T} [yr^{-1}]','FontSize',22);

  set(gca,'Position',[.13 .14 .775 .815])
  print('-dpng',[fam_abbrev{ii} '_LES_upper_limits.png']);
  print('-depsc2',[fam_abbrev{ii} '_LES_upper_limits.eps']);
  close all;

end

% Loop over families and make a plot for each family.
for ii=1:1
  close all;
  figure;

  % get family information
  idx_ii = idx_cell{ii};
  leg_ii = leg_cell{ii};
  line_ii = line_cell{ii};

  % number of waveforms in the family.
  nWfs = numel(idx_ii);

  % Loop over waveforms and add to plot.
  d_conv = [9.221e-21 2.998e-20 3.117e-20 2.514e-20];
  for jj=1:nWfs
    dist = d_conv(jj)./hrss{idx_ii(jj)};
    loglog(dist,ULs{idx_ii(jj)},line_ii{jj},'LineWidth',2);

    if (jj==1)
      hold on;
    end
  end
  hold off;

%  xlabel('h$_{rss}$ [strain/Hz$^{1/2}$]','Interpreter','latex','FontName','Helvetica');
  set(gca,'XMinorTick','on','YMinorTick','on')
  xlim([8e-2 1e2]);
  %title([fam_name{ii} ' upper limits vs.h_{rss}']);
  xlabh = get(gca,'XLabel');
  set(xlabh,'Position',get(xlabh,'Position') - [0 0.22 0])
  %largePlot(15,12);
  largePlot(9.33,7);
  grid on;
  legend(leg_ii,'Location','NorthWest');
  pretty(20);

  % Do axis labels after pretty() so we can set the font size individually.
  xlabel('Distance [Mpc]','FontSize',22);
  %ylabel('Rate upper limit [yr^{-1}]','FontSize',22);
  ylabel('R_{90%,T} [yr^{-1}]','FontSize',22);

  set(gca,'Position',[.13 .14 .775 .815])
  print('-dpng',[fam_abbrev{ii} '_LES_upper_limits_dist.png']);
  print('-depsc2',[fam_abbrev{ii} '_LES_upper_limits_dist.eps']);
  close all;

end



return;