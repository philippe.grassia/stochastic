Last modified 17/04/2015
Contact mabizoua@lal.in2p3.fr

##################################
Purpose: generate plots and html report page

##################################
src/ 
- plots.m : generate general plots of different variables. Plots are generated in figures_<ID>.
- correlogram.m : generate the triggers' correlogram plots
- timebet.m : study the time between 2 consecutive triggers
- FAR.m : generate all FAR files and FAR plots
- windows_overlap.m : study the triggers' distribution between windows
- beg_end.m : study a pb with L1 S6 data (now obsolete)


- get_loudest_events.sh : generate the list of the loudest events (nb of loudest is an argument of the script)
- generate_html.sh : generate an HTML plot

##################################
How to execute:

- Update the ../searchParamaters.m file. Especially, for the zero-lag results, one needs to define which
background results to use. The ID_BKG is used to point to the right background ID results.

- Interactive mode (OFFLINE set to 0)
1. In a matlab interactive session one can execute each matlab macro.
(plots.m, correlogram.m timebet.m, FAR.m ....)
2. ./get_loudest_events.sh
3. ./generate_html.sh <option>

- Background mode (OFFLINE set to 1)
./background.sh <option>

##################################
Output:
an index.html file in
/home/$USER/public_html/${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/background_estimation

