function diagnostic(SearchPath,Data,varargin)  
% plots : make series of plot 
% DESCRIPTION :
%    plot the following graph:
%     - GSP vs SNR
%     - GPS vs SNRfrac
%     - GPS vs Dur
%     - Dur vs SNR
%     - SNRfrac vs SNR
%     - SNRfrac vs Dur
%     - SNRfrac vs SNR_clean
%     - GSP vs Fminy vs SNR (all trigger)
%     - GSP vs Fmin vs SNR (with various cut)
%     - GSP vs Fmax vs SNR (all trigger)
%     - GSP vs Fmax vs SNR (with various cut)
%     - Fmin vs SNR
%     - Fmax vs SNR
%     - Dur histo
%     - Events Nb vs Lag
%     - Events Nb vs Window
%     - EventsNb vs window histo
%     - SNR histo
%     - ra histo
%     - dec histo
%     - windows GPS time histo (SNR > thr)
%
% SYNTAX :
%   plots (SearchPath,Data,options)
% 
% INPUT : 
%    SearchPath : the path of the search folder
%    Data : STAMPAS data object
%    options : keys values input
%        Folder : figures folder by default searchPath/figures
%                 directory
%        Cut : cell array of cut, by default noCut 
% 
% AUTHOR : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.2
% DATE : Mar 2016
%

  import classes.utils.waitbar
  import classes.utils.logfile

  %---------------------------------------------------------------------
  %% INPUT PARSER
  %---------------------------------------------------------------------
  % 
  p=inputParser;
  addRequired(p,'SearchPath');
  addRequired(p,'Data');
  if verLessThan('matlab','8.2')
    addParamValue(p,'Folder','');
    addParamValue(p,'Cut',{'noCut'});
  else
    addParameter(p,'Folder','');
    addParameter(p,'Cut',{'noCut'});
  end
  parse(p,SearchPath,Data,varargin{:});
  
  data = p.Results.Data;
  searchPath = p.Results.SearchPath;
  if isempty(p.Results.Folder)
    ff = [searchPath '/figures'];
  else
    ff = p.Results.Folder;
  end
  cut = p.Results.Cut; 
  
  % if multiple cut add a clean cut that combine all the cut 
  if length(cut)~=1
    allCut=strcat(cut,'+');
    allCut=strcat(allCut{:});
  end  

  config=readConfig([searchPath '/config_bkg.txt']);
  
  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % check input
  % if cut is not pass as an input take no cut 
  % if seedless data, split data into coherent, ifo1, ifo2 data and
  % run diagnostic function for each of them
  % 
  waitbar('diagnostic [init]');
  logfile([searchPath '/logfiles/diagnostic.log']);
  logfile('diagnostic plot begin ...');
  
  % | order trigger by SNR
  if isstruct(data.format.SNR)
    data.sort('SNR.coherent','descend');
  else
    data.sort('SNR','descend');
  end
  SNR = data.SNR;
  
  % define snrfrac 
  SNRfrac=data.SNRfrac;
  
  % define variable
  det = {'H1','L1'};
  SNR_max     = max(SNR);
  SNRfrac_max = max(SNRfrac);
  
  % Frequency Notch
  freqNotch = load([searchPath '/tmp/frequencies.txt']);
  idx  = [0,find(diff(freqNotch)>1)];
  freqNotchBand = [freqNotch(idx(1:end -1)+1);...
                   freqNotch(idx(2:end))];
  
  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 
  % make various plot. 
  % in order to increase speed we define function and we use
  % cellfun for each cut
  % 
  close all

          
  %%  GPS vs SNR
  %---------------------------------------------------------------------
  % plot GPS time vs SNR ofr each ifo and cut
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs SNR'));
  logfile('plot : GPS vs SNR');
  
  function f1(cut,leg,idx)
    for ifo=1:2
      figure()
      hold all
      switch cut
        case 'noCut'
          plot(data.GPSstart,SNR,'+b');      
          ll={leg};          
          
        case 'clean'
          plot(data(idx).GPSstart,SNR(idx),'+b');      
          ll={leg};          
          
        otherwise
          plot(data.GPSstart,SNR,'+b');      
          plot(data(~idx).GPSstart,SNR(~idx),'*r');      
	  ll={'All triggers',strrep(leg,'<','>')};
      end
      hold off

      set(gca(),'YScale','log');
      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','SNR');
      set(get(gca(),'YLabel'),'Fontsize',15);
      
      l = legend(ll);
      set(l, 'Location', 'NorthWest');
      
      ylim([5 2*SNR_max]);
      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
      make_png([ff '/' cut], ['GPS' det{ifo} '_SNR']);
      close;
    end 
  end
  
  [leg,m,idx]=decode(cut,data);
  cellfun(@f1,cut,leg,idx);
  
  % allCut & clean version
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f1('clean','Remaining triggers',idx);
    f1('allCut','All Cut',idx);
  end

  
  %%  GPS vs SNRfrac
  %---------------------------------------------------------------------
  % plot GPS vs SNRfrac for each ifo and for various cut
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs SNRfrac'));
  logfile('plot : GPS vs SNRfrac');

  function f2(cut,leg,idx)
    for ifo=1:2
      figure()
      hold on
      switch cut
        case 'noCut'
          plot(data.GPSstart(['ifo' num2str(ifo)]),SNRfrac,'+b');
          ll=leg;
          
        case 'clean'
          plot(data(idx).GPSstart(['ifo' num2str(ifo)]),SNRfrac(idx),'+b');
          ll=leg;
          
        otherwise
          plot(data.GPSstart(['ifo' num2str(ifo)]),SNRfrac,'+b');
          plot(data(~idx).GPSstart(['ifo' num2str(ifo)]),SNRfrac(~idx),'*r');
          ll={'All triggers',strrep(leg,'<','>')};
      end        
      hold off

      set(gca(),'YScale','log');
      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','SNRfrac');
      set(get(gca(),'YLabel'),'Fontsize',15);
      
      l = legend(ll);
      set(l, 'Location', 'NorthWest');
      
      ylim([0 1]);
      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
      make_png([ff '/' cut], ['GPS' det{ifo} '_SNRfrac'])
      close;
    end
  end  
    
  [leg,m,idx]=decode(cut,data);
  cellfun(@f2,cut,leg,idx);

  % noCut & clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f2('clean','Remaining triggers',idx);
    f2('allCut','All Cut',idx);
  end


  %% GPS vs Duration
  %---------------------------------------------------------------------
  % plot duration vs GPS for each ifo and for various cut
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs duration'));
  logfile('plot : GPS vs duration');

  function f3(cut,leg,idx)
    for ifo=1:2
      figure()
      hold all
      switch cut
        case 'noCut'
          plot(data.GPSstart(['ifo' num2str(ifo)]), data.duration,'+b');
          ll=leg;
          
        case 'clean'
          plot(data(idx).GPSstart(['ifo' num2str(ifo)]), data(idx).duration,'+b');
          ll=leg;
          
        otherwise
          plot(data.GPSstart(['ifo' num2str(ifo)]), data.duration,'+b');
          plot(data(~idx).GPSstart(['ifo' num2str(ifo)]), data(~idx).duration,'*r');
          ll={'All triggers',strrep(leg,'<','>')};
      end
      hold off

      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','Duration [s]');
      set(get(gca(),'YLabel'),'Fontsize',15);
      
      l = legend(ll);
      set(l, 'Location', 'NorthWest');

      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);

      make_png([ff '/' cut], ['GPS' det{ifo} '_Duration'])
      close;
    end
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f3,cut,leg,idx);
  
  % allCut & clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f3('clean','Remaining triggers',idx);
    f3('allCut','All Cut',idx);
  end
    


  %% Duration vs SNR
  %---------------------------------------------------------------------
  % plot duration vs SNR for various cut
  %
  waitbar(sprintf('diagnostic : %30s','Duration vs SNR'));
  logfile('plot : Duration vs SNR');

  function f4(cut,leg,idx)
    figure()
    hold on
    switch cut
      case 'noCut'
        plot(data.duration,SNR,'+b');
        ll=leg;

      case 'clean'
        plot(data(idx).duration,SNR(idx),'+b');
        ll=leg;
        
      otherwise
        plot(data.duration,SNR,'+b');
        plot(data(~idx).duration,SNR(~idx),'*r');
        ll={'All triggers', strrep(leg,'<','>')};
    end
    hold off

    set(gca(),'Xscale','lin');
    set(gca(),'YScale','lin');

    set(gca(),'Box','on');
    set(get(gca(),'XLabel'),'String','Duration [s]');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','SNR');
    set(get(gca(),'YLabel'),'Fontsize',15);

    ylim([5 2*SNR_max]);

    l = legend(ll);
    set(l, 'Location', 'NorthWest');
    
    make_png([ff '/' cut], ['Duration_SNR']);
    close;
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f4,cut,leg,idx);

  % allCut & clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f4('clean','Remaining triggers',idx);
    f4('allCut','All Cut',idx);
  end



  %% SNRfrac vs SNR
  %---------------------------------------------------------------------
  % plot SNRfrac vs SNR for various cut
  %
  waitbar(sprintf('diagnostic : %30s','SNRfrac vs SNR'));
  logfile('plot : SNRfrac vs SNR');

  function f5(cut,leg,idx)
    figure()
    hold on
    switch cut
      case 'noCut'
        plot(SNRfrac,SNR,'+b');
        ll=leg;

      case 'clean'
        plot(SNRfrac(idx),SNR(idx),'+b');
        ll=leg;
        
      otherwise
        plot(SNRfrac,SNR,'+b');
        plot(SNRfrac(~idx),SNR(~idx),'*r');
        ll={'All triggers', strrep(leg,'<','>')};
    end
    hold off
    
    set(gca(),'YScale','log');
    set(gca(),'Box','on');
    set(get(gca(),'XLabel'),'String','SNRfrac');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','SNR');
    set(get(gca(),'YLabel'),'Fontsize',15);

    xlim([0 1.1])
    ylim([5 2*SNR_max]);
    
    l = legend(ll);
    set(l, 'Location', 'NorthWest');
    
    make_png([ff '/' cut], ['SNRfrac_SNR']);
    close;
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f5,cut,leg,idx);

  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f5('clean','Remaning triggers',idx);
    f5('allCut','All Cut',idx);
  end
  

  %% SNRfrac vs Dur
  %---------------------------------------------------------------------
  % plot SNRfrac vs duration for various cut
  %
  waitbar(sprintf('diagnostic : %30s','SNRfrac vs Duration'));
  logfile('plot : SNRfrac vs Duration');

  function f6(cut,leg,idx)
    figure()
    hold on
    switch cut
      case 'noCut'
        plot(SNRfrac,data.duration,'+b');
        ll = leg;

      case 'clean'
        plot(SNRfrac(idx),data(idx).duration,'+b');
        ll = leg;
        
      otherwise
        plot(SNRfrac,data.duration,'+b');
        plot(SNRfrac(~idx),data(~idx).duration,'*r');
        ll={'All triggers', strrep(leg,'<','>')};
    end
    hold off

    set(gca(),'YScale','log');
    set(gca(),'Box','on');
    set(get(gca(),'XLabel'),'String','SNRfrac');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','Duration [s]');
    set(get(gca(),'YLabel'),'Fontsize',15);
    
    xlim([0 1.1])
    
    l = legend(ll);
    set(l, 'Location', 'NorthWest');
    
    make_png([ff '/' cut], ['SNRfrac_Duration'])
    close;
  end  

  [leg,m,idx]=decode(cut,data);
  cellfun(@f6,cut,leg,idx);
  
  % allCut & clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f6('allCut','All Cut',idx);
    f6('clean','Remaining triggers',idx);
  end
                

  %% GPS vs Fmin vs SNR all triggers
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs Fmin'));
  logfile('plot : GPS vs Fmin ');

  function f10(cut,leg,idx)
    for ifo=1:2
      figure();
      idx=idx&(data.SNR>25);
%      scatter(data(idx).GPSstart(['ifo' num2str(ifo)]), ...
%              data(idx).fmin, [], min(SNR(idx),50), '+');
      scatter(data(idx).GPSstart(['ifo' num2str(ifo)]), ...
              data(idx).fmin, [], SNR(idx), '+');

      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','Minimal frequency [Hz]');
      set(get(gca(),'YLabel'),'Fontsize',15);

      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);

      c = colorbar;
      title(c, 'SNR');

      make_png([ff '/' cut], ['GPS' det{ifo} '_Fmin_SNR_scatter'])
      close;
    end
  end

  [leg,m,idx]=decode(cut,data);

  cellfun(@f10,cut,leg,idx);
  
  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f10('allCut','All Cut',idx);
    f10('clean','Remaining triggers',idx);
  end


  %% GPS vs Fmax vs SNR all triggers
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs Fmax'));
  logfile('plot : GPS vs Fmax');

  function f11(cut,leg,idx)
    for ifo=1:2
      figure();
      idx=idx&(data.SNR>25);
      scatter(data(idx).GPSstart(['ifo' num2str(ifo)]), ...
              data(idx).fmax, [], min(SNR(idx),50), '+');

      set(gca(),'Box','on');
      set(get(gca(),'XLabel'),'String',[det{ifo} ' GPS']);
      set(get(gca(),'XLabel'),'Fontsize',15);
      set(get(gca(),'YLabel'),'String','Maximal frequency [Hz]');
      set(get(gca(),'YLabel'),'Fontsize',15);

      xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
            max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);

      c = colorbar;
      title(c, 'SNR');

      make_png([ff '/' cut], ['GPS' det{ifo} '_Fmax_SNR_scatter'])
      close;
    end
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f11,cut,leg,idx);
  
  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f11('allCut','All Cut',idx);
    f10('clean','Remaining triggers',idx);
  end


  %% Fmin vs SNR
  %---------------------------------------------------------------------
  % plot Fmin vs SNR  for various cut
  %
  waitbar(sprintf('diagnostic : %30s','Fmin vs SNR'));
  logfile('plot : Fmin vs SNR');

  function f7(cut,leg,idx)  
    figure
    hold on
    switch cut
      case 'noCut'
        plot(data.fmin,SNR,'+b');
        ll={leg};

      case 'clean'
        plot(data(idx).fmin,SNR(idx),'+b');
        ll={leg};
        
      otherwise
        plot(data.fmin,SNR,'+b');
        plot(data(~idx).fmin,SNR(~idx),'*r');
        ll={'All triggers',strrep(leg,'<','>')};
    end
    
    xlimit=get(gca, 'xlim');
    for fq=1:size(freqNotchBand,2)
      h=rectangle('position', ...
                  [freqNotchBand(1,fq) 5 ...
                   max(diff(freqNotchBand(:,fq)),1) 2*SNR_max-5], ...
                  'FaceColor','g', 'EdgeColor','g' );
      uistack(h,'bottom');
    end
    hold off

    %trick for the rectangle legend
    line(0, 0,'LineWidth',5 ,'Color','g');
    
    set(gca(),'YScale','log');
    set(gca(),'Box','on');
    set(get(gca(),'XLabel'),'String','Minimal frequency [Hz]');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','SNR');
    set(get(gca(),'YLabel'),'Fontsize',15);
      
    ylim([5 2*SNR_max]);
    xlim(xlimit);
    
    l=legend([ll 'Frequency notch']);
    set(l, 'Location', 'NorthEast');
    
    make_png([ff '/' cut], ['MinFreq_SNR'])
    close;
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f7,cut,leg,idx);
  
  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f7('allCut','All Cut',idx);
    f7('clean','Remaining triggers',idx);
  end

  %% Fmax vs SNR
  %---------------------------------------------------------------------
  % plot Fmax vs SNR plot various cut
  %
  waitbar(sprintf('diagnostic : %30s','Fmax vs SNR'));
  logfile('plot : Fmax vs SNR');

  function f8(cut,leg,idx)
    figure()
    hold on
    switch cut
      case 'noCut'
        plot(data.fmax,SNR,'+b');
        ll={leg};
        
      case 'clean'
        plot(data(idx).fmax,SNR(idx),'+b');
        ll={leg};
        
      otherwise
        plot(data.fmax,SNR,'+b');
        plot(data(~idx).fmax,SNR(~idx),'*r');
        ll={'All triggers', strrep(leg,'<','>')};
    end

    ylimit=get(gca,'ylim');
    xlimit=get(gca, 'xlim');
    for fq=1:size(freqNotchBand,2)
      h=rectangle('position', ...
                  [freqNotchBand(1,fq) 5 ...
                   max(diff(freqNotchBand(:,fq)),1) 2*ylimit(2)-5], ...
                  'FaceColor','g', 'EdgeColor','g' );
      uistack(h,'bottom');
    end
    hold off

    %trick for the rectangle legend
    line(0, 0,'LineWidth',5 ,'Color','g');

    set(gca(),'YScale','log');
    set(gca(),'Box','on');
    set(get(gca(),'XLabel'),'String','Maximal frequency [Hz]');
    set(get(gca(),'XLabel'),'Fontsize',15);
    set(get(gca(),'YLabel'),'String','SNR');
    set(get(gca(),'YLabel'),'Fontsize',15);

    ylim([5 2*ylimit(2)]);
    xlim(xlimit);

    l=legend([ll 'Frequency notch']);
    set(l, 'Location', 'NorthEast');
    
    make_png([ff '/' cut], ['MaxFreq_SNR'])
    close;
  end

  [leg,m,idx]=decode(cut,data);
  cellfun(@f8,cut,leg,idx);
  
  % clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f8('allCut','All Cut',idx);
    f8('clean','Remaining triggers',idx);
  end


  %% GPS vs SNR
  %---------------------------------------------------------------------
  % plot GPS vs SNR distribution
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs SNR'));
  logfile('plot : GPS vs SNR');

  for ifo=1:2
    figure()
    hist33 (data.GPSstart(['ifo' num2str(ifo)]), ...
            log10(SNR),100,100)
    xlabel([det{ifo} ' GPS'], 'Fontsize', 15);
    ylabel('log_{10}(SNR)', 'Fontsize', 15);
    xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
          max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
    make_png([ff '/diagnostic'], ['GPS' det{ifo} '_SNR_density'])
    close;
  end
  
  %% Fmin vs Dur
  %---------------------------------------------------------------------
  % plot Fmin sv SNR distribution
  %
  waitbar(sprintf('diagnostic : %30s','Fmin vs Duration'));
  logfile('plot : Fmin vs Duration');

  figure()
  hist33 (data.fmin, data.duration,100,100, 'intbinsy')
  xlabel('Minimal frequency [Hz]', 'Fontsize', 15);
  ylabel('Duration', 'Fontsize', 15);
  make_png([ff '/diagnostic'], 'Freq_Duration_density')
  close;


  %% GPS vs Fmin
  %---------------------------------------------------------------------
  % plot Fmin vs SNR distribution
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs Fmin'));
  logfile('plot : GPS vs Fmin');

  for ifo=1:2
    figure()
    hist33 (data.GPSstart(['ifo' num2str(ifo)]), ...
            data.fmin,100,100)
    xlabel([det{ifo} ' GPS'], 'Fontsize', 15);
    ylabel('Minimal Frequency [Hz]', 'Fontsize', 15);
    xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
          max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
    make_png([ff '/diagnostic'], ['GPS' det{ifo} '_Freq_density'])
    close;
  end

  %% Fmin vs SNR
  %---------------------------------------------------------------------
  % plot Fmin vd SNR distribution
  %
  waitbar(sprintf('diagnostic : %30s','Fmin vs SNR'));
  logfile('plot : Fmin vs SNR');

  figure()
  hist33 (data.fmin, SNR,100,100)
  xlabel('Minimal frequency [Hz]', 'Fontsize', 15);
  ylabel('SNR', 'Fontsize', 15);
  make_png([ff '/diagnostic'], 'Fmin_SNR_density')
  close;


  %% Fmean vs SNR
  %---------------------------------------------------------------------
  % plot Fmin vd SNR distribution
  %
  waitbar(sprintf('diagnostic : %30s','Fmean vs SNR'));
  logfile('plot : Fmean vs SNR');

  figure()
  hist33 (mean([data.fmin,data.fmin],2), SNR,100,100)
  xlabel('Mean frequency [Hz]', 'Fontsize', 15);
  ylabel('SNR', 'Fontsize', 15);
  make_png([ff '/diagnostic'], 'Fmean_SNR_density')
  close;

  
  %% GPS vs Dur
  %---------------------------------------------------------------------
  % plot SNR vs duration distribution
  %
  waitbar(sprintf('diagnostic : %30s','GPS vs Duration'));
  logfile('plot : GPS vs Duration');

  for ifo=1:2
    figure()
    hist33 (data.GPSstart(['ifo' num2str(ifo)]), ...
            data.duration,100,50)
    xlabel([det{ifo} ' GPS'], 'Fontsize', 15);
    ylabel('Duration [s]', 'Fontsize', 15);
    xlim([min(data.GPSstart(['ifo' num2str(ifo)]))-100000,...
          max(data.GPSstart(['ifo' num2str(ifo)]))+100000]);
    make_png([ff '/diagnostic'], ['GPS' det{ifo} '_Duration_density'])
  end

  %% lag vs Dur
  %---------------------------------------------------------------------
  % plot lag vs duration distribution
  if config.doZebra==1
    waitbar(sprintf('diagnostic : %30s','lag vs Duration'));
    logfile('plot : lag vs Duration');

    figure()
    hist33(data.lag,data.duration,200,50)
    xlabel('Lag #', 'Fontsize', 15);
    ylabel('Duration [s]', 'Fontsize', 15);
    make_png([ff '/diagnostic'], 'Lag_Duration_density')
    close;
  end
  %% Duration histo
  %---------------------------------------------------------------------
  % plot duration distribution
  waitbar(sprintf('diagnostic : %30s','Duration histo'));
  logfile('plot : Duration histo');

  figure()

  if config.doZebra==1
    xmax=60;
    x=0:.5:xmax;
    outsider=data.duration>xmax;
  else
    xmax=250;
    x=0:1:xmax;
    outsider=data.duration>xmax;
  end

  hist(data(~outsider).duration,x);
  h = findobj(gca,'Type','patch');
  set(h(1),'FaceColor','none','EdgeColor','k');
  xlim([0 xmax+5]);
  grid
  xlabel('Duration [s]', 'Fontsize', 15);
  ylabel('Events nb', 'Fontsize', 15);
  legend(['Overflow (>' num2str(xmax) ') : ' ...
         num2str(sum(outsider)) '[' ...
         num2str(round(1000*sum(outsider)/data.numTriggers)/10) '%]'])
  make_png([ff '/diagnostic'], 'Duration_histo');
  close;

  %% EventsNb vs Lag
  %---------------------------------------------------------------------
  % 
  waitbar(sprintf('diagnostic : %30s','Nb events vs lag'));
  logfile('plot : Nb events vs lag');

  figure()
  lag_max=max(data.lag);
  lag_min=min(data.lag);
  a=zeros(lag_max-lag_min+1,3);
  lags = lag_min:lag_max;
  for i=1:numel(lags)
    ext=data.lag==lags(i);
    a(i,1)=lags(i);
    a(i,2)=sum(ext,1);
    a(i,3)=mean(SNR(ext));
  end

  average=mean(a(:,2));
  sigma=std(a(:,2));

  bar(a(:,1), a(:,2), 'BaseValue', average*.9);
  xlim([lag_min-1 lag_max+1])
  ylim([average*.9 average*1.1])
  line([lag_min lag_max], [average average],'Color','red')
  line([lag_min lag_max], [average-sigma average-sigma],'Color', ...
       'red','LineStyle','--')
  line([lag_min lag_max], [average+sigma average+sigma],'Color', ...
       'red','LineStyle','--')

  legend('data', ...
         ['mean: ' num2str(average,'%6.1f')], ...
         ['sigma:' num2str(sigma,'%6.1f')])

  xlabel('Lag #', 'Fontsize', 15);
  ylabel('Events nb', 'Fontsize', 15);
  make_png([ff '/diagnostic'], 'EventsNb_Lag')
  close;
  clear a;


  %% EventsNb vs windowIdx
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','Nb events vs windows idx'));
  logfile('plot : Nb events vs windows idx');

  figure()
  windows=unique(data.windows.idx);
  window_nb=size(windows,1);
  window_max=max(data.windows.idx);

  tab=zeros(window_max,2);
  x=1:1:window_max;

  [N,BIN]=histc(data.windows.idx,x);
  bar(1:window_max, N, 'b')
  xlim([windows(1)-10 windows(end)+10])

  average = mean(N);
  sigma   = std(N);
  line([1 window_max], [average average],'Color','red')
  line([1 window_max], [average-sigma average-sigma],'Color','red', ...
       'LineStyle','--')
  line([1 window_max], [average+sigma average+sigma],'Color','red', ...
       'LineStyle','--')
  legend('All triggers', ...
         ['mean :' num2str(average,'%6.1f')], ...
         ['sigma :' num2str(sigma,'%6.1f')], ...
         'Location','SouthEast');
  xlabel('Window #','Fontsize', 15)
  ylabel('Events nb / window','Fontsize', 15)
  make_png([ff '/diagnostic'], 'EventsNb_window')
  close;

  %% EventsNb vs windows idx histo 
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','Nb events vs windows idx histo'));
  logfile('plot : Nb events vs windows idx histo');

  try
    figure()
    histlog(N,1,'b');
    xlabel('Events nb / window','Fontsize', 15)
    ylabel('Counts','Fontsize', 15)
    [max_nb,id]=max(N);
    xlim([0 1.05*max_nb]);
    ylim_sup=get(gca,'ylim');
    line([average average], [ylim_sup(1) ylim_sup(2)], 'Color','red')
    line([average-sigma average-sigma], [ylim_sup(1) ylim_sup(2)], ...
         'Color','red','LineStyle','--')
    line([average+sigma average+sigma], [ylim_sup(1) ylim_sup(2)], ...
         'Color','red','LineStyle','--')
    legend('All triggers', ...
         ['mean :' num2str(average,'%6.1f')], ...
         ['sigma :' num2str(sigma,'%6.1f')], ...
         'Location','Best');
    make_png([ff '/diagnostic'], 'EventsNb_window_histo')
    close;
  catch
  end

  %% SNR histo
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','SNR histo'));
  logfile('plot : SNR histo');
  
  function f9(cut,leg,idx)
    figure
    SNR_max=max(SNR);
    if SNR_max>40
      xmax=40; 
      x=6:.1:xmax;
      ss=SNR<xmax;
    else
      xmax=10;
      x=2:.1:xmax;
      ss=SNR<xmax;
    end

    hold all
    switch cut
      case 'noCut'
        histlog(SNR(ss),x,[0 0 0]);
        ll={leg};

      case 'clean'
        histlog(SNR(ss&idx),x,[0 0 0]);
        ll={leg};

      otherwise
        histlog(SNR(ss),x,[0 0 0]);
        histlog(SNR(ss&idx),x,[1 0 0]);
        ll={'All triggers',strrep(leg,'<','>')};
    end  
    hold off

    set(gca(),'Box','on');        
    set(gca(),'XMinorGrid','on')
    set(gca(),'YMinorGrid','on')
    set(gca(),'XLim',[0 xmax+.1]);
    yl=get(gca(),'YLim');
    set(gca(),'YLim',[.1 1.1*yl(2)]);

    xlabel('SNR', 'Fontsize', 15);
    ylabel('Events nb', 'Fontsize', 15);
    title(['Overflow (SNR>' num2str(xmax) ') : ' ...
	   num2str(sum(~ss)) ...
	   '[' num2str(round(1000*sum(~ss)/data.numTriggers)/10) '%]']);

    legend(ll);
    make_png([ff '/' cut], ['SNR_histo'])
    close;
  end  

  [leg,~,idx]=decode(cut,data);
  cellfun(@f9,cut,leg,idx);
  
  % allCut & clean plot
  if length(cut)~=1
    [~,~,idx]=decode(allCut,data);
    f9('allCut','All Cut',idx);
    f9('clean','Remaining triggers',idx);
  end


  %% RA
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','Ra histo'));
  logfile('plot : Ra histo');

  figure()
  
  [N]=hist(data.ra,24);
  hist(data.ra,24);
  xlabel('RA [h]', 'fontsize', 15);
  ylabel('Counts', 'fontsize', 15)
  xlim([-1 25])

  average = mean(N);
  sigma   = std(N);

  line([0 24], [average average],'Color','red')
  line([0 24], [average-sigma average-sigma],'Color',...
       'red','LineStyle','--');
  line([0 24], [average+sigma average+sigma],'Color',...
       'red','LineStyle','--');
  legend('All triggers', ...
         ['mean: ' num2str(average,'%6.1f')], ...
         ['sigma:' num2str(sigma,'%6.1f')],'Location','SouthEast')
  make_png([ff '/diagnostic'], 'RA')
  close;


  %% Dec
  %---------------------------------------------------------------------
  %
  waitbar(sprintf('diagnostic : %30s','Dec histo'));
  logfile('plot : Dec histo');

  figure()
  [N]=hist(cos(data.dec*pi/180+pi/2),90);
  hist(cos(data.dec*pi/180+pi/2),90);
  xlabel('cos(dec+\pi/2) [deg]', 'fontsize', 15);
  ylabel('Counts', 'fontsize', 15)
  xlim([-1.05 1.05])
  
  average=mean(N);
  sigma=std(N);

  line([-1 1], [average average],'Color','red')
  line([-1 1], [average-sigma average-sigma],'Color',...
       'red','LineStyle','--');
  line([-1 1], [average+sigma average+sigma],'Color',...
       'red','LineStyle','--');
  legend('All triggers', ...
         ['mean: ' num2str(average,'%6.1f')], ...
         ['sigma:' num2str(sigma,'%6.1f')],'Location','SouthEast')
  make_png([ff '/diagnostic'], 'dec')
  close;



  %---------------------------------------------------------------------
  %% Close Function 
  %---------------------------------------------------------------------
  % display end message and delete waitbar & logfile
  %
  logfile('Diagnostic end without errors');
  waitbar(sprintf('diagnostic : [done] %30s',''));
  
  delete(logfile);
  delete(waitbar);


end
