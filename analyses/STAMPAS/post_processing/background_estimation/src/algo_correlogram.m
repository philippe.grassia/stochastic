function myh = algo_correlogram (evl1, evl2, timeidx, deltat, binnb)

myh = zeros (binnb,1);

ev1nb = length (evl1);
ev2nb = length (evl2);

delta = deltat*binnb + 1;

for (i=1:ev1nb)
  if (mod(i,10000)==0)
    disp(['rate ' num2str(i) '/' num2str(ev1nb)]);
  end

  extr = find (evl1(i,timeidx) - evl2 (:, timeidx) > 0 & evl1(i,timeidx) - evl2 (:, timeidx) < delta);

  for (j=1:size(extr))
    dist = evl1(i,timeidx) - evl2(extr(j),timeidx);
    myh = addToH(dist,myh,deltat);
  end
end


function his = addToH(distance,myhis,dtime)
hnb = length(myhis);
idx=ceil(distance/dtime);
his=myhis;
if (idx<=hnb)
    his(idx) = his(idx)+1;
end

