function [F] = get_far(SNR,FAR)  
% get_far : get the FAR of the triggers
% DESCRIPTION :
%   compute the far of triggers from a distribution
%
% SYNTAX :
%   [f] = get_far (snr,far,livetime)
% 
% INPUT : 
%    snr : array of triggers SNR
%    far : struct containing FAR distribution
%      - snr : array of snr
%      - far : array of FAR
%    livetime : lifetime of the run 
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : MAY 2017
%
  FAR.snr=FAR.snr(FAR.far>0);
  FAR.far=FAR.far(FAR.far>0);
  F = interp1(FAR.snr,FAR.far,SNR, 'linear','extrap');
end