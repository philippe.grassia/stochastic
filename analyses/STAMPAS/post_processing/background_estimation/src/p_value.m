function [P] = p_value(SNR,FAR,LIVETIME)  
% p_value : compute p_value for a give SNR
% DESCRIPTION :
%   compute the p_value for given triggers SNR 
%
% SYNTAX :
%   [p] = p_value (snr,far,livetime)
% 
% INPUT : 
%    snr = array of snr triggers
%    far : struct fo far dirstibution
%         - far : array of far value
%         - snr : array of snr 
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : MAY 2017
%

P = zeros(size(SNR));

for i=1:length(SNR)
  idx = SNR(i)>=FAR.snr(1:end-1)&SNR(i)<=FAR.snr(2:end);
  P(i) = 1 - exp(-FAR.far(idx)*LIVETIME);
end
