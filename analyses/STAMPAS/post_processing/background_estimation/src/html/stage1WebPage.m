function [] = stage1WebPage(params)  
% stage1WebPage :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = stage1WebPage ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%
  params.webpath = regexprep(params.webpath,'stage1','');

  % create a new html document
  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');
  
  % <h1> title
  doc.createNode('h1','STAMPAS report page');
  
  doc.createNode('h2','Search informations');

  d=doc.createNode('div','','Attribute',struct('class','info'));
  t=doc.createNode('table','Parent',d);
  
  % header
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Lonetrack search',...
                 'Attribute',struct('colspan','2'),...
                 'Parent',tr);
  
  % stage 1 link
 
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Stage 1',...
                 'Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','results',...
                 'Attribute',struct('href','stage1'),...
                 'Parent',tr);

  %{
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','ifo1 results',...
                 'Attribute',struct('href','ifo1'),...
                 'Parent',tr);
  
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','ifo2 results',...
                 'Attribute',struct('href','ifo2'),...
                 'Parent',tr);
  %}
  
  % stage 2 link
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Stage 2',...
                 'Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','results',...
                 'Attribute',struct('href','stage2'),...
                 'Parent',tr);
  

  system(['cp template/style.css ' params.webpath '/style.css']);
  doc.write([params.webpath '/index.html']);
  
  
end

