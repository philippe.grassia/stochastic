function [] = stage2WebPage(params)  
% stage2WebPage :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = stage2WebPage ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%
  params.webpath = regexprep(params.webpath,'stage1','');

  % create a new html document
  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');
  
  % <h1> title
  doc.createNode('h1','STAMPAS report page');

  d=doc.createNode('div','',...
                   'Attribute',struct('class','ariane'));
  doc.createNode('a','main',...
                 'Attribute',struct('href','../'), ...
                 'Parent',d);
  d.appendChild(doc.createTextNode([' > Stage 2']));

  
  % -------------------------------------------------------------
  %% FAP PLOTS
  % -------------------------------------------------------------
  %
  doc.createNode('h2','FAP');
  d=doc.createNode('div','','Attribute',struct('class','plots'));
  t=doc.createNode('table','Parent',d);
  
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Lonetrack search',...
                 'Attribute',struct('colspan','2'),...
                 'Parent',tr);

  tr = doc.createNode('tr','Parent',t);
  addplot(doc,'figures/snrmax_htrack_combine.png',tr);
  addplot(doc,'figures/snrmax_htrack_combine_zoom.png',tr);

  tr = doc.createNode('tr','Parent',t);
  addplot(doc,'figures/snrmax_htrack_bknd_combine.png',tr);
  addplot(doc,'figures/snrmax_htrack_bknd_combine_zoom.png',tr);


  % -------------------------------------------------------------

  %% DISTRIBUTION
  % -------------------------------------------------------------
  %
  doc.createNode('h2','Distribution');
  d=doc.createNode('div','','Attribute',struct('class','plots'));
  t=doc.createNode('table','Parent',d);
  
  tr = doc.createNode('tr','Parent',t);
  addplot(doc,'figures/snrmax_times.png',tr);
  addplot(doc,'figures/snrmax_times_heatmap.png',tr);

  tr = doc.createNode('tr','Parent',t);
  addplot(doc,'figures/timedelay_hist.png',tr);
  addplot(doc,'figures/timedelay_times_heatmap.png',tr);

  if ~exist([params.webpath '/stage2'])
    system(['mkdir ' params.webpath '/stage2']);
    system(['mkdir ' params.webpath '/stage2/figures']);
  end

  system(['cp ' params.searchPath  '/figures/LonetrackPProc/*.png ' ...
          params.webpath '/stage2/figures']);
  system(['cp template/style.css ' params.webpath '/stage2']);
  doc.write([params.webpath '/stage2/index.html']);
  
  
end

