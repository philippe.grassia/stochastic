function [] = diagnosticWebPage(params)  
% diagnosticWebPage : generate html page showing STAMPAS diagnostic
% DESCRIPTION :
%    generate web report page containing information and sanity
%    check for STAMPAS
% 
%   <h1> STAMPAS report page </h1>
%   
%   <h2> Diagnotci plots </h2>
%    <div class='plots'>
%      <table>
%        * table of diagnotic plots *
%      </table>
%    </div>
%
% SYNTAX :
%   diagnosticWebPage (paramas)
% 
% INPUT : 
%   params : html parameters
%  
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%

% create a new html document
  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');
  
  % <h1> title
  doc.createNode('h1','STAMPAS report page');
  
  % add a ariane string for navigation
  d=doc.createNode('div','',...
                   'Attribute',struct('class','ariane'));
  doc.createNode('a','main',...
                 'Attribute',struct('href','../'), ...
                 'Parent',d);
  d.appendChild(doc.createTextNode([' > diagnostic']));


  doc.createNode('h2','diagnostic plots');

  d=doc.createNode('div','Attribute',struct('class','plots'));
  t=doc.createNode('table','Parent',d);
  
  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/GPSH1_Freq_density.png'],tr);
  addplot(doc,['figures/GPSL1_Freq_density.png'],tr);
  addplot(doc,['figures/Freq_Duration_density.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Fmin_SNR_density.png'],tr);
  addplot(doc,['figures/Fmean_SNR_density.png'],tr);
  addplot(doc,['figures/Duration_histo.png'],tr);

  if params.config.doZebra
    tr=doc.createNode('tr','Parent',t);
    addplot(doc,['figures/GPSH1_Duration_density.png'],tr);
    addplot(doc,['figures/GPSL1_Duration_density.png'],tr);
    addplot(doc,['figures/ConEventTimeDiff_Duration.png'],tr);
    
    tr=doc.createNode('tr','Parent',t);
    addplot(doc,['figures/ConEventTimeDiff.png'],tr);
    addplot(doc,['figures/ConEventTimeDiff_zoom.png'],tr);
    addplot(doc,['figures/ConEventTimeDiff_NbEvent_Duration.png'],tr);
    
    tr=doc.createNode('tr','Parent',t);
    addplot(doc,['figures/RA.png'],tr);
    addplot(doc,['figures/dec.png'],tr);
    addplot(doc,['figures/Autocorrelogram_10.png'],tr);
  else
    tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/GPSH1_Duration_density.png'],tr);
  addplot(doc,['figures/GPSL1_Duration_density.png'],tr);
  end    


  % -------------------------------------------------------------------------
  %% Lonetrack specific part
  % -------------------------------------------------------------------------
  %

  if params.config.doZebra==0
  
    % -------------------------------------------------------------------------
    % Distribution
    
    doc.createNode('h2','Distribution');
    d=doc.createNode('div','','Attribute',struct('class','plots'));
    t=doc.createNode('table','Parent',d);
    
    tr = doc.createNode('tr','Parent',t);
    addplot(doc,'figures/snrmax_times.png',tr);
    addplot(doc,'figures/snrmax_times_heatmap.png',tr);
    
    tr = doc.createNode('tr','Parent',t);
    addplot(doc,'figures/timedelay_hist.png',tr);
    addplot(doc,'figures/timedelay_times_heatmap.png',tr);

    % -------------------------------------------------------------------------
    % FAP
  
    doc.createNode('h2','FAP');
    d=doc.createNode('div','','Attribute',struct('class','plots'));
    t=doc.createNode('table','Parent',d);
    
    tr = doc.createNode('tr','Parent',t);
    addplot(doc,'figures/snrmax_htrack_combine.png',tr);
    addplot(doc,'figures/snrmax_htrack_combine_zoom.png',tr);
    
    tr = doc.createNode('tr','Parent',t);
    addplot(doc,'figures/snrmax_htrack_bknd_combine.png',tr);
    addplot(doc,'figures/snrmax_htrack_bknd_combine_zoom.png',tr);
    
  end

 
  % -------------------------------------------------------------------------
  %% Write Web Page
  % -------------------------------------------------------------------------
  %
  if ~exist([params.webpath '/diagnostic'])
    system(['mkdir ' params.webpath '/diagnostic']);
    system(['mkdir ' params.webpath '/diagnostic/figures']);
  end

  system(['cp -r ' params.figuresPath '/diagnostic/*.png ' ...
          params.webpath '/diagnostic/figures/.']);
  system(['cp -r  ./template/style.css ' params.webpath '/diagnostic' ...
          '/style.css']);
  
  doc.write([params.webpath '/diagnostic/index.html']);
  

end

