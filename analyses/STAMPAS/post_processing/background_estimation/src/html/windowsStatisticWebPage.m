function windowsStatisticWebPage(params)  
% windowsStatisticWebPage : generate html document display windows statistic
% DESCRIPTION :
%   generate web page containing information about windows
%   statistic
%   
%  <h1> STAMPAS report page </h1>
%
%  <h2> Windows statistic plot </h2>
%  <div class='plots'>
%    <table>
%      * table of plots *
%    </table>
%  </div>
%   
% SYNTAX :
%   windowsStatisticWebPage (params)
% 
% INPUT : 
%    params : html parameters
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%
  
  % create a new html document
  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');

  % <h1> title
  doc.createNode('h1','STAMPAS report page');

  % add a ariane string for navigation
  d=doc.createNode('div','',...
                   'Attribute',struct('class','ariane'));
  doc.createNode('a','main',...
                 'Attribute',struct('href','../'), ...
                 'Parent',d);
  d.appendChild(doc.createTextNode([' > Windows statistic']));

  
  doc.createNode('h2','Windows statistic plot');

  d=doc.createNode('div','Attribute',struct('class','plots'));  
  t=doc.createNode('table','Parent',d);
  
  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Tstart_10W_histo.png'],tr);
  addplot(doc,['figures/Tstart_10W_histo_zoom.png'],tr);
  addplot(doc,['figures/Tstart_1W_histo.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Tstart_Duration_8W_density.png'],tr);
  addplot(doc,['figures/Tstart_Duration_8W_density_zoom.png'],tr);
  addplot(doc,['figures/Tstart_Duration_allW_frac_density.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Tend_Duration_8W_density.png'],tr);
  addplot(doc,['figures/Tend_Duration_8W_density_zoom.png'],tr);
  addplot(doc,['figures/Tend_Duration_allW_frac_density.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Tstart_SNR_8W_density.png'],tr);
  addplot(doc,['figures/Tstart_SNR_8W_density_zoom.png'],tr);
  addplot(doc,['figures/Tstart_SNR_allW_frac_density.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Tend_SNR_8W_density.png'],tr);
  addplot(doc,['figures/Tend_SNR_8W_density_zoom.png'],tr);
  addplot(doc,['figures/Tend_SNR_allW_frac_density.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Tstart_allW_frac.png'],tr);
  

  if ~exist([params.webpath '/winstat'])
    system(['mkdir ' params.webpath '/winstat']);
    system(['mkdir ' params.webpath '/winstat/figures']);
  end
    
  system(['cp -r ' params.searchPath '/figures/winstat/* ' ...
          params.webpath '/winstat/figures']);
  system(['cp -r  ./template/style.css ' params.webpath '/' ...
          'winstat/style.css']);

  doc.write([params.webpath '/winstat/index.html']);

end

