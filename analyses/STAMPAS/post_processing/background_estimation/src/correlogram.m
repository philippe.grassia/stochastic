function [] = correlogram(searchPath,data,folder)  
% correlogram :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = correlogram ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

  import classes.utils.waitbar
  import classes.utils.logfile
    
  %
  %-----------------------------------------------------------
  %% INIT
  %-----------------------------------------------------------
  %
  waitbar('correlogram [init]');
  logfile([ searchPath '/logfiles/correlogram.log']);
  logfile('correlogram begin ...');

  % Get the windows' nb in each group
  bknd_jobfile=load([searchPath '/tmp/jobfile_bknd.txt']);

  windows      = bknd_jobfile(:,[2 3 5]);
  window_nb    = windows(end,3);
  window_group = zeros(window_nb,2);

  for i=1:window_nb
    ext = find(windows(:,3)==i);
    window_group(i,1)=i;
    window_group(i,2)=size(ext,1);
  end
  
  %
  %-----------------------------------------------------------
  %% MAIN
  %-----------------------------------------------------------
  %
  close all

  % Correlogram parameters
  delta_t  = .5;
  t_max    = 2500;
  binnb    = t_max/delta_t;
  ConWinNb = 10;
  h2       = zeros(binnb+1,1);

  lag_min  = min([data.lag]);
  lag_max  = max([data.lag]);
  lag_nb   = lag_max-lag_min+1;

  for i=lag_min:lag_max
    sub=data(data.lag==i);
    %  display(['lag ' num2str(i) ' ' num2str(window_nb)])
    for j=1:window_nb
      % one considers only large group of windows to avoid edge effects
      if window_group(j,2)>ConWinNb
        a=sub(sub.windows.group.id==j).GPSstart;
        if numel(a)>1
          h1 = algo_correlogram2 (a, delta_t, binnb);    
          h2=h2+h1;
        end
      end
    end
  end

  figure(2)
  subplot(2,1,1)

  x=[(0:binnb)*delta_t];
  plot(x, h2)

  ave=mean(h2);

  line([x(1)+.5 x(end)], [ave ave],'Color','red')
  if ConWinNb>0
    title(['More than ' num2str(ConWinNb) ' consecutive windows'],'Fontsize', 15)
  end

  xlabel ('Correlogram [s]', 'Fontsize', 15);
  ylabel ('Events nb', 'Fontsize', 15);
  grid
  leg=legend(['Underflow: ' num2str(h2(1))]);
  set(gca,'Xscale','log');
  xlim([0 t_max*1.1])

  % h2 bin nb: binnb+1
  % h3 bin nb : binnb --> easier to manipulate
  h3         = h2(1:end-1);
  b          = fft(h3);
  sampling   = 1./delta_t;
  delta_freq = sampling/binnb;
  freq       = 0:delta_freq:sampling/2.-delta_freq;

  subplot(2,1,2)
  ff = sqrt(2./binnb*sampling);
  semilogy(freq,ff*abs(b(1:binnb/2)))
  xlabel('Frequency (Hz)', 'Fontsize',15);
  ylabel('A.U.','Fontsize',15);
  set(gca,'Xscale','log');
  grid
  xlim([delta_freq*.9 sampling/2*1.1])
  make_png([folder '/diagnostic'], ['Autocorrelogram_' num2str(ConWinNb)]);

  %-----------------------------------------------------------
  %% CLOSE FUNCTION
  %-----------------------------------------------------------
  %
  waitbar('correlogram [done]');
  logfile('correlogram done without errors');
  
  delete(waitbar);
  delete(logfile);
  
end

%-----------------------------------------------------------
%% TOOLS FUNCTION
%-----------------------------------------------------------
%
function myh = algo_correlogram (evl1, evl2, timeidx, deltat, binnb)

  myh = zeros (binnb,1);

  ev1nb = length (evl1);
  ev2nb = length (evl2);

  delta = deltat*binnb + 1;

  for (i=1:ev1nb)
    if (mod(i,10000)==0)
      disp(['rate ' num2str(i) '/' num2str(ev1nb)]);
    end

    extr = find (evl1(i,timeidx) - evl2 (:, timeidx) > 0 & evl1(i,timeidx) - evl2 (:, timeidx) < delta);

    for (j=1:size(extr))
      dist = evl1(i,timeidx) - evl2(extr(j),timeidx);
      myh = addToH(dist,myh,deltat);
    end
  end
end

function myh = algo_correlogram2 (evl1, deltat, binnb)
  myh=zeros(binnb+1,1);
  ev1nb=length(evl1);
  delta = deltat*binnb + 1;

  if ~issorted(evl1)
    evl1=sort(evl1);
  end

  for (i=1:ev1nb-1)
    if (mod(i,100000)==0)
      disp(['rate ' num2str(i) '/' num2str(ev1nb)]);
    end
    
    dist=[];
    for (j=i+1:ev1nb)
      dt=evl1(j)-evl1(i);
      if dt<delta
        dist = [dist; dt];
      else
        myh = addToH(dist,myh,deltat);
        break;
      end
    end
    if j==ev1nb
      myh = addToH(dist,myh,deltat);
    end
  end
end

function his = addToH(distance,myhis,dtime)
  hnb = length(myhis);
  idx=ceil(distance/dtime)+1;
  his=myhis;
  if (idx<=hnb)
    his(idx) = his(idx)+1;
  end
end
