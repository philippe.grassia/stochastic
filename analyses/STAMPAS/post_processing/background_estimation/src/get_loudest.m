function [] = get_loudest(File,Data,varargin)  
% get_loudest :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = get_loudest ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

  import classes.utils.waitbar


  %---------------------------------------------------------------------
  %% INPUT PARSER
  %---------------------------------------------------------------------
  % 
  % parse & check input
  %
  p=inputParser;
  addRequired(p,'File');
  addRequired(p,'Data');
  if verLessThan('matlab','8.2')
    addParamValue(p,'SNR',30,@isnumeric);
    addParamValue(p,'Cut','noCut');
    addParamValue(p,'Max',100,@isnumeric);
  else
    addParamValue(p,'SNR',30,@isnumeric);
    addParamValue(p,'Cut','noCut');
    addParamValue(p,'Max',100,@isnumeric);
  end
  parse(p,File,Data,varargin{:});
  
  data        = p.Results.Data;
  file        = p.Results.File;
  cut         = p.Results.Cut;
  thr         = p.Results.SNR;
  MaxTriggers = p.Results.Max;
  
  [folder,file]=fileparts(file);
  if isempty(folder);folder='.';end

  try
    data.lonetrack;
    doLonetrack = 1;
  catch
    doLonetrack = 0;
  end

  if ~isempty(folder)&&~exist(folder)
    system(['mkdir -p ' folder]);
  end


  if MaxTriggers == -1
    MaxTriggers=Inf;
  end
  
  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % remove veto flaged trigger
  %
  waitbar('loudest [init]');

  try
    if length(cut)>1
      cut=strcat(cut,'+');
      cut=strcat(cut{:});
    end
    [~,~,idx]=decode(cut,data);
    data=data(idx);
  catch
  end  

  % Order according to descending SNR
  if ~isprop(data,'lonetrack')
    data.sort('SNR.coherent', 'descend');
  else
    data.sort('SNR', 'descend');
  end
  S = data.SNR;


  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 
  % get interresting carateristic of the loudNb first trigger
  %
  loudNb = max(min(sum(S>thr),MaxTriggers),10);

  if doLonetrack
    D=[...
      1:loudNb;...                          % trigger idx
      S(1:loudNb)';...                      % SNR 
      data(1:loudNb).GPSstart('ifo1')';...          % GPS start ifo1
      data(1:loudNb).GPSstart('ifo2')';...          % GPS start ifo2
      data(1:loudNb).duration()';...          % duration
      data(1:loudNb).lag()';...               % lag number
      data(1:loudNb).fmin()';...              % fmin
      data(1:loudNb).fmax()';...              % fmax
      data(1:loudNb).ra()';...                % ra
      data(1:loudNb).dec()';...               % dec
      1000*data(1:loudNb).tau()';...               % tau
      data(1:loudNb).SNRfrac()';...           % SNRfrac coherent
      data(1:loudNb).SNRfrac()';...           % SNRfrac ifo1
      data(1:loudNb).SNRfrac()';...           % SNRfrac ifo2
      data(1:loudNb).veto.ifo1()';...         % DQflags ifo1
      data(1:loudNb).veto.ifo2()';...         % DQflags ifo2      
    ];
  else
    D=[...
      1:loudNb;...                          % trigger idx
      S(1:loudNb)';...                      % SNR 
      data(1:loudNb).GPSstart';...          % GPS start ifo1
      data(1:loudNb).GPSstart('ifo2')';...  % GPS start ifo2
      data(1:loudNb).duration';...          % duration
      data(1:loudNb).lag';...               % lag number
      data(1:loudNb).fmin';...              % fmin
      data(1:loudNb).fmax';...              % fmax
      data(1:loudNb).ra';...                % ra
      data(1:loudNb).dec';...               % dec
      1000*data(1:loudNb).tau';...               % tau
      data(1:loudNb).SNRfrac';...           % SNRfrac coherent
      data(1:loudNb).SNRfrac('ifo1')';...   % SNRfrac ifo1
      data(1:loudNb).SNRfrac('ifo2')';...   % SNRfrac ifo2
     %data(1:loudNb).windows.start.ifo1'+data(1:loudNb).SNRfracTime';...      % SNRfrac time ifo1
     %data(1:loudNb).windows.start.ifo2'+data(1:loudNb).SNRfracTime';...      % SNRfrac time ifo2
      data(1:loudNb).veto.ifo1';...         % DQflags ifo1
      data(1:loudNb).veto.ifo2';...         % DQflags ifo2
                                              %data(1:loudNb).veto.Rveto';...        % Rveto ifo2 
    ];
  end

  % write the loudest triggers property in a text file   
  fid = fopen([folder '/' file '.txt'], 'w');
  fprintf(fid,['%d %7.2f %12.2f %12.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f ' ...
               '%7.4f %3.3f %3.3f %3.3f %d %d\n'],D);
  fclose(fid);
  
  waitbar('loudest [done]');
  
  delete(waitbar);
end