function [] = FAR(SearchPath,Data,varargin)  
% FAR :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = FAR ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

  import classes.utils.waitbar
  import classes.utils.logfile
  
  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % parse & check input
  % compute FAR boundary and compute FAR for various cut
  %

  % parse input
  p=inputParser;
  addRequired(p,'SearchPath');
  addRequired(p,'Data');
  if verLessThan('matlab','8.2')
    addParamValue(p,'Folder','');
    addParamValue(p,'Cut','all');
    addParamValue(p,'MCpath','');
  else
    addParameter(p,'Folder','');
    addParameter(p,'Cut','all');
    addParameter(p,'MCpath','');
  end
  parse(p,SearchPath,Data,varargin{:});

  data       = p.Results.Data;
  searchPath = p.Results.SearchPath;
  if isempty(p.Results.Folder)
    ff       = [searchPath '/figures'];
  else
    ff       = p.Results.Folder;
  end
  cut        = p.Results.Cut;
  mc         = p.Results.MCpath;

  waitbar('FAR [init]');
  logfile([searchPath '/logfiles/far.log']);
  logfile('FAR begin ...');

  S=data.SNR;
  
  SNRMin=floor(min(S));
  SNRMax=ceil(max(S));
  SNRs=SNRMin:0.1:SNRMax;
  
  % Get livetime
  config=readConfig([searchPath '/config_bkg.txt']);
  if config.doZeroLag==1
    TotTime=load([searchPath '/tmp/TTimeZeroLag.txt']);
  else
    TotTime=load([searchPath '/tmp/TTime.txt']);
  end
  

  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 
  % compute and plot FAR
  %
  if ~exist(ff)
    system(['mkdir -p ' ff]);
  end

  for c=cut
    waitbar(sprintf('FAR [%3.0f]',100*find(strcmp(c{:},cut))/numel(cut)));
    
    [~,~,idx]=decode(c{:},data);
    [snr, far]=FAR_compute(data(idx), SNRs, TotTime);
    save([ff '/FAR_' c{:} '.mat'],'snr','far');
    clear far
  end

  % compute FAR for clean data : with all cut apply
  clean=strcat(cut,'+');
  [~,~,idx]=decode(strcat(clean{:}),data);
  [snr, far]=FAR_compute(data(idx), SNRs, TotTime);
  save([ff '/FAR_clean.mat'],'snr','far');

  % compute cumulative cut
  idx=ones(data.numTriggers,1);
  leg={};
  snr=zeros(length(SNRs),length(cut));
  far=zeros(length(SNRs),length(cut));
  for c=cut
    [l,~,id]=decode(c{:},data);
    idx=idx&id;
    leg=[leg,l];

    [s, f]=FAR_compute(data(idx), SNRs, TotTime);
    snr(:,strcmp(c,cut))=s;
    far(:,strcmp(c,cut))=f;
    clear s f;
  end
  save([ff '/FAR_cumulative.mat'],'snr','far','leg');
  clear far
  
  % plots
  waitbar('FAR : plots ...');
  logfile('FAR : plots ...');
  FARplots(ff,cut,mc);
  
  
  %---------------------------------------------------------------------
  %% CLOSE FUNCTION
  %---------------------------------------------------------------------
  % 
  %
  waitbar('FAR : [done]');
  logfile('FAR end without errors ');

  delete(waitbar);
  delete(logfile);
  
end



%% Tools functions
%---------------------------------------------------------------------
% 
function [snr, far]=FAR_compute(data, SNRs, TotTime);

  S=data.SNR;

  [N,snr]=hist(S,SNRs);
  N_cum=cumsum(N);
  N_cum_max=N_cum(end);
  for i=1:length(N_cum)
    N_cum_inv(i,1)=N_cum_max-N_cum(i);
  end
  far=N_cum_inv/TotTime;
  if size(snr)~=size(far)
    snr=snr';
  end
end





