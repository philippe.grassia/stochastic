function [] = timebet(SearchPath,Data,varargin)  
% timebet :
% DESCRIPTION :
%   Function to study the triggers time distribution. 
%   Fits are done with ezyfit third party package in
%   stamp2/src/misc
% 
% SYNTAX :
%   [] = timebet ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  import classes.utils.waitbar
  import classes.utils.logfile

  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % parse & check input
  %
  
  p=inputParser;
  addRequired(p,'SearchPath');
  addRequired(p,'Data');
  if verLessThan('matlab','8.2')
    addParamValue(p,'Folder','');
  else
    addParamValue(p,'Folder','');
  end
  parse(p,SearchPath,Data,varargin{:});
  
  data       = p.Results.Data;
  searchPath = p.Results.SearchPath;
  if isempty(p.Results.Folder)
    ff       = [searchPath '/figures'];
  else
    ff       = p.Results.Folder;
  end

  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 
  %
  waitbar('timebet [init]');
  logfile([searchPath '/logfiles/timebet.log']);
  logfile('timebet begin ...');
  
  
  bknd_jobfile=load([searchPath '/tmp/jobfile_bknd.txt']);
  
  windows      = bknd_jobfile(:,[2 3 5]);
  window_nb    = windows(end,3);
  window_group = zeros(window_nb,2);
  for i=1:window_nb
    ext = find(windows(:,3)==i);
    window_group(i,1)=i;
    window_group(i,2)=size(ext,1);
  end


  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 
  %

  % Time between 2 triggers
  ConWinNb=0;
  
  lag_min = min(data.lag);
  lag_max = max(data.lag);
  lag_nb  = lag_max-lag_min+1;
  total   = -2*ones(numel(data),5);
  offset=0;

  for i=lag_min:lag_max
    sub = data([data.lag]==i);
    waitbar(sprintf('timebet [%3.0f] ',i));
  
    for j=1:window_nb
      % one considers only large group of windows to avoid edge effects
      if window_group(j,2)>ConWinNb
        for kk=1:2
          win=sub.windows.group.id==j & ...
              sub.fmin>(kk-1)*500 & ...
              sub.fmin<kk*500;
          evt_nb = sum(win);

          if evt_nb>0
            b = zeros(evt_nb,4);
            b(:,1)=sub(win).GPSstart;
            b(:,2)=sub(win).duration;
            b(:,3)=sub(win).fmin;	
            b(:,4)=sub(win).fmax;		

            if ~issorted(b,'rows')
              b=sortrows(b,1);
            end
            
            for k=1:evt_nb-1
              total(offset+k,1)=b(k+1,1)-b(k,1);
              total(offset+k,2)=b(k,2);
              total(offset+k,3)=b(k,1);
              total(offset+k,5)=b(k,3);
              if b(k,1)+b(k,2)<b(k+1,1)
                total(offset+k,4)=j;
              else
                total(offset+k,4)=-j;
              end
            end
            offset=offset+evt_nb-1;
          end
        end
      end
    end
  end
  
  total=total(total(:,1)>0,:);
  
  close all

  x  = 0:.5:80;
  a1 = 150000;
  b1 = -.1;

  
  %% Consecutif event time difference
  %---------------------------------------------------------------------
  % 
  figure()
  plotandfitthis(x,total,a1,b1,[0.2 0.4 0.1 0.05]);  
  legend('All triggers in each lag')
  xlabel('Time between 2 consecutive events [s]', 'Fontsize', 15);
  ylabel('Events nb', 'Fontsize', 15);
  grid
  make_png([ff '/diagnostic'], 'ConEventTimeDiff')

  %% Consecutif event time di[ff '/diagnostic']erence zoom
  %---------------------------------------------------------------------
  % 
  figure()
  dx=.5;
  sel=find(total(:,1)<=40);
  a1=150000;
  b1=-.2;
  try
    plotandfitthis(dx,total(sel),a1,b1,[0.25 0.4 0.1 0.05]);
  end
  grid
  legend('All triggers in each lag')
  xlabel('Time between 2 consecutive events [s] (zoom)', 'Fontsize', 15);
  ylabel('Events nb', 'Fontsize', 15);
  make_png([ff '/diagnostic'], 'ConEventTimeDiff_zoom')
  

  %% Correlation bettween 2 consecutive event
  %---------------------------------------------------------------------
  % 
  % Correlation between time between 2 consecutive triggers and
  % triggers duration
  tot = total(find(total(:,1)<=100),:);
  s   = zeros(100,4);
  x_min = -.1;
  x_max = 1;
  for i=1:100
    ext=find(tot(:,1)>x_min & tot(:,1)<=x_max);
    s(i,1)=max(0,x_min);
    s(i,2)=size(ext,1);
    s(i,3)=mean(tot(ext,2));
    s(i,4)=std(tot(ext,2));
    x_min=x_max;
    x_max=x_max+1;
  end



  %% Consecutive events time di[ff '/diagnostic']erence vs duration
  %---------------------------------------------------------------------
  % 
  figure()
  [ax,p1,p2]=plotyy(s(:,1),s(:,2),s(:,1),s(:,3),'plot', 'plot');
  set(ax(1),'Yscale', 'log')
  hold on
  plot(ax(1),s(1:4,1),s(1:4,2),'+r')
  plot(ax(1),s(5:11,1),s(5:11,2),'+g')
  plot(ax(1),s(12:36,1),s(12:36,2),'+k')
  hold off
  set (ax(1),'YLimMode', 'manual');
  set(ax(1),'xlim',[-5 105])
  set(ax(2),'xlim',[-5 105])
  grid

  xlabel('Time between 2 consecutive events','fontsize', 15)
  ylabel(ax(1),'Nb of events','fontsize', 15)
  ylabel(ax(2),'Events duration average','fontsize', 15)
  make_png([ff '/diagnostic'], 'ConEventTimeDiff_Duration')


  %% Consecutive events time di[ff '/diagnostic']erence vs Nb event vs duration
  %---------------------------------------------------------------------
  % 
  figure()
  plot(s(1:36,2),s(1:36,3),'+')
  hold on
  plot(s(1:4,2),s(1:4,3),'+r')
  plot(s(5:11,2),s(5:11,3),'+g')
  plot(s(12:36,2),s(12:36,3),'+k')
  plot(s(37:100,2),s(37:100,3),'+b')
  hold off
  grid

  xlabel('Nb of events in a time between 2 consecutive events bin','fontsize', 15)
  ylabel('Event duration average','fontsize', 15)

  make_png([ff '/diagnostic'], 'ConEventTimeDiff_NbEvent_Duration')

  %---------------------------------------------------------------------
  %% CLOSE FUNCTION
  %---------------------------------------------------------------------
  % 
  % close function & display ed message
  %
  waitbar('timebet [done]');
  logfile('timebet done without errors');

  delete(logfile)
  delete(waitbar)
  
end


%---------------------------------------------------------------------
%% TOOLS FUNCTION
%---------------------------------------------------------------------
% 
function plotandfitthis (x,total,a1,b1, location)
  if isempty(total)
    return
  end
  [x1,n1,underflow,overflow]=histlog(total,x,'b');

  ext=find(n1>.1);
  x1=x1(ext);
  n1=n1(ext);
  stairs(x1,n1);

  h = get( gcf, 'CurrentAxes');
  set (h,'YScale','log');

  undofit
  showfit(['a1*exp(b1*x); a1=' num2str(a1) '; b1=' num2str(b1)], 'boxlocation',location) 
  hold on
  histlog(total,x,'b');
  hold off

  % Bring the fit foreward
  chil=get(gca,'children');
  chil2(1,1)=chil(2,1);
  chil2(2,1)=chil(3,1);
  chil2(3,1)=chil(1,1);
  set(gca,'children',chil2);
end