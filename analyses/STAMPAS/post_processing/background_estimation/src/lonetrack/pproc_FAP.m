function pproc_FAP(snrmax,initp,T,q,config,folder)

% pproc_FAP : 
% DESCRIPTION :
%   compute and plot the FAR for Lonetrack triggers
%   
% SYNTAX :
%   pproc_FAR (snrmax_histo,edges,folder)
% 
% INPUT : 
%    snrmax : 
%    initp :
%    T :
%    q :
%    config :
%    folder : figures folder
%
% AUTHOR : Michael Coughglin
% CONTACT : 
% VERSION : 1.0
% DATE : January 2017
%

import classes.utils.waitbar


fprintf('fap_plots ...\n');

% prepare for getting the snrmax
snrperc = 0.90;
tmp = sort(q.snrmax);
tmp = tmp(~isnan(tmp));
snrth = tmp(floor(length(tmp)*snrperc));
fprintf('Using temp SNR threshold: %.2f\n',snrth);


% prepare for probability plots
dp = 1/1e12;

% prepare for probability plots: background in 1 starting at SNR=4
dp = 1/length(snrmax);
pvals = 1 : -dp : dp;

% background in 1 down to SNR=4
dpx = 1/sum(~isnan(q.snrmax));
pvalsx = 1 : -dpx : dpx;

% define 5-sigma line and 4sigma line
s5 = 1744278*T;
s4 = 15787*T;
s3 = 370*T;

% calculate sort(snrmax1) just once

snrmax_sort = sort(snrmax);
snrmax_sortx = sort(q.snrmax(~isnan(q.snrmax)));

% calculate sorted indices that will be plotted
snrmax_sortx_ind = find(snrmax_sortx<snrth);

%% get Trial factor
if config.lonetrackNSlides >0
  trialFactor = pvalsx(snrmax_sortx_ind(end));
  trialFactorSmall = 1;
else
  trialFactor = mean(initp);
  %trialFactorSmall = sum(~isnan(q.snrmax))/length(q.snrmax);
  trialFactorSmall = str2num(config.lonetrackFAP);
end

% threshold for 3-sigma detection in one detector
th3 = interp1(trialFactor*pvals, snrmax_sort, 1/s3);
% threshold for 4-sigma detection in one detector
th4 = interp1(trialFactor*pvals, snrmax_sort, 1/s4);
% threshold for 5-sigma detection in one detector
th5 = interp1(trialFactor*pvals, snrmax_sort, 1/s5);

if isnan(th3)
  fprintf('3 sigma not available... using max(snr) instead...\n');
  th3 = max(snrmax_sort);
end

if isnan(th4)
  fprintf('4 sigma not available... using max(snr) instead...\n');
  th4 = max(snrmax_sort);
end

if isnan(th5)
  fprintf('5 sigma not available... using max(snr) instead...\n');
  th5 = max(snrmax_sort);
end

fprintf('3 sigma: %.5f\n',th3);
fprintf('4 sigma: %.5f\n',th4);
fprintf('5 sigma: %.5f\n',th5);

xmax = max(snrmax_sort)+1;

% diagnostic plots: snrmax1
figure;
h1 = semilogy(snrmax_sortx(snrmax_sortx_ind), trialFactorSmall*pvalsx(snrmax_sortx_ind),'b','LineWidth',3);
hold on;
semilogy(snrmax_sort, trialFactor*pvals,'b','LineWidth',3);
hold on
h2 = semilogy([0 xmax], [1 1]/s5, 'k--','LineWidth',3);
semilogy([0 xmax], [1 1]/s4, 'k--','LineWidth',3);
semilogy([0 xmax], [1 1]/s3, 'k--','LineWidth',3);
hold off
%axis([0 xmax 1e-12 1]);
axis([0 xmax 1e-8 1]);
grid on;
xlabel('\Lambda');
ylabel('FAP (including trial factors)');
legend([h1,h2],{'Background','3,4,5-sigma'});
pretty;
make_png(folder,'snrmax_htrack_bknd_combine_zoom');
close;

% diagnostic plots: snrmax1
figure;
h1 = semilogy(snrmax_sortx(snrmax_sortx_ind),trialFactorSmall*pvalsx(snrmax_sortx_ind),'b','LineWidth',3);
hold on;
semilogy(snrmax_sort, trialFactor*pvals,'b','LineWidth',3);
hold on
h2 = semilogy([0 xmax], [1 1]/s5, 'k--','LineWidth',3);
semilogy([0 xmax], [1 1]/s4, 'k--','LineWidth',3);
semilogy([0 xmax], [1 1]/s3, 'k--','LineWidth',3);
hold off
axis([0 xmax 1e-12 1]);
%axis([0 xmax 1e-8 1]);
grid on;
xlabel('\Lambda');
ylabel('FAP (including trial factors)');
legend([h1,h2],{'Background','3,4,5-sigma'});
pretty;
make_png(folder, 'snrmax_htrack_bknd_combine');
close;

% diagnostic plots: snrmax1
figure;
h1 = semilogy(snrmax_sortx(snrmax_sortx_ind), trialFactorSmall*pvalsx(snrmax_sortx_ind),'b','LineWidth',3);
hold on;
semilogy(snrmax_sort, trialFactor*pvals,'b','LineWidth',3);
hold on
h2 = semilogy([0 xmax], [1 1]/s5, 'k--','LineWidth',3);
semilogy([0 xmax], [1 1]/s4, 'k--','LineWidth',3);
semilogy([0 xmax], [1 1]/s3, 'k--','LineWidth',3);
hold off
%axis([0 xmax 1e-12 1]);
axis([0 xmax 1e-8 1]);
grid on;
xlabel('\Lambda');
ylabel('FAP (including trial factors)');
legend([h1,h2],{'Background','3,4,5-sigma'});
pretty;
make_png(folder,'snrmax_htrack_combine_zoom');
close;

% diagnostic plots: snrmax1
figure;
h1 = semilogy(snrmax_sortx(snrmax_sortx_ind), trialFactorSmall*pvalsx(snrmax_sortx_ind),'b','LineWidth',3);
hold on;
semilogy(snrmax_sort, trialFactor*pvals,'b','LineWidth',3);
hold on
h2 = semilogy([0 xmax], [1 1]/s5, 'k--','LineWidth',3);
semilogy([0 xmax], [1 1]/s4, 'k--','LineWidth',3);
semilogy([0 xmax], [1 1]/s3, 'k--','LineWidth',3);
hold off
axis([0 xmax 1e-12 1]);
%axis([0 xmax 1e-8 1]);
grid on;
xlabel('\Lambda');
ylabel('FAP (including trial factors)');
legend([h1,h2],{'Background','3,4,5-sigma'});
pretty;
make_png(folder, 'snrmax_htrack_combine');
close;
