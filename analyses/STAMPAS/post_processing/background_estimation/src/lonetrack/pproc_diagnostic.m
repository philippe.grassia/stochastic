function pproc_diagnostic(snrmax,snrtimes,timedelay,folder)

% pproc_et_loudest :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = pproc_diagnostic ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentiin Frey
% CONTACT : 
% VERSION :
% DATE : 
%

import classes.utils.waitbar

waitbar('pproc_diagnostic [start]');

snr_max=max(snrmax);
snr_min=min(snrmax);
map_max=max(snrtimes);
timedelay_min=min(timedelay);
timedelay_max=max(timedelay);

% Reduce the number of triggers 
MaxTriggers=1000000;
if length(snrmax)>MaxTriggers
  [snrmax_x,ind]=sort(snrmax,'descend');

  snrmax=snrmax_x(1,1:MaxTriggers);
  snrtimes=snrtimes(1,ind(1:MaxTriggers));
  timedelay=timedelay(1,ind(1:MaxTriggers));
  clear snrmax_x,ind;
  snr_min=snrmax(end);
end

% snrmax vs time
figure;
h1 = semilogy(snrtimes, snrmax,'kx','LineWidth',3);
grid on;
xlabel('Map Number');
ylabel('\Lambda');

xlim([0 map_max+10]);
ylim([0.9*snr_min 1.1*snr_max]);

legend([h1],{'Background'});
make_png(folder,'snrmax_times');
close;

% time delay vs time
figure;
h1 = plot(snrtimes, timedelay,'kx','LineWidth',3);
grid on;
xlabel('Map Number');
ylabel('Time Delay [s]');

legend([h1],{'Background'});
pretty;
make_png(folder,'timedelay_times');
close;

% 2D snrmax vs times heatmap
snrtimes_unique = unique(snrtimes);
bins_edges = linspace(min(snrmax),max(snrmax),51);
bins = (bins_edges(1:end-1) + bins_edges(2:end))/2;
[X,Y] = meshgrid(snrtimes_unique,bins);
Z = zeros(length(snrtimes_unique),length(bins));

for ii = 1:length(snrtimes_unique)
  idx = find(snrtimes == snrtimes_unique(ii));
  N = histc(snrmax(idx), bins);
  Z(ii,:) = N / sum(N);
end

figure;
h = pcolor(X,Y,Z');
set(h, 'EdgeColor', 'none');

xlabel('Map Number');
ylabel('\Lambda');
caxis([0 0.1]);
colorbar;
pretty;

make_png(folder,'snrmax_times_heatmap');
close;

% 2D snrmax vs times heatmap
% snrtimes_unique = unique(snrtimes);
bins_edges = linspace(timedelay_min,timedelay_max,51);
bins = (bins_edges(1:end-1) + bins_edges(2:end))/2;
[X,Y] = meshgrid(snrtimes_unique,bins);
Z = zeros(length(snrtimes_unique),length(bins));

for ii = 1:length(snrtimes_unique)
  idx = find(snrtimes == snrtimes_unique(ii));
  N = histc(timedelay(idx), bins);
  Z(ii,:) = N / sum(N);
end

figure;
h = pcolor(X,Y,Z');
set(h, 'EdgeColor', 'none');
xlabel('Map Number');
ylabel('Time Delay [s]');
%caxis([0 0.1]);
colorbar;
pretty;

make_png(folder,'timedelay_times_heatmap');
close;

bins_edges = linspace(timedelay_min,timedelay_max,51);
bins = (bins_edges(1:end-1) + bins_edges(2:end))/2;
[N] = hist(timedelay, bins);
Z = N / sum(N);

% timedelay histogram
figure;
plot(bins,Z);
ylabel('Counts (normalized)');
xlabel('Time Delay [s]');
pretty;
make_png(folder,'timedelay_hist');
close;

figure
hist(timedelay,bins);
xlim([-0.012 0.012]);
ylabel('Counts');
xlabel('Time Delay [s]');
pretty;
make_png(folder,'timedelay_histbar');
close;

waitbar('pproc_diagnostic [done ]');
return;
