#! /usr/bin/env bash

# functions
usage(){
    echo 'Arg 1: GPS_START'
    echo 'Arg 2: GPS_END'
    echo 'Arg 3: ID'
    echo 'Arg 4: MC run [1] or real data [0]'
    echo '[Arg 6: background ID for zerolag]'
    exit
}

insert_command(){
    sed "/<!-- command -->/ {
         h
         r command.txt
         g
         N
     }" index.html > index_tmp.html 
    mv index_tmp.html index.html
}

insert_windows_html(){
    ifo=$1
    figures_folder=$2
    sed "/<!-- ${ifo}_WINDOW_EVENTS -->/ {
         h
         r ${figures_folder}/windows_${ifo}.txt
         g
         N
     }" index.html > index_tmp.html 
    mv index_tmp.html index.html
}

insert_trigger_html(){
# create html trigger info row
    RUN=$1
    MC=$2
    sed "/<!-- TRIGGERS -->/i\ <tr>" index.html > index_tmp.html 
    mv index_tmp.html index.html

    if [ $MC == "0" ]; then
	sed "/<!-- TRIGGERS -->/i\ <th class=\"triggers\">[INDEX]</th> <th class=\"triggers\">[SNR]</th> <th class=\"triggers\">[GPS_H1]</th> <th class=\"triggers\">[GPS_L1]</th> <th class=\"triggers\">[DUR]</th> <th class=\"triggers\">[LAG]</th> <th class=\"triggers\">[F_MIN]</th> <th class=\"triggers\">[F_MAX]</th> <th class=\"triggers\">[RA]</th> <th class=\"triggers\">[DEC]</th> <th class=\"triggers\">[SNR_FRAC]</th> <th class=\"triggers\"><a href=\"${web_server}/~[USER]/${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/FTMAPS/plots_[INDEX]/\">link</a></th> <th class=\"triggers\"><a href=\"${web_server}/~[USER]/${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/OMEGA/H1/[OMEGA_GPS_H1]\">omega</a> , <a href=\"${web_server}/~[USER]/${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/OMEGA_HOFT/H1/[OMEGA_GPS_H1]\">hoft</a></th> <th class=\"triggers\"><a href=\"${web_server}/~[USER]/${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/OMEGA/L1/[OMEGA_GPS_L1]\">omega</a> , <a href=\"${web_server}/~[USER]/${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/OMEGA_HOFT/L1/[OMEGA_GPS_L1]\">hoft</a></th> <th class=\"triggers\">[DQ_H1]</th> <th class=\"triggers\">[DQ_L1]</th>" index.html > index_tmp.html
    else
	sed "/<!-- TRIGGERS -->/i\ <th class=\"triggers\">[INDEX]</th> <th class=\"triggers\">[SNR]</th> <th class=\"triggers\">[GPS_H1]</th> <th class=\"triggers\">[GPS_L1]</th> <th class=\"triggers\">[DUR]</th> <th class=\"triggers\">[LAG]</th> <th class=\"triggers\">[F_MIN]</th> <th class=\"triggers\">[F_MAX]</th> <th class=\"triggers\">[RA]</th> <th class=\"triggers\">[DEC]</th> <th class=\"triggers\">[SNR_FRAC]</th> <th class=\"triggers\"></th> <th class=\"triggers\"></th> <th class=\"triggers\"></th> <th class=\"triggers\">[DQ_H1]</th> <th class=\"triggers\">[DQ_L1]</th>" index.html > index_tmp.html

    fi
    mv index_tmp.html index.html
    sed "/<!-- TRIGGERS -->/i\ </tr>" index.html > index_tmp.html
    mv index_tmp.html index.html
}

insert_zerolag_html(){
    RUN=$1
    sed "/<!-- ZEROLAG -->/ {
         h
         r template/template_zerolag.html
         g
         N
     }" index.html > index_tmp.html 
    mv index_tmp.html index.html

    
}

# main
[[ $# -lt 4 ]] && usage

GPS_START=$1
GPS_END=$2
ID=$3
MC=$4


if [[ $# -eq 5 ]]; then
    ID_BKG=$5
else
    ID_BKG=0
fi

if [ ${#GPS_START} -lt 9 ]; then
    echo GPS_START $GPS_START should have 9 or 10 digits
    exit 1
fi

if [ ${#GPS_END} -lt 9 ]; then
    echo GPS_END $GPS_END should have 9 or 10 digits
    exit 1
fi

if [ ${GPS_START} -lt 900000000 ]; then
    RUN="S5"
    SEARCH='s5-allsky'
elif [ ${GPS_START} -lt 1000000000 ]; then
    RUN="S6"
    SEARCH='s6-allsky'
elif [ ${GPS_START} -lt 1124004000 ]; then
    RUN="ER7"
    SEARCH='er7-allsky'
elif [ ${GPS_START} -lt 1126310417 ]; then
    RUN="ER8"
    SEARCH='er8-allsky'
elif [ ${GPS_START} -lt 1137283217 ];then
    RUN="O1"
    SEARCH='o1-allsky'
else
    RUN="O2"
    SEARCH="o2-allsky"
fi

searchPath='BKG/Start-'${GPS_START}'_Stop-'${GPS_END}'_ID-'${ID}

HOST=`hostname -d | cut -d "." -f 1`
if [[ $HOST == "atlas" ]]; then
    web_repo='WWW/LSC'
    web_server="https://www.atlas.aei.uni-hannover.de/"
elif [[ $HOST == "ligo" ]];then
    web_repo='public_html'
    web_server="https://ldas-jobs.ligo.caltech.edu/"
else
    echo "Only designed to work on LIGO and ATLAS clusters. Exiting..."
    exit
fi


echo 'Generating report page for search ' $SEARCH ' dag Start-'${GPS_START}'_Stop-'${GPS_END}'_ID-'${ID}

# Get zerolag or bkg
zerolag=`grep doZeroLag ../../BKG/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/config_bkg.txt | cut -d " " -f 2`
lonetrack=`grep doLonetrack ../../BKG/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/config_bkg.txt | cut -d " " -f 2`
figures_folder='../../BKG/Start-'${GPS_START}'_Stop-'${GPS_END}'_ID-'${ID}'/figures'

path=/home/$USER/${web_repo}/${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/background_estimation

cp template/template_background.html index.html

# Include extra investigation sections

# Title

sed -e "s|\[ID\]|${ID}|g" index.html > index_tmp.html
mv index_tmp.html index.html

sed -e "s|\[RUN\]|${RUN}|g" index.html > index_tmp.html
mv index_tmp.html index.html

sed -e "s|\[USER\]|${USER}|g" index.html > index_tmp.html
mv index_tmp.html index.html

sed -e "s|\[DATE\]|`date`|g" index.html > index_tmp.html
mv index_tmp.html index.html

# zerolag section
if [ $zerolag -eq 1 ]; then
    insert_zerolag_html ${RUN}
    sed -e "s|\[TYPE\]|ZERO-LAG (background ID ${ID_BKG})|g" index.html > index_tmp.html
    mv index_tmp.html index.html
else
    sed -e "s|\[TYPE\]|BACKGROUND|g" index.html > index_tmp.html
    mv index_tmp.html index.html
fi

insert_command

# Windows statitic report
#insert_windows_html H1 ${figures_folder}
#insert_windows_html L1 ${figures_folder}
    
# Loudest triggers 
while read line
do
    INDEX=`echo $line | cut -d " " -f 1`
    SNR=`echo $line | cut -d " " -f 2`
    GPS_H1=`echo $line | cut -d " " -f 3`
    GPS_L1=`echo $line | cut -d " " -f 4`
    DUR=`echo $line | cut -d " " -f 5`
    LAG=`echo $line | cut -d " " -f 6`
    F_MIN=`echo $line | cut -d " " -f 7`
    F_MAX=`echo $line | cut -d " " -f 8`
    RA=`echo $line | cut -d " " -f 9`
    DEC=`echo $line | cut -d " " -f 10`
    SNR_FRAC=`echo $line | cut -d " " -f 11`
    DQ_H1=`echo $line | cut -d " " -f 12`
    DQ_L1=`echo $line | cut -d " " -f 13`

    if [ $DQ_H1 == 1 ]; then
	DQ_H1=""
    else
	DQ_H1="YES"
    fi

    if [ $DQ_L1 == 1 ]; then
	DQ_L1=""
    else
	DQ_L1="YES"
    fi

    insert_trigger_html ${RUN} ${MC}

    time_H1=`echo ${GPS_H1} | sed  '/\./s/\.*0*$//g'`
    time_L1=`echo ${GPS_L1} | sed  '/\./s/\.*0*$//g'`

    sed -e "s|\[INDEX\]|${INDEX}|g" \
	-e "s|\[SNR\]|${SNR}|g" \
	-e "s|\[GPS_H1\]|${GPS_H1}|g" \
	-e "s|\[GPS_L1\]|${GPS_L1}|g" \
	-e "s|\[OMEGA_GPS_H1\]|${time_H1}|g" \
	-e "s|\[OMEGA_GPS_L1\]|${time_L1}|g" \
	-e "s|\[DUR\]|${DUR}|g" \
	-e "s|\[LAG\]|${LAG}|g" \
	-e "s|\[F_MIN\]|${F_MIN}|g" \
	-e "s|\[F_MAX\]|${F_MAX}|g" \
	-e "s|\[RA\]|${RA}|g" \
	-e "s|\[DEC\]|${DEC}|g" \
	-e "s|\[SNR_FRAC\]|${SNR_FRAC}|g" \
	-e "s|\[DQ_H1\]|${DQ_H1}|g" \
	-e "s|\[DQ_L1\]|${DQ_L1}|g" \
	-e "s|\[RUN\]|${RUN}|g" \
	-e "s|\[USER\]|${USER}|g" \
	index.html > index_tmp.html
	mv index_tmp.html index.html

done < ${figures_folder}/loudest.txt

if [ -e /home/$USER/${web_repo} ]; then
    if [ ! -e ${path} ] ; then 
	mkdir -p ${path}
    fi
    if [ ! -e ${path}/figures ] ; then 
	mkdir -p ${path}/figures
    fi

    if [ ${lonetrack:0:1} == "1" ]; then
        if [ ! -e ${path}/ifo1 ] ; then
            mkdir -p ${path}/ifo1
        fi
        if [ ! -e ${path}/ifo1/figures ] ; then
            mkdir -p ${path}/ifo1/figures
        fi

        cp $figures_folder/ifo1/*.png ${path}/ifo1/figures/.
        cp template/style.css ${path}/ifo1/.
        cp index.html ${path}/ifo1/.

        if [ ! -e ${path}/ifo2 ] ; then
            mkdir -p ${path}/ifo2
        fi
        if [ ! -e ${path}/ifo2/figures ] ; then
            mkdir -p ${path}/ifo2/figures
        fi

        cp $figures_folder/ifo2/*.png ${path}/ifo2/figures/.
        cp template/style.css ${path}/ifo2/.
        cp index.html ${path}/ifo2/.

    fi

    cp $figures_folder/*.png ${path}/figures/.
    cp template/style.css ${path}/.
    mv index.html ${path}/.

fi

echo "result web page available on :"
echo $web_server~${USER}/${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/background_estimation



