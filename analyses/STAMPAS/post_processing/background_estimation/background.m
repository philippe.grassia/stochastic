%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% background : launch all background step that analyse trigger,
% plot triggers distribution and FAR and generated HTML at the end 
% 
% DESCRIPTION : 
%   plot several triggers distribution for various cut.  
%   compute the FAR and generate HTML doc at the end
% 
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : May 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import classes.utils.waitbar
import classes.utils.logfile

fprintf('\n');

%------------------------------------------------------------------
%% DEFINE CUT
%------------------------------------------------------------------
% get the cut using the variable definie
%
cut={'noCut'};

if doTFveto
  cut=[cut, 'TFveto'];
end

if doRveto
  cut=[cut, 'Rveto'];
end

if ~isempty(SNRfracCut)
  cut=[cut, ['SNRfrac:' num2str(SNRfracCut)]];
end

if doDQFlags
  cut=[cut, 'DQFlags'];
end

if false
  cut=[cut, 'HWInj'];
end

%------------------------------------------------------------------
%% INIT
%------------------------------------------------------------------
% 
if ~exist([searchPath '/figures'])
  system(['mkdir -p ' searchPath '/figures']);
end
config = readConfig([searchPath '/config_bkg.txt']);
start  = str2num(regexprep(searchPath, '.*Start-([0-9]*)_.*','$1'));
stop   = str2num(regexprep(searchPath, '.*Stop-([0-9]*)_.*','$1'));

config.searchPath=searchPath;

%------------------------------------------------------------------
%% get data
%------------------------------------------------------------------
if ~exist([searchPath '/results/results_merged/results_filtered.mat'])
  error(['file : ' searchPath '/results/results_merged/' ...
	 'results_filtered.mat not found']);
end
load([searchPath '/results/results_merged/results_filtered.mat']);

%% DIAGNOSTIC PLOTS
%------------------------------------------------------------------
%
diagnostic(searchPath,data,'Cut',cut);


if config.doZebra
  %% FAR PLOTS
  %------------------------------------------------------------------
  try
    FAR(searchPath,data,'Cut',cut);
  catch
    fprintf('FAR plots failed... continuing\n.');
  end
  

  %% CORRELOGRAM PLOTS
  %------------------------------------------------------------------
  %correlogram(searchPath,data,[searchPath '/figures'])
  %% WINDOWS OVERLAP PLOTS
  %------------------------------------------------------------------
  window_overlap(searchPath,data,'Folder',[searchPath '/figures'],...
		 'Overlap',config.overlap,'Size',config.window);

  %% TIMEBET PLOTS
  %------------------------------------------------------------------
  % /!\ warning timebet take a lot of time to get fast result comment
  % the folowing line
  %timebet(searchPath,data,'Folder',[searchPath '/figures'])

  %% LOUDEST
  %------------------------------------------------------------------
  % compute the loudest cut : cut apply in the list of loudest
  % only TFveto & Rveto if exist

  lcut={'noCut'};
  if sum(strcmp(cut,'TFveto'))
    lcut=[lcut,'TFveto'];
  end
  if sum(strcmp(cut,'Rveto'))
    lcut=[lcut,'Rveto'];
  end
  get_loudest([searchPath '/figures/loudest.txt'],data,'Cut',cut);

  %% Rveto DIAGNOSTIC PLOTS
  %------------------------------------------------------------------
  % only if Rveto is used
  if sum(cell2mat(strfind(cut,'Rveto')))
    RvetoDiagnostic(data,[searchPath '/figures/']);
  end

  %% SNRfrac DIAGNOSTIC PLOTS
  %------------------------------------------------------------------
  % only if SNRfrac is used
  if sum(cell2mat(strfind(cut,'SNRfrac')))
    SNRfracDiagnostic(data,[searchPath '/figures/']);
  end
end

%
if config.doLonetrackPProc
  %% jobs
  jobs=load([searchPath '/tmp/joblistTrue.txt']);
  windows_nb=length(jobs);

  %% Lonetrack data
  %------------------------------------------------------------------
  lonetrackPath = [searchPath '/LonetrackPProc/'];
  trigger_1_filename=[searchPath '/Lonetrack/bknd/stage1_triggers.mat'];    
  trigger_filename=[lonetrackPath '/bknd/stage2_triggers.mat'];    
  snrhisto_filename=[lonetrackPath '/bknd/stage2_far.mat'];

  if ~exist(trigger_filename)
    error([trigger_filename ' not found. Please run extract.sh first'])
  else
    fprintf('\nLoading already generated merged files ...\n');
    load(trigger_1_filename);
    load(trigger_filename);
    load(snrhisto_filename);
  end

  %% FAR plots
  %------------------------------------------------------------------
  BkgTotTime=load([searchPath '/tmp/TTime.txt']);
  ZLTotTime=load([searchPath '/tmp/TTimeZeroLag.txt']);
  pproc_FAR(snrmax_histo,edges,BkgTotTime,ZLTotTime,[searchPath '/figures/'])

  %% FAP plots
  %------------------------------------------------------------------
  filename = [lonetrackPath '/lonetrack_0/lonetrack_' num2str(jobs(1,2)) '.mat'];
  q = load(filename);
  pproc_FAP(snrmax,initp,windows_nb,q,config,[searchPath '/figures/diagnostic/']);

  %% Get loudest
  %------------------------------------------------------------------
  lonetrack_get_loudest(searchPath,[searchPath '/figures/loudest.txt'],...
                        snrmax,AP,snrtimes,timedelay,...
                        id,lag,jobs,gpstime,10,0,...
                        load([searchPath '/figures/pproc_FAR_clean.mat']));

  %% diagnostic plots
  %------------------------------------------------------------------
  pproc_diagnostic(snrmax,snrtimes,timedelay,[searchPath '/figures/diagnostic/']);


  %% single ifo diagnostic plots
  %------------------------------------------------------------------
  clear snrmax;
  clear snrtimes;
  clear timedelay;
  clear q;
  clear jobs;

end

if config.doVLT
  fprintf('Processing VLT results ... \n')
  FAR(searchPath,data,'Cut',cut);
 
  %% CORRELOGRAM PLOTS
  %------------------------------------------------------------------
  correlogram(searchPath,data,[searchPath '/figures']);

  %% WINDOWS OVERLAP PLOTS
  %------------------------------------------------------------------
  window_overlap(searchPath,data,'Folder',[searchPath '/figures'],...
                 'Overlap',config.overlap,'Size',config.window);

  % LOUDEST TRIGGER: VLT is generally run with no PP vetos
  %------------------------------------------------------------------
  get_loudest_VLT([searchPath '/figures/loudest.txt'],data,'Cut',cut);
 
end


%% PSD PLOTS
%------------------------------------------------------------------


fprintf('\nPSD estimation\n');
PSDplots([searchPath '/tmp/jobfile_bknd.txt'],...
         'Start',start,...
         'Stop',stop,...
         'HPFreq',22,...
         'Fmin',config.fmin,...
         'Fmax',config.fmax,...
         'Folder',[searchPath '/figures']);
%


%% GENERATED REPORT WEB PAGE 
%------------------------------------------------------------------
%
fprintf('mcpath=%s\n', mcPath)
fprintf('webPath=%s\n', webPath)
generate_html(searchPath,'Cut',cut,'MC',mcPath,'WEB',webPath);
exit
