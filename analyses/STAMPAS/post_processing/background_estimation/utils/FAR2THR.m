%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FAR2THR : get the SNR for a specific FAR
% PURPOSE :
%   get the clean FAR and try to estimate the SNR of a specific FAR
%   value
%
% AUTHOR  : Valentin FREY
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE    : Jun 2016
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER's PARAMETERS
FOLDER = '../../BKG/Start-1126073342_Stop-1137283217_ID-11/' % searchPath
FAR    = 1/(50*365*24*3600) % [s]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

load([FOLDER '/figures/FAR_clean.mat']);

idx=find(far<FAR & far>0);

y1 = far(idx(1)-1);
y2 = far(idx(1));
x1 = snr(idx(1)-1);
x2 = snr(idx(1));

THR=(FAR-y1)/(y2-y1)*(x2-x1)+x1;
fprintf('FAR : %g - THR : %f',FAR, THR);

exit