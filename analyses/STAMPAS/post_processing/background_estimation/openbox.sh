#!/bin/bash  
# wrapper for openbox.m matlab script
# ------------------------------------------------------------------
# Syntax:
#   openbox [args...]
# ------------------------------------------------------------------
# Purpose: 
#    wrapper to launch openbox.m matlab script with good parameters
#    there is two way to run this script : 
#
#    - without arguments : an interactive mode are beginning in order 
#    to guide user along the all processus. The processus are 
#    divide into several step to define the variable
#
#    - with argument : then just pass the good argument (see the syntax)
# 
# ------------------------------------------------------------------
# author:                         FREY valentin
# EMail Address:                  valentin.frey@gmail.com
# version                         1.0
# Comment:                        
# ------------------------------------------------------------------

# ------------------------------------------------------------------
# Syntax:
#   ./openbox.sh 
#      run in an interactively mode
#
#   ./openbox.sh DAG [OPTION]
#      DAG : dag path from STAMPAS folder eg: Start-1126569617_Stop-1129566062_ID-2
#      OPTION :
#         --DQFlags : add cut on DQflags
#         --TFveto  : add cut using TFveto
#         --Rveto   : add cut using Rveto
#         --SNRfrac n  : add cut on SNRfrac, n between 0 and 1
#         --MC MCdag : add a MC dag


# ----------------------------------------------------
## user function
# ---------------------------------------------------- 
usage(){
    echo 'background.sh script : '
    echo '* no argument usage '
    echo '    ./openbox.sh '
    echo '    then follow the intruction'
    echo ''
    echo '* with argument usage : '
    echo '    ./openbox.sh DAG [OPTIONS]'
    echo '       DAG     : path of the dag eg Start-1126073342_Stop-1137283217_ID-2'
    echo '       OPTIONS :'
    echo '          --DQFlags    : add cut on DQflags'
    echo '          --TFveto     : add cut using TFveto'
    echo '          --Rveto      : add cut using Rveto'
    echo '          --SNRfrac n  : add cut on SNRfrac, n between 0 and 1'
    echo '          --MC MCdag   : add a MC dag'
    echo '          --BKG BKGdag : add a MC dag'
    echo ''
}

read_input(){

    # --------------------------------------------------------------
    ### DAG STEP
    # --------------------------------------------------------------
    #
    dagStep=false;
    while [[ ${dagStep} == 'false' ]];do
        echo -e '\n\e[0;33mCHOOSE A DAG: \e[0m'
        echo -e '\e[0;33m | \e[0m'
        ls ../../BKG | awk 'BEGIN{i=0}{i++;printf "\033[0;33m | [%2d]\033[0m %s\n",i,$1}'
	dag=($(ls ../../BKG))
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | > \e[0m")" searchPath
	
	# check input
	check=`ls ../../BKG  | grep '${searchPath}'`
        if [[ ${check} == ${searchPath} ]];then
            ZL=`cat ../../BKG/$searchPath/config_bkg.txt | grep 'doZeroLag' | awk '{print $2}'`
	    if [[ $ZL == 0 ]];then
                echo -e "\e[0;33m | \e[1;31m this is not a ZeroLag dag. try another dag or use background.sh script\e[0m\n"
            else
		dagStep='true';
	    fi

	elif [[ ${searchPath} -ge 1 ]] && [[ ${searchPath} -le ${#dag[*]} ]];then
	    searchPath=${dag[$searchPath-1]}
            ZL=`cat ../../BKG/$searchPath/config_bkg.txt | grep 'doZeroLag' | awk '{print $2}'`
	    if [[ $ZL == 0 ]];then
                echo -e "\e[0;33m | \e[1;31m this is not a ZeroLag dag. try another dag or use background.sh script\e[0m\n"
            else
		dagStep='true';
	    fi
	else
            echo -e "\e[0;33m | \e[1;31m bad input. write a dag path or a dag idx\e[0m\n"	    
        fi
        searchPath='../../BKG/'${searchPath}
    done

    GPS_START=`echo ${searchPath} | sed -r 's/.*Start-([0-9]*).*/\1/'`
    GPS_END=`echo ${searchPath} | sed -r 's/.*Stop-([0-9]*).*/\1/'`
    ID=`echo ${searchPath} | sed -r 's/.*ID-([0-9]*).*/\1/'`
    
    if [ ${GPS_START} -lt 900000000 ]; then
        RUN="S5"
        SEARCH='s5-allsky'
    elif [ ${GPS_START} -lt 1000000000 ]; then
	RUN="S6"
	SEARCH='s6-allsky'
    elif [ ${GPS_START} -lt 1124004000 ]; then
        RUN="ER7"
        SEARCH='er7-allsky'
    elif [ ${GPS_START} -lt 1126073340 ]; then
        RUN="ER8"
        SEARCH='er8-allsky'
    elif [ ${GPS_START} -lt 1137283217 ];then
        RUN="O1"
        SEARCH='o1-allsky'
    else
        RUN="O2"
	SEARCH="o2-allsky"
    fi
    
    # --------------------------------------------------------------
    ### TFveto CUT STEP
    # --------------------------------------------------------------
    #
    TFvetoStep=false
    while [[ ${TFvetoStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE TFveto Cut (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" doTFveto
	
	# check input
        if [[ ${doTFveto} == '' ]];then
            doTFveto='y';
        fi
	
        if [[ ${doTFveto} != 'y' ]] && [[ ${doTFveto} != 'n' ]];then
            echo -e "\e[0;33m | \e[1;31m please write y or n\e[0m\n"
        elif [[ ${doTFveto} == 'y' ]];then
	    doTFveto=1
	    TFvetoStep='true'
        elif [[ ${doTFveto} == 'n' ]];then
	    doTFveto=0
            TFvetoStep='true'
        fi
    done
    

    # --------------------------------------------------------------
    ### Rveto step CUT STEP
    # --------------------------------------------------------------
    #
    RvetoStep=false
    while [[ ${RvetoStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE Rveto Cut (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" doRveto
	
	# check input
        if [[ ${doRveto} == '' ]];then
            doRveto='y';
        fi
	
        if [[ ${doRveto} != 'y' ]] && [[ ${doRveto} != 'n' ]];then
            echo -e "\e[0;33m | \e[1;31m please write y or n\e[0m\n"
        elif [[ ${doRveto} == 'y' ]];then
	    doRveto=1
            RvetoStep='true'
        elif [[ ${doRveto} == 'n' ]];then
	    doRveto=0
            RvetoStep='true'
        fi
    done


    # --------------------------------------------------------------
    ### SNRfrac CUT STEP
    # --------------------------------------------------------------
    #
    snrfracStep=false
    while [[ ${snrfracStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE SNRfrac Cut (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" doSNRfracCut
	
	# check input
        if [[ ${doSNRfracCut} == '' ]];then
            doSNRfracCut='y';
        fi
	
        if [[ ${doSNRfracCut} != 'y' ]] && [[ ${doSNRfracCut} != 'n' ]];then
            echo -e "\e[0;33m | \e[1;31m please write y or n\e[0m\n"
        elif [[ ${doSNRfracCut} == 'y' ]];then
	    doSNRfracCut=1
            snrfracStep='true'
        elif [[ ${doSNRfracCut} == 'n' ]];then
	    doSNRfracCut=0
	    SNRfracCut=''
            snrfracStep='true'
        fi
    done
    
    if [[ ${doSNRfracCut} == 1 ]];then
	snrfracCutStep=false;
	while [[ ${snrfracCutStep} == 'false' ]];do
            echo -e '\n\e[0;33mCHOOSE A SNRfrac THRESHOLD (0-1): \e[0m'
            echo -e '\e[0;33m | \e[0m'
            read -p "$(echo -e "\e[0;33m | [0.5]> \e[0m")" SNRfracCut
	    
       	    # check input
            if [[ ${SNRfracCut} == '' ]];then
		SNRfracCut=0.5;
            fi
	    
            if [[ ! ${SNRfracCut} =~ ^[+-]?[0-9]+\.?[0-9]*$ ]] && [[ $(echo $SNRfracCut | sed -r 's/(.*)\..*/\1/') -ge 1 ]];then
		echo -e "\e[0;33m | \e[1;31m SNRfraccut should be a number between 0 & 1\e[0m\n"
            else
		snrfracCutStep='true'
            fi
	done
    fi
    
    # --------------------------------------------------------------
    ### DQFlag step CUT STEP
    # --------------------------------------------------------------
    #
    DQFlagsStep=false
    while [[ ${DQFlagsStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE DQFlags Cut (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" doDQFlags
	
	# check input
        if [[ ${doDQFlags} == '' ]];then
            doDQFlags='y';
        fi
	
        if [[ ${doDQFlags} != 'y' ]] && [[ ${doDQFlags} != 'n' ]];then
            echo -e "\e[0;33m | \e[1;31m please write y or n\e[0m\n"
        elif [[ ${doDQFlags} == 'y' ]];then
	    doDQFlags=1
            DQFlagsStep='true'
        elif [[ ${doDQFlags} == 'n' ]];then
	    doDQFlags=0
            DQFlagsStep='true'
        fi
    done



    # --------------------------------------------------------------
    ### USE BKG STEP
    # --------------------------------------------------------------
    #
    useBKGStep=false;
    while [[ ${useBKGStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE BKG DAG (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [n]> \e[0m")" useBKG
	
	if [[ ${useBKG} == '' ]];then
	    BKG=''
            useBKGStep='true';
        elif [[ ${useBKG} == 'y' ]];then
            useBKGStep='true';
	else
            echo -e '\e[0;33m | \e[1;31m Bad answer, please write y or n\e[0m\n'
        fi
    done

    ### BKG DAG STEP
    # only if we wan't to use BKG is true
    #
    if [[ ${useBKG} == 'y' ]];then
	BKGStep=false;
	while [[ ${BKGStep} == 'false' ]];do
            echo -e '\n\e[0;33mCHOOSE A BKG DAG: \e[0m'
            echo -e '\e[0;33m | \e[0m'
            ls ../../BKG | awk 'BEGIN{i=0}{i++;printf "\033[0;33m | [%2d]\033[0m %s\n",i,$1}'
	    bkg=($(ls ../../BKG))
            echo -e '\e[0;33m | \e[0m'
            read -p "$(echo -e "\e[0;33m | > \e[0m")" BKG
	    

            # check input
	    check=`ls ../../BKG  | grep '${BKG}'`
	    if [[ ${check} == ${BKG} ]];then
		ZL=`cat ../../BKG/$BKG/config_bkg.txt | grep 'doZeroLag' | awk '{print $2}'`
		if [[ $ZL == 1 ]];then
                    echo -e "\e[0;33m | \e[1;31m this is a ZeroLag dag. try another dag\e[0m\n"
		else
		    BKGStep='true';
		    BKG='../../BKG/'${BKG}
		fi

	    elif [[ -d ${BKG} ]];then
		ZL=`cat $BKG/config_bkg.txt | grep 'doZeroLag' | awk '{print $2}'`
		if [[ $ZL == 1 ]];then
                    echo -e "\e[0;33m | \e[1;31m this is a ZeroLag dag. try another dag\e[0m\n"
		else
		    BKGStep='true';
		fi

	    elif [[ ${BKG} -ge 1 ]] && [[ ${BKG} -le ${#bkg[*]} ]];then
		BKG=${bkg[${BKG}-1]}
		ZL=`cat ../../BKG/$BKG/config_bkg.txt | grep 'doZeroLag' | awk '{print $2}'`
		if [[ $ZL == 1 ]];then
                    echo -e "\e[0;33m | \e[1;31m this is a ZeroLag dag. try another dag\e[0m\n"
		else
		    BKGStep='true';
		    BKG='../../BKG/'${BKG}
		fi		
	    else
		echo -e '\e[0;33m | \e[1;31m Bad answer, please enter a dag from the previous list\e[0m\n'
	    fi
	done
    fi


    # --------------------------------------------------------------
    ### USE MC STEP
    # --------------------------------------------------------------
    #
    useMCStep=false;
    while [[ ${useMCStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE MC DAG (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [n]> \e[0m")" useMC
	
	if [[ ${useMC} == '' ]];then
	    MC=''
            useMCStep='true';
        elif [[ ${useMC} == 'y' ]];then
            useMCStep='true';
        elif [[ ${useMC} == 'n' ]];then
            useMCStep='true';
	else
            echo -e '\e[0;33m | \e[1;31m Bad answer, please write y or n\e[0m\n'
        fi
    done

    ### MC DAG STEP
    # only if we wan't to use MC is true
    #
    if [[ ${useMC} == 'y' ]];then
	MCStep=false;
	while [[ ${MCStep} == 'false' ]];do
            echo -e '\n\e[0;33mCHOOSE A MC DAG: \e[0m'
            echo -e '\e[0;33m | \e[0m'
            ls ../../BKG | awk 'BEGIN{i=0}{i++;printf "\033[0;33m | [%2d]\033[0m %s\n",i,$1}'
	    mc=($(ls ../../BKG))
            echo -e '\e[0;33m | \e[0m'
            read -p "$(echo -e "\e[0;33m | > \e[0m")" MC
	    

            # check input
            check=`ls ../../BKG  | grep '${searchPath}'`
	    if [[ ${check} == ${MC} ]];then
		isMC=`cat ../../BKG/$MC/config_bkg.txt | grep 'doMC' | awk '{print $2}'`
		if [[ $isMC == 0 ]];then
                    echo -e "\e[0;33m | \e[1;31m this not a MC dag. try another dag\e[0m\n"
		else
		    MCStep='true';
		fi
		
	    elif [[ ${MC} -ge 1 ]] && [[ ${MC} -le ${#mc[*]} ]];then
		MC=${mc[${MC}-1]}
		isMC=`cat ../../BKG/$MC/config_bkg.txt | grep 'doMC' | awk '{print $2}'`
		if [[ $isMC == 0 ]];then
                    echo -e "\e[0;33m | \e[1;31m this not a MC dag. try another dag\e[0m\n"
		else
		    MCStep='true';
		fi
		
	    else
		echo -e '\e[0;33m | \e[1;31m Bad answer, please enter a dag from the previous list\e[0m\n'
	    fi
            MC='../../BKG/'${MC}
	done
    fi



    # --------------------------------------------------------------
    ### WEB PATH STEP
    # --------------------------------------------------------------
    #
    webStep=false;
    while [[ ${webStep} == 'false' ]];do
        echo -e '\n\e[0;33mWEB FOLDER : \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/background_estimation]> \e[0m")" webfolder
	
        if [[ ${webfolder} == '' ]];then
	    webfolder=${SEARCH}'/Start-'${GPS_START}'_Stop-'${GPS_END}'_ID-'${ID}'/background_estimation/'
        fi
        webStep=true;
    done
}


# ----------------------------------------------------
## INPUT PARSER
# ---------------------------------------------------- 
if [[ $# -eq 0 ]]; then
    usage;
    read_input;
    
elif [[ $# -ge 1 ]]; then
    if [[ $1 == '-h' ]];then
        usage
        exit 0
    fi

    searchPath=`echo $1 | sed -r 's|../../BKG/||'` || exit $?
    searchPath='../../BKG/'${searchPath}

   if [[ ! -d ${searchPath} ]];then
	echo -e '\e[1;31m bad DAG path : directory $1 not found\e[0m\n' 
	exit 1
    fi
    GPS_START=`echo ${searchPath} | sed -r 's/.*Start-([0-9]*).*/\1/'`
    GPS_END=`echo ${searchPath} | sed -r 's/.*Stop-([0-9]*).*/\1/'`
    ID=`echo ${searchPath} | sed -r 's/.*ID-([0-9]*).*/\1/'`
    doTFveto=0
    doRveto=0
    doDQFlags=0
    SNRfracCut=''
    MC=''
    BKG=''

    ### get options
    while [ $# -gt 0 ]; do
	case $1 in
	    --TFveto )
		doTFveto=1
		;;

	    --SNRfrac )
		shift
		SNRfracCut=$1
		;;

	    --Rveto )
		doRveto=1
		;;
	    
	    --MC )
		shift
		MC=$1
		;;

	    --BKG )
		shift
		BKG=$1
		;;
	    
	    --DQFlags )
		doDQFlags=1
		;;
	    
	esac
	shift
    done
else
    usage;
fi

# ----------------------------------------------------
## CHECK INPUT
# ---------------------------------------------------- 
ZL=`cat $searchPath/config_bkg.txt | grep 'doZeroLag' | awk '{print $2}'`
if [[ $ZL == 0 ]];then
    echo 'DAG : ' ${searchPath} 'is not a ZeroLag  dag'
    echo 'please run the background script to process this dag. '
    echo 'abord now ... '
    exit 1
fi

if [[ ${BKG} != '' ]];then
    ZL=`cat $BKG/config_bkg.txt | grep 'doZeroLag' | awk '{print $2}'`
    if [[ $ZL == 1 ]];then
	echo 'DAG : ' ${BKG} 'is a ZeroLag  dag'
	echo 'please run the openbox script to process this dag. '
	echo 'abord now ... '
	exit 1
    fi
fi

if [[ ${MC} != '' ]];then
    isMC=`cat $MC/config_bkg.txt | grep 'doMC' | awk '{print $2}'`
    if [[ $isMC == 0 ]];then
	echo  'DAG : ' $MC
	echo 'is not a MC dag'
	echo 'abord now ... '
	exit 1
    fi
fi

if [ ${#GPS_START} -lt 9 ]; then
    echo GPS_START $GPS_START should have 9 or 10 digits
    exit 1
fi

if [ ${#GPS_END} -lt 9 ]; then
    echo GPS_END $GPS_END should have 9 or 10 digits
    exit 1
fi

if [ ! "$ID" = "${ID%[[:space:]]*}" ]; then
    echo ID $ID should not have white space
    exit 1
fi

if ! [ -d ${searchPath}'/logfiles' ];then
    mkdir -p ${searchPath}'/logfiles'
fi
echo $* `date` > ${searchPath}'/logfiles/background.txt'


HOST=`hostname -d | cut -d "." -f 1`
if [[ $HOST == "atlas" ]]; then
    web_repo='WWW/LSC'
    web_server="http://www.atlas.aei.uni-hannover.de/"
    web_server_suffix='/LSC/'
elif [[ $HOST == "ligo" ]];then
    web_repo='public_html'
    web_server="https://ldas-jobs.ligo.caltech.edu/"
    web_server_suffix=''
else
    echo "Only designed to work on LIGO and ATLAS clusters. Exiting..."
    exit
fi


# ----------------------------------------------------
## MAIN
# ---------------------------------------------------- 
MATLAB_args='-nodisplay -singleCompThread'
matlab ${MATLAB_args} <<EOF
  searchPath='${searchPath}';
  mcPath='${MC}';
  bkgPath='${BKG}';

  doTFveto=${doTFveto};
  doRveto=${doRveto};
  doDQFlags=${doDQFlags};
  SNRfracCut='${SNRfracCut}';                                      

  webPath = '${webfolder}';

  openbox;
EOF

web_omega_folder=${SEARCH}'/LOUDEST/'
output='/home/'${USER}'/'${web_repo}'/'${webfolder}

# Start the omega scans only at CIT
# Should be corrected to allow omegascan on hoft elsewhere
HOST=`hostname -d | grep caltech`
if [ ${#HOST} -gt 0 ]; then
    echo ' '
    echo 'Omegascan of the loudest events will be generated in a screen session. Please wait for few hours. '
    echo 'Things will appear in:'
    echo ${web_server}'~'${USER}'/'${web_omega_folder}'/OMEGA/'

    screen -dmS 1 sh -c "sleep 5s; cd ../omegaScan/; ./launchOmegaScanList.sh ${web_omega_folder} ../../${searchPath}/figures/loudest.txt 1"
    screen -dmS 1 sh -c "sleep 5s; cd ../omegaScan/; ./launchOmegaScanList.sh ${web_omega_folder} ../../${searchPath}/figures/loudest.txt 2"
else
    echo 'Omegascan of the loudest events will be generated by hand using the following scripts run manualy at CIT: ' `ls omegascan_*.sh`
    cat omegascan_H1.sh | sort | uniq > tmp.sh
    mv tmp.sh omegascan_H1.sh

    cat omegascan_L1.sh | sort | uniq > tmp.sh
    mv tmp.sh omegascan_L1.sh
fi

### generate the ftmaps
echo ' '
echo 'Fftmap of the loudest events will be generated in a screen session. Please wait for few hours. '
screen -dmS 1 sh -c "sleep 5s;matlab -nodisplay -r \"plotlist '${searchPath}/figures/loudest.txt' '${webfolder}'; exit;\""

echo ' '
echo 'Summary page will appear in:' 
echo ${web_server}'~'${USER}${web_server_suffix}'/'${webfolder}


exit 0

