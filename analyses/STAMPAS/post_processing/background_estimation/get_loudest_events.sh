#!/bin/bash

# functions
usage(){
    echo './get_loudest_events.sh DAG [OPTIONS]'
    echo '   DAG: dag path eg ../../BKG/Start-1126569617_Stop-1129566062_ID-2'
    echo '   OPTIONS:'
    echo '      --DQFlags   : remove triggers flaged by DQFlags'
    echo '      --SNRfrac N : remove triggers flags by SNRfrac cut , N should be between 0 & 1' 
    echo '      --TFveto    : remove triggers flaged by TFveto'
    echo '      --Rveto     : remove triggers flaged by Rveto'
    echo '      --Max N     : get the N first trigger, default 100 '
    echo '      --SNR N     : triggers with SNR greater than N, default 30'
    echo '      -o file     : output file name, default loudest.txt'

    exit
}

if [[ $# -eq 0 ]];then
    usage
    exit
fi

#----------------------------------------------------------
## DEFAULT PARAMETERS
#----------------------------------------------------------
#
cut="'noCut'"
SNR=30
MAX=100
OUT='./loudest.txt'

#----------------------------------------------------------
## INPUT PARSER
#----------------------------------------------------------
#
searchPath=$1

shift
while [ $# -gt 0 ]; do
    case $1 in
        --TFveto )
            cut=$cut",'TFveto'";
            ;;
	
        --SNRfrac )
	    shift
            cut=$cut",'SNRfrac:$1'";
            ;;

        --Rveto )
            cut=$cut",'Rveto'";
            ;;

        --DQFlags )
            cut=$cut",'DQFlags'";
            ;;

        --SNR )
	    shift
	    SNR=$1
            ;;

        --Max )
	    shift
	    MAX=$1
            ;;

        -o )
	    shift
	    OUT=$1
            ;;

	*)
	    echo 'unknow options : '$1	    
	    exit
	    ;;

    esac
    shift
done

echo "run : get_loudest(${OUT},data,'SNR',${SNR},'Max',${MAX},'Cut',{${cut}});"

#----------------------------------------------------------
## MAIN
#----------------------------------------------------------
#
matlab -nodisplay <<EOF
load ${searchPath}/results/results_merged/results_filtered.mat
get_loudest('${OUT}',data,'SNR',${SNR},'Max',${MAX},'Cut',{${cut}});
exit
EOF
