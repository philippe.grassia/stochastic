function [trueGPS1,trueGPS2] = findTrueGPS(startGPS,endGPS,startGPS1,endGPS1,startGPS2,endGPS2,ID,pair,t_start,matfilesPath,matfilesName)
% function findTrueGPS()
% This functions retrieve the true GPS of the data
% used to compute a given ft-map. Specifically it returns
% when used with plotTriggers, the GPS start times of
% the trigger. This is specially important when the analysis
% has been done without the noStitching option. Data chunks
% without gaps between them can be used, and the GPS is known
% with certainty only at the beginning and then end of each job.
%
% CAREFUL: The trigger can appear in-between two data chunks.
% This algorithm only calculates the GPS times associated with 
% the beginning of the triggers. It does not say anything about
% the GPS times used "inside" the algorithm!!
%
% Written by S. Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

searchPath=generate_searchPath('BKG',startGPS,endGPS,ID);
searchPath=['../../' searchPath];

% Segment duration of available data.
params.segmentDuration = 1;

% Location of data.
itf1Prefix=[pair(1) '-' pair(1:2) '_' matfilesName];
itf2Prefix=[pair(3) '-' pair(3:4) '_' matfilesName];
params.anteproc.inmats1 = [matfilesPath '/' itf1Prefix];
params.anteproc.inmats2 = [matfilesPath '/' itf2Prefix];
params.anteproc.useCache = false;

% Useful quantities.
segDur = params.segmentDuration/2;


% Don't change these parameters!
params.anteproc.jobFileTimeShift = false;
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;

%% For the first ifo

% Start and end GPS times to find data between.
params.startGPS = startGPS1;
params.endGPS = endGPS1;

% Find all anteproc'd .mat files with GPS times
% within the specified start and end times.
matlist = find_anteproc_mats(params);

% Loop over mat files corresponding to detector one.  Calculate data
% available based on GPS start time and total length (determined from file
% names).  Make a list (gps1) of all segment START times available.
gps1 = [];
for ii=1:length(matlist.ifo1)
  gps_start = regexp(matlist.ifo1{ii},'-(\d{9,10}(|\.\d+))-','tokens','once');
  gps_start = str2num(gps_start{:});
  dur = regexp(matlist.ifo1{ii},'-(\d+|\d+\.\d+)\.mat','tokens','once');
  dur = str2num(dur{:});
  gps1 = [gps1 gps_start:segDur:(gps_start + dur - params.segmentDuration)];
end

% Do same calculation for detector 2.

% Start and end GPS times to find data between.
params.startGPS = startGPS2;
params.endGPS = endGPS2;

% Find all anteproc'd .mat files with GPS times
% within the specified start and end times.
matlist = find_anteproc_mats(params);

gps2 = [];
for ii=1:length(matlist.ifo2)
  gps_start = regexp(matlist.ifo2{ii},'-(\d{9,10}(|\.\d+))-','tokens','once');
  gps_start = str2num(gps_start{:});
  dur = regexp(matlist.ifo2{ii},'-(\d+|\d+\.\d+)\.mat','tokens','once');
  dur = str2num(dur{:});
  gps2 = [gps2 gps_start:segDur:(gps_start + dur - params.segmentDuration)];
end

% Remove any duplicate GPS times that may occur due to possible
% overlap of .mat files.
gps1 = sort(unique(gps1));
gps2 = sort(unique(gps2));
gps1=gps1';
gps2=gps2';

%% True GPS in IFO 1

[void,idx]=max(double(gps1==startGPS1));
trueGPS1=gps1(idx+t_start/segDur);

%% True GPS in IFO 2

[void,idx]=max(double(gps2==startGPS2));
trueGPS2=gps2(idx+t_start/segDur);
   

