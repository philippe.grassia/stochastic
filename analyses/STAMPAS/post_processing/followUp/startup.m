% Designed to set up the matlab environment for running the
% STAMP All-Sky pipeline.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EDIT THE FOLLOWING:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Path to your stamp2 directory.
stamp2_dir = '/home/sfranco/STAMP_clean/stamp2';

% Path to the functions subdirectory of the STAMPAS repertory.
func_dir = '/home/sfranco/newSTAMPAS/functions';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

verNum = str2num(version('-release'));
if(~isempty(verNum))
  verOld = verNum < 14;
 else
   verOld = false;
end
if(verOld || (~isdeployed))

addpath(genpath(stamp2_dir));
addpath(func_dir);

[no_use,h] = system('hostname -d');
h = h(1:(end-1)); % remove endline
if strcmpi(h,'atlas.aei.uni-hannover.de')
  addpath('/opt/lscsoft/ligotools/matlab');
end

end
