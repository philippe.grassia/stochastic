function clustermap_followUp_bkg(job1,job2)
% function clustermap_followUp_bkg(job1,job2)
%
% Purpose: follow up on triggers in a time-slide or zero-lag
%   analysis.  Need to know the job number of the trigger(s)
%   in detector 1 and detector 2.  Prints diagnostic ft-maps
%   of the interval in question.
%
% Inputs:
%   job1 - job for detector 1
%   job2 - job for detector 2
%
% Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters to edit.
% GPS start and end times of the full search and the ID number.
GPS_start = 816065659;
GPS_end = 877591542;
ID = 40;

% Add functions.
addpath('../../');
startup;

% Set up search path.
searchPath = generate_searchPath('BKG',GPS_start,GPS_end,ID);
searchPath = ['../../' searchPath];

% Initialize default parameters.
params = stampDefaults;

% Load search parameters
cparams = load([searchPath '/tmp/clustParam.mat']);

% Load basic jobfile (not adjusted by variable windows).
if (job1 == job2)
  zeroLag = true;
  jobfile = load([searchPath '/tmp/joblistTrue.txt']);
else % time-slide
  zeroLag = false;
  jobfile = load([searchPath '/tmp/bknd_jobfile.txt']);
end
Njobs = size(jobfile,1);

% Calculate lag number, accounting for circular time-shift.
lag = job2 - job1;
if (lag < 0)
  lag = lag + Njobs;
end

% If using variable windows, overwrite jobfile
% with variable windows-adjusted jobfile.
if cparams.doVariableWindows
  % Get nbGroupsPerJob so we can load the correct
  % variable windows-adjusted jobfile.
  [names,values] = textread([searchPath '/config_bkg.txt'], ...
			    '%s %s\n',-1,'commentstyle','matlab');
  nbGroupsPerJob = values(find(strcmp(names,'nbGroupsPerJob')));
  % Convert from cell -> string -> number.
  nbGroupsPerJob = str2num(cell2mat(nbGroupsPerJob));
  
  % Get first analysis in each group of science segments.
  % (all jobs in a group are analyzed in a single Condor
  % job, see functions/make_dag_bkg_anteproc.m)
  groups = unique(jobfile(:,5));
  group_starts = groups(1):nbGroupsPerJob:groups(end);
  
  % Determine which group job1 falls into.
  g_idx = find(group_starts <= job1);
  g_idx = g_idx(end);
  
  % Load variable windows-adjusted_jobfile.
  if zeroLag
    jobfile_path = [searchPath '/tmp/joblistTrue_' num2str(group_starts(g_idx)) ...
		    '.txt'];
  else
    jobfile_path = [searchPath '/tmp/bknd_jobfile_' num2str(lag) '_' ...
		    num2str(group_starts(g_idx)) '.txt'];
  end
  jobfile = load(jobfile_path);
end

% Get segment duration from params file.
pfiles = dir([searchPath '/tmp/params*.txt']);
[names,values] = textread([searchPath '/tmp/' pfiles(1).name],'%s %s\n',-1, ...
			  'commentstyle','matlab');
segmentDuration = values(find(strcmp(names,'segmentDuration')));
% Convert from cell -> string -> number.
segmentDuration = str2num(cell2mat(segmentDuration));
% Assign to params struct.
params.segmentDuration = segmentDuration;

% Set up GPS start/end times and anteproc job parameters.
params.startGPS = jobfile(job1,2);
params.endGPS = jobfile(job1,3);
params.anteproc.jobNum1 = job1;
params.anteproc.jobNum2 = job2;

% Set up other parameters.
% Can be hard-coded for STAMPAS background studies.
params.alternative_sigma = cparams.altSigma;
params.fmin = str2num(cparams.fmin);
params.fmax = str2num(cparams.fmax);
params.anteproc.loadFiles = true;
params.anteproc.timeShift1 = 0;
params.anteproc.timeshift2 = 0;
params.anteproc.jobFileTimeShift = true;
params.anteproc.jobfile = jobfile_path;
params.matavailable = true;


% Handle cachefile or paths to pre-processed .mat files.
if (exist([searchPath '/anteproc_cache.mat']) == 2)
  params.anteproc.useCache = true;
  params.anteproc.cacheFile = [searchPath '/anteproc_cache.mat'];
else
  itf1Prefix=[cparams.pair(1) '-' cparams.pair(1:2) '_' cparams.matfilesName];
  itf2Prefix=[cparams.pair(3) '-' cparams.pair(3:4) '_' cparams.matfilesName];
  params.anteproc.inmats1 = [matfilesPath '/' itf1Prefix];
  params.anteproc.inmats2 = [matfilesPath '/' itf2Prefix];
end

% Stitching of data?
if (lag ~= 0 & ~cparams.noStitching)
  params.anteproc.bkndstudy = true;
  params.anteproc.bkndstudydur = jobfile(job1,3) - jobfile(job1,2) + ...
      params.segmentDuration/2;
else
  params.anteproc.bkndstudy = false;
end

% Notched frequencies.
params.doStampFreqMask = true;
params.StampFreqsToRemove = textread([searchPath '/tmp/frequencies.txt'],'%d',-1,'commentstyle','matlab');

% Clustering parameter.s
params = burstegardDefaults(params);
doZebra = str2num(cparams.doZebra);

% Set up sky positions and zebragard parameters.
% Seed is the sum of ifo1 and ifo2 start gps (without variable windows
% effect).  This is copied from functions/run_clustermap.m
rand('twister',jobfile(job1,2)+jobfile(job2,2));
if (doZebra)
  params = zebragardDefaults(params);
  params.ra = rand(5,1)*24;   % right ascension in hours
  params.dec = (180/pi)*(acos(1-2*rand(5,1))-pi/2); % declination in degrees  
else
  params.ra=rand(1,1)*24;   % right ascension in hours
  params.dec=(180/pi)*(acos(1-2*rand(1,1))-pi/2); % declination in degrees
  params.skypatch = true;
  params.fastring = true;
  params.dtheta = 0.7;
  params.thetamax = 180;
end

% Apply a glitch cut (parameters hard-coded in STAMPAS).
params.glitch.doCut = true;
params.glitch.numBands = 3;
params.glitch.doCoincidentCut = false;

% save all clusters
params.doAllClusters=true;
params.stochPlus = true; %% for overlapping ft-maps and to unify the results format

% save plots
params.savePlots = true;

% call clustermap
stoch_out=clustermap(params, params.startGPS, params.endGPS); %%STAMP

% EOF