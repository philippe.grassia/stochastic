format long g

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function plotTriggers()
%
% Traditional STAMP wrapper function, adapted to STAMPAS.
% Used to plot ft-maps around precise triggers found in both
% bkg and inj studies.
%
%Adapted to STAMPAS by: Samuel Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PARAMETERS OF THE SEARCH
addpath('..')
searchParameters

altSigma=false;   %% activate altSigma
allSky=false;      %% launch an all-sky search

%% PARAMETERS OF THE TRIGGER

GPS=829468587;
RA=23.9298;
DEC=-67.117;
TimeSlide=1;

%% For injections
waveform=['adi_B_tapered.dat'];
alpha=100;

%% OPTIONS
params = stampDefaults; %% DON'T TOUCH THAT
params.saveMat=false;  %% save matfiles containing numerical data of the maps
params.savePlots=true; %% save the plots
saveResults=true; %% save trigger results in a text file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% output file prefix
str ='test';
stoch_out.start=true;

params.alternative_sigma=altSigma;

searchPath=generate_searchPath(TYPE,GPS_start,GPS_end,ID);
searchPath=['/home/sfranco/newSTAMPAS/' searchPath];

a=[searchPath '/tmp/bknd_jobfile.txt'];
b=[searchPath '/tmp/joblist.txt'];
c=[searchPath '/tmp/joblist_injections.txt'];
type=['bkg'];

if exist(a)==2
   doAnte=1;
   doBkg=1;
   l=load(a);
   l=l(l(:,2)<=GPS,:);
   idx=l(end,5);
   l=l(l(:,5)==idx,:);

   jobNumber0=l(1,1);
   jobFile=[searchPath '/tmp/bknd_jobfile_' num2str(TimeSlide) '_' num2str(jobNumber0) '.txt'];
   if exist(jobFile)~=2
      while exist(jobFile)~=2
         idx=idx-1;
         l=load(a); 
         l=l(l(:,2)<=GPS,:);
         l=l(l(:,5)==idx,:);
         jobNumber0=l(1,1);
         jobFile=[searchPath '/tmp/bknd_jobfile_' num2str(TimeSlide) '_' num2str(jobNumber0) '.txt'];
      end
   end
elseif exist(c)==2
   doAnte=0;
   doBkg=0;
   jobFile=c;
   type='inj';

   fid=fopen([searchPath '/waveforms.txt'],'r');
   C=textscan(fid, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n','Delimiter',' ','CommentStyle','%','EmptyValue',0,'MultipleDelimsAsOne',1,'CollectOutput',1);
   fclose(fid);
   % first column is waveform names
   waveforms=C{:}(:,1);
   % the rest gives alphas values, need to convert
   % from cell to array of numbers.
   temp = C{:}(:,2:end);
   for ii=1:size(temp,1)
     alphas{ii}=cell2mat(cellfun(@(x) str2num(x),temp(ii,:),'UniformOutput',0));
   end
   clear C;

   for i=1:size(waveforms,1)
      W=waveforms{i};
      if strcmp(W,waveform)
         wvfNumber=i;
         break;
      end
   end

   alphas=alphas{i};

   for j=1:size(alphas,2)
      if alphas(j)==alpha;
         subJob=j;
         break;
      end
   end

else
%% should not happen
end

jobs=load(jobFile);
jobs_tmp=jobs(jobs(:,2)==GPS,:);
jobNumber=jobs_tmp(1);
clear jobs_tmp;
configFile=[searchPath '/config_' type '.txt'];

[names,values]=textread(configFile, '%s %s\n',-1,'commentstyle','matlab');

matLocDefault=true;

for ii=1:length(names)
   switch names{ii}
      case 'pair'
      pair = values{ii};
  
      case 'fmin'
      params.fmin = str2num(values{ii});

      case 'fmax'
      params.fmax = str2num(values{ii});

      case 'doZebra'
      doZebra = str2num(values{ii});

      case 'noStitching'
      noStitching = str2num(values{ii});

      case 'matfilesLocation'
      matfilesLocation = values{ii};
      if strcmp(matfilesLocation,'default')
         matLocDefault = true;
      else
         matLocDefault = false;
      end

      case 'matfilesName'
      matfilesName = values{ii};
   otherwise

   end
end

if matLocDefault
   matfilesPath = [searchPath '/matfiles'];
else
   matfilesPath = [matfilesLocation];
end


% anteproc pre-processing parameters
if doAnte
   params.anteproc.loadFiles = true;
   itf1Prefix=[pair(1) '-' pair(1:2) '_' matfilesName];
   itf2Prefix=[pair(3) '-' pair(3:4) '_' matfilesName];
   params.anteproc.inmats1 = [matfilesPath '/' itf1Prefix];
   params.anteproc.inmats2 = [matfilesPath '/' itf2Prefix];

   % Time-shift amount (if doing a "time-based" time-shift).
   params.anteproc.timeShift1 = 0;
   params.anteproc.timeShift2 = 0; 
   % Do a jobfile time-shift?
   params.anteproc.jobFileTimeShift = true;

   % Jobfile to use.
   if TimeSlide~=0
      params.anteproc.jobfile = [searchPath '/tmp/bknd_jobfile_' num2str(TimeSlide) '_' num2str(jobNumber0) '.txt'];
   else
      params.anteproc.jobfile = [searchPath '/tmp/joblist.txt'];
   end
 
   % Load data from which job for each detector?
   params.anteproc.jobNum1 = jobNumber;
   params.anteproc.jobNum2 = jobNumber+TimeSlide;
 
   % Remove gaps in data?
   if TimeSlide~=0 & ~noStitching
      params.anteproc.bkndstudy = true;
   else
      params.anteproc.bkndstudy = false;
   end

   % How long are the maps supposed to be (in seconds)? The code double-checks
   % the map size after removing gaps.
   params.anteproc.bkndstudydur = (jobs((jobNumber),4));
end

% input mat directory
if doBkg
   params.matavailable=true;
elseif ~doBkg
   params.matavailable=false;
end  

if ~(doBkg)
   params.paramsFile = [searchPath '/tmp/params_inj_' num2str(wvfNumber) '_' num2str(jobNumber) '_' num2str(subJob) '.txt'];
else
   params.paramsFile = [searchPath '/tmp/params_H1.txt'];
end

% jobfile
if TimeSlide~=0 & (doAnte)
   params.jobsFile = [searchPath '/tmp/bknd_jobfile_' num2str(TimeSlide) '_' num2str(jobNumber0) '.txt'];
elseif (doBkg) & (doAnte)
   params.jobsFile = [searchPath '/tmp/joblistTrue.txt'];
elseif ~(doBkg) & (~doAnte)
   params.jobsFile = [searchPath '/tmp/joblist_injections.txt'];
elseif ~(doAnte)
  %% should not happen
   params.jobsFile = jobFile;
end

t_start=num2str(jobs((jobNumber),2));
t_end=num2str(jobs((jobNumber),3));

% search direction-------------------------------------------------------------
params.ra=RA;   % right ascencion in hours 
params.dec=DEC; % declination in degrees 

% apply notches
params.doStampFreqMask=true;
params.StampFreqsToRemove = textread([searchPath '/tmp/frequencies.txt'],'%d',-1,'commentstyle','matlab');

% perform a clustering search search
params = burstegardDefaults(params);

% Uses zebragard to speed-up the search
if (doZebra)
   params=zebragardDefaults(params);
end

%% do or not all sky search
if allSky
   params.skypatch = true;
   params.fastring = true;
   params.dtheta=0.7;
   params.thetamax = 180;
end

% Parameters and their values.

params.doBurstegard = true;
params.burstegard.NCN = 80;
params.burstegard.NR = 2;
params.burstegard.pixelThreshold = 0.75;
params.burstegard.tmetric = 1;
params.burstegard.fmetric = 1;
params.burstegard.debug = 0;

% apply a glitch cut
params.glitch.doCut = true;
params.glitch.doCoincidentCut=false;
params.glitch.numBands=3;

% create loop over search directions
 
params.doAllClusters = true;
params.stochPlus = true; %% for overlapping ft-maps and to unify the results format

%Variable windows

% Lag used for the seconds ifo data
currentLag=mod((jobNumber)+TimeSlide,size(jobs,1));
if currentLag==0
   currentLag=size(jobs,1);
end


%% Temporarily adapt the duration of the ifo 2 job
if doAnte
  l=load(a);
  if l(jobNumber,2)~=jobs(jobNumber,2)
     newOverlap=jobs(jobNumber,4)-l(currentLag,4);
     oldStart2=l(currentLag,2);
     t_start_num_ifo2=l(currentLag-1,2);
     [newStart2,newDuration2]=anteproc_varWindows(t_start_num_ifo2,l(currentLag,3), ...
                              params.anteproc.inmats1,params.anteproc.inmats2,oldStart2,...
                              l((jobNumber)+(TimeSlide),4),newOverlap);
     memoOldStart=oldStart2;
     memoOldDuration=l(currentLag,4);
     jobs(currentLag,2)=newStart2;
     jobs(currentLag,4)=newDuration2;
     dlmwrite(jobFile,jobs,'delimiter',' ','precision','%.1f');
  end
end

GPSStart2=jobs(currentLag,2);
GPSEnd2=jobs(currentLag,3);

% call clustermap
try
   if ~doBkg %% to be able to read the waveform files
      cd ../..
   end
   stoch_out=clustermap(params, t_start, t_end); %%STAMP  
   if params.savePlots & ~doBkg
      system('mv *.png post_processing/followUp');
      system('mv *.eps post_processing/followUp');
   end 
   if params.saveMat
      system('mv *.mat post_processing/followUp');
   end
catch  %% in case of error, jobfile is reset the way it was 
   if exist('memoOldStart')==1
      jobs(currentLag,2)=memoOldStart;
      jobs(currentLag,4)=memoOldDuration;
      clear memoOldStart
      clear memoOldDuration
      dlmwrite(jobFile,jobs,'delimiter',' ','precision','%.1f');
   end
   error('clustermap failed')
end

%% Jobfile restored
if exist('memoOldStart')==1
   jobs(currentLag,2)=memoOldStart;
   jobs(currentLag,4)=memoOldDuration;
   clear memoOldStart    
   clear memoOldDuration
   dlmwrite(jobFile,jobs,'delimiter',' ','precision','%.1f');
end

if saveResults
   if ~doBkg
      cd post_processing/followUp
   end

   RESULT=[];
   gCuts=[];
   resultsGCuts=[];

   for ii=1:size(stoch_out.max_SNR,2) %%Trigger list generated
       for jj=1:length(stoch_out.allClustersSnrs{ii})
         RESULT=[RESULT; str2num(t_start), stoch_out.sources(ii,1), stoch_out.sources(ii,2),stoch_out.allClustersFreqs{ii}(jj,1), ...
                stoch_out.allClustersFreqs{ii}(jj,2) ,stoch_out.allClustersSnrs{ii}(jj), stoch_out.allClustersTimes{ii}(jj,1), ...
                stoch_out.allClustersTimes{ii}(jj,2), stoch_out.allClustersSNRfracTimes{ii}(jj), ...
                stoch_out.allClustersNbPix{ii}(jj), 0, jj, TimeSlide];
         if doAnte&~allSky
             [gps1,gps2]=findTrueGPS(GPSstart,GPSend,str2num(t_start),str2num(t_end),GPSStart2,GPSEnd2,ID,pair,stoch_out.allClustersTimes{ii}(jj,1),matfilesPath,matfilesName);
             display(['Trigger ' num2str(jj) ])
             display(['True ' pair(1:2) ' startGPS: ' num2str(gps1)]);
             display(['True ' pair(3:4) ' startGPS: ' num2str(gps2)]);
             RESULT(jj,11)=gps2-str2num(t_start);         
         end
      end

       %% Works only with 1s resolution
       tCuts=(stoch_out.cutcols{ii}')*1/2;
       ra=repmat(stoch_out.sources(ii,1),size(tCuts));
       dec=repmat(stoch_out.sources(ii,2),size(tCuts));
       lag=repmat(TimeSlide,size(tCuts));
       t_start_col=repmat(str2num(t_start),size(tCuts));
       detCuts = stoch_out.detcuts{ii}';
       gCuts=[gCuts; t_start_col tCuts ra dec lag detCuts];
       resultsGCuts=[resultsGCuts; t_start_col+tCuts];
   end

   resultsGCuts=unique(resultsGCuts);
   dlmwrite('resultsGCut.txt',resultsGCuts,'delimiter',' ','precision','%.4f');
   dlmwrite('results.txt',RESULT,'delimiter',' ','precision','%.4f');

end
