#! /bin/bash

# functions
usage(){
    echo 'Arg 1: ifo name [L1/H1]'
    exit
}

[[ $# -ne 1 ]] && usage

IFO=$1

# To be customized
UPV_FOLDER='/home/detchar/public_html/NewUPV/O1/Omicron/'${IFO}'/2kHz/'


rm tmp
if [ -d UPV/${IFO}/UPV ]; then
    rm UPV/${IFO}/UPV/*
else
    mkdir -p UPV/${IFO}/UPV
fi
if [ -d UPV/${IFO}/UPV_H ]; then
    rm UPV/${IFO}/UPV_H/*
else
    mkdir -p UPV/${IFO}/UPV_H
fi

# Hierarchical vetoes
for file in `ls ${UPV_FOLDER}/DARM*[0-9]-H/`; do
    echo $file | grep veto | grep -v up | grep -v "/:" | grep -v gif >> tmp
done

cat tmp | sort | uniq > tmp1
cat tmp1 | awk -F 'round' '{print $1}' > tmp2
cat tmp2 | sort | uniq > tmp3


for file in `cat tmp3`; do
    cat ${UPV_FOLDER}/DARM*[0-9]-H/${file}* >> UPV/${IFO}/UPV_H/${file}veto-H.txt
done

# Non hierarchical vetoes
rm tmp
for file in `ls ${UPV_FOLDER}/DARM*[0-9]`; do
    echo $file | grep veto | grep -v up | grep -v "DARM" | grep -v gif >> tmp
done

cat tmp | sort | uniq > tmp1

for file in `cat tmp1`; do
    cat ${UPV_FOLDER}/DARM*[0-9]/${file} >> UPV/${IFO}/UPV/${file}
done

rm tmp*
echo 'UPV veto lists are in the UPV folder. They need to be move manually into allDQs folder.'