function apply_vetoes(input, output, veto, ifo)
% Apply veto list to triggers (hierarchical approach)

% Check veto list exist
if exist(veto)==2
  dq=load(veto);
  tr=load(input);
  tr_save=tr;

  display(['Applying vetoes in ' veto]);
  index=ones(size(tr,1),1);


  if size(tr,2)>4
  if str2num(ifo)==1
      idx_ifo=14;
    else
      idx_ifo=15;
    end
    tr1=tr(:,[idx_ifo idx_ifo 2]);
    tr1(:,2)=tr1(:,2)+3;
    tr=tr1;
  end

  for i=1:size(dq,1)
    ext=find(tr(:,1)<dq(i,2)&tr(:,2)>dq(i,1));
    index(ext)=0;
  end
  tr=tr_save(logical(index),:);
  fid=fopen(output,'w');
  if size(tr,2)==2
    for i=1:size(tr,1)
      fprintf(fid,'%12.2f %12.2f\n', tr(i,1), tr(i,2));  
    end
  elseif size(tr,2)==3
    for i=1:size(tr,1)
      fprintf(fid,'%12.2f %12.2f %7.2f\n', tr(i,1), tr(i,2), tr(i,3));
    end
  else
    for i=1:size(tr,1)
      fprintf(fid,['%d %7.2f %12.2f %12.2f %7.2f %7.2f %7.2f %7.2f %7.2f ' ...
		   '%7.2f %3.3f %3.3f %3.3f %12.2f %12.2f %d %d %d\n'],tr(i,:));
    end
  end
  fclose(fid);
else
  display(['No veto file ' veto ' found']);
end
exit
