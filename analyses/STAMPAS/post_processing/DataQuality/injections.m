%%%%
%
% Construct a list of t_start t_end of injections
% One assumes that the following files exist
% injections/radec.txt
% injections/waveforms.txt
% The output is in tmp/injections.txt
%
% Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%
%%%%

addpath ../
radec=load('injections/radec.txt');

% Get the list of waveforms
B=readtext('injections/waveforms.txt', ' ', '%', '', 'textual');

% The waveform name is stored in the first column
nb_wvfs=size(B,1);

% If no waveforms are found, exit
if nb_wvfs==0
  error(['No waveform found in waveforms.txt']);
  return
end

waveform_names=[];
for i=1:nb_wvfs
  waveform_names=[waveform_names; B(i,1)];
end

%% get rid of the "/" for ebbh waveforms
for i=1:nb_wvfs
  if ~isempty(regexp(waveform_names{i},'.*/$'))
    waveform_names{i}=waveform_names{i}(1:end-1);  
  end
end

% Get the alpha values
nb_max_alphas=0;
for i=1:nb_wvfs
  % non-empty elements of row i of B
  B_i = B(i,~cellfun(@isempty,B(i,:)));
  alphas_i = B_i(2:end);
  alphas{i} = cellfun(@str2num, alphas_i);
  nb_max_alphas=max(nb_max_alphas,size(alphas{i},2));
  %alphas{i} = arrayfun(@(x) num2str(x,'%.6f'),double_alphas{i}, ...
  %		       'UniformOutput',false);
end
% Get other parameters 
fmin=zeros(nb_wvfs,1);
fmax=zeros(nb_wvfs,1);
distance=zeros(nb_wvfs,1);
duration=zeros(nb_wvfs,1);
hrss=zeros(nb_wvfs,1);

for i=1:nb_wvfs
    filename=strcat('../../waveforms/', waveform_names{i});
    if strfind(filename,'.mat')
       data_out = load(filename);
       fmin(i) = 10;
       fmax(i) = 2000;
       distance(i) = data_out.dist;
       duration(i) = data_out.duration; 
    else

       [~,s_fmin]=system(['head -2 ' char(filename) ' | awk ''{print $2}'' ']);
       [~,s_fmax]=system(['head -2 ' char(filename) ' | awk ''{print $3}'' ']);
       [~,s_distance]=system(['head -2 ' char(filename) ' | awk ''{print $4}'' ']);
       [~,s_hrss]=system(['head -2 ' char(filename) ' | awk ''{print $5}'' ']);
    
       if strfind(s_fmin,'fmin')
          if size(s_fmin)<5
              error(['Cannot find the fmin information this waveform: ' ...
                   s_fmin]);
          end
          fmin(i)=str2num(s_fmin(5:end));
       end
       if strfind(s_fmax,'fmax')
          if size(s_fmax)<5
             error(['Cannot find the fmax information this waveform: ' ...
                   s_fmax]);
          end
          fmax(i)=str2num(s_fmax(5:end));
       end
       if strfind(s_distance,'dist');
          if size(s_distance)<10
             error(['Cannot find the distance information this waveform: ' ...
                   s_distance]);
          end
          distance(i)=str2num(s_distance(10:end));
       end
       if strfind(s_hrss,'hrss')
          if size(s_hrss)<5
            error(['Cannot find the hrss information this waveform: ' ...
                   s_hrss]);
          end
          hrss(i)=str2num(s_hrss(5:end));
       end
    
       if strfind(s_fmin,'fmin')
          if size(s_fmin)<5
	    error(['Cannot find the fmin information this waveform: ' ...
	       s_fmin]);
          end
          fmin(i)=str2num(s_fmin(5:end));
       end
      if strfind(s_fmax,'fmax')
         if size(s_fmax)<5
            error(['Cannot find the fmax information this waveform: ' ...
	       s_fmax]);
         end
         fmax(i)=str2num(s_fmax(5:end));
      end
      if strfind(s_distance,'dist');
         if size(s_distance)<10
            error(['Cannot find the distance information this waveform: ' ...
	     s_distance]);
         end
         distance(i)=str2num(s_distance(10:end));
      end
      if strfind(s_hrss,'hrss')
         if size(s_hrss)<5
            error(['Cannot find the hrss information this waveform: ' ...
	     s_hrss]);
         end
         hrss(i)=str2num(s_hrss(5:end));
      end
  
      [void,s_duration]=system(['tail -1 ' char(filename) ' | cut -d " " -f 1']);
      tmp=str2num(s_duration);
      duration(i)=tmp(1);
   end
end

parameters.nb_wvfs=nb_wvfs;
parameters.nb_max_alphas=nb_max_alphas;
parameters.alphas=alphas;
parameters.waveform_names=waveform_names;
parameters.fmin=fmin;
parameters.fmax=fmax;
parameters.distance=distance;
parameters.duration=duration;
parameters.hrss=hrss;


times=[];
for i=1:size(radec,1)
  for j=5:5%size(radec,2)
    t_start=radec(i,j);
%    t_end=radec(i,j)+parameters.duration(j-2);
    t_end=radec(i,j)+3;
    times=[times; t_start t_end];
  end
end

timesp=sortrows(times);

if exist('tmp') ~= 1
  system('mkdir tmp');
end
dlmwrite(['tmp/injections.txt'],timesp,'delimiter',' ','precision','%.2f %.2f\n');

return;
