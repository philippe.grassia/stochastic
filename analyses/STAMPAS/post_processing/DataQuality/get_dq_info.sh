# functions
usage(){
    echo 'Arg 1: GPS tim'

    exit
}

# main
[[ $# -ne 1 ]] && usage

GPS_TIME=$1

if [ $GPS_TIME -lt 1000000000 ]; then
    ligolw_dq_query -d -i -q ${GPS_TIME}
else
    ligolw_dq_query_dqsegdb --segment-url https://segments.ligo.org -a ${GPS_TIME} > tmp
    python dq.py -i tmp
    rm tmp
fi
