#!/usr/bin/env bash

## This script loops over all the data quality flags files
## and interesect them with the science segments
## Currently set for H1 L1.
##
## Marie Anne Bizouard (mabizoua@lal.in2p3.fr) 08/11/2014
##########################################################

usage(){
    echo 'Arg 1: run name [S5/S6/O1]'
    echo 'Arg 2: science segment calibration name (C00/C01/C02)'
    echo '       S5: H1/L1:Science'
    echo '       S6: H1/L1:DMT-SCIENCE'
    echo '       O1: H1/L1:DMT-ANALYSIS_READY (C00) H1/L1:DCS-ANALYSIS-READY_C01 (C01) H1/L1:DCS-ANALYSIS-READY_C02 (C02)'
    echo 'Arg3: maximal deadtime [percentage] acceptable for a veto [ 5 ]'
    exit
}

[[ $# -ne 3 ]] && usage

run=$1
cal=$2
upper_percent=$3

if [ ! -d allDQs_Science/${run} ]; then
    mkdir -p allDQs_Science/${run}
    mkdir -p allDQs_Science/${run}/unsafe
fi

for ifo in 'H1' 'L1'; do
    if [ ${run} == S5 ]; then
        science_dq_name=${ifo}\:Science
    elif [ ${run} == S6 ]; then
        science_dq_name=${ifo}\:DMT-SCIENCE
    elif [ ${run} == O1 ]; then
        if [ ${cal} == "C00" ]; then
            science_dq_name=${ifo}\:DMT-ANALYSIS_READY
        elif [ ${cal} == "C01" ]; then
            science_dq_name=${ifo}\:DCS-ANALYSIS_READY_C01
        elif [ ${cal} == "C02" ]; then
            science_dq_name=${ifo}\:DCS-ANALYSIS_READY_C02
        else
            echo 'wrong calibration choice for O1'
            exit 1
        fi
    fi
    
    if [ ! -f science/${run}/${science_dq_name}.txt ]; then
        if [ ! -d science/${run} ]; then
            mkdir -p science/${run}
        fi
        mv allDQs/${run}/${science_dq_name}.txt  science/${run}/.
    fi
    
    DQFiles=`ls allDQs/${run}/${ifo}*`
    count=0
    for dq in $DQFiles
    do
        count=$((${count}+1))
    done
    echo 'Will process' ${count} ' segments for ' $ifo

    # science duration
    sc_duration=`cat science/${run}/${science_dq_name}.txt | awk 'BEGIN{x=0}{x=x+$2-$1}END{print x}'`
    for dq in $DQFiles
    do
        dqRep=`echo $dq | cut -d '/' -f 3`
        segexpr 'intersection('science/${run}/${science_dq_name}.txt','$dq')' > allDQs_Science/${run}/$dqRep
        if [ -f allDQs_Science/${run}/$dqRep ]; then
            dq_duration=`cat allDQs_Science/${run}/$dqRep | awk 'BEGIN{x=0}{x=x+$2-$1}END{print x}'`
            dt=`echo "scale=5; $dq_duration / $sc_duration * 100" | bc -l`
            dt=`printf "%.0f" $(echo "scale=2;$dt" | bc)`
	    
            if [ $dt -gt $upper_percent ]; then
                echo $dq ' has a deadtime (' $dt '%) larger than acceptable limit set at ' $upper_percent '%'
                mv allDQs_Science/${run}/$dqRep allDQs_Science/${run}/unsafe/$dqRep
            fi
        fi
    done
    
    # get rid of unsafe channels
    UNSAFE=`cat unsafe_${ifo}.txt`
    count=0;
    for dq in $UNSAFE; do
#      echo $dq
        if [ -f allDQs_Science/${run}/${dq}_veto.txt ]; then
            mv allDQs_Science/${run}/${dq}_veto.txt allDQs_Science/${run}/unsafe/.
            count=$((${count}+1))
        fi
    done
    echo 'Get rid of ' $count ' veto files for ' ${ifo}
done

