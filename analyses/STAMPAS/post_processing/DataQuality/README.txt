1. Prepare all DQfags and vetoes segment lists
NB: Accessing the database will not work on my laptop (21/03/2016 as dqsegdb software is not installed)

1a. get DQ flags from segments.ligo.org
get_DQ.sh

1b. Add UPV vetoes if any are available

1c. By hand, mv science segments in science/<run>

1c. Intersect with science segments periods 
get_DQ_in_science.sh
all veto list with a large deadtime will be moved to an "unsafe" folder

2. Prepare injections to test how many injections are killed by vetoes
Copy in injections/ from a dag that span the whole run period the files:
waveforms.txt
tmp/radec.txt

matlab -nodisplay -r injections

Currently only adiA injections are considered

3. Generate the table
performance.sh

Several arguments are requested. The table should be generated for no time shift, and with a 
1 day time shift for instance



