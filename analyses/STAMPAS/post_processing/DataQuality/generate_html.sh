#! /usr/bin/env bash

# functions
usage(){
    echo 'Arg 1: run'
    echo 'Arg 2: Minimal duration (in second) of the dq segments [0]'
    echo 'Arg 3: shift (in second) applied to the trigger list [0]'
    echo 'Arg 4: triggers file name'
    exit
}


insert_dq_html(){
# create html dq info row
    IFO=$1
#    sed "/<!-- DQs -->/i\ <tr>" index.html > index_tmp.html 
#    mv index_tmp.html index.html

    sed "/<!-- DQs -->/i\ <tr>  <th class="header"> [NAME] </th> <th class="header"> [TRIGGERS_NB] </th> <th class="header"> [DEADTIME_SEC] </th> <th class="header"> [DEADTIME_PER] </th> <th class="header"> [SEGMENTS_NB] </th> <th class="header"> [USED_SEGMENTS] </th> <th class="header"> [USED_SEGMENTS_DURATION] </th> <th class="header"> [EFF] </th> <th class="header"> [EFF_SNR50] </th> <th class="header"> [EFF_SNR10] </th> <th class="header"> [FRACTION_SEGMENTS] </th> <th class="header"> [EFFDT] </th> <th class="header"> <a href=figures/injection_[NAME].png\>[VETOED_INJ_NB]</a> </th> <th class="header"><a href=figures/[NAME].png\>plot</a></th><th class="header"> [TRIGGERS] </th> </tr>" index_${IFO}.html > index_tmp.html

    mv index_tmp.html index_${IFO}.html
#    sed "/<!-- DQs -->/i\ </tr>" index.html > index_tmp.html
#    mv index_tmp.html index.html
}


# main
[[ $# -lt 3 ]] && usage

RUN=$1
DQ_MIN_DURATION=$2
TIMESHIFT=$3
FILENAME=$4

SEARCH=`echo "$RUN" | tr '[:upper:]' '[:lower:]'`'-allsky'

HOST=`hostname -d | cut -d "." -f 1`
if [[ $HOST == "atlas" ]]; then
    web_repo='WWW/LSC'
    web_server="https://www.atlas.aei.uni-hannover.de"
    location=${web_server}'/~'${USER}'/LSC/'${SEARCH}'/DataQuality/index.html'
elif [[ $HOST == "ligo" ]];then
    web_repo='public_html'
    web_server="https://ldas-jobs.ligo.caltech.edu"
    location=${web_server}'/~'${USER}'/'${SEARCH}'/DataQuality/index.html'
else
    echo "local laptop. Right?"
    if [ -d /home/$USER/public_html ]; then
        web_repo='public_html'
    else
        web_repo=''
    fi
    location='/home/'${USER}'/public_html/'${SEARCH}'/DataQuality/index.html'
fi

echo 'Generating report page for search ' $SEARCH ' ' $RUN

path=/home/$USER/${web_repo}/${SEARCH}/DataQuality

cp template/template.html index.html

# Include extra investigation sections

# Title

sed -e "s|\[RUN\]|${RUN}|g" index.html > index_tmp.html
mv index_tmp.html index.html

sed -e "s|\[USER\]|${USER}|g" index.html > index_tmp.html
mv index_tmp.html index.html

sed -e "s|\[DATE\]|`date`|g" index.html > index_tmp.html
mv index_tmp.html index.html

#insert_command

# Loudest triggers 
for ifo in 'H1' 'L1'; do
    cp index.html index_${ifo}.html
    while read line
    do
        NAME=`echo $line | cut -d " " -f 1 | cut -d "/" -f 3 | cut -d "." -f 1`
        TRIGGERS_NB=`echo $line | cut -d " " -f 2`
        DEADTIME_SEC=`echo $line | cut -d " " -f 3`
        DEADTIME_PER=`echo $line | cut -d " " -f 4`
        SEGMENTS_NB=`echo $line | cut -d " " -f 5`
        USED_SEGMENTS=`echo $line | cut -d " " -f 6`
        USED_SEGMENTS_DURATION=`echo $line | cut -d " " -f 7`
        EFF=`echo $line | cut -d " " -f 8`
        EFF_SNR50=`echo $line | cut -d " " -f 9`
        TRIGGERS50_NB=`echo $line | cut -d " " -f 10`
        EFF_SNR10=`echo $line | cut -d " " -f 11`
        TRIGGERS10_NB=`echo $line | cut -d " " -f 12`
        FRACTION_SEGMENTS=`echo $line | cut -d " " -f 13`
        EFFDT=`echo $line | cut -d " " -f 14`
        VETOED_INJ_NB=`echo $line | cut -d " " -f 15`
        TRIGGERS=`echo $line | cut -d " " -f 16-`
        
#        echo $NAME $TRIGGERS_NB $DEADTIME_SEC
        
        insert_dq_html ${ifo}
        
        sed -e "s|\[NAME\]|${NAME}|g" \
            -e "s|\[TRIGGERS_NB\]|${TRIGGERS_NB}|g" \
            -e "s|\[TRIGGERS50_NB\]|${TRIGGERS50_NB}|g" \
            -e "s|\[TRIGGERS10_NB\]|${TRIGGERS10_NB}|g" \
            -e "s|\[DEADTIME_SEC\]|${DEADTIME_SEC}|g" \
            -e "s|\[DEADTIME_PER\]|${DEADTIME_PER}|g" \
            -e "s|\[SEGMENTS_NB\]|${SEGMENTS_NB}|g" \
            -e "s|\[USED_SEGMENTS\]|${USED_SEGMENTS}|g" \
            -e "s|\[USED_SEGMENTS_DURATION\]|${USED_SEGMENTS_DURATION}|g" \
            -e "s|\[EFF\]|${EFF}|g" \
            -e "s|\[EFF_SNR50\]|${EFF_SNR50}|g" \
            -e "s|\[EFF_SNR10\]|${EFF_SNR10}|g" \
            -e "s|\[FRACTION_SEGMENTS\]|${FRACTION_SEGMENTS}|g" \
            -e "s|\[EFFDT\]|${EFFDT}|g" \
            -e "s|\[VETOED_INJ_NB\]|${VETOED_INJ_NB}|g" \
            -e "s|\[RUN\]|${RUN}|g" \
            -e "s|\[USER\]|${USER}|g" \
            -e "s|\[TIMESHIFT\]|${TIMESHIFT}|g" \
            -e "s|\[DQ_MIN_DURATION\]|${DQ_MIN_DURATION}|g" \
            -e "s|\[TRIGGERS\]|${TRIGGERS}|g" \
            index_${ifo}.html > index_tmp.html
        mv index_tmp.html index_${ifo}.html
        
    done < tmp/${ifo}_dqlist.table
done

# copy files to the final destination

if [ ! -d ${path} ]; then
    mkdir -p ${path}
#else
#    if [ -f ${path}/index.html ]; then
#        old=`tconvert now`
#        mkdir ${path}/${old}
#        mv ${path}/index.html ${path}/${old}/.
#        mv ${path}/figures ${path}/${old}/.
#        mv ${path}/sorttable.* ${path}/${old}/.
#    fi
fi

cp template/top.html index.html
sed -e "s|\[RUN\]|${RUN}|g" index.html > index_tmp.html
mv index_tmp.html index.html

sed -e "s|\[USER\]|${USER}|g" index.html > index_tmp.html
mv index_tmp.html index.html

sed -e "s|\[DATE\]|`date`|g" index.html > index_tmp.html
mv index_tmp.html index.html

ls index*

mv index*.html ${path}/.
cp -R figures ${path}/.
cp template/sorttable.* ${path}/.

if [ ! -d ${path}/${FILENAME} ]; then
    mkdir -p ${path}/${FILENAME}
else
    rm -r ${path}/${FILENAME}/*
fi

cp ${path}/index*.html ${path}/${FILENAME}/.
cp -R ${path}/figures  ${path}/${FILENAME}/.
cp template/sorttable.* ${path}/${FILENAME}/.

echo "result web page available on :" 
echo ${location}

