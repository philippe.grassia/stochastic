ifo='L1';
scfile='allDQs_Science/O1/L1:DCS-ANALYSIS_READY_C02.txt';
dq_duration=0;

dqlists = textread([ifo '_dqlist.txt'], '%s', 'delimiter', '\n');
trfile=['loudest_' ifo '_full.txt'];

tr=load(trfile);
sc=load(scfile);
sc_duration=sum(sc(:,2)-sc(:,1));

tr_nb=size(tr,1);

for k=1:size(dqlists,1)
  trp=[tr zeros(tr_nb,2)];
  dqfile=char(dqlists(k));
  if length(strfind(dqfile,'veto'))>=0
    dq=load(dqfile);
    % dq selection
    dq=dq(find(dq(:,2)-dq(:,1)>dq_duration),:);
    dt=sum(dq(:,2)-dq(:,1))/sc_duration*100;
    if dt<1
      for i=1:size(tr,1)
        ext=find(dq(:,1)<trp(i,2)&dq(:,2)>trp(i,1));
        if size(ext,1)>0
          trp(i,4)=1;
        end
      end
      
      plot(trp(:,1), trp(:,3),'+');
      hold on
      ext=find(trp(:,4)==1);
      plot(trp(ext,1), trp(ext,3),'ro');
      hold off
      title(strrep(dqfile,'_','\_'))
      legend(num2str(dt),'location','NorthWest')
%      pause
    end
  end
end

