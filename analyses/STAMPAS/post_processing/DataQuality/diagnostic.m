function diagnostic(ifo,science_dq_name,dqfile,dq_duration)

%%%%
%
% Compute basic information about one veto file
%
% Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%
% Arguments:
% ifo='L1';
% scfile='science/O1/H1:DCS-ANALYSIS_READY_C02.txt';
% dqfile=''
% dq_duration=0;
%
%%%%

addpath('../')

if ~isnumeric(dq_duration)==1
  dq_duration=str2num(dq_duration);
end

scfile=[science_dq_name '.txt'];
trfile=['tmp/loudest_' ifo '_full.txt'];

tr=load(trfile);
sc=load(scfile);
sc_duration=sum(sc(:,2)-sc(:,1));

fid=fopen(['tmp/' ifo '_dqlist.table'],'w');
 
if exist('tmp/injections.txt','file')==0
  injection_test=0;
  display('Warning: no tmp/injections.txt exists');
else
  injection_test=1;
  inj=load('tmp/injections.txt');
  inj_nb=size(inj,1);
end

display(['Studying ' dqfile]);

dq=load(dqfile);
dqname=strrep(strrep(dqfile,'allDQs_Science/O1',''),'.txt','');
% dq selection
dq=dq(find(dq(:,2)-dq(:,1)>dq_duration),:);

dqinfo.dqname=dqfile;
dqinfo.deadtime_sec=sum(dq(:,2)-dq(:,1));
dqinfo.deadtime_per=dqinfo.deadtime_sec/sc_duration*100;
dqinfo.segments_nb=size(dq,1);
dqinfo.used_segments=0;
dqinfo.fraction_segments=3;
dqinfo.min_usedseg_duration=10000;

% To get rid of large deadtime vetoes 
%  if dqinfo.deadtime_per > 5
%    display(['mv ' char(dqfile) ' unsafe/ ' char(dqfile)])
%  end
  
trp=[];
for i=1:size(dq,1)
  ext=find(tr(:,1)<dq(i,2)&tr(:,2)>dq(i,1));
  if size(ext,1)>0 
    for j=1:4
      if size(find(ext(:)==j),1)>0
        display([dqname ' vetoes n ' num2str(j) ' ' num2str(dqinfo.deadtime_per)]);
      end
    end
    trp=[trp;tr(ext,:)];
    
    dqinfo.used_segments=dqinfo.used_segments+1;
    dqinfo.min_usedseg_duration=min(dqinfo.min_usedseg_duration,dq(i,2)-dq(i,1));
    a=0;
    for j=1:size(ext,1)
      a=a+min(dq(i,2),tr(ext(j),2))-max(dq(i,1),tr(ext(j),1));
    end
    dqinfo.fraction_segments=dqinfo.fraction_segments+a/(dq(i,2)-dq(i,1));
  end
end

trp=unique(trp,'rows');
figure(2)
% display vetoed triggers
plot(tr(:,1),tr(:,3),'+');
if size(trp,1)>0
  hold on
  plot(trp(:,1),trp(:,3),'or');
  hold off
  legend('all triggers','vetoed triggers','location','Best')
else
  legend('all triggers','location','Best')
end
title(strrep(dqname,'_','\_'))
make_png('figures', dqname);

display(['Vetoed triggers nb:' num2str(size(trp,1))])

% How many injections are vetoed?
if injection_test==1
  dqinfo.vetoed_inj_nb=0;
  for i=1:inj_nb
    ext=find(dq(:,1)<inj(i,2)&dq(:,2)>inj(i,1));
    if size(ext,1)>0
      dqinfo.vetoed_inj_nb=dqinfo.vetoed_inj_nb+1;
    end
  end
else
  dqinfo.vetoed_inj_nb=-1;
end


% get unique trp
trt=unique(trp,'rows');
if size(trt,1)>0
  dqinfo.triggers_nb=size(tr,1);
  dqinfo.triggers_vetoed=size(trt,1);
  dqinfo.triggers_snr30_vetoed=size(find(trt(:,3)>30),1);
  dqinfo.triggers_snr50_vetoed=size(find(trt(:,3)>50),1);
  dqinfo.used_segments=dqinfo.used_segments/dqinfo.segments_nb;
  dqinfo.fraction_segments=dqinfo.fraction_segments/dqinfo.segments_nb;
  dqinfo.effdt=dqinfo.triggers_vetoed/dqinfo.triggers_nb/dqinfo.deadtime_per;
  fprintf(fid,'%s %d %.2f %.2f %d %.2e %.2f %d %d %d %.2e %.2f %d\n', ...
          dqinfo.dqname, dqinfo.triggers_nb, dqinfo.deadtime_sec, dqinfo.deadtime_per, ...
          dqinfo.segments_nb, dqinfo.used_segments, ...
          dqinfo.min_usedseg_duration, ...
          dqinfo.triggers_vetoed, dqinfo.triggers_snr30_vetoed, ...
          dqinfo.triggers_snr50_vetoed, dqinfo.fraction_segments, ...
          dqinfo.effdt,dqinfo.vetoed_inj_nb);
  %    if dqinfo.effdt>10
  %      dqinfo
  %    end
else
  display('No coincidence with triggers');
end


fclose(fid);

