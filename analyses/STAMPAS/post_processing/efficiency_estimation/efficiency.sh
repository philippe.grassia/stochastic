#!/usr/bin/env bash
# wrapper for efficiency.m matlab script
# --------------------------------------------------------------
# Syntax:
#   ./efficiency.sh
#     run in an interactive mode
# 
#   ./efficiency DAG [OPTIONS]
#     DAG : the DAG path 
#     OPTIONS : 
#       --DQFlags   : add cut on DQflags
#       --TFveto    : add cut using TFveto
#       --Rveto     : add cut using Rveto
#       --SNRfrac n : add cut on SNRfrac, n must be beetween 0 & 1
#       --SNR n     : add cut on SNR eith thr n. n should be positive
#
# --------------------------------------------------------------
# Purpose:
#   This script generates all efficiency curves
#   It also studies why some injections are missed
#
# --------------------------------------------------------------
# author:                                          M.A. Bizouard 
#                                                  valentin FREY 
# EMail Address:                           mabizoua@lal.in2p3.fr
#                                              frey@lal.in2p3.fr
# version:                                                   1.0
# Comment:
# --------------------------------------------------------------



# ----------------------------------------------------
## user function
# ----------------------------------------------------
usage(){
    echo 'efficiency.sh script : '
    echo '* no argument usage '
    echo '    ./efficiency.sh '
    echo '    then follow the intruction'
    echo ''
    echo '* with argument usage : '
    echo '    ./efficiency.sh DAG [OPTIONS]'
    echo '       DAG     : path of the dag eg Start-1126073342_Stop-1137283217_ID-2'
    echo '       OPTIONS :'
    echo '          --DQFlags    : add cut on DQflags'
    echo '          --TFveto     : add cut using TFveto'
    echo '          --Rveto      : add cut using Rveto'
    echo '          --SNRfrac n  : add cut on SNRfrac, n between 0 and 1'
    echo '          --SNR n      : add cut on NSR with THR n, n should be positive'
    echo ''
}

read_input(){
    # ----------------------------------------------------
    ## dag step  
    # ----------------------------------------------------
    #
    dagStep=false;
    while [[ ${dagStep} == 'false' ]];do
	echo -e '\n\e[0;33mCHOOSE A DAG: \e[0m'
	echo -e '\e[0;33m | \e[0m'
	ls ../../INJ | awk 'BEGIN{i=0}{i++;printf "\033[0;33m | [%2d]\033[0m %s\n",i,$1}'
	dag=($(ls ../../INJ))
        echo -e '\e[0;33m | \e[0m'
	read -p "$(echo -e "\e[0;33m | > \e[0m")" searchPath	
	# check entry
	check=`ls ../../INJ/  | grep ${searchPath}`
        if [[ ${check} == ${searchPath} ]];then
	    dagStep='true';
	elif [[ ${searchPath} -ge 1 ]] && [[ ${searchPath} -le ${#dag[*]} ]];then
            searchPath=${dag[${searchPath}-1]}
	    dagStep='true';
	else
            echo -e "\e[0;33m | \e[1;31m bad input please write the dag path or the dag id\e[0m\n"
        fi
        searchPath='../../INJ/'${searchPath}
    done


    # ----------------------------------------------------
    ## Get search
    # ----------------------------------------------------
    GPS_START=`echo ${searchPath} | sed -r 's/.*Start-([0-9]*).*/\1/'`
    GPS_END=`echo ${searchPath} | sed -r 's/.*Stop-([0-9]*).*/\1/'`
    ID=`echo ${searchPath} | sed -r 's/.*ID-([0-9]*).*/\1/'`

    if [ ${GPS_START} -lt 900000000 ]; then
	RUN="S5"
	SEARCH='s5-allsky'
    elif [ ${GPS_START} -lt 1000000000 ]; then
	RUN="S6"
	SEARCH='s6-allsky'
    elif [ ${GPS_START} -lt 1124004000 ]; then
	RUN="ER7"
	SEARCH='er7-allsky'
    elif [ ${GPS_START} -lt 1126073340 ]; then
	RUN="ER8"
	SEARCH='er8-allsky'
    elif [ ${GPS_START} -lt 1137283217 ];then
	RUN="O1"
	SEARCH='o1-allsky'
    else
	RUN="O2"
	SEARCH="o2-allsky"
    fi



    # --------------------------------------------------------------                  
    ### TFveto CUT STEP                                 
    # --------------------------------------------------------------
    #
    TFvetoStep=false
    while [[ ${TFvetoStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE TFveto Cut (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" doTFveto
	
        # check input
        if [[ ${doTFveto} == '' ]];then
            doTFveto='y';
        fi

        if [[ ${doTFveto} != 'y' ]] && [[ ${doTFveto} != 'n' ]];then
            echo -e "\e[0;33m | \e[1;31m please write y or n\e[0m\n"
        elif [[ ${doTFveto} == 'y' ]];then
            doTFveto=1
            TFvetoStep='true'
        elif [[ ${doTFveto} == 'n' ]];then
            doTFveto=0
            TFvetoStep='true'
        fi
    done


    # --------------------------------------------------------------                  
    ### Rveto CUT STEP                                 
    # --------------------------------------------------------------
    #
    RvetoStep=false
    while [[ ${RvetoStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE Rveto Cut (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" doRveto

        # check input
        if [[ ${doRveto} == '' ]];then
            doRveto='y';
        fi

        if [[ ${doRveto} != 'y' ]] && [[ ${doRveto} != 'n' ]];then
            echo -e "\e[0;33m | \e[1;31m please write y or n\e[0m\n"
        elif [[ ${doRveto} == 'y' ]];then
            doRveto=1
            RvetoStep='true'
        elif [[ ${doRveto} == 'n' ]];then
            doRveto=0
            RvetoStep='true'
        fi
    done


    # ----------------------------------------------------
    ## SNR Cut step  
    # ----------------------------------------------------
    #
    SNRStep=false
    while [[ ${SNRStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE SNR Cut (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" doSNR

        # check input
        if [[ ${doSNR} == '' ]];then
            doSNR='y';
        fi

        if [[ ${doSNR} != 'y' ]] && [[ ${doSNR} != 'n' ]];then
            echo -e "\e[0;33m | \e[1;31m please write y or n\e[0m\n"
        elif [[ ${doSNR} == 'y' ]];then
            doSNR=1
            SNRStep='true'
        elif [[ ${doSNR} == 'n' ]];then
            doSNR=0
            SNRStep='true'
        fi
    done
    
    if [[ ${doSNR} == 1 ]];then
	snrStep=false;
	while [[ ${snrStep} == 'false' ]];do
	    echo -e '\n\e[0;33mCHOOSE A SNR THRESHOLD: \e[0m'
	    echo -e '\e[0;33m | \e[0m'
	    read -p "$(echo -e "\e[0;33m | [30]> \e[0m")" SNRCut
	    
	# check entry
	    check=`ls ../../INJ  | grep ${searchPath}`
	    if [[ ${SNRCut} == '' ]];then
		SNRCut=30;
	    fi
	    
	    if [[ ! ${SNRCut} =~ ^[+-]?[0-9]+\.?[0-9]*$ ]];then
		echo -e "\e[0;33m | \e[1;31m SNRcut should be a number\e[0m\n"
	    else
		snrStep='true'
	    fi
	done
    fi

    # --------------------------------------------------------------                  
    ### SNRfrac CUT STEP                                 
    # --------------------------------------------------------------
    #
    snrfracStep=false
    while [[ ${snrfracStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE SNRfrac Cut (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" doSNRfracCut
	
        # check input
        if [[ ${doSNRfracCut} == '' ]];then
            doSNRfracCut='y';
        fi
	
        if [[ ${doSNRfracCut} != 'y' ]] && [[ ${doSNRfracCut} != 'n' ]];then
            echo -e "\e[0;33m | \e[1;31m please write y or n\e[0m\n"
        elif [[ ${doSNRfracCut} == 'y' ]];then
            doSNRfracCut=1
            snrfracStep='true'
        elif [[ ${doSNRfracCut} == 'n' ]];then
            doSNRfracCut=0
            SNRfracCut=''
            snrfracStep='true'
        fi
    done

    if [[ ${doSNRfracCut} == 1 ]];then
	snrfracStep=false;
	while [[ ${snrfracStep} == 'false' ]];do
	    echo -e '\n\e[0;33mCHOOSE A SNRfrac THRESHOLD (0-1): \e[0m'
	    echo -e '\e[0;33m | \e[0m'
	    read -p "$(echo -e "\e[0;33m | [0.5]> \e[0m")" SNRfracCut
	    
	# check entry
	    if [[ ${SNRfracCut} == '' ]];then
		SNRfracCut=0.5;
	    fi

	    if [[ ! ${SNRfracCut} =~ ^[+-]?[0-9]+\.?[0-9]*$ ]] && [[ $(echo $SNRfracCut | sed -r 's/(.*)\..*/\1/') -ge 1 ]];then
		echo -e "\e[0;33m | \e[1;31m SNRfraccut should be a number between 0 & 1\e[0m\n"
	    else
		snrfracStep='true'
	    fi
	done
    fi


    # --------------------------------------------------------------                  
    ### DQFlags CUT STEP                                 
    # --------------------------------------------------------------
    #
    DQFlagsStep=false
    while [[ ${DQFlagsStep} == 'false' ]];do
        echo -e '\n\e[0;33mUSE DQFlags Cut (y/n): \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [y]> \e[0m")" doDQFlags

        # check input
        if [[ ${doDQFlags} == '' ]];then
            doDQFlags='y';
        fi

        if [[ ${doDQFlags} != 'y' ]] && [[ ${doDQFlags} != 'n' ]];then
            echo -e "\e[0;33m | \e[1;31m please write y or n\e[0m\n"
        elif [[ ${doDQFlags} == 'y' ]];then
            doDQFlags=1
            DQFlagsStep='true'
        elif [[ ${doDQFlags} == 'n' ]];then
            doDQFlags=0
            DQFlagsStep='true'
        fi
    done


    # --------------------------------------------------------------                                                  
    ### USE WEB STEP                                                                                                  
    # --------------------------------------------------------------                                                  
    #                                                                                                                 
    webStep=false;
    while [[ ${webStep} == 'false' ]];do
        echo -e '\n\e[0;33mWEB FOLDER : \e[0m'
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [${SEARCH}/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/efficiency_estimation]>\
 \e[0m")" webfolder
	
        if [[ ${webfolder} == '' ]];then
            webfolder=${SEARCH}'/Start-'${GPS_START}'_Stop-'${GPS_END}'_ID-'${ID}'/efficiency_estimation/'
        fi
        webStep=true;
    done
}

# ----------------------------------------------------
## Input Parser
# ----------------------------------------------------
if [[ $# -eq 0 ]]; then
    read_input;
    GPS_START=`echo ${searchPath} | sed -r 's/.*Start-([0-9]*).*/\1/'`
    GPS_END=`echo ${searchPath} | sed -r 's/.*Stop-([0-9]*).*/\1/'`
    ID=`echo ${searchPath} | sed -r 's/.*ID-([0-9]*).*/\1/'`


elif [[ $# -ge 1 ]]; then
    if [[ $1 == '-h' ]];then
        usage
        exit 0
    fi

    searchPath=`echo $1 | sed -r 's|../../INJ/||'` || exit $?
    searchPath='../../INJ/'${searchPath}
    
   if [[ ! -d ${searchPath} ]];then
        echo -e '\e[1;31m bad DAG path : directory $1 not found\e[0m\n'
        exit 1
    fi
    GPS_START=`echo ${searchPath} | sed -r 's/.*Start-([0-9]*).*/\1/'`
    GPS_END=`echo ${searchPath} | sed -r 's/.*Stop-([0-9]*).*/\1/'`
    ID=`echo ${searchPath} | sed -r 's/.*ID-([0-9]*).*/\1/'`
    doTFveto=0
    doRveto=0
    doDQFlags=0
    SNRfracCut=''
    SNRCut=''

    ### GEt option
    while [ $# -gt 0 ]; do
        case $1 in
            --TFveto )
                doTFveto=1
                ;;

            --SNRfrac )
                shift
                SNRfracCut=$1
                ;;

            --SNR )
                shift
                SNRCut=$1
                ;;

            --Rveto )
                doRveto=1
                ;;

            --DQFlags )
                doDQFlags=1
                ;;

            --webFolder )
                shift
                webfolder=${SEARCH}'/'$1
                ;;
        esac
        shift
    done
else
    usage
fi

# mk logfiles path
if ! [ -d ${searchPath}'/logfiles' ];then
    mkdir -p ${searchPath}'/logfiles'
fi
echo $* `date` >> ${searchPath}'/logfiles/command.txt' || exit $?
 
# ----------------------------------------------------
## Main
# ----------------------------------------------------
matlab -nodisplay <<EOF
  searchPath='${searchPath}';

  doTFveto=${doTFveto};
  doRveto=${doRveto};
  doDQFlags=${doDQFlags};
  SNRfracCut='${SNRfracCut}';
  SNRCut='${SNRCut}';
  webPath = '${webfolder}';  
  efficiency;
EOF

log_file='../../INJ/Start-'${GPS_START}'_Stop-'${GPS_END}'_ID-'${ID}'/logfiles/efficiency.log'
echo 'efficiency.sh run by' $USER 'on' `date` 'with the following parameters' > $log_file
echo 'Arg 1: GPS_START ' ${GPS_START} >> $log_file
echo 'Arg 2: GPS_END ' ${GPS_END} >> $log_file
echo 'Arg 3: ID ' ${ID} >> $log_file
echo 'Arg 4: SNRfracCut ' ${SNRfracCut} >> $log_file
echo 'Arg 5: SNRCut ' ${SNRCut} >> $log_file
echo ' ' >> $log_file
