%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% efficency :
% DESCRIPTION :
% 
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist([searchPath '/figures'])
  system(['mkdir -p ' searchPath '/figures']);
end

%------------------------------------------------------------------
%% Define Cut
%------------------------------------------------------------------
% get the cut using the variable definie
%
cut={'noCut'};

if doTFveto
  cut=[cut, 'TFveto'];
end

if doRveto
  cut=[cut, 'Rveto'];
end

if ~isempty(SNRfracCut)
  cut=[cut, ['SNRfrac:' num2str(SNRfracCut)]];
end

if ~isempty(SNRCut)
  cut=[cut, ['SNR:' num2str(SNRCut)]];
end

if doDQFlags
  cut=[cut, 'DQFlags'];
end

if length(cut)>1
  allcut=strcat(cut,'+');
  allcut=strcat(allcut{2:end});
  cutcomb=combnk(cut(2:end),2);
  cut=[cut cellfun(@(x,y) [x '+' y],cutcomb(:,1),cutcomb(:,2),'UniformOutput',false)'];
  cut=[cut allcut];
end
save([searchPath '/figures/cut.mat'],'cut');

%------------------------------------------------------------------
%% MAIN
%------------------------------------------------------------------
%
efficiencyCalc(searchPath,cut);
efficiencyPlots(searchPath);
diagnostic(searchPath);
missedfound(searchPath);
STAMPASdiagnostic(searchPath);

if sum(cell2mat(strfind(cut,'Rveto')))
  RvetoDiagnostic(searchPath);
end

generate_html(searchPath,'WEB',webPath); 

exit

