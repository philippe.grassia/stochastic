function [] = diagnostic(searchPath)  
% diagnostic :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = diagnostic ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  
  import classes.waveforms.dictionary
  import classes.utils.waitbar
  import classes.utils.logfile

  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % get waveform dictionary
  % create waitbar & logfile
  %

  % get config
  config = readConfig([searchPath '/config_inj.txt']);

  % 
  % Get waveform dictionary 
  %---------------------------------------------------------------------
  % /!\  load first the dictionary because it use java library that
  % clean all singelton class -> instancied waitbar & logtfile will
  % be delete   
  %
  dic = dictionary.parse([searchPath '/waveforms.txt']);

  waitbar(sprintf('diagnostic plot [init]'));

  logfile([searchPath '/logfiles/diganostic.log']);
  logfile('diagnostic begin ...');

  figures_folder = [searchPath '/figures'];

  % 
  % Get frequency notch 
  %---------------------------------------------------------------------
  % frequency notch a usefull to check if injections are cut or if
  % some excess of triggers are found near the frequency notches
  %
  freqNotch = load([searchPath '/tmp/frequencies.txt']);
  idx  = [0,find(diff(freqNotch)>1)];
  freqNotch = [freqNotch(idx(1:end -1)+1);...
               freqNotch(idx(2:end))];


  
  for wvf=dic.headers'
    cc=colormap(jet(numel(wvf.alphas)));
    dataCell=[]; % data cell including all the data for each alphas 
    
    % Get data
    %--------------------------------------------------
    % store all data in a cell
    % each element of the cell correspond to an alphas
    % to plot data we simply use the cellfun function
    %
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.legend,'get data'));

    logfile('************************************************');
    logfile(sprintf('process : %s',wvf.legend));

    for al=1:numel(wvf.alphas)
      injName10=strcat(wvf.file,'_', num2str(wvf.alphas(al),'%0.10f'));
      injName6=strcat(wvf.file,'_', num2str(wvf.alphas(al),'%0.6f'));
      if exist([searchPath '/results/results_merged/results_' ...
                injName6  '_filtered.mat'])
        load([searchPath '/results/results_merged/results_' ...
              injName6 '_filtered.mat']);
        load([searchPath '/results/results_merged/injections_' ...
              injName6 '_merged.mat']);
      elseif exist([searchPath '/results/results_merged/results_' ...
                injName10  '_filtered.mat'])
        load([searchPath '/results/results_merged/results_' ...
              injName10 '_filtered.mat']);
        load([searchPath '/results/results_merged/injections_' ...
              injName10 '_merged.mat']);

      else
        logfile(['results_' injName6 '_filtered no found'...
                            ' probably no ' ...
                            'triggers associated']);        
        data=[];
        injections=[];
      end
      dataCell{al}=data;
      injectionCell{al}=injections;
      clear data injections;
      color{al}=[cc(al,:)];

    end
    close all
    
    distance_max=wvf.distance/sqrt(wvf.alphas(1));
    distance_min=wvf.distance/sqrt(wvf.alphas(end));
    
    hrss_min=wvf.hrss*sqrt(min(wvf.alphas))*1e20;
    hrss_max=wvf.hrss*sqrt(max(wvf.alphas))*1e20;
    
    if strcmp(wvf.xlabel,'distance')
      % adi case (distance)
      ytick=linspace(distance_min,distance_max,length(wvf.alphas));
      ytick2=wvf.distance./sqrt(wvf.alphas);
      yticklabel=cell(1,length(wvf.alphas));
      yticklabel(:) = {''};
      for i=1:length(wvf.alphas)
        a=sprintf('%.1f',ytick2(i));
        yticklabel(1,i)={[a ' Mpc']};
      end
      if distance_min==distance_max
        distance_min=distance_min-10;
        distance_max=distance_max+10;
      end
      clim_interval=[distance_min distance_max];
    else
      % other waveforms (hrss)
      ytick=linspace(hrss_min,hrss_max,length(wvf.alphas));
      ytick2=wvf.hrss.*sqrt(wvf.alphas);
      yticklabel=cell(1,length(wvf.alphas));
      yticklabel(:)={''};
      for i=1:length(wvf.alphas)
        a=sprintf('%.3g',ytick2(i));
        yticklabel(1,i)={a};
      end
      if hrss_min==hrss_max
        hrss_min=hrss_min-10;
        hrss_max=hrss_max+10;
      end
      clim_interval=[hrss_min hrss_max];
    end
    
    % Remove empty cells that would make the next cellfuns fail
    color(cellfun(@isempty,dataCell))=[];
    dataCell(cellfun(@isempty,dataCell))=[];

    color(cellfun(@(x) x.numTriggers == 0,dataCell))=[];
    dataCell(cellfun(@(x) x.numTriggers == 0,dataCell))=[];
    
    %--------------------------------------------------------
    %% MAIN 
    %--------------------------------------------------------
    % make several plot for each waveform made
    %

    if isempty(dataCell);continue;end

    %% SNR vs SNRfrac
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.id,'plot SNR vs SNRfrac'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.id,'plot SNR vs SNRfrac'));

    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    hold all
    f = @(x,c) plot(x.SNR, x.SNRfrac,'+','color',c);
    cellfun(f, dataCell, color, 'UniformOutput', false);  
    
    hold off

    colorbar('YTick',ytick,'YTickLabel',yticklabel);

    set(gca(),'XScale','log');
    set(gca(),'YLim',[0 1]);
    grid
    
    xlabel('SNR');
    ylabel('SNRfrac');
    title(wvf.legend);
    
    make_png([figures_folder '/' wvf.group '/' wvf.id], 'SNR_vs_SNRfrac');
    

    %% SNRfrac vs SNRfractime fraction
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.id,'plot SNRfrac vs SNRfractime'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.id,'plot SNRfrac vs SNRfractime'));

    fig=figure();
    axes('Parent',fig,'CLim',clim_interval);
    hold all
    
    f = @(x,c) plot((x.SNRfracTime-x.tmin)./x.duration, x.SNRfrac,...
                    '+','color',c);
    
    cellfun(f, dataCell, color, 'UniformOutput', false);
    
    hold off
    
    colorbar('YTick',ytick,'YTickLabel',yticklabel);
    ylim([0 1]);
    grid
    
    xlabel('SNRfrac time fraction');
    ylabel('SNRfrac');
    title(wvf.legend);
    
    make_png([figures_folder '/'  wvf.group '/' wvf.id] , 'SNRfractime_vs_SNRfrac');
    

    %% Duration vs SNRfrac
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.id,'plot Duration vs SNRfrac'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.id,'plot Duration vs SNRfrac'));

    fig=figure();
    axes('Parent',fig,'CLim',clim_interval);
    hold all
    
    f = @(x,c) plot(x.duration, x.SNRfrac,'+','color',c);

    [status] = cellfun(f, dataCell, color,'UniformOutput', false);
    
    colorbar('YTick',ytick,'YTickLabel',yticklabel);
    hold off

    ylim([0 1]);
    grid
    ylimit=get(gca, 'ylim');
    line([wvf.duration wvf.duration], ylimit, ...
         'Color', 'r');
    
    xlabel('Duration [s]');
    ylabel('SNRfrac');
    title(wvf.legend);
    
    make_png([figures_folder '/' wvf.group '/' wvf.id], 'Duration_vs_SNRfrac');
    
    
    %% Duration vs SNR  
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.id,'plot Duration vs SNR'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.id,'plot Duration vs SNR'));

    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    hold all

    f = @(x,c) plot(x.duration, x.SNR,'+','Color', c);

    cellfun(f, dataCell, color,'UniformOutput', false);
    
    hold off
    colorbar('YTick',ytick,'YTickLabel',yticklabel);
    
    grid
    ylimit=get(gca, 'ylim');
    line([wvf.duration wvf.duration], ylimit, ...
         'Color', 'r');
    
    xlabel('Duration [s]');
    ylabel('SNR');
    
    title(wvf.legend);
    make_png([figures_folder '/' wvf.group '/' wvf.id],'Duration_vs_SNR');
    

    %% Duration vs Fmin
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.legend,'plot Duration vs Fmin'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.legend,'plot Duration vs Fmin'));

    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    hold all
    
    f = @(x,c) plot(x.duration, x.fmin,'+', ...
                    'Color', c);
    [status] = cellfun(f, dataCell, color,'UniformOutput', false);
    
    
    xlimit=get(gca, 'xlim');
    ylimit=get(gca, 'ylim');
    for fq=1:size(freqNotch,2)
      if freqNotch(1,fq)>=ylimit(1) && freqNotch(2,fq)<=ylimit(2)	 
        h=rectangle('position', [xlimit(1) freqNotch(1,fq)  ...
                            xlimit(2) max(diff(freqNotch(:,fq)),1)], 'FaceColor','g', 'EdgeColor','g' );
        uistack(h,'bottom');
      end
    end
        
    line(xlimit, [wvf.fmin wvf.fmin], 'Color', 'r');
    line([wvf.duration wvf.duration], ylimit,'Color', 'r');    
    
    % trick for the rectangle legend
    line(0, 0,'LineWidth',5 ,'Color','g');

    hold off

    colorbar('YTick',ytick,'YTickLabel',yticklabel);
    grid

    set(gca(),'XLim',xlimit);
    set(gca(),'YLim',ylimit);
    
    xlabel('Duration [s]');
    ylabel('F_{min} [Hz]');
    
    title(wvf.legend);
    make_png([figures_folder '/' wvf.group '/' wvf.id],'Fmin_vs_duration');
    

    %% Duration vs Fmax
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.legend,'plot Duration vs Fmax'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.legend,'plot Duration vs Fmax '));

    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    hold all
    
    f = @(x,c) plot(x.duration, x.fmax,'+', ...
                    'Color', c);
    [status] = cellfun(f, dataCell, color,'UniformOutput', false);
    
    xlimit=get(gca, 'xlim');
    ylimit=get(gca, 'ylim');
    
    for fq=1:size(freqNotch,2)
      if freqNotch(1,fq)>=ylimit(1) && freqNotch(2,fq)<=ylimit(2)	 
        h=rectangle('position', [xlimit(1) freqNotch(1,fq)  ...
                            xlimit(2) max(diff(freqNotch(:,fq)),1)], 'FaceColor','g', 'EdgeColor','g' );
        uistack(h,'bottom');
      end
    end
    
    xlim(xlimit)
    ylim(ylimit)
    
    line(xlimit, [wvf.fmax wvf.fmax], 'Color', 'r');
    line([wvf.duration wvf.duration], ylimit,'Color', 'r');
    
    % trick for the rectangle legend
    line(0, 0,'LineWidth',5 ,'Color','g');
    
    hold off
    colorbar('YTick',ytick,'YTickLabel',yticklabel);    

    grid
    xlabel('Duration [s]');
    ylabel('F_{max} [Hz]');
    
    title(wvf.legend);
    make_png([figures_folder '/' wvf.group '/' wvf.id],'Fmax_vs_duration');
    

    %% Tstart vs duration
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.legend,'plot Tstart vs duration'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.legend,'plot Tstart vs duration'));

    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    hold all
    
    f = @(x,c) plot(x.tmin, x.duration,'+', ...
                    'Color', c);
    cellfun(f, dataCell, color,'UniformOutput', false);
    xlimit=get(gca, 'xlim');
    line(xlimit, [wvf.duration wvf.duration], ...
         'Color', 'r');

    hold off
    colorbar('YTick',ytick,'YTickLabel',yticklabel);    
    grid
    
    xlabel('T_{start} [Hz]');
    ylabel('Duration [s]');
    
    title(wvf.legend);
    make_png([figures_folder '/' wvf.group '/' wvf.id],'Tstart_vs_duration');
    

    %% Tstop vs duration
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.legend,'plot Tstop vs duration'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.legend,'plot Tstop vs duration '));

    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    hold all
    
    f = @(x,c) plot(x.tmax,x.duration,'+', ...
                    'Color', c);
    cellfun(f, dataCell, color,'UniformOutput', false);
    xlimit=get(gca, 'xlim');
    line(xlimit, [wvf.duration wvf.duration],'Color', 'r');

    hold off
    colorbar('YTick',ytick,'YTickLabel',yticklabel);
    
    grid
    
    xlabel('T_{stop} [Hz]');
    ylabel('Duration [s]');
    
    title(wvf.legend);
    make_png([figures_folder '/' wvf.group '/' wvf.id],'Tstop_vs_duration');
    
    
    %% Duration histogram per amplitude
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.legend,'plot Duration histogram'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.legend,'plot Duration histogram'));

    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    hold all
    
    edges=0:1:500;
    f = @(x) histc([x.tmax]-[x.tmin],edges);
    [n] = cellfun(f, dataCell, 'UniformOutput', false);

    for al=1:size(n,2)
      hold on
      s=stairs(edges,n{al});
      set (s, 'Color', color{al});
    end
    ylimit=get(gca, 'ylim');

    line([wvf.duration wvf.duration], ylimit,'Color', 'r');

    hold off
    colorbar('YTick',ytick,'YTickLabel',yticklabel);
    
    set(gca,'XScale','log');
    grid
    
    xlabel('Duration [s]');
    ylabel('Events nb');
    
    title(wvf.legend);
    make_png([figures_folder '/' wvf.group '/' wvf.id],'Duration');
    

    %% SNR histogram per amplitude
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.legend,'plot SNR histogram'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.legend,'plot SNR histogram'));

    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    hold all
    
    snr_max=max(cell2mat(cellfun(@(x) max(x.SNR),dataCell,'UniformOutput',false)));
    
    log_snr_max=log10(snr_max*1.1);
    if log_snr_max > 1
      edges=logspace(1,log_snr_max,100);
    else
      edges=logspace(0.1,log_snr_max,100);        
    end
    
    f = @(x) histc(x.SNR,edges);

    [n] = cellfun(f, dataCell, 'UniformOutput', false);
    
    for al=1:size(n,2)
      hold on
      s=stairs(edges,n{al});
      set (s, 'Color', color{al});
    end
    hold off
    colorbar('YTick',ytick,'YTickLabel',yticklabel);
    
    set(gca,'XScale','log');
    grid
    
    xlabel('SNR');
    ylabel('Events nb');
    
    title(wvf.legend);
    make_png([figures_folder '/' wvf.group '/' wvf.id],'SNR');


    %% Times delay recovery
    %--------------------------------------------------------
    waitbar(sprintf('diagnostic plot [%10s] : %30s', ...
                    wvf.legend,'plot time delay histogram'));
    logfile(sprintf('diagnostic plot [%10s]: %30s',...
                    wvf.legend,'plot time delay histogram'));

    H=getdetector('LHO');
    L=getdetector('LLO');

    td_Y = -0.5:0.01:0.5;
    M = zeros(length(td_Y),length(dataCell));
    Xs=[];
    Td_inj=[];
    Td_t=[];
    for al=1:length(dataCell)
      Y=[];
      for i=1:dataCell{al}.numElements
        ii=dataCell{al}(i).GPSstart<=injectionCell{al}.startGPS+injectionCell{al}.duration&dataCell{al}(i).GPSstop>=injectionCell{al}.startGPS;
        
        if sum(ii) ~= 1
          ii = find(ii==1);
          ii = ii(1);
        end
        inj = injectionCell{al}(ii);
        
        td_inj=caltau(H,L,inj.startGPS,...
                      [inj.ra,inj.decl],...
                      struct('c',299792458));
        if ~config.doLonetrack
          td_t=caltau(H,L,dataCell{al}(i).GPSstart,...
                      [dataCell{al}(i).ra,dataCell{al}(i).dec],...
                      struct('c',299792458));
        else
          td_t = dataCell{al}(i).tau;
        end

        
        Y=[Y;(td_inj-td_t)];
        Td_inj=[Td_inj;td_inj];
        Td_t = [Td_t;td_t];
        Xs = [Xs;al];
      end

      M(:,al) = hist(Y,td_Y);
    end
    X=1:length(dataCell);

    %% hist plot
    fig=figure();
    
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    hold all
    imagesc(X,td_Y,M);
    colorbar
    try
       l=cellfun(@(x) num2str(sqrt(x.alpha)*wvf.hrss,'%g\n') , injectionCell,'UniformOutput',false);
    catch
    end
    xlim([1 length(dataCell)])
    ylim([-2 2]);
    set(gca,'XTick',X);
    try
        set(gca,'XTickLabel',l);
    catch
    end
    set(gca,'XTickLabelRotation',45);
    xlabel('Hrss');
    ylabel('Error on Time Delay (injected - recovered)[s]');
    box
    title(wvf.legend);
    make_png([figures_folder '/' wvf.group '/' wvf.id],'Times_delay');

    %% scatter error plot 
    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);
    scatter(Xs,[Td_inj-Td_t],'+');

    set(gca,'XTick',X);
    set(gca,'XTickLabel',l);
    set(gca,'XTickLabelRotation',45);
    xlabel('Hrss');
    ylabel('Error on Time Delay (injected - recovered [s])');
    box
    title(wvf.legend);
    make_png([figures_folder '/' wvf.group '/' wvf.id],'Times_delay_scatter');
    

    %% scatter time delay inj vs rec plot 
    fig=figure();
    axes1 = axes('Parent',fig,'CLim',clim_interval);

    Al=arrayfun(@(x) injectionCell{x}.alpha,Xs);
    scatter(Td_inj*1000,Td_t*1000,[],wvf.distance./sqrt(Al),'+');

    xlim([-15 15]);
    ylim([-15 15]);

    xlabel('Time delay injected [ms]');
    ylabel('Time Delay recovered [ms]');
    box
    title(wvf.legend);

    c=colorbar('YTick',ytick,'YTickLabel',yticklabel);

    make_png([figures_folder '/' wvf.group '/' wvf.id],'Times_delay_inj_vs_rec');
    
    clear color dataCell injectionCell; 
  end

  logfile('diagnostic plot done without errors');
  waitbar(sprintf('diagnostic plot [done] %30s',' '));
    
  delete(waitbar);
  delete(logfile);

end
