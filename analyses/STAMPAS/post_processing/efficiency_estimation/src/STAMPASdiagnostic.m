function STAMPASdiagnostic(searchPath,varargin)  
% STAMPASdiagnostic : make pipeline diagnostic
% DESCRIPTION :
% 
% SYNTAX :
%   [] = STAMPASdiagnostic (searchPath)
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION :
% DATE : 
%

  import classes.waveforms.dictionary
  import classes.utils.waitbar

  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % get waveform dictionary
  % create waitbar & logfile
  %

  % 
  % Get waveform dictionary 
  %---------------------------------------------------------------------
  % /!\  load first the dictionary because it use java library that
  % clean all singelton class -> instancied waitbar & logtfile will
  % be delete   
  %
  dic = dictionary.parse([searchPath '/waveforms.txt']);

  waitbar(sprintf('STAMPAS diagnostic plot [init]'));

  folder = [searchPath '/figures'];

  % Allocate memory
  injection = cell(dic.nbAlphas,1);
  cursor    = 1;

  % Get data
  %--------------------------------------------------
  % store all injection in a cell
  %
  for wvf=dic.headers'
    waitbar(sprintf('STAMPAS diagnostic [load data] : %30s', ...
                    wvf.legend));

    for al=1:numel(wvf.alphas)
      injName10=strcat(wvf.file,'_', num2str(wvf.alphas(al),'%0.10f'));;
      injName6=strcat(wvf.file,'_', num2str(wvf.alphas(al),'%0.6f'));;
                                           
      if exist([searchPath '/results/results_merged/injections_' ...
                injName10  '_merged.mat'])
        m=matfile([searchPath '/results/results_merged/injections_' ...
                   injName10 '_merged.mat']);

      elseif exist([searchPath '/results/results_merged/injections_' ...
                    injName6  '_merged.mat'])
        m=matfile([searchPath '/results/results_merged/injections_' ...
                   injName6 '_merged.mat']);
      else
        error(sprintf('injection : %s - %f not found', wvf.id,al ))
      end

      injection{cursor}=m.injections;
      cursor=cursor+1;
    end
    
  end

  %---------------------------------------------------------------------
  %% MAIN
  %---------------------------------------------------------------------
  % 

  %% PSI
  %---------------------------------------------------------------------
  % 
  waitbar(sprintf('STAMPAS diagnostic [plot] : %30s', ...
                  'psi distribution'));

  psi = 0:0.1:360;  

  figure
  ax=gca();

  y=cell2mat(cellfun(@(x) x.psi, injection,'UniformOutput',false));
  h=hist(y,psi);

  bar(psi,h,...
      'FaceColor', 'k', ...
      'EdgeColor', 'k', ...
      'BarWidth', 1,...
      'basevalue', 0,...
      'Parent',ax);
  
  ylabel('count #');
  xlabel('psi');
  xlim([0 360]);
  make_png([folder '/global'],'psi')


  %% IOTA
  % ------------------------------------------------------------
  %
  waitbar(sprintf('STAMPAS diagnostic [plot] : %30s', ...
                  'iota distribution'));

  iota = -1:0.1:1;  

  y=cell2mat(cellfun(@(x) cos(x.iota*pi/180), injection,'UniformOutput',false));
  h = hist(y,iota);

  figure
  ax=gca();
  
  bar(iota,h,...
      'FaceColor', 'k', ...
      'EdgeColor', 'k', ...
      'BarWidth', 1,...
      'basevalue', 0,...
      'Parent',ax);
  
  ylabel('count #');
  xlabel('cos(iota)');
  xlim([-1.1 1.1]);

  make_png([folder '/global'],'iota')

  %% RA
  % ------------------------------------------------------------
  %
  waitbar(sprintf('STAMPAS diagnostic [plot] : %30s', ...
                  'RA distribution'));

  ra = 0:0.1:24;  

  figure
  ax=gca();
  y=cell2mat(cellfun(@(x) x.ra ,injection,'UniformOutput',false));
  h = hist(y,ra);

  bar(ra,h,...
      'FaceColor', 'k', ...
      'EdgeColor', 'k', ...
      'BarWidth', 1,...
      'basevalue', 0,...
      'Parent',ax);
  
  ylabel('count #');
  xlabel('ra [h]');

  xlim([0 24]);

  make_png([folder '/global'],'ra')


  %% DEC
  % ------------------------------------------------------------
  %
  waitbar(sprintf('STAMPAS diagnostic [plot] : %30s', ...
                  'Dec distribution'));

  dec = -1:0.01:90;  

  y = cell2mat(cellfun(@(x) cos(x.decl*pi/180+pi/2),injection,'UniformOutput',false));
  h = hist(y,dec);

  figure
  ax=gca();

  bar(dec,h,...
      'FaceColor', 'k', ...
      'EdgeColor', 'k', ...
      'BarWidth', 1,...
      'basevalue', 0,...
      'Parent',ax);
  
  ylabel('count #');
  xlabel('cos(dec+\pi/2) [deg]');

  xlim([-1 1]);

  make_png([folder '/global'],'dec')


  %% TIME DELAY
  % ------------------------------------------------------------
  %
  waitbar(sprintf('STAMPAS diagnostic [plot] : %30s', ...
                  'times delay distribution'));
  
  td = linspace(-0.01,0.01,20);  
  y=zeros(length(injection{1}.ra),1);
  for i=1:length(injection{1}.ra)
    
    y(i,1) = caltau(getdetector('LHO'), getdetector('LLO'),...
                      injection{1}.startGPS(i),...
                      [injection{1}.ra(i),injection{1}.decl(i)],...
                      struct('c',299792458));
  end

  h = hist(y,td);
  
  figure
  ax=gca();
  
  bar(1000*td,h,...
      'FaceColor', 'k', ...
      'EdgeColor', 'k', ...
      'BarWidth', 1,...
      'basevalue', 0,...
      'Parent',ax);

  ylabel('count #');
  xlabel('Times delay [ms]');
  box
  grid minor
  xlim([-15 15]);
  
  make_png([folder '/global'],'times_delay')
  

  %---------------------------------------------------------------------
  %% CLOSE FUNCTION
  %---------------------------------------------------------------------
  % 
  waitbar(sprintf('STAMPAS diagnostic : [done] %30s',''));
  delete(waitbar);

end


