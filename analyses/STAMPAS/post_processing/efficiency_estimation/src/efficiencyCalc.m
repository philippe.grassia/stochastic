function efficiencyCalc(searchPath,cut)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For each injection strenght the efficiency is computed
% It is equal to the ratio of injection present
% in the merged injection files to the total 
% number of injection.
% Results (ASCII format) are saved in the 
% figures_<RUN>_ID<ID_inj> folder
%
% Contact: Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  import classes.waveforms.dictionary
  import classes.utils.waitbar
  import classes.utils.logfile
  
  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % get waveform dictionary
  % create waitbar & logfile
  %

  
  % 
  %% Get waveform dictionary
  %---------------------------------------------------------------------
  %
  % /!\  load first the dictionary because it use java library that
  % clean all singelton class -> instancied waitbar & logtfile will
  % be delete   
  %
  dic = dictionary.parse([searchPath '/waveforms.txt']);


  waitbar(sprintf('efficiency calc [init]'));
  
  logfile([searchPath '/logfiles/efficiencyCalc.log']);

  logfile('efficiency calc begin ...');
  logfile('Compute the efficiency with the follwoing cuts:');
  cellfun(@(x) logfile(sprintf(' - %s',decode(x))) ,cut,'UniformOutput',false);

  % Get the number of injections performed
  nbInjections = get_nb_injections (searchPath);
    
  %
  %---------------------------------------------------------------------
  %% MAIN 
  %---------------------------------------------------------------------
  %
  % loop over the waveform to compute the efficiency as a ratio of
  % recovered injection over the number of injections
  %
  step=0;
  for wvf=dic.headers'
    logfile(['process ' wvf.file ' ' num2str(wvf.hrss) ...
             ' ' num2str(wvf.distance)]); 
    
    eff=zeros(numel(wvf.alphas),2*numel(cut)+3);
    
    for al=1:numel(wvf.alphas)
      injName10=[wvf.file '_' num2str(wvf.alphas(al),'%0.10f')];
      injName6=[wvf.file '_' num2str(wvf.alphas(al),'%0.6f')];
      
      if exist([searchPath '/results/results_merged/results_' injName6 '_filtered.mat'])
        load([searchPath '/results/results_merged/results_' injName6 '_filtered.mat']);
      elseif exist([searchPath '/results/results_merged/results_' injName10 '_filtered.mat'])
        load([searchPath '/results/results_merged/results_' injName10 '_filtered.mat']);
      else
        logfile(['File ' searchPath '/results/results_merged/results_' ...
                 injName6 '_filtered.mat not found, probably no ' ...
                 'injection recovered. Continue']);
        data=[];
      end
      
      eff(al,1) = wvf.distance/sqrt(wvf.alphas(al));
      eff(al,2) = wvf.hrss*sqrt(wvf.alphas(al));
      eff(al,3) = wvf.energy*wvf.alphas(al);

      for c=1:numel(cut)
        label=decode(cut{c});
        if ~isempty(data)
          eff(al,2+2*c:3+2*c)=compute(data,cut{c},nbInjections);
        end        
      end

      waitbar(sprintf('efficiency calc [%3.0f]',100*step/dic.nbAlphas));
      step=step+1;
    end % alphas

    save([searchPath '/figures/eff_' wvf.id '.mat'],'eff','cut');
  end % wvf
  

  %
  %---------------------------------------------------------------------
  %% CLOSE FUNCTION
  %---------------------------------------------------------------------
  %
  % display end message on waitbar & logfile
  % delete instancied singelton object and close
  %
  waitbar(sprintf('efficiency calc [done]'));
  logfile('efficiency calc done without errors');
  
  delete(waitbar);
  delete(logfile);
end     
   


function eff=compute(data,cut,N)

  eff = [ 0 0];

  if data.numTriggers == 0;return;end
  
  [~,~,idx]=decode(cut,data);
  
  eff(1)=sum(idx)/N;
  eff(2)=sqrt(1/N*sum(idx)/N*(1-sum(idx)/N));
end
