function contEff(searchPath,wvfname, nInj, EB)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script for making efficiency estimates for continuum injections.
% To be run after the injection post processing scripts are run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
effDir = [searchPath, '/figures/'];

if ~(exist(effDir) == 7)
  error('Need to run the Injection post processing first');
end


effMats = dir(effDir);
effValues = [];
hValues = [];

for ii  = 3:length(effMats);
  effFile = effMats(ii).name;

  if strcmp(effFile(1:4), 'eff_') & strcmp(effFile(end-3:end), '.mat')
    clear eff;
    load([effDir, '/', effFile]);
    effValues = [effValues,eff(:, 8)];

    % Some reference hrss to check all injections have the same values
    if isempty(hValues)
       href = eff(:, 2);
    end

    if ~isequal(href, eff(:,2))
       error('something wrong. Injections dont have the same hrss amplitude.')
    end
       
    hValues = [hValues, eff(:, 2)];
  end
end


for ii = 1:length(href)
    JJ = href(ii) == hValues;
    marg_eff(ii) = mean(effValues(JJ));
end

% Sort to make sure that amplitudes are in the right order. 
[href, idx] = sort(href);
marg_eff = marg_eff(idx);

%% We will fit the data to a sigmoidal curve. 
close all; 
fsig = fittype( '1/(1+exp(-a*(x-b)))', 'independent', 'x', 'dependent', 'y' );

% Define array for the h values
hs = linspace(href(1), href(end), 500);
dhs = hs(2) - hs(1);

% Deine array for efficiencies at which to calculate error bars
ees = 0.1:0.1:0.9;

% First interpolate the marg curve
feff = fit(log10(href), marg_eff', fsig, 'StartPoint', [4.6, log10(5e-21)]);
mEff = feff(log10(hs));
hmarg_inv = zeros(nInj, length(ees)); 
% repeat for all of them
for ii = 1:nInj
    hold on
    f0  = fit(log10(href), effValues(:, ii), fsig,'StartPoint', [4.6, log10(5e-21)]);
    coffv = coeffvalues(f0);
    hind_inv(ii, :) = 10.^(coffv(2) - (1/coffv(1))*log((1 - ees)./ees));
    
    if sum(hind_inv(ii, :) >= 1e-18) > 0
       keyboard
    end

    clear f0, coffv;
end
error_bar = zeros(length(ees), 2);
quant  = (1-EB)/2;
quant_frac = floor(quant*nInj);

for ii = 1:length(ees)
    nfrac = 0;

    hsort1 = sort(hind_inv(:, ii), 'ascend');
    hsort2 = sort(hind_inv(:, ii), 'descend');
    
    error_bar(ii, 1) = hsort1(quant_frac);
    error_bar(ii, 2) =  hsort2(quant_frac);
    
end

close all;
figure;
xlim([1e-22 1e-18])
ylim([0, 1])
semilogx(hs, mEff, 'k')
legend('Efficiency Curve')
hold on
%eb = errorbar(hmarg_inv, ees, error_bar, 'horizontal')
for ii =1:length(ees)
    idx = (hs <=error_bar(ii, 2) ) & (hs >=error_bar(ii, 1));
    xs = hs(idx);
    ys = ees(ii)*ones(length(xs));
    plot(xs, ys, 'b')
    hold on
end
grid minor
xlabel('h_{rss}')
ylabel('Efficiency')
legend('Efficiency Curve')
title(['Efficiency plot with ' num2str(EB*100) '% confidence levels' ])
xlim([1e-22 1e-18])
ylim([0, 1])
saveas(gcf, [effDir,wvfname,'/eff_marg.png'])
print([effDir,wvfname,'/eff_marg_thumbnail.png'], '-dpng', '-r45')
