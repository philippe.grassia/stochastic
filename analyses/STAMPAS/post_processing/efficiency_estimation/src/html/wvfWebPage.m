function [] = wvfWebPage(wvf,params)  
% wvfWebPage : generate report web page waveform's specific
%
% DESCRIPTION :
%   generate a report page conating waveforms information
% 
%  <h1>STAMPAS report page</h1>
%
%  <h2> Waveform Information </h2>
%  <div class='info'>
%    * some waveforms informations
%  </div>
%
%  <h2> efficiency </h2>
%  <div class='plots'>
%    * efficiency plots for all the cuts
%  </div>
%
%  <h2> Missefound </h2>
%  <div class='plots'>
%    * missefound plots table
%  </div>
% 
%  <h2> Diagnostics Plots </h2>
%  <div class='plots'>
%    * Diagnostics Plots table
%  </div>
% 
%  <h2> Veto Diagnostics </h2>
%  <div class='plots'>
%    * Veto Plots table
%  </div>
% 
% SYNTAX :
%   wvfWebPage (wvf,params)
% 
% INPUT : 
%    wvf :waveforms name
%    params : html params
% 
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%
  import classes.utils.logfile


  % load cut used for efficiency plot
  m=matfile([params.searchPath '/figures/eff_' wvf.id '.mat']);
  cut=m.cut;
  
  % create a new html document
  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');
  doc.setMathJax;
  
  % <h1 title>
  doc.createNode('h1','STAMPAS report page');
  
  % add ariane string for navigation 
  div=doc.createNode('div','Attribute',struct('class','ariane'));
  doc.createNode('a','main',...
                 'Attribute',struct('href','../../'),...
                 'Parent',div);
  div.appendChild(doc.createTextNode(' > '));
  doc.createNode('a',wvf.group,...
                 'Attribute',struct('href','../'), ...
                 'Parent',div);
  div.appendChild(doc.createTextNode([' > ' wvf.id]));

  % <h2> title
  % and <div class='info'> container
  doc.createNode('h2','Waveform Information',...
                 'Attribute',struct('id',wvf.id));
  div=doc.createNode('div',...
                     'Attribute',struct('class','info'));
  % table 1 : information
  t=doc.createNode('table','Parent',div);

  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Name : ','Parent',tr);
  doc.createNode('td',wvf.legend,'Parent',tr);

  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Group : ','Parent',tr);
  doc.createNode('td',wvf.group,'Parent',tr);

  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Fmin : ','Parent',tr);
  doc.createNode('td',num2str(wvf.fmin),'Parent',tr);

  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Fmax : ','Parent',tr);
  doc.createNode('td',num2str(wvf.fmax),'Parent',tr);

  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Duration : ','Parent',tr);
  doc.createNode('td',num2str(wvf.duration),'Parent',tr);

  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','FTmap : ','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','link',...
                 'Attribute',struct('href',['figures/' wvf.id '.png']),...
                 'Parent',td);

  
  % table 2 : efficiency plots
  doc.createNode('h2','Efficiency');
  div=doc.createNode('div',...
                     'Attribute',struct('class','plots'));
  t=doc.createNode('table','Parent',div);
  
  tr=doc.createNode('tr','Parent',t);
  th=doc.createNode('th',...
                    'Attribute',struct('colspan','3'), ...
                    'Parent',tr);
  
  dth = doc.createNode('div','Parent',th);
  doc.createNode('a','hrss',...
                 'Attribute',struct(...
                     'href','#',...
                     'onclick','changeEffPlots("hrss");return false;'),...
                 'Parent',dth);
  sup=doc.createNode('sup','Parent',dth);
  doc.createNode('a','1',...
                 'Attribute',struct('href','#hrss_info'),...
                 'Parent',sup);
  
  dth = doc.createNode('div','Parent',th);
  doc.createNode('a','energy',...
                 'Attribute',struct(...
                     'href','#',...
                     'onclick','changeEffPlots("energy");return false;'),...
                 'Parent',dth);
  sup=doc.createNode('sup','Parent',dth);
  doc.createNode('a','3',...
                 'Attribute',struct('href','#energy_info'),...
                 'Parent',sup);
  
  if strcmp(wvf.xlabel,'distance')
    dth = doc.createNode('div','Parent',th);
    doc.createNode('a', 'distance',...
                   'Attribute',struct(...
                       'href','#',...
                       'onclick','changeEffPlots("distance");return false;'),...
                   'Parent',dth);
    sup=doc.createNode('sup','Parent',dth);
    doc.createNode('a','2',...
                   'Attribute',struct('href',['#distance_info']),...
                   'Parent',sup);
  end
  
  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/eff_' wvf.id '_hrss.png'],tr);
  addplot(doc,['figures/eff_' wvf.id '_legend.png'],tr);
  
  % table 3 : missefound
  doc.createNode('h2','Missefound');
  div=doc.createNode('div','Attribute',struct('class', ...
                                               'plots'));
  t=doc.createNode('table','Parent',div);
  
  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/missedfound.png'],tr);
  addplot(doc,['figures/ratioSNR_vs_distance.png'],tr);
  addplot(doc,['figures/ratioSNR_vs_simuSNR.png'],tr);
  
  
  % table 4 : diagnostic plot
  doc.createNode('h2','Diagnostics Plots');
  div = doc.createNode('div','Attribute',struct('class', ...
                                                 'plots'));
  t=doc.createNode('table','Parent',div);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/SNR_vs_SNRfrac.png'],tr);
  addplot(doc,['figures/SNRfractime_vs_SNRfrac.png'],tr);
  addplot(doc,['figures/Duration_vs_SNRfrac.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Duration_vs_SNR.png'],tr);
  addplot(doc,['figures/Fmin_vs_duration.png'],tr);
  addplot(doc,['figures/Fmax_vs_duration.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Tstart_vs_duration.png'],tr);
  addplot(doc,['figures/Tstop_vs_duration.png'],tr);
  addplot(doc,['figures/Duration.png'],tr);
  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/SNR.png'],tr);
  addplot(doc,['figures/Times_delay.png'],tr);
  addplot(doc,['figures/Times_delay_scatter.png'],tr);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/Times_delay_inj_vs_rec.png'],tr);

  % table 5 : veto
  doc.createNode('h2','Veto Diagnostics');
  div = doc.createNode('div','Attribute',struct('class', ...
                                                 'plots'));
  if ~isempty(cell2mat(strfind(cut,'Rveto')))
    t=doc.createNode('table','Parent',div);
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','RVeto','Attribute',struct('colspan', ...
                                                    '3'),'Parent',tr);

    tr=doc.createNode('tr','Parent',t);
    addplot(doc,['figures/RvetoSNR.png'],tr);
    addplot(doc,['figures/RvetoSNRratioGPSH1.png'],tr);
    addplot(doc,['figures/RvetoSNRratioGPSL1.png'],tr);

    tr=doc.createNode('tr','Parent',t);
    addplot(doc,['figures/RvetoHist.png'],tr);
    addplot(doc,['figures/RvetoHist_Win.png'],tr);
  end

  % footer
  addFooter(doc);

  % make path
  logfile(['make path : ' params.webpath '/' wvf.group '/' wvf.id]);
  if ~exist([params.webpath '/' wvf.group '/' wvf.id])
    system(['mkdir -p ' params.webpath '/' wvf.group '/' wvf.id]);
    system(['mkdir -p ' params.webpath '/' wvf.group '/' wvf.id '/' ...
            'figures']);
  end
  
  system(['cp -r  ./template/style.css ' params.webpath '/' wvf.group '/' ...
          wvf.id '/style.css']);
  system(['cp -r  ./template/main.js ' params.webpath '/' wvf.group '/' ...
          wvf.id '/main.js']);
  system(['cp -r ' params.searchPath '/figures/' wvf.group '/' wvf.id ...
          '/*.png ' params.webpath '/' wvf.group '/' wvf.id '/figures/']);
  system(['cp -r ./template/' wvf.id '.png ' params.webpath '/' wvf.group ...
          '/' wvf.id '/figures/']);

  % write html doc
  doc.write([params.webpath '/' wvf.group '/' wvf.id '/index.html']);

end
