function addplot(doc,name,parent)  
% addplot : add plot to a html document
%
% DESCRIPTION :
%   add plot to a table from a html document. Use the thumbnail image for
%   the table display and make a link to the real size image
%   
%   <td>
%     <a href='$name$'>
%       <img src=$name$_thumbnail/>
%     </a>
%   </td>
%
% SYNTAX :
%   [] = addplot (doc,name,parent)
% 
% INPUT : 
%   doc : html document
%   name : image name
%   parent : <tr> parent markup 
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%
  
  td=doc.createNode('td','Parent',parent);
  a=doc.createNode('a',...
                   'Attribute',struct('href',name),...
                   'Parent',td);
  doc.createNode('img',...
                 'Attribute',struct(...
                     'src',regexprep(name,'.png','_thumbnail.png')),...
                 'Parent',a);
end

