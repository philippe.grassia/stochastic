function generate_html(SearchPath,varargin)  
% generate_html : generate html page to display injection search
% results
%
% DESCRIPTION :
%    generated main page that summarize global information
%    generated one page by group wavefoms  
%    generated one page by  wavefoms  
%    generated one page for STAMPAS diagnostic
% 
% SYNTAX :
%   generate_html (searchPAth)
% 
% INPUT : 
%   searchPath : path to the search folder
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Aug 2016
%

  import classes.utils.waitbar
  import classes.utils.logfile

  % If continuum injection run then, we will load the temporarily generated dictionary instead
  dict_ci_path = [SearchPath, '/ContWaveforms/dic_ci.yml'];
  if exist(dict_ci_path)==2
     dictionary = classes.waveforms.dictionary(dict_ci_path);
  else
    import classes.waveforms.dictionary  
  end
  %---------------------------------------------------------------------
  %% INPUT PARSER                                                                                                     
  %---------------------------------------------------------------------
  % 
  % parse input
  %
  p=inputParser;
  addRequired(p,'SearchPath');
  if verLessThan('matlab','8.2')
    addParamValue(p,'WEB','');
  else
    addParameter(p,'WEB','');
  end
  parse(p,SearchPath,varargin{:});

  searchPath = p.Results.SearchPath;
  web        = p.Results.WEB;



  %---------------------------------------------------------------------
  %% INIT
  %---------------------------------------------------------------------
  % 
  % get params for tht html generations
  % make path 
  % copy css and config files
  %
  params = HTMLparams(searchPath);

  % start waitbar
  waitbar(sprintf('generate html [init]'));

  logfile([searchPath '/logfiles/generate_html.log']);
  logfile('generate HTML begin ...');

  if ~isempty(web)
    params.webpath = [params.host '/' web];
  end
    
  %
  %=====================================================================
  %% MAIN WEB REPORT PAGE 
  %=====================================================================
  %
  % main page that symmarize all wavefoms informations, efficiency
  % and search parameters.
  mainWebPage(params);

  %
  %=====================================================================
  %% GROUP WEB REPORT PAGE
  %=====================================================================
  %
  % create a new web page
  % create a table of plot for each waveform
  % we use addplot function define at the end of this function to
  % add plot on a specific table
  %
  for grp=params.dic.group
    grpWebPage(grp{:},params);
    wvf = params.dic.headers(grp{:});

    %
    %=====================================================================
    %% WAVEFORM WEB REPORT PAGE
    %=====================================================================
    %
    % create a new web page
    % create a table of plot for each waveform
    % we use addplot function define at the end of this function to
    % add plot on a specific table
    %
    for w=wvf'
      waitbar(sprintf('generate html [%3.0f]',100*find(strcmp(w.id,params.dic.id))/params.dic.nbWvf));
      logfile(['process waveform : ' w.legend]);

      wvfWebPage(w,params);
    end
  end

  %=====================================================================
  %% STAMPAS DIAGNOSTIC REPORT
  %=====================================================================
  %
  % create a new web page
  % create a table of plot that show general plot showing that
  % every thing went good
  % we use addplot function define at the end of this function to
  % add plot on a specific table
  %
  diagnosticWebPage(params);


  %
  %---------------------------------------------------------------------
  %% CLOSE FUNCTION
  %---------------------------------------------------------------------
  %
  % write the HTML doc into a index.html page  
  % move all figure in the web folder
  %
  logfile('generate_html done without errors');
  waitbar(sprintf('generate html [done]'));
  
  delete(logfile);
  delete(waitbar);

end

