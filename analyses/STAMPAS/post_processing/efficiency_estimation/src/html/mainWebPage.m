function mainWebPage(params)  
% mainWebPage : generate html page containing main information 
%
% DESCRIPTION :
%   generate html document.the web page containing global
%   information for an injection search
%
%   <h1> STAMPAS report page </h1>
%
%   <h2> dag information </h2>
%   <div class='info'> 
%     <table>
%       * global search information like tstart, tend, run etc...
%     </table>
%   </div>
%
%  <h2>dictionary</h2>
%    * waveforms used for this search
%    * SEE addDic for more information
% 
%  <h2>Estimated distance @ hrss 50%</h2>
%    * efficiency table 
%    * SEE addEfficiencyTable for more information
%
%  <footer>
%    * additional information concerning energy computation or
%    convention used.
%    * SEE addFooter for more information
%  </footer>
%
% SYNTAX :
%   [] = mainWebPage (params)
% 
% INPUT : 
%   params : HTML params
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1,0
% DATE : Aug 2016
%

  % ------------------------------------------------------------
  %% INIT 
  % ------------------------------------------------------------
  %  create web page & add style and js script 
  %
  if ~exist(params.webpath)
    system(['mkdir -p ' params.webpath]);
    system(['mkdir -p ' params.webpath '/info']);
  end


  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');
  doc.setMathJax; % used for latex equation

  % ------------------------------------------------------------
  %% MAIN
  % ------------------------------------------------------------
  % create the html page
  %

  % <h1> main title
  doc.createNode('h1','STAMPAS report page');
  
  % <h2> title
  doc.createNode('h2','dag information');
  
  % create <div class='info'> container and a <table> markup
  div=doc.createNode('div','Attribute',struct('class','info'));
  t=doc.createNode('table','Parent',div);
  
  % search info 
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Search : ','Parent',tr);
  doc.createNode('td',params.search,'Parent',tr);
  
  % run info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Run : ','Parent',tr);
  doc.createNode('td',params.run,'Parent',tr);
  
  % type info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Type : ','Parent',tr);
  doc.createNode('td',params.type,'Parent',tr);

  % GPS start info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','GPS start : ','Parent',tr);
  doc.createNode('td',num2str(params.start),'Parent',tr);

  % GPS stop info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','GPS stop : ','Parent',tr);
  doc.createNode('td',num2str(params.stop),'Parent',tr);

  % id info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Id : ','Parent',tr);
  doc.createNode('td',num2str(params.id),'Parent',tr);

  % inj info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Number of Injections : ','Parent',tr);
  doc.createNode('td',num2str(params.config.NumberOfInjections),'Parent', ...
                 tr);

  % config file info
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Config file : ','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','see file','Attribute',...
                 struct('href',['info/config.txt']),...
                 'Parent',td);

  % STAMPAS Diagnostic
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','STAMPAS diagnostic ','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a','link','Attribute',struct('href', ...
                                               'diagnostic'),'Parent',td);

  % cut info
  scut=params.cut(cellfun(@isempty,strfind(params.cut,'+')));
  if length(scut) > 1
    tr=doc.createNode('tr','Parent',t);
    doc.createNode('th','Cut apply : ','Parent',tr);
    doc.createNode('td',decode(scut{2}),'Parent',tr,'Attribute', ...
                   struct('colspan','2'));
    for c=scut(3:end)
      tr=doc.createNode('tr','Parent',t);
      doc.createNode('th','Parent',tr);
      doc.createNode('td',decode(c{:}),'Parent',tr,'Attribute', ...
                     struct('colspan','2'));
    end
  end

  % add list of waveforms
  addDic(doc,params.dic);

  % add efficiency table 
  addEfficiencyTable(doc,params.dic,params);
  writeEfficiencyLatexTable(params.searchPath,...
                            [params.webpath '/STAMPAS_efficiency.tex'],...
                            params.dic);
  
  % add footer 
  addFooter(doc);
  
  % --------------------------------------------------------------------
  %%  Write Web Page
  % --------------------------------------------------------------------
  %
  
  % move file
  system(['cp -r ' params.searchPath '/config_inj.txt ' ...
          params.webpath '/info/config.txt']);
  system(['cp -r  ./template/style.css ' params.webpath '/style.css']);
  
  % write HTML doc
  doc.write([params.webpath '/index.html'])
  
end
