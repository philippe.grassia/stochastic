function addDic(doc,dic)  
% addDic : use the dic to create the ToC.
%
% DESCRIPTION :
%   the ToC is divide in two part
%     - physical waveform
%     - ad-hoc waveform
%   column represent the group name
%   line reprensent to the waveform corresponding to the group name
%
%  <h2>dictionay</h2>
%  <div class='dic'>
%    <table>
%      <tr>
%        <th colspan='$nbGroup$'>Physical wavefoms</th>
%      </tr>
%      <tr>
%        <th> * one headers by group name * 
%          <a href=''> *link to the group information * </a> 
%        </th>
%      </tr>
%
%      <tr>
%        <td> * one headers by waveforms correponding to the groups *
%          <a href=''> *link to the waveform information * </a> 
%        </td>
%      </tr>
%    </table>
%
%    <table>
%      <tr>
%        <th colspan='$nbGroup$'>Ad-hoc wavefoms</th>
%      </tr>
%      <tr>
%        <th> * one headers by group name * 
%          <a href=''> *link to the group information * </a> 
%        </th>
%      </tr>
%
%      <tr>
%        <td> * one headers by waveforms correponding to the groups *
%          <a href=''> *link to the waveform information * </a> 
%        </td>
%      </tr>
%    </table>
%  </div>
%
% SYNTAX :
%   addDic (doc,dic)
% 
% INPUT : 
%   doc : html doc
%   dic : dictionary
% 
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%

  % ----------------------------------------------------------------- 
  %% INIT
  % -----------------------------------------------------------------
  % 

  % get physical wavefoms & adhoc waveforms
  wvfs = dic.headers;
  phys_wvfs  = wvfs(arrayfun(@(h) strcmp(h.xlabel,'distance'),wvfs));
  adhoc_wvfs = wvfs(arrayfun(@(h) strcmp(h.xlabel,'hrss'),wvfs));
  
  % get physical & adhoc group
  phys_grp  = unique({phys_wvfs.group});
  adhoc_grp = unique({adhoc_wvfs.group});


  % ----------------------------------------------------------------- 
  %% MAIN
  % -----------------------------------------------------------------
  % 

  % add toc 
  doc.createNode('h2','dictionary','Parent',doc.body);
  div = doc.createNode('div',...
                       'Attribute',struct('class','dic'), ...
                       'Parent',doc.body);


  % deal with physical waveforms first
  % -----------------------------------------------------------------
  t  = doc.createNode('table','Parent',div);
  tr = doc.createNode('tr','Parent',t);
  doc.createNode('th','Physical waveform',...
                 'Attribute',struct(...
                     'class','tableTitle',...
                     'colspan',num2str(numel(phys_wvfs))),...
                 'Parent',t);
  
  tr = doc.createNode('tr','Parent',t);
  if ~isempty(phys_grp) % Check to see we have physical waveforms
    for w=phys_grp
      th = doc.createNode('th','Parent',tr);
      doc.createNode('a',w{:},...
                     'Attribute',struct('href',w{:}), ...
                     'Parent',th);
    end
  end
  currentWvfNb=1;
  while true
    tr = doc.createNode('tr','Parent',t);
    emptyWvf=0;
    if ~isempty(phys_grp)
      for grp=phys_grp
	gwvf = phys_wvfs(strcmp({phys_wvfs.group},grp));
	if currentWvfNb<=numel(gwvf)
          td = doc.createNode('td','Parent',t);
          doc.createNode('a',gwvf(currentWvfNb).id,...
			 'Attribute',struct('href', ...
                                            [gwvf(currentWvfNb).group ...
						 '/' gwvf(currentWvfNb).id]), ...
			 'Parent',td);
	else
          doc.createNode('td','','Parent',t);
          emptyWvf=emptyWvf+1;
	end
      end
    end
    if emptyWvf == numel(phys_grp)
      break
    else
      currentWvfNb=currentWvfNb+1;
    end
  end
  
  % deal with adhoc waveforms
  % -----------------------------------------------------------------
  t   = doc.createNode('table','Parent',div);
  tr = doc.createNode('tr','Parent',t);
  doc.createNode('th','Ad-hoc waveform',...
                 'Attribute',struct(...
                     'class','tableTitle',...
                     'colspan',num2str(numel(phys_wvfs))),...
                 'Parent',t);
  tr = doc.createNode('tr','Parent',t);
  if ~isempty(adhoc_grp) %% Check to see if we have adhoc waveforms
    for w=adhoc_grp
      th = doc.createNode('th','Parent',tr);
      doc.createNode('a',w{:},'Attribute',struct('href',w{:}), ...
                     'Parent',th);
    end
  end
  currentWvfNb=1;
  while true
    tr = doc.createNode('tr','Parent',t);
    emptyWvf=0;
    if ~isempty(adhoc_grp)
      for grp=adhoc_grp
        gwvf = adhoc_wvfs(strcmp({adhoc_wvfs.group},grp));
        if currentWvfNb<=numel(gwvf)
          td = doc.createNode('td','Parent',t);
          doc.createNode('a',gwvf(currentWvfNb).id,...
                         'Attribute',struct('href', ...
                                            [gwvf(currentWvfNb).group ...
                              '/' gwvf(currentWvfNb).id]),'Parent', ...
                         td);
        else
          doc.createNode('td','','Parent',t);
          emptyWvf=emptyWvf+1;
        end
      end
    end
    if emptyWvf == numel(adhoc_grp)
      break
    else
      currentWvfNb=currentWvfNb+1;
    end
  end
end
