function addEfficiencyTable(doc,dic,params)  
% addEfficiencyTable : generate html table for efficiency
% DESCRIPTION :
%   html table summarize information concerning wavefoms and
%   efficiency
%   each line corespond to a wavefoms
%
%  <h2>Estimated distance @ hrss 50%</h2>
%  <div class='eff'>
%    <table>
%      <tr>
%        <th rowspan='2'>Waveform</th>
%        <th colspan='3'>dictionary</th>
%        <th colspan='3'>@ 50%</th>
%      </tr>
%      <tr>
%        <th> hrss <sup><a href='#hrss_info'>1</a></sup> </th>
%        <th>distance <sup><a href='#distance_info'>2</a></sup> </th>
%        <th>energy <sup><a href='#energy_info'>3</a></sup> </th>
%        <th> hrss <sup><a href='#hrss_info'>1</a></sup> </th>
%        <th>distance <sup><a href='#distance_info'>2</a></sup> </th>
%        <th>energy <sup><a href='#energy_info'>3</a></sup> </th>
%      </tr>
%
%      <tr> *one line for each wavefoms* 
%        <td> *one cell for each wavefom informations * </td>
%      </tr>
%    </table>
%    <small>
%      <a href='link to the .tex table'>download .tex table</a>
%    </small>
%  </div>
%
% SYNTAX :
%   addEfficiencyTable (doc,dic,params)
% 
% INPUT : 
%    doc : html document
%    dic : dictionary
%    params : HTML params
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%

  % <h2> title <h2>
  doc.createNode('h2','Estimated distance @ hrss 50%');

  % <div class = 'eff'> container
  div=doc.createNode('div','Attribute',struct('class','eff'));

  % <table><tr> markup
  t=doc.createNode('table','Parent',div);
  tr=doc.createNode('tr','Parent',t);

  % define <th> table's headers 
  doc.createNode('th','Waveform',...
                 'Attribute',struct('rowspan','2'),'Parent',tr);
  doc.createNode('th','dictionary',...
                 'Attribute',struct('colspan','3'),...
                 'Parent',tr);

  doc.createNode('th','@ 50%',...
                 'Attribute',struct('colspan','3'), ...
                 'Parent',tr);

  tr=doc.createNode('tr','Parent',t);
  td=doc.createNode('th','Parent',tr);
  td.appendChild(doc.createTextNode('hrss '));
  sup=doc.createNode('sup','Parent',td);
  doc.createNode('a','1',...
                 'Attribute',struct('href','#hrss_info'), ...
                 'Parent',sup);

  td=doc.createNode('th','Parent',tr);
  td.appendChild(doc.createTextNode('distance '));
  sup=doc.createNode('sup','Parent',td);
  doc.createNode('a','2','Attribute',struct('href',['#' ...
                      'distance_info']),'Parent',sup);

  td=doc.createNode('th','Parent',tr);
  td.appendChild(doc.createTextNode('energy '));
  sup=doc.createNode('sup','Parent',td);
  doc.createNode('a','3',...
                 'Attribute',struct('href','#energy_info'),...
                 'Parent',sup);
  
  td=doc.createNode('th','Parent',tr);
  td.appendChild(doc.createTextNode('hrss '));
  sup=doc.createNode('sup','Parent',td);
  doc.createNode('a','1',...
                 'Attribute',struct('href','#hrss_info'), ...
                 'Parent',sup);

  td=doc.createNode('th','Parent',tr);
  td.appendChild(doc.createTextNode('distance '));
  sup=doc.createNode('sup','Parent',td);
  doc.createNode('a','2','Attribute',struct('href',['#' ...
                      'distance_info']),'Parent',sup);

  td=doc.createNode('th','Parent',tr);
  td.appendChild(doc.createTextNode('energy '));
  sup=doc.createNode('sup','Parent',td);
  doc.createNode('a','3',...
                 'Attribute',struct('href','#energy_info'),...
                 'Parent',sup);
  
  % fill he table with the wvf effciency 
  if isa(dic,'classes.waveforms.dictionary')
    wvf = dic.headers;
  else
    wvf = dic;
  end

  for w=wvf'
    [hrss, distance, energy] = estimateDistance(params.searchPath,w);

    tr=doc.createNode('tr','Parent',t);
    doc.createNode('td',w.legend,'Parent',tr);
    doc.createNode('td',num2str(w.hrss),'Parent',tr);
    if strcmp(w.xlabel,'distance')
      doc.createNode('td',num2str(w.distance),'Parent',tr);
    else
      doc.createNode('td','-','Parent',tr);
    end
    doc.createNode('td',num2str(w.energy),'Parent',tr);
    doc.createNode('td',num2str(hrss),'Parent',tr);
    doc.createNode('td',num2str(distance),'Parent',tr);
    doc.createNode('td',num2str(energy),'Parent',tr);
  end


  small=doc.createNode('small','Parent',div);
  doc.createNode('a','download .tex file',...
                 'Attribute',struct('href','STAMPAS_efficiency.tex'),...
                 'Parent',small);
  
end