function writeEfficiencyLatexTable(searchPath,file,dic)  
% writeEfficiencyLatexTable : create a latex table
% DESCRIPTION :
%   convert the efficiency table into latex table. Usefull for
%   paper 
%   this is a stand alone fucntion
%
% SYNTAX :
%   writeEfficiencyLatexTable (searchPath,file,dic)
% 
% INPUT : 
%    searchaPath : path to the injection search
%    file : .tex file ame 
%    dic : dictionary
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%

  fid = fopen(file,'w');
  
  fprintf(fid,'\\begin{tabular}{|l|lll|lll|}\n');
  fprintf(fid,'\\hline\n');
  fprintf(fid,['Waveform & '...
               '\\multicolumn{3}{c|}{dictionary} & ' ...
               '\\multicolumn{3}{c|}{@ 50\\%%} \\\\ \n']);
  fprintf(fid,['\\cline{2-7} & '...
               'hrss & distance [Mpc] & energy & ' ...
               'hrss & distance [Mpc] & energy \\\\ \n']);
  fprintf(fid,'\\hline\n');

  if isa(dic,'classes.waveforms.dictionary')
    wvf = dic.headers;
  else
    wvf = dic;
  end

  for w=wvf'
    [hrss, distance, energy] = estimateDistance(searchPath,w);
    
    fprintf(fid,'%s & ',w.legend);
    fprintf(fid,'%g & ',w.hrss);
    if strcmp(w.xlabel,'distance')
      fprintf(fid,'%f & ',w.distance);
    else
      fprintf(fid,' - & ');
    end
    fprintf(fid,'%g & ',w.energy);
    fprintf(fid,'%g & ',hrss);
    fprintf(fid,'%f & ',distance);
    fprintf(fid,'%g \\\\ \n',energy);
  end
  
  fprintf(fid,'\\hline\n');
  fprintf(fid,'\\end{tabular}\n');
  fclose(fid);
  
end

