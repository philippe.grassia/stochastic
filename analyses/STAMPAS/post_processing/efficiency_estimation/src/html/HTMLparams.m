function [params] = HTMLparams(searchPath)  
% HTMLparams : define all parameters used for html generation
% DESCRIPTION :
%   using the searchPath get all information from the search and
%   define parameterd needed to used html generation functions
%
% SYNTAX :
%   [] = HTMLparams (searchPath)
% 
% INPUT : 
%   searchPath : dag path   
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%
  import classes.waveforms.dictionary

  % get cut used for the post_processing
  params = load([searchPath '/figures/cut.mat']);

  % get search parameters
  params.searchPath = searchPath;
  params.start   = str2num(regexprep(searchPath, '.*Start-([0-9]*)_.*','$1'));
  params.stop    = str2num(regexprep(searchPath, '.*Stop-([0-9]*)_.*','$1'));
  params.id      = str2num(regexprep(searchPath, '.*ID-([0-9]*).*','$1'));
  params.type    = regexprep(searchPath,'.*/(BKG|INJ)/.*', '$1');
  params.run     = get_run_from_GPStimes(params.start, params.stop);
  params.search  = [lower(params.run) '-allsky'];
  
  % get hostname
  [~,h] = system('hostname -d');
  user=getenv('USER');
  h=h(1:end-1); % remove last char
  if (strcmpi(h,'ligo.caltech.edu') | strcmpi(h,'ligo-wa.caltech.edu') ...
      | strcmpi(h,'ligo-la.caltech.edu'))
    params.host=['/home/' user '/public_html'];
  elseif strcmpi(h,'atlas.local')
    params.host=['/home/' user '/WWW/LSC'];
  end

  % get web path
  params.webpath = sprintf('%s/%s/Start-%d_Stop-%d_ID-%d/efficiency_estimation/',...
                           params.host,params.search,params.start,params.stop,params.id);
  
  % get the dictionary
  params.dic = dictionary.parse([searchPath '/waveforms.txt']);
  
  % get config file
  params.config  = readConfig([searchPath '/config_' lower(params.type) '.txt']);
  
end

