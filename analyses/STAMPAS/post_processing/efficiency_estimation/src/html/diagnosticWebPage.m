function diagnosticWebPage(params)  
% diagnosticWebPage : generate html document to display STAMPAS
% diagnostic plot
% DESCRIPTION :
%   STAMPAS disagnostic check the pipeline status 
%   check that every random number are really random
%   check there is no bias in the pipeline 
%   etc ... 
%  
%  <h1>STAMPAS report page<h1>
%
%  <h2>IOTA & PSI Distribution</h2>
%  <div class='plots'>
%    <table>
%      <tr>
%        <td>
%          <img src='* IOTA distribution image *'/>
%        </td>
%        <td>
%          <img src='* PSI distribution image *'/>
%        </td>
%      </tr>
%    </table>
%  </div>
%
%  <h2>RA & DEC Distribution</h2>
%  <div class='plots'>
%    <table>
%      <tr>
%        <td>
%          <img src='* RA distribution image *'/>
%        </td>
%        <td>
%          <img src='* DEC distribution image *'/>
%        </td>
%      </tr>
%    </table>
%  </div>
%
% SYNTAX :
%   diagnosticWebPage (params)
% 
% INPUT : 
%    params : efficiency HTML params 
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%

  import classes.utils.logfile

  % create a new html document
  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');
  doc.createNode('h1','STAMPAS report page');

  % add ariane string for navigation
  d=doc.createNode('div','','Attribute',struct('class','ariane'));
  doc.createNode('a','main','Attribute',struct('href','../'), ...
                 'Parent',d);
  d.appendChild(doc.createTextNode([' > diagnostic']));
  
  %% Table of plots
  % ------------------------------------------------------------------
  % 

  % IOTA & PSI
  doc.createNode('h2','IOTA & PSI Distribution');
  wd=doc.createNode('div','Attribute',struct('class','plots'));
  t=doc.createNode('table','Parent',wd);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/iota.png'],tr);
  addplot(doc,['figures/psi.png'],tr);

  % RA & DEC
  doc.createNode('h2','RA & DEC Distribution');
  wd=doc.createNode('div','Attribute',struct('class','plots'));
  t=doc.createNode('table','Parent',wd);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/ra.png'],tr);
  addplot(doc,['figures/dec.png'],tr);

  % Time delay
  doc.createNode('h2','Time Delay Distribution');
  wd=doc.createNode('div','Attribute',struct('class','plots'));
  t=doc.createNode('table','Parent',wd);

  tr=doc.createNode('tr','Parent',t);
  addplot(doc,['figures/times_delay.png'],tr);

  
  %% Write Web Page
  % ------------------------------------------------------------------
  % 

  % make path
  logfile(['make path : ' params.webpath '/diagnostic']);
  if ~exist([params.webpath '/diagnostic'])
    system(['mkdir -p ' params.webpath '/diagnostic']);
    system(['mkdir -p ' params.webpath '/diagnostic/figures']);
  end
  
  system(['cp -r  ./template/style.css ' params.webpath '/diagnostic/' ...
                      'style.css']);
  system(['cp -r ' params.searchPath '/figures/global/*.png ' params.webpath ...
          '/diagnostic/figures/']);

  % write HTML doc
  doc.write([params.webpath '/diagnostic/index.html']);
end

