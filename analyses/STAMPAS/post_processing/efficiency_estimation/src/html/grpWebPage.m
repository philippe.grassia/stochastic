function grpWebPage(grp,params)  
% grpWebPage : generate group web page
% DESCRIPTION :
%   generate html document containing information group specific
%
%  <h1> STAMPAS report page </h1>
%  <h2>Group Information</h2>
%  <div class='info'>
%    * group information like name, waveforms, etc ...  * 
%  </div>
%
%  <h2>Estimated distance @ hrss 50%</h2>
%  <div class='eff'>
%    <table>
%      * efficiency table 
%      * SEE addEfficicencyTable for more information 
%    <table>
%  </div>
%
%  <h2>Efficiency plots</h2>
%  <div class='plots'>
%    <table>
%      * efficiency plots table 
%      * one plots for each cut or combinaison cut used
%      * each plots is made with hrss, energy and distance (id
%        physical waveforms).
%    <table>
%  </div>
%
% SYNTAX :
%   grpWebPage (grp,params)
% 
% INPUT : 
%    grp : group name
%    params : HTML params
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Aug 2016
%

  if ~exist([params.webpath '/' grp])
    system(['mkdir -p ' params.webpath '/' grp]);
    system(['mkdir -p ' params.webpath '/' grp '/figures']);
  end


  % create a new html document
  doc = html.document;
  doc.setStyle('style.css');
  doc.setScript('main.js');
  doc.setMathJax;

  % get group's waveforms 
  wvf=params.dic.headers(grp);

  % load cut used for efficiency plot
  m=matfile([params.searchPath '/figures/eff_' wvf(1).id '.mat']);
  cut=m.cut;

  doc.createNode('h1','STAMPAS report page');

  % add a ariane string for navigation
  d=doc.createNode('div','',...
                   'Attribute',struct('class','ariane'));
  doc.createNode('a','main',...
                 'Attribute',struct('href','../'), ...
                 'Parent',d);
  d.appendChild(doc.createTextNode([' > ' grp]));
  
  % <h2> title
  % and a <div id='$grp$Info' class='info'> container
  doc.createNode('h2','Group Information',...
                 'Attribute',struct('id',grp));
  div=doc.createNode('div',...
                     'Attribute',struct(...
                         'id',[grp 'Info'],...
                         'class','info')...
                     );
  
  % table 1 : information
  t=doc.createNode('table','Parent',div);
  
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Name : ','Parent',tr);
  doc.createNode('td',grp,'Parent',tr);
  
  tr=doc.createNode('tr','Parent',t);
  doc.createNode('th','Waveforms : ','Parent',tr);
  td=doc.createNode('td','Parent',tr);
  doc.createNode('a',wvf(1).legend,'Attribute',...
                 struct('href',wvf(1).id),...
                 'Parent',td);
  
  if length(wvf)>1
    for w=wvf(2:end)'
      tr=doc.createNode('tr','Parent',t);
      doc.createNode('th','Parent',tr);
      td=doc.createNode('td','Parent',tr);
      doc.createNode('a',w.legend,'Attribute',...
                      struct('href',w.id),...
                      'Parent',td);
    end
  end
  
  % table 2 efficiency
  addEfficiencyTable(doc,params.dic.headers(grp),params);  
  writeEfficiencyLatexTable(params.searchPath,...
                            [params.webpath '/' grp '/STAMPAS_efficiency.tex'],...
                            params.dic.headers(grp));
  
  % table 3 : efficiency plots
  doc.createNode('h2','Efficiency plots');
  div=doc.createNode('div','Attribute',struct('class','plots'));
  t=doc.createNode('table','Parent',div);

  tr=doc.createNode('tr','Parent',t);
  th=doc.createNode('th','Attribute',struct('colspan','3'), ...
                     'Parent',tr);

  dth = doc.createNode('div','Parent',th);
  doc.createNode('a', 'hrss', 'Attribute',...
                  struct('href','#','onclick', ...
                         'changeEffPlots("hrss");return false'),...
                  'Parent',dth);
  sup=doc.createNode('sup','Parent',dth);
  doc.createNode('a','1','Attribute',struct('href',['#' ...
                      'hrss_info']),'Parent',sup);

  dth = doc.createNode('div','Parent',th);
  doc.createNode('a','energy',...
                 'Attribute',struct(...
                     'href','#', ...
                     'onclick','changeEffPlots("energy");return false'),...
                 'Parent',dth);
  sup=doc.createNode('sup','Parent',dth);
  doc.createNode('a','3',...
                 'Attribute',struct('href','#energy_info'),...
                 'Parent',sup);
  
  if strcmp(wvf(1).xlabel,'distance')
    dth = doc.createNode('div','Parent',th);
    doc.createNode('a', 'distance', 'Attribute',...
                   struct('href','#','onclick', ...
                          'changeEffPlots("distance");return false'),...
                   'Parent',dth);
    sup=doc.createNode('sup','Parent',dth);
    doc.createNode('a','2',...
                   'Attribute',struct('href','#distance_info'),...
                   'Parent',sup);
  end
  
  row=0;
  tr=doc.createNode('tr','Parent',t);
  for c=1:numel(cut)
    % max row per line : 3
    row=row+1;
    if row>3;tr=doc.createNode('tr','Parent',t);row=1;end
    addplot(doc,['figures/eff_' grp '_' cut{c} '_hrss.png'],tr);
  end
  
  % footer
  addFooter(doc);

  % make path & copy file  
  system(['cp -r  ./template/style.css ' params.webpath '/' grp '/style.css']);
  system(['cp -r  ./template/main.js ' params.webpath '/' grp '/main.js']);
  system(['cp -r ' params.searchPath '/figures/' grp '/*.png ' params.webpath ...
          '/' grp '/figures']);
  
  % write html doc
  doc.write([params.webpath '/' grp '/index.html']);
end

