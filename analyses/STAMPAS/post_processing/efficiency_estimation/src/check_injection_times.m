function check_injection_times(searchPath)

injections=load([searchPath '/tmp/radec.txt']);
joblist=load([searchPath '/tmp/joblist_injections.txt']);
dag=readtext([searchPath '/stamp_allsky_inj.dag'], ' ', '%', '', 'textual');

nb=size(dag,1);

results=[];
for i=3:4:nb
  dag_i = dag(i,~cellfun(@isempty,dag(i,:)));
  lagnumber_i = dag_i(9);
  a=strsplit(lagnumber_i{1},'-');
  wvf_index=cellfun(@str2num, a(1,2));
  b=strsplit(char(a(1,1)),'lagNumber="');
  inj_number=cellfun(@str2num, b(1,2));
  if wvf_index>0
    jobnumber_i=dag_i(8);
    a=strsplit(jobnumber_i{1},'"');
    jobnumber=cellfun(@str2num, a(1,2));

    grouplength_i=dag_i(10);
    a=strsplit(grouplength_i{1},'"');
    grouplength=cellfun(@str2num, a(1,2));

    x1=injections(inj_number,2+wvf_index);
    x2=joblist(jobnumber,2);
    x3=joblist(jobnumber+grouplength-1,3);
    group_index=joblist(jobnumber,5);
    group=joblist(joblist(:,5)==group_index,:);
    group_duration=group(end,3)-group(1,2);
    f=(x1-group(1,2))/group_duration;

%    display([jobnumber_i ' ' lagnumber_i ' ' grouplength_i ' ' ...
%	     num2str(x1) ' ' num2str(x2) ' ' num2str(x3) ' ' num2str(group_duration) ' ' num2str(f) ...
%	    ' ' num2str(x1-x2)])
    results=[results; wvf_index inj_number x1 x2 x3 group_duration f x1-group(1,2)];
  end

end

figures_folder=[searchPath '/figures'];
dlmwrite([figures_folder '/injections.txt'],results,'delimiter',' ','precision','%.1f');

figure(1)
hist(results(:,7),100);
%hold on 
%ext=results(:,1)==1;
%hist(results(ext,6),100);
%hold off

xlabel('t_{inj}-t_{start-segment} / segment duration','fontsize',15);
ylabel('Number of events','fontsize',15);

make_png(figures_folder,'injections_time_frac')

figure(2)
hist(results(:,8),100);
%hold on 
%ext=results(:,1)==1;
%hist(results(ext,7),100);
%hold off

xlabel('t_{inj}-t_{start-segment} [s]','fontsize',15);
ylabel('Number of events','fontsize',15);

make_png(figures_folder,'injections_time')


figure(3)
a=results(find(results(:,8)<100),8);
hist(a,100);
xlim([0 100])
height=ylim;

rectangle('Position', [0 0 10 height(2)],'FaceColor', [.5 .5 .5])
%hold on 
%ext=results(:,1)==1;
%hist(results(ext,7),100);
%hold off

xlabel('t_{inj}-t_{start-segment} [s]','fontsize',15);
ylabel('Number of events','fontsize',15);

make_png(figures_folder,'injections_time_zoom')


