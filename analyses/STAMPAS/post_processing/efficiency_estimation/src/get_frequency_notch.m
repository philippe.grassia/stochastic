function [freqNotch]=get_frequency_notch(RUN)

FqN = load(['../../freqs_' RUN 'H1L1_1s_1Hz.txt']);
idx  = [0,find(diff(FqN)>1)];
freqNotch = [FqN(idx(1:end -1)+1); FqN(idx(2:end))];
