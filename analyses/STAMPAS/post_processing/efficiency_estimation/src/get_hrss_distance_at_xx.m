function [x3] = get_hrss_distance_at_xx(file,option,percent)  
% get_hrss_distance_at_xx :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = get_hrss_distance_at_xx (file,option)
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Mab
% CONTACT : mabizoua@lal.in2p3.fr
% VERSION :
% DATE : 
%

load(file);

points_nb = size(eff,1);
col       = size(eff,2)-1;

if points_nb == 0
  return
end

if strcmp(option,'hrss')
  ind=2;
else
  ind=1;
end

if percent>1
  percent=percent/100;
  warning(['determination of the hrss at ' num2str(percent*100) '% ' ...
           'efficiency']);
end

x3=Inf;
for i=points_nb:-1:1
  if eff(i,col)<percent
    if i>1
      a=(eff(i-1,col)-eff(i,col))/(eff(i-1,ind)-eff(i,ind));
      b=eff(i-1,col)+eff(i-1,ind)*(eff(i-1,col)-eff(i,col))/ ...
        (eff(i,ind)-eff(i-1,ind));
      x3=(percent-b)/a;
    else
      warning(['First point is below ' num2str(percent*100) '% ' ...
               'efficiency']);
    end
    break
  end
end


end

