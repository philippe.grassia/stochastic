function get_hrss_distance_energy(GPS_start_inj,GPS_end_inj,ID_inj,RUN,percent)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M. A. Bizouard (mabizoua@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

display ('get_hrss_distance_energy  ...')

if isnumeric(GPS_start_inj)
  GPS_start_inj=num2str(GPS_start_inj);
end
if isnumeric(GPS_end_inj)
  GPS_end_inj=num2str(GPS_end_inj);
end
if isnumeric(ID_inj)
  ID_inj=num2str(ID_inj);
end

if ischar(percent)
  percent=str2num(percent);
end


searchPath=['INJ/Start-' GPS_start_inj '_Stop-' GPS_end_inj '_ID-' ID_inj];
figures_folder=['../../' searchPath '/figures'];

OFFLINE=str2num(getenv('OFFLINE'));

figures_folder=['../../' searchPath '/figures'];

% Get waveforms parameters
parameters = get_waveform_parameters(searchPath);

nb_wvf=size(parameters.waveform_names,1);

% Outfile file
fid1=fopen([figures_folder '/hrss.txt'],'w');
fid2=fopen([figures_folder '/hrss.html'],'w');

% Column in eff files. Col 11 corresponds to SNRfrac+SNR cuts
% applied
col=11;

fprintf(fid2,'<table>\n');
fprintf(fid2,'<tr><td>Waveform  </td><td> hrss @ 50%% </td><td> distance @ 50%% (Mpc) </td><td>  Energy (Msun)</td></tr>\n');
for i=1:nb_wvf
  data=load([figures_folder '/eff_' char(parameters.waveform_names(i)) '.txt']);

  hrss=get_hrss_distance_at_xx(data,'hrss',col,percent);
  %test=strfind(char(parameters.waveform_names(i)),'adi');
  test = cell2mat(strfind({'adi','inspiral','PT'},char(parameters.waveform_names(i))));
  if length(test)>0
    distance_at_xx=get_hrss_distance_at_xx(data,'distance',col,percent)*1e6;
  else
    distance_at_xx=10000;
  end
  distance=10000;
  egw=energy(hrss,distance,parameters.fmax(i));
  fprintf(fid1,'%s %.2g %.2f %.1g\n',char(parameters.waveform_names(i)),hrss,distance_at_xx/1e6,egw);
  if length(test)>0
    fprintf(fid2,'<tr><td>%s </td><td> %.3g </td><td> %.2f </td><td> %.2g </td></tr>\n', ...
	    char(parameters.waveform_names(i)),hrss,distance_at_xx/ ...
	    1e6,egw);
  else
    fprintf(fid2,'<tr><td>%s </td><td> %.3g </td><td> - </td><td> %.2g </td></tr>\n', ...
	    char(parameters.waveform_names(i)),hrss,egw);
  end
end
fprintf(fid2,'</table>\n');

if OFFLINE==1
  exit
end

return



function x3=get_hrss_distance_at_xx (data, option, col, percent)

%
% Get hrss/distance at xx% using a simple extrapolation method.
%
x3 = 0;

points_nb=size(data,1);
if points_nb == 0
  return
end

if strcmp(option,'hrss')
  ind=2;
else
  ind=1;
end

if percent>1
  percent=percent/100;
  warning(['determination of the hrss at ' num2str(percent*100) '% efficiency'])
end

for i=points_nb:-1:1
  if data(i,col)<percent
    if i>1
      a=(data(i-1,col)-data(i,col))/(data(i-1,ind)-data(i,ind));
      b=data(i-1,col)+data(i-1,ind)*(data(i-1,col)-data(i,col))/(data(i,ind)-data(i-1,ind));
      x3=(percent-b)/a;
    else
      display(['First point is below ' num2str(percent*100) '% efficiency']);
    end
    break
  end

end

return


function egw=energy(hrss,r,f)

% hrss in 1/sqrtHz
% r in pc
% f in Hz

msun=1.988e30;        % kg
msunc2=msun*(3e8)^2;  % kg m^2 s^-2
c=2.99e8;             % m s^-1
G=6.67e-11;           % kg m^3 s^-2

egw=(hrss*r*3.08e16*pi*f)^2;
egw=egw*c^3/G;
egw=egw/msunc2;   % solar mass c^2
return
