function [H50,D50,E50] = estimateDistance(searchPath,wvf)  
% estimateDistance : estimate hrss and diatnce at 50% for the given
% wvf
% DESCRIPTION :
%    fit a sigmoid on the efficiency curve and extract the
%    hrss @ 50%. for physical wavforms compute the distance @ 50%
%      
%
% SYNTAX :
%   [H50 D50] = estimateDistance (searchPath, wvfId)
% 
% INPUT : 
%    searchPath ; path of the search
%    wvfId : id of the waveform (eg adiA)
%
% OUTPUT :
%    H50 : hrss @ 50%
%    D50 : distance @ 50%
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : may 2016
%

  %% CHECK INPUT
  % ---------------------------------------------------------------
  %
  if ~exist(searchPath)
    error(['path : ' searchPath ' not found']);
  end

  if ~exist([searchPath '/figures/eff_'  wvf.id '.mat'])
    error(['file : ' searchPath '/figures/eff_'  wvf.id '.mat not found']);
  end
  

  %% GET DATA
  % ---------------------------------------------------------------
  %
  load([searchPath '/figures/eff_'  wvf.id '.mat']);
  Y = eff(:,end-1);

  %% HRSS
  % ---------------------------------------------------------------
  % for physical wvf compute the hrss @ 50%
  %
  
  H50=10^(fitEff(log10(eff(:,2)),Y));

  %% ENERGY
  % ---------------------------------------------------------------
  % for physical wvf compute the energy @ 50%
  %
  %E50 = 10^fitEff(log10(eff(:,3)),Y);
  E50 = wvf.energy*(H50/wvf.hrss)^2;

  %% DISTANCE
  % ---------------------------------------------------------------
  % for physical wvf compute the distance @ 50%
  % for other waveform return '-'
  %
  if strcmp(wvf.xlabel,'distance')
    D50 = wvf.distance*wvf.hrss/H50;
    %D50 = 10^fitEff(log10(eff(:,1)),Y);
  else
    D50 = '-';
  end

end



function out=fitEff(D,Y)
% Fit function seems to have a problem for this model 
  ft = fittype('a/(1+(x/v50)^(b*(1+c*tanh(x/v50))))',...
               'independent', {'x'},...
               'coefficients',{'a','v50','b','c'});
  
  opt = fitoptions('Method','NonlinearLeastSquares',...
                   'MaxFunEvals',100000,...
                   'MaxIter',10000,...
                   'StartPoint',[1,      1, 10,  0],...
                   'Lower',     [0, min(D),  1, -1],...
                   'Upper',     [1, max(D), 50,  1],...
                   'Robust','LAR');
  
  [cfit,g,o] = fit(D,Y,ft,opt);
  
  if o.exitflag<1
    o
    g
    o.message
    wvf.id
    plot(D,Y)
    set(gca,'XScale','log')
    hold all
    plot(D,cfit(D))
    hold off
    print('-dpng',[searchPath '/figures/fitProblem.png']);
    disp(['diaplot generate save : ' searchPath '/figures/fitProblem.png'])
    error('fit not converge');
  end

  out = cfit.v50;

end
