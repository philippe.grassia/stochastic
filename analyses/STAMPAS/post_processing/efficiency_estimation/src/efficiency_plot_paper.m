function efficiency_plot_paper(family_name,amp_variable,ind,waveform_names,colors,styles,...
				  legend_label,figures_folder,run)

% 3 4   all
% 5 6   DQ
% 7 8   SNRfrac
% 9 10  SNR
% 11 12 SNRfrac+SNR
% 13 14 DQ+SNRfrac
% 15 16 DQ+SNRfrac+SNR

addpath('../../../stamp2/src/misc/ezyfit')

warning off
if strcmp(amp_variable,'distance')==1
  amp_index=1;
  xscale=[.2 100];
  xlabelname='Distance [Mpc]';
else
  amp_index=2;
  xscale=[1e-22 5e-19];
  xlabelname='h_{rss} [strain/Hz^{1/2}]';
end

display (['Plot ' char(waveform_names(ind(1))) '-->' ...
	  char(waveform_names(ind(end)))])

first=1;
type=5;
p=[];
index=0;
if strcmp(run,'S5')==1
  figure(ind(1))
else
  figure(2*ind(1))
end
for i=1:length(ind)
  index=index+1;
  filename=[figures_folder '/eff_' char(waveform_names(ind(i))) '.txt'];
  data = load(filename);
  h=errorbar(data(:,amp_index),data(:,1+2*type),data(:,2+2*type), ...
	     'Color', colors(ind(i),:),'LineStyle','none','LineWidth',2);
  if first==1
    first=2;
    hold on
  end
  set(gca,'XScale','log');
  errorbar_tick(h,180000);
  % Spline fit
  xfit=logspace(log10(data(1,amp_index)),log10(data(end,amp_index)),100);
  yfit=interp1(data(:,amp_index),data(:,1+2*type),xfit,'pchip');
  p(index)=plot(xfit,yfit,char(styles{ind(i)}),'color',colors(ind(i),:),'LineWidth',2);
  ylim([0 1])
end
hold off
grid on

if amp_index==2
  l=legend(p,legend_label);
  set(l,'Location','NorthWest');
else
  l=legend(p,legend_label);
  set(l,'Location','NorthEast');
end

xlim(xscale);
xlabel(xlabelname, 'Fontsize', 15);
ylabel('Detection efficiency', 'Fontsize', 15);

if strcmp(amp_variable,'distance')==1
  make_png(figures_folder, ['eff_' family_name '_distance_' run])
else
  make_png(figures_folder, ['eff_' family_name '_' run])
end

%figure
%plot(data(:,amp_index),data(:,1+2*type))
%set(gca,'XScale','log');
%showfit(['a/(1+(log10(x)/d)^(b*(1+c*tanh(log10(x)/d))));a=1;b=1;c=10;d=-47.94'],...
%	    'dispfitlegend','off','dispeqboxmode','off');
%showfit('spline',...
%	'dispfitlegend','off','dispeqboxmode','off');


return