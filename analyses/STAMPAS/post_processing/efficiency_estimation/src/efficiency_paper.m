function efficiency_paper(GPS_start_inj,GPS_end_inj,ID_inj,RUN)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This MATLAB script plots efficiency curves from pre-existing
% efficiency files created by the efficiencyCalc.m script.
%
% Input:
% - Plots: you can put it either a list of the files you
%   which to use for the plotting, or if they are part of the same
%   injection study, you can directly put the (fileName) you gave
%   them. The script will use the (fileName).list file to look for
%   all the efficiency files.
% - X_full: the values of the X axis, energy, distance, alpha factor, in 
%   the order given by efficiencyCalc.m. All given for a single alpha
%   factor (use X_part if you need to repeat several times the same
%           list)
% - Label: label of the X axis
% - Units: units of this label
% - Title: title of the plot
% - logScale: if set to 1, the X axis will use a log scale
% - graphName: neame of the output graph
% - interp: if set to true, the script will return for each curve
%   the X value associated with a 50% efficiency when it exists.
%
% Legend has to be changed by the user at the end of the file
%
% M. A. Bizouard (mabizoua@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

display('efficiencyPots ...');
if isnumeric(GPS_start_inj)
  GPS_start_inj=num2str(GPS_start_inj);
end
if isnumeric(GPS_end_inj)
  GPS_end_inj=num2str(GPS_end_inj);
end
if isnumeric(ID_inj)
  ID_inj=num2str(ID_inj);
end

searchPath=['INJ/Start-' GPS_start_inj '_Stop-' GPS_end_inj '_ID-' ID_inj];

% logfile
logfile(searchPath, 'efficiencyPlots.log');

% get OFFLINE value
OFFLINE=str2num(getenv('OFFLINE'));



figures_folder=['figures_' RUN '_ID' num2str(ID_inj)];

% Get waveforms parameters
parameters = get_waveform_parameters(searchPath);

% Remove adi-D waveforms for the S5/S6 paper
% Inverse MONO and LINE waveforms
parameters.waveform_names=parameters.waveform_names([1:3 5 8 9 6 7 10:end]);
parameters.alphas=parameters.alphas([1:3 5 8 9 6 7 10:end]);
parameters.fmin=parameters.fmin([1:3 5 8 9 6 7 10:end]);
parameters.fmax=parameters.fmax([1:3 5 8 9 6 7 10:end]);
parameters.distance=parameters.distance([1:3 5 8 9 6 7 10:end]);
parameters.duration=parameters.duration([1:3 5 8 9 6 7 10:end]);
parameters.hrss=parameters.hrss([1:3 5 8 9 6 7 10:end]);

nb_wvf=size(parameters.waveform_names,1);

leg_names_ADI={'ADI A','ADI B','ADI C','ADI E'};
leg_names_lmq={'MONO A','MONO B','LINE A','LINE B','QUAD A','QUAD B'};
leg_names_sg={'SG A','SG B'};
leg_names_wnb={'WNB A','WNB B','WNB C'};

a=strfind(parameters.waveform_names,'adi');
c=strfind(parameters.waveform_names,'line');
d=strfind(parameters.waveform_names,'mono');
e=strfind(parameters.waveform_names,'quad');
f=strfind(parameters.waveform_names,'sg');
g=strfind(parameters.waveform_names,'wnb');

ind_adi=[];
ind_lmq=[];
ind_sg=[];
ind_wnb=[];

i_adi=1;
i_lmq=1;
i_sg=1;
i_wnb=1;

%close all
for i=1:nb_wvf
  if a{i}>0
    ind_adi=[ind_adi ;i];
    tmp=char(strsplit(char(parameters.waveform_names(i)),'_tapered.dat'));
    leg_adi{i_adi}=upper(strrep(tmp(1,:),'_','-'));
    i_adi=i_adi+1;
  end
  if size(c{i},1)+size(d{i},1)+size(e{i},1)>0
    ind_lmq=[ind_lmq ;i];
    tmp=char(strsplit(char(parameters.waveform_names(i)),'.dat'));
    tt=strrep(tmp(1,:),'A','-A');
    leg_lmq{i_lmq}=upper(strrep(tt,'B','-B'));
    i_lmq=i_lmq+1;
  end
  if size(f{i},1)>0
    ind_sg=[ind_sg ;i];
    tmp=char(strsplit(char(parameters.waveform_names(i)),'.dat'));
    tt=strrep(tmp(1,:),'A','-A');
    tt=strrep(tt,'C','-C');
    leg_sg{i_sg}=upper(strrep(tt,'B','-B'));
    i_sg=i_sg+1;
  end
  if size(g{i},1)>0
    ind_wnb=[ind_wnb ;i];
    tmp=char(strsplit(char(parameters.waveform_names(i)),'.dat'));
    tt=strrep(tmp(1,:),'A','-A');
    tt=strrep(tt,'C','-C');
    leg_wnb{i_wnb}=upper(strrep(tt,'B','-B'));
    i_wnb=i_wnb+1;
  end
  
end

color = [0 0 1; 1 0 0; 0 0 0; 0 1 0; 0 0 1; 0 1 0; 1 0 0; 1 1 0; 0 0 0; 1 0 1; 0 0 1; 1 0 0; 0 0 1; 1 0 0; 0 0 0];
style={'-','--','-.','-','-','-','--','--','-.','-.','-','--','-','--','-.'};
if size(ind_adi,1)>0
  efficiency_plot_paper('ADI','hrss',ind_adi,parameters.waveform_names,color,style,leg_adi,figures_folder,RUN);
end

if size(ind_adi,1)>0
  efficiency_plot_paper('ADI','distance',ind_adi,parameters.waveform_names,color,style,leg_adi,figures_folder,RUN);
end

if size(ind_lmq,1)>0
  efficiency_plot_paper('LMQ','hrss',ind_lmq',parameters.waveform_names,color,style,leg_lmq,figures_folder,RUN);
end

if size(ind_sg,1)>0
  efficiency_plot_paper('SG','hrss',ind_sg,parameters.waveform_names,color,style,leg_sg,figures_folder,RUN);
end

if size(ind_wnb,1)>0
  efficiency_plot_paper('WNB','hrss',ind_wnb,parameters.waveform_names,color,style,leg_wnb,figures_folder,RUN);
end


if OFFLINE==1
  exit
end
