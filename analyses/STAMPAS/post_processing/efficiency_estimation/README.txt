Last edited: 17/02/2015

1. efficiency.sh

- Compute the efficiency for each waveform (efficiencyCalc.m)
- Draw the efficiency curves for each waveform (efficiencyPlots.m)
- Diagnostic plots to study missed/found injections (missedfound.m)
- Diagnostic plots (plots_correlation.m and plots_histogram.m)
- generate_html.sh
