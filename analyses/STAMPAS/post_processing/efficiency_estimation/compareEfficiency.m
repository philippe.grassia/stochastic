%
%  AUTHOR : Valentin FREY
%
%  FILE : compareEfficiency.m
%
%  DESCRIPTION : Compare the efficiency 
%  between two run
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
tic;

usage(){
    echo 'Arg 1: GPS_START'
    echo 'Arg 2: GPS_END'
    echo 'Arg 3: ID'

    exit
}

# main
[[ $# -ne 3 ]] && usage

GPS_START=$1
GPS_END=$2
ID=$3


RUN   = 'S5';
RUN_1 = 101; % first run id  
RUN_2 = 103; % second run id



%%% waveform parameter
FOLDER = ['INJ/Start-' GPS_START '_Stop-' GPS_END '_ID-' ID];
param = get_waveform_parameters(FOLDER);

nb_wf = size(param.waveform_names,1);
for wf=1:nb_wf
  nb_alphas{wf} = size(param.alphas{wf},2);
end


%%% efficiency data file 
figures_folder{1}=['figures_' RUN '_ID' num2str(RUN_1)];
figures_folder{2}=['figures_' RUN '_ID' num2str(RUN_2)];
fileName=['eff'];

%%% open file compare.txt
fid = fopen(['compare/compare' num2str(RUN_1) '-' num2str(RUN_2) '.txt'],'w');
    
%%%   --  Config end --   %%%                                                                                                                                 

for wvf=1:nb_wf
  for run=1:2
    name=char(param.waveform_names(wvf));   
    data = load([figures_folder{run} '/' fileName '_' name '.txt']);
    
    [yy_sup, idx_sup] = min(data(find(data(:,9)>0.5), 9)); %% for adi E in the Stoch initial param
    [yy_inf, idx_inf] = max(data(find(data(:,9)<0.5), 9));

    if yy_inf == 0 | yy_sup == 0 
      xx(run)=0;
    elseif  size(yy_inf,1) == 0 | size(yy_sup,1) == 0
      xx(run)=0;
    else
  
      if wvf<6 %%% for the adi injection
	pente = (yy_inf-yy_sup) / (data(find(data(:,9) == yy_inf),1) - data(find(data(:,9) == yy_sup),1));
	xx(run) = (0.5-yy_sup)/pente + data(find(data(:,9) == yy_sup),1);
      else  %%%for the other injection
	pente = (yy_sup-yy_inf) / (data(find(data(:,9) == yy_sup),2) - data(find(data(:,9) == yy_inf),2));
	xx(run) = (0.5-yy_inf)/pente + data(find(data(:,9) == yy_inf),2);
      end

    end
  end
  ratio = xx(2)/xx(1);
  wvfName = regexprep(param.waveform_names{wvf}, '_tapered.dat|.dat', '');
  display([wvfName ' '  num2str(xx(1)) ' ' num2str(xx(2)) ' ' num2str(ratio)]);
  fprintf(fid, '%s \t%e \t%e \t%f \n', wvfName, xx(1),xx(2), ratio);    
end

fclose(fid);