function parameters = get_waveform_parameters(searchPath)
% Gets the main parameters of the waveforms used in the current INJ
% dag. It assumes:
% - the list of waveforms is stored in the waveforms.txt file found
% in the search folder. 
% - the waveforms are stored in the STAMPAS local copy folder
% (STAMPAS/waveforms). 
%
% Inputs:
% - searchPath: path to the search folder
%
% Outputs:
% - parameters structure containing all parameters: alphas,
% waveform names, freq min, freq max, distance and duration
%
% To be done:
% - Currently not working properly for the eBBH waveforms
% - Save parameters in a file & add a check at the beginning if the
% parameters file exist. If yes, load the parameters from the file

parameters=[];

searchPath=strrep(searchPath, '../../', '');

% Check whether searchPath exists
if exist(['../../' searchPath],'dir')==0
    error([searchPath ' does not exist']);
    return
end

% If continuum injection run then, we will load the temporarily generated dictionary instead

dict_ci_path = ['../../',searchPath, '/ContWaveforms/dic_ci.yml'];

if exist(dict_ci_path)==2
  dic = classes.waveforms.dictionary(dict_ci_path);
else
  dic = classes.waveforms.dictionary;
end

% Get the list of waveforms
B=readtext(['../../' searchPath '/waveforms.txt'], ' ', '%', '', 'textual');

% The waveform name is stored in the first column
nb_wvfs=size(B,1);

% If no waveforms are found, exit
if nb_wvfs==0
  error(['No waveform found in ' searchPath 'waveforms.txt']);
  return
end

waveform_names=[];
for i=1:nb_wvfs
  waveform_names=[waveform_names; B(i,1)];
end

%% get rid of the "/" for ebbh waveforms
for i=1:nb_wvfs
  if ~isempty(regexp(waveform_names{i},'.*/$'))
    waveform_names{i}=waveform_names{i}(1:end-1);  
  end
end

% Get the alpha values
nb_max_alphas=0;
for i=1:nb_wvfs
  % non-empty elements of row i of B
  B_i = B(i,~cellfun(@isempty,B(i,:)));
  alphas_i = B_i(2:end);
  alphas{i} = cellfun(@str2num, alphas_i);
  nb_max_alphas=max(nb_max_alphas,size(alphas{i},2));
  %alphas{i} = arrayfun(@(x) num2str(x,'%.6f'),double_alphas{i}, ...
  %		       'UniformOutput',false);
end
% Get other parameters 
fmin=zeros(nb_wvfs,1);
fmax=zeros(nb_wvfs,1);
distance=zeros(nb_wvfs,1);
duration=zeros(nb_wvfs,1);
hrss=zeros(nb_wvfs,1);

for i=1:nb_wvfs
  header = dic.headers(waveform_names{i});
  fmin(i) = header.fmin;
  fmax(i) = header.fmax;
  distance(i) = header.distance;
  duration(i) = header.duration; 
  hrss(i) = header.hrss;  
end


% TP 4/15/2015- nb_alphas is no longer used since
% this can be different for different waveforms.
parameters.nb_wvfs=nb_wvfs;
parameters.nb_max_alphas=nb_max_alphas;
parameters.alphas=alphas;
parameters.waveform_names=waveform_names;
parameters.fmin=fmin;
parameters.fmax=fmax;
parameters.distance=distance;
parameters.duration=duration;
parameters.hrss=hrss;
parameters.searchPath=searchPath;
%parameters.double_alphas=double_alphas;

return;
