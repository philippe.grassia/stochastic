function [nbInjections] = get_nb_injections (searchPath)

searchPath=strrep(searchPath,'../../', '');

[names,values]=textread(['../../' searchPath '/config_inj.txt'], '%s %s\n',-1,'commentstyle','matlab');
for ii=1:length(names)
  switch names{ii}
   case 'NumberOfInjections'
    nbInjections = str2num(values{ii});
    break;   
   case 'waveform_path'
    waveform_path = values{ii};
    if exist(['../../' waveform_path])==7 
      waveform_path = ['../../' waveform_path];
    else  
      error('waveform_path does not exist')
    end
  end
end
