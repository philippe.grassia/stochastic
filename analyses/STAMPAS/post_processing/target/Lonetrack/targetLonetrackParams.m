function [params] = targetLonetrackParams(searchPath)  
% targetParams : get the params struct use by run_clustermap
% DESCRIPTION :
%   get the parameter use by run_clustermap from the tmp/params &
%   tmp/clustparam files
%  
% SYNTAX :
%   [params] = targetParams (searchPath)
% 
% INPUT : 
%    searchPath : the search directory 
%
% OUTPUT :
%    params : structure containing the parameters 
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : sep 2015
%
  load([searchPath '/tmp/clustParam.mat']);

  params = stampDefaults;

  params.searchPath = searchPath;
  params.type    = regexprep(searchPath,'.*/(BKG|INJ)/.*', '$1');

  params.savePlots = false;
  params.saveMat   = false;

  doZebra          = strassign(doZebra);
  doSeedless       = strassign(doSeedless);
  doSeedlessAllsky = strassign(doSeedlessAllsky);
  doLonetrack      = strassign(doLonetrack);
  doParallel       = strassign(doParallel);
  doGPU            = strassign(doGPU);
  segDur           = strassign(segDur);

  params.config  = readConfig([params.searchPath '/config_' ...
                      lower(params.type) '.txt']);

  if ~doSeedless
    % create loop over search directions
    if doZebra==1
      params.skypatch = false;
      params.fastring = false;
    else
      params.skypatch = true;
      params.fastring = true;
    end

    % sky resolution parameters
    if strcmp(pair,'H1L1')
      params.dtheta=0.7;
    else
      params.dtheta=0.3;
    end
    params.thetamax = 180;

    % save all clusters
    params.doAllClusters=true;
    % for overlapping ft-maps and to unify the results format
    params.stochPlus = true;
  end

  params.fmin = strassign(fmin);
  params.fmax = strassign(fmax);


  %%% FreqNotch
  params.doStampFreqMask=true;
  params.StampFreqsToRemove = ...
      textread([searchPath '/tmp/frequencies.txt'],...
               '%d',-1,'commentstyle','matlab');

  params.StampFreqsToRemove = ...
      params.StampFreqsToRemove(params.StampFreqsToRemove...
                                >=params.fmin & ...
                                params.StampFreqsToRemove...
                                <=params.fmax);
  if segDur < 1
    ff = params.fmin:(1/segDur):params.fmax;
    params.StampFreqsToRemove = intersect(ff, ...
                                          params.StampFreqsToRemove);
    params.glitch.doCut = false;
    params.glitch.numBands = 1;
    params.glitch.doCoincidentCut = false;
  else
    % apply a glitch cut
    params.glitch.doCut = true;
    params.glitch.numBands = 6;
    params.glitch.doCoincidentCut = false;
    params.glitch.Xi_frac = 0.02;
  end

  if doSeedless
    % Set seedless parameters based on parameters in seedless
    % params
    % file
    params = getSeedlessParams(params,seedlessParams,doLonetrack);

    % Set GPU or Parallel CPU/GPU usage
    params.doGPU = doGPU;
    params.doParallel = doParallel;

    % Initialization of the matlabpool when using parallel (and GPU)
    params=initParallel(params);

  else
    % perform a clustering search, use default burstegard
    % parameters
    params = burstegardDefaults(params);

    % Uses zebragard to speed-up the search
    if doZebra==1
      params=zebragardDefaults(params);
    end
  end
  
  params.anteproc.loadFiles = true;
  itf1Prefix=[pair(1) '-' pair(1:2) '_' matfilesName];
  itf2Prefix=[pair(3) '-' pair(3:4) '_' matfilesName];
  if strcmp(matfilesPath(1),'/')
    params.anteproc.inmats1 = [matfilesPath '/' itf1Prefix];
    params.anteproc.inmats2 = [matfilesPath '/' itf2Prefix];
  else
    params.anteproc.inmats1 = ['../../' matfilesPath '/' itf1Prefix];
    params.anteproc.inmats2 = ['../../' matfilesPath '/' itf2Prefix];
  end
  params.anteproc.useCache = false;
  
  % Cachefiles option (cachefile tells code where
  % the pre-processed files are).  This option is
  % Typically used when pre-processed files are
  % stored on the nodes

  useCachefiles = exist([searchPath '/anteproc_cache.mat'])==2;
  
  %{
  if useCachefiles
    params.anteproc.useCache = true;
    params.anteproc.cacheFile = [searchPath '/anteproc_cache.mat'];
  else
    params.anteproc.useCache = false;    
  end
  %}

  % Time-shift amount (if doing a "time-based" time-shift).
  params.anteproc.timeShift1 = 0;
  params.anteproc.timeShift2 = 0;

  % Do a jobfile time-shift?
  params.anteproc.jobFileTimeShift = true;

  % No stitching of the data. No check to be done
  params.anteproc.bkndstudy = false;

end

