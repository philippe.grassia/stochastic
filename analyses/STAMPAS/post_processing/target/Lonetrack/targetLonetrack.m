function targetLonetrack(searchPath,aa,folder)  
% targetLonetrack :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = targetLonetrack ()
% 
% INPUT : 
%    
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Mar 2017
%
  params = targetLonetrackParams(searchPath);
  
  params.ra  = 24*rand(1,1);
  params.dec=(180/pi)*(acos(1-2*rand(1,1))-pi/2);
  
  params.anteproc.jobfile = [searchPath '/tmp/jobfile_bknd.txt'];
  params.pFile       = [searchPath '/tmp/p_H1.txt'];

  params.stochtrack.lonetrack = true;

  if params.config.doZeroLag
    trigger(1)  = aa(1); % index
    trigger(3)  = aa(4); % SNR coherent
    trigger(4)  = aa(5); % SNR ifo1 
    trigger(5)  = aa(6); % SNR ifo2
    trigger(6)  = aa(7); % start
    trigger(7)  = aa(8); % stop
    trigger(8)  = aa(9); % duration
    trigger(9)  = aa(10); % lag
    trigger(10) = aa(11); % mini lag
    trigger(11) = aa(12); % fmin
    trigger(12) = aa(13); % fmax
    trigger(13) = aa(14); % timedelay 
    trigger(14) = aa(15); % SNRfrac
  else
    trigger(1)  = aa(1); % index
    trigger(3)  = aa(3); % SNR coherent
    trigger(4)  = aa(4); % SNR ifo1 
    trigger(5)  = aa(5); % SNR ifo2
    trigger(6)  = aa(6); % start
    trigger(7)  = aa(7); % stop
    trigger(8)  = aa(8); % duration
    trigger(9)  = aa(9); % lag
    trigger(10) = aa(10); % mini lag
    trigger(11) = aa(11); % fmin
    trigger(12) = aa(12); % fmax
    trigger(13) = aa(13); % timedelay 
    trigger(14) = aa(14); % SNRfrac
 end    

  %% Init HTML Doc
  doc=html.document;
  doc.setStyle('style.css');

  doc.createNode('h1','STAMPAS TARGET ANALYSIS');
  idoc = doc.createNode('div','Attribute',struct('id','info'));
  pdoc = doc.createNode('div','Attribute',struct('id','plots'));
  
  % make folder
  if exist([folder '/FTMAPS/plots_' num2str(trigger(1))])~=7
    system(['mkdir -p ' folder '/FTMAPS/ftmap_' ...
            num2str(trigger(1))]);
    system(['cp template/style.css ' ...
            folder '/FTMAPS/ftmap_' num2str(trigger(1))...
            '/style.css']);
  end

  plotdir = [folder '/FTMAPS/ftmap_' num2str(trigger(1))];
  params.returnMap = true;
  
  % get the window corresponding to the given gps time
  jobs = load(params.anteproc.jobfile);
  %idx  = jobs(:,2)<=min(trigger(6:7)) & ...
  %       jobs(:,3)>=min(trigger(6:7))+trigger(8);
  idx = find(jobs(:,2) == trigger(6) | jobs(:,2) == trigger(7));

  t_start = jobs(idx,2);
  t_end   = jobs(idx,3);
  winIdx  = jobs(idx,1);
  
  if sum(idx)>1
    t_start = t_start(1);
    t_end   = t_end(1);
    winIdx  = winIdx(1);
  end

  params.lonetrackdir = [searchPath '/LonetrackTmp/lonetrack_0/tmp_' num2str(t_start)];
  system(['mkdir -p ' params.lonetrackdir]);
  
  currentLag=mod(winIdx+trigger(9),size(jobs,1));

  if currentLag==0
    currentLag=size(jobs,1);
  end

  params.anteproc.jobNum1 = winIdx;
  params.anteproc.jobNum2 = winIdx;

  rng(jobs(winIdx,2)+jobs(currentLag,2),'twister');

  % run clustermap
  stoch_out=clustermap(params, t_start, t_end)

  % save plot
  targetLonetrackPlots(stoch_out,plotdir,trigger);
  targetLonetrack_plots2html(doc);
  
  % include info
  targetLonetrackInfo(stoch_out,trigger,doc);

  clear stoch_out;

  doc.write([folder '/FTMAPS/ftmap_' num2str(trigger(1)) '/index.html'])


end
