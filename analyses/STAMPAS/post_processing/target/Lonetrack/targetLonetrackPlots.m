function [] = targetLonetrackPlots(stoch_out, dir,triggers)  
% targetPlots : plot the ftmap give by clustermap
% DESCRIPTION : the various type  of ftmap using clustermap
% 
% SYNTAX :
%   [] = targetPlots (stoch_out, dir, triggers)
% 
% INPUT : 
%    stoch_out : struct give by clustermap
%    dir : the save plot directory
%    triggers : array with containing triggers caracteristic
%       - trigger idx
%       - SNR 
%       - GPS start ifo1
%       - GPS start ifo2
%       - duration
%       - lag number
%       - fmin
%       - fmax
%       - ra
%       - dec
%       - SNRfrac coherent
%       - SNRfrac ifo1
%       - SNRfrac ifo2
%       - DQflags ifo1
%       - DQflags ifo2
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.2
% DATE : Mar 2016
%

  map     = stoch_out.map;
  cluster = stoch_out.cluster;

  t=map.segstarttime;
  f=map.f;

  start = min(triggers(3:4));
  stop  = min(triggers(3:4))+triggers(5);
  fmin  = triggers(8);
  fmax  = triggers(9);

  % general Plot
  figure();
  imagesc(t-start-0.5,f+0.5,map.snr, [-5 5]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'SNR map','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-snr');

  figure();
  imagesc(t-start-0.5,f+0.5,cluster.reconMax, [-5 5]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'SNR map','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'cluster-snr');

  figure();
  imagesc(t-start-0.5,f+0.5,map.naiP1./map.P1,[0 15]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'autopower map for ifo 1','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-ap1');

  figure();
  imagesc(t-start-0.5,f+0.5,map.naiP2./map.P2,[0 15]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'autopower map for ifo 2','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-ap2');

  figure();
  imagesc(t-start-0.5,f+0.5,log10(map.P1),[-47 -43]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'P map ifo1','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-p1');

  figure();
  imagesc(t-start-0.5,f+0.5,log10(map.P2),[-47 -43]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'P map ifo2','GPS ' num2str(stop)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-p2');

  %%% get the good index to make a zoomed plot
  tt=map.segstarttime>max(start-0.1*(stop-start),map.segstarttime(1)) & ...
     map.segstarttime<min(stop+0.1*(stop-start),map.segstarttime(end));

  ff=map.f>max(fmin-0.1*(fmax-fmin),map.f(1)) & ...
     map.f<min(fmax+0.1*(fmax-fmin),map.f(end));

  %%% general Plot
  figure();
  imagesc(t(tt)-start-0.5,f(ff)+0.5,map.snr(ff, tt), [-5 5]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'SNR map','GPS ' num2str(stop)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-snr-zoom');

  figure();
  imagesc(t(tt)-start-0.5,f(ff)+0.5,cluster.reconMax(ff, tt), [-5 5]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'SNR map','GPS ' num2str(stop)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'cluster-snr-zoom');

  figure();
  imagesc(t(tt)-start-0.5,f(ff)+0.5,map.naiP1(ff,tt)./map.P1(ff, tt));
  set(gca,'Ydir','normal');
  colorbar;
  title({'autopower map for ifo 1','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir,'map-ap1-zoom');

  figure();
  imagesc(t(tt)-start-0.5,f(ff)+0.5,map.naiP2(ff,tt)./map.P2(ff, tt));
  set(gca,'Ydir','normal');
  colorbar;
  title({'autopower map for ifo 2','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-ap2-zoom');

  figure();
  imagesc(t(tt)-start-0.5,f(ff)+0.5,log10(map.P1(ff, tt)),[-47 -43]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'P map for ifo 1','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-p1-zoom');

  figure();
  imagesc(t(tt)-start-0.5,f(ff)+0.5,log10(map.P2(ff,tt)),[-47 -43]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'P map for ifo 2','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-p2-zoom');


  figure();
  imagesc(t-start-0.5,f+0.5,log10(stoch_out.lonetrack.out1.reconMax),[-47 -43]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'recon Max ifo 1','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-recon-max-1');

  figure();
  imagesc(t(tt)-start-0.5,f(ff)+0.5,log10(stoch_out.lonetrack.out1.reconMax(ff,tt)),[-47 -43]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'recon Max ifo 1','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-recon-max-1-zoom');
  
  figure();
  imagesc(t-start-0.5,f+0.5,log10(stoch_out.lonetrack.out2.reconMax),[-47 -43]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'recon Max ifo 2','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-recon-max-2');

  figure();
  imagesc(t(tt)-start-0.5,f(ff)+0.5,log10(stoch_out.lonetrack.out2.reconMax(ff,tt)),[-47 -43]);
  set(gca,'Ydir','normal');
  colorbar;
  title({'recon Max ifo 2','GPS ' num2str(start)},'Fontsize',15);
  xlabel('relative time [s]','Fontsize',15);
  ylabel('frequency [Hz]','Fontsize',15);
  make_png(dir, 'map-recon-max-2-zoom');

end

