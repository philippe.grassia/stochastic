function [] = targetZebragard_plots2html(doc)  
% plots2html : addplot to a given html doc
% DESCRIPTION :
%    create a table of plot 
%
% SYNTAX :
%   [] = plots2html (doc)
% 
% INPUT : 
%    doc : html.document object
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Mar 2016
%

  d=doc.getElementById('plots');
  doc.createNode('h2','diagnostic plots','Parent',d);
  div = doc.createNode('div','Attribute',struct('class','plots'),'Parent',d);
  t = doc.createNode('table','Parent',div);
  
  % coherent map
  tr =  doc.createNode('tr','Parent',t);
  doc.createNode('th','Coherent map','Attribute',struct('colspan','2'),'Parent',tr);

  tr =  doc.createNode('tr','Parent',t);
  addplot('./map-snr.png',tr);
  addplot('./cluster-snr.png',tr);

  tr =  doc.createNode('tr','Parent',t);
  addplot('./map-snr-zoom.png',tr);
  addplot('./cluster-snr-zoom.png',tr);


  % AP map
  tr =  doc.createNode('tr','Parent',t);
  doc.createNode('th','AP map','Attribute',struct('colspan','2'),'Parent',tr);

  tr =  doc.createNode('tr','Parent',t);
  addplot('./map-ap1.png',tr);
  addplot('./map-ap2.png',tr);

  tr =  doc.createNode('tr','Parent',t);
  addplot('./map-ap1-zoom.png',tr);
  addplot('./map-ap2-zoom.png',tr);


  % P map
  tr =  doc.createNode('tr','Parent',t);
  doc.createNode('th','P map','Attribute',struct('colspan','2'),'Parent',tr);

  tr =  doc.createNode('tr','Parent',t);
  addplot('./map-p1.png',tr);
  addplot('./map-p2.png',tr);

  tr =  doc.createNode('tr','Parent',t);
  addplot('./map-p1-zoom.png',tr);
  addplot('./map-p2-zoom.png',tr);

  function addplot(name,parent)
    td=doc.createNode('td','Parent',parent);
    a=doc.createNode('a','Attribute',...
                     struct('href',name),...
                     'Parent',td);
    doc.createNode('img','Attribute',...
                   struct('src',regexprep(name,'.png','_thumbnail.png')),...
                   'Parent',a);
  end
end

