function [] = plotlist(list,web)  
% plotlist : plot the ftmap of a list of trigger
% DESCRIPTION :
%   this function get the n loudest event on a given
%   run and then run a quick clustermap in order to get stoch_out map
%   and then plot it !
%   In order to increase the time we use paralell computing 
%
% SYNTAX :
%   [] = plotlist ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.2
% DATE : Mar 2016
%
  s=fileparts(mfilename('fullpath'));
  addpath([s '/Zebragard'])
  addpath([s '/Lonetrack'])

  start   = str2num(regexprep(list, '.*Start-([0-9]*)_.*','$1'));
  stop    = str2num(regexprep(list, '.*Stop-([0-9]*)_.*','$1'));
  id      = str2num(regexprep(list, '.*ID-([0-9]*).*','$1'));
  type    = regexprep(list,'.*/(BKG|INJ)/.*', '$1');
  run     = get_run_from_GPStimes(start, stop);
  search  = [lower(run) '-allsky'];
  
  [~,h] = system('hostname -d');
  user=getenv('USER');
  h=h(1:end-1); % remove last char
  if (strcmpi(h,'ligo.caltech.edu') | strcmpi(h,'ligo-wa.caltech.edu') ...
      | strcmpi(h,'ligo-la.caltech.edu'))
    host=['/home/' user '/public_html'];
  elseif strcmpi(h,'atlas.local')
    host=['/home/' user '/WWW/LSC'];
  end

  if nargin==1
    folder = [host '/' search '/Start-' num2str(start) '_Stop-' ...
              num2str(stop) '_ID-' num2str(id) '/background_estimation/'];
  else
    folder = [host '/' web];
  end
  
  %----------------------------------------------------------------
  %% INIT
  %----------------------------------------------------------------
  %
  % check input 
  %
  if ~exist(list)
    error(['cannot access to the file : ' list]);
  end
  
  if ~exist(folder)
    error(['cannot access web to the folder : ' folder]);
  end

  searchPath=regexprep(list,'(.*)/BKG/Start-([0-9]*)_Stop-([0-9]*)_ID-([0-9]*)/.*','$1/BKG/Start-$2_Stop-$3_ID-$4');

  % get data
  triggers = load(list);


  % first remove old FTmap & create a FTMAPS directory
  if exist([folder '/FTMAPS/'])
    system(['rm -r ' folder '/FTMAPS/ftmap_* || exit $?']);
  else
    system(['mkdir -p ' folder '/FTMAPS/']);  
  end


  %----------------------------------------------------------------
  %% MAIN
  %----------------------------------------------------------------
  %
  % loop over the triggers and for each of them plot interresting
  % plot
  %  
  params = targetParams(searchPath);
  for ii=1:size(triggers,1)
    if params.doZebragard
      targetZebragard(searchPath,triggers(ii,:),folder);
    elseif params.doStochtrack
      targetLonetrack(searchPath,triggers(ii,:),folder);
    end
  end


end



