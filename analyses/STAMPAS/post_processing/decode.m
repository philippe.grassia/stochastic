function [varargout] = decode(cut,data)  
% decode : decode cut string
% DESCRIPTION :
%   Create a label,marker & idx associated to a encoded cut
%   eg:
%     SNR:30         ->    SNR >= 30
%     SNRfrac:0.5    ->    SNRfrac <= 30
%     DQFlags+TFveto ->    DQFlags && TFveto
%
% SYNTAX :
%   [label,marker,idx] = decode (cut,data)
% 
% INPUT : 
%    cut: str or cell containing encoded cut
%    data [optional] : data struct, give only if you want the idx output 
%
% OUTPUT :
%    label str or cell containing label associated to the cut
%    marker : and unique marker to caraterise the cut in a plot
%    idx [optional] : only if you give a data as input. return the
%    idx for data that satisfy the cut. 
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Mar 2016
%
  
  if nargout==3&&nargin==1 
    error('you should give a data object to get the cut idx')
  end
  
  if iscell(cut)
    if nargout==3
      f = @(x) decode(x,data);
    else
      f = @(x) decode(x);
    end
      [varargout{1:nargout}]=cellfun(f,cut,'UniformOutput',false);
    return

  end


  if nargin==2
    idx=ones(data.numTriggers,1);  
  end
  label='';
  str=strsplit(cut,'+');
  

  for c=1:numel(str)
    switch str{c}
      
      case 'noCut'
        label  = [label,'Without Cut'];
        marker = '+b';
        if nargin==2
          idx=idx&ones(data.numTriggers,1);
        end

      case 'all'
        label  = [label,'All'];
        marker = '+b';
        if nargin==2
          idx=idx&ones(data.numTriggers,1);
        end
        
      case 'DQFlags'
        label  = [label 'DQFlags'];
        marker = '^g';
        if nargin==2
          idx=idx&(~data.veto.ifo1&~data.veto.ifo2);
        end

      case 'Rveto'
        label  = [label 'Rveto'];
        marker = 'ok';
        if nargin==2
          idx=idx&~(data.veto.Rveto);
        end

      case 'TFveto'
        label  = [label 'TFveto'];
        marker = 'xc';
        if nargin==2
          idx=idx&~(data.veto.TFveto);
        end

      otherwise
        if ~isempty(regexp(str{c},'SNR:[0-9]*','match','once'))
          thr=str2num(regexprep(str{c},'SNR:([0-9]*)','$1'));
          label=[label 'SNR >= ' num2str(thr)];
          marker = '.y';
          if nargin==2
            idx=idx&data.SNR>=thr;
          end

        elseif ~isempty(regexp(str{c},'SNRfrac:[0-9]*','match','once'))
          thr=str2num(regexprep(str{c},'SNRfrac:([0-9]*)','$1'));
          label=[label 'SNRfrac <= ' num2str(thr)];
          marker = 'hr';
          if nargin==2
            if ~isprop(data,'lontrack')
              idx=idx&(data.SNRfrac('ifo1')<=thr&data.SNRfrac('ifo2')<=thr);
              idx=idx&data.SNRfrac('coherent')<=thr;
            else
              idx=idx&data.SNRfrac<=thr;
            end
          end
        end
    end
    
    if c~=numel(str);label=[label ' && ' ];end
  end
  
  varargout{1} = label;
  varargout{2} = marker;
  if nargin==2
    varargout{3} = idx;
  end
end
