Last edited: 12/10/2015
Contact: mabizoua@lal.in2p3.fr, frey@lal.in2p3.fr

***| PURPOSE
The purpose of the extract_triggers scripts and matlab macros 
is to extract from the "results" repertory all triggers, select 
them and save them into a file.

***| USING    

*| for BKG    
just run the mergeTriggersBkg.sh script, you will need the gps
start & end times and also the ID of your run. An optional arugment
is the veto file if you wan't to veto spesific time & frequency of 
your run

*| for INJ
Just run the mergeTriggersInj.sh script, you will need the gps
start & end times and also the ID of your run. An optional arugment
is the veto file if you wan't to veto spesific time & frequency of 
your run


***| INFORMATION
The shell scipt just launch series of .m files to merge and filter file


*| MERGE
Juste merge the results folder into one .mat files. For BKG there is at 
the end only one file : results.mat . For INJ there is one .mat files for 
each wvf & alphas. The triggers in those files are ordered by gps start time
incresing
OUT FILE: 

*| REMOVE DOUBLE
remove double triggers due to overlapping windows, or multiple triggers 
finding. Keep the loudest triggers.
OUT FILE : results_*_NODBL.mat

*| ASSOCIATE INJECTION
for INJ only : associated trigger recovers to an injection made. The 
association are done by computing the overlaping of the injection and
the recover trigger.
OUT FILE : results_*_filtered.mat

*| APPLY DQ FLAGS
apply DQ flags if defined.
add two	 new field on the data struct array : veto.ifo1 & veto.ifo2
OUT FILE : results_*_filtered.mat 

*| APPLY ppS VETO
applys user specific flags (post-processing STAMPAS veto [ppS veto]). 
This step is optional, user can define veto list in the form of 
[tstart tend fmin fmax] all triggers that overlap more than 10% are 
vetoed.
add a new field on the data struct array : veto.ppS
OUT FILE : results_*_filtered.mat 

