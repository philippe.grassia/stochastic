#!/bin/bash  
# wrapper for extract.m matlab function
# ------------------------------------------------------------------
# Syntax:
#   ./extract.sh 
#       run in an interactively mode
#
#   ./extract.sh TYPE START STOP ID [TFveto]
#       TYPE  : BKG or INJ
#       START : GPS start time of the dag
#       STOP  : GPS stop time of the dag
#       ID    : ID of the dag
#       TFveto [OPTIONAL] : TFveto file if use of TFveto
#
#   ./extract.sh DAG [TFveto]
#       DAG : dag path from STAMPAS folder eg: BKG/Start-1126569617_Stop-1129566062_ID-
#       TFveto [OPTIONAL] : TFveto file if use of TFveto
#
# ------------------------------------------------------------------
# Purpose: 
#    Just an interface to launch the extract.m matlab function with 
#    the good parameters. There are several way to use this script:
#
#    - without any argument : an interactive mode are beginning in 
#    order to guide user along the processus. The processus are 
#    divide into several step to define the variable 
#
#    - with argument : then just pass the good argument (see the syntax)
#
# ------------------------------------------------------------------
# author:                         FREY valentin
# EMail Address:                  valentin.frey@gmail.com
# version                         1.0
# Comment:                        
# ------------------------------------------------------------------



# ------------------------------------------------------------------
## USER FUNCTION
# ------------------------------------------------------------------
# Define function used in the following of the code :
#
# usage : just a reminder of how to use this script.
#
# read_input : function that interactively ask to the user the 
#              parameters need for this script.
#
usage(){
    echo 'extract.sh script :'
    echo '* no argument usage'
    echo '  ./extract.sh '
    echo '  follow the intruction'
    echo ''
    echo '* argument usage :'
    echo '  ./extract.sh TYPE GPSstart GPSstop ID [TFveto]'
    echo '      TYPE  : INJ or BKG'
    echo '      start : GPS start time of the dag'
    echo '      stop  : GPS stop time of the dag'
    echo '      id    : ID of the dag'
    echo '      TFveto : [OPTIONAL] file containing the TFveto frequency x time bad region'
    echo ''
    echo '  ./extract.sh DAG [TFveto]'
    echo '      DAG : DAG path eg: BKG/Start-1126569617_Stop-1129566062_ID-1'
    echo '      TFveto : [OPTIONAL] file containing the TFveto frequency x time bad region'
    echo ''
}


read_input(){
    ### choose DAG step
    dagStep=false;
    while [[ $dagStep == 'false' ]];do
	echo -e '\n\e[0;33mCHOOSE A DAG: \e[0m'
	echo -e '\e[0;33m | \e[0m'
        echo -e '\n\e[0;33mINJ : \e[0m'
        ls ../../INJ | awk 'BEGIN{i=0}{i++;printf "\033[0;33m | [%2d]\033[0m INJ/%s\n",i,$1}'
	dag=($(ls ../../INJ | awk '{printf "INJ/%s\n",$0}'))
	echo -e '\e[0;33m | \e[0m'
        echo -e '\n\e[0;33mBKG : \e[0m'
	ls ../../BKG | awk -v x=${#dag[@]} 'BEGIN{i=x}{i++;printf "\033[0;33m | [%2d]\033[0m BKG/%s\n",i,$1}'
	dag+=($(ls ../../BKG | awk '{printf "BKG/%s\n",$0}'))
        echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | > \e[0m")" searchPath
	
	# check input
        check=`ls -d ../../INJ/* ../../BKG/*  | grep ${searchPath} | sed 's|\.\./\.\./||'`
	if [[ ${check} == ${searchPath} ]];then
	    dagStep='true';
	elif [[ searchPath -ge 1 ]] && [[ searchPath -le ${#dag[*]} ]];then
	    searchPath=${dag[searchPath-1]}
	    dagStep='true';
	else
	    echo -e '\e[0;33m | \e[1;31m Bad dag, please write dag from the previous list\e[0m\n'	    
        fi	    
    done
    
    ### Use TFveto step 
    TFvetoStep=false;
    while [[ $TFvetoStep == 'false' ]];do
        echo -e '\n\e[0;33mUSE TFveto (y/n): \e[0m'
	echo -e '\e[0;33m | \e[0m'
        read -p "$(echo -e "\e[0;33m | [n]> \e[0m")" useTFveto
	
	if [[ ${useTFveto} == '' ]] || [[ ${useTFveto} == 'n' ]];then
	    veto='';
	    TFvetoStep='true';
	elif [[ ${useTFveto} == 'y' ]];then
	    TFvetoStep='true';
	else
	    echo -e '\e[0;33m | \e[1;31m Bad answer, please write y or n\e[0m\n'
        fi
    done
    
    ### TFveto file step 
    if [[ ${useTFveto} == 'y' ]];then
	TFvetoFileStep=false;
	while [[ ${TFvetoFileStep} == 'false' ]];do
	    echo -e '\n\e[0;33mChoose TFveto file: \e[0m'
	    echo -e '\e[0;33m | \e[0m'
	    read -p "$(echo -e "\e[0;33m | > \e[0m")" veto
	    
		# check input
	    if [[ -f ${veto} ]];then
		TFvetoFileStep='true';
	    else
		echo -e '\e[0;33m | \e[1;31m file not found, please check the path\e[0m\n'
	    fi
	done
    fi
}


#-------------------------------------------------------
## INPUT PARSER
#-------------------------------------------------------
# switch between the number of argument to get variable
#
if [[ $# -eq 0 ]];then
    usage
    read_input
    type=`echo $searchPath  |sed -r 's/.*(BKG|INJ).*/\1/'`
    start=`echo $searchPath |sed -r 's/.*Start-([0-9]*).*/\1/'`
    stop=`echo $searchPath  |sed -r 's/.*Stop-([0-9]*).*/\1/'`
    id=`echo $searchPath    |sed -r 's/.*ID-([0-9]*).*/\1/'`
    
elif [[ $# -eq 1 ]] || [[ $# -eq 2 ]];then
    searchPath=$1;
    type=`echo $searchPath  |sed -r 's/.*(BKG|INJ).*/\1/'`
    start=`echo $searchPath |sed -r 's/.*Start-([0-9]*).*/\1/'`
    stop=`echo $searchPath  |sed -r 's/.*Stop-([0-9]*).*/\1/'`
    id=`echo $searchPath    |sed -r 's/.*ID-([0-9]*).*/\1/'`
    veto=$2
    if [[ $# -eq 1 ]];then
      veto='';
    fi
elif [[ $# -eq 4 ]] || [[ $# -eq 5 ]];then
    type=$1
    start=$2
    stop=$3
    id=$4
    searchPath=${type}'/Start-'${start}'_Stop-'${stop}'_ID-'${id}
    veto=$5

else
    usage;
fi


#-------------------------------------------------------
## CHECK INPUT
#-------------------------------------------------------
# check the input parse & write the commande in a logfile 
#
if [ ${#start} -lt 9 ]; then
    echo start $start should have 9 or 10 digits
    exit 1
fi

if [ ${#stop} -lt 9 ]; then
    echo stop $stop should have 9 or 10 digits
    exit 1
fi

if [ ! "$id" = "${id%[[:space:]]*}" ]; then
    echo id $id should not have white space
    exit 1
fi

if ! [ -d ../../${searchPath}'/logfiles' ];then
    mkdir -p ../../${searchPath}'/logfiles'
fi
echo $* `date` > ../../${searchPath}'/logfiles/extract.txt'


#-------------------------------------------------------
## MAIN
#-------------------------------------------------------
# launch matlab 
#
matlab -nodisplay<<EOF
   extract('${searchPath}','${veto}');
EOF

exit 0

