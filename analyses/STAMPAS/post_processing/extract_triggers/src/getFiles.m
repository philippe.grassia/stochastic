function files = getFiles(folder,rg)  
% getFiles :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = getFiles ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%
  
  list=dir([folder '/*']);
  list=list(cellfun(@isempty,...                 % remove the current
                    regexp({list.name},...       % directory and the
                           '^\.',...             % parent directory
                           'match')));
  
  % recursive list of files matching the regexp argin
  files={};
  for ff=1:numel(list)
    if list(ff).isdir
      files=[files,getFiles([folder '/' list(ff).name],rg)];
    else
      if regexp(list(ff).name,rg,'match','once')
        files=[files, [folder '/' list(ff).name]];
      end
    end
  end
end

