function Rveto(data)  
% Rveto : compare SNR of each ifo an veto all triggers that have an
% asymetric SNR 
%
% DESCRIPTION :
%   Rveto is using the following cut :
%   ~(log10(SNR1/SNR2) > R1 | log10(SNR2/SNR1) > R2) |
%   (log10(SNR1) > THR1 & log10(SNR2) > THR2)
%   
%   R1   : 0.4
%   R2   : 0.4
%   THR1 : 3
%   THR1 : 3
%
% in the future R1, R2, THR1, THR2 will be give as argument
% 
% SYNTAX :
%   Rveto (data)
% 
% INPUT : 
%    data : classes.data.triggers object
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : May 2016
%
  import classes.utils.waitbar

  %if ~isstruct(data.SNR);return;end

  waitbar('Rveto [init]');

  cut=~(data.SNR('ifo1')./data.SNR('ifo2')>2.9|...
        data.SNR('ifo2')./data.SNR('ifo1')>2.9);
  cut=cut| (log10(data.SNR('ifo1'))>4& ...
            log10(data.SNR('ifo2'))>4);
  data.addFields('veto.Rveto',~cut);

  waitbar.warning(sprintf('Nb triggers vetoed : %d/%d',sum(~cut),data.numTriggers));
  waitbar('Rveto [done]');

  delete(waitbar);
end

