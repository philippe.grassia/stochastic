function veto = get_dqflags(file1,file2)  
% get_dqflags:
% DESCRIPTION :
% 
% SYNTAX :
%   [] = get_dqflags ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

rc1=exist(file1,'file');
rc2=exist(file2,'file');

if rc1+rc2==0
    warning('DQ files are not available');
    veto.ifo1=[];
    veto.ifo2=[];
elseif (rc1==2 & rc2==0) | (rc1==0 & rc2==2)
    error('One DQ file is missing. Please check');
elseif rc1==2 & rc2==2
    veto.ifo1=load(file1);
    veto.ifo2=load(file2);
else
    error('Please check the presence of the DQ files');
end

return

