function remove_double(Data,varargin)  
% remove_double : remove double triggers 
% DESCRIPTION :
%   This script is designed to remove overlapping clusters
%   in a list of triggers.  Duplicate/overlapping clusters
%   may occur due to the use of multiple sky positions in
%   STAMPAS.  Currently, we keep only the cluster with
%   the highest SNR and assume that any clusters that
%   overlap at all are duplicates.
%
% SYNTAX :
%   remove_double (data,option)
% 
% INPUT : 
%   data : dataInterface object
%   key value options  
%      'dt' -- time resolution default 1s
%      'df' -- frequency resolution default 1Hz
%
% AUTHOR : 
%   Tanner Prestegard
%   Valentin FREY (Modified by)
%
% CONTACT : 
%   prestegard@physics.umn.edu
%   valentin.frey@gmail.com
%
% VERSION : 1.2
% DATE : Jan 2016
%
  import classes.utils.waitbar

  waitbar(sprintf('remove double %s','[init]'));
  
  %------------------------------------------------
  %% INPUT PARSER
  %------------------------------------------------
  p=inputParser;
  addRequired(p,'Data',@(x) isa(x,'classes.data.dataInterface'));
  if verLessThan('matlab','8.2')
    addParamValue(p,'dt',1,@isnumeric);
    addParamValue(p,'df',1,@isnumeric);
  else
    addParameter(p,'dt',1,@isnumeric);
    addParameter(p,'df',1,@isnumeric);
  end
  parse(p,Data,varargin{:});

  data       = p.Results.Data;
  segDur     = p.Results.dt;
  df         = p.Results.df;
  prec       = '%.4f';  % set output precision
  frac_param = 0;  % overlap fraction for clusters should be
                   % > frac_param to consider them overlapping.
                   % default: 0.

  %------------------------------------------------
  %% MAIN 
  %------------------------------------------------
  % set up cluster details: 
  % timeshift #, start + end times, freqs, snr.
  % 
  
  clust_data=[ ...
      data.lag, ...
      data.GPSstart, ...
      data.GPSstop, ...
      data.fmin, ...
      data.fmax, ...
      data.SNR, ...
             ];

  % get # of timeslides
  ts = unique(clust_data(:,1));
  for ii=1:numel(ts)
    
    % Progress bar
    waitbar(sprintf(['remove double [%3.0f]'],100*ii/numel(ts)));
    
    c1=find(clust_data(:,1) == ts(ii));
    sub_data = clust_data(clust_data(:,1) == ts(ii),:);

    % j1 = cluster being compared to
    % f1 = matching clusters found
    [j1,f1,frac] = deal([]);
    for jj=1:(size(sub_data,1)-1)
      gS = sub_data(jj,2);
      gE = sub_data(jj,3);
      fS = sub_data(jj,4);
      fE = sub_data(jj,5);
      % compare gps times to find any clusters that possibly overlap
      c2 = find(sub_data((jj+1):end,3) >= gS &...
                sub_data((jj+1):end,2) <= gE );
      % adjust c2 to account for sub data set positions
      c2 = c2 + jj;

      % calculate percent overlap
      olap_t_length = (min(sub_data(c2,3),gE) - ...
                       max(sub_data(c2,2),gS))/(segDur/2) + 1;
      olap_f_length=(min(sub_data(c2,5),fE) - ...
                     max(sub_data(c2,4),fS))/df + 1;
      orig_area = ((gE-gS)/(segDur/2)+1)*((fE-fS)/df+1);
      
      f = (olap_t_length .* olap_f_length)/orig_area;
      frac=[frac;max(0,f)];

      j1 = [j1; ones(size(c2))*jj];
      f1 = [f1; c2];
    end
    % remove overlapping clusters (defined above)
    idx = find(frac > frac_param);
    j1 = j1(idx);
    f1 = f1(idx);
    
    % set up snrs
    snr1 = sub_data(j1,6);
    snr2 = sub_data(f1,6);
    
    % decide which cluster to remove
    % currently just keeping cluster with highest SNR
    idx2 = find(snr1 >= snr2);
    r1 = j1;
    r1(idx2) = f1(idx2);
    
    % remove items
    data.removeElements(c1(unique(r1)));
    clust_data(c1(unique(r1)),:) = [];
  end

  % display end message and close waitbar
  waitbar(sprintf('remove double %s', '[done]'));
  delete(waitbar);
end



