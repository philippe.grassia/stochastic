function merge_lonetrack(searchPath)

% pproc_lonetrackpproc :
% DESCRIPTION : merge zerolag lonetrack files. It applies the
% threshold on the single ifo autopower that is used to compute the
% background reaching 5 sigma. Thresholds are stored in stage1.mat
% 
% SYNTAX :
%   [] = pproc_lonetrackpproc ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Michael Coughlin, Marie Anne Bizouard
% CONTACT : 
% VERSION :
% DATE : 
%

import classes.utils.waitbar
import classes.utils.logfile

logfile([searchPath '/logfiles/merge_lonetrack.log']);
logfile('merge_lonetrack begin ...');
fprintf('merge_lonetrack begin ...');

%% Get config
config = readConfig([searchPath '/config_bkg.txt']);

if config.lonetrackNSlides > 0
  error('lonetrackNSlides is not null')
end

%% Folders and files
jobfile = [searchPath '/tmp/joblistTrue.txt'];
jobs = load(jobfile);
T = length(jobs);

zerolonetrackPath = [searchPath '/Lonetrack/'];
zerobkndoutputDir = [zerolonetrackPath '/bknd'];
save_dirs = dir([zerolonetrackPath '/lonetrack_*']);

% Get thresholds
try
  stage1 = load([zerobkndoutputDir '/stage1.mat']);
  th1 = stage1.th1;
  th2 = stage1.th2;
catch;
  error('No Stage 1 file...\n');
  th1=0;
  th2=0;
end

%% Init
snrmax_zero = [];
snrtimes_zero = [];
timedelay_zero = [];
pass_zero   = [];
AP=[];

% FAR histogram
snr_overflow=30;
edges=0:0.01:snr_overflow;
snrmax_zero_histo=zeros(1,length(edges));

% Triggers identification
pn=[+1,-1];
id=[];
lag=[];


% loop over output files
for i = 1:length(save_dirs)
  save_dir = [zerolonetrackPath '/' save_dirs(i).name];
  for aa=1:T
    if aa == 1 || mod(aa,1000)==1
      fprintf('Job number: %d / %d\n',aa,T);
    end
    % load data
    try
      filename = [save_dir '/lonetrack_' num2str(jobs(aa,2)) '.mat'];
      r = load(filename);

      pass1 = double(r.stoch_out.lonetrack.out1.snr_gamma > th1);
      pass2 = double(r.stoch_out.lonetrack.out2.snr_gamma > th2);
      pass=max([pass1;pass2]);

      AP = [AP;[r.stoch_out.lonetrack.out1.snr_gamma,...
                  r.stoch_out.lonetrack.out2.snr_gamma]];

      if pass1==1 && pass2==0
      	snrmax = r.stoch_out.lonetrack.snrmax1;
      	idx = 1;
	timedelay = r.stoch_out.lonetrack.timedelay1;
      elseif pass1==0 && pass2==1
      	snrmax = r.stoch_out.lonetrack.snrmax2;
      	idx = 2;
	timedelay = r.stoch_out.lonetrack.timedelay2;
      else
      	[snrmax,idx] = max([r.stoch_out.lonetrack.snrmax1; r.stoch_out.lonetrack.snrmax2]);
	if idx == 1
	  timedelay = r.stoch_out.lonetrack.timedelay1;
	else
	  timedelay = r.stoch_out.lonetrack.timedelay2;
	end
      end
      
      snrtimes_zero = [snrtimes_zero aa];
      % halftrack results
      snrmax_zero = [snrmax_zero snrmax];
      timedelay_zero = [timedelay_zero timedelay];
      pass_zero = [pass_zero pass];

      id = [id pn(idx)*jobs(aa,2)];
      lag = [lag; 0 0];
    catch ME
      ME.message
      error('Error during parsing files')
    end
  end
end

indexes = find(pass_zero);

% snrmax histogram
[snrmax_histo,xxx]=histc(snrmax_zero,edges);
[snrmax_zero_histo,xxx]=histc(snrmax_zero(indexes),edges);

save([zerobkndoutputDir '/stage1_triggers.mat'],'snrmax_zero', ...
     'timedelay_zero','snrtimes_zero', 'pass_zero', 'id', 'lag','AP','th1','th2');
save([zerobkndoutputDir '/stage1_far.mat'],'snrmax_zero_histo','edges');


return;
