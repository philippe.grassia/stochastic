function algo = get_algo(file)  
% get_algo :
% DESCRIPTION :
% 
% SYNTAX :
%   algo = get_algo (file)
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION : 1.0
% DATE : Fev 2016
%
fid=fopen(file,'r');
l=fgetl(fid);

while ischar(l)
    tf=regexprep(l,'doZebra ([0-9]*)','$1');
    if ~isempty(tf)
        if (str2num(tf)==1)
            algo='zebragard';
        else
            algo='lonetack';
        end
        break
    end
    l=fgetl(fid);
end
fclose(fid);

return

