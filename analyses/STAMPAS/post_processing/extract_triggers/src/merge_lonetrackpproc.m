function merge_lonetrackpproc(searchPath)

% pproc_lonetrackpproc :
% DESCRIPTION : merge 5 sigma background lonetrack files. The
% threshold applied on the single ifo autopower to reach 5 sigmas
% are stored in stage1.mat

% 
% SYNTAX :
%   [] = pproc_lonetrackpproc ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Michael Coughlin, Marie Anne Bizouard
% CONTACT : 
% VERSION :
% DATE : 
%

import classes.utils.waitbar
import classes.utils.logfile

logfile([searchPath '/logfiles/merge_lonetrackpproc.log']);
logfile('merge_lonetrackpproc begin ...');

%% Get config
config = readConfig([searchPath '/config_bkg.txt']);

%% Folders and files
jobfile = [searchPath '/tmp/joblistTrue.txt'];
jobs = load(jobfile);
T = size(jobs,1);

lonetrackPath = [searchPath '/LonetrackPProc/'];
bkndoutputDir = [lonetrackPath '/bknd'];
save_dirs = dir([lonetrackPath '/lonetrack_*']); 

system(['mkdir -p ' bkndoutputDir]);

%% Init
snrmax = []; 
initp = [];
snrtimes = [];
timedelay = [];
gpstime = [];

snrth = -1;
snrperc = 0.90;

% For identification
pn=[+1,-1];
id=[];
lag=[];
LAG = repelem(1:T-1,floor((config.window*2-1)/2));
MINI_LAG = repmat(0:floor((config.window*2-1)/2)-1,1,T-1);

% FAR histogram
snr_overflow=30;
edges=0:0.01:snr_overflow;
snrmax_histo=zeros(1,length(edges));

% loop over out files
ii=0;
for i = 1:length(save_dirs)
  save_dir = [lonetrackPath '/' save_dirs(i).name];

  for aa=1:T
    if aa == 1 || mod(aa,1000)==1
      fprintf('Job number: %d / %d. Triggers #: %d\n',aa,T,length(snrmax));
      if aa>1 && sum(snrmax_histo)==0
	error('snrmax_histo is empty after 1000 loops. Please check')
      end
    end
    % load data
    try
      filename = [save_dir '/lonetrack_' num2str(jobs(aa,2)) '.mat'];
      q = load(filename);
      ii=ii+1;

      % halftrack results
      if snrth == -1
	tmp = sort(q.snrmax);
	tmp = tmp(~isnan(tmp));
	snrth = tmp(floor(length(tmp)*snrperc));
	fprintf('Using SNR threshold: %.2f\n',snrth);
	logfile(['Using SNR threshold: ' num2str(snrth)]);
      end

      if max(q.snrmax) > snr_overflow
       	error(['File ' filename ...
       	       ' contains a SNR larger than ' ...
	       num2str(snr_overflow) ...
       	       '. Please modify the histogram boundary to ' ...
	       num2str(max(q.snrmax))])
      end
     
      % halftrack results
      snrmax = [snrmax q.snrmax(q.snrmax>=snrth)];
      snrtimes = [snrtimes q.mapindex(q.snrmax>=snrth)];
      timedelay = [timedelay q.timedelay(q.snrmax>=snrth)];
      gpstime = [gpstime q.gpstime(q.snrmax>=snrth)];
      lag=[lag;q.mapindex(q.snrmax>=snrth)',q.lagindex(q.snrmax>=snrth)'];

      % identifier for mapping stage 1 - stage 2 triggers
      % for ifo 1 id = +GPS time,
      % for ifo 2 id = -GPS time,
      % need to improve for 3 ifo ...
      id = [id pn(q.idx(q.snrmax>=snrth))*jobs(aa,2)];

      % starting p-values calculation
      initp(ii) = str2num(config.lonetrackFAP)*sum(q.snrmax>=snrth)/length(q.snrmax);

      % snrmax histogram
      [n,xxx]=histc(q.snrmax,edges);
      snrmax_histo=snrmax_histo+n;
    catch ME
      ME.message
      error(['Error during processing triggers']);
    end
  end
end

save([bkndoutputDir '/stage2_triggers.mat'],'snrmax','snrtimes','timedelay','gpstime','initp','id','lag','-v7.3');
save([bkndoutputDir '/stage2_far.mat'],'snrmax_histo','edges','-v7.3');

return;
