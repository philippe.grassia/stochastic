function [dt,df] = get_resolution(file)  
% get_resolution :
% DESCRIPTION :
% 
% SYNTAX :
%   [] = get_resolution ()
% 
% INPUT : 
%    
% OUTPUT :
%
% AUTHOR : Valentin FREY    
% CONTACT : valentin.frey@gmail.com
% VERSION :
% DATE : 
%

fid=fopen(file,'r');
l=fgetl(fid);

while ischar(l) 
    if regexp(l,'^%','once'),l=fgetl(fid);continue;end
    if regexp(l,'deltaF ([0-9]*)')
        df=str2num(regexprep(l,'deltaF (.*)','$1'));
    elseif regexp(l,'segmentDuration ([0-9]*)')
        dt=str2num(regexprep(l,'segmentDuration (.*)','$1'));
    end
    l=fgetl(fid);
end

fclose(fid);
return

