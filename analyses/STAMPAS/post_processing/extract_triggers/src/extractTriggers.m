classdef extractTriggers<controller
% extractTriggers : extract the triggers of a given run
%
% DESCRIPTION : 
%   Application that represent a STAMPAS search. One
%   can apply all the extractTriggers process just by launch the
%   exec methods. Another way is to run a specific process just 
%   by calling methods independently.
%   For more help about some methods just type 
% >> help extractTriggers.methods  
% 
% SYNTAX :
%   obj=extractTriggers(searchPath)
% 
% INPUT : 
%   searchPath : the path of the run. This path can be relative
%                from the extractTriggers directory or relative
%                from the STAMPAS directory. 
%         e.g : BKG/Start-1127174417_Stop-1127779217_ID-1
%         e.g : ../../BKG/Start-1127174417_Stop-1127779217_ID-1
%    
% PROPERTIES : 
%   searchPath
%   type
%   m_path
%   start
%   stop
%   run
%   vetoFile
%   files
%   params
%   data
%   injections
%
% For more help about a specific properties, just type
% >> help extractTriggers.properties
%
% METHODS : 
%   merge
%   removeDouble
%   associateInjection
%   applyDQFlags
%   apply_ppS_veto
%   exec
%
% For more help about a specific methods, just type
% >> help extractTriggers.methods
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Nov 2015
%
    properties(Access=public)
        searchPath % the search Path
        type       % the type of the search
        m_path     % the results_merged path
        start      % GPS start time of the search
        stop       % GPS stop time of the search
        run        % the run name of the search
        vetoFile=''; % define vetoFile for the ppS veto 
        files={};    % the files contained in the search
        params       % struct containing the injection parameters
    end
    
    methods
        function obj=extractTriggers(searchPath)  
        % contructor of extractTriggers application
        %
        % SYNTAX :
        %   obj=extractTriggers(searchPath)
        % 
        % INPUT : 
        %   searchPath : the path of the search folder
        % 
            obj@controller('extract_triggers');
            
            %%% fill the field  
            obj.searchPath = searchPath;
            obj.type       = obj.searchPath;
            obj.params     = obj.searchPath;
            obj.m_path     = [obj.searchPath ...
                              '/results/results_merged'];
            
            %%% get results file name
            if strcmp(obj.type, 'BKG')
                obj.files={'results'};
                
            elseif strcmp(obj.type, 'INJ')
                [~,a]=system(['grep nullWaveform ' obj.searchPath ...
                              '/config_inj.txt | awk ''{print $2}''']);
                
                if str2num(a)==1
                    obj.files={'results/results_noWaveform'};
                end
                for wvf=1:obj.params.nb_wvfs
                    for al=1:numel(obj.params.alphas{wvf})
                        injName= ...
                            char(strcat(obj.params.waveform_names(wvf),'_', ...
                                        num2str(obj.params ...
                                                .alphas{wvf}(al),'%0.6f')));
                        obj.files=[obj.files, ['results/results_' injName]];
                    end
                end
                
            else
                error([ type 'must be INJ or BKG']);
            end
            
            if numel(obj.files)==0
                error(['No files for ' searchPath '/results/result_*' ...
                       'found. Stop here'])
            end
        end

    end
    
    methods
        %%| Protected methods : cannot be call by user directly
        %%| use the public methods (interface) to use it
        
        function v=init(h,obj)
        %%% init
        %%| this methods just return true  
        %%|
        %%| This methods should not be run directly by a user
        %%| 
            mergeController().init(h);
            v=true;
        end
        
        function main(h,obj)
        %%% main
        %%| function use for the exec methods. apply the main
        %%| methods of each process and save the data in a .mat
        %%| files at each step 
        %%|
        %%| This methods should not be run directly by a user
        %%| 
            
            %%% merge triggers into one files
            hh=mergeController();
            hh.fill(h);
            hh.main(h)
            h.data=hh.data;
            h.injections=hh.injections;
            %%| save in a _merged.mat file
            hh=saveController('_merged');
            hh.fill(h);
            hh.main(h);
            
            %%% remove double 
            hh=removeDoubleController();
            hh.fill(h);
            hh.main(h);
            h.data=hh.data;
            %%| save in a _NODBL.mat
            hh=saveController('_NODBL');
            hh.fill(h);
            hh.main(h);

            %%% associate injections 
            if strcmp(h.type,'INJ')
                hh=associateInjectionsController();
                hh.fill(h,obj);
                hh.main(h)
                h.data=hh.data;
                %%| save in a _filtered.mat
                hh=saveController('_filtered');
                hh.fill(h);
                hh.main(h);
            end

            %%% apply DQ flags
            hh=DQFlagsController();
            hh.fill(h);
            hh.main(h)
            h.data=hh.data;
            %%| save in a _filtered.mat
            hh=saveController('_filtered');
            hh.fill(h);
            hh.main(h);
                        
            %%% apply ppS veto
            if strcmp(h.type,'BKG')&~isempty(h.vetoFile)
                hh=ppSvetoController();
                hh.fill(h);
                hh.main(h)
                h.data=hh.data;
                %%| save in a _filtered.mat
                hh=saveController('_filtered');
                hh.fill(h);
                hh.main(h);
            end
        end

        function launch(obj,h)
        %%% launch:
        %%| Main methods for the extractTriggers process. take a
        %%| process object as argument. run over the files and 
        %%| apply the main methods of the given process object
        %%| 
        %%| This methods should not be run directly by a user
        %%|
            display([h.name ' ...']);
            
            %%% set logfile    
            logfile(obj.searchPath, [h.name '.log']);
            
            if h.init(obj)
                for ii=1:numel(obj.files)
                    %%% get the injections of the current file
                    if ~isempty(obj.injections) & ...
                            numel(obj.injections)==numel(obj.files)
                        h.injections=obj.injections{ii};
                    end
                    
                    %%% get the data of the current files
                    if numel(obj.data)==numel(obj.files)
                        h.data=obj.data{ii};
                    end
                    
                    %%% get the current file
                    h.currentFile=obj.files{ii};
            
                    %%% run the process
                    h.main(obj);

                    %%% store data in the extractTriggers obj
                    obj.data{ii}=h.data;
                    obj.injections{ii}=h.injections;
                end
            end
            obj.close();
        end             

    end

    methods(Access = public)
        %%| user interface methods 

        function close(obj)
        % close the application, if offline the exit matlab
        % 
        % SYNTAX :
        %   extracTriggers.close
        %
            OFFLINE=str2num(getenv('OFFLINE'));
            if OFFLINE==1
                exit
            end
        end

        function clean(obj)
        % clean the application :
        %   remove the results_merged directory
        %   clear the data properties
        %   clear the injections properties
        %
        % SYNTAX :
        %   extracTriggers.clean
        %
            system(['rm -rf  ' obj.m_path]);
            obj.data=[];
            obj.injections=[];
        end

        function merge(obj)
        % merge the results files from all results folder
        % into one .mat file
        %
        % DESCRIPTION :
        %   Load every data struct array in the results files and
        %   append it into a global data_merged struct array then
        %   sort all triggers wrt to the H1 GPS time.
        %   For injection merge also the injections_*.mat file
        %   Then fill the data & injections field og the
        %   extractTriggers obj
        %
        % SYNTAX :
        %   extracTriggers.merge()
        %
        % AUTHOR : 
        %   Tanner Prestegard
        %   Valentin FREY (modified by) 
        %   
        % CONTACT :
        %   prestegard@physics.umn.edu 
        %   frey@lal.in2p3.fr
        %
            obj.launch(mergeController());
        end
        
        function removeDouble(obj)
        % remove Double triggers dur to overlapping 
        %
        % DESCRIPTION :
        %   This script is designed to remove overlapping clusters
        %   in a list of triggers.  Duplicate/overlapping clusters
        %   may occur due to the use of multiple sky positions in
        %   STAMPAS.  Currently, we keep only the cluster with
        %   the highest SNR and assume that any clusters that
        %   overlap at all are duplicates.
        %
        % SYNTAX :
        %   extracTriggers.removeDouble()
        %
        % AUTHOR : 
        %   Tanner Prestegard
        %   Valentin FREY (modified by) 
        %   
        % CONTACT :
        %   prestegard@physics.umn.edu 
        %   frey@lal.in2p3.fr
        %    
            obj.launch(removeDoubleController());
        end
        
        function associateInjections(obj)
        % associate any trigger to an injections 
        %
        % DESCRIPTION :
        %   The script tries to associate an injection to a trigger
        %   Output files are stored in _filtered.txt files
        %
        % SYNTAX :
        %   extracTriggers.associateInjection()
        %
        % AUTHOR : 
        % SYNTAX :
        %   extracTriggers.associateInjection()
        %
        %   M. A. Bizouard
        %   Valentin FREY (modified by) 
        %
        % CONTACT :
        %   mabzoua@lal.in2p3.fr
        %   frey@lal.in2p3.fr
        % 
            obj.launch(associateInjectionsController());
        end
        
        function apply_DQFlags(obj)
        % apply the DQ flags 
        % 
        % DESCRIPTION :
        %   This script reads the filtered triggers, apply each IFO
        %   DQ vetoes and fills the field veto.flags of the results
        %   struct array
        %                                                                              
        % SYNTAX :
        %   extracTriggers.applyDQFlags()
        %
        % AUTHOR :
        %   M. A. Bizouard
        %   Valentin FREY (modified by)
        %
        % CONTACT:
        %   mabizoua@lal.in2p3.fr
        %   frey@lal.in2p3.fr
        %
            obj.launch(DQFlagsController());
        end

        function apply_ppS_veto(obj)
        % apply_STAMPAS_veto : apply some veto define in a text
        % file 
        %
        % DESCRIPTION :
        %   Veto define in a .txt file in the format of 
        %   [tmin tmax fmin fmax]. If more than 10% of the triggers
        %   is on the veto zone rectangle, then flag the triggers. 
        % 
        % SYNTAX :
        %   extracTriggers.apply_STAMPAS_veto()
        % 
        % AUTHOR : Valentin FREY    
        % CONTACT : valentin.frey@gmail.com
        % VERSION : 1.1
        % DATE : Oct 2015
        %
            obj.launch(ppSvetoController());
        end

        function apply_glitchCut(obj)
        % apply_glitchCut : flag triggers when the glitch cut is
        % activated
        %
        % DESCRIPTION :
        %    check that triggers do not overlap glitch cut
        %    times. If there
        %    is some overlap then flag the trigger
        %
        % SYNTAX :
        %   extracTriggers.apply_glitchCut
        % 
        % AUTHOR : Valentin FREY    
        % CONTACT : frey@lal.in2p3.fr
        % VERSION : 1.0
        % DATE : Nov 2015
        %        
            obj.launch(glitchCutController());
        end
        
        function save(obj,suffix)
        % save the data & injections field of the extractTriggers
        % obj. 
        % 
        % DESCRIPTION :
        % by default extractTriggers application does not save data
        % after each process. this methods allow to save the
        % current data store on the data & injections field.
        % Data are save in a the results/results_meged directory
        % under a file named results|injection_*suffix.mat
        %
        % SYNTAX ;
        %   extracTriggers.apply_glitchCut(suffix)
        %
        % INPUT :
        %  suffix : suffix of the file saved 
        %  
        % AUTHOR : Valentin FREY    
        % CONTACT : frey@lal.in2p3.fr
        % VERSION : 1.0
        % DATE : Nov 2015
        %
            obj.launch(saveController(suffix));
        end            

        function exec(obj)
        % run all extractTriggers process 
        % 
        % DESCRIPTION :
        %   apply all the extract triggers process one after the
        %   other. The process are the following:
        %      merge
        %      removeDouble
        %      associateInjection (INJ)
        %      apply_DQFlags
        %      apply_glitchCut
        %      apply_ppS_veto (BKG)
        %   This methods has been created to automatise the
        %   extractTriggers process. The process can be run
        %   independently of this methods
        %   to see more details about the process apply in this
        %   methods, just type 
        %   >> help extractTriggers.methods
        %
        % SYNTAX ;
        %   extracTriggers.exec()
        %  
        % AUTHOR : Valentin FREY    
        % CONTACT : frey@lal.in2p3.fr
        % VERSION : 1.0
        % DATE : Nov 2015
        % 
            obj.launch(extractTriggers(obj.searchPath));
        end
    end

    %%% SETTER METHODS %%%        
    methods
        function obj=set.searchPath(obj,v)
            v=strrep(v,'../../', '');
            obj.searchPath=['../../' v];
        end

        function obj=set.type(obj,v)
            v=strrep(v,'../../', '');
            v=regexprep(v,'(BKG|INJ)/.*', '$1');
            if ~strcmp(v, 'BKG') & ~strcmp(v,'INJ')
                error(['type need to be BKG or INJ\n please enter a ' ...
                       'correct searchPath']);
            end
            obj.type=v;
        end

    end

    %%% GETTER METHODS %%%
    methods     
        function v=get.start(obj)
            if isempty(obj.start)
                v=regexprep(obj.searchPath, '.*Start-([0-9]*)_.*','$1');
                if ~isnumeric(v)
                    v=str2num(v);
                end                
                obj.start=v;
            end
            v=obj.start;
        end
        
        function v=get.stop(obj)
            if isempty(obj.stop)
                v=regexprep(obj.searchPath, '.*Stop-([0-9]*)_.*','$1');
                if ~isnumeric(v)
                    v=str2num(v);
                end   
                obj.stop=v;
            end
            v=obj.start;
        end
        
        function v=get.run(obj)
            v=get_run_from_GPStimes(obj.start, obj.stop);
        end

        function obj=set.params(obj,v)
            if strcmp(obj.type,'INJ')
                v=strrep(v,'../../', '');
                obj.params=get_waveform_parameters(v);
            end
        end
        
    end
end
