#! /usr/bin/env bash

GPS_START=931081116
GPS_END=971614699
ID=1
IFO1="H"
IFO2="L"
RUN="S6"

loudest_evts_nb=100
filename_prefix=S6_TS100_100
results='results_S6_TS100_100'

echo 'extracting loudest triggers ...'

cat ../../BKG/Start-${GPS_START}_Stop-${GPS_END}_ID-${ID}/results/results_merged/results_bkg_filtered*.txt | sort  -k6 -g -r | awk -v len=${loudest_evts_nb} '{if (NR<len) print $0}' > ${filename_prefix}.txt
