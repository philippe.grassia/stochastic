#!/usr/bin/env bash

if [ $# != 1 ]; then
    echo 'Missing parameters:'
    echo '1: loud events list file (2 columns L/H GPS)'
    exit
fi

loudest_file=$1
evts_nb=`wc -l ${loudest_file} | awk '{print $1}'`

for i in `seq 1 ${evts_nb}`
do
    IFO=`cat ${loudest_file} | head -$i | tail -1 | cut -d ' ' -f 1 `
    time=`cat ${loudest_file} | head -$i | tail -1 | cut -d ' ' -f 2 `
    echo $i ' ' $IFO ' ' $time

    if [ -f ${IFO}1_scandone.txt ]; then
	rc=`grep $time H1_scandone.txt`
	if [ ${#rc} -gt 0 ]; then
	    echo 'wscan for time ' $time ' already done'
	else
	    ${PWD}/launchOmegaScan.sh ${IFO} $time $time p
	    echo $time >> ${IFO}1_scandone.txt
	fi
    else
	${PWD}/launchOmegaScan.sh ${IFO} $time $time p
	echo $time >> ${IFO}1_scandone.txt
    fi

    

done

