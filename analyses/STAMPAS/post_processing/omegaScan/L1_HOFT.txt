# Q Scan configuration file
# Automatically generated with wconfigure.sh
# by user bhughey on 2016-10-30 21:30:25 CDT
# from sample frame files:
#   /archive/frames/postO1/raw/L1/L-L1_R-11619/L-L1_R-1161915776-64.gwf

[Context,Context]

[Parameters,Parameter Estimation]

[Notes,Notes]

[L1:CAL,L1 calibrated]

{
  channelName:                 'L1:GDS-CALIB_STRAIN'
  frameType:                   'L1_HOFT_C00'
  sampleFrequency:             4096
  searchTimeRange:             64
  searchFrequencyRange:        [0 Inf]
  searchQRange:                [4 96]
  searchMaximumEnergyLoss:     0.2
  whiteNoiseFalseRate:         1e-3
  searchWindowDuration:        0.5
  plotTimeRanges:              [1 4 16]
  plotFrequencyRange:          []
  plotNormalizedEnergyRange:   [0 25.5]
  alwaysPlotFlag:              1
}

