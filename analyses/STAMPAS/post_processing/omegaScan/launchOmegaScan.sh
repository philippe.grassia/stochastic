#!/usr/bin/env bash

if [ $# != 5 ]; then
    echo 'Missing parameters:'
    echo '1: IFO [H1/L1]'
    echo '2: GPS time'
    echo '3: folder name'
    echo '4: rough [r] or precise [p]'
    echo '5: tree name'
    exit
fi

IFO=$1

if [ $4 == "r" ]; then
    ext="_rough"
    folder_ext="/rough"
else
    ext=""
    folder_ext="/"$1
fi

source /cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/etc/profile.d/conda.sh
conda activate ligo-py37
gwdetchar-omega -i ${IFO} $2 -o /home/${USER}/public_html/$5/OMEGA${folder_ext}/$3

