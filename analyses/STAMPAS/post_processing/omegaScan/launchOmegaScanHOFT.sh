#!/usr/bin/env bash

if [ $# != 4 ]; then
    echo 'Missing parameters:'
    echo '1: IFO [H/L]'
    echo '2: GPS time'
    echo '3: folder name'
    echo '4: tree name'
    exit
fi

IFO=$1
echo 'folder:' $4

source /cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/etc/profile.d/conda.sh
conda activate ligo-py37
gwdetchar-omega -i ${IFO} $2 -o /home/${USER}/public_html/$5/OMEGA_HOFT${folder_ext}/$3 -f ${IFO}_HOFT.txt

