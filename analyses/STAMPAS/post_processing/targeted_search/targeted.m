function targeted(searchPathBKG,searchPathZL,outputPath,gpsStart,gpsEnd)

outputPath = sprintf('%s/%d-%d',outputPath,gpsStart,gpsEnd);
if ~exist(outputPath)
   system(['mkdir -p ' outputPath]);
end

jobs=load([searchPathBKG '/tmp/joblistTrue.txt']);

BkgTotTime=load([searchPathBKG '/tmp/TTime.txt']);
ZLTotTime=load([searchPathBKG '/tmp/TTimeZeroLag.txt']);

%% Lonetrack data
%------------------------------------------------------------------
lonetrackPath = [searchPathZL '/Lonetrack'];
trigger_filename=[lonetrackPath '/bknd/stage1_triggers.mat'];
zl = load(trigger_filename);

% FAR histogram
snr_overflow=30;
edges=0:0.01:snr_overflow;
% snrmax histogram
indexes = find(zl.pass_zero);
zlgps = jobs(zl.snrtimes_zero,2);
%indexes2 = find(gpsStart <= zlgps & zlgps <= gpsEnd);
%indexes = intersect(indexes,indexes2);
[snrmax_histo,xxx]=histc(zl.snrmax_zero(indexes),edges);
indexes2 = find(gpsStart <= zlgps & zlgps <= gpsEnd);
indexes = intersect(indexes,indexes2);
zlsnrslice = zl.snrmax_zero(indexes);

N_cum=cumsum(snrmax_histo);
N_cum_max=N_cum(end);
for i=1:length(N_cum)
  N_cum_inv(i,1)=N_cum_max-N_cum(i);
end

idx = find(gpsStart <= jobs(:,2) & jobs(:,3) <= gpsEnd);
dGPS = sum(jobs(idx,4));

zlfar=N_cum_inv/ZLTotTime;
zlfar=zlfar*(ZLTotTime/dGPS);
zlsnr=edges';

lonetrackPath = [searchPathBKG '/LonetrackPProc/'];
snrhisto_filename=[lonetrackPath '/bknd/stage2_far.mat'];
dataBKG = load(snrhisto_filename);

N_cum=cumsum(dataBKG.snrmax_histo);
N_cum_max=N_cum(end);
for i=1:length(N_cum)
  N_cum_inv(i,1)=N_cum_max-N_cum(i);
end

bkgfar=N_cum_inv/BkgTotTime;
bkgfar=bkgfar*(ZLTotTime/dGPS);
bkgsnr=dataBKG.edges';

zlfarslice = interp1(bkgsnr,bkgfar,zlsnrslice);

% define 5-sigma line and 4sigma line
s5 = 1/1744278/dGPS;
s4 = 1/15787/dGPS;
s3 = 1/370/dGPS;

xmin = 1e-2; xmax = 15;
ymin = 1e-15; ymax = 1;

figure;
ax=gca();
hg=hggroup;

ho=[];
leg={};
nsall=bkgsnr(bkgsnr<xmax&bkgsnr>xmin);
nfall=min(max(bkgfar(bkgsnr<xmax&bkgsnr>xmin),ymin),ymax);
h=bar(nsall,nfall, ...
       'FaceColor', [.7 .7 .7], ...
       'EdgeColor', [.7 .7 .7], ...
       'BarWidth', 1, ...
       'basevalue', ymin,'Parent',ax);
hold on
ho=[ho,h];
dx=nsall(2)-nsall(1);
stairs(nsall-dx/2,nfall, 'Color','k','Parent',ax)
leg=[leg,'background'];
h=plot(zlsnr-dx/2,zlfar,'*r');
ho=[ho,h];
leg=[leg,'Zero Lag'];
h=plot(zlsnrslice-dx/2,zlfarslice,'+g');
ho=[ho,h];
leg=[leg,'Zero Lag Slice'];
h = loglog([xmin xmax], [1 1]*s5, 'k--','LineWidth',3);
loglog([xmin xmax], [1 1]*s4, 'k--','LineWidth',3);
loglog([xmin xmax], [1 1]*s3, 'k--','LineWidth',3);
ho=[ho,h];
leg=[leg,'3,4,5-sigma'];
hold off
leg1 = legend(ho,leg);
set(leg1,'Location','SouthWest');

set(ax,'XLim',[xmin xmax]);
set(ax,'YLim',[ymin ymax]);
set(ax,'XScale','log');
set(ax,'YScale','log');
set(ax,'XGrid','on');
set(ax,'YGrid','on');
set(get(ax,'XLabel'),'String','SNR');
set(get(ax,'YLabel'),'String','FAR [Hz]');
set(ax,'Box','on');

print('-dpng',[outputPath '/FAR_ZL.png']);
print('-depsc2',[outputPath '/FAR_ZL.eps']);
print('-dpdf',[outputPath '/FAR_ZL.pdf']);
close;

