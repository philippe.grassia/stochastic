#! /usr/bin/env bash

# creates executbale which returns svnversion of archive
# (c) 2008 Gareth Jones <gareth.jones@astro.cf.ac.uk>
# Licensed under GPL v3 or later

usage() {
    echo "Usage: $(basename $0) <STAMP2_PATH> <SEARCH_PATH> <DAG_NAME>"
}

if [ $# -lt 2 ]; then
    usage
    exit 0
fi

stamp2_path=$1
shift
search_path=$1
shift

if [ $# == 1 ]; then
    dag_name=_$1
    shift
else
    dag_name=""
fi

script_name=get_svnversion${dag_name}.sh
local_path=`echo $PWD`
search_path=${local_path}/${search_path}
output_name=svnversion${dag_name}.txt

if [ -f ${search_path}/${output_name} ]; then
    rm ${search_path}/${output_name}
fi

svn info . | awk 'BEGIN{ORS=""}/URL/{print $2 " "}' > ${search_path}/${output_name}
svnversion . >> ${search_path}/${output_name}

if [ `awk '{if( $2 ~ /[^[:digit:]]/) print $2}' ${search_path}/${output_name}` ]; then 
    echo WARNING: svn version is mixed, this is not ideal for analysis runs
    echo WARNING: using \"svn update\" to update the svn archive
fi

cd ${stamp2_path}
svn info . | awk 'BEGIN{ORS=""}/URL/{print $2 " "}' >> ${search_path}/${output_name}
svnversion . >> ${search_path}/${output_name}

#if [ `awk '{if( $2 ~ /[^[:digit:]]/) print $2}' ${search_path}/${output_name}` ]; then 
#    echo WARNING: svn version is mixed, this is not ideal for analysis runs
#    echo WARNING: using \"svn update\" to update the svn archive
#fi

#echo `cat ${search_path}/${output_name}`

cat ${search_path}/${output_name} | awk '{print "echo " $0}' > ${search_path}/${script_name}
chmod +x ${search_path}/${script_name}


