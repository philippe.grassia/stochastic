function [fmin,fmax,duration]=get_waveform_information(waveform)
%
% Function to determine the fmin,fmax,duration of a waveform. 
% In case of a "folder" type waveform, it assumes that all
% waveforms in the same folder has the same duration
%
%
% Valentin FREY
%

% The waveform is actually a folder

if ~isempty(regexp(waveform,'.*/$'))
  temp = dir([waveform]);
  temp = regexpi({temp.name},'(.*\.dat.*|.*\.txt.*)', 'tokens','once');
  temp = vertcat(temp{:});

  fid=fopen([waveform_path '/' waveform temp{1}],'r');
  fgetl(fid);                    % skip first line
  info=strsplit(fgetl(fid),' '); % get fmin,fmax
  fgetl(fid); fgetl(fid);        % skip two line
  ht=fscanf(fid,'%f',[3 Inf])';  % get ht
  fclose(fid);

  duration = ht(end,1)-ht(1,1);
  fmin     = num2str(info{2});
  fmax     = num2str(info{3});
  
elseif findstr(waveform, 'onthefly')
  ht=load(waveform);
  duration=ht.duration;
  fmin=ht.fmin;
elseif findstr(waveform, 'inspiral')
  ht=load(waveform);
  duration=ht.duration;
  fmin=ht.fmin;
  fmax=ht.fmax;
elseif findstr(waveform, 'gw170817')
  ht=load(waveform);
  duration=ht.duration;
  fmin=ht.fmin;
  fmax=ht.fmax;
elseif findstr(waveform, 'msmagnetar')
  ht=load(waveform);
  duration=ht.duration;
  fmin=ht.fmin;
  fmax=ht.f0;
elseif findstr(waveform, 'magnetar')
  ht=load(waveform);
  duration=ht.duration;
  fmin=ht.fmin;
  fmax=ht.fmax;
elseif findstr(waveform, 'sinusoid')
  ht=load(waveform);
  duration=ht.duration;
  fmin=ht.fmin;
  fmax=ht.fmax;
elseif findstr(waveform, 'rmodes')
  ht=load(waveform);
  duration=ht.duration;
  fmin=ht.fmin;
  fmax=ht.fmax;
else

  % Get file extension
  [~,~,ext] = fileparts(waveform);

  % .mat file
  if strcmpi(ext,'.mat')
    % Injection mat files should contain a struct called 'inj'
    % which has 3 members: t, hp, hx.
    load(waveform);
    try
      t = inj.t; hp = inj.hp; hx = inj.hx; fmin = inj.fmin; fmax = inj.fmax;
    catch
      error('The injection .mat file is not set up correctly.');
    end
    duration = t(end) - t(1);
  else
    fid=fopen(waveform,'r');
    fgetl(fid);                    % skip first line
    info=strsplit(fgetl(fid),' '); % get fmin,fmax
    fgetl(fid); fgetl(fid);        % skip two line
    ht=fscanf(fid,'%f',[3 Inf])';  % get ht
    fclose(fid);

    duration = ht(end,1)-ht(1,1);
    fmin     = num2str(info{2});
    fmax     = num2str(info{3});
  end
end


