function LISTLite = shorten_jobs_list_overlap(LIST,duration,overlap,segDur,fixed_duration,add_group)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function shorten_jobs_list_overlap()
%
% This functions creates, from a source joblist, a joblist with jobs 
% of duration *duration*. An overlap between the jobs can be included.
% If *fixed_duration* is set to 0, jobs longer than *duration* but not
% long enough to be splited into two overlapping jobs are kept unique.
% If *fixed_duration* is set to 1, their duration is shorten to *duration*
% If add_group is 1, a fifth column is added in the file, identifying 
% all the jobs issued from a single job of the original joblist. 
%
%Written by: Samuel Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if ~isnumeric(duration) | ~isnumeric(overlap)
   error('Duration of jobs or overlap must be a number')
end

if overlap>=duration
   error('Overlap duration greater than jobs duration')
end

if fixed_duration~=0 & fixed_duration~=1
   display('Warning: Duration of jobs is fixed by default')
   fixed_duration=1;
end

LISTLite=[];
group=1;
job=1;

gpss = LIST(1,2):(duration/2):LIST(end,3);

ii = 1;
for i=1:length(gpss)-2 %% loop on each science segment

  gpsStart = gpss(i); gpsEnd = gpss(i+2);

  indexes = find(LIST(:,3) < gpsStart | LIST(:,2) > gpsEnd);
  indexes = setdiff(1:length(LIST(:,4)),indexes);
   
  indexes

  ttseg = gpsStart:gpsEnd;
  pass = zeros(size(ttseg));
  winduration = 0;
  for index = indexes
     thisGPSStart = LIST(index,2); thisGPSEnd = LIST(index,3);
     passseg = find(ttseg >= thisGPSStart & ttseg <= thisGPSEnd);
     pass(passseg) = 1;
  end
  winduration = sum(pass); 
 
  if winduration >= duration/2
     LISTLite=[LISTLite; ii gpsStart gpsEnd winduration ii];
     ii = ii + 1;
  end
end


