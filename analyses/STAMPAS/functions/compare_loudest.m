function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry,memoryJob,doGPU,lonetrackFAP)
% function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry)
%
% Writes a Condor .dag file for background studies (time-slides and zero-lag).
%
% Inputs:
%   searchPath - path to search directory
%   NShifts    - number of time-slides to do
%   idxLag     - 0 if doing zero-lag, 1 if not.
%   TOffset    - offset for time-slides (ex: set to 100 to do
%                100 time-slides starting at 101 - 200 instead of 1-100).
%   denseDAG   - if true, combine multiple science segments into a condor job.
%   nbGroupsPerJob - number of science segments per condor job.
%   retry      - number of times to retry condor job if it fails.
%   doGPU      - adds GPU condor options if needed
%
%  Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

searchPaths = {'/home/mcoughlin/STAMP/O1/C02/250_NoCut/STAMPAS/BKG/Start-1126073342_Stop-1137283217_ID-1/','/home/mcoughlin/STAMP/O1/C02/250_NoCut_Xi/STAMPAS/BKG/Start-1126073342_Stop-1137283217_ID-1/','/home/mcoughlin/STAMP/O1/C02/250_NoCut_Xi_6bands/STAMPAS/BKG/Start-1126073342_Stop-1137283217_ID-1/','/home/mcoughlin/STAMP/O1/C02/250/STAMPAS/BKG/Start-1126073342_Stop-1137283217_ID-1/','/home/mcoughlin/STAMP/O1/C02/250_HotPixel_Xi_6bands/STAMPAS/BKG/Start-1126073342_Stop-1137283217_ID-1/'}; 
lonetrackFAP = 0.01;
data = [];

plotDir = '/home/mcoughlin/STAMP/O1/C02/250_Compare/plots';
system(['mkdir -p ' plotDir]);

for ii = 1:length(searchPaths)
   searchPath = searchPaths{ii};

   jobfile = [searchPath '/tmp/joblistTrue.txt'];
   jobs = load(jobfile);
   T = length(jobs);

   lonetrackPath = [searchPath '/Lonetrack/'];
   bkndoutputDir = [lonetrackPath '/bknd'];
   system(['mkdir -p ' bkndoutputDir]); 
   save_dirs = dir([lonetrackPath '/lonetrack_*']); 

   H1 = load([bkndoutputDir '/h1_trig.txt']);
   L1 = load([bkndoutputDir '/l1_trig.txt']);

   T = length(H1);

   % prepare for probability plots
   dp = 1/T;
   pvals = 1 : -dp : dp;

   data(ii).H1 = sort(H1(:,3));
   data(ii).L1 = sort(L1(:,3));
   data(ii).pvals = pvals;
end

figure;
loglog(data(1).H1, data(1).pvals);
hold all
for ii = 2:length(data)
   loglog(data(ii).H1, data(ii).pvals);
end
hold off
grid on;
xlabel('SNR_{tot, max}');
ylabel('FAP');
xlim([1 1000]);
ax = gca;
ax.YTick = [1e-5 1e-4 1e-3 1e-2 1e-1 1e0];
legend_names = {'0.15','0.027','0.027 - 6 bands','0.15 - Hot Pixel','0.027 - Hot Pixel'};
leg1 = legend(legend_names);
set(leg1,'Location','NorthEast');
pretty;
grid on;
print('-dpng',[plotDir '/H1_lsnr_htrack']);
print('-depsc2',[plotDir '/H1_lsnr_htrack']);
close;

figure;
loglog(data(1).L1, data(1).pvals);
hold all
for ii = 2:length(data)
   loglog(data(ii).L1, data(ii).pvals);
end
hold off
grid on;
xlabel('SNR_{tot, max}');
ylabel('FAP');
xlim([1 1000]);
ax = gca;
ax.YTick = [1e-5 1e-4 1e-3 1e-2 1e-1 1e0];
legend_names = {'0.15','0.027','0.027-6 bands','0.15 - Hot Pixel','0.027 - Hot Pixel'};
leg1 = legend(legend_names);
set(leg1,'Location','NorthEast');
pretty;
grid on;
print('-dpng',[plotDir '/L1_lsnr_htrack']);
print('-depsc2',[plotDir '/L1_lsnr_htrack']);
close;

return;
