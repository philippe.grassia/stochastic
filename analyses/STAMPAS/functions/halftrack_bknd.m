function [snrmax,SNRfrac,timedelay,gpstimes] = halftrack_bknd(params, lonetrackFile, det)
% function [snrmax] = halftrack_bknd_v3(job1, job2, det)
% job1 = raw data job
% job2 = halftrack job

% load lonetrack triggers
r=load(lonetrackFile);
if det == 1
   params.anteproc.jobNum1 = r.stoch_out.params.anteproc.jobNum1;
else
   params.anteproc.jobNum2 = r.stoch_out.params.anteproc.jobNum2;
end

[matlist,params] = find_anteproc_mats(params);
[map1, params1] = load_anteproc_mats(matlist,params,1);
[map2, params2] = load_anteproc_mats(matlist,params,2);
[params, map1, map2] = check_anteproc_data(map1,map2,params1,params2, ...
                                               params);
% Set segstarttimes to be equal (may need to be adjusted due to
% time-shifts).
map2.segstarttime = map1.segstarttime;
[map, params, pp] = combine_anteproc_mats(map1,map2,params);
clear map1 map2 params1 params2;

% apply mask
if params.doStampFreqMask
  map = maskmaps(map, params);
end

% initialize ccSpec struct and other radiometer variables
% eht: Note that cc and ccSpec are not identical in preproc--they differ by a 
% factor of Q.  The following definition of ccSpec yields the correct estimator
% for map.y downstream, but use caution comparing with variables in preproc.
ccSpec.data = map.cc;
ccSpec.flow = params.fmin;
ccSpec.deltaF = pp.deltaF;
sensInt.flow = params.fmin;
sensInt.deltaF = pp.deltaF;
sensInt.data = map.sensInt;
ccVar = map.ccVar;
midGPSTimes = map.segstarttime + params.segmentDuration/2;
[det1 det2] = ifo2dets(params);
source = [0 0];

kk = 1;
map = freshmap(map);
% run the radiometer; calculate ft-maps
[map] = ccSpecReadout_stamp(det1, det2, ...
    midGPSTimes, source(kk,:), ccSpec, ccVar, sensInt, params, pp, map);
map.snr = map.y ./ map.sigma;
% calculate Xi stastic
params.stochtrack.stochsky = 1;
params.glitch.doCut = true;
params.glitch.numBands = 6;
params.glitch.doCoincidentCut = false;
params.glitch.Xi_frac = 0.027;
map = calXi(params, map);
[map, cutcols, detcuts, xi_values] = glitchCut(params, map);

% Any cuts on map.snr (from glitch cut or frequency notches) also apply to naiP1 and naiP2
map.naiP1(isnan(map.snr)) = NaN;
map.naiP2(isnan(map.snr)) = NaN;
map.fft1(isnan(map.snr)) = NaN;
map.fft2(isnan(map.snr)) = NaN;

% load lonetrack triggers
r=load(lonetrackFile);

% prepare for shifting with respect to cluster 1
out1A = r.stoch_out.lonetrack.out1;
recon1A = ceil(out1A.reconMax);
list1A = find(recon1A==1);
out2A.reconMax = recon1A;
% frequency map
[M,N] = size(out1A.reconMax);
f = [out1A.params.fmin : out1A.deltaF : out1A.params.fmax]';
fmap = repmat(f, 1, N);
out1A.fvals=fmap(list1A);  out2A.fvals=fmap(list1A);
out1A.NE = NullEnergy(map,out2A.reconMax>0,1);

% prepare for shifting with respect to cluster 2
out2B = r.stoch_out.lonetrack.out2;
recon2B = ceil(out2B.reconMax);
list2B = find(recon2B==1);
out1B.reconMax = recon2B;
out1B.fvals=fmap(list2B);  out2B.fvals=fmap(list2B);
% copy over metadata
out1B.pp = out1A.pp;
out1B.params = out1A.params;
out1B.deltaF = out1A.deltaF;
out1B.NE = NullEnergy(map,out1B.reconMax>0,1);

% loop over time-shifts (shift by two segments to avoid overlap effect)
nshifts = floor((length(map.segstarttime)-1)/2);
nsegs = length(map.segstarttime);

snrmax = [];
SNRfrac = []; 
timedelay = [];
gpstimes = [];

for ii=1:nshifts
  % shift by two overlapping segments: the first element is zero-lag
  % the map wraps back on itself
  jj=2*ii;
%  shftidx = [[jj-1:nsegs] [1:jj-2]];
%  fft1=map.fft1(:,shftidx);  fft2=map.fft2(:,shftidx);
%  P1=map.P1(:,shftidx);  P2=map.P2(:,shftidx);
  % look for LHO cluster in LLO data
%  out2A.fft2 = fft2(list1A);
%  out2A.P2 = P2(list1A);
%  snrmax1(ii) = csnr(params, out1A, out2A);
  % look for LLO cluster in LHO data
%  out1B.fft1 = fft1(list2B);
%  out1B.P1 = P1(list2B);
%  Snrmax2(ii) = csnr(params, out1B, out2B);
  % instead, use efficient shifting
  if det==1
    list1A_sh = mod(list1A(:) + (jj-2)*M - 1, M*N) + 1;
    out2A.fft2 = map.fft2(list1A_sh);
    out2A.P2 = map.P2(list1A_sh);
    [snrmax(ii), snr0, SNRfrac(ii), timedelay(ii)] = csnr_v3(params, out1A, out2A, N);
  elseif det==2
  % look for LLO cluster in LHO data
    list2B_sh = mod(list2B(:) + (jj-2)*M - 1, M*N) + 1;
    out1B.fft1 = map.fft1(list2B_sh);
    out1B.P1 = map.P1(list2B_sh);
    [snrmax(ii), snr0, SNRfrac(ii), timedelay(ii)] = csnr_v3(params, out1B, out2B, N);
   else
     error('unexpected value: det');
   end
   if r.stoch_out.params.doOverlap  
      gpstimes(ii) = r.stoch_out.params.startGPS + (r.stoch_out.params.segmentDuration/2)*jj;
   else
      gpstimes(ii) = r.stoch_out.params.startGPS + (r.stoch_out.params.segmentDuration)*jj;
   end
end

return
