function plot_waveforms

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

outputDir = './plots/';

if ~exist(outputDir)
   system(['mkdir ' outputDir])
end

waveforms = {'vlm_A','vlm_B','vlm_C','vlm_D','vlm_E','vlm_F'};
waveforms = {'vlm_E','vlm_F'};

data = [];
legend_labels = {};
for ii = 1:length(waveforms)
   file = ['waveforms/' waveforms{ii} '.dat'];
   data_out = load(file);
   data(ii).tt = data_out(:,1);
   data(ii).hp = data_out(:,2);
   data(ii).hc = data_out(:,3);
   legend_labels{ii} = strrep(waveforms{ii},'vlm_','');
end

figure;
ii = 1;
plot(data(ii).tt,data(ii).hp)
hold all
for ii = 2:length(data)
   plot(data(ii).tt,data(ii).hp)
end
hold off
legend(legend_labels);
xlabel('Time [s]');
ylabel('Strain');
print('-dpng',[outputDir '/timeseries.png'])
print('-depsc2',[outputDir '/timeseries.eps'])
print('-dpdf',[outputDir '/timeseries.pdf'])
close;

figure;
ii = 1;
plot(data(ii).tt,data(ii).hp)
hold all
for ii = 2:length(data)
   plot(data(ii).tt,data(ii).hp)
end
hold off
legend(legend_labels);
xlim([-60 0]);
xlabel('Time [s]');
ylabel('Strain');
print('-dpng',[outputDir '/timeseries_zoom.png'])
print('-depsc2',[outputDir '/timeseries_zoom.eps'])
print('-dpdf',[outputDir '/timeseries_zoom.pdf'])
close;

