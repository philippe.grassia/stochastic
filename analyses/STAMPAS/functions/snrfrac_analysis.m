function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry,memoryJob,doGPU,lonetrackFAP)
% function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry)
%
% Writes a Condor .dag file for background studies (time-slides and zero-lag).
%
% Inputs:
%   searchPath - path to search directory
%   NShifts    - number of time-slides to do
%   idxLag     - 0 if doing zero-lag, 1 if not.
%   TOffset    - offset for time-slides (ex: set to 100 to do
%                100 time-slides starting at 101 - 200 instead of 1-100).
%   denseDAG   - if true, combine multiple science segments into a condor job.
%   nbGroupsPerJob - number of science segments per condor job.
%   retry      - number of times to retry condor job if it fails.
%   doGPU      - adds GPU condor options if needed
%
%  Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);


plotDir = sprintf('%s/plots',searchPath);
if ~exist(plotDir)
   system(['mkdir -p ' plotDir]);
end


resultsPath = [searchPath '/results/results_merged/'];
results_files = dir([resultsPath '/results_*_filtered.mat']); 

injfiles = {};
alphas = [];
% loop over out files
ii=0;
for i = 1:length(results_files)
  filename = [resultsPath '/' results_files(i).name];
  filenameSplit = regexp(filename,'/','split');
  filenameEnd = filenameSplit{end};
  filenameEnd = strrep(filenameEnd,'results_','');
  filenameSplit = regexp(filenameEnd,'_','split');
  injfile = strjoin(filenameSplit(1:end-2),'_');

  ii = ii + 1;
  injfiles{ii} = injfile;
  alpha = str2num(filenameSplit{end-1});
  alphas = [alphas alpha];
end
[~,idx] = sort(alphas,'ascend');
results_files = results_files(idx);

injfiles = unique(injfiles);

for j = 1:length(injfiles)
  plotname = strrep(injfiles{j},'.dat','');

  figure;
  set(gcf, 'PaperSize',[8 6])
  set(gcf, 'PaperPosition', [0 0 8 6])
  hold on

  for i = 1:length(results_files)
    filename = [resultsPath '/' results_files(i).name];
    filenameSplit = regexp(filename,'/','split');
    filenameEnd = filenameSplit{end};
    filenameEnd = strrep(filenameEnd,'results_','');
    filenameSplit = regexp(filenameEnd,'_','split');
    injfile = strjoin(filenameSplit(1:end-2),'_');

    if ~strcmp(injfiles{j},injfile)
      continue
    end

    alpha = str2num(filenameSplit{end-1});
    q = load(filename);
    SNR = q.data.SNR;
    SNRfrac = q.data.SNRfrac;

    C = log10(alpha) * ones(size(SNR));
    scatter(SNR,SNRfrac,20,C,'filled');
    fprintf('%s %10.5f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f\n',injfile,alpha,min(SNR),median(SNR),max(SNR),min(SNRfrac),median(SNRfrac),max(SNRfrac));

  end
  hold off
  grid on;
  xlabel('SNR');
  ylabel('SNR frac');
  xlim([0 100]);
  cbar = colorbar();
  ylabel(cbar,'log10(alpha)');
  pretty;
  print('-dpng',[plotDir '/' plotname]);
  print('-depsc2',[plotDir '/' plotname]);
  print('-dpdf',[plotDir '/' plotname]);
  close;

end

return;
