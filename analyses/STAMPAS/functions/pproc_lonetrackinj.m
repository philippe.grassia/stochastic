function pproc_lonetrackpproc(searchPath,bkndFile)
% function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry)
%
% Writes a Condor .dag file for background studies (time-slides and zero-lag).
%
% Inputs:
%   searchPath - path to search directory
%   NShifts    - number of time-slides to do
%   idxLag     - 0 if doing zero-lag, 1 if not.
%   TOffset    - offset for time-slides (ex: set to 100 to do
%                100 time-slides starting at 101 - 200 instead of 1-100).
%   denseDAG   - if true, combine multiple science segments into a condor job.
%   nbGroupsPerJob - number of science segments per condor job.
%   retry      - number of times to retry condor job if it fails.
%   doGPU      - adds GPU condor options if needed
%
%  Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

try
   bknd = load(bkndFile);
   th1 = bknd.stage1.th1;
   th2 = bknd.stage1.th2; 
   %th1 = 0; th2 = 0;
catch;
   fprintf('No background file...\n');
end

lonetrackPath = [searchPath '/results/'];
save_dirs = dir([lonetrackPath '/results_*']); 
injoutputDir = [searchPath '/LonetrackPProc/inj'];
system(['mkdir -p ' injoutputDir]);

waveforms = {};
alphas = [];
snrmax = []; 
lsnr1 = [];
snrmax1 = [];
lsnr2 = [];
snrmax2 = [];

ii = 1;
% loop over out files
for i = 1:length(save_dirs)
  save_dir = [lonetrackPath '/' save_dirs(i).name];
  matfiles = dir([save_dir '/*.txt']);
  for aa=1:length(matfiles)
    fprintf('Job number: %d\n',aa);
    % load data
    try
      filename = [save_dir '/' matfiles(aa).name];
      q = load(filename);
      filenameSplit = regexp(filename,'/','split');
      folder = filenameSplit{end-1};
      folder = strrep(folder,'results_','');
      folder = strrep(folder,'.dat',''); 
      folder = strrep(folder,'.mat','');     
      folderSplit = regexp(folder,'_','split');

      waveform = strjoin(folderSplit(1:end-1),'_');
      alpha = str2num(folderSplit{end});

      if strcmp(waveform,'')
         continue
      end

      waveforms{ii} = waveform;
      alphas(ii) = alpha;

      indexes1 = find(q(:,18) >= th1);
      indexes2 = find(q(:,20) >= th2);

      if isempty(indexes1)
         snrmax1(ii) = 0; lsnr1(ii) = 0;
      else
         snrmax1(ii) = max(q(indexes1,17)); lsnr1(ii) = max(q(indexes1,18));
      end

      if isempty(indexes2)
         snrmax2(ii) = 0; lsnr2(ii) = 0;
      else
         snrmax2(ii) = max(q(indexes2,19)); lsnr2(ii) = max(q(indexes2,20));
      end

      snrmax(ii) = max([snrmax1(ii) snrmax2(ii)]);
      ii = ii + 1; 

    catch
    end
  end
end

fprintf('\n\n');

waveforms_unique = unique(waveforms);
for i = 1:length(waveforms_unique)
  waveform = waveforms_unique{i};
  indexes = find(strcmpi(waveform,waveforms));

  if findstr(waveform,'onthefly')
    filename=sprintf('waveforms/%s.mat',waveform)
    data_out = load(filename);
    hrss(i) = data_out.h0;
    phi0 = data_out.phi0;
    f0 = data_out.f0;
    fdot = data_out.fdot;
    duration(i) = data_out.duration;
    distance(i) = 0;  
  
    f1 = f0 + fdot * duration(i);

    fmin(i) = min([f0 f1]); fmax(i) = max([f0 f1]);
  else
    filename=sprintf('waveforms/%s.dat',waveform);
    [void,s_fmin]=system(['head -2 ' char(filename) ' | awk ''{print $2}'' ']);
    [void,s_fmax]=system(['head -2 ' char(filename) ' | awk ''{print $3}'' ']);
    [void,s_distance]=system(['head -2 ' char(filename) ' | awk ''{print $4}'' ']);
    [void,s_hrss]=system(['head -2 ' char(filename) ' | awk ''{print $5}'' ']);

    if strfind(s_fmin,'fmin')
      fmin(i)=str2num(s_fmin(5:end));
    end
    if strfind(s_fmax,'fmax')
      fmax(i)=str2num(s_fmax(5:end));
    end
    if strfind(s_distance,'dist');
      distance(i)=str2num(s_distance(10:end));
    end
    if strfind(s_hrss,'hrss')
      hrss(i)=str2num(s_hrss(5:end));
    end

    [void,s_duration]=system(['tail -1 ' char(filename) ' | cut -d " " -f 1']);
    tmp=str2num(s_duration);
    duration(i)=tmp(1);
  end

   alphasWaveform = alphas(indexes); 
   snrmaxWaveform = snrmax(indexes);
   snrmax1Waveform = snrmax1(indexes);
   snrmax2Waveform = snrmax2(indexes);
   lsnr1Waveform = lsnr1(indexes);
   lsnr2Waveform = lsnr2(indexes);

   alphas_unique = unique(alphasWaveform);
   snrbar = [];

   for j = 1:length(alphas_unique)
      indexes = find(alphasWaveform == alphas_unique(j));
      snrs = snrmaxWaveform(indexes);
      snrmax1s = snrmax1Waveform(indexes);
      snrmax2s = snrmax2Waveform(indexes);
      lsnr1s = lsnr1Waveform(indexes);
      lsnr2s = lsnr2Waveform(indexes);

      minSNR = min(snrs);
      medianSNR = median(snrs);
      maxSNR = max(snrs);
      snrbar = [snrbar medianSNR];
      %fprintf('%s %.3e %.2f %.2f %.2f\n',waveform,alphas_unique(j),minSNR,medianSNR,maxSNR);
   end
   fprintf('\n\n');

   if distance(i) == 0
       [alphas_unique,idx] = sort(alphas_unique,'descend');
       snrbar = snrbar(idx);
       D = fliplr(hrss(i)./sqrt(alphas_unique));
   else
       [alphas_unique,idx] = sort(alphas_unique,'descend');
       snrbar = snrbar(idx);
       D = distance(i)./sqrt(alphas_unique);
   end

   D
   snrbar

   try
      bknd;
   catch
      continue
   end
 
   plotDir = [injoutputDir '/' waveform];
   system(['mkdir -p ' plotDir]);

   if isnan(bknd.th5)
      fprintf('5 sigma not available... using max(snr) instead...\n');
      bknd.th5 = max(bknd.snrmax);
   end

   % apply 50% FDP
   D3_index = max(find(snrbar>bknd.th3));
   D4_index = max(find(snrbar>bknd.th4));
   D5_index = max(find(snrbar>bknd.th5));

   D3 = D(D3_index);
   D4 = D(D4_index);
   D5 = D(D5_index);
   fprintf('Waveform %s: Detection distances (3sig, 4sig, 5sig)=(%.5e, %.5e, %.5e)\n', ...
      waveform,D3, D4, D5);

   cmin = min(D);
   cmax = max(D);

   %ColorSet = varycolor(length(D));
   ColorSet = jet(length(D));
   DsAll = floor(min(D)):1:ceil(max(D));
   ColorSet = jet(length(DsAll));

   xmax = max([max(bknd.snrmax_sort) snrbar(D3_index) snrbar(D4_index) snrbar(D5_index)]) + 1;

   % diagnostic plots: snrmax1
   figure;
   semilogy(bknd.snrmax_sortx(bknd.snrmax_sortx<bknd.snrth), bknd.pvalsx(bknd.snrmax_sortx<bknd.snrth) ...
     *(sum(~isnan(bknd.q.snrmax))/length(bknd.q.snrmax)),'LineWidth',3);
   hold on;
   semilogy(bknd.snrmax_sort, mean(bknd.initp)*bknd.pvals,'LineWidth',3);
   hold on
   semilogy([0 xmax], [1 1]/bknd.s5, 'k--','LineWidth',3);
   semilogy([0 xmax], [1 1]/bknd.s4, 'k--','LineWidth',3);
   semilogy([0 xmax], [1 1]/bknd.s3, 'k--','LineWidth',3);
   for kk=1:length(D)
      [junk,index] = min(abs(DsAll-D(kk)));
      semilogy(snrbar(kk)*[1 1], [1e-12 1], 'Color', ColorSet(index,:),'LineWidth',3);
   end
   hold off
   axis([0 xmax 1e-12 1]);
   grid on;
   xlabel('\Lambda');
   ylabel('FAP');
   c = colorbar;
   ylabel(c,'log10(Alpha)','FontSize',20);
   caxis([cmin cmax])
   pretty;
   print('-dpng',[plotDir '/snrmax_htrack_bknd_combine']);
   print('-depsc2',[plotDir '/snrmax_htrack_bknd_combine']);
   close;

end

return;

