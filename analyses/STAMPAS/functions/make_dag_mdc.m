function make_dag_mdc(GPS_start_inj, GPS_end_inj, ID_inj, ...
		      GPS_start_bkg, GPS_end_bkg, ID_bkg, ...
		      retry, mem)

%
% This function generates a dag file for STAMPAS computing the
% parameters of the injections performed in a given dag
%
% Contact: Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%


mem=strassign(mem);
retry=strassign(retry);
  
searchPath=generate_searchPath('INJ',GPS_start_inj,GPS_end_inj,ID_inj);

fid=fopen([searchPath '/stamp_allsky_mdc.dag'],'w+');
njobs=0;

waveforms_file=[searchPath '/waveforms.txt'];
if exist(waveforms_file) ~= 2
  error(['The file ' waveforms_file ' is not found.']);
  return;
end

% Get the list of waveforms
B=readtext(waveforms_file, ' ', '%', '', 'textual');
    
% If no waveforms are found, exit
if size(B,1)==0
  error(['No waveform found in ' waveforms_file]);
  return;
end

wvf_nb=size(B,1);
alpha_nb=size(B,2)-1;

for j=1:wvf_nb
  for i=1:alpha_nb
    njobs=njobs+1;
    fprintf(fid,'JOB %d stamp_allsky_mdc.sub\n',njobs);
    fprintf(fid,'RETRY %d %d\n',njobs,retry);
    fprintf(fid,'VARS %d jobnumb="%d" mem="%d" searchPath="%s" GPS_start_inj="%s" GPS_end_inj="%s" ID_inj="%s" waveform_idx="%d" alpha_idx="%d" GPS_start_bkg="%s" GPS_end_bkg="%s" ID_bkg="%s" ',...
	    njobs,njobs,mem,searchPath, ...
	    GPS_start_inj, GPS_end_inj, ID_inj, ...
	    j,i,GPS_start_bkg, GPS_end_bkg, ID_bkg);
    fprintf(fid,'\n\n');
  end
end


   
fclose(fid);
