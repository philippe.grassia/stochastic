function compute_zerolag_livetime (searchPath, list_of_jobs, window)

%
% Zero-lag lifetime calculation
%

njobs = length(list_of_jobs(:,5));
timeTotZL = njobs*window;

display(['Total zero-lag lifetime: ' num2str(timeTotZL) ' (s)']);
dlmwrite([searchPath '/tmp/TTimeZeroLag.txt'],timeTotZL,'precision','%.0f');
DaysZL = timeTotZL/86400;
display(['Total zero-lag lifetime: ' num2str(DaysZL) ' (days)']);

