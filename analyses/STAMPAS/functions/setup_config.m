function setup_config(configFile)

% function setup_config()
%
% This script generates the files needed to perform an all-sky analysis: dag,
% joblists for injection, background and zero lag
%
% Samuel Franco (franco@lal.in2p3.fr)
% Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%

% Get configFile values
[names,values]=textread(configFile, '%s %s\n',-1,'commentstyle','matlab');

for ii=1:length(names)
  switch names{ii}
   case 'inj'
    inj = str2num(values{ii});
    
   case 'ID'
    ID = str2num(values{ii}); 
    
   case 'Hostname'
    Hostname = values{ii};
    hostname = Hostname(1:6);
    
   case 'pair'
    parameters.pair = values{ii};      
    pair = values{ii};
    
   case 'cachePath'
    parameters.cachePath = values{ii};      
    
   case 'doZebra'
    doZebra = str2num(values{ii});

   case 'doSeedless'
    doSeedless = str2num(values{ii});
    
   case 'doSeedlessAllsky'
    doSeedlessAllsky = str2num(values{ii});

   case 'seedlessParams'
    seedlessParams = values{ii};

   case 'doLonetrack'
    doLonetrack = str2num(values{ii});

   case 'doLonetrackPProc'
    doLonetrackPProc = str2num(values{ii});

   case 'lonetrackFAP'
    lonetrackFAP = str2num(values{ii});

   case 'lonetrackNSlides'
    lonetrackNSlides = str2num(values{ii});

   case 'doParallel'
    doParallel = str2num(values{ii});

   case 'doGPU'
    doGPU = str2num(values{ii});

   case 'jobMemory'
    jobMemory = str2num(values{ii});

   case 'doDgPlots'
    doDgPlots = str2num(values{ii});
    
   case 'doMC'
    parameters.doDetectorNoiseSim = str2num(values{ii});
    
   case 'DetectorNoiseFileLIGO'
    parameters.DetectorNoiseFileLIGO = values{ii};
    
   case 'DetectorNoiseFileVirgo'
    parameters.DetectorNoiseFileVirgo = values{ii};
    
   case 'GPS_start'
    GPS_start = str2num(values{ii});
    
   case 'GPS_end'
    GPS_end = str2num(values{ii});
    
   case 'window'
    window = str2num(values{ii});
    parameters.window = window;
    
   case 'fmin'            
    fmin = str2num(values{ii});
    
   case 'fmax'
    fmax = str2num(values{ii});
    
   case 'NTShifts'
    NTShifts = str2num(values{ii});
    
   case 'TShiftInj'
    TShiftInj = str2num(values{ii});
    
   case 'TSOffset'
    TSOffset = str2num(values{ii});
    
   case 'overlap'
    overlap = str2num(values{ii});
    
   case 'doStampFreqMask'
    doStampFreqMask = str2num(values{ii});
   
   case 'FrequenciesToNotch'
    freqs = values{ii};
    
   case 'alternative_sigma'
    altSigma = str2num(values{ii});
    
   case 'doZeroLag'
    doZeroLag = str2num(values{ii});
    
   case 'NumberOfInjections'
    NumberOfInjections = str2num(values{ii});

   case 'injectionSeparation'
    injectionSeparation = str2num(values{ii});

   case 'blindZone'
    blindZone = str2num(values{ii});
    
   case 'nullWaveform'
    nullWaveform = str2num(values{ii});
    
   case 'waveforms'
    % Get the list of waveforms
    B=readtext(values{ii}, ' ', '%', '', 'textual');
    
    % If no waveforms are found, exit
    if size(B,1)==0
      error(['No waveform found in ' searchPath 'waveforms.txt']);
      return
    end
    
    waveforms=B(:,1);
    
    temp=B(:,2:end);
    for i=1:size(temp,1)
      alphas{i}=cell2mat(cellfun(@(x) str2num(x),temp(i,:),'UniformOutput',0));
    end
    
   case 'waveform_path'
    waveform_path = values{ii};     
    
   case 'firstAnteproc'
    firstAnteproc = str2num(values{ii});
    
   case 'denseDAG'
    denseDAG = str2num(values{ii});
    
   case 'doVariableWindows'
    doVariableWindows = str2num(values{ii});
    
   case 'noStitching'
    noStitching = str2num(values{ii});
    
   case 'matfilesLocation'
    matfilesLocation = values{ii};
    if strcmp(matfilesLocation,'default')
      matLocDefault = true;
    else
      matLocDefault = false;
    end
    
   case 'matfilesName'
    matfilesName = values{ii};
    
   case 'doRandomPolarization'
    doRandomPolarization = str2num(values{ii});
    
   case 'nbGroupsPerJob'
    nbGroupsPerJob = str2num(values{ii});
    
   case 'genCachefiles'
    genCachefiles = str2num(values{ii});     
    
   case 'doNodes'
    doNodes = str2num(values{ii});
    
   case 'nodes_dir'
    nodes_dir = values{ii};
    
   case 'windowLimit'
    windowLimit = str2num(values{ii});
    
   case 'randomInjections'
    randomInjections = str2num(values{ii});
    
   case 'retry'
    retry = str2num(values{ii});

   case 'ignoreGaps'
    ignoreGaps = str2num(values{ii});

   case 'stampmdc'
     stampmdc = str2num(values{ii});
     
   case 'mdclist'
     mdclist = values{ii};

   case 'mdclog'
     mdclog = values{ii};
      
   case 'mdcchannel1'
     mdcchannel1 = values{ii};

   case 'mdcchannel2'
     mdcchannel2 = values{ii};

   case 'doTFNotch'
     doTFNotch = str2num(values{ii});

   case 'TFNotchesFile'
     TFNotchesFile = values{ii};

  %% For fixed sky position 
  case 'fixedSkyPosition'
    fixedSkyPosition = values{ii};

  case 'fixedRa'
    fixedRa = values{ii};

  case 'fixedDec'
    fixedDec = values{ii};

  case 'injCont'
    injCont = str2num(values{ii});

  case 'numSample'
    numSample = str2num(values{ii});
  end
end

[names,values]=textread('params_0.txt', '%s %s\n',-1,'commentstyle','matlab');

if doZebra*doSeedless==1
  error('Zebragard and stochtrack cant be turned on together');
end

for ii=1:length(names)
  switch names{ii}
   case 'numSegmentsPerInterval'
    bufferSegments=str2num(values{ii}); 
    
   case 'segmentDuration'
    segDur=str2num(values{ii});
   otherwise
    
  end
end


% T+bufferSegments*segDur seconds are needed to compute
% a ft-map of final duration T, due to the way the signal
% variance is estimated 
bufferSegments=bufferSegments*segDur+4;

% Default assignment. Will be modfified only for bkg.
parameters.doAnteproc=false; 

% Run name is defined according to the GPS time

parameters.run=get_run_from_GPStimes (GPS_start, GPS_end);
% Cross check the consistency of options

if ~denseDAG
  nbGroupsPerJob=1;
end

% Determination of the cachefiles to be used
if hostname == 'ligo.c'
  parameters.cacheSuffix = '';
elseif hostname == 'ligo-w'
  parameters.cacheSuffix = '_lho';
elseif hostname == 'ligo-l'
  parameters.cacheSuffix = '_lho';
elseif hostname == 'atlas.'
  parameters.cacheSuffix = '_atlas';
else
  error('STAMPAS designed for Caltech, Hanford, Livingston and ATLAS usage only')
end

% Create the search sub-directory
if ~inj
  searchPath=generate_searchPath('BKG',GPS_start,GPS_end,ID);
else
  searchPath=generate_searchPath('INJ',GPS_start,GPS_end,ID);
end

%make a copy of seedlessParams in the search Directory. 
if doSeedless==1
   system(['cp ' seedlessParams ' ' searchPath '/' seedlessParams]);
   seedlessParams = [searchPath '/' seedlessParams];
end


[~, username] = system('echo $USER');
username = strtrim(username);
dagIdx = regexp(searchPath, '/');
dagDir = searchPath(dagIdx(end)+1:end);
scratchPath = fullfile('/scratch',username, dagDir);
system(['mkdir -p ' scratchPath]);
system(['mkdir -p ' scratchPath  '/logs']);


% Fixed Position
try 
  fixedSkyPosition;
catch
  fixedSkyPosition=false;
end

if str2num(fixedSkyPosition)
  try
    fixedRa;
    fixedDec;
  catch
    error(['you should specify a fixed Ra and Dec in order to use ' ...
           'fixed sky position']);
  end

else
  fixedRa  = NaN;
  fixedDec = NaN;
end

% TFNotch
if doTFNotch
  if ~exist(TFNotchesFile);
    error('TFNotchesFile not exist')
  end
  TFNotchesFile = TFNotchesFile;
else
  TFNotchesFile = '';
end

% BKG case
if ~inj
  if TSOffset ~= 0 & doZeroLag ~= 0
    error(['An offset for time shift is applied while zerolag is' ...
	   ' applied. Please check the config file and restart.'])
  end
  
  % matfiles location
  if matLocDefault
    matfilesPath = [searchPath '/matfiles'];
    if ~exist (['./' matfilesPath])
      mkdir(['./' matfilesPath]);
    end
  else
    matfilesPath = [matfilesLocation];
    if ~exist(matfilesPath);
      mkdir(matfilesPath);
    end
  end

  if doLonetrack || doLonetrackPProc
    idxLag = 0;
  else
    % Handling zero-lag case
    idxLag=1-doZeroLag;
  end

  % First pass for anteproc, always done without anteproc
  if firstAnteproc
    % Check that the search launched does not exist already
    if exist([searchPath '/tmp']) == 7
     error('A similar search is already present. Process aborted');
    end
    
    % Create subdirectories of the search
    system(['mkdir ./' searchPath '/tmp']);
    system(['mkdir ./' searchPath '/logs']);
    system(['mkdir -p ' searchPath '/results']);

    % cp frequencies to notch
    system(['cp ' freqs ' ' searchPath '/tmp/frequencies.txt']);

    parameters.doShift=1;
    parameters.shiftTime=num2str(0);
    parameters.alpha=num2str(1);
    parameters.largeShift=false;

    % general parameters of bkg studies
    parameters.ppseed=num2str(-1);
    parameters.tShift1=num2str(0);
    parameters.stampinj=false;
    parameters.storemats=false;       
    parameters.doAnteproc=true;
    parameters.stampmdc=false;

    % Param file for ifo1
    parameters.detector=1;
    parameters.outputfiledir=[matfilesPath '/'];    
    parameters.outputfilename=[matfilesName];
    generate_param_file([searchPath '/tmp/params_' parameters.pair(1:2) '.txt'],parameters,searchPath);

    % Param file for ifo2
    parameters.detector=2;
    parameters.outputfiledir=[matfilesPath '/'];  
    generate_param_file([searchPath '/tmp/params_' parameters.pair(3:4) '.txt'],parameters,searchPath);
      
    % create jobs list for anteproc files generation
    % overlaps are made to be certain to cover all the data with
    % pre-processing files
      
    list1=select_jobs(pair,parameters.run,GPS_start,GPS_end);
    list_of_jobs=shorten_jobs_list_overlap(list1,window+bufferSegments,bufferSegments+segDur,segDur,0,0);
    list_of_jobs=mod_jobs_list(list_of_jobs,segDur);

    dlmwrite([searchPath '/tmp/joblist.txt'],list_of_jobs,'delimiter',' ','precision','%.8f');
    
    % dag for first pass anteproc
    display('Generate the anteproc dag ...')
    make_dag_anteproc(searchPath,parameters.pair,[searchPath '/tmp/params_'],...
		      [searchPath '/tmp/joblist.txt'],retry,scratchPath);
    dlmwrite([searchPath '/APok.txt'],1)         

    % Generate DAG-files for timeshift or zerolag 
  else
    doInj=0;
    
    itf1Prefix=[parameters.pair(1) '-' parameters.pair(1:2) '_' matfilesName];
    itf2Prefix=[parameters.pair(3) '-' parameters.pair(3:4) '_' matfilesName];

    % Generating cachefile
    if genCachefiles
      anteproc_cache(GPS_start,GPS_end,ID,parameters.pair(1:2), ...
		     parameters.pair(3:4),matfilesPath,matfilesName,segDur,doNodes,nodes_dir);
    end

    list1=anteproc_prep4bnkd_stampas(searchPath,GPS_start,GPS_end,overlap,[matfilesPath '/' itf1Prefix], ...
				     [matfilesPath '/' itf2Prefix], window,segDur,1,load([parameters.run '_joblists/jobs' pair  '.txt']));         

    if ignoreGaps
       list_of_jobs=shorten_jobs_list_ignoregaps(list1,window,overlap,segDur,1,1);
    else
       list_of_jobs=shorten_jobs_list_overlap(list1,window,overlap,segDur,1,1);
    end

    list_of_jobs=mod_jobs_list(list_of_jobs,segDur);

    dlmwrite([searchPath '/tmp/jobfile_bknd.txt'],list_of_jobs,'delimiter','\t','precision','%.8f');
    dlmwrite([searchPath '/tmp/joblistTrue.txt'],list_of_jobs,'delimiter','\t','precision','%.8f');   
 
    % Check background duration
    if NTShifts>size(list_of_jobs,1)-1
      warning(['Job list too short, doing only ' num2str(size(list_of_jobs,1)-1) ' timeshifts'])
      NTShifts=size(list_of_jobs,1)-1;
    end
    
    % Zerolag livetime computation
    display('Compute zerolag livetime ...');
    if doLonetrack || doLonetrackPProc
      %compute_zerolag_livetime_lonetrack(searchPath, list_of_jobs, window);
      compute_zerolag_livetime(searchPath, list_of_jobs, overlap);
    else
      compute_zerolag_livetime(searchPath, list_of_jobs, overlap);
    end

    % Timeslides livetime computation
    if doLonetrack || doLonetrackPProc
      if lonetrackNSlides > 0
        NTShiftsLivetime = lonetrackNSlides;
      else
        % Simply length of jobs for Lonetrack
        NTShiftsLivetime = length(list_of_jobs(:,1));
      end
    else
      NTShiftsLivetime = NTShifts;
    end

    if NTShiftsLivetime>0
      display('Compute timeslides livetime ...');
      if doLonetrack || doLonetrackPProc
         compute_timeslides_livetime_lonetrack(searchPath, list_of_jobs, window, ...
				  overlap, NTShiftsLivetime,segDur);
      else
         compute_timeslides_livetime(searchPath, list_of_jobs, window, ...
                                  overlap, NTShiftsLivetime);
      end
    end

    % generate the clustParam.mat used by clustermap.m
    make_clustParam(searchPath,parameters.pair,fmin,fmax,doInj,...
		    doZebra,doSeedless,doSeedlessAllsky,seedlessParams,doLonetrack,...
		    doGPU,doParallel,...
		    doDgPlots,doVariableWindows,overlap,altSigma,noStitching,segDur,...
		    matfilesPath,matfilesName,bufferSegments,0,windowLimit,doTFNotch,TFNotchesFile,doStampFreqMask,fixedSkyPosition,fixedRa,fixedDec);
    display('Generate the background dag ...')
  
    if doLonetrackPProc
      make_dag_lonetrackpproc_anteproc(searchPath,NTShifts,idxLag,TSOffset,denseDAG,nbGroupsPerJob,...
				       retry,jobMemory,doGPU,lonetrackFAP,lonetrackNSlides,scratchPath);
    else
      make_dag_bkg_anteproc(searchPath,NTShifts,idxLag,TSOffset,denseDAG,nbGroupsPerJob,...
			    retry,jobMemory,doGPU, doLonetrack,scratchPath);
    end
  end

end

% injections
if inj

  % Check that the search launched does not exist already
  if exist([searchPath '/tmp']) == 7
    error('A similar search is already saved. Process aborted');
  end
  
  % Creates subdirectories of the search
  system(['mkdir ./' searchPath '/tmp']);
  system(['mkdir ./' searchPath '/logs']);
  system(['mkdir -p ' searchPath '/results']);

  %% If requesting injections across randomly sampled points in the parameter space
  %% This will create new mat files with randomly sampled parameters and ... 
  %% ... overwrite the B struct with them. 
  
  try
     injCont == injCont;
  catch
       injCont = false;
  end


  if injCont
    [B,waveform_path] =  injection_continuum(searchPath,B,numSample,fmin,fmax);
    waveforms = B(:, 1);

    temp=B(:,2:end);
    for jj=1:size(temp,1)
      alphas{jj}=cell2mat(cellfun(@(x) str2num(x),temp(jj,:),'UniformOutput',0));
    end
    
  end
  
  % cp frequencies to notch
  system(['cp ' freqs ' ' searchPath '/tmp/frequencies.txt']);
  
  % write parameters structure
  
  parameters.doShift=1;
  parameters.shiftTime=num2str(0);
  parameters.alpha=num2str(1);
  
  if NTShifts>0
    parameters.largeShift=true;
  else 
    parameters.largeShift=false;
  end
  
  % NTShift=0: injection on zero-lag. Dangerous, use with caution.
  if NTShifts>0
    parameters.largeShiftTime2=num2str(TShiftInj);
  else
    TShiftInj=0;
  end
  
  % Add a fifth column is added in the jobfiles, identifying 
  % all the jobs issued from a single job of the original joblist. 
  groupInj=1;
  display('Generate joblist_injections.txt ...');
  
  % Truncate the original joblist with search GPS boundaries
  list1=select_jobs(pair,parameters.run,GPS_start,GPS_end);
  
  % Time shift the second IFO and determine coincident time (can
  % be long)
  list1=tshiftjobs_nosave(list1,num2str(TShiftInj),num2str(TShiftInj));
  
  %list_of_jobs=shorten_jobs_list_overlap(list1,window+bufferSegments,bufferSegments+groupInj*overlap,segDur,1,groupInj);
  
  % Extract overlapping window size jobs and determine group index
  % (can be long)

  if ignoreGaps
    list_of_jobs=shorten_jobs_list_ignoregaps(list1,window,overlap,segDur,1,groupInj);
  else
    list_of_jobs=shorten_jobs_list_overlap(list1,window,overlap,segDur,1,groupInj);
  end

  list_of_jobs=mod_jobs_list(list_of_jobs,segDur);
 
  dlmwrite([searchPath '/tmp/joblist_injections.txt'],list_of_jobs,'delimiter',' ','precision','%.8f');

  % Generation of random time start of injection over the full run
  % time range
  
  display('Generate injections random times and positions ...');

  onthefly = 0;
  ontheflytypes = {'sinusoid','inspiral','rmodes','magnetar','msmagnetar'};
   for ii = 1:length(waveforms)
    for jj = 1:length(ontheflytypes)
      if findstr(waveforms{ii}, ontheflytypes{jj})
        onthefly = 1;
      end
    end
  end

  if onthefly
    injTimes=generate_injection_times_onthefly(GPS_start,GPS_end,NumberOfInjections,...
					       injectionSeparation,list_of_jobs, ...
					       waveforms, ...
					       waveform_path,blindZone,injCont);
  elseif stampmdc
    injTimes = generate_injection_times_mdc(GPS_start,GPS_end, ...
                                            mdclist,mdclog,list_of_jobs);

  else
    injTimes=generate_injection_times(GPS_start,GPS_end,NumberOfInjections,...
				      injectionSeparation,list_of_jobs, ...
				      waveforms, waveform_path,blindZone);
  end
  % Incase the number of possible injections is smaller than what
  % has been requested, one needs to redefined NumberOfInjections
  NumberOfInjections=size(injTimes,1);

  % Generation of random source sky position over a sphere
  ra_dec=zeros(NumberOfInjections,2); 

  % if fixedSkyPosition is true, all source sky position are with fixedRa and fixedDec, else
  % sky positions are randomly generated with a reproducible seed on a sphere.
  if str2num(fixedSkyPosition)
    ra_dec(:,1) = str2num(fixedRa)*ones(NumberOfInjections,1);
    ra_dec(:,2) = str2num(fixedDec)*ones(NumberOfInjections,1);
  else
    rng(GPS_start,'twister');
    ra_dec(:,1)=24*rand(NumberOfInjections,1);
    ra_dec(:,2)=acos(2*rand(NumberOfInjections,1)-1)*180/acos(-1)-90;
  end

  ra_dec=[ra_dec injTimes];
  
  % Injection location and time are saved. Positions are the same for each waveform used
  % Times are identical for a given waveform, for any alpha factor
  dlmwrite([searchPath '/tmp/radec.txt'],ra_dec,'delimiter',' ','precision','%.4f');
  
  %% Study (or not) the injection segment without any injection
  if nullWaveform
    idx_inj=0;
    % generate the nowaveform joblist
    display('Generate the nowaveform joblist ...')
    job_list_nowaveform=generate_joblist_nowaveform(searchPath,waveform_path, waveforms);
    dlmwrite([searchPath '/tmp/joblist_nowaveform.txt'],job_list_nowaveform,'delimiter',' ','precision','%.4f');
  else
    idx_inj=1;
  end
  
  % Each parameters structure is different for injections
  for i=idx_inj:size(waveforms,1) 
    if i==0
      parameters.stampinj=false;
      parameters.stampmdc=false;
      parameters.nameInj=['noWaveform'];
    else
        W=waveforms(i);

      if stampmdc
        parameters.stampinj = false;
        parameters.stampmdc = true;
        parameters.mdclist = mdclist;
        parameters.mdcchannel1 = mdcchannel1;
        parameters.mdcchannel2 = mdcchannel2;
      else
        parameters.stampinj = true;
        parameters.stampmdc = false;
      end

      parameters.injfile=[waveform_path W{1}];

      % random injection polarization
      parameters.doRandPol = false;
      if doRandomPolarization
	parameters.doRandPol = true;
      end

      % if injection name ends in '/', it is probably because
      % we are using a directory of waveform files for random
      % polarization. so we remove the '/' for purposes related to
      % naming of results directories in STAMPAS.
      parameters.nameInj=W{1};
      if ~isempty(regexp(parameters.nameInj,'/$'))
	parameters.nameInj = parameters.nameInj(1:end-1);
      end
      parameters.alphas=alphas{i};
      parameters.alphas=parameters.alphas(parameters.alphas(:)~=0);
    end
    savefile=[searchPath '/tmp/parameters_' num2str(i)];
    save(savefile, 'parameters');
  end

  % Generate DAG-files
  doInj=1;
  if doRandomPolarization
    doFixedPol=1;
  else
    doFixedPol=0;
  end
 
  % generate the clustParam.mat used by clustermap.m
  make_clustParam(searchPath,parameters.pair,fmin,fmax,doInj,...
		  doZebra,doSeedless,doSeedlessAllsky,seedlessParams,doLonetrack,...
		  doGPU,doParallel,...
		  doDgPlots,doVariableWindows,overlap+bufferSegments/2,altSigma,...
		  noStitching,segDur,'','',bufferSegments,doFixedPol,windowLimit,doTFNotch,TFNotchesFile,doStampFreqMask,fixedSkyPosition,fixedRa,fixedDec);

  % generate the dag
  display('Generate the injection dag ...')
  if doLonetrack==1
    copy_alphas=alphas;
  else
    copy_alphas=cell(size(alphas));
    for i=1:size(alphas,2)
      copy_alphas{i}=0;
    end
  end
  make_dag_inj(searchPath,waveform_path,waveforms,idx_inj,retry,jobMemory,doGPU,copy_alphas,scratchPath,injCont);
end

%% Control file used by the submission script
dlmwrite([searchPath '/ok.txt'],1)

exit
