function compute_MDC_parameters (GPS_start_inj, GPS_end_inj, ID_inj, waveform_index, alpha_index, GPS_start_bkg, GPS_end_bkg, ID_bkg)

% 
% Funtion to compute and save all parameters of the injections made
% on the fly during an injection DAG
%
% Inputs: GPS_start_inj, GPS_end_inj, ID_inj: Dag parameters of the
%         injections dag
%         waveform_index: wavefor index (1, ...)
%         GPS_start_bkg, GPS_end_bkg, ID_bkg: Dag parameters of the
%         dag that contains the anteproc files
% 
% Output: a file in the INJ dag tmp file that stors all parameters
%
% Contact: Marie Anne Bizouard (mabizoua@lal.in2p3.fr) 2015
%

GPS_start_bkg=strassign(GPS_start_bkg);
GPS_end_bkg=strassign(GPS_end_bkg);
ID_bkg=strassign(ID_bkg);

GPS_start_inj=strassign(GPS_start_inj);
GPS_end_inj=strassign(GPS_end_inj);
ID_inj=strassign(ID_inj);

waveform_index=strassign(waveform_index);
alpha_index=strassign(alpha_index);

% Parameters
searchPath=generate_searchPath('INJ',GPS_start_inj,GPS_end_inj,ID_inj);

if exist(searchPath)~=7
  error (['Input argument ' searchPath ' is not found']);
  exit 1
end

BKG_path=generate_searchPath('BKG',GPS_start_bkg,GPS_end_bkg,ID_bkg);
% Indicate where to get the anteproc matfiles
if exist(BKG_path)~=7
  error (['Input argument ' BKG_path ' is not found']);
  exit 1
end

run=get_run_from_GPStimes (GPS_start_inj, GPS_end_inj);

if strcmp(run,'') ==1
  error(['Run has not be recognized from teh GPS start and end' ...
	 ' times. Please check.']);
end

anteproc_cache_filename=[BKG_path '/anteproc_cache.mat'];
anteproc_matfiles_name=['STAMP_' run '_dt1_df1_nspi17'];

% Open output file
fid=fopen([searchPath '/tmp/injections_' num2str(waveform_index) '_' num2str(alpha_index) '.txt'], 'w');

% get all params_inj_X_Y_Z.txt files where X>0 (X==0 corresponds to
% nowaveform). Y is the jobNumber and Z is the alpha index. One
% needs to get all alpha values as psi and iota are different for
% each alpha values.

temp = dir([searchPath '/tmp/params_inj_*_' num2str(alpha_index) '.txt']);
r_dirs = {temp.name};
r_dirs(cellfun(@isempty,regexp(r_dirs,['params_inj_' num2str(waveform_index) '_[0-9]'],'match','once'))) = [];
display(num2str(numel(r_dirs)))
for ii=1:numel(r_dirs)
  fprintf('%s\n',r_dirs{ii});
  
  % Read injection parameters
  paramsFile=[searchPath '/tmp/' r_dirs{ii}];
  params = readParamsFromFile(paramsFile);
  
  inj_params=compute_waveform_parameters(params, anteproc_cache_filename, ...
					 anteproc_matfiles_name);
  
  fprintf(fid,...
	  ['%2.2f %3.2f %10.1f %0.2g %s %s ' ...
	   '%3.2f %3.2f %0.4g %0.4g %3.2f %3.2f %3.2f %3.2f %0.3g %0.6f \n'], ...
	  inj_params.ra, ...
	  inj_params.decl, ...
	  inj_params.startGPS, ...
	  inj_params.duration, ...
	  inj_params.name, ...
	  inj_params.inj_type, ...
	  inj_params.iota, ...
	  inj_params.psi, ...
	  inj_params.F1, ...
	  inj_params.F2, ...
	  inj_params.SNR1, ...
	  inj_params.SNR2, ...
	  inj_params.SNR1s, ...
	  inj_params.SNR2s, ...
	  inj_params.hrss, ...
	  inj_params.alpha);
end

fclose(fid);

