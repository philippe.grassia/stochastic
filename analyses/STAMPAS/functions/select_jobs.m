function new_job_list=select_jobs(pair,run,GPS_start,GPS_end)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function select_jobs()
%
% This function. given a joblist, returns the jobs located
% in a time interval GPS_Start-GPS_end. The starting time
% of the first job is set to be GPS_start, and the ending
% time of the last job to be GPS_end.
%
%Written by: Samuel Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if pair=='H1L1' | pair=='H1V1' & run == 'S6' | pair=='V1L1' & run == 'S6'
  jobfile=load([run '_joblists/jobs' pair  '.txt']);
else
  error('Error during the selection of the pair')
end

if ~isnumeric(GPS_start) | ~isnumeric(GPS_end)
  error('GPS_start or GPS_end is not an integer')
end

filter=jobfile(:,3) >= GPS_start & jobfile(:,2) <= GPS_end;
new_job_list=jobfile(filter,:);

if size(new_job_list,1) == 0
  error('No coincidence time for this choice of GPS times')
end

%% First and last jobs are cut if need be
if new_job_list(1,2)<GPS_start
  new_job_list(1,2)=GPS_start;
  new_job_list(1,4)=new_job_list(1,3)-new_job_list(1,2);
end

if new_job_list(end,3)>GPS_end
  new_job_list(end,3)=GPS_end;
  new_job_list(end,4)=new_job_list(end,3)-new_job_list(end,2);
end

%% Add group numbers
new_job_list=[new_job_list, (1:size(new_job_list,1))'];

return
