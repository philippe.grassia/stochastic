function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry,memoryJob,doGPU,lonetrackFAP)
% function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry)
%
% Writes a Condor .dag file for background studies (time-slides and zero-lag).
%
% Inputs:
%   searchPath - path to search directory
%   NShifts    - number of time-slides to do
%   idxLag     - 0 if doing zero-lag, 1 if not.
%   TOffset    - offset for time-slides (ex: set to 100 to do
%                100 time-slides starting at 101 - 200 instead of 1-100).
%   denseDAG   - if true, combine multiple science segments into a condor job.
%   nbGroupsPerJob - number of science segments per condor job.
%   retry      - number of times to retry condor job if it fails.
%   doGPU      - adds GPU condor options if needed
%
%  Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

searchPath = 'BKG/Start-1126073342_Stop-1137283217_ID-1/'; 
lonetrackFAP = 0.01;

jobfile = [searchPath '/tmp/joblistTrue.txt'];
jobs = load(jobfile);
T = length(jobs);

lonetrackPath = [searchPath '/Lonetrack/'];
bkndoutputDir = [lonetrackPath '/bknd'];
system(['mkdir -p ' bkndoutputDir]); 
save_dirs = dir([lonetrackPath '/lonetrack_*']); 

snrmax1 = []; snrmax2 = []; snrmaxQS = [];
lsnr1 = []; lsnr2 = [];

H1 = load([bkndoutputDir '/h1_trig.txt']);
L1 = load([bkndoutputDir '/l1_trig.txt']);
coherent = load([bkndoutputDir '/coherent_trig.txt']);

[nrows,ncols] = size(H1);

[junk,indexes] = sort(H1(:,3),'descend');
H1_sort = H1(indexes,:);
[junk,indexes] = sort(L1(:,3),'descend');
L1_sort = L1(indexes,:);
[junk,indexes] = sort(coherent(:,3),'descend');
coherent_sort = coherent(indexes,:);

nprint = 100;
fid_H1 = fopen([bkndoutputDir '/h1_trig_loudest.txt'],'w+');
fid_L1 = fopen([bkndoutputDir '/l1_trig_loudest.txt'],'w+');
fid_coherent = fopen([bkndoutputDir '/coherent_trig_loudest.txt'],'w+');

for ii = 1:nprint
   vals = H1_sort(ii,:);
   aa = vals(1); gps = vals(2); snr = vals(3);
   tmin = vals(4); tmax = vals(5); tmid = vals(6);
   fmin = vals(7); fmax = vals(8); fmid = vals(9);
   fprintf(fid_H1,'%d %.0f %.5f %.0f %.0f %.0f %.0f %.0f %.0f\n',...
      aa,gps,snr,tmin,tmax,tmid,fmin,fmax,fmid);
end

for ii = 1:nprint
   vals = L1_sort(ii,:);
   aa = vals(1); gps = vals(2); snr = vals(3);
   tmin = vals(4); tmax = vals(5); tmid = vals(6);
   fmin = vals(7); fmax = vals(8); fmid = vals(9);
   fprintf(fid_L1,'%d %.0f %.5f %.0f %.0f %.0f %.0f %.0f %.0f\n',...
      aa,gps,snr,tmin,tmax,tmid,fmin,fmax,fmid);
end

for ii = 1:nprint
   vals = coherent_sort(ii,:);
   aa = vals(1); gps = vals(2); snr = vals(3);
   tmin = vals(4); tmax = vals(5); tmid = vals(6);
   fmin = vals(7); fmax = vals(8); fmid = vals(9);
   fprintf(fid_coherent,'%d %.0f %.5f %.0f %.0f %.0f %.0f %.0f %.0f\n',...
      aa,gps,snr,tmin,tmax,tmid,fmin,fmax,fmid);
end

fclose(fid_H1); fclose(fid_L1); fclose(fid_coherent);

return;
