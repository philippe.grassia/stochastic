function params = getSeedlessParams(params,seedlessParams,doLonetrack)


params = stochtrackDefaults(params);
params.doStochtrack = 1;
params.stochtrack.lonetrack = doLonetrack;

params.stochtrack.doSeed = 1;
params.stochtrack.seed = 1;

params.stochtrack.cbc.min_eccentricity = 0;
params.stochtrack.cbc.max_eccentricity = 0.5;
params.stochtrack.cbc.n_eccentricity = 6;


lines = textread(seedlessParams,'%s','delimiter','\n');
for i = 1:length(lines)
  line = lines{i};
  lineSplit = regexp(line,':','split');
  if strcmp(lineSplit{1},'doBezier')
    params.stochtrack.doBezier = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'doCBC')
    params.stochtrack.doCBC = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'doECBC')
    params.stochtrack.doECBC = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'doBurstegard')
    params.stochtrack.doBurstegard = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'doViterbi')
    params.stochtrack.doViterbi = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'doMSmagnetar')
    params.stochtrack.doMSmagnetar = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'cbcType')
    params.stochtrack.cbcType = lineSplit{2};
  elseif strcmp(lineSplit{1},'doAllsky')
    params.stochtrack.cbc.allsky = str2num(lineSplit{2});
    params.stochtrack.cbc.ndirs = 40;
  elseif strcmp(lineSplit{1},'T')
    params.stochtrack.T = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'F')
    params.stochtrack.F = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'mindur')
    params.stochtrack.mindur = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'norm')
    params.stochtrack.norm = lineSplit{2};
  elseif strcmp(lineSplit{1},'doPixelCut')
    params.stochtrack.doPixelCut = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'pixelThreshold')
    params.stochtrack.pixel_threshold = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'doLonetrackPixelCut')
    params.stochtrack.doLonetrackPixelCut = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'lonetrackPixelThreshold')
    params.stochtrack.lonetrack_pixel_threshold = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'lonetrackSigma')
    params.stochtrack.lonetrack_sigma = lineSplit{2};
  elseif strcmp(lineSplit{1},'doLonetrackAltSigma')
    params.stochtrack.doLonetrackAltSigma = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'altSigmaBins')
    params.altsigma_bins = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'doMultipleMaps')
    params.stochtrack.doMultipleMaps = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'numMaps')
    params.stochtrack.numMaps = str2num(lineSplit{2});
 elseif strcmp(lineSplit{1},'doCoarseGrainMap')
    params.doCoarseGrainMap = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'coarseGrain_segDur')
    params.coarseGrain_segDur = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'coarseGrain_deltaF')
    params.coarseGrain_deltaF = str2num(lineSplit{2});
%%%% 
  elseif strcmp(lineSplit{1},'min_eccentricity')
    params.stochtrack.cbc.min_eccentricity = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'max_eccentricity')
    params.stochtrack.cbc.max_eccentricity = str2num(lineSplit{2});
  elseif strcmp(lineSplit{1},'n_eccentricity')
    params.stochtrack.cbc.n_eccentricity = str2num(lineSplit{2});
  end
end

if params.stochtrack.doCBC || params.stochtrack.doECBC
  params.stochtrack.doDF = true;
  if strcmp(params.stochtrack.cbcType,'LowMass')
    params.stochtrack.cbc.min_mass = 0.10;
    params.stochtrack.cbc.max_mass = 1.5;
    params.stochtrack.extra_pixels = 550;
  elseif strcmp(params.stochtrack.cbcType,'HighMass')
    params.stochtrack.cbc.min_mass = 3.0;
    params.stochtrack.cbc.max_mass = 50.0;
    params.stochtrack.extra_pixels = 550;
  end
else
  clear params.stochtrack.cbc.min_eccentricity, params.stochtrack.cbc.max_eccentricity, params.stochtrack.cbc.n_eccentricity
end

if params.stochtrack.doBurstegard
  params.burstegard.NCN = 80;
  params.burstegard.NR = 2;
  params.burstegard.pixelThreshold = 1.75;
  params.burstegard.tmetric = 1;
  params.burstegard.fmetric = 1;
  params.burstegard.debug = 0;
  params.burstegard.weightedSNR = true;

  % The super-cluster is the sum of all clusters.
  params.burstegard.super = false;

  % findtrack is a cluster-the-cluster option.
  params.burstegard.findtrack = true;

  % rr is a variable for findtrack.
  params.burstegard.rr = 35;

end

if params.stochtrack.doViterbi
  % viterbi spindown default
  params.viterbi.spindown = 10;
end

return

