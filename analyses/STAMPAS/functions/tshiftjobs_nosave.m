function outputlist=tshiftjobs_nosave(data, secstocrop, shiftsec)

% This routine shortens the joblist to contain joblist of coincident
% data taking into account a constant timeshift.
% INPUT: data -> input job list 
%        secstocrop -> total secs to crop from the job
%                      (to remove small jobs that don't have useful data)
%                      e.g., numSegmentsPerInterval - 1 + 2 * buffersec
%        shiftsec -> secs to shift the second detector
% Routine by shivaraj
% Adapted to STAMPAS by Samuel Franco (franco@lal.in2p3.fr)
% Contact shivaraj@physics.umn.edu

outputlist=[];

secstocrop=str2num(secstocrop);
shiftsec=str2num(shiftsec);

%% List of all GPS times from the beginning to the end of
%% the joblist
l_data=size(data,1);
tt = data(1,2):data(end,3);

%% List of GPS times actually containing data
temp = [];
for ii = 1:l_data
  temp = [temp data(ii,2):data(ii,3)];
end

%% newtt will correspond to the times which,
%% when a timeshift shiftsec is applied, still
%% corresponds to coincident data
cut = ismember(tt,temp);
newcut = cut(shiftsec+1:end);
finalcut = cut(1:end-shiftsec) & newcut;
newtt = tt(finalcut);

kk = 1;
job = [newtt(1)];
%% Loop over all the newtt times to constitue the longest possible
%% new science segments
for jj = 2:length(newtt)
  if(newtt(jj)-job(end) > 1 | jj==length(newtt))
    if(job(end)-job(1) > secstocrop)
      outputlist=[outputlist; kk, job(1), job(end), job(end)-job(1)];
    end
    job = [newtt(jj)];
    kk = kk + 1;
  else
    job = [job newtt(jj)];
  end
end

