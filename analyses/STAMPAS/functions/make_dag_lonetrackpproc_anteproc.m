function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry,memoryJob,doGPU,lonetrackFAP,lonetrackNSlides,scratchPath)
% function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry)
%
% Writes a Condor .dag file for background studies (time-slides and zero-lag).
%
% Inputs:
%   searchPath - path to search directory
%   NShifts    - number of time-slides to do
%   idxLag     - 0 if doing zero-lag, 1 if not.
%   TOffset    - offset for time-slides (ex: set to 100 to do
%                100 time-slides starting at 101 - 200 instead of 1-100).
%   denseDAG   - if true, combine multiple science segments into a condor job.
%   nbGroupsPerJob - number of science segments per condor job.
%   retry      - number of times to retry condor job if it fails.
%   doGPU      - adds GPU condor options if needed
%
%  Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

jobfile = [searchPath '/tmp/joblistTrue.txt'];
jobs = load(jobfile);
T = length(jobs);


lonetrackPath = [searchPath '/Lonetrack/'];
bkndoutputDir = [lonetrackPath '/bknd'];
system(['mkdir -p ' bkndoutputDir]); 
save_dirs = dir([lonetrackPath '/lonetrack_*']); 

snrmax1 = []; snrmax2 = []; snrmaxQS = [];
lsnr1 = []; lsnr2 = [];

% loop over out files
ii=0;
for i = 1:length(save_dirs)
  save_dir = [lonetrackPath '/' save_dirs(i).name];

  for aa=1:T
    fprintf('Job number: %d\n',aa);
    % load data
    try
      filename = [save_dir '/lonetrack_' num2str(jobs(aa,2)) '.mat'];
      q = load(filename);
      ii=ii+1;
      snrmax1(ii) = q.stoch_out.lonetrack.snrmax1;
      snrmax2(ii) = q.stoch_out.lonetrack.snrmax2;
      snrmaxQS(ii) = sqrt(snrmax1(ii).^2 + snrmax2(ii).^2);
      lsnr1(ii)=q.stoch_out.lonetrack.out1.snr_gamma;
      lsnr2(ii)=q.stoch_out.lonetrack.out2.snr_gamma;
      filenames{ii} = filename;
    catch
    end
  end
end

if length(snrmax1) == 0
   error('No files available in %s...\n',lonetrackPath);
end

% account for missing files
fprintf('%i files missing\n', T-ii);
T=ii;

[junk,index] = max(snrmax1);
% print threshold and loudest event
fprintf('SNR1(p=1/%i) = %2.2f\n', T, snrmax1(index));
fprintf('SNR2(p=1/%i) = %2.2f\n', T, snrmax2(index));
fprintf('LSNR1(p=1/%i) = %2.2f\n', T, lsnr1(index));
fprintf('LSNR2(p=1/%i) = %2.2f\n', T, lsnr2(index));
fprintf('Loudest file: %s\n', filenames{index});

[junk,index] = max(snrmax2);
% print threshold and loudest event
fprintf('SNR1(p=1/%i) = %2.2f\n', T, snrmax1(index));
fprintf('SNR2(p=1/%i) = %2.2f\n', T, snrmax2(index));
fprintf('LSNR1(p=1/%i) = %2.2f\n', T, lsnr1(index));
fprintf('LSNR2(p=1/%i) = %2.2f\n', T, lsnr2(index));
fprintf('Loudest file: %s\n', filenames{index});

[junk,index] = max(lsnr1);
% print threshold and loudest event
fprintf('SNR1(p=1/%i) = %2.2f\n', T, snrmax1(index));
fprintf('SNR2(p=1/%i) = %2.2f\n', T, snrmax2(index));
fprintf('LSNR1(p=1/%i) = %2.2f\n', T, lsnr1(index));
fprintf('LSNR2(p=1/%i) = %2.2f\n', T, lsnr2(index));
fprintf('Loudest file: %s\n', filenames{index});

[junk,index] = max(lsnr2);
% print threshold and loudest event
fprintf('SNR1(p=1/%i) = %2.2f\n', T, snrmax1(index));
fprintf('SNR2(p=1/%i) = %2.2f\n', T, snrmax2(index));
fprintf('LSNR1(p=1/%i) = %2.2f\n', T, lsnr1(index));
fprintf('LSNR2(p=1/%i) = %2.2f\n', T, lsnr2(index));
fprintf('Loudest file: %s\n', filenames{index});

% prepare for probability plots
dp = 1/T;
pvals = 1 : -dp : dp;

th1 = interp1(pvals,sort(lsnr1),lonetrackFAP);
th2 = interp1(pvals,sort(lsnr2),lonetrackFAP);

fprintf('FAP: %5f\n', lonetrackFAP);
fprintf('Threshold 1: %5f\n', th1);
fprintf('Threshold 2: %5f\n', th2);

figure;
semilogy(sort(snrmax1), pvals,'k');
hold on;
semilogy(sort(snrmax2), pvals,'b--');
hold off;
%axis([0 400 1e-4 1]);
grid on;
xlabel('SNR');
ylabel('FAP');
xlim([0 20]);
pretty;
print('-dpng',[bkndoutputDir '/snrmax_htrack']);
print('-depsc2',[bkndoutputDir '/snrmax_htrack']);
close;

figure;
semilogy(sort(lsnr1), pvals, 'k');
hold on
semilogy(sort(lsnr2), pvals,'b--');
hold off
hold on
semilogy([1 th1],[lonetrackFAP lonetrackFAP],'k--','LineWidth',3);
semilogy([th1 th1],[lonetrackFAP 1],'k--','LineWidth',3);
semilogy([1 th2],[lonetrackFAP lonetrackFAP],'b--','LineWidth',3);
semilogy([th2 th2],[lonetrackFAP 1],'b--','LineWidth',3);
hold off
grid on;
xlabel('SNR_{tot, max}');
ylabel('FAP');
pretty;
print('-dpng',[bkndoutputDir '/lsnr_htrack']);
print('-depsc2',[bkndoutputDir '/lsnr_htrack']);
close;

nslides = length(lsnr1);
nsh = floor((size(q.stoch_out.lonetrack.out1.reconMax,2)-1)/2);
pass1 = lsnr1 > th1;
pass2 = lsnr2 > th2;

fprintf('%i/%i supression in stage1\n', sum(pass1)+sum(pass2),length(pass1));

snrmax_zero = [];
pass_zero = [];
for aa=1:T
   pass = max([pass1(aa) pass2(aa)]);

   if pass1(aa) && pass2(aa)
      [snrmax,idx] = max([snrmax1(aa) ; snrmax2(aa)]);
   elseif ~(pass1(aa) || pass2(aa))
      [snrmax,idx] = max([snrmax1(aa) ; snrmax2(aa)]);
   elseif pass1(aa)
      snrmax = snrmax1(aa);
      idx = 1;
   elseif pass2(aa)
      snrmax = snrmax2(aa);
      idx = 2;
   end
   snrmax_zero = [snrmax_zero snrmax];
   pass_zero = [pass_zero pass];
end

indexes1 = setdiff(1:length(pass_zero),find(pass_zero));
indexes2 = find(pass_zero);

save([bkndoutputDir '/stage1.mat']);

% Open file for writing.
fid=fopen([searchPath '/stamp_allsky_lonetrackpproc.dag'],'w+');

% Set up lag numbers (add 0 if doing zero-lag)
timeslides = (1+TOffset):(NShifts+TOffset);
if ~(idxLag)
  timeslides = [0 timeslides];
end

% Loop over time-slides.
firstTimeSlide = true;
job_number = 1;
for jj=timeslides
  % Load correct jobfile and get information.
  if (jj == 0) % zero-lag
    jobfile = [searchPath '/tmp/joblistTrue.txt'];
    jobs = load(jobfile);
  elseif (jj ~= 0 & firstTimeSlide)
    % First time-slide, need to load correct jobfile.
    % Only want to do this once (hence the firstTimeSlide variable).
    jobfile = [searchPath '/tmp/jobfile_bknd.txt'];
    jobs = load(jobfile);
    firstTimeSlide = false;
  end
  
  % Define job_idx (list of first analysis job in each condor job)
  if (denseDAG) % Group multiple science segments into a single condor job.
    % Indices of all science segments.
    job_idx = unique(jobs(:,5))';
    % Keep only the first one in each group of science segments.
    job_idx = job_idx(1):nbGroupsPerJob:job_idx(end);
  else % One condor job per analysis job.
    job_idx = 1:size(jobs,1); % Number of jobs
  end

  for ii=job_idx
    % Get first analysis job to be analyzed by the Condor job
    % and the number of jobs in the group.
    if (denseDAG)
      jobs_in_group = find(jobs(:,5)>=ii & jobs(:,5) < (ii+nbGroupsPerJob));
      job1 = jobs_in_group(1);
      groupLength = numel(jobs_in_group);
    else
      job1 = ii;
      groupLength = 1;
    end
    
    % Print to file.
    if doGPU==1
      fprintf(fid,'JOB %d stamp_allsky_lonetrackpproc_gpu.sub\n',job_number);
    else
      fprintf(fid,'JOB %d stamp_allsky_lonetrackpproc.sub\n',job_number);
    end

    fprintf(fid,'RETRY %d %d\n',job_number,retry);
    fprintf(fid,['VARS %d jobnumb="%d" mem="%d" searchPath="%s" paramsFile="%s"' ...
		 ' jobsFile="%s" lagNumber="%d" jobNumber="%d"' ...
		 ' groupLength="%d" scratchPath="%s" '],job_number,job_number,memoryJob,...
	    searchPath,[searchPath '/tmp/params_H1.txt'], ...
	    jobfile, jj, job1, groupLength, scratchPath);
    fprintf(fid,'\n\n');
    job_number = job_number + 1;    
  end
end


% Print PARENT/CHILD syntax so that all jobs depend on the
% first job.  This is to prevent us from running all jobs
% if the first job fails (usually indicates configuration
% problems).
Njobs = job_number - 1;
if Njobs>1
  fprintf(fid,'\nPARENT 1 CHILD ');
  for k=2:Njobs
    fprintf(fid,' %d ',k);
  end

end

% Close the file.
fclose(fid);

return;
