function make_dag_inj(searchPath,waveform_path,waveforms,idx_inj,retry,jobMemory,doGPU,alphas,scratchPath,injCont)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function make_dag_inj(searchPath,waveform_path,waveforms,idx_inj,retry,jobMemory,doGPU,alphas)
%
% This function generates the dag file for STAMPAS injection 
% studies. One job corresponds to one injection and 1 or several
% amplitude factor.
%
% Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if injCont
  dictionary_path = [waveform_path, 'dic_ci.yml'];
  dic = classes.waveforms.dictionary(dictionary_path);
else
  dic = classes.waveforms.dictionary;
end

fid=fopen([searchPath '/stamp_allsky_inj.dag'],'w+');
njobs=1;

jobs=load([searchPath '/tmp/joblist_injections.txt']);
if idx_inj==0
  jobs_nowvf=load([searchPath '/tmp/joblist_nowaveform.txt']);
end
injections=load([searchPath '/tmp/radec.txt']);
Nbwvf=size(waveforms,1);

mem=jobMemory;
% No waveform case
if idx_inj==0

  groups=unique(jobs_nowvf(:,5),'rows');
  for ii=1:size(groups,1)
    i=groups(ii);

    ext=find(jobs_nowvf(:,5)==i);
    if size(ext,1)==0
      error(['make_dag_inj there should be a job ' num2str(i)]);
    end

    job_number=jobs_nowvf(ext(1),1);
    groupLength=1;
    
    if size(ext,1)==1
      if doGPU==1
	fprintf(fid,'JOB %d stamp_allsky_inj_gpu.sub\n',njobs);
      else
	fprintf(fid,'JOB %d stamp_allsky_inj.sub\n',njobs);
      end
      fprintf(fid,'RETRY %d %d\n',njobs,retry);
      fprintf(fid,'VARS %d jobnumb="%d" mem="%d" searchPath="%s" paramsFile="%s" jobsFile="%s" jobNumber="%d" lagNumber="%s"  groupLength="%d" scratchPath="%s" ', ...
	      njobs,njobs,mem,searchPath,...
	      [searchPath '/tmp/params_inj_0_' num2str(njobs)],...
	      [searchPath '/tmp/joblist_injections.txt'], ...
              job_number,[num2str(njobs) '-0-0'],groupLength,scratchPath); 
      fprintf(fid,'\n\n');
      njobs=njobs+1;
    else
      test=diff(jobs_nowvf(ext,1));
      for j=1:size(test,1)
	if test(j)~=1 | j==size(test,1)
	  
	  if j==size(test,1)
	    groupLength=groupLength+1;
	  end
	  
	  if doGPU==1
	    fprintf(fid,'JOB %d stamp_allsky_inj_gpu.sub\n',njobs);
	  else
	    fprintf(fid,'JOB %d stamp_allsky_inj.sub\n',njobs);
	  end
	  fprintf(fid,'RETRY %d %d\n',njobs,retry);
	  fprintf(fid,'VARS %d jobnumb="%d" mem="%d" searchPath="%s" paramsFile="%s" jobsFile="%s" jobNumber="%d" lagNumber="%s"  groupLength="%d" scratchPath="%s" ', ...
		  njobs,njobs,mem,searchPath,...
		  [searchPath '/tmp/params_inj_0_' num2str(njobs)],...
		  [searchPath '/tmp/joblist_injections.txt'], ...
		  job_number,[num2str(njobs) '-0-0'],groupLength, scratchPath); 
	  fprintf(fid,'\n\n');
	  njobs=njobs+1;
	  job_number=jobs_nowvf(ext(j+1),1);
	  groupLength=1;

	else
	  groupLength=groupLength+1;
	end
      end
      
    end
    
  end
  % To synchronize with the next jobs
%  njobs=njobs-1;
end


% All waveforms
for j=1:Nbwvf % Loop over each waveform
  wheader = dic.headers(waveforms{j});
  duration=wheader.duration;
  
  %%% for Lonetrack 1 jobs = 1 injection, 1 wvf, 1 alpha
  %%% for the others 1 jobs = 1 injection, 1 wvf, all alphas
  %%% (the input alphas must be 0 for zebragard)

  for i=1:size(injections,1);
    t_start=injections(i,j+2);
    t_stop=injections(i,j+2)+duration;
    ext=find(t_stop>jobs(:,2) & t_start<jobs(:,3));
    if size(ext,1)>0
      selected_jobs=jobs(ext,:);
    else
      display([num2str(i) ' ' num2str(t_start) ' ' num2str(t_stop) ...
	       ' ' num2str(j)]);
      error('All foreseen injections should be found in at least 1 job')
    end
    
    final_joblist=[];
    
    ext1=find(jobs(:,5)==selected_jobs(1,5));
    if size(ext1,1)>0
      final_joblist=[final_joblist;selected_jobs(:,:)];
    else
      final_joblist=selected_jobs(:,:);
    end
    
    job_number=final_joblist(1,1);
    groupLength=size(final_joblist,1);
    for al=1:size(alphas{j},2)
      if alphas{j}(al)==0
	value=0;
      else
	value=al;
      end
      %      display([num2str(i) ' ' num2str(j) ' ' num2str(alphas{j}(al))]);
      if doGPU==1
	fprintf(fid,'JOB %d stamp_allsky_inj_gpu.sub\n',njobs);
      else
	fprintf(fid,'JOB %d stamp_allsky_inj.sub\n',njobs);
      end
      fprintf(fid,'RETRY %d %d\n',njobs,retry);
      fprintf(fid,'VARS %d jobnumb="%d" mem="%d" searchPath="%s" paramsFile="%s" jobsFile="%s" jobNumber="%d" lagNumber="%s"  groupLength="%d" scratchPath="%s"', ...
	      njobs,njobs,mem,searchPath,...
	      [searchPath '/tmp/params_inj_' num2str(j) '_' num2str(i)],...
	      [searchPath '/tmp/joblist_injections.txt'], job_number, ...
	      [num2str(i) '-' num2str(j) '-' num2str(value)], groupLength,scratchPath); 
      
      fprintf(fid,'\n\n');
      njobs=njobs+1;
    end
  end
end


if job_number>1
  fprintf(fid,'\nPARENT 1 CHILD ');
  for k=2:njobs-1
    fprintf(fid,' %d ',k);
  end
end


fclose(fid);
