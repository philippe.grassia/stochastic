function [newStart,newDuration] = anteproc_varWindows(startGPS,endGPS,inmats1,inmats2,oldStart,oldDuration,newOverlap,searchPath,segDur)
% function [newStart,newDuration] = anteproc_varWindows(startGPS,endGPS,inmats1,inmats2,oldStart,oldDuration,newOverlap,searchPath,segDur)
%
% This function is used to adapt a jobfile previoulsy created
% by anteproc_prep4bknd_stampas.m, when using the variable
% windows option. It returns the start time which must be used
% for as a start for the next ft-map if need be.
%
% This script edits the jobfile called 'bknd_jobfile.txt'.
%
% Written by T. Prestegard (prestegard@physics.umn.edu)
% modified by S. Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Users - modify these parameters. %%%%%%%%%%%%%%%%%%%%%%%
% Start and end GPS times to find data between.
params.startGPS = startGPS;
params.endGPS = endGPS;

% Segment duration of available data.
params.segmentDuration = segDur;

useCachefiles =	exist([searchPath '/anteproc_cache.mat'])==2;

% Location of data.
params.anteproc.inmats1 = inmats1;
params.anteproc.inmats2 = inmats2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Useful quantities.
segDur = params.segmentDuration/2;

% Don't change these parameters!
params.anteproc.jobFileTimeShift = false;
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;
if ~useCachefiles
  params.anteproc.useCache = 0;
else
  params.anteproc.useCache = 1;
  params.anteproc.cacheFile = [searchPath '/anteproc_cache.mat'];
end

% Find all anteproc'd .mat files with GPS times
% within the specified start and end times.
matlist = find_anteproc_mats(params);

% Loop over mat files corresponding to detector one.  Calculate data
% available based on GPS start time and total length (determined from file
% names).  Make a list (gps1) of all segment START times available.
gps1 = [];
for ii=1:length(matlist.ifo1)
  gps_start = regexp(matlist.ifo1{ii},'-(\d{9,10}(|\.\d+))-','tokens','once');
  gps_start = str2num(gps_start{:});
  dur = regexp(matlist.ifo1{ii},'-(\d+|\d+\.\d+)\.mat','tokens','once');
  dur = str2num(dur{:});
  gps1 = [gps1 gps_start:segDur:(gps_start + dur - params.segmentDuration)];
end

% Do same calculation for detector 2.
gps2 = [];
for ii=1:length(matlist.ifo2)
  gps_start = regexp(matlist.ifo2{ii},'-(\d{9,10}(|\.\d+))-','tokens','once');
  gps_start = str2num(gps_start{:});
  dur = regexp(matlist.ifo2{ii},'-(\d+|\d+\.\d+)\.mat','tokens','once');
  dur = str2num(dur{:});
  gps2 = [gps2 gps_start:segDur:(gps_start + dur - params.segmentDuration)];
end

% Remove any duplicate GPS times that may occur due to possible
% overlap of .mat files.
gps1 = sort(unique(gps1));
gps2 = sort(unique(gps2));

% Find available coincident data for the two detectors of interest.
overlaps = sort(intersect(gps1,gps2));
% This lists all the gpstimes (by step of segDur seconds) of the data


% finds the gps time of the job without varWindows
startPoint=double(overlaps(:)==oldStart);

[void,idxOld]=max(startPoint);

if void==0
   error('The loaded mat files seem not to include the current gps time')
end

%% Selects the gps in the list which is newOverlap/segDur time segments
%% before the old gps. newOverlap is the duration by which the time-window
%% is effectively increased.
idxNew=idxOld-newOverlap/segDur;

newStart=overlaps(idxNew);
newDuration=(idxOld-idxNew)*segDur+oldDuration;
