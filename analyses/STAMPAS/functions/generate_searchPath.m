function searchPath = generate_SearchPath(type,GPS_start,GPS_end,N)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function generate_SearchPath()
%
% This function takes as input the parameters of a given
% STAMPAS search, and returns the path to the corresponding
% repertory.
%
%Written by: Samuel Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   
if isscalar(GPS_start)
   GPS_start=num2str(GPS_start);
elseif ischar(GPS_start)
   % do nothing
else
   error('GPS_start should be a double or a string')
end

if isscalar(GPS_end)
   GPS_end=num2str(GPS_end);
elseif ischar(GPS_end)
   % do nothing
else
   error('GPS_end should be a double or a string')
end

if isscalar(N)
   N=num2str(N);
elseif ischar(N)
   % do	nothing
else
   error('NShifts or NInj should be a double or a string')
end

searchPath=[type '/Start-' GPS_start '_Stop-' GPS_end '_ID-' N];
