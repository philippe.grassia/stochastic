function [injTimesAll]= generate_injection_times_mdc(GPS_start,GPS_end,mdclist,mdclog,joblist)
%
% The function draws randomly integer GPS times within [GPS_start
% GPS_end]. Each GPS time must be separated of at least
% inj_separation seconds. Injections starting at the drawn GPS
% times must overlap one of the jobs given in the joblist file. 
%

%
% Inputs: GPS_start
%         GPS_end
%         mdclist
%         joblist
%         blind_zone: duration at the beginning and the end of a
%         group of jobs where the statistic cannot be computed
%
% Outputs: injTimes : GPS start time of the injections that are in
%          at least 1 science job
%
% Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
% Modified by Valentin FREY (mdc reading)
  
  % ---------------------------------------------------------------------
  %% GET FRAMES LIST
  % ---------------------------------------------------------------------
  %
  try
    [frames]=textread(mdclist, '%s\n',-1,'commentstyle','matlab');
  catch
    error('cannot read frames list : %s ', mdclist);
  end
  frames(cellfun(@isempty,frames))=[];

  
  % ---------------------------------------------------------------------
  %% GET FRAMES LOG INFORMATION
  % ---------------------------------------------------------------------
  % get gps of frames and gps of injection
  %
  [gps_f, gps_inj]=textread(mdclog,['%*s %*f %*f %*f %*f %*f %*f '...
                      '%*f %*f %f %f %*s %*f %*f %*f %*s %*f %*f '...
                      '%*f %*s %*f %*f %*f\n'],'commentstyle','shell');
  

  % ---------------------------------------------------------------------
  %% MAIN
  % ---------------------------------------------------------------------
  % we get the frames gps of the list of frames then we use the log
  % file to get the correponding injection time  
  %
  injTimesAll=zeros(length(frames),1);

  for fr = frames'
    % get the gps frames from string
    [gps_f_s] =str2num( regexprep(fr{:},'.*-([0-9]*)-([0-9]*).gwf','$1'));
    
    % get the correponding gps injection time
    injTime=gps_inj(gps_f_s==gps_f);
    
    % check if it's in the dag gps time
    if injTime<GPS_start || injTime>GPS_end
      continue
    end
    
    % check if it's on the segments
    if ~sum(injTime>joblist(:,2) & injTime<joblist(:,3))
      continue
    end
    
    injTimesAll(strcmp(frames,fr{:}))=injTime;
    
  end
  injTimesAll(injTimesAll==0)=[];
  injTimesAll=sort(injTimesAll,'ascend');

end


