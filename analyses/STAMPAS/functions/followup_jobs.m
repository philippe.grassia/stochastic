
dagfile = 'INJ/Start-1126569617_Stop-1128988817_ID-2/stamp_allsky_inj.dag';

waveformNumTrue = 3;
alphaNumTrue = 6;

lines = textread(dagfile,'%s','delimiter','\n','bufsize', 16384*4);

for i = 1:length(lines)
   line = lines{i};
   lineSplit = regexp(line,' ','split');

   if length(lineSplit) < 8
      continue
   end

   for j = 1:length(lineSplit)
      splitval = lineSplit{j};
      splitval = regexp(splitval,'=','split');

      if length(splitval) == 1 
         continue
      end 
   
      splitval1 = splitval{1};
      splitval2 = splitval{2};

      if strcmp(splitval1,'searchPath')
         searchPath = splitval2(2:end-1);
      elseif strcmp(splitval1,'paramsFile')
         paramsFile = splitval2(2:end-1);
      elseif strcmp(splitval1,'jobsFile')
         jobsFile = splitval2(2:end-1);
      elseif strcmp(splitval1,'jobNumber')
         jobNumber = splitval2(2:end-1);   
      elseif strcmp(splitval1,'lagNumber')
         lagNumber = splitval2(2:end-1);
      elseif strcmp(splitval1,'groupLength')
         groupLength = splitval2(2:end-1);
      end

   end
   lagNumberSplit = regexp(lagNumber,'-','split');
   injectionNum = str2num(lagNumberSplit{1});
   waveformNum = str2num(lagNumberSplit{2});
   alphaNum = str2num(lagNumberSplit{3});

   if waveformNum ~= waveformNumTrue
      continue
   end
   if alphaNum ~= alphaNumTrue
      continue
   end
   stoch_out = run_clustermap(searchPath, paramsFile, jobsFile, ...
                                 lagNumber, jobNumber, groupLength)
end

