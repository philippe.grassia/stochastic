function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry,memoryJob,doGPU,lonetrackFAP)
% function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry)
%
% Writes a Condor .dag file for background studies (time-slides and zero-lag).
%
% Inputs:
%   searchPath - path to search directory
%   NShifts    - number of time-slides to do
%   idxLag     - 0 if doing zero-lag, 1 if not.
%   TOffset    - offset for time-slides (ex: set to 100 to do
%                100 time-slides starting at 101 - 200 instead of 1-100).
%   denseDAG   - if true, combine multiple science segments into a condor job.
%   nbGroupsPerJob - number of science segments per condor job.
%   retry      - number of times to retry condor job if it fails.
%   doGPU      - adds GPU condor options if needed
%
%  Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

searchPath = 'BKG/Start-1126073342_Stop-1137283217_ID-1/'; 
lonetrackFAP = 0.01;

jobfile = [searchPath '/tmp/joblistTrue.txt'];
jobs = load(jobfile);
T = length(jobs);

lonetrackPath = [searchPath '/Lonetrack/'];
bkndoutputDir = [lonetrackPath '/bknd'];
system(['mkdir -p ' bkndoutputDir]); 
save_dirs = dir([lonetrackPath '/lonetrack_*']); 

snrmax1 = []; snrmax2 = []; snrmaxQS = [];
lsnr1 = []; lsnr2 = [];

fid_H1 = fopen([bkndoutputDir '/h1_trig.txt'],'w+');
fid_L1 = fopen([bkndoutputDir '/l1_trig.txt'],'w+');
fid_coherent = fopen([bkndoutputDir '/coherent_trig.txt'],'w+');

% loop over out files
ii=0;
for i = 1:length(save_dirs)
  save_dir = [lonetrackPath '/' save_dirs(i).name];

  for aa=1:T
  %for aa=1:100
    fprintf('Job number: %d\n',aa);
    % load data
    try
      filename = [save_dir '/lonetrack_' num2str(jobs(aa,2)) '.mat'];
      q = load(filename);
      ii=ii+1;
      snrmax1(ii) = q.stoch_out.lonetrack.snrmax1;
      snrmax2(ii) = q.stoch_out.lonetrack.snrmax2;
      snrmaxQS(ii) = sqrt(snrmax1(ii).^2 + snrmax2(ii).^2);
      lsnr1(ii)=q.stoch_out.lonetrack.out1.snr_gamma;
      lsnr2(ii)=q.stoch_out.lonetrack.out2.snr_gamma;
      filenames{ii} = filename;
 
      fprintf(fid_H1,'%d %.0f %.5f %.0f %.0f %.0f %.0f %.0f %.0f\n',aa,jobs(aa,2),q.stoch_out.lonetrack.out1.snr_gamma,q.stoch_out.lonetrack.out1.gps_min,q.stoch_out.lonetrack.out1.gps_max,mean([q.stoch_out.lonetrack.out1.gps_min,q.stoch_out.lonetrack.out1.gps_max]),q.stoch_out.lonetrack.out1.fmin,q.stoch_out.lonetrack.out1.fmax,mean([q.stoch_out.lonetrack.out1.fmin,q.stoch_out.lonetrack.out1.fmax]));

      fprintf(fid_L1,'%d %.0f %.5f %.0f %.0f %.0f %.0f %.0f %.0f\n',aa,jobs(aa,2),q.stoch_out.lonetrack.out2.snr_gamma,q.stoch_out.lonetrack.out2.gps_min,q.stoch_out.lonetrack.out2.gps_max,mean([q.stoch_out.lonetrack.out2.gps_min,q.stoch_out.lonetrack.out2.gps_max]),q.stoch_out.lonetrack.out2.fmin,q.stoch_out.lonetrack.out2.fmax,mean([q.stoch_out.lonetrack.out2.fmin,q.stoch_out.lonetrack.out2.fmax]));

      if snrmax1(ii) >= snrmax2(ii)
         fprintf(fid_coherent,'%d %.0f %.5f %.0f %.0f %.0f %.0f %.0f %.0f\n',aa,jobs(aa,2),q.stoch_out.lonetrack.snrmax1,q.stoch_out.lonetrack.out1.gps_min,q.stoch_out.lonetrack.out1.gps_max,mean([q.stoch_out.lonetrack.out1.gps_min,q.stoch_out.lonetrack.out1.gps_max]),q.stoch_out.lonetrack.out1.fmin,q.stoch_out.lonetrack.out1.fmax,mean([q.stoch_out.lonetrack.out1.fmin,q.stoch_out.lonetrack.out1.fmax]));
      else
         fprintf(fid_coherent,'%d %.0f %.5f %.0f %.0f %.0f %.0f %.0f %.0f\n',aa,jobs(aa,2),q.stoch_out.lonetrack.out2.snrmax2,q.stoch_out.lonetrack.out2.gps_min,q.stoch_out.lonetrack.out2.gps_max,mean([q.stoch_out.lonetrack.out2.gps_min,q.stoch_out.lonetrack.out2.gps_max]),q.stoch_out.lonetrack.out2.fmin,q.stoch_out.lonetrack.out2.fmax,mean([q.stoch_out.lonetrack.out2.fmin,q.stoch_out.lonetrack.out2.fmax]));
      end
    catch
    end
  end
end

fclose(fid_H1); fclose(fid_L1); fclose(fid_coherent);

if length(snrmax1) == 0
   error('No files available in %s...\n',lonetrackPath);
end

% account for missing files
fprintf('%i files missing\n', T-ii);
T=ii;

[junk,index] = max(snrmax1);
% print threshold and loudest event
fprintf('SNR1(p=1/%i) = %2.2f\n', T, snrmax1(index));
fprintf('SNR2(p=1/%i) = %2.2f\n', T, snrmax2(index));
fprintf('LSNR1(p=1/%i) = %2.2f\n', T, lsnr1(index));
fprintf('LSNR2(p=1/%i) = %2.2f\n', T, lsnr2(index));
fprintf('Loudest file: %s\n', filenames{index});

[junk,index] = max(snrmax2);
% print threshold and loudest event
fprintf('SNR1(p=1/%i) = %2.2f\n', T, snrmax1(index));
fprintf('SNR2(p=1/%i) = %2.2f\n', T, snrmax2(index));
fprintf('LSNR1(p=1/%i) = %2.2f\n', T, lsnr1(index));
fprintf('LSNR2(p=1/%i) = %2.2f\n', T, lsnr2(index));
fprintf('Loudest file: %s\n', filenames{index});

[junk,index] = max(lsnr1);
% print threshold and loudest event
fprintf('SNR1(p=1/%i) = %2.2f\n', T, snrmax1(index));
fprintf('SNR2(p=1/%i) = %2.2f\n', T, snrmax2(index));
fprintf('LSNR1(p=1/%i) = %2.2f\n', T, lsnr1(index));
fprintf('LSNR2(p=1/%i) = %2.2f\n', T, lsnr2(index));
fprintf('Loudest file: %s\n', filenames{index});

[junk,index] = max(lsnr2);
% print threshold and loudest event
fprintf('SNR1(p=1/%i) = %2.2f\n', T, snrmax1(index));
fprintf('SNR2(p=1/%i) = %2.2f\n', T, snrmax2(index));
fprintf('LSNR1(p=1/%i) = %2.2f\n', T, lsnr1(index));
fprintf('LSNR2(p=1/%i) = %2.2f\n', T, lsnr2(index));
fprintf('Loudest file: %s\n', filenames{index});

% prepare for probability plots
dp = 1/T;
pvals = 1 : -dp : dp;

th1 = interp1(pvals,sort(lsnr1),lonetrackFAP);
th2 = interp1(pvals,sort(lsnr2),lonetrackFAP);

fprintf('FAP: %5f\n', lonetrackFAP);
fprintf('Threshold 1: %5f\n', th1);
fprintf('Threshold 2: %5f\n', th2);

figure;
semilogy(sort(snrmax1), pvals,'k');
hold on;
semilogy(sort(snrmax2), pvals,'b--');
hold off;
%axis([0 400 1e-4 1]);
grid on;
xlabel('SNR');
ylabel('FAP');
xlim([0 20]);
pretty;
print('-dpng',[bkndoutputDir '/snrmax_htrack']);
print('-depsc2',[bkndoutputDir '/snrmax_htrack']);
close;

figure;
semilogy(sort(lsnr1), pvals, 'k');
hold on
semilogy(sort(lsnr2), pvals,'b--');
hold off
hold on
semilogy([1 th1],[1e-2 1e-2],'k--','LineWidth',3);
semilogy([th1 th1],[1e-2 1],'k--','LineWidth',3);
semilogy([1 th2],[1e-2 1e-2],'b--','LineWidth',3);
semilogy([th2 th2],[1e-2 1],'b--','LineWidth',3);
hold off
grid on;
xlabel('SNR_{tot, max}');
ylabel('FAP');
pretty;
print('-dpng',[bkndoutputDir '/lsnr_htrack']);
print('-depsc2',[bkndoutputDir '/lsnr_htrack']);
close;

nslides = length(lsnr1);
nsh = floor((size(q.stoch_out.lonetrack.out1.reconMax,2)-1)/2);
pass1 = lsnr1 > th1;
pass2 = lsnr2 > th2;

fprintf('%i/%i supression in stage1\n', sum(pass1)+sum(pass2),length(pass1));
save([bkndoutputDir '/loudest.mat']);


return;
