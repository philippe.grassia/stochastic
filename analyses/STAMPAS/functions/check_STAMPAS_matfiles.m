function check_STAMPAS_matfiles(path)  
% check_STAMPAS_matfile : check matfile generation results
% DESCRIPTION :
%   basic check on the matfiles produced by anteproc and preproc
%   for STAMPAS
%
% SYNTAX :
%   check_STAMPAS_matfile (path)
% 
% INPUT : 
%    path : path to the matfiles folder (eg BKG/Start-1126073342_Stop-1137283217_ID-13) 
%
% AUTHOR : Valentin FREY    
% CONTACT : frey@lal.in2p3.fr
% VERSION : 1.0
% DATE : Oct 2016
%

path = [regexprep(path,'(.*)/matfiles','$1') '/matfiles'];
fprintf('Check matfile for folder : %s\n',path);

folder=dir([path '/*-*']);

for ff={folder.name}
  error='';
  fprintf('checking for %s ...',ff{:});
  file=dir([path '/' ff{:} '/*.mat']);
  for f={file.name}
    contains = load([path '/' ff{:} '/' f{:}]);
    
    try 
      contains.params;
    catch
      error = [error sprintf('\t%s: missing params struct\n',f{:})];
    end

    try 
      contains.map;
    catch
      error = [error sprintf('\t%s: missing map struct\n',f{:})];
    end
    
    if max(contains.map.P(:))>1 || min(contains.map.P(:))<1e-100
      error = [error sprintf('\t%s: max map.P : %g\n',max(contains.map.P(:)))];
      error = [error sprintf('\t%s: min map.P : %g\n',min(contains.map.P(:)))];
    end

    if max(contains.map.naiP(:))>1 || min(contains.map.naiP(:))<1e-100
      error = [error sprintf('\t%s: max map.naiP : %g\n',max(contains.map.naiP(:)))];
      error = [error sprintf('\t%s: min map.naiP : %g\n',min(contains.map.naiP(:)))];
    end

    if max(abs(contains.map.rbartilde(:)))>1 || min(abs(contains.map.rbartilde(:)))<1e-100
      error = [error sprintf('\t%s: max map.rbartilde : %g\n',max(abs(contains.map.rbartilde(:))))];
      error = [error sprintf('\t%s: min map.rbartilde : %g\n',min(abs(contains.map.rbartilde(:))))];
    end

    clear contains
  end
  
  if strcmp(error,'')
    fprintf(' ok \n');
  else
    fprintf(' not ok \n');
    fprintf('%s',error);
  end
end
return

