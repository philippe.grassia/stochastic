function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry,memoryJob,doGPU,lonetrackFAP)
% function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry)
%
% Writes a Condor .dag file for background studies (time-slides and zero-lag).
%
% Inputs:
%   searchPath - path to search directory
%   NShifts    - number of time-slides to do
%   idxLag     - 0 if doing zero-lag, 1 if not.
%   TOffset    - offset for time-slides (ex: set to 100 to do
%                100 time-slides starting at 101 - 200 instead of 1-100).
%   denseDAG   - if true, combine multiple science segments into a condor job.
%   nbGroupsPerJob - number of science segments per condor job.
%   retry      - number of times to retry condor job if it fails.
%   doGPU      - adds GPU condor options if needed
%
%  Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

searchPath = 'BKG/Start-1126073342_Stop-1137283217_ID-1/'; 
lonetrackFAP = 0.01;

jobfile = [searchPath '/tmp/joblistTrue.txt'];
jobs = load(jobfile);
T = length(jobs);

lonetrackPath = [searchPath '/Lonetrack/'];
bkndoutputDir = [lonetrackPath '/bknd'];
system(['mkdir -p ' bkndoutputDir]); 

filename = [bkndoutputDir '/stage1.mat'];
data_out = load(filename);

figure;
scatter(data_out.lsnr1, data_out.lsnr2, 20, data_out.snrmax1,'filled');
grid on;
xlabel('Lonetrack SNR_1');
ylabel('Lonetrack SNR_2');
c=colorbar;
ylabel(c,'Coherent SNR_2');
%xlim([0 20]);
pretty;
print('-dpng',[bkndoutputDir '/snrmax1_htrack']);
print('-depsc2',[bkndoutputDir '/snrmax1_htrack']);
close;

figure;
scatter(data_out.lsnr1, data_out.lsnr2, 20, data_out.snrmax2,'filled');
grid on;
xlabel('Lonetrack SNR_1');
ylabel('Lonetrack SNR_2');
c=colorbar;
ylabel(c,'Coherent SNR_2');
%xlim([0 20]);
pretty;
print('-dpng',[bkndoutputDir '/snrmax2_htrack']);
print('-depsc2',[bkndoutputDir '/snrmax2_htrack']);
close;



return;
