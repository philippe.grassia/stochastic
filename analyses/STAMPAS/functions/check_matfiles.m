function check_matfiles()
% This code is designed to be run from the STAMPAS/functions directory.
% Some paths are relative to that.

% Search parameters - user can change these. -----
GPS_start = 816065659;
GPS_end = 820000000;
ID = 30;
% ------------------------------------------------

% Other parameters - do not change! --------------

% Approximate number of MB required per second of data.
% Used to check .mat file sizes to make sure they are consistent
% with the file size based on the data duration.
mbPerSeg = 0.08862;

% We allow some error in file size compared to the above number.
% Acceptable error in MB for large files.
errMB = 0.2;
% Acceptable fractional error for small files.
errFrac = 0.05;

% ------------------------------------------------

% Set up search path.
searchPath = generate_searchPath('BKG',GPS_start,GPS_end,ID);
searchPath = ['../' searchPath];

% Load useful parameters.
% Get IFO pair, matfilesPath, and matfileName
%  from config_bkg.txt.
config_file = [searchPath '/config_bkg.txt'];
[cnames,cvals] = textread(config_file, '%s %s\n',-1,'commentstyle','matlab');
pair = cvals(find(strcmp(cnames,'pair')));
pair = cell2mat(pair);
matfilesPath = cvals(find(strcmpi(cnames,'matfilesLocation')));
matfilesPath = cell2mat(matfilesPath);
if (strcmpi(matfilesPath,'default'))
  matfilesPath = [searchPath '/matfiles/'];
end
matfilesName = cvals(find(strcmpi(cnames,'matfilesName')));
matfilesName = cell2mat(matfilesName);

% Setup for using find_anteproc_mats.m.
params.anteproc.jobFileTimeShift = false;
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;
params.anteproc.useCache = false;

% Set up locations of pre-processing files.
params.anteproc.inmats1 = [matfilesPath '/' pair(1) '-' pair(1:2) '_' matfilesName];
params.anteproc.inmats2 = [matfilesPath '/' pair(3) '-' pair(3:4) '_' matfilesName];

% Get number of segments per interval (nSPI) and segment duration from
% params file for detector 1 (should be same for detector 2).
pfile = [searchPath '/tmp/params_' pair(1:2) '.txt'];
[names,values] = textread(pfile, '%s %s\n',-1,'commentstyle','matlab');
nSPI = values(find(strcmpi(names,'numSegmentsPerInterval')));
nSPI = str2num(cell2mat(nSPI));
tmp = values(find(strcmpi(names,'segmentDuration')));
params.segmentDuration = str2num(cell2mat(tmp));

% Useful quantities.
segDur = params.segmentDuration/2;
bufferSecs = values(find(strcmpi(names,'bufferSecs1')));
bufferSecs = str2num(cell2mat(bufferSecs));
edgeTimeLost = ((nSPI-1)/2)*params.segmentDuration + bufferSecs;

% Load pre-processing jobfile.
jobfile = [searchPath '/tmp/joblist.txt'];
jobs = load(jobfile);

% Get jobs corresponding to GPS times of data desired.
jobs_idx = find(jobs(:,3) > GPS_start & jobs(:,2) < GPS_end);
job_start = min(jobs_idx);
job_end = max(jobs_idx);
%job_end = 1000; %%%

% Loop over jobs in jobsfile and calculate the start times of all
% ft-map columns (data segments) we should have.
% Account for time lost at the edges due to the averaging of
% adjacent segments to calculate sigma, and the buffer segments.
if (job_end == size(jobs,1))
  fprintf('Analyzing all jobs from jobfile %s.\n',jobfile);
else
  fprintf('Analyzing %i jobs from jobfile %s.\n',(job_end-job_start+1),jobfile);
end
jobfile_gps_times = [];
for ii=(job_start:job_end)
  % Start time of 1st data segment in the job.
  gps_start = jobs(ii,2) + edgeTimeLost;

  % Start time of last data segment in the job.
  gps_end = jobs(ii,3) - edgeTimeLost - params.segmentDuration;

  % Gget all data segment start times for the job.
  seg_times = gps_start:segDur:gps_end;

  % Add to full list of segment start times.
  jobfile_gps_times = [jobfile_gps_times seg_times];
end

% Remove duplicates (may occur due to slight overlap of jobs).
jobfile_gps_times = unique(jobfile_gps_times);

% Get list of pre-processed files corresponding to the GPS
% times specified.
params.startGPS = GPS_start;
params.endGPS = GPS_end;
%params.startGPS = jobs(job_start,2); %%%
%params.endGPS = jobs(job_end,3); %%%
matlist = find_anteproc_mats(params);

fprintf('\n');
% Loop over detectors.
% convert to segment start times and
% check matfile sizes
for ii=1:2

  % Set up blank arrays.
  gps_mats{ii} = [];
  fsize_probs(ii) = 0;
  err(ii) = 0;

  % Fieldname for structs.
  ifo = ['ifo' num2str(ii)];

  % Print number of pre-processed files to check.
  fprintf('Checking %i .mat files for detector %i in %s.\n', ...
	  numel(matlist.(ifo)),ii,matfilesPath);

  % Get GPS start times and durations from file names.
  gps_start_str = regexp(matlist.(ifo),'-(\d{9,10}(|\.\d+))-','tokens','once');
  gps_starts.(ifo) = cellfun(@(x) str2num(x{1}), gps_start_str);
  dur_str = regexp(matlist.(ifo),'-(\d+|\d+\.\d+)\.mat','tokens','once');
  durs.(ifo) = cellfun(@(x) str2num(x{1}), dur_str);

  % Loop over all GPS start times.
  for jj=1:numel(gps_starts.(ifo))
    % Make a list of all data segment start times.
    gps_mats{ii} = [gps_mats{ii} gps_starts.(ifo)(jj):segDur:(gps_starts.(ifo)(jj) + durs.(ifo)(jj) - params.segmentDuration)];


    % Check file size and print errors.
    temp = dir(matlist.(ifo){jj});
    expected_file_size = mbPerSeg*(durs.(ifo)(jj)*2-1);
    actual_file_size = temp.bytes/(1024^2); % in MB
    d = abs(expected_file_size-actual_file_size);
    if ((actual_file_size >= 10 & d > errMB) | ...
	actual_file_size < 10 & (d/actual_file_size) > errFrac)
      fprintf('File %s (with size %.2f MB) has size error of %.2f MB.\n', ...
	      matlist.(ifo){jj},actual_file_size,d);
      fsize_probs(ii) = fsize_probs(ii) + 1;
    end
  end

  % Print results if no file size issues.
  if (fsize_probs(ii) == 0)
    fprintf('No file size problems for detector %i .mat files.\n',ii);
  end

  % Remove any duplicate GPS times that may occur due to possible
  % overlap of pre-processed .mat files.
  gps_mats{ii} = sort(unique(gps_mats{ii}));

  fprintf('\n');

  if (numel(jobfile_gps_times) == numel(gps_mats{ii}))
    if (sum(abs(jobfile_gps_times - gps_mats{ii})) ~= 0)
      err(ii) = 1;
    end
  else
    err(ii) = 1;
  end

end
  
% Compare to segment list from jobfile.
if (err(1) & err(2))
  error('Missing data for both detectors.');
elseif (err(1))
  error('Missing data for detector 1.');
elseif (err(2))
  error('Missing data for detector 2.');
else
  fprintf('Segment times match up!\n');
end


return;