function LISTLite = shorten_jobs_list_overlap(LIST,duration,overlap,segDur,fixed_duration,add_group)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function shorten_jobs_list_overlap()
%
% This functions creates, from a source joblist, a joblist with jobs 
% of duration *duration*. An overlap between the jobs can be included.
% If *fixed_duration* is set to 0, jobs longer than *duration* but not
% long enough to be splited into two overlapping jobs are kept unique.
% If *fixed_duration* is set to 1, their duration is shorten to *duration*
% If add_group is 1, a fifth column is added in the file, identifying 
% all the jobs issued from a single job of the original joblist. 
%
%Written by: Samuel Franco (franco@lal.in2p3.fr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if ~isnumeric(duration) | ~isnumeric(overlap)
   error('Duration of jobs or overlap must be a number')
end

if overlap>=duration
   error('Overlap duration greater than jobs duration')
end

if fixed_duration~=0 & fixed_duration~=1
   display('Warning: Duration of jobs is fixed by default')
   fixed_duration=1;
end

LISTLite=[];
group=1;
job=1;

for i=1:size(LIST,1) %% loop on each science segment
  if LIST(i,4)<2*duration-overlap %%Jobs longer than threshold but impossible to split
     t_beg=LIST(i,2);
     if fixed_duration
        if LIST(i,4)<duration %% Jobs shorter than the set duration are excluded
           continue;
        end
        duration_1=duration;
     else
        duration_1=LIST(i,4);
     end
     if add_group %% add_group=0 correspond to pre-processing, and = 1 to processing jobfiles 
                  %% which need the group information (one group == one science segment)
        NLine=[job, t_beg, t_beg+duration_1-segDur/2, duration_1, group];
     else
        NLine=[job, t_beg, t_beg+duration_1, duration_1];
     end
     LISTLite=[LISTLite; NLine];
     group=group+1;
     job=job+1;
  elseif LIST(i,4)>=2*duration-overlap %%Jobs to be splited
     t_beg=LIST(i,2);
     n=floor((LIST(i,4)-duration)/(duration-overlap)); %% how many jobs will result from the splitting - 1
     for j=1:n %% loop on each jobs composing the segment, except the last one
       if add_group %% Conventions are different betweenn pre-processing and processing jobfiles, hence the difference
          NLine=[job, t_beg+(j-1)*(duration-overlap), t_beg+(j-1)*(duration-overlap)+duration-segDur/2, duration, group];
       else
          NLine=[job, t_beg+(j-1)*(duration-overlap), t_beg+(j-1)*(duration-overlap)+duration, duration];
       end
       job=job+1;
       LISTLite=[LISTLite; NLine];
     end
     j=n+1; %% we add the last job attached to this segment 
     if fixed_duration 
        rest=0;
     else
        rest=LIST(i,4)-duration-n*(duration-overlap); %% what will be added to the duration of
     end                                              %% the last resulting job
     if add_group
        NLine=[job, t_beg+(j-1)*(duration-overlap), t_beg+(j-1)*(duration-overlap)+duration-segDur/2+rest, duration+rest, group];
     else
        NLine=[job, t_beg+(j-1)*(duration-overlap), t_beg+(j-1)*(duration-overlap)+duration+rest, duration+rest];
     end
     job=job+1;
     LISTLite=[LISTLite; NLine];   
     group=group+1;
  end
end

