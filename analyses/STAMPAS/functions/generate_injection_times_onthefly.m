function [injTimesAll]= generate_injection_times(GPS_start,GPS_end,Nbinj,inj_separation,joblist,waveforms,waveform_path,blind_zone,injCont)

%
% The function draws randomly integer GPS times within [GPS_start
% GPS_end]. Each GPS time must be separated of at least
% inj_separation seconds. Injections starting at the drawn GPS
% times must overlap one of the jobs given in the joblist file. 
%

%
% Inputs: GPS_start
%         GPS_end
%         Nbinj
%         inj_separation
%         joblist
%         waveforms
%         waveform_path
%         blind_zone: duration at the beginning and the end of a
%         group of jobs where the statistic cannot be computed
%
% Outputs: injTimes : GPS start time of the injections that are in
%          at least 1 science job
%
% Marie Anne Bizouard (mabizoua@lal.in2p3.fr)
%


if injCont
  dictionary_path = [waveform_path, 'dic_ci.yml'];
  dic = classes.waveforms.dictionary(dictionary_path);
else
  dic = classes.waveforms.dictionary;
end
%% Reproducible seed
rng(GPS_start, 'twister'); 
Nbwvf=size(waveforms,1);

% Injections must be fully inside a group of jobs
jobs=zeros(joblist(end,5),5);
jobs_nb=size(jobs,1);

for i=1:joblist(end,5)
  group=joblist(joblist(:,5)==i,:);
  jobs(i,1)=i;
  jobs(i,2)=group(1,2);
  jobs(i,3)=group(end,3);
  jobs(i,4)=jobs(i,3)-jobs(i,2);
  jobs(i,5)=i;
end
cc=0;
injTimesAll=[];
outside=zeros(Nbinj,Nbwvf);

% compute the maximal number of injections
NbInjMax=floor(sum(floor(jobs(:,3)-jobs(:,2)-2*blind_zone)/inj_separation));

if Nbinj>NbInjMax
  Nbinj=NbInjMax;
  error(['The number of requested injections is higher than the' ...
	 ' maximal possible number of ' num2str(NbInjMax) ...
	'. Please reduce the number of injections.'])
else
  display(['Maximal number of injections: ' num2str(NbInjMax)])
end

Ntrial=floor((GPS_end-GPS_start)/inj_separation);

for wvf=1:Nbwvf
  Nbinj_eff=Nbinj;
  factor=1;
  count=0;

  wheader = dic.headers(waveforms{wvf});

  duration=wheader.duration;

  while Nbinj_eff<=Nbinj
    injTimes=[];
    count=count+1;

    % Draw random times between GPS_start and GPS_end
    %injTimes=round(GPS_start+(GPS_end-GPS_start)*rand(factor*Ntrial,1));
    %injTimes=sortrows(injTimes);

    jobindexes = find(duration<jobs(:,3)-jobs(:,2));
    for ii = 1:factor*Ntrial
       jobindex = ceil(rand*length(jobindexes));
       injTimes(ii) = round(jobs(jobindex,2) + rand*(jobs(jobindex,3)-jobs(jobindex,2)));
    end
    injTimes=sort(injTimes,'ascend');

    % display(num2str(size(injTimes,1)))
    % Exclude injections that fall completly outside of a science segment
    %    for i=1:size(injTimes,1)
    %      ext=find(injTimes(i)+duration>jobs(:,2)&injTimes(i)<jobs(:,3));
    %      if size(ext,1)==0
    %	     injTimes(i)=-1;
    %      end
    %    end

    % Exclude injections that are less than inj_separation seconds from
    % each other
    i=1;
    j=1;
    nb=length(injTimes);
    while(i<nb & i==j)
      j=i+1;
      %      display([num2str(i) ' ' num2str(j) ' ' num2str(nb)])
      while(j<=nb)
       if injTimes(j)-injTimes(i)<inj_separation
          injTimes(j)=-1;
       else
         i=j;
         break;
       end
       j=j+1;
      end
    end

%    display(num2str(size(injTimes,1)))
    injTimes=injTimes(injTimes>0);

    % Exclude injections that are not fully inside a group of jobs
    for i=1:length(injTimes)
      ext=find(injTimes(i)+duration<=jobs(:,3)-blind_zone & injTimes(i)>=jobs(:,2)+blind_zone);
      if length(ext)==0
	injTimes(i)=-1;
      end
    end

    injTimes=injTimes(injTimes>0)';
    %display([num2str(size(injTimes,1)) ' ' num2str(max(1,size(injTimes,1))/Nbinj_eff)]);

    if factor>10 & max(1,length(injTimes))/Nbinj_eff<1.0001
      error(['Not converging processus. You may require too many' ...
	     ' injections given the analyzed period']);
      display(['Ntrial:' num2str(Ntrial) ' ' ...
	       'Nbinj_eff:' num2str(Nbinj_eff) ' ' ...
	       'Nbinj:' num2str(Nbinj) ' '])
      break;
    end

    Nbinj_eff=max(1,length(injTimes));
    factor=factor+1;
  end

  if length(find(diff(injTimes)<inj_separation))>0
    error(['There are still injections seperated less than ' ...
	   num2str(inj_separation) ' s from each other']);
    ext=find(diff(injTimes)<inj_separation)
    display([num2str(injTimes(ext)) ' ' num2str(injTimes(ext+1))])
  end
  injTimesAll=[injTimesAll sort(injTimes(randperm(Nbinj_eff, Nbinj)))];

end

