function [injections,waveformPath] = injection_continuum(searchPath,waveforms,numSample, flow, fhigh)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This script randomly samples injection parameter space for a "continuum" of injections 
%% with STAMPAS. Right now, this is designed to work only with on the fly injections. It 
%% ramdonly picks parameters from the parameter space and creates the necessary mat files 
%% in the dag folder. It also writes the waveforms.txt folder and creates a temporary dic.yml
%% file for post processing. 
%% 
%% Sharan Banagiri (sharan.banagiri@ligo.org)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for ii=1:size(waveforms, 1)
    wvf_name  = waveforms{ii, 1};
    if strcmp(wvf_name(end-3:end), '.mat') || strcmp(wvf_name(end-3:end), '.dat') || strcmp(wvf_name(end-3:end), '.txt')
       error('Error using the specified waveforms in contiuum injectons. If you directly specify the injection file, please set the injCont parameter in your config file to false')
    end
end

% make a folder for storing the generated waveforms
waveformPath = [searchPath, '/ContWaveforms/'];
system(['mkdir -p ' waveformPath]);

% create a waveforms file
fid = fopen([waveformPath, '/waveforms.txt'], 'w+');
injections = cell(numSample, size(waveforms, 2));

% create a temporary dictionary file, dic.yml
fid_dic = fopen([waveformPath, 'dic_ci.yml'], 'w+');

addpath(genpath('contInjections'));
cnt = 1;
for jj=1:size(waveforms, 1)
    switch waveforms{jj, 1}
	   
      case 'msmagnetar'
	% addpath('./contInjections/msMagnetar');
	fprintf('Setting up msmagnetar continuum injections ... \n')
	
	fprintf(fid_dic, 'msmagnetar:\n');
	fprintf(fid_dic, '  info:\n');
	fprintf(fid_dic, '    xlabel: distance\n');
	fprintf(fid_dic,'\n');
	%% The msmagnetar model has three parameters of interest, which are initial frequency f0 , time scale tau ad breaking index nn
	%% At each chosen point in the parameter space, the number of injections done is the NumberOfInjections value in the config file.
   
	%% We random sample over frequency space
	flow1 = max(500,flow);
	fhigh1 = min(fhigh, 2000);
	f0s = flow1 + (fhigh1 - flow1)* rand(numSample, 1);
	
	%% We random sample the logarithm of the time scale scale between 10^3 seconds to 10^5 seconds
	ttlow = 2.69; 
	tthigh = 4.69;
	tts = ttlow  + (tthigh  - ttlow)*rand(numSample, 1);
	taus = 10.^tts;
	
	%% We assume that the braking index is discrete; i.e nn = 2.5, 3 or 5
	%% corresponsing to observed, magnetic dipole and GW quadrapole values respectively. 
	nns = datasample([2.5, 3, 5], numSample);
	
	
	for ii = 1:numSample
	  f0 = f0s(ii);
	  tau = taus(ii);
          nn = nns(ii);
	  [duration,fmin,fmax,hrss,energy,distance,epsilon] = genmsmagnetar(f0,tau,nn);
	  filename = sprintf('msmagnetar%d.mat',ii);
	  tprnt = [filename];
	  for kk = 2:size(waveforms, 2)
	      tprnt = [tprnt ' ' waveforms{jj, kk}];
	  end
	  injections{cnt, 1} = filename;
	  injections(cnt, 2:end) = waveforms(jj,2:end);
	  tprnt = [tprnt '\n'];
	  fprintf(fid, tprnt);
	  filePath = [waveformPath,  filename];
	  save(filePath, 'f0', 'tau', 'epsilon', 'duration', 'fmin', 'fmax', 'hrss', 'nn', 'filename', 'filePath');

	  
	  %% Now we write to the dictionary file. 
	  fprintf(fid_dic, ['  ', 'msmagnetar', num2str(ii), ':\n']);
	  fprintf(fid_dic, ['    legend: MS magnetar ', num2str(ii), '\n']);
	  fprintf(fid_dic, ['    file: ', filename, '\n']);
	  fprintf(fid_dic, ['    fmin: ', num2str(fmin), '\n']);
	  fprintf(fid_dic, ['    fmax: ', num2str(fmax), '\n']);
	  fprintf(fid_dic, ['    energy: ', num2str(energy), '\n']);
	  fprintf(fid_dic, ['    duration: ', num2str(duration), '\n']);
	  fprintf(fid_dic, ['    hrss: ', num2str(hrss), '\n']);
	  fprintf(fid_dic, ['    distance: ', num2str(distance), '\n']);
	  fprintf(fid_dic,  '    constructor: msmagnetar_injection\n' );
	  fprintf(fid_dic,  '    type: onthefly\n');
	  fprintf(fid_dic, '\n');
	  
	  cnt = cnt+1;
	end
	
	   
      case 'sinusoid'
	fprintf('Setting up sinusoidal continuum injections ... \n');

	fprintf(fid_dic, 'sinusoid:\n');
        fprintf(fid_dic, '  info:\n');
        fprintf(fid_dic, '    xlabel: hrss\n');
	fprintf(fid_dic, '\n');
	%% We random sample over frequency space
        flow1 = max(100,flow);
        fhigh1 = min(fhigh, 2000);
	f0s = flow1 + (fhigh1 - flow1)* rand(numSample, 1);   

	% we chose fdot to be between -0.1 to 0.1 Hz/second
	fdots = -0.1 + 0.2*rand(numSample, 1);

	%% We random sample the logarithm of the duration, between 3000 seconds to 13,000 seconds
        tlow = 3.5;
        thigh = 4.11;
        ts = tlow  + (thigh  - tlow)*rand(numSample, 1);
        durations = 2*10.^ts;

	%% We want a fiducial hrss of 1e-20
	h0 = 1e-20;
	phi0  = 0;

	for ii = 1:numSample
	  f0 = f0s(ii);
	  duration = durations(ii);
	  fdot = fdots(ii);
	  [hrss,fmin,fmax,energy,distance, h0] = gensinInj(f0,fdot,duration,h0,phi0);
	  
	  filename = sprintf('sinusoid%d.mat',ii);
          matnames{jj, ii} = filename;
	  tprnt = [filename];
          for kk = 2:size(waveforms, 2)
              tprnt = [tprnt ' ' waveforms{jj, kk}];
          end
	  injections{cnt, 1} = filename;
	  injections(cnt, 2:end) = waveforms(jj, 2:end);
          tprnt = [tprnt '\n'];
          fprintf(fid, tprnt);
	  filePath = [waveformPath,  filename];
	  save(filePath, 'f0', 'duration','fdot', 'fmin', 'fmax', 'hrss','h0','phi0','filename', 'filePath');
	  
	  %% Now we write to the dictionary file.
          fprintf(fid_dic, ['  ', 'sinusoid', num2str(ii), ':\n']);
          fprintf(fid_dic, ['    legend: sinusoid ', num2str(ii), '\n']);
          fprintf(fid_dic, ['    file: ', filename, '\n']);
          fprintf(fid_dic, ['    fmin: ', num2str(fmin), '\n']);
          fprintf(fid_dic, ['    fmax: ', num2str(fmax), '\n']);
          fprintf(fid_dic, ['    energy: ', num2str(energy), '\n']);
          fprintf(fid_dic, ['    duration: ', num2str(duration), '\n']);
          fprintf(fid_dic, ['    hrss: ', num2str(hrss), '\n']);
          fprintf(fid_dic, ['    distance: ', num2str(distance), '\n']);
          fprintf(fid_dic,  '    constructor: sinusoid_injection\n' );
          fprintf(fid_dic,  '    type: onthefly\n');
          fprintf(fid_dic, '\n');

	  cnt = cnt +1;
	end

      case 'rmodes'
	fprintf('Setting up rmodes continuum injections ... \n')

        fprintf(fid_dic, 'rmodes:\n');
        fprintf(fid_dic, '  info:\n');
        fprintf(fid_dic, '    xlabel: distance\n');
        fprintf(fid_dic,'\n');
        %% The rmode model has only two parameters of interest, which are a frequency scale f0 and duration
        %% At each chosen point in the parameter space, the number of injections done is the NumberOfInjections value in the config file.

        %% We random sample over frequency space
        flow1 = max(500,flow);
        fhigh1 = min(fhigh, 2000);
        f0s = flow1 + (fhigh1 - flow1)* rand(numSample, 1);

        %% We random sample the logarithm of the time scale scale between 10^2 seconds to 10^3 seconds
        ttlow = 2;
        tthigh = 4;
        tts = ttlow  + (tthigh  - ttlow)*rand(numSample, 1);
        durations = 10.^tts;

	for ii = 1:numSample
	  f0 = f0s(ii);
	  duration = durations(ii); 
	  [fmin, fmax, hrss, energy, distance,alpha] = genrmodes(f0, duration);
	  filename = sprintf('rmodes%d.mat',ii);
	  tprnt = [filename];
          for kk = 2:size(waveforms, 2)
            tprnt = [tprnt ' ' waveforms{jj, kk}];
          end
          injections{cnt, 1} = filename;
          injections(cnt, 2:end) = waveforms(jj,2:end);
          tprnt = [tprnt '\n'];
          fprintf(fid, tprnt);
          filePath = [waveformPath, filename];
	  dist = distance;
	  save(filePath, 'f0', 'alpha', 'dist', 'duration', 'fmin', 'fmax', 'hrss', 'filename', 'filePath');

	  %% Now we write to the dictionary file.
          fprintf(fid_dic, ['  ', 'rmodes', num2str(ii), ':\n']);
          fprintf(fid_dic, ['    legend: rmodes ', num2str(ii), '\n']);
          fprintf(fid_dic, ['    file: ', filename, '\n']);
          fprintf(fid_dic, ['    fmin: ', num2str(fmin), '\n']);
          fprintf(fid_dic, ['    fmax: ', num2str(fmax), '\n']);
          fprintf(fid_dic, ['    energy: ', num2str(energy), '\n']);
          fprintf(fid_dic, ['    duration: ', num2str(duration), '\n']);
          fprintf(fid_dic, ['    hrss: ', num2str(hrss), '\n']);
          fprintf(fid_dic, ['    distance: ', num2str(distance), '\n']);
	  fprintf(fid_dic, ['    alphas: ', num2str(alpha), '\n']);
          fprintf(fid_dic,  '    constructor: rmodes_injection\n' );
          fprintf(fid_dic,  '    type: onthefly\n');
          fprintf(fid_dic, '\n');

	  cnt = cnt + 1;

	end


      case 'inspiral'
	fprintf('Setting up inspiral injections ... \n')

        fprintf(fid_dic, 'inspiral:\n');
        fprintf(fid_dic, '  info:\n');
        fprintf(fid_dic, '    xlabel: distance\n');
        fprintf(fid_dic,'\n');
        %% The inspiral model has only two (three) parameters of interest, which are the masses m1 and m2
        %% At each chosen point in the parameter space, the number of injections done is the NumberOfInjections value in the config file.

        %% We random sample over frequency space
        masses1 = 10*rand(numSample, 1);
        masses2 = 10*rand(numSample, 1);


	for ii = 1:numSample
	  mass1 = masses1(ii);
	  mass2 = masses2(ii); 

	  % Should iota always be zero?
	  iota = 0;

	  % distantce = 10 Mpc
	  distance = 10;

	  [duration,fmin, fmax, hrss, energy] = geninspiral(mass1,mass2,iota,distance);

	  filename = sprintf('inspiral%d.mat',ii);
	  tprnt = [filename];
          for kk = 2:size(waveforms, 2)
            tprnt = [tprnt ' ' waveforms{jj, kk}];
          end
          injections{cnt, 1} = filename;
          injections(cnt, 2:end) = waveforms(jj,2:end);
          tprnt = [tprnt '\n'];
          fprintf(fid, tprnt);
          filePath = [waveformPath, filename];
	  dist = distance;
	  save(filePath, 'f0', 'dist', 'duration', 'fmin', 'fmax', 'hrss','mass1', 'mass2', 'iota', 'filename', 'filePath');

	  %% Now we write to the dictionary file.
          fprintf(fid_dic, ['  ', 'inspiral', num2str(ii), ':\n']);
          fprintf(fid_dic, ['    legend: inspiral ', num2str(ii), '\n']);
          fprintf(fid_dic, ['    file: ', filename, '\n']);
          fprintf(fid_dic, ['    fmin: ', num2str(fmin), '\n']);
          fprintf(fid_dic, ['    fmax: ', num2str(fmax), '\n']);
          fprintf(fid_dic, ['    energy: ', num2str(energy), '\n']);
          fprintf(fid_dic, ['    duration: ', num2str(duration), '\n']);
          fprintf(fid_dic, ['    hrss: ', num2str(hrss), '\n']);
          fprintf(fid_dic, ['    distance: ', num2str(distance), '\n']);
          fprintf(fid_dic,  '    constructor: inspiral_injection\n' );
          fprintf(fid_dic,  '    type: onthefly\n');
          fprintf(fid_dic, '\n');

	  cnt = cnt + 1;

	end





	   

    end
end

%% close waveforms.txt and dic.yml
fclose(fid);
fclose(fid_dic);

end
