function mf=compute_matched_filter(params,psd1,psd2)

% Get the simulation iLIGO
psd=load('PSDs/aLIGO_O1PSD_6Hz.txt');

% Get waveform amplitude at the source and build the strain amplitude at each IFO
stamp_inj.file=params.pass.stamp.file;

if strcmpi(params.pass.stamp.inj_type, 'fly')
  params.pass.stamp.dataStartGPS = params.pass.stamp.start;
  params.pass.stamp.dataEndGPS = params.pass.stamp.start + params.pass.stamp.duration;
end

% Get hp and hx of the injection
[hp, hx]=load_injection(params, stamp_inj, params.sampleRate);

params.pass.stamp.hp=hp;
params.pass.stamp.hx=hx;

% Add default params values
params.fixAntennaFactors=0;
params.bestAntennaFactors=0;

% end GSP time is fixed to t_start + size of the map (500s)
params.startGPS=params.job.startGPS;
params.endGPS=params.job.startGPS+params.mapsize;


% Build the strain signal in both IFOs, for the source location and
% start time.
% h1 and h2 are scaled by sqrt(alpha) but not hx and hp
display(['Injection start time: ' num2str(params.job.startGPS)])
params= getPointSourceData_IM(params, params.job.startGPS, params.sampleRate);

h1=params.pass.stamp.h1;
h2=params.pass.stamp.h2;

% Compute SNRs with iLIGO simulated PSD
flow=params.flow;
fhigh=params.fhigh;

SNR1=matchedfilter(h1,h1,psd,params.sampleRate,flow,fhigh);
SNR2=matchedfilter(h2,h2,psd,params.sampleRate,flow,fhigh);

% Compute SNRs with real data PSDs

SNR1s=matchedfilter(h1,h1,psd1,params.sampleRate,flow,fhigh);
SNR2s=matchedfilter(h2,h2,psd2,params.sampleRate,flow,fhigh);

% Get waveform duration
output=strsplit(params.pass.stamp.file,'/');
waveform_path=output{1};
waveform_name=output{2};

% Compute hrss
hrss=get_hrss(hp,hx,params.sampleRate,params.pass.stamp.alpha);

% Save parameters
mf.ra=params.pass.stamp.ra;
mf.decl=params.pass.stamp.decl;
mf.startGPS=params.pass.stamp.startGPS;
mf.name=params.pass.stamp.file;
mf.inj_type=params.pass.stamp.inj_type;
mf.iota=params.pass.stamp.iota;
mf.psi=params.pass.stamp.psi;
mf.F1=params.pass.stamp.F1;
mf.F2=params.pass.stamp.F2;
mf.SNR1=SNR1;
mf.SNR2=SNR2;
mf.SNR1s=SNR1s;
mf.SNR2s=SNR2s;
mf.hrss=hrss;

