function run=get_run_from_GPStimes (GPS_start, GPS_end)

% 
% Fuction to get the run name from the start and end times
%
% Contact: Marie Anne Bizouard (mabizoua@lal.in2p3.fr) 2015
%

run='';
if GPS_start >= 814700000 & GPS_end <= 880000000
  run='S5';
end

if GPS_start >= 880000000 & GPS_end <= 1000000000
  run='S6';
end

if GPS_start >= 1116700000 & GPS_end <= 1118400000
  run='ER7';
end

if GPS_start >= 1123804817 & GPS_end <= 1126051217
  run='ER8';
end

if GPS_start >= 1126051217 & GPS_end <= 1137283217
  run='O1';
end

if GPS_start >= 1164067217 & GPS_end <= 1187733618
  run='O2';
end

if GPS_start >= 1235433617
  run='O3';
end
