function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry,memoryJob,doGPU,doLonetrack,scratchPath)
% function make_dag_bkg_anteproc(searchPath,NShifts,idxLag,TOffset,denseDAG,nbGroupsPerJob,retry)
%
% Writes a Condor .dag file for background studies (time-slides and zero-lag).
%
% Inputs:
%   searchPath - path to search directory
%   NShifts    - number of time-slides to do
%   idxLag     - 0 if doing zero-lag, 1 if not.
%   TOffset    - offset for time-slides (ex: set to 100 to do
%                100 time-slides starting at 101 - 200 instead of 1-100).
%   denseDAG   - if true, combine multiple science segments into a condor job.
%   nbGroupsPerJob - number of science segments per condor job.
%   retry      - number of times to retry condor job if it fails.
%   doGPU      - adds GPU condor options if needed
%
%  Written by Tanner Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Open file for writing.
fid=fopen([searchPath '/stamp_allsky_bkg.dag'],'w+');


% Set up lag numbers (add 0 if doing zero-lag)
timeslides = (1+TOffset):(NShifts+TOffset);
if ~(idxLag)
  timeslides = [0 timeslides];
end

% Loop over time-slides.
firstTimeSlide = true;
job_number = 1;
for jj=timeslides
  % Load correct jobfile and get information.
  if (jj == 0) % zero-lag
    jobfile = [searchPath '/tmp/joblistTrue.txt'];
    jobs = load(jobfile);
  elseif (jj ~= 0 & firstTimeSlide)
    % First time-slide, need to load correct jobfile.
    % Only want to do this once (hence the firstTimeSlide variable).
    jobfile = [searchPath '/tmp/jobfile_bknd.txt'];
    jobs = load(jobfile);
    firstTimeSlide = false;
  end
  
  % Define job_idx (list of first analysis job in each condor job)
  if (denseDAG) % Group multiple science segments into a single condor job.
    % Indices of all science segments.
    job_idx = unique(jobs(:,5))';
    % Keep only the first one in each group of science segments.
    job_idx = job_idx(1):nbGroupsPerJob:job_idx(end);
  else % One condor job per analysis job.
    job_idx = 1:size(jobs,1); % Number of jobs
  end

  for ii=job_idx
    % Get first analysis job to be analyzed by the Condor job
    % and the number of jobs in the group.
    if (denseDAG)
      jobs_in_group = find(jobs(:,5)>=ii & jobs(:,5) < (ii+nbGroupsPerJob));
      job1 = jobs_in_group(1);
      groupLength = numel(jobs_in_group);
    else
      job1 = ii;
      groupLength = 1;
    end
    
    % Print to file.
    if doGPU==1
      fprintf(fid,'JOB %d stamp_allsky_bkg_gpu.sub\n',job_number);
    else
      fprintf(fid,'JOB %d stamp_allsky_bkg.sub\n',job_number);
    end

    fprintf(fid,'RETRY %d %d\n',job_number,retry);
    fprintf(fid,['VARS %d jobnumb="%d" mem="%d" searchPath="%s" paramsFile="%s"' ...
		 ' jobsFile="%s" lagNumber="%d" jobNumber="%d"' ...
		 ' groupLength="%d" scratchPath="%s" '],job_number,job_number,memoryJob,...
	    searchPath,[searchPath '/tmp/params_H1.txt'], ...
	    jobfile, jj, job1, groupLength,scratchPath);
    fprintf(fid,'\n\n');
    job_number = job_number + 1;    
  end
end


% Print PARENT/CHILD syntax so that all jobs depend on the
% first job.  This is to prevent us from running all jobs
% if the first job fails (usually indicates configuration
% problems).
%Njobs = job_number - 1;
%if Njobs>1
%  fprintf(fid,'\nPARENT 1 CHILD ');
%  for k=2:Njobs
%    fprintf(fid,' %d ',k);
%  end
%end



% Close the file.
fclose(fid);

return;
