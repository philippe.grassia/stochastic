function w = cfreqz_freqvec(nfft, Fs, s)
%CFREQZ_FREQVEC Returns the frequency vector for the given NFFT
%       - extract for compilation -
%   Inputs:
%       nfft    -   The number of points
%       Fs      -   The sampling frequency of the filter
%       s       -   1 = 0-2pi, 2 = 0-pi

%   Author(s): J. Schickler
%   Copyright 1988-2002 The MathWorks, Inc.
%   $Revision: 1.1 $  $Date: 2004/09/12 23:16:36 $ 

if nargin < 2, Fs = []; end
if nargin < 3, s  = 2; end

w = 0:2.*pi./(s.*nfft):2.*pi./s-1./nfft;

if ~isempty(Fs), % Fs was given, return freq. in Hz
    w = w.*Fs./(2.*pi); % Convert from rad/sample to Hz      
end

% [EOF]
