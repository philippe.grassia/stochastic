function y = cdividenowarn(num,den)
% CDIVIDENOWARN Divides two polynomials while suppressing warnings.
%     - extracted for compilation
% CDIVIDENOWARN(NUM,DEN) array divides two polynomials but suppresses warnings 
% to avoid "Divide by zero" warnings.

%   Copyright 1988-2002 The MathWorks, Inc.
%   $Revision: 1.1 $  $Date: 2004/09/12 23:16:36 $

s = warning; % Cache warning state
warning off  % Avoid "Divide by zero" warnings
y = (num./den);
warning(s);  % Reset warning state

% [EOF] dividenowarn.m
