function stamp_pem_anteproc_loop(paramsFile,channelListFile,startGPS,endGPS,number)
%-------------------------
% stamp_pem_anteproc_loop(paramsFile,channelListFile,startGPS,endGPS,number)
% 
% Either pre-processes DARM channel from channelListFile (if last parameter is 0) or
% does STAMP analysis on channels (number-1)*10+1 ---> (number*10) of channelListFile
% assuming DARM has already been pre-processed (and is in location specified in 
% outputFilePrefix of paramsFile)
% 
% This is primarily used as a wrapper script that is compiled and submitted for 
% runs over bulk amounts of data.
% 
% PARAMETERS:
% 	paramsFile : str
%		     	STAMP parameter file setting
% 			segment duration, deltaF, output Directory, etc.
% 	channelListfile : str
% 			list of channels. will calculate coherence with first
% 			channel in file.
% 	startGPS : int
% 			start GPS time
% 	endGPS : int
% 			end GPS time
% 	number : int 
% 			channel to start with in channel list file
% 			if 0 will preprocess DARM channel
% 			if >0, will calculate coherence with DARM for 
% 			channels (number-1)*10+1 -> (number*10). 
% RETURNS: 
% 	NONE, this script submits things. To see output go to outputFilePrefix 
% 	directoy specified in "paramsFile"
%--------------------------

if isstr(number)
  number = str2num(number);
  startGPS = str2num(startGPS);
  endGPS = str2num(endGPS);
end

[channelNames,channelRates] = textread(channelListFile,'%s %f');
params = readParamsFromFilePEM(paramsFile);
outDir = [params.outputFilePrefix '/' num2str(startGPS) '_' num2str(endGPS) '/' strrep(channelNames{1},':','-')];
if exist(outDir)~=7
    mkdir ([outDir]);
end
fid = fopen([outDir '/failed_channels-' num2str(number) '.txt'],'w+');

cjobs = [(((number-1)*10)+1):number*10];
if number == 0;
	cluster_out = doAnteprocPEM(paramsFile,channelNames{1},channelNames{1},startGPS,endGPS,0);
	return;
end

for ii=1:length(cjobs)
  fprintf('running job %d...\n',cjobs(ii));
  if cjobs(ii)<=length(channelNames);
    if cjobs(ii)==1
	continue;
    end
    try
     cluster_out = doAnteprocPEM(paramsFile,channelNames{1},channelNames{cjobs(ii)},startGPS,endGPS,1);
    catch err
        fprintf('channel %s failed with error %s, continuing on...\n',channelNames{cjobs(ii)},err.message);
        fprintf(fid,'%s %f %s\n',channelNames{cjobs(ii)},channelRates(cjobs(ii)),err.message);
    end
  end
end
fclose(fid);
