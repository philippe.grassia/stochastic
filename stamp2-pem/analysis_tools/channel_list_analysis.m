function channel_list_analysis(paramsFile,channelList,startGPS,endGPS,darmProcessed)
% 
% channel_list_analysis(paramsFile,channelList,startGPS,endGPS,darmProcessed)
%
% runs stamp analysis on a set of channels. "DARM" channel is assumed to be first in the list.
% Will create a separate page for each channel summarizing results and then a summary page 
% with links to each of those channels and a time-averaged coherence matrix displaying all of the 
% channel's coherences.
% 
% 
% PARAMETERS:
% ----------
% 	paramsFile : str
% 		location of parameter file specifying parameters for stamp-pem run
% 	channelList : str
% 		location of channel list specifying parameters for stamp-pem run
% 		(in "channelName sampleRate" format, but sampleRate can be any number 
% 		at this point. It will be written over with value from frames.)
%		"DARM" (or channel with which all others are compared) is assumed to be
%		first channel in the list.
%
%	startGPS : int
% 		startGPS time
% 	endGPS : int
% 		endGPS time for analysis
% 	darmProcessed : bool (optional)
%		If you know that the darm data has already been processed for these
% 		GPS times and the data exists on the outputFilePrefix path then
% 		set this to true and it might save you some time. If you're not sure
% 		set it to false and DARM will be pre-processed on the fly.
% RETURNS:
% -------
% 	NONE
% 
%
% CONTACT: patrick.meyers@LIGO.org
% -------

try 
  darmProcessed;
catch
  darmProcessed = 0;
end

[channelNames,smplrate] = textread(channelList,'%s %d');
DARM = channelNames{1};
for ii = 1:length(channelNames)
	pem_channel = channelNames{ii};
	out = two_channel_coherence(paramsFile,DARM,pem_channel,startGPS,endGPS,darmProcessed);
end
stamp_pem_summary_html(paramsFile,channelList,startGPS,endGPS);
[coherenceMatFile] = harvestAndSaveCoherenceData(paramsFile,channelList,startGPS,endGPS);
