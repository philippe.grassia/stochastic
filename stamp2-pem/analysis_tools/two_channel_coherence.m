function [out] = two_channel_coherence(paramsFile,channel1,channel2,startGPS,endGPS,darmProcessed)
% 
% channel_list_analysis(paramsFile,channelList,startGPS,endGPS,darmProcessed)
%
% runs stamp analysis on two channels. Will create a summary html page and outputs
% data in struct form.
% 
% 
% PARAMETERS:
% ----------
%       paramsFile : str
%               location of parameter file specifying parameters for stamp-pem run
%       channel1 : str
%               name of first channel for coherence
%       channel2 : str
%               name of second channel for coherence
%       startGPS : int
%               startGPS time
%       endGPS : int
%               endGPS time for analysis
%       darmProcessed : bool (optional)
%               If you know that the darm data has already been processed for these
%               GPS times and the data exists on the outputFilePrefix path then
%               set this to true and it might save you some time. If you're not sure
%               set it to false and DARM will be pre-processed on the fly.
% RETURNS:
% -------
%       NONE
% 
%
% CONTACT: patrick.meyers@LIGO.org
% -------


% check if darmProcessed is set
% if not, assume we need to process
% DARM data
try 
darmProcessed;
catch
darmProcessed=0;
end

% run doAnteprocPEM
out = doAnteprocPEM(paramsFile,channel1,channel2,startGPS,endGPS,darmProcessed);
params = readParamsFromFilePEM(paramsFile);
params.startGPS = startGPS;
params.endGPS = endGPS;


% create page for this aux channel
stamp_pem_chnl_html(params,channel1,channel2);
params.htmlpath = [params.outputFilePrefix '/' num2str(params.startGPS) '_' num2str(params.endGPS) '/' strrep(channel1,':','-') '/' strrep(channel2,':','-')];

fprintf('You can find the page at\n%s\n',[params.htmlpath '/pem.html']);
