function stamp_pem(paramsFile, channlesListFile, startGPS, endGPS)
% function stamp_pem(pemParamsFile, channlesListFile, startGPS, endGPS)
% DEPRECATED FOR RUNS OVER LARGE CHANNEL LISTS: 
% USE  stamp_pem_anteproc_loop.m OR stamp_pem_anteproc.m FOR LARGE
% CHANNEL LISTS!
%------------------------
% Given a STAMP-PEM parameter file, channelsListfile, start and 
% end GPS times this code generates coherence plots. 
% The output of this code could be used in conjuction with other stamp codes 
% (such as various clustering algorithms) to further analyse the data
% ----------------------
% This code either performs single STAMP jobs (pre-processing 
% both channels on the fly) or submits a .dag file to the cluster that 
% has each job as an individual stamp job.
% This does NOT utilize the "anteproc" functionality and is therefore very slow
% for long channel lists.
% DEPRECATED FOR LARGE CHANNEL LISTS
% Routine written by Michael Coughlin and Shivaraj Kandhasamy and Patrick Meyers.
% Contact: patrick.meyers@ligo.org,michael.coughlin@ligo.org, shivaraj.kandhasamy@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% To use it as a compiled code
startGPS = strassign(startGPS);
endGPS = strassign(endGPS);

%set threshhold for interesting coherence information
%[gpsStart,gpsEnd]=stamp_pem_seg_check(startGPS, endGPS);

% Extract parameters from input parameter file 
params = readParamsFromFilePEM(paramsFile);

% Read in channels list
if exist(channlesListFile)
  [channelNames, channelSampleRates] = ...
    textread(channlesListFile,'%s %f');
else
  error ('No channels list provided \n');
end

% Number of completed channels
completedChannels = 0;

% Number of channels for which completion is desired; It could be less than
% the number of channesl in the channels list file; Curretly we use all
% the channels in the given file
desiredChannels = length(channelNames);

% Indexes of completed channels
whichChannels = [];

% Decide whether to run on condor or not
if (endGPS-startGPS >= 100) && desiredChannels >= 11
  params.runCondor = 1;
end
if params.runCondor
  % intialize counter for condor jobs
  cc = 1;
end

% create master output directory
%
outDir = [params.outputFilePrefix '/' num2str(startGPS) '_' num2str(endGPS) '/' strrep(channelNames{1},':','-')];
if exist(outDir)~=7
  mkdir ([outDir]);
end
fhigh = params.fhigh;
% Loop over channels
for ii = 1:length(channelNames) % self coherence is also calculated
  % Check whether the sampling rate for the channel is large enough and
  % the desiredChannels limit has not been exceeded
  params.resampleRate1 = min(channelSampleRates(ii), channelSampleRates(1));
  params.resampleRate2 = params.resampleRate1;
  params.fhigh = min(fhigh,(params.resampleRate1/2 - 1));
  if completedChannels < desiredChannels 
    % Run STAMP-PEM pipeline
    if params.runCondor
      if cc==1
        dagFilename = ['STAMP-PEM' '_' num2str(startGPS) '_' num2str(endGPS) '.dag'];
        fprintf('Creating dag file %s ...', dagFilename);
        fid = fopen(dagFilename,'w');
        if exist(['logfiles/'])~=7
          mkdir (['logfiles/']);
        end
      end
      fprintf(fid,'JOB %d %s\n',cc,'clustermapPEM.sub');
      %fprintf(fid,'RETRY %d 3\n',cc);
      temp = ['VARS ' num2str(cc) ' jobNumber="' num2str(cc) '" paramsFile="' paramsFile '" channelName1="' channelNames{1} '" channelName2="' channelNames{ii} '" startGPS="' num2str(startGPS) '" endGPS="' num2str(endGPS) '"'];
      fprintf(fid,'%s\n\n',temp);
      cc = cc + 1;
    else             
      try
      cluster_out = clustermapPEM(paramsFile, channelNames{1}, channelNames{ii}, startGPS, endGPS);
      catch
      fprintf('%s failed. Continuing on...\n',channelNames{ii});
      end
      completedChannels = completedChannels + 1;
      whichChannels = [whichChannels ii];
    end
  else
    fprintf('The channel %s is not used \n', channelNames{ii});
  end
end
fprintf(' Done.\n');

if params.runCondor
  fprintf(fid,'JOB %d %s\n',cc,'stamp_pem_summary_html.sub');
  temp = ['VARS ' num2str(cc) ' jobNumber="' num2str(cc) '" paramsFile="' paramsFile '" channlesListFile="' channlesListFile  '" startGPS="' num2str(startGPS) '" endGPS="' num2str(endGPS) '"'];
  fprintf(fid,'%s\n\n',temp);
  temp2 = ['PARENT ' num2str(1:cc-1) ' CHILD ' num2str(cc)];
  fprintf(fid, temp2);
  fclose(fid);
  
  [exeNAvailable, msg] = system(['ls stamp_functions/clustermapPEM']);
  if exeNAvailable
    fprintf('Exe file stamp_functions/clustermapPEM not available, creating it ... \n');
    mcc -R -nodisplay -m clustermapPEM -d stamp_functions;
    fprintf(' Done.\n');
  end

  [exeNAvailable, msg] = system(['ls stamp_pem_summary_html']);
  if exeNAvailable
    fprintf('Exe file stamp_pem_summary_html not available, creating it ... \n');
    mcc -R -nodisplay -m stamp_pem_summary_html;
    fprintf(' Done.\n');
  end

  fprintf('Submitting dag file to condor. \n');
  [nouse1, nouse2] = system(['condor_submit_dag -maxjobs 100 ' dagFilename]);
  % estimated wait time; 60 sec data for one channel takes ~30 sec; 180 misc 
  % overhead time
  est_time = (endGPS-startGPS)*30/60*max(desiredChannels/100,1)+180;
  fprintf('Results will be available in ~%d sec. If not check the status of the dag file ''%s'' \n', round(est_time), dagFilename); 
  fprintf('Results will be available at ''%s'' .\n',[params.outputFilePrefix '/' num2str(startGPS) '_' num2str(endGPS) '/' strrep(channelNames{1},':','-') '/summary.html']);
else
  stamp_pem_summary_html(paramsFile, channlesListFile, startGPS, endGPS);
end  
 
return;
