function stamp_pem_chnl_html(params, channelName1, channelName2)
% function stamp_pem_page(params, cluster_out)
% Given a STAMP params struct and clustermap output struct, 
% creates a HTML summary page
% Routine written by Michael Coughlin.
% Modified: July 31, 2012
% Contact: michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Output path for HTML file
params.htmlpath = [params.outputFilePrefix '/' num2str(params.startGPS) '_' num2str(params.endGPS) '/' strrep(channelName1,':','-') '/' strrep(channelName2,':','-')];

% Open HTML page for writing
fid=fopen([params.htmlpath '/pem.html'],'w+');

% HTML Header
fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
fprintf(fid,'\n');
fprintf(fid,'<html>\n');
fprintf(fid,'\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<title>PEM Page for %s: %d - %d</title>\n',strrep(channelName2,':','-'),params.startGPS,params.endGPS);
fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../main.css">\n');
fprintf(fid,'<script src="../../sorttable.js"></script>\n');
fprintf(fid,'</head>\n');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">STAMP-PEM page for channels %s and %s : %d - %d</span></big></big></big></big><br>\n', strrep(channelName2,':','-'),  strrep(channelName1,':','-'), params.startGPS, params.endGPS);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

% HTML table of channel plots
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Cross-power spectrogram </span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Coherence </span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');

fprintf(fid,'<a href="snr.png"><img alt="" src="snr.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');

fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="coherence.png"><img alt="" src="coherence.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');

fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">%s PSD spectrogram (log scale)</span></big></big><br>\n',strrep(channelName1,':','-'));
fprintf(fid,'</td>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">%s PSD spectrogram (log scale)</span></big></big><br>\n',strrep(channelName2,':','-'));
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');

fprintf(fid,'<a href="P1.png"><img alt="" src="P1.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="P2.png"><img alt="" src="P2.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');

% spectral variance plot
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Spectral Variance Histogram Channel 1</span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;">Spectral Variance Histogram Channel 2</span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="P1_specvar.png"><img alt="" src="P1_specvar.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="P2_specvar.png"><img alt="" src="P2_specvar.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');


if params.doBicoherence
  fprintf(fid,'<tr>\n');
  fprintf(fid,'<td\n');
  fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
  fprintf(fid,'style="font-weight: bold;">Bicoherence</span></big></big><br>\n');
  fprintf(fid,'</td>\n');
  fprintf(fid,'<td\n');
  fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
  fprintf(fid,'style="font-weight: bold;">Bicoherence p-value</span></big></big><br>\n');
  fprintf(fid,'</td>\n');
  fprintf(fid,'</tr>\n');
  fprintf(fid,'<tr>\n');
  fprintf(fid,'<td style="vertical-align: top;">\n');
  fprintf(fid,'<a href="bicoherence.png"><img alt="" src="bicoherence.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
  fprintf(fid,'</td>');
  fprintf(fid,'<td style="vertical-align: top;">\n');
  fprintf(fid,'<a href="bicoherence_pvalue.png"><img alt="" src="bicoherence_pvalue.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
  fprintf(fid,'</td>');
  fprintf(fid,'</tr>\n');
end

fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');

% Close HTML
fprintf(fid,'</html>\n');
fclose(fid);
