function stamp_pem_summary_page(pemParamsFile, channlesListFile, startGPS, ...
                               endGPS)
%function stamp_pem_summary_page(pemParamsFile, channlesListFile, startGPS, ...
%                               endGPS)
% The code makes the HTML page usign the plots produced by running stamp_pem.m
% Routine written by M Coughlin, P Meyers, S Kandhasamy
% Contact: michael.coughlin@ligo.org, shivaraj.kandhasamy@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For running compiled code
startGPS = strassign(startGPS);
endGPS = strassign(endGPS);

% Read in STAMP-PEM parameter file
params = readParamsFromFilePEM(pemParamsFile);
params.startGPS = startGPS;
params.endGPS = endGPS;
params = checkParams(params);


[coherenceMatFile] = harvestAndSaveCoherenceData(pemParamsFile,channlesListFile,startGPS,endGPS);
makeBokehPlotsPEM(coherenceMatFile,params.outputFilePrefix,'BOKEH');


% Read in channels list
if exist(channlesListFile)
  [channelNames, channelSampleRates] = ...
      textread(channlesListFile,'%s %f');
else
  error ('No channels file provided \n');
end

% Output path for HTML file
params.htmlpath = [params.outputFilePrefix '/' num2str(params.startGPS) '_' num2str(params.endGPS) '/' strrep(channelNames{1},':','-') ]; 

% Open HTML file for writing %%%%
fid = fopen([params.htmlpath '/summaryALL.html'],'w+');
% HTML Header
fprintf(fid,'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n');
fprintf(fid,'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n');
fprintf(fid,'\n');
fprintf(fid,'<html>\n');
fprintf(fid,'\n');
fprintf(fid,'<head>\n');
fprintf(fid,'<title>STAMP-PEM summary page for reference Channel %s: %d - %d</title>\n', channelNames{1}, params.startGPS, params.endGPS);
fprintf(fid,'<link rel="stylesheet" type="text/css" href="../../../../../style/main.css">\n');
fprintf(fid,'<script src="../../../../../style/sorttable.js"></script>\n');
fprintf(fid,'</head>\n');

fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 1260px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top; background-color:SpringGreen;"><big><big><big><big><span\n');
fprintf(fid,'style="font-weight: bold;">STAMP-PEM summary page for reference Channel %s: %d - %d</span></big></big></big></big><br>\n', channelNames{1}, params.startGPS, params.endGPS);
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');
fprintf(fid,'<br>');

% Table of final coherence and master coherence matrix; created later in 
% this code.
fprintf(fid,'<table\n');
fprintf(fid,'style="text-align: left; width: 200px; height: 67px; margin-left:auto; margin-right: auto;"\n');
fprintf(fid,'border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<tbody>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td colspan="2" style="text-align: center; vertical-align: top;"<big><big><span style="font-weight: bold;"> <a href="summaryALL.html">ALL</a>');
if ~isempty(params.subsystem);
  for ii=1:length(params.subsystem)
    fprintf(fid,',<a href="summary%s.html"> %s </a>',params.subsystem{ii},params.subsystem{ii});
  end
end
fprintf(fid,'</span></big></big><br> \n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>');

fprintf(fid,'<tr>\n');
fprintf(fid,'<td\n');
fprintf(fid,'style="text-align: center; vertical-align: top;"<big><big><span\n');
fprintf(fid,'style="font-weight: bold;"> Coherence Matrix (ALL) </span></big></big><br>\n');
fprintf(fid,'</td>\n');
fprintf(fid,'</tr>\n');
fprintf(fid,'<tr>\n');
fprintf(fid,'<td style="vertical-align: top;">\n');
fprintf(fid,'<a href="./cohMatrixALL.png"><img alt="" src="./cohMatrixALL.png" style="border: 0px solid ; width: 630px; height: 432px;"></a><br>\n');
fprintf(fid,'</td>');
fprintf(fid,'</tr>\n');
fprintf(fid,'</tbody>\n');
fprintf(fid,'</table>\n');

% HTML table header
fprintf(fid,'<table class="sortable" id="sort" style="text-align: center; width: 1260px; height: 67px; margin-left:auto; margin-right: auto; background-color: white;" border="1" cellpadding="2" cellspacing="2">\n');
fprintf(fid,'<thead>\n');
fprintf(fid,'<tr><th>PEM Channel</th><th>max coherence</th></tr>\n');
fprintf(fid,'</thead>\n');

colorCode = {'blue','yellow', 'orange','red'};
% Loop over channels
for ii = 1:length(channelNames)
  channelPlotsDir = ([params.htmlpath '/' strrep(channelNames{ii},':','-')]);
  % Check if there is data available for this channel
  if exist([channelPlotsDir]) == 7 
    % Read in mat file associated with this channel
    try
      tempData = load([channelPlotsDir '/coherence.mat']);
      coherence_data = tempData.coherence;
    catch
      coherence_data = NaN;
      %error('The %s data is missing \n',channelNames{ii});
    end
    
    % Creating HTML rows containing channel and ft-map SNR information
    pemchnldir = strrep(channelPlotsDir,'/public_html','');
    pemchnldir = strrep(pemchnldir,'/home/','~');
    fprintf(fid,'<tr>');
    %fprintf(fid,'<td><a href="%s/pem.html">%s</a></td>',...
    fprintf(fid,'<td><a href="%s/pem.html">%s</a></td>',...
	    strrep(channelNames{ii},':','-'), strrep(channelNames{ii},':','-'));
    try
      cutf = tempData.ff > 10; % look at > 10 Hz to identify problematic channels
    catch
    end
    if ~isnan(max(coherence_data))
      font_color = colorCode{ceil(max(coherence_data(cutf))*10/3)};
    else
      font_color = ['black'];
    end
    fprintf(fid,'<td style="background-color: %s">%.2f</td>', font_color, max(coherence_data));
    fprintf(fid,'</tr>\n');
    % Creat corresponding HTML for that PEM channel
    stamp_pem_chnl_html(params, channelNames{1}, channelNames{ii})
  end
end

% Close HTML table
fprintf(fid,'</table>\n');

fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');

% Show user and time information
[junk,user] = system('echo $USER');
[junk,time] = system('date');

fprintf(fid,'<br>\n');
fprintf(fid,'<br>\n');
fprintf(fid,'Created by user %s on %s \n',user,time);

% Close HTML
fprintf(fid,'</html>\n');
fclose(fid);  % end HTML writing %%%%%%

% make master coherence plots
makeCoherencePlotsPEM(coherenceMatFile,params.htmlpath,'cohMatrixALL','all',params.cohMatrixdoLog);
% make coherence plots and htmls for sub-systems
if ~isempty(params.subsystem)
  for ii=1:length(params.subsystem)
    [nouse1, nouse2] = system(['cp ' params.htmlpath '/summaryALL.html ' ...
		    params.htmlpath '/summary' params.subsystem{ii} '.html']);
    [nouse1, nouse2] = system(['sed -i ''s/ALL.png/' params.subsystem{ii} ...
		    '.png/g'' ' ...
		    params.htmlpath '/summary' params.subsystem{ii} '.html']);
    [nouse1, nouse2] = system(['sed -i ''s/(ALL)/(' params.subsystem{ii} ...
		    ')/g'' ' ...
		    params.htmlpath '/summary' params.subsystem{ii} '.html']);
    [nouse1, linepos] = system(['grep -n "table" ' params.htmlpath '/summary' ...
		    params.subsystem{ii} '.html | cut -f1 -d:']);
    linepos = strsplit(linepos);
    [nouse1, nouse2] = system(['sed -i ''' num2str(str2num(linepos{end-2})+4) ...
		    ',' num2str(str2num(linepos{end-1})-1) '{/' ...
		    params.subsystem{ii} '/!d;}'' ' params.htmlpath ...
		    '/summary' params.subsystem{ii} '.html']);
    
    makeCoherencePlotsPEM(coherenceMatFile, params.htmlpath,...
			  ['cohMatrix' params.subsystem{ii}], params.subsystem{ii}, params.cohMatrixdoLog);  
  end
end
function [params] = checkParams(params)

try
  params.cohMatrixdoLog;
catch
  params.cohMatrixdoLog = 0;
end
try 
  params.subsystem;
catch
  params.subsystem = [];
end


