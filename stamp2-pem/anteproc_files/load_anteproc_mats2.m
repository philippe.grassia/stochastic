function [out_map, out_params] = load_anteproc_mats(matlist,params,ifo_num)
% function [out_map, out_params] = load_anteproc_mats(matlist,params,ifo_num)
%
% Loads .mat files found by find_anteproc_mats.m and organizes all of the
% loaded data into an ft-map.  Only loads file for one detector at a time
% (specified by 'ifo_num');
%
% Based on loadmats.m.
% Written by T. Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Setup. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (ifo_num ==1)
  ts = params.anteproc.timeShift1;
elseif (ifo_num == 2)
  ts = params.anteproc.timeShift2;
else
  error('ifo_num must be 1 or 2.');
end

% Get start/end GPS times.
sGPS = params.anteproc.startGPS(ifo_num);
eGPS = params.anteproc.endGPS(ifo_num);
fn = ['ifo' num2str(ifo_num)];

% List of .mat files.
matlist = matlist.(fn);

% Determine number of .mat files to load.
nMatLoads = length(matlist);
if (nMatLoads < 1)
  error('No .mat files found.')
end

% Load data. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii=1:nMatLoads
  M=load(matlist{ii});

  % Check that the mat files include the requested band.
  if ((params.fmin < M.params.flow) || (params.fmax > M.params.fhigh))
    error('Requested frequencies out of range of mat files.');
  end

  % Use the first .mat file to get information about the data.
%  if (ii==1)
   if 0
    % Record metadata.
    out_params = M.params;

    % Initialize new ft-maps with template.
    out_map.segDur = out_params.segmentDuration;

    out_map.segstarttime = sGPS:(out_params.segmentDuration/2): ...
  (eGPS - out_params.segmentDuration/2);
    T = length(out_map.segstarttime);
%    out_map.rbartilde = NaN*ones(size(M.map.rbartilde,1),T);
%    out_map.naiP = NaN*ones(size(M.map.naiP,1),T);
    out_map.P = NaN*ones(size(M.map.P,1),T);
    out_map.fft = NaN*ones(size(M.map.fft,1),T);

  end
  out_map = M.map;
  out_params = M.params;

  % Find relevant time indices from new and old ft-maps, accounting for
  % the appropriate timeshift.
%  [C, new_t, old_t] = intersect(out_map.segstarttime,M.map.segstarttime);
%  out_map.realsegstarttime(new_t) = M.map.segstarttime(old_t);
  % Fill new maps,
%  out_map.rbartilde(:, new_t) = M.map.rbartilde(:, old_t);
%  out_map.naiP(:, new_t) = M.map.naiP(:, old_t);
%  out_map.P(:, new_t) = M.map.P(:, old_t);
%  out_map.fft(:, new_t) = M.map.fft(:, old_t);

end

% If there params.anteproc.bkndstudy = false and there is missing data, we
% may need to adjust out_map.realsegstarttime.


%idx1 = find(out_map.realsegstarttime == 0);
%fixGPS = fliplr(idx1*(out_params.segmentDuration/2));
%out_map.realsegstarttime(idx1) = out_map.realsegstarttime(max(idx1)+1) - fixGPS;

% adjust end
%if (numel(out_map.realsegstarttime) < numel(out_map.segstarttime))
%  Norig = numel(out_map.realsegstarttime);
%  Nextra = numel(out_map.segstarttime) - numel(out_map.realsegstarttime);
%  gps_plus = (1:Nextra)*(out_params.segmentDuration/2);
%  out_map.realsegstarttime((Norig+1):(Norig+Nextra)) = ...
%out_map.realsegstarttime(Norig) + gps_plus;
%end



% Calculate frequencies based on last .mat file.
% Note: DON'T CUT FREQUENCIES YET.  This will be done after the
% cross-correlation is completed.
numFreqs = floor((M.params.fhigh-M.params.flow)/M.params.deltaF)+1;
out_map.f = M.params.flow + M.params.deltaF*transpose([0:numFreqs-1]);

% Adjust segment start times and check number of columns.

% Use last .mat file to get rbartilde info.
%out_map.rbtinfo = M.map.rbtinfo;
out_map.fftinfo = M.map.fftinfo;

return;
