function stoch_out = doAnteprocPEM(paramsFile, channelName1, channelName2, startGPS, endGPS, darmMatFileExist)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function stoch_out = clustermapPEM(paramsFile, channelName1, channelName2, startGPS, endGPS)
% Adapted from STAMP
%
% params is a text file in the form of a name/value pairs .
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Running analysis %s Vs %s \n',channelName1, channelName2);
tic; % check how long does it take to run

% Turn of warning due to use of psd function
warning('off','signal:psd:PSDisObsolete');

% Extract parameters from input parameter file that will be passed between
% routines
params = readParamsFromFilePEM(paramsFile);
% add misc parameters
params.ifo1 = channelName1(1:2);
params.ifo2 = channelName2(1:2);

% To use it as a compiled code
startGPS = strassign(startGPS);
endGPS = strassign(endGPS);
if isstr(darmMatFileExist)
  darmMatFileExist=str2num(darmMatFileExist);
end

% store it in params struct
params.startGPS = startGPS;
params.endGPS = endGPS;
params.channelName1 = channelName1;
params.channelName2 = channelName2;

params.outDir = [params.outputFilePrefix '/' num2str(startGPS) '_' num2str(endGPS) '/' strrep(channelName1,':','-')];
[nouse1, timevect1, nouse2] = getFrameDataAnteproc(channelName1, startGPS, 1);
[nouse3, timevect2, nouse4] = getFrameDataAnteproc(channelName2, startGPS, 1);
if (isempty(nouse1) | isempty(nouse3))
  stoch_out = {};
  return;
end
params.resampleRate1 = 1/(timevect1(2)-timevect1(1));
params.resampleRate2 = 1/(timevect2(2)-timevect2(1));
params.fhigh = min([params.fhigh,params.resampleRate1/2-params.deltaF,params.resampleRate2/2-params.deltaF]);
if params.fRef > params.fhigh
   params.fReference = ceil((params.flow + params.fhigh)/2);
   keyboard
else
   params.fReference = params.fRef;
end

%reset sample rate to highest value we need.
for ii=1:15
  if 2^ii>=2*params.fhigh
    params.resampleRate1 = min(2^ii,params.resampleRate1);
    if params.resampleRate1 == 2*params.fhigh
      params.fhigh = params.fhigh - params.deltaF;
    end
    break;
  end
end
if ~darmMatFileExist
  fprintf('No darm mat file exists.\n Running anteproc on darm...\n');
  params.save_anteproc_mats = true;
  [map1,params1] = anteprocPEM(params,channelName1);
end

params.resampleRate1 = 1/(timevect2(2)-timevect2(1));
for ii=1:15
  if 2^ii>=2*params.fhigh
    params.resampleRate1 = min(2^ii,params.resampleRate1);
    if params.resampleRate1 == 2*params.fhigh
      params.fhigh = params.fhigh - params.deltaF;
    end
    break;
  end
end
 

%flow1 = map1.rbtinfo.flow + map1.rbtinfo.deltaF;
%flow2 = map2.rbtinfo.flow + map2.rbtinfo.deltaF;
%params.flow = max([flow1 flow2]);

%fhigh1 = map1.rbtinfo.flow + map1.rbtinfo.deltaF*length(map1.P);
%fhigh2 = map2.rbtinfo.flow + map2.rbtinfo.deltaF*length(map2.P);
%params.fhigh = min([fhigh1 fhigh2]);
%used to be params.resamplerate = min([...]);
resampleRate = min([1/(timevect1(2)-timevect1(1)) 1/(timevect2(2)-timevect2(1))]);
params.resampleRate1 = min(resampleRate, params.resampleRate1);
params.fmin = 1;
%params.fhigh = params.resampleRate1/2 - 2;
params.fmax = params.fhigh;

if params.fmax > 4000
   params.fmax = 1024;
end

params.matavailable = true;
params.anteproc.loadFiles = true;
params.anteproc.jobFileTimeShift = false;
params.anteproc.timeShift1 = 0;
params.anteproc.timeShift2 = 0;
params.anteproc.useCache = false;
params.anteproc.bkndstudy = false;

params.outputfiledir = [params.outDir '/anteproc/' strrep(channelName1,':','_') '/'];
params.outputfilename = 'stamppem';
ifo_lett = params.ifo1(1);
params.anteproc.inmats1 = [params.outputfiledir ifo_lett '-' params.ifo1 ...
                    '_' params.outputfilename];
params.outputfiledir = [params.outDir '/anteproc/' strrep(channelName2,':','_') '/'];
params.outputfilename = 'stamppem';
ifo_lett = params.ifo2(1);
params.anteproc.inmats2 = [params.outputfiledir ifo_lett '-' params.ifo1 ...
                    '_' params.outputfilename];

% locate or create mats for the requested data
% if mat files containing ft-information already available, use that
% (for backward compatibility)
if darmMatFileExist
  %if params.anteproc.loadFiles
    % Load anteproc'd .mat files.  Contains separate FFT for each detector.
    % CSD not calculated yet in these .mat files.
    fprintf('Darm data should exist...Loading anteproc''d .mat files.\n');

    [matlist,params] = find_anteproc_mats(params);
    params.anteproc.startGPS = [params.startGPS params.startGPS];
    params.anteproc.endGPS = [params.endGPS params.endGPS];
    [map1, params1] = load_anteproc_mats(matlist,params,1);
  %else
    % Load normal pre-processed STAMP .mat files.
  %  [matlist, params] = findmats(params);
  %  [map, params, pp] = loadmats(matlist, params);
  %end
%else % otherwsie create ft-maps
  % Making sure that the sampling rates and resampling rates are fine;
  % we collect 1 sec of data to identify sampling rates of the channels
%  [nouse1, timevect1, nouse2] = getFrameDataAnteproc(channelName1, startGPS, 1);
%  [nouse3, timevect2, nouse4] = getFrameDataAnteproc(channelName2, startGPS, 1);
%  if (isempty(nouse1) | isempty(nouse3))
%    stoch_out = {};
%    return;
%  end
%  params.resampleRate1 = min([1/(timevect1(2)-timevect1(1)) 1/(timevect2(2)-timevect2(1)) params.resampleRate1]);
%  params.resampleRate2 = min([1/(timevect1(2)-timevect1(1)) 1/(timevect2(2)-timevect2(1)) params.resampleRate2]);
%  params.fhigh = min([params.resampleRate1/2-params.deltaF params.resampleRate2/2-params.deltaF params.fhigh]);
  if params.fRef > params.fhigh
     params.fReference = ceil((params.flow + params.fhigh)/2);
  else
     params.fReference = params.fRef;
  end


  % Generate meta data of spectrograms
  %[map, params] = preprocPEM(params);
  % creating other meta data structure
%  pp.flow = params.flow;
  %pp.fhigh = params.fhigh;
%  pp.deltaF = params.deltaF;
%  pp.w1w2bar = params.w1w2bar;
%  pp.w1w2squaredbar = params.w1w2squaredbar;
%  pp.w1w2ovlsquaredbar = params.w1w2ovlsquaredbar;
end
if ~strcmp(channelName1,channelName2);
  fprintf('Running anteproc on %s\n',channelName2);
  params.save_anteproc_mats = false;
  [map2,params2] = anteprocPEM(params,channelName2);
else
  map2 = map1;
  params2 = params1;
end
[params, map1, map2] = check_anteproc_data(map1,map2,params1,params2, ...
                                               params);
 
% Plots for search direction corresponding to maximum SNR.
if params.savePlots
  saveAnteprocPlotsAndData(params, map1, map2);
end

% store param values
stoch_out.params = params;

% warn the user if diagnostic tools are on
if params.diagnostic
  fprintf('CAUTION: diagnostic tools on.\n');
end
%outTime = toc;

% close all open plots
close all;
fprintf('Elapsed time = %f seconds.\n', toc);

fprintf('Done\n');

return;
