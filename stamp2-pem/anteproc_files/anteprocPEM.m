function [map,params] = anteprocPEM(params,channelName)
% function [map, params] = preprocPEM(params)
%
%  This function produces cross-power and auto power ft-maps of given two
%  channels which can be futher processed.
%
%  Routine adopted from stochastic and STAMP algorithms
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ddmmyyyyhhmmss  = datestr(now);

% check if everything is set and, if not, use default values
%params = preprocPEMDefaults(params);

startTime   = params.startGPS;
jobDuration = params.endGPS - params.startGPS;

% store job start time and duration
params.job.startGPS = startTime;
params.job.duration = jobDuration;
jobNumber = 1;

fprintf('Processing data for detector %s, GPS times %.1f - %.1f.\n', ...
	params.ifo1,startTime,startTime+jobDuration);

% add few more things to the params structure
params.ddmmyyyyhhmmss = ddmmyyyyhhmmss;
%params.jobsFile = jobsFile;

params.channelName1 = channelName;
params.ASQchannel1 = channelName;
params.frameType1 = '';
params.frameCachePath1 = '';
params.alphaBetaFile1 = '';
params.calCavGainFile1 = '';
params.calResponseFile1 = '';
params.nargin = 1;
params.site1 = channelName(1:2);
params.mapsize = params.endGPS - params.startGPS;

params.outputfiledir = [params.outDir '/anteproc/' strrep(channelName,':','_') '/'];
params.outputfilename = 'stamppem';

folders = dir([params.outputfiledir '/' channelName(1) '-*']);
if length(folders) > 0
   fprintf('Data for %s already exists... continuing.\n')
   folder = [params.outputfiledir '/' folders(1).name];
   files = dir([folder '/*.mat']);
   file = [folder '/' files(1).name];
   data_out = load(file);
   params = data_out.params; map = data_out.map;
   return;
end
params = anteprocDefaults(params);
%determine bad GPS times, if given...
if params.doBadGPSTimes
  if isempty(params.badGPSTimesFile)
    badtimesstart = 9999999999;
    badtimesend = 0;
  else
    [badtimesstart,badtimesend] = textread(params.badGPSTimesFile,'%f%f\n',-1,'commentstyle','matlab');
  end
end



% if Monte Carlo is requested, read in noise or signal files
mc.init=true;
if params.doDetectorNoiseSim
  mc.transfer = load(params.DetectorNoiseFile);
end

%determine bad GPS times, if given...
if params.doBadGPSTimes
  if isempty(params.badGPSTimesFile)
    badtimesstart = 9999999999;
    badtimesend = 0;
  else
    [badtimesstart,badtimesend] = textread(params.badGPSTimesFile,'%f%f\n',-1,'commentstyle','matlab');
  end
end

% set total number of discrete frequencies
numFreqs = floor((params.fhigh-params.flow)/params.deltaF)+1;

% initialize lastLoadedDataEnd variable
lastLoadedDataEnd1 = 0;

% get appropriate detector structure for each ifo
if (isnan(params.azimuth1))
  detector1 = getdetector(params.site1);
else
  detector1 = getdetector(params.site1,params.azimuth1);
end

% construct filter coefficients for high-pass filtering
if params.doHighPass1
  [b1,a1] = butter(params.highPassOrder1, params.highPassFreq1/(params.resampleRate1/2), 'high');
end

% set values for psd estimation (on resampled data, HP filtered data)
psdFFTLength1 = params.resampleRate1*(1/params.deltaF);
psdWindow1    = hann(psdFFTLength1);
psdOverlapLength1 = psdFFTLength1/2;
detrendFlag1  = 'none';

% set values for data windowing, zero-padding, and FFT
if params.doOverlap
  params.hannDuration1=params.segmentDuration;
end;
numPoints1    = params.segmentDuration*params.resampleRate1;
dataWindow1   = tukeywin(numPoints1, params.hannDuration1/params.segmentDuration);
fftLength1    = 2*numPoints1;

% construct frequency mask for later use
data = constructFreqMask(params.flow, params.fhigh, params.deltaF, ...
                         params.freqsToRemove, params.nBinsToRemove, params.doFreqMask);
mask = constructFreqSeries(data, params.flow, params.deltaF);

% frequency vector and overlap reduction function
f = params.flow + params.deltaF*transpose([0:numFreqs-1]);

% If one of the data streams is params.heterodyned, set gamma.symmetry to 0
% so that negative frequencies are not included in the normalization.
if params.heterodyned
  gamma = constructFreqSeries(data, params.flow, params.deltaF, 0);
else
  gamma = constructFreqSeries(data, params.flow, params.deltaF, 1);
end

% For STAMP, overlap reduction factor is unity (similar to radiometer)
% and H(f) (= fRef) is flat in strain power
% TP: this is calculated when the .mat files are loaded
% by STAMP (in combine_anteproc_mats.m)
%{
if params.stochmap
  gamma.data = ones(length(gamma.data),1);
  params = rmfield(params,'fRef');
  params.fRef.data = ones(length(gamma.data),1);
  params.fRef.flow = params.flow;
  params.fRef.deltaF = params.deltaF;
end
%}

% construct name of gps times files and frame cache files for this job
gpsTimesFile1 = ...
    [params.gpsTimesPath1 'gpsTimes' params.ifo1(1) '.' num2str(jobNumber) '.txt'];
frameCacheFile1 = ...
    [params.frameCachePath1 'frameFiles' params.ifo1(1) '.' num2str(jobNumber) '.txt'];

% channel names
channelName1 = [params.ifo1 ':' params.ASQchannel1];

% read in calibration info

if ( ~strncmp(params.alphaBetaFile1,   'none', length(params.alphaBetaFile1))   & ...
     ~strncmp(params.calCavGainFile1,  'none', length(params.calCavGainFile1))  & ...
     ~strncmp(params.calResponseFile1, 'none', length(params.calResponseFile1)) )
[t1, f1, R01, C01, alpha1, gamma1] = ...
  readCalibrationFromFiles(params.alphaBetaFile1, params.calCavGainFile1, params.calResponseFile1);
end;

% check that params.numSegmentsPerInterval is odd
if mod(params.numSegmentsPerInterval,2)==0
  error('params.numSegmentsPerInterval must be odd');
end

% determine number of intervals and segments to analyse
bufferSecsMax = params.bufferSecs1;


if ~params.doSidereal %do not worry about sidereal times
  M = floor( (jobDuration - 2*bufferSecsMax)/params.segmentDuration );

  if params.doOverlap
    if(mod((jobDuration-2*bufferSecsMax)/params.segmentDuration,0.5)==0)
      M = (jobDuration - 2*bufferSecsMax)/params.segmentDuration;
    end
    numSegmentsTotal = 2*M-1;
    numIntervalsTotal = 2*(M - (params.numSegmentsPerInterval-1)) - 1;
    intervalTimeStride = params.segmentDuration/2;
  else
    numSegmentsTotal = M;
    numIntervalsTotal = M - (params.numSegmentsPerInterval-1);
    intervalTimeStride = params.segmentDuration;
  end

  centeredStartTime = startTime + bufferSecsMax + ...
    floor( (jobDuration - 2*bufferSecsMax - M*params.segmentDuration)/ 2 );

else %calculate parameters compatible with the sidereal time
  srfac = 23.9344696 / 24; %sidereal time conversion factor
  srtime = GPStoGreenwichMeanSiderealTime(startTime) * 3600;
  md = mod(srtime,params.segmentDuration/srfac);
  centeredStartTime = round(startTime + params.segmentDuration - md*srfac);

  %this is the start time of the first segment in this job that
  %is compatible with the sidereal timing, but we still have to
  %check that there is enough time for the preceding buffer

  if centeredStartTime - startTime < bufferSecsMax
    centeredStartTime = centeredStartTime + params.segmentDuration;
  end

  %now we can calculate the remaining bookkeeping variables
  M = floor( (jobDuration - bufferSecsMax - centeredStartTime + ...
              startTime) / params.segmentDuration );

  if params.doOverlap
    if(mod((jobDuration-2*bufferSecsMax)/params.segmentDuration,0.5)==0)
      M = (jobDuration - 2*bufferSecsMax)/params.segmentDuration;
    end
    numIntervalsTotal = 2*(M - (params.numSegmentsPerInterval-1)) - 1;
    intervalTimeStride = params.segmentDuration/2;
  else
    numIntervalsTotal = M - (params.numSegmentsPerInterval-1);
    intervalTimeStride = params.segmentDuration;
  end
end

if(numIntervalsTotal<1)
  error('Job duration is very small; provide a different start and end GPS times.');
end

isFirstPass=true;
% analyse the data
for I=1:numIntervalsTotal

  badSegmentData = false;
  badResponse = false;

  intervalStartTime = centeredStartTime + (I-1)*intervalTimeStride;

  % check if first pass through the loop
  if isFirstPass

    for J=1:params.numSegmentsPerInterval

      % read in time-series data from frames
      dataStartTime1 = intervalStartTime + (J-1)*params.segmentDuration - params.bufferSecs1;
      dataDuration1 = params.segmentDuration + 2*params.bufferSecs1;

      if dataStartTime1+dataDuration1 > lastLoadedDataEnd1
        lastLoadedDataEnd1 = min(dataStartTime1+params.minDataLoadLength,startTime+jobDuration);
        lastLoadedDataStart1 = dataStartTime1;
        tmpDuration = lastLoadedDataEnd1 - lastLoadedDataStart1;
        %[longadcdata1, data1OK, params] = readTimeSeriesData3(channelName1,...
        %                      dataStartTime1, tmpDuration,...
        %                      params.frameType1, params.frameDuration1,...
        %                      gpsTimesFile1, frameCacheFile1, ...
        %                      params, mc);
        [longadcdata1, timevect1, data1OK] = getFrameDataAnteproc(params.channelName1, dataStartTime1,tmpDuration);
      end
%      longadcdata1.data = tempadcdata1;
%      longadcdata1.deltaT = timevect1(2)-timevect1(1);
try
      startindex = (dataStartTime1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT + 1;
      endindex = (dataStartTime1 + dataDuration1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT;
      adcdata1.data = longadcdata1.data(startindex:endindex);
      adcdata1.tlow = dataStartTime1;
      adcdata1.deltaT = longadcdata1.deltaT;
catch
keyboard
end

      % if either data stream is bad, set flag and exit loop
      if (data1OK==false)
        badSegmentData = true;
        break
      end

      if (~ isfield(adcdata1,'fbase') )
        adcdata1.fbase = NaN;
      end

      if (~ isfield(adcdata1,'phase') )
	adcdata1.phase = NaN;
      end

      % KLUDGE: can override base frequency in parameter file
      if (~ isnan(params.fbase1) )
	adcdata1.fbase = params.fbase1;
      end;
      % End KLUDGE

      if isnan(adcdata1.fbase)
	if params.heterodyned
	  error('Trying to do params.heterodyned analysis on non-params.heterodyned data');
	end
      else
	if (~ params.heterodyned)
	  error('Trying to do non-params.heterodyned analysis on params.heterodyned data');
	end
      end

      % downsample the data
      sampleRate1 = 1/adcdata1.deltaT;
      p1 = 1;
      q1 = floor(sampleRate1/params.resampleRate1);
      deltaT1 = 1/params.resampleRate1;

      if sampleRate1 == params.resampleRate1
        data = adcdata1.data;
      else
        data = resample(adcdata1.data, p1, q1, params.nResample1, params.betaParam1);
      end
      n1(J) = constructTimeSeries(data, adcdata1.tlow, deltaT1, ...
                                  adcdata1.fbase, adcdata1.phase);

      % free-up some memory
      clear adcdata1;

      % calculate response functions from calibration data

      if ( strncmp(params.alphaBetaFile1,   'none', length(params.alphaBetaFile1))   | ...
           strncmp(params.calCavGainFile1,  'none', length(params.calCavGainFile1))  | ...
           strncmp(params.calResponseFile1, 'none', length(params.calResponseFile1)) )

        % the data is already calibrated
        response1(J) = constructFreqSeries(ones(numFreqs,1), params.flow, params.deltaF);
        transfer1(J) = response1(J);

      else
        calibsec1 = dataStartTime1 + params.bufferSecs1;
        [R1, responseOK1] = ...
          calculateResponse(t1, f1, R01, C01, alpha1, gamma1, calibsec1,...
			    params.ASQchannel1);

        % if response function is bad, set flag and exit loop
        if responseOK1==false
          badResponse = true;
          break
        end

        % evaluate response function at desired frequencies
        response1(J) = convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 0, 0);

        % convert to transfer functions (units: counts/strain)
        transfer1(J) = convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 1, 0);
      end

    end % loop over segments J

    % if bad data or bad response function for any segment, continue with
    % next interval
    if (badSegmentData | badResponse)
      continue
    else
      isFirstPass = false;
    end

  else

    % shift data and response functions accordingly
    for J=1:params.numSegmentsPerInterval-1

      if params.doOverlap
        % shift data by half a segment; need to worry about buffer
        N1 = length(n1(J).data);
        bufferOffset1 = params.bufferSecs1/n1(J).deltaT;

        data  = [n1(J).data(N1/2+1-bufferOffset1:N1-bufferOffset1) ; ...
                 n1(J+1).data(1+bufferOffset1:N1/2+bufferOffset1)];
        tlow  = n1(J).tlow+intervalTimeStride;
        n1(J) = constructTimeSeries(data, tlow, n1(J).deltaT, ...
                                    n1(J).fbase, n1(J).phase);

        % get response function corresponding to shifted start times
        if ( strncmp(params.alphaBetaFile1,   'none', length(params.alphaBetaFile1))   | ...
             strncmp(params.calCavGainFile1,  'none', length(params.calCavGainFile1))  | ...
             strncmp(params.calResponseFile1, 'none', length(params.calResponseFile1)) )

          % the data is already calibrated
          response1(J) = constructFreqSeries(ones(numFreqs,1), params.flow, params.deltaF);
          transfer1(J) = response1(J);

        else
          calibsec1 = n1(J).tlow + params.bufferSecs1;
          [R1, responseOK1] = ...
            calculateResponse(t1, f1, R01, C01, alpha1, gamma1, calibsec1,...
                            params.ASQchannel1);

          % if response function is bad, set flag and exit loop
          if responseOK1==false
            badResponse = true;
            break
          end

          % evaluate response function at desired frequencies
          response1(J) = convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 0, 0);

          % convert to transfer functions (units: counts/strain)
          transfer1(J) = convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 1, 0);
        end

      else
        % simple shift by a full segment
	n1(J)=n1(J+1);
        response1(J)=response1(J+1);
        transfer1(J)=transfer1(J+1);
      end

    end % loop over J

    % if bad response function for any segment, continue with next interval
    if badResponse
      continue
    end

    % read in time-series data for next segment
    dataStartTime1 = intervalStartTime ...
                     + (params.numSegmentsPerInterval-1)*params.segmentDuration ...
                     - params.bufferSecs1;
    dataDuration1  = params.segmentDuration + 2*params.bufferSecs1;

    if dataStartTime1+dataDuration1 > lastLoadedDataEnd1
      lastLoadedDataEnd1 = min(dataStartTime1+params.minDataLoadLength,startTime+jobDuration);
      lastLoadedDataStart1 = dataStartTime1;
      tmpDuration = lastLoadedDataEnd1 - lastLoadedDataStart1;
      %[longadcdata1, data1OK, params] = readTimeSeriesData3(channelName1,...
      %                        dataStartTime1, tmpDuration,...
      %                        params.frameType1, params.frameDuration1,...
      %                        gpsTimesFile1, frameCacheFile1, ...
      %                        params, mc);
      [longadcdata1, timevect1, data1OK] = getFrameDataAnteproc(params.channelName1, dataStartTime1,tmpDuration);
    end
%    longadcdata1.data = tempadcdata1;
%    longadcdata1.deltaT = timevect1(2)-timevect1(1);

    startindex = (dataStartTime1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT + 1;
    endindex = (dataStartTime1 + dataDuration1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT;
    adcdata1.data = longadcdata1.data(startindex:endindex);
    adcdata1.tlow = dataStartTime1;
    adcdata1.deltaT = longadcdata1.deltaT;

    % if either data stream is bad, set flag and exit loop
    if (data1OK==false)
      badSegmentData = true;
      break
    end

    if (~ isfield(adcdata1,'fbase') )
      adcdata1.fbase = NaN;
    end

    if (~ isfield(adcdata1,'phase') )
      adcdata1.phase = NaN;
    end

    % KLUDGE: can override base frequency in parameter file
    if (~ isnan(params.fbase1) )
      adcdata1.fbase = params.fbase1;
    end
    % End KLUDGE

    if isnan(adcdata1.fbase)
      if params.heterodyned
        error('Trying to do params.heterodyned analysis on non-params.heterodyned data');
      end
    else
      if (~ params.heterodyned)
        error('Trying to do non-params.heterodyned analysis on params.heterodyned data');
      end
    end

   % downsample the data
    sampleRate1 = 1/adcdata1.deltaT;
    p1 = 1;
    q1 = floor(sampleRate1/params.resampleRate1);
    deltaT1 = 1/params.resampleRate1;

    if sampleRate1 == params.resampleRate1
      data = adcdata1.data;
    else
      data = resample(adcdata1.data, p1, q1, params.nResample1, params.betaParam1);
    end

    n1(params.numSegmentsPerInterval) = ...
      constructTimeSeries(data, adcdata1.tlow, deltaT1, ...
                          adcdata1.fbase, adcdata1.phase);

    % free-up some memory
    clear adcdata1;

    % calculate response functions from calibration data
    if ( strncmp(params.alphaBetaFile1,   'none', length(params.alphaBetaFile1))   | ...
	 strncmp(params.calCavGainFile1,  'none', length(params.calCavGainFile1))  | ...
	 strncmp(params.calResponseFile1, 'none', length(params.calResponseFile1)) )

      % the data is already calibrated
      response1(params.numSegmentsPerInterval) = ...
        constructFreqSeries(ones(numFreqs,1), params.flow, params.deltaF);
      transfer1(params.numSegmentsPerInterval) = response1(params.numSegmentsPerInterval);

    else
      calibsec1 = dataStartTime1 + params.bufferSecs1;
      [R1, responseOK1] = ...
        calculateResponse(t1, f1, R01, C01, alpha1, gamma1, calibsec1,...
                            params.ASQchannel1);

    % if response function is bad, set flag and exit loop
    if responseOK1==false
      badResponse = true;
      break
    end
    %% evaluate response function at desired frequencies
    response1(params.numSegmentsPerInterval) = ...
      convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 0, 0);

    % convert to transfer functions (units: counts/strain)
      transfer1(params.numSegmentsPerInterval) = ...
        convertResponse(f1, R1, params.flow, params.deltaF, numFreqs, 1, 0);
    end

  end % of if isFirstPass ... else ... end

  % if bad data or bad response function for any segment, continue with
  % next interval
  if (badSegmentData | badResponse)
    continue
  end

  % initialize data array for average psds
  avg_data1 = zeros(numFreqs,1);

  % loop over number of segments
  for J=1:params.numSegmentsPerInterval

    if params.doShift1
      shiftoffset = round(params.ShiftTime1 / n1(J).deltaT);

      if (shiftoffset >= length(n1(J).data))
        error('Time-shift is larger than buffer segments.');
      end

      qtempdata1 = circshift(n1(J).data,shiftoffset);
    else
      qtempdata1 = n1(J).data;
    end

    o1 = n1(J);
    o1.data = qtempdata1;

    if params.doHighPass1
      if params.highPassOrder1<=6
        highpassed1 = constructTimeSeries(filtfilt(b1,a1,o1.data), ...
          o1.tlow, o1.deltaT, o1.fbase, o1.phase);
      else
        try
          params.cascade1;
        catch
          params.cascade1=true;
          fprintf('Warning: HP filter1 n>6 requires a cascade filter\n');
        end
        tmp1 = cascadefilter(o1.data, params.highPassOrder1, ...
          params.highPassFreq1, params.resampleRate1);
        highpassed1 = constructTimeSeries(tmp1, o1.tlow, o1.deltaT, ...
          o1.fbase, o1.phase);
      end
    else
      highpassed1 = o1;
    end
%------------------------------------------------------------------------------

    % chop-off bad data at start and end of HP filtered, resampled data
    firstIndex1 = 1 + params.bufferSecs1*params.resampleRate1;
    lastIndex1  = length(highpassed1.data)-params.bufferSecs1*params.resampleRate1;

    r1(J) = constructTimeSeries(highpassed1.data(firstIndex1:lastIndex1), ...
                                highpassed1.tlow + params.bufferSecs1, ...
                                highpassed1.deltaT, ...
				highpassed1.fbase, highpassed1.phase);

    % estimate power spectra for optimal filter
    if I == 1
	fftw('planner','hybrid');
    end
    [temp1,freqs1] = psd(r1(J).data, psdFFTLength1, 1/r1(J).deltaT, ...
                         psdWindow1, psdOverlapLength1, detrendFlag1);

    % normalize appropriately

    % If all the bins in the PSD are independent, we are dealing with
    % complex params.heterodyned data
    if length(temp1) == psdFFTLength1
      % Account for heterodyning of data.  This appears to be
      % the correct normalization because psd(), unlike pwelch(),
      % calculates the two-sided PSD of both real and complex data.
      freqs1shifted = fftshift(freqs1);
      spec1 = ...
          constructFreqSeries(2*r1(J).deltaT*fftshift(temp1), ...
		        	r1(J).fbase + freqs1shifted(1) ...
				- 1/r1(J).deltaT, ...
				freqs1(2)-freqs1(1), 0);
    else
      spec1 = ...
	  constructFreqSeries(2*r1(J).deltaT*temp1, freqs1(1), ...
				freqs1(2)-freqs1(1), 0);
    end

    % coarse-grain noise power spectra to desired freqs
    psd1 = coarseGrain(spec1, params.flow, params.deltaF, numFreqs);

    % calibrate the power spectra
    calPSD1 = ...
      constructFreqSeries(psd1.data.*(abs(response1(J).data).^2), ...
                            psd1.flow, psd1.deltaF, psd1.symmetry);

    % calculate avg power spectra, ignoring middle segment if desired
    midSegment = (params.numSegmentsPerInterval+1)/2;
    if ( (params.ignoreMidSegment) & (J==midSegment) )
      % do nothing
      %fprintf('Ignoring middle segment\n');
    else
      avg_data1 = avg_data1 + calPSD1.data;
    end

    if (J==midSegment)
      % This calculates the "naive" theorerical variance, i.e.,
      % that calculated from the current segment without averaging
      % over the whole interval.
      % This is useful for the stationarity veto which excludes
      % segments for which the naive sigma differs too much from
      % the one calculated with the sliding PSD average.
      naiP1 = calPSD1.data;
    end

  end % loop over segments J

  % construct average power spectra
  if params.ignoreMidSegment
    avg_data1 = avg_data1/(params.numSegmentsPerInterval-1);
  else
    avg_data1 = avg_data1/params.numSegmentsPerInterval;
  end

  calPSD1_avg = constructFreqSeries(avg_data1, params.flow, params.deltaF, 0);

  % analyse the middle data segment %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % window, zero-pad and fft the data
  rbartilde1 = windowAndFFT(r1(midSegment), dataWindow1, fftLength1);
  fft1 = coarseGrain(rbartilde1, params.flow, params.deltaF, numFreqs);

  params.bias = 1/(2*(params.segmentDuration * params.deltaF * 2 - 1) * (9/11)) + 1;
  params.naivebias = 1/((params.segmentDuration * params.deltaF * 2 - 1) * (9/11)) + 1;
  params.SiderealTime = GPStoGreenwichMeanSiderealTime(r1(midSegment).tlow);

  % check if the interval is bad
  if params.doBadGPSTimes %determine if the current interval is bad
    c1 = intervalStartTime - bufferSecsMax < badtimesend;
    c2 = intervalStartTime + 3*params.segmentDuration + bufferSecsMax > badtimesstart;

    if sum(c1&c2) > 0 | sqrt(ccVar/naiVar)>params.maxDSigRatio | sqrt(ccVar/naiVar)<params.minDSigRatio
      badtimesflag = 1;
    else
      badtimesflag = 0;
    end
  else %do not check for bad times
    badtimesflag = 0;
  end

  % finally record the value in the relevant filename, if the bad time flag ok
  if badtimesflag == 0
    map.f = [params.flow:params.deltaF:params.fhigh];
    P1 = calPSD1_avg.data;
    map.rbartilde(:,I) = rbartilde1.data;
    map.naiP(:,I) = naiP1;
    map.P(:,I) = P1;
    map.segstarttime(:,I) = r1(midSegment).tlow;
    map.fft(:,I) = fft1.data;
  end % badtimesflag if statement

end % loop over intervals I

% Save information about rbartilde.
map.rbtinfo.symmetry = rbartilde1.symmetry;
map.rbtinfo.deltaF = rbartilde1.deltaF;
map.rbtinfo.flow = rbartilde1.flow;

% Map duration.
map.dur = map.segstarttime(end)+params.segmentDuration-map.segstarttime(1);

% Save .mat files.
if params.save_anteproc_mats
  save_anteproc_mats(map,params);
  fprintf('saving anteproc .mat file...\n');
end
fprintf('elapsed_time = %f\n', toc);

return;
