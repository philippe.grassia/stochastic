function [datavectTS, timevect dataOK] = getFrameDataAnteprocAnteproc(channelName, ...
                                        dataStartTime, dataDuration)
%function [datavect, timevect, dataOK] = getFrameDataAnteproc(channelName, ...
%                                        dataStartTime, dataDuration)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generate frame cache for the time requested %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(channelName,[channelName(1) '1:GDS-CALIB_STRAIN']);
  frameType = [channelName(1) '1_HOFT_C00']; % try archived frames
  [rflag, frames] = system(['gw_data_find -o ' channelName(1) ' -t ' frameType ' -s ' num2str(dataStartTime) ' -e ' num2str(dataStartTime+dataDuration) ' -u file']);
else
  frameType = [channelName(1) '1_C']; % first check for commissioning frames
  [rflag, frames] = system(['gw_data_find -o ' channelName(1) ' -t ' frameType ' -s ' num2str(dataStartTime) ' -e ' num2str(dataStartTime+dataDuration) ' -u file']);
end
if (length(frames)<=1 | strcmp(frames,'No files found!'))
  frameType = [channelName(1) '1_R']; % try archived frames
  [rflag, frames] = system(['gw_data_find -o ' channelName(1) ' -t ' frameType ' -s ' num2str(dataStartTime) ' -e ' num2str(dataStartTime+dataDuration) ' -u file']);
end


if(length(frames)>1)
  % Only return non-empty frame lines
  frames = regexp(frames,'\n','split');
  frames = frames(find( ~strcmpi(frames,'')));

  if strcmp(frames,'No files found!')
    error('gw data find found no matching frames')
  end
  % Return arrays of frame gps and duration
  gps = zeros(size(frames));
  dur = zeros(size(frames));

%  datavect = [];
%  timevect = [];
  for ii = 1:length(frames); % get data
    %fprintf('%d / %d\n',ii, length(frames));
    frames{ii} = strrep(frames{ii},'file://localhost','');
    frame_split = regexp(strrep(frames{ii},'.gwf',''),'-','split');
    %[tempdata, tsamp, fsamp, gps0] = frextract(frames{ii},channelName);
    % using frextract is too slow, so instead we use frgetvect; but it
    % requires a couple more additional details (startGPS time of the frame
    % and duration of the frame) which is available via 'frame_split' variable
    framestarttime = str2double(frame_split{end-1});
    framedur = str2double(frame_split{end});
    frameend = framestarttime + framedur;
    if ii==1
      framestarttime = dataStartTime;
    end
    if frameend>(dataStartTime+dataDuration)
      frameend = dataStartTime+dataDuration;
      framedur = frameend - framestarttime;
    end
    [tempdata, tsamp, fsamp, gps0] = frgetvect(frames{ii}, channelName, ...
           framestarttime, framedur);
    if ii==1
	datavectTS.data=zeros(length(frames)*length(tempdata),1);
	timevect=zeros(length(frames)*length(tsamp),1);
    end
	newdat = tsamp+gps0;
	idx1 = (ii-1)*length(tempdata)+1;
	idx2 = (ii)*length(tempdata);
    datavectTS.data(idx1:idx2) = tempdata;
    timevect(idx1:idx2) = newdat;
%keyboard
%    datavect=[datavect;tempdata];
%    timevect=[timevect;tsamp+gps0];
  end
  % select data that is between given start and end time
  datavectTS.data = datavectTS.data(timevect>=dataStartTime & timevect<dataStartTime+dataDuration);
  datavectTS.deltaT = timevect(2)-timevect(1);
  timevect = timevect(timevect>=dataStartTime & timevect<dataStartTime+dataDuration);
  %fprintf('done loading data\n');
  if strcmp(channelName,'L1:OAF-CAL_YARM_DQ');
    [s,g] = zp2sos([100/16384 100/16384 100/16384],[1/16384 1/16384 1/16384],1);
    Hd = dfilt.df2sos(s,g);
    datavectTS.data = filter(Hd,datavectTS.data);
    fprintf('done dewhitening data\n');
  end
  dataOK = 1; % data is fine to use
else
  error('error running gw_data_find \n');
  dataOK = 0; % no data
end

return;
