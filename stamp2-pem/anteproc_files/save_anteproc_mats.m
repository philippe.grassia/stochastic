function save_anteproc_mats(in_map,params)
% function save_anteproc_mats(map, params)
% This function saves map and params for a single .mat file
% produced by anteproc.  It crops the .mat files accordingly
% based on params.batch and params.mapsize.
%
% Based on savemap.m.
% Written by T. Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Warning message.
if params.segmentDuration<1 || params.segmentDuration>100
  warning('This segment duration may cause trouble: talk to Eric.');
  % If segmentDuration<1 then the file names will have a decimal in
  % them; that may cause problems with regular expressions in clustermap.
  % If the segmentDuration is >100s then the mat file will have only
  % one segment in it and the requirement of an overlapping segment may
  % cause problems.  -ethrane
end

% Number of .mat files to be produced.
nColsPerMat = params.mapsize/(params.segmentDuration/2) - 1;
nmats = ceil(size(in_map.segstarttime,2)/nColsPerMat);
%nmats = ceil( (in_map.segstarttime(end)-in_map.segstarttime(1)) / ...
%	      (params.mapsize - params.segmentDuration) );

% Job start and end times.
startGPS_job = in_map.segstarttime(1);
endGPS_job = in_map.segstarttime(end);

for ii=1:nmats
  fprintf('Creating mat file %i/%i\n', ii,nmats);
  map = in_map;

  % Maps will overlap by half of a segment, i.e. the last column in map #1
  % will overlap partly with the first column in map #2, etc.
  startGPS = startGPS_job + (ii-1)*(params.mapsize - params.segmentDuration/2);
  endGPS = startGPS + params.mapsize - params.segmentDuration;

  if (endGPS > endGPS_job)
    endGPS = endGPS_job;
  end

  cut = ((in_map.segstarttime >= startGPS) & (map.segstarttime <= endGPS));

  % Crop the map, if necessary.
  map.segstarttime = map.segstarttime(cut);
  %map.P = map.P(:,cut);
  %map.naiP = map.naiP(:,cut);
  %map.rbartilde = map.rbartilde(:,cut);
  map.fft = map.fft(:,cut);
  map.start = map.segstarttime(1);
  map.dur = map.segstarttime(end) - map.segstarttime(1) + ...
	    params.segmentDuration;

  % Determine prefixes for output directory.
  dirtime = floor(map.segstarttime(1)/10000);
  folderpostfix = num2str(dirtime);
  %  params.outputfilepath = [params.outputfiledir params.outputfilename '-' ...
  %		    params.ifo1 '-' folderpostfix];
  ifo_lett = params.ifo1(1);
  params.outputfilepath = [params.outputfiledir ifo_lett '-' params.ifo1 ...
		    '_' params.outputfilename '-' folderpostfix];

  % Create a new directory if necessary.
  if ~exist(params.outputfilepath)
    system(['mkdir -p ' params.outputfilepath]);
  end

  % Save .mat file.
%  save([params.outputfilepath '/' params.outputfilename '-' params.ifo1 '-' ...
%	num2str(map.segstarttime(1)) '-' num2str(map.dur) '.mat'], 'map', ...
%       'params');
  save([params.outputfilepath '/' ifo_lett '-' params.ifo1 '_' ...
	params.outputfilename '-' num2str(map.segstarttime(1)) '-' num2str(map.dur) ...
	'.mat'], 'map', 'params');
end

return;
