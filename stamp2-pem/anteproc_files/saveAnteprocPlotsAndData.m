function saveAnteprocPlotsAndData(params, map1, map2)
% function saveplotsAndData(params, map)

% Output directory
% combine maps and then run savePlotsAndData on the 
% resulting map.
% if you want, save at the end. 
fReference = params.fReference;
[map,params,pp] = combine_anteproc_mats(map1,map2,params);
params.fReference = fReference;
ccSpec.data = map.cc;
ccSpec.flow = params.flow;
ccSpec.deltaF = pp.deltaF;
sensInt.flow = params.flow;
sensInt.deltaF = pp.deltaF;
sensInt.data = map.sensInt;
ccVar = map.ccVar;
midGPSTimes = map.segstarttime + params.segmentDuration/2;
det1 = getdetector(params.ifo1);
det2 = getdetector(params.ifo2);
source = [6 30];
[map] = ccSpecReadout_stamp(det1, det2, ... 
  midGPSTimes, source, ccSpec, ccVar, sensInt, params, pp, map);
if params.doPlots
  map.snr = map.y ./ map.sigma;
  map.snrz = map.z ./map.sigma;
end

saveplotsAndData(params,map);

return;
