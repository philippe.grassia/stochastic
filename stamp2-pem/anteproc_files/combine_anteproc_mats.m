function [map, params, pp] = combine_anteproc_mats(map1,map2,params)
% function [map, params, pp] = combine_anteproc_mats(map1,map2,params)
%
% Calculates the cross-spectrum for the data given.  Also trims the maps
% based on the frequencies requested by the user.  Finally, calculates
% window factors and saves them in the 'pp' struct.  The output map,
% params, and pp structs should be ready for STAMP analysis (i.e., in
% the same form as data pre-processed in the "normal" way for STAMP).
%
% Written by T. Prestegard (prestegard@physics.umn.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Setup. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check if GPS times of map1 and map2 match up.  Should already be adjusted
% for timeshift, so they should be exactly the same.
t_diff = sum(abs(map1.segstarttime - map2.segstarttime));
if (t_diff > 0)
  error('GPS times of the two anteproc maps do not match up.');
end

% Calculate overlap reduction function (ones for STAMP).
% Determine total number of discrete frequencies available in the
% raw data.
numFreqs = floor((params.fhigh-params.flow)/params.deltaF)+1;
f = params.flow + params.deltaF*transpose([0:numFreqs-1]);
data = ones(size(f));
if params.heterodyned
  gamma = constructFreqSeries(data, params.flow, params.deltaF, 0);
else
  gamma = constructFreqSeries(data, params.flow, params.deltaF, 1);
end

% Adjust reference frequency properly for STAMP analysis.
params = rmfield(params,'fRef');
params.fRef.data = ones(length(gamma.data),1);
params.fRef.flow = params.flow;
params.fRef.deltaF = params.deltaF;

% Prepare for optimal filter calculation. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numPoints1 = params.segmentDuration*params.resampleRate1;
dataWindow1 = tukeywin(numPoints1, params.hannDuration1/ ...
                       params.segmentDuration);
numPoints2 = params.segmentDuration*params.resampleRate2;
dataWindow2 = tukeywin(numPoints2, params.hannDuration2/ ...
                       params.segmentDuration);
data = constructFreqMask(params.flow, params.fhigh, params.deltaF, ...
                         params.freqsToRemove, params.nBinsToRemove, ...
                         params.doFreqMask);
mask = constructFreqSeries(data, params.flow, params.deltaF);

% Calculate window factors.
[w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar] = windowFactors(dataWindow1,...
                                                  dataWindow2);

% Set up rbartilde structs.
rbartilde1 = map1.rbtinfo;
rbartilde2 = map2.rbtinfo;

% Set up PSD structs.
% PSD symmetry of 0 seems to be hard-coded into preproc (thus, anteproc too).
[calPSD1_avg.flow, calPSD2_avg.flow] = deal(params.flow);
[calPSD1_avg.deltaF, calPSD2_avg.deltaF] = deal(params.deltaF);
[calPSD1_avg.symmetry, calPSD2_avg.symmetry] = deal(0);

% Set up response structs.
[response1.flow, response2.flow] = deal(params.flow);
[response1.deltaF, response2.deltaF] = deal(params.deltaF);
[response1.symmetry, response2.symmetry] = deal(0);


% Loop over columns in maps; calculate optimal filter and cross-spectrum. %%%
numCols = size(map1.segstarttime,2);
for ii=1:numCols

  % Setup for calculation.
  % Response should just be ones since we are using h(t).
  rbartilde1.data = map1.rbartilde(:,ii);
  calPSD1_avg.data = map1.P(:,ii);
  response1.data = ones(size(calPSD1_avg.data));

  rbartilde2.data = map2.rbartilde(:,ii);
  calPSD2_avg.data = map2.P(:,ii);
  response2.data = ones(size(calPSD2_avg.data));
  rbartilde2.data = padarray(rbartilde2.data,length(rbartilde2.data)-length(rbartilde1.data),0);

  % Calculate optimal filter.
  [Q, ccVar, sensInt] = calOptimalFilter(params.segmentDuration, gamma, ...
                                         params.fRef, params.alphaExp, ...
                                         calPSD1_avg, calPSD2_avg, ...
                                         dataWindow1, dataWindow2, mask);
  % Calculate CSD.
  [ccStat,CSD] = calCSD(rbartilde1, rbartilde2, Q, response1, response2);

  % Assign output arrays.
  map.cc(:,ii) = CSD.data .* Q.data;
  map.sensInt(:,ii) = sensInt.data;
  map.ccVar(ii) = ccVar;

end

% Assign other outputs and adjust frequencies to those chosen by the user.
new_f = params.fmin:params.deltaF:params.fmax;
[no_use,idx] = intersect(round(10000*f)/10000,round(10000*new_f)/10000);
if isempty(idx)
   error('params.fmin - params.flow must be a multiple of %.4f',params.deltaF);
end
map.segstarttime = map1.segstarttime;
map.P1 = map1.P(idx,:);
map.P2 = map2.P(idx,:);
map.naiP1 = map1.naiP(idx,:);
map.naiP2 = map2.naiP(idx,:);
map.cc = map.cc(idx,:);
map.sensInt = map.sensInt(idx,:);
map.deltaF = params.deltaF;
map.segDur = params.segmentDuration;
map.start = map1.segstarttime(1);
map.f = f(idx);
map.dur = (size(map.P1,2)+1)*params.segmentDuration/2;
map.fft1 = map1.fft(idx,:);
map.fft2 = map2.fft(idx,:);

% Calculate stats in 'pp' struct.
pp.flow = params.flow;
pp.fhigh = params.fhigh;
pp.deltaF = params.deltaF;
pp.w1w2bar = w1w2bar;
pp.w1w2squaredbar = w1w2squaredbar;
pp.w1w2ovlsquaredbar = w1w2ovlsquaredbar;


return;
