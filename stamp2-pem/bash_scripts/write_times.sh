#!/bin/bash


echo $1 > input/start_times.txt
tconvert -f %H $1 >> input/start_times.txt
tconvert -f %d%m%y $1 >> input/start_times.txt

# what's today's date?
today=`tconvert -f %d%m%y $1`
# what was yesterday's date?
yesterday=`tconvert -f %d%m%y $1-86400`
# what is the hour for the start time?
hour=`tconvert -f %H $1`
# date
date=`tconvert -f %D $1`
tconvert -f %D $date >> input/start_times.txt

# if hour is 00 then we have a new day
# so replace yesterday's date where it appears in paramFile
# with today's date and create a new
# page for today!
if [[ "$hour" == "00" ]]; then
	sed -i s/$yesterday/$today/g input/online_params.txt
	cp -r ~/public_html/summary_page/template ~/public_html/summary_page/$today
	sed -i s|mm/dd/yy|$date|g ~/public_html/summary_page/$today/index.html
fi
