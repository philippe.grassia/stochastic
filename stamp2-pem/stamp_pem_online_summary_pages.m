function stamp_pem_online_summary_pages(pemParamsFile,channelListFile)

%check that value is a number
params = readParamsFromFilePEM(pemParamsFile);
% read start times for stamp_pem run
[startGPS endGPS starthr date] = read_start_times;

% query segment database for locked segments between
% start and end times
%[startGPSvec endGPSvec] = query_segment_database(startGPS, endGPS);
locked_times = load('input/locked_segs.txt');
try
	startGPSvec = locked_times(:,1);
	endGPSvec = locked_times(:,2);
catch
	startGPSvec = [];
	endGPSvec = [];
end

% value = 1 run summary page code on times in file
% kill current condor jobs
%!condor_rm meyers;
% if there were any locked segments, run summary page code. if not say so
if isempty(startGPSvec)
  fprintf('There are no locked segments between %d and %d\n',startGPS,endGPS);
else	
%	try
		% do post processing (subroutine)
		postProcessing(pemParamsFile,channelListFile,starthr,date,startGPSvec,endGPSvec);
%	catch
%		fprintf('Summary code failed...');
%	end % try/catch
end % if isempty

% if there were no locked segments or if summary page code is done
% rewrite times and then run stamp_pem

% write new start times (subroutine, see below)
write_start_times;
% read those times (subroutine, see below)
[startGPS endGPS starthr date day_start_time] = read_start_times;
% query segment database for new times (subroutine, see below)
[startGPSvec endGPSvec] = query_segment_database(startGPS, endGPS, starthr, date,day_start_time);

% run stamp_pem on new times if there are any!
if isempty(startGPSvec)
  fprintf('There are no locked segments between %d and %d\n',startGPS,endGPS);
  starthrstr = sprintf('%02d',starthr);
  endhrstr = sprintf('%02d',starthr+2);
  cmd = sprintf('./bash_scripts/no_locked_segs.sh %02d-%02d_cohMatrix.png %06d',starthr,starthr+2,date);
  junk = system(cmd);
else
  for ii=1:length(startGPSvec)
	if (endGPSvec(ii)-startGPSvec(ii)) > params.minDataLoadLength
    		stamp_pem_anteproc_loop_submit(pemParamsFile,channelListFile,startGPSvec(ii),endGPSvec(ii));
	end
  end % for loop
end% if
end% fxn


%%%%%%%%%% SUB ROUTINES %%%%%%%%%%%%

function [startGPSvec endGPSvec] = query_segment_database(startGPS, endGPS, starthr, date,day_start_time)
% QUERY_SEGMENT_DATABASE Description
%	[STARTGPSVEC ENDGPSVEC] = QUERY_SEGMENT_DATABASE(STARTGPS, ENDGPS)
%
% queries segment data base for locked segments in between startGPS and endGPS

% create query command
segment_query_day_command =  ...
        sprintf('ligolw_segment_query_dqsegdb --segment-url=https://dqsegdb5.phy.syr.edu --query-segments --gps-start-time %d --gps-end-time %d --include-segments=''H1:DMT-ANALYSIS_READY'' -o /home/meyers/public_html/summary_page/%06d/%06d.xml.gz',day_start_time,endGPS,date,date)
        
%print_to_txt_comm = ['ligolw_print -t segment -c start_time -c end_time /home/meyers/public_html/summary_page/' num2str(date) '/' num2str(date) '.xml.gz > ./input/locked_segs.txt'];
%segment_query_command = ['ligolw_segment_query --query-segments' ...
%		    ' --segment-url https://segdb-er.ligo.caltech.edu -s ' ...
%		    num2str(startGPS) ' -e ' num2str(endGPS)...
%		    ' --include-segments ''L1:DMT-DC_READOUT_LOCKED'' | ligolw_print -t segment -c start_time -c end_time > ./locked_segs.txt'];
segment_query_hour_command =  ...
	['ligolw_segment_query_dqsegdb --segment-url=https://dqsegdb5.phy.syr.edu'...
	' --query-segments --gps-start-time ' num2str(startGPS)...
	' --gps-end-time ' num2str(endGPS)...
	' --include-segments=''H1:DMT-ANALYSIS_READY'' | ligolw_print -t segment -c start_time -c end_time > ./input/locked_segs.txt']
 
% have system run query command
junk  = system(segment_query_hour_command);
junk2 = system(segment_query_day_command);

% read locked times from file made using query command
locked_times = load('./input/locked_segs.txt');

if isempty(locked_times)
  startGPSvec = [];
  endGPSvec = [];
else
  startGPSvec = locked_times(:,1);
  endGPSvec = locked_times(:,2);
end
% return start and end times for locks thath happened
% between input GPS times


end % function

%%%%%%%%%%%%%%%%%
%READ START TIMES
%%%%%%%%%%%%%%%%%


function [startGPS endGPS starthr date day_start_time] = read_start_times()
% READ_START_TIMES reads in start and end GPS times where we want to find locked segments
%	[STARTGPS ENDGPS] = READ_START_TIMES()
%
% reads start and end GPS times from local file start_times.txt. usually these times are rr2 hours apart
% and correspond to times between which we expect there to be locks that we want to run stamp_pem on.
% start_times.txt usually has two lines: a start time and an end time.

time = textread('./input/start_times.txt','%d');

startGPS = time(1);
endGPS = startGPS + 7200;
%startGPS = 1101668490;
%endGPS = 1101675539;
starthr = time(2);
date = time(3);
day_start_time = time(4);

if ~(endGPS-startGPS)==7200
  fprintf('WARNING: start and end times are NOT 2 hours apart\n');
end

end

%%%%%%%%%%%%%%%%%%
% WRITE START TIMES
%%%%%%%%%%%%%%%%%%
function [] = write_start_times()
% WRITE_START_TIMES rewrites start_times.txt
%	[] = WRITE_START_TIMES()
%
% rewrites start_times.txt to make second line the first line and add 7200 to second line
% just basically iterates things by 2 hours

% read in times
time = textread('./input/start_times.txt','%d');

%
cmd = sprintf('./bash_scripts/write_times.sh %d',time(1)+7200);

junk = system(cmd);

end % function

%%%%%%%%%%%%%%%%%%%
% POST PROCESSING
%%%%%%%%%%%%%%%%%%%
function postProcessing(pemParamsFile,channelListFile,starthr,date,startGPS,endGPS)
% subroutine for creating coherence matrices
prefix = sprintf('%02d-%02d',starthr,starthr+2);
cohPlotTitle = sprintf('%s_cohMatrix',prefix);
params = readParamsFromFilePEM(pemParamsFile);

% harvest coherence data for last lock
coherenceMatFile = harvestData(pemParamsFile,channelListFile,startGPS,endGPS);
if ~strcmp(coherenceMatFile,'None')
	makeCoherencePlotsPEM(coherenceMatFile,params.outputFilePrefix,cohPlotTitle,'all',false);
	makeBokehPlotsPEM(coherenceMatFile,params.outputFilePrefix,[prefix '_bokeh']);
else
	return
end

day_lock_file = sprintf('%06d',date); % ddmmyy.mat is name of .mat file that contains data from the whole day
if exist([params.outputFilePrefix '/' day_lock_file])
	day_list = {coherenceMatFile [params.outputFilePrefix '/' day_lock_file '.mat']}; % populate list of files to combine
else
	day_list = {coherenceMatFile};
end
[day_lock_file] = combineCoherenceData(day_list,channelListFile,params.outputFilePrefix,day_lock_file); % combine files and resave over day_lock_file
test = load(day_log_file);
if isempty(test.coherence_matrix)
  return
else
  makeBokehPlotsPEM(day_lock_file,params.outputFilePrefix,'DAY');
end

% write name of 

% harvest coherence data for whole day
end % function
%%%%%%%%%%%%%%%%%%%
% HARVEST AND COMBINE COHERENCE DATA
%%%%%%%%%%%%%%%%%%%

function [coherenceMatFile] = harvestData(pemParamsFile,channelListFile,startGPSvec,endGPSvec)
% harvestData(pemParamsFile,channelListFile,startGPSvec,endGPSvec)
% harvests data from all locks. 

% read in params
params = readParamsFromFilePEM(pemParamsFile);

% initialize vector
locked_files = [];
index=1;
for ii = 1:length(startGPSvec)
	% for each start and end time, harvest the coherence data.
	% store files that are produced so we can combine them in the next step.
	if (endGPSvec(ii)-startGPSvec(ii))>params.minDataLoadLength
	locked_files{index} = harvestAndSaveCoherenceData(pemParamsFile,channelListFile,startGPSvec(ii),endGPSvec(ii));
	fprintf('successfully saved and output %s...\n',locked_files{index});
	index=index+1;
	end
end % fotamp
% combine coherence data from the locked files in locked_files
if ~isempty(locked_files)
	[coherenceMatFile] = combineCoherenceData(locked_files,channelListFile,...
		     params.outputFilePrefix,...
		     ['locked_file_' num2str(startGPSvec(1)) '_' num2str(endGPSvec(end))]);
else
coherenceMatFile = 'None';
end
test = load(coherenceMatFile);
if isempty(test.coherence_matrix);
  coherenceMatFile = 'None';
end

end % function
