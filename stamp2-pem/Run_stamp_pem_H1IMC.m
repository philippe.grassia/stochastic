% This is a wrapper script that produces STAMP-PEM coherence results
%

% Input
pemParamsFile = 'input/stamp_pem_params.txt';
channlesListFile = 'input/channel_list_H1IMC.txt';
startGPS = 1082972416;
endGPS = startGPS+5*60;

% Running the pipeline
stamp_pem(pemParamsFile, channlesListFile, startGPS, endGPS);
