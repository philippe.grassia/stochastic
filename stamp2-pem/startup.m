if ~isdeployed
  [nouse, curDir] = system('pwd');
  fprintf('THIS CODE IS TESTED ONLY WITH MATLAB2013A (DEFAULT AT CIT CLUSTER), MAY HAVE PROBLMES WITH OTHER VERSIONS. \n'); 
  fprintf('Adding functions in the current directory/subdirectories ... ');
  addpath([curDir(1:end-1)]);
  addpath([curDir(1:end-1) '/stamp_functions']);
  addpath([curDir(1:end-1) '/stochastic_functions']);
  addpath([curDir(1:end-1) '/anteproc_files']);
  addpath([curDir(1:end-1) '/html_generation_scripts']);
  addpath([curDir(1:end-1) '/analysis_tools']);
  fprintf('done.\n');
end
