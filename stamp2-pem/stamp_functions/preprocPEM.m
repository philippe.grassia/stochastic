function [map, params] = preprocPEM(params)
% function [map, params] = preprocPEM(params)
%
%  This function produces cross-power and auto power ft-maps of given two
%  channels which can be futher processed.
%
%  Routine adopted from stochastic and STAMP algorithms
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ddmmyyyyhhmmss  = datestr(now);

% check if everything is set and, if not, use default values
%params = preprocPEMDefaults(params);

startTime   = params.startGPS;
jobDuration = params.endGPS - params.startGPS;

% store job start time and duration
params.job.startGPS = startTime;
params.job.duration = jobDuration;

%determine bad GPS times, if given...
if params.doBadGPSTimes
  if isempty(params.badGPSTimesFile)
    badtimesstart = 9999999999;
    badtimesend = 0;
  else
    [badtimesstart,badtimesend] = textread(params.badGPSTimesFile,'%f%f\n',-1,'commentstyle','matlab');
  end
end

% set total number of discrete frequencies
numFreqs = floor((params.fhigh-params.flow)/params.deltaF)+1;

% construct filter coefficients for high-pass filtering
if params.doHighPass1
  [b1,a1] = butter(params.highPassOrder1, ...
    params.highPassFreq1/(params.resampleRate1/2), 'high');
end

if params.doHighPass2
  [b2,a2] = butter(params.highPassOrder2, params.highPassFreq2/(params.resampleRate2/2), 'high');
end



% set values for psd estimation (on resampled data, HP filtered data)
psdFFTLength1 = params.resampleRate1*(1/params.deltaF);
psdWindow1    = hann(psdFFTLength1);
psdOverlapLength1 = psdFFTLength1/2; 
detrendFlag1  = 'none';

psdFFTLength2 = params.resampleRate2*(1/params.deltaF);
psdWindow2    = hann(psdFFTLength2);
psdOverlapLength2 = psdFFTLength2/2; 
detrendFlag2  = 'none';

% set values for data windowing, zero-padding, and FFT
if params.doOverlap
  params.hannDuration1=params.segmentDuration;
  params.hannDuration2=params.segmentDuration;
end;
numPoints1    = params.segmentDuration*params.resampleRate1; 
dataWindow1   = tukeywin(numPoints1, params.hannDuration1/params.segmentDuration);
fftLength1    = 2*numPoints1;

numPoints2    = params.segmentDuration*params.resampleRate2;
dataWindow2   = tukeywin(numPoints2, params.hannDuration2/params.segmentDuration);
fftLength2    = 2*numPoints2;

% construct frequency mask for later use
data = constructFreqMask(params.flow, params.fhigh, params.deltaF, ...
                         params.freqsToRemove, params.nBinsToRemove, params.doFreqMask);
mask = constructFreqSeries(data, params.flow, params.deltaF);

% frequency vector and overlap reduction function
f = params.flow + params.deltaF*transpose([0:numFreqs-1]);
gamma.data = ones(length(f),1); % overlap reduction function; not used for PEM
gamma.flow = params.flow;
gamma.deltaF = params.deltaF;
gamma.symmetry = 1;
  
% check that params.numSegmentsPerInterval is odd    
if mod(params.numSegmentsPerInterval,2)==0
  error('params.numSegmentsPerInterval must be odd');
end

% determine number of intervals and segments to analyse
bufferSecsMax = max(params.bufferSecs1,params.bufferSecs2);

if ~params.doSidereal %do not worry about sidereal times
  M = floor( (jobDuration - 2*bufferSecsMax)/params.segmentDuration );

  if params.doOverlap
    if(mod((jobDuration-2*bufferSecsMax)/params.segmentDuration,0.5)==0)
      M = (jobDuration - 2*bufferSecsMax)/params.segmentDuration;
    end
    numSegmentsTotal = 2*M-1;
    numIntervalsTotal = 2*(M - (params.numSegmentsPerInterval-1)) - 1;
    intervalTimeStride = params.segmentDuration/2;
  else 
    numSegmentsTotal = M;
    numIntervalsTotal = M - (params.numSegmentsPerInterval-1);
    intervalTimeStride = params.segmentDuration;
  end

  centeredStartTime = startTime + bufferSecsMax + ...
    floor( (jobDuration - 2*bufferSecsMax - M*params.segmentDuration)/ 2 );

else %calculate parameters compatible with the sidereal time
  srfac = 23.9344696 / 24; %sidereal time conversion factor
  srtime = GPStoGreenwichMeanSiderealTime(startTime) * 3600;
  md = mod(srtime,params.segmentDuration/srfac);
  centeredStartTime = round(startTime + params.segmentDuration - md*srfac); 

  %this is the start time of the first segment in this job that 
  %is compatible with the sidereal timing, but we still have to 
  %check that there is enough time for the preceding buffer

  if centeredStartTime - startTime < bufferSecsMax
    centeredStartTime = centeredStartTime + params.segmentDuration;
  end

  %now we can calculate the remaining bookkeeping variables
  M = floor( (jobDuration - bufferSecsMax - centeredStartTime + ...
              startTime) / params.segmentDuration );

  if params.doOverlap
    if(mod((jobDuration-2*bufferSecsMax)/params.segmentDuration,0.5)==0)
      M = (jobDuration - 2*bufferSecsMax)/params.segmentDuration;
    end
    numIntervalsTotal = 2*(M - (params.numSegmentsPerInterval-1)) - 1;
    intervalTimeStride = params.segmentDuration/2;
  else 
    numIntervalsTotal = M - (params.numSegmentsPerInterval-1);
    intervalTimeStride = params.segmentDuration;
  end
end

if(numIntervalsTotal<1)
  error('Job duration is very small; provide a different start and end GPS times.');
end

lastLoadedDataEnd1 = 0;
lastLoadedDataEnd2 = 0;

isFirstPass=true;
% analyse the data
for I=1:numIntervalsTotal

  badSegmentData = false;
  badResponse = false;

  intervalStartTime = centeredStartTime + (I-1)*intervalTimeStride;

  % check if first pass through the loop
  if isFirstPass

    for J=1:params.numSegmentsPerInterval

      % read in time-series data from frames
      dataStartTime1 = intervalStartTime + (J-1)*params.segmentDuration - params.bufferSecs1;
      dataStartTime2 = intervalStartTime + (J-1)*params.segmentDuration - params.bufferSecs2;

      dataDuration1 = params.segmentDuration + 2*params.bufferSecs1;
      dataDuration2 = params.segmentDuration + 2*params.bufferSecs2;

      if dataStartTime1+dataDuration1 > lastLoadedDataEnd1
        lastLoadedDataEnd1 = min(dataStartTime1+params.minDataLoadLength,startTime+jobDuration);
        lastLoadedDataStart1 = dataStartTime1;
        tmpDuration = lastLoadedDataEnd1 - lastLoadedDataStart1;
        [tempadcdata1, timevect1, data1OK] = getFrameData(params.channelName1, dataStartTime1, tmpDuration);
      end
      longadcdata1.data = tempadcdata1;
      longadcdata1.deltaT = timevect1(2)-timevect1(1);

      if dataStartTime2+dataDuration2 > lastLoadedDataEnd2
        lastLoadedDataEnd2 = min(dataStartTime2+params.minDataLoadLength,startTime+jobDuration);
        lastLoadedDataStart2 = dataStartTime2;
        tmpDuration = lastLoadedDataEnd2 - lastLoadedDataStart2;  
        [tempadcdata2, timevect2, data2OK] = getFrameData(params.channelName2, dataStartTime2,tmpDuration);
      end
      longadcdata2.data = tempadcdata2;
      longadcdata2.deltaT = timevect2(2)-timevect2(1);

      startindex = (dataStartTime1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT + 1;
      endindex = (dataStartTime1 + dataDuration1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT;
      adcdata1.data = longadcdata1.data(startindex:endindex);
      adcdata1.tlow = dataStartTime1;
      adcdata1.deltaT = longadcdata1.deltaT;

      startindex = (dataStartTime2 - lastLoadedDataStart2) ...
                      /longadcdata2.deltaT + 1;
      endindex = (dataStartTime2 + dataDuration2 - lastLoadedDataStart2) ...
                      /longadcdata2.deltaT;
      adcdata2.data = longadcdata2.data(startindex:endindex);
      adcdata2.tlow = dataStartTime2;
      adcdata2.deltaT = longadcdata2.deltaT;

      % if either data stream is bad, set flag and exit loop
      if ( (data1OK==false) | (data2OK==false) )
        badSegmentData = true;
        break
      end

      % downsample the data 
      sampleRate1 = 1/adcdata1.deltaT;
      sampleRate2 = 1/adcdata2.deltaT;
      p1 = 1;  
      p2 = 1;  
      q1 = floor(sampleRate1/params.resampleRate1);
      q2 = floor(sampleRate2/params.resampleRate2);

      deltaT1 = 1/params.resampleRate1;
      deltaT2 = 1/params.resampleRate2;

      if sampleRate1 == params.resampleRate1
        data = adcdata1.data;
      else
        data = resample(adcdata1.data, p1, q1, params.nResample1, params.betaParam1);
      end
      n1(J) = constructTimeSeries(data, adcdata1.tlow, deltaT1);
      if sampleRate2 == params.resampleRate2
        data = adcdata2.data;
      else
        data = resample(adcdata2.data, p2, q2, params.nResample2, params.betaParam2);
      end
      n2(J) = constructTimeSeries(data, adcdata2.tlow, deltaT2);

      % free-up some memory
      clear adcdata1; 
      clear adcdata2;

    end % loop over segments J

    % if bad data or bad response function for any segment, continue with 
    % next interval
    if badSegmentData
      continue
    else
      isFirstPass = false;
    end

  else

    % shift data and response functions accordingly
    for J=1:params.numSegmentsPerInterval-1

      if params.doOverlap
        % shift data by half a segment; need to worry about buffer
        N1 = length(n1(J).data);
        N2 = length(n2(J).data);
        bufferOffset1 = params.bufferSecs1/n1(J).deltaT;
        bufferOffset2 = params.bufferSecs2/n2(J).deltaT;

        data  = [n1(J).data(N1/2+1-bufferOffset1:N1-bufferOffset1) ; ...
                 n1(J+1).data(1+bufferOffset1:N1/2+bufferOffset1)];
        tlow  = n1(J).tlow+intervalTimeStride;
        n1(J) = constructTimeSeries(data, tlow, n1(J).deltaT);

        data  = [n2(J).data(N2/2+1-bufferOffset2:N2-bufferOffset2) ; ...
                 n2(J+1).data(1+bufferOffset2:N2/2+bufferOffset2)];
        tlow  = n2(J).tlow+intervalTimeStride;
        n2(J) = constructTimeSeries(data, tlow, n2(J).deltaT);
      else
        % simple shift by a full segment
        n1(J)=n1(J+1);
        n2(J)=n2(J+1);
     
        response1(J)=response1(J+1);
        response2(J)=response2(J+1);

        transfer1(J)=transfer1(J+1);
        transfer2(J)=transfer2(J+1);
      end

    end % loop over J

    % read in time-series data for next segment
    dataStartTime1 = intervalStartTime ...
                     + (params.numSegmentsPerInterval-1)*params.segmentDuration ...
                     - params.bufferSecs1;
    dataStartTime2 = intervalStartTime ...
                     + (params.numSegmentsPerInterval-1)*params.segmentDuration ...
                     - params.bufferSecs2;

    dataDuration1  = params.segmentDuration + 2*params.bufferSecs1;
    dataDuration2  = params.segmentDuration + 2*params.bufferSecs2;

    if dataStartTime1+dataDuration1 > lastLoadedDataEnd1
      lastLoadedDataEnd1 = min(dataStartTime1+params.minDataLoadLength,startTime+jobDuration);
      lastLoadedDataStart1 = dataStartTime1;
      tmpDuration = lastLoadedDataEnd1 - lastLoadedDataStart1;
      [tempadcdata1, timevect1, data1OK] = getFrameData(params.channelName1,...
                              dataStartTime1, tmpDuration);
    end
    longadcdata1.data = tempadcdata1;
    longadcdata1.deltaT = timevect1(2) - timevect1(1);

    if dataStartTime2+dataDuration2 > lastLoadedDataEnd2
      lastLoadedDataEnd2 = min(dataStartTime2+params.minDataLoadLength,startTime+jobDuration);
      lastLoadedDataStart2 = dataStartTime2;
      tmpDuration = lastLoadedDataEnd2 - lastLoadedDataStart2;
      [tempadcdata2, timevect2, data2OK] = getFrameData(params.channelName2,...
                                dataStartTime2,tmpDuration);
    end
    longadcdata2.data = tempadcdata2;
    longadcdata2.deltaT = timevect2(2) - timevect2(1);

    startindex = (dataStartTime1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT + 1;
    endindex = (dataStartTime1 + dataDuration1 - lastLoadedDataStart1) ...
                      /longadcdata1.deltaT;
    adcdata1.data = longadcdata1.data(startindex:endindex);
    adcdata1.tlow = dataStartTime1;
    adcdata1.deltaT = longadcdata1.deltaT;

    startindex = (dataStartTime2 - lastLoadedDataStart2) ...
                      /longadcdata2.deltaT + 1;
    endindex = (dataStartTime2 + dataDuration2 - lastLoadedDataStart2) ...
                      /longadcdata2.deltaT;
    adcdata2.data = longadcdata2.data(startindex:endindex);
    adcdata2.tlow = dataStartTime2;
    adcdata2.deltaT = longadcdata2.deltaT;

    % if either data stream is bad, set flag and exit loop
    if ( (data1OK==false) | (data2OK==false) )
      badSegmentData = true;
      break
    end

   % downsample the data 
    sampleRate1 = 1/adcdata1.deltaT;
    sampleRate2 = 1/adcdata2.deltaT;
    p1 = 1;  
    p2 = 1;  
    q1 = floor(sampleRate1/params.resampleRate1);
    q2 = floor(sampleRate2/params.resampleRate2);

    deltaT1 = 1/params.resampleRate1;
    deltaT2 = 1/params.resampleRate2;

    if sampleRate1 == params.resampleRate1
      data = adcdata1.data;
    else
      data = resample(adcdata1.data, p1, q1, params.nResample1, params.betaParam1);
    end
    n1(params.numSegmentsPerInterval) = ...
      constructTimeSeries(data, adcdata1.tlow, deltaT1);

    if sampleRate2 == params.resampleRate2
      data = adcdata2.data;
    else
      data = resample(adcdata2.data, p2, q2, params.nResample2, params.betaParam2);
    end
    n2(params.numSegmentsPerInterval) = ...
      constructTimeSeries(data, adcdata2.tlow, deltaT2);

    % free-up some memory
    clear adcdata1; 
    clear adcdata2;

  end % of if isFirstPass ... else ... end

  % if bad data or bad response function for any segment, continue with 
  % next interval
  if badSegmentData
    continue
  end

  % initialize data array for average psds
  avg_data1 = zeros(numFreqs,1);
  avg_data2 = zeros(numFreqs,1);

  % loop over number of segments
  for J=1:params.numSegmentsPerInterval

    if params.doShift1
      shiftoffset = round(params.ShiftTime1 / n1(J).deltaT);

      if (shiftoffset >= length(n1(J).data))
        error('Time-shift is larger than buffer segments.');
      end

      qtempdata1 = circshift(n1(J).data,shiftoffset);
    else
      qtempdata1 = n1(J).data;
    end
    if params.doShift2
      shiftoffset = round(params.ShiftTime2 / n2(J).deltaT);

      if (shiftoffset >= length(n2(J).data))
        error('Time-shift is larger than buffer segments.');
      end

      qtempdata2 = circshift(n2(J).data,shiftoffset);
    else
      qtempdata2 = n2(J).data;
    end

    o1 = n1(J);
    o2 = n2(J);
    o1.data = qtempdata1;
    o2.data = qtempdata2;

    % high-pass filter the data (optional)
    if params.doHighPass1
      if params.highPassOrder1<=6
        highpassed1 = constructTimeSeries(filtfilt(b1,a1,o1.data), ...
          o1.tlow, o1.deltaT);
      else
        try
          params.cascade1;
        catch
          params.cascade1=true;
          fprintf('Warning: HP filter1 n>6 requires a cascade filter\n');
        end
        tmp1 = cascadefilter(o1.data, params.highPassOrder1, ...
          params.highPassFreq1, params.resampleRate1);
        highpassed1 = constructTimeSeries(tmp1, o1.tlow, o1.deltaT);
      end
    else
      highpassed1 = o1;
    end
%------------------------------------------------------------------------------
    if params.doHighPass2
      if params.highPassOrder1<=6
        highpassed2 = constructTimeSeries(filtfilt(b2,a2,o2.data), ...
          o2.tlow, o2.deltaT);
      else
        try
          params.cascade2;
        catch
          params.cascade2=true;
          fprintf('Warning: HP filter2 n>6 requires a cascade filter\n');
        end
        tmp2 = cascadefilter(o2.data, params.highPassOrder2, ...
          params.highPassFreq2, params.resampleRate2);
        highpassed2 = constructTimeSeries(tmp2, o2.tlow, o2.deltaT);
      end
    else
      highpassed2 = o2;
    end

    % chop-off bad data at start and end of HP filtered, resampled data
    firstIndex1 = 1 + params.bufferSecs1*params.resampleRate1;
    firstIndex2 = 1 + params.bufferSecs2*params.resampleRate2;

    lastIndex1  = length(highpassed1.data)-params.bufferSecs1*params.resampleRate1;
    lastIndex2  = length(highpassed2.data)-params.bufferSecs2*params.resampleRate2;

    r1(J) = constructTimeSeries(highpassed1.data(firstIndex1:lastIndex1), ...
                                highpassed1.tlow + params.bufferSecs1, ...
                                highpassed1.deltaT);
    r2(J) = constructTimeSeries(highpassed2.data(firstIndex2:lastIndex2), ...
                                highpassed2.tlow + params.bufferSecs2, ...
                                highpassed2.deltaT);

    % estimate power spectra for optimal filter
    [temp1,freqs1] = psd(r1(J).data, psdFFTLength1, 1/r1(J).deltaT, ...
                         psdWindow1, psdOverlapLength1, detrendFlag1);
    [temp2,freqs2] = psd(r2(J).data, psdFFTLength2, 1/r2(J).deltaT, ...
                         psdWindow2, psdOverlapLength2, detrendFlag2);

    % normalize appropriately

    % If all the bins in the PSD are independent, we are dealing with
    % complex params.heterodyned data
    if length(temp1) == psdFFTLength1
      % Account for heterodyning of data.  This appears to be
      % the correct normalization because psd(), unlike pwelch(),
      % calculates the two-sided PSD of both real and complex data.
      freqs1shifted = fftshift(freqs1);
      spec1 = ...
          constructFreqSeries(2*r1(J).deltaT*fftshift(temp1), ...
		        	r1(J).fbase + freqs1shifted(1) ...
				- 1/r1(J).deltaT, ...
				freqs1(2)-freqs1(1), 0);
    else
      spec1 = ...
	  constructFreqSeries(2*r1(J).deltaT*temp1, freqs1(1), ...
				freqs1(2)-freqs1(1), 0);
    end

    % If all the bins in the PSD are independent, we are dealing with
    % complex params.heterodyned data
    if length(temp2) == psdFFTLength2
      % Account for heterodyning of data.  This appears to be
      % the correct normalization because psd(), unlike pwelch(),
      % calculates the two-sided PSD of both real and complex data.
      freqs2shifted = fftshift(freqs2);
      spec2 = ...
            constructFreqSeries(2*r2(J).deltaT*fftshift(temp2), ...
				r2(J).fbase + freqs2shifted(1) ...
				- 1/r2(J).deltaT, ...
				freqs2(2)-freqs2(1), 0);
    else
      spec2 = ...
	    constructFreqSeries(2*r2(J).deltaT*temp2, freqs2(1), ...
				freqs2(2)-freqs2(1), 0);
    end
    % coarse-grain noise power spectra to desired freqs
    psd1 = coarseGrain(spec1, params.flow, params.deltaF, numFreqs);
    psd2 = coarseGrain(spec2, params.flow, params.deltaF, numFreqs);

    % calibrate the power spectra
    calPSD1 = ...
      constructFreqSeries(psd1.data, ...
                            psd1.flow, psd1.deltaF, psd1.symmetry);
    calPSD2 = ...
      constructFreqSeries(psd2.data, ...
                            psd2.flow, psd2.deltaF, psd2.symmetry);
  
    % calculate avg power spectra, ignoring middle segment if desired
    midSegment = (params.numSegmentsPerInterval+1)/2;
    if ( (params.ignoreMidSegment) & (J==midSegment) )
      % do nothing
      %fprintf('Ignoring middle segment\n');
    else
      avg_data1 = avg_data1 + calPSD1.data;
      avg_data2 = avg_data2 + calPSD2.data;
    end

    if (J==midSegment)
      % This calculates the "naive" theorerical variance, i.e.,
      % that calculated from the current segment without averaging 
      % over the whole interval.
      % This is useful for the stationarity veto which excludes
      % segments for which the naive sigma differs too much from
      % the one calculated with the sliding PSD average.
      [naiQ, naiVar, naiSensInt] ...
	    = calOptimalFilter(params.segmentDuration, gamma, ...
			       params.fRef, params.alphaExp, ... 
			       calPSD1, calPSD2, ...
			       dataWindow1, dataWindow2, mask);
      naiP1 = calPSD1.data;
      naiP2 = calPSD2.data;
    end
 
  end % loop over segments J

  % construct average power spectra
  if params.ignoreMidSegment
    avg_data1 = avg_data1/(params.numSegmentsPerInterval-1);
    avg_data2 = avg_data2/(params.numSegmentsPerInterval-1);
  else
    avg_data1 = avg_data1/params.numSegmentsPerInterval;
    avg_data2 = avg_data2/params.numSegmentsPerInterval;
  end

  calPSD1_avg = constructFreqSeries(avg_data1, params.flow, params.deltaF, 0);
  calPSD2_avg = constructFreqSeries(avg_data2, params.flow, params.deltaF, 0);

  % calculate optimal filter, theoretical variance, and sensitivity 
  % integrand using avg psds
  [Q, ccVar, sensInt] = calOptimalFilter(params.segmentDuration, gamma, ...
                                           params.fRef, params.alphaExp, ... 
                                           calPSD1_avg, calPSD2_avg, ...
                                           dataWindow1, dataWindow2, mask);

  % analyse the middle data segment %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % window, zero-pad and fft the data
  rbartilde1 = windowAndFFT(r1(midSegment), dataWindow1, fftLength1);
  rbartilde2 = windowAndFFT(r2(midSegment), dataWindow2, fftLength2);
  % already calibrated data, so response functions are 1
  response.data = ones(length(f),1); % overlap reduction function; not used for PEM
  response.flow = params.flow;
  response.deltaF = params.deltaF;

  % calculate the value and spectrum of the CC statistic
  [ccStat,CSD] = calCSD(rbartilde1, rbartilde2, Q, ...
                                     response, response);

  % calculate the value and spectrum of the CC statistic
  [fft1,fft2] = calFFT(rbartilde1, rbartilde2, Q, ...
                                       response, response);

  %recover the window parameters, add them to the structure
  [w1w2bar, w1w2squaredbar, w1w2ovlsquaredbar] = windowFactors(dataWindow1,...
                                                  dataWindow2);
  params.w1w2bar = w1w2bar;
  params.w1w2squaredbar = w1w2squaredbar;
  params.w1w2ovlsquaredbar = w1w2ovlsquaredbar;
  params.bias = 1/(2*(params.segmentDuration * params.deltaF * 2 - 1) * (9/11)) + 1;
  params.naivebias = 1/((params.segmentDuration * params.deltaF * 2 - 1) * (9/11)) + 1;
  params.SiderealTime = GPStoGreenwichMeanSiderealTime(r1(midSegment).tlow);
  

  %check if the interval is bad
  if params.doBadGPSTimes %determine if the current interval is bad
    c1 = intervalStartTime - bufferSecsMax < badtimesend;
    c2 = intervalStartTime + 3*params.segmentDuration + bufferSecsMax > badtimesstart;

    if sum(c1&c2) > 0 | sqrt(ccVar/naiVar)>params.maxDSigRatio | sqrt(ccVar/naiVar)<params.minDSigRatio
      badtimesflag = 1;
    else
      badtimesflag = 0;
    end
  else %do not check for bad times
    badtimesflag = 0;
  end

  %finally record the value in the relevant filename, if the bad time flag ok
  if badtimesflag == 0
    gpsstring = num2str(r1(midSegment).tlow);
    halfdur = params.segmentDuration/2;
    filename = [params.outputFilePrefix '-' ...
      gpsstring '-' num2str(halfdur) '.gwf'];
    P1 = calPSD1_avg.data;
    P2 = calPSD2_avg.data;
    CC = 2 * CSD.data / w1w2bar / params.segmentDuration;

    rec(1).channel = [params.ifo1 ':AdjacentPSD'];
    rec(1).data = P1;
    rec(1).type = 'd';
    rec(1).mode = 'a';

    rec(2).channel = [params.ifo2 ':AdjacentPSD'];
    rec(2).data = P2;
    rec(2).type = 'd';
    rec(2).mode = 'a';

    rec(3).channel = [params.ifo1 ':LocalPSD'];
    rec(3).data = naiP1;
    rec(3).type = 'd';
    rec(3).mode = 'a';

    rec(4).channel = [params.ifo2 ':LocalPSD'];
    rec(4).data = naiP2;
    rec(4).type = 'd';
    rec(4).mode = 'a';

    rec(5).channel = [params.ifo1 params.ifo2 ':CSD'];
    rec(5).data = CC;
    rec(5).type = 'dc';
    rec(5).mode = 'a';

%cet-------try to pass parameters as such---------------
    etparams.name = [params.ifo1 params.ifo2 ':Params'];
    if ~params.stochmap
      % the next two lines remove sub-structs that are used for STAMP only
      params0 = rmfield(params, 'pass');
      params0 = rmfield(params0, 'job');
      doublestr = [];
      tt = fieldnames(params0);
      kk=11; %cet
      for jj = 1:length(tt)
        tmp = getfield(params0, tt{jj});
        if ischar(tmp)
          tmpstr = [tt{jj} ' ' tmp];
        else
          if size(tmp,1)>1
            %if it is an array, needs to be a row 
            tmp = transpose(tmp);
          end
            % this if-then statement skips over parameter sub-structs that are
            % used primar
            tmpstr = [tt{jj} ' ' num2str(tmp)];
            tmp = num2str(tmp);
        end
        ttvals{jj}=tmp;
        doublestr = [doublestr double(tmpstr) 10];
      end
      etparams.parameters=horzcat(tt, ttvals');
    end

    rec(6).channel = [params.ifo1 params.ifo2 ':flow'];
    rec(6).data = params.flow;
    rec(6).type = 'd';
    rec(6).mode = 'a';

    rec(7).channel = [params.ifo1 params.ifo2 ':fhigh'];
    rec(7).data = params.fhigh;
    rec(7).type = 'd';
    rec(7).mode = 'a';

    rec(8).channel = [params.ifo1 params.ifo2 ':deltaF'];
    rec(8).data = params.deltaF;
    rec(8).type = 'd';
    rec(8).mode = 'a';

    rec(9).channel = [params.ifo1 params.ifo2 ':GPStime'];
    rec(9).data = r1(midSegment).tlow;
    rec(9).type = 'd';
    rec(9).mode = 'a';

    rec(10).channel = [params.ifo1 params.ifo2 ':params.segmentDuration'];
    rec(10).data = params.segmentDuration;
    rec(10).type = 'd';
    rec(10).mode = 'a';

    rec(11).channel = [params.ifo1 params.ifo2 ':w1w2bar'];
    rec(11).data = params.w1w2bar;
    rec(11).type = 'd';
    rec(11).mode = 'a';

    rec(12).channel = [params.ifo1 params.ifo2 ':w1w2squaredbar'];
    rec(12).data = params.w1w2squaredbar;
    rec(12).type = 'd';
    rec(12).mode = 'a';
				    
    rec(13).channel = [params.ifo1 params.ifo2 ':w1w2ovlsquaredbar'];
    rec(13).data = params.w1w2ovlsquaredbar;
    rec(13).type = 'd';
    rec(13).mode = 'a';

    rec(14).channel = [params.ifo1 params.ifo2 ':bias'];
    rec(14).data = params.bias;
    rec(14).type = 'd';
    rec(14).mode = 'a';
 
    rec(15).channel = [params.ifo1 params.ifo2 ':naivebias'];
    rec(15).data = params.naivebias;
    rec(15).type = 'd';
    rec(15).mode = 'a';
 
    rec(16).channel = [params.ifo1 params.ifo2 ':SiderealTime'];
    rec(16).data = params.SiderealTime;
    rec(16).type = 'd';
    rec(16).mode = 'a';
    rec(17).channel = [params.ifo1 params.ifo2 ':params.numSegmentsPerInterval'];
    rec(17).data = params.numSegmentsPerInterval;
    rec(17).type = 'd';
    rec(17).mode = 'a';

    rec(18).channel = [params.ifo1 params.ifo2 ':params.resampleRate1'];
    rec(18).data = params.resampleRate1;
    rec(18).type = 'd';
    rec(18).mode = 'a';

    rec(19).channel = [params.ifo1 params.ifo2 ':params.resampleRate2'];
    rec(19).data = params.resampleRate2;
    rec(19).type = 'd';
    rec(19).mode = 'a';

    rec(20).channel = [params.ifo1 params.ifo2 ':params.nResample1'];
    rec(20).data = params.nResample1;
    rec(20).type = 'd';
    rec(20).mode = 'a';

    rec(21).channel = [params.ifo1 params.ifo2 ':params.nResample2'];
    rec(21).data = params.nResample2;
    rec(21).type = 'd';
    rec(21).mode = 'a';

    rec(22).channel = [params.ifo1 params.ifo2 ':params.betaParam1'];
    rec(22).data = params.betaParam1;
    rec(22).type = 'd';
    rec(22).mode = 'a';

    rec(23).channel = [params.ifo1 params.ifo2 ':params.betaParam2'];
    rec(23).data = params.betaParam2;
    rec(23).type = 'd';
    rec(23).mode = 'a';

    if ~params.stochmap
      mkframe(filename,rec,'n',params.segmentDuration,r1(midSegment).tlow,etparams);
    else
      map.P1(:,I) = rec(1).data;
      map.P2(:,I) = rec(2).data;
      map.naiP1(:,I) = rec(3).data;
      map.naiP2(:,I) = rec(4).data;
      map.cc(:,I) = CSD.data.*Q.data;
      map.sensInt(:,I) = sensInt.data;
      map.ccVar(I) = ccVar;
      map.segstarttime(:,I) = rec(9).data;
      map.fft1(:,I) = fft1.data;
      map.fft2(:,I) = fft2.data;
      %map.bias(:,I) = rec(14).data;
      %map.naivebias(:,I) = rec(15).data;
    end % params.stochmap if statement

  end % badtimesflag if statement

end % loop over intervals I

if params.stochmap
  % add remaining metdata to map
  map.deltaF = params.deltaF;
  map.segDur = params.segmentDuration;
  map.start = map.segstarttime(1);
  map.f = f;
 
  %fprintf('Done with preproc phase...\n');
  if params.storemats 
    fprintf('Storing .mat files from preproc ... \n');
    % when params.batch==true we make mat files in batches
    if params.batch
      % the start of the job
      startGPS_job = map.segstarttime(1);
      % the end of the job
      endGPS_job = map.segstarttime(end) + params.segmentDuration;
      % the number of mat files to create
%      nmats = ceil( (endGPS_job-startGPS_job)/params.mapsize );
      % take into account the overlapping segment
      mod_mapsize = params.mapsize - mod(params.mapsize, ...
					 params.segmentDuration/2);
      nmats = ceil( (endGPS_job-startGPS_job) / ...
        (params.mapsize-params.segmentDuration) );
      % create nmats mat files
      for ii=1:nmats
        % warning message
        if params.segmentDuration<1 || params.segmentDuration>100
          warning('This segment duration may cause trouble: talk to Eric.');
          %If segmentDuration<1 then the file names will have a decimal in them
          %That may cause problems with regular expressions in clustermap.
          %If the segmentDuration is >100s then the mat file will have only one
          %segment in it and the requirement of an overlapping segment may
          %cause problems.  -ethrane
        end
        % maps will overlap by one segment
        params.startGPS = startGPS_job + ...
          (ii-1)*(mod_mapsize - params.segmentDuration) ;
        params.endGPS = params.startGPS + params.mapsize;
        if params.endGPS>endGPS_job
          params.endGPS=endGPS_job;
        end

        fprintf('creating mat file #%i\n', ii);
        savemap(map, params);
      end
      % when not in batch mode we make one mat file for the entire job or for 
      % some specific time interval given by start and end GPS as input 
      % parameters
    else
      savemap(map, params);
    end
  end
end

return;
