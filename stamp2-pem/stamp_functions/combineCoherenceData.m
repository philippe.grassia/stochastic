function [finalMatFile] = combineCoherenceData2(lockfiles,channelsListFile,outputPrefix,outputTitle)
% description:
% combineBokehPlots(startGPSvec, endGPSvec, pemFile,channelsListFile)
% combines coherence data for start and end times contained
% in startGPSvec and endGPSvec that has already been produced.
% saves a lock file in :
% params.outputFilePrefix /combined_lock_startGPSvec(1)_endGPSvec(end).mat

% 
tmp = load(lockfiles{1});
ff = tmp.ff;
if ~(length(lockfiles) == 1)
  if exist(channelsListFile)
    [channelNames, channelSampleRates] = ...
	textread(channelsListFile,'%s %f');
  else
    error ('No channels list provided \n');
  end
  
  darmChannel = strrep(channelNames{1},':','-');
  psd1_matrix = zeros(length(ff),length(channelNames));
  psd2_matrix = zeros(length(ff),length(channelNames));
  csd12_matrix = zeros(length(ff),length(channelNames));
  coherence_matrix = zeros(length(ff),length(channelNames));
  Ntot = 0;
  for ii=1:length(lockfiles)
    fprintf('loading %s...\n',lockfiles{ii});
    tempData=load(lockfiles{ii});
    if isempty(tempData.psd1_matrix);
      continue;
    end
    if ~(ff==tempData.ff)
      fprintf('frequencies dont match up...continuing on...\n');
    end
    
    for jj=1:length(channelNames)
      [idxs] = find(strcmp(tempData.chnlnames,channelNames(jj)));
      N=0;
      if ~(isempty(idxs));
	% get number of segments
	%try
	N = tempData.theorCoherence.^-2;
	% average old map with new map
	psd1_matrix(:,jj) = (N*tempData.psd1_matrix(:,idxs(1))+psd1_matrix(:,jj)*Ntot)/(N+Ntot);
	psd2_matrix(:,jj) = (N*tempData.psd2_matrix(:,idxs(1))+psd2_matrix(:,jj)*Ntot)/(N+Ntot);
	csd12_matrix(:,jj) = (N*tempData.csd12_matrix(:,idxs(1))+csd12_matrix(:,jj)*Ntot)/(N+Ntot);
	coherence_matrix(:,jj) = ...
	    abs(csd12_matrix(:,jj))./(psd1_matrix(:,jj).*psd2_matrix(:,jj)).^0.5;
	%catch
	%  continue;
	%end
	% now add these segments to previous ones since this map has now
	% been properly added into our average
      end
    end
    Ntot = Ntot+N;
  end
  chnlnames=channelNames;
  coherence_matrix(isnan(coherence_matrix))=0;
  theorCoherence = Ntot.^-0.5;
  finalMatFile = [outputPrefix '/' outputTitle '.mat'];
  save([outputPrefix '/' outputTitle '.mat'],'coherence_matrix','psd1_matrix','psd2_matrix','csd12_matrix','chnlnames','ff','theorCoherence');		
else
  cmd = sprintf('cp %s %s',lockfiles{1},[outputPrefix '/' outputTitle '.mat'])
  finalMatFile = [outputPrefix '/' outputTitle '.mat']
  junk = system(cmd);
end