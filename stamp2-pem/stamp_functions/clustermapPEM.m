function stoch_out = clustermapPEM(paramsFile, channelName1, channelName2, startGPS, endGPS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function stoch_out = clustermapPEM(paramsFile, channelName1, channelName2, startGPS, endGPS)
% Adapted from STAMP 
% 
% params is a text file in the form of a name/value pairs .
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Running analysis %s Vs %s \n',channelName1, channelName2);
tic; % check how long does it take to run

% Turn of warning due to use of psd function
warning('off','signal:psd:PSDisObsolete');

% Extract parameters from input parameter file that will be passed between
% routines
params = readParamsFromFilePEM(paramsFile);
% add misc parameters
params.ifo1 = channelName1(1:2);
params.ifo2 = channelName2(1:2);

% To use it as a compiled code
startGPS = strassign(startGPS);
endGPS = strassign(endGPS);

% store it in params struct
params.startGPS = startGPS;
params.endGPS = endGPS;
params.channelName1 = channelName1;
params.channelName2 = channelName2;

% locate or create mats for the requested data
% if mat files containing ft-information already available, use that
% (for backward compatibility)
if params.matavailable
  if params.anteproc.loadFiles
    % Load anteproc'd .mat files.  Contains separate FFT for each detector.
    % CSD not calculated yet in these .mat files.
    fprintf('Loading anteproc''d .mat files.\n');
    [matlist,params] = find_anteproc_mats(params);
    [map1, params1] = load_anteproc_mats(matlist,params,1);
    [map2, params2] = load_anteproc_mats(matlist,params,2);
    [params, map1, map2] = check_anteproc_data(map1,map2,params1,params2, ...
                                               params);
    % Set segstarttimes to be equal (may need to be adjusted due to
    % time-shifts).
    map2.segstarttime = map1.segstarttime;
    [map, params, pp] = combine_anteproc_mats(map1,map2,params);
    clear map1 map2 params1 params2;
  else
    % Load normal pre-processed STAMP .mat files.
    [matlist, params] = findmats(params);
    [map, params, pp] = loadmats(matlist, params);
  end
else % otherwsie create ft-maps
  % Making sure that the sampling rates and resampling rates are fine;
  % we collect 1 sec of data to identify sampling rates of the channels
  [nouse1, timevect1, nouse2] = getFrameData(channelName1, startGPS, 1);
  [nouse3, timevect2, nouse4] = getFrameData(channelName2, startGPS, 1);
  if (isempty(nouse1) | isempty(nouse3))
    stoch_out = {}; 
    return;
  end
  params.resampleRate1 = min([1/(timevect1(2)-timevect1(1)) 1/(timevect2(2)-timevect2(1)) params.resampleRate1]);
  params.resampleRate2 = min([1/(timevect1(2)-timevect1(1)) 1/(timevect2(2)-timevect2(1)) params.resampleRate2]);
  params.fhigh = min([params.resampleRate1/2-params.deltaF params.resampleRate2/2-params.deltaF params.fhigh]); 
  if params.fRef > params.fhigh
     params.fRef = ceil((params.flow + params.fhigh)/2);
  end

  % Generate meta data of spectrograms
  [map, params] = preprocPEM(params);
  % creating other meta data structure
  pp.flow = params.flow;
  pp.fhigh = params.fhigh;
  pp.deltaF = params.deltaF;
  pp.w1w2bar = params.w1w2bar;
  pp.w1w2squaredbar = params.w1w2squaredbar;
  pp.w1w2ovlsquaredbar = params.w1w2ovlsquaredbar;
end

% check values of params for self-consistency
%params = paramCheck(params);

% apply Frequency mask
if params.doMaskMap
  map = maskmapsPEM(map, params);
end

% initialize ccSpec struct and other radiometer variables
ccSpec.data = map.cc;
ccSpec.flow = params.flow;
ccSpec.deltaF = pp.deltaF;
sensInt.flow = params.flow;
sensInt.deltaF = pp.deltaF;
sensInt.data = map.sensInt;
ccVar = map.ccVar;
midGPSTimes = map.segstarttime + params.segmentDuration/2;
det1 = getdetector(params.ifo1);
det2 = getdetector(params.ifo2);

% get sky location
source = [6 30]; % random sky location; for colocated detectors with 
                 % omidirectional response, sky location don't matter

% the number of searches executed
ss=0;

% loop over search directions
for kk=1:size(source,1)
  % initialize ft-map struct
  %map = freshmap(map);

  % run the radiometer; calculate ft-maps
  [map] = ccSpecReadout_stamp(det1, det2, ... 
    midGPSTimes, source(kk,:), ccSpec, ccVar, sensInt, params, pp, map);

  % define SNR map
  map.snr = map.y ./ map.sigma;
  map.snrz = map.z ./map.sigma;

  if params.glitchinj
     map = glitchinj(params,map);
  end

  % initialize injection struct
  inj.init = [];

  % loop over injection trials
  % Note: if no injections are performed, then alpha_n is set to one and
  % inj_trials is set to one.
  for tt=1:1 % legacy loop
    map0 = map;

    % calculate Xi stastic
    map0 = calXi(params, map0);
    
    % apply glitch cut if requested in params struct
    % cutcols records the ft-map columns (segments) that are cut
    [map0, stoch_out.cutcols{kk}, stoch_out.detcuts{kk}] = ...
	glitchCut(params, map0);

    % save mat file(s) -- one for each search direction
    if params.saveMat
      save([params.outputfilename '_' num2str(kk) '.mat'], ...
           'map0','params','pp');
    end

    % broadband radiometer comparison
    if params.doRadiometer
      y_rad = sum(map0.y.*map0.sigma.^-2, 1) ./ sum(map0.sigma.^-2, 1);
      y_sig = sum(map0.sigma.^-2, 1).^-0.5;
      fprintf('radiometer=\n');
      fprintf('%1.4e +/- %1.4e\n', y_rad, y_sig);
    end

    % perform clustering algorithms if requested 
    if params.doClusterSearch 
      [stoch_out, ss] = searches(map0, params, kk, ss, stoch_out);
    end
 
    % Add search direction to map struct.
    map0.source = source(kk,:);

    % Make sure max cluster is saved; compatible with searches that use a
    % cluster struct (burstegard, BurstCluster, Box search).
    if (params.doBurstegard | params.doBoxSearch | params.doClusterSearch ...
      | params.doStochtrack)
      if ~exist('max_cluster')
        max_cluster.snr_gamma = -1;
      end
      if (max_cluster.snr_gamma < stoch_out.cluster.snr_gamma)
        max_cluster = stoch_out.cluster;
	    max_map = map0;
      end
    end

    if params.doPE
      stoch_out = stamp_pe(map0, params, stoch_out);
    end

  end
end

% Save 'max' structs in stoch_out struct.
% Also set 'map' to be 'max_map' for plotting purposes.
if (params.doBurstegard | params.doBoxSearch | params.doClusterSearch | ...
  params.doStochtrack)
  map = max_map;
  % Print search result summary to screen.
  fprintf(['\nSearch results:\nMax. SNR = %.3f, found at (ra,dec) =' ...
    ' (%.2f h, %.2f deg.)\n'], max(stoch_out.max_SNR), map.source(1), ...
	  map.source(2));
  stoch_out.cluster = max_cluster;
  stoch_out.cluster.source = max_map.source;
end
if params.returnMap
  stoch_out.map = map;
end

% Plots for search direction corresponding to maximum SNR.
if params.savePlots
  saveplotsAndData(params, map0);
  if params.doBurstegard
    burstegard_plot(max_cluster, map, params);
  end
end

% record overall map sensitivity
stoch_out.ccSigBar = sqrt(mean(ccVar(~isnan(ccVar))));

% store param values
stoch_out.params = params;

% store source locations %% Useful for all-sky analyses
stoch_out.sources = source;

% warn the user if diagnostic tools are on
if params.diagnostic
  fprintf('CAUTION: diagnostic tools on.\n'); 
end

% close all open plots
close all;
fprintf('Elapsed time = %f seconds.\n', toc);
fprintf('Done\n');

return;
