function [coherenceMatFile] = harvestAndSaveCoherenceData(pemParamsFile,channelListFile,startGPS,endGPS);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% loops over channels in channel list
% and aggregates coherence data from each
% channel and saves one file containing all of the data in
% "coherenceMatFile"
%
% %%%%%%%%%%%%% INPUTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pemParamsFile: param file used to create data
%                 we're aggregating
% channelListFile: list of channels to search through
% start/endGPS: start and end times to combine
%
% %%%%%%%%%%%%%% OUTPUTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% coherenceMatFile: .mat file containing aggregated
%                   coherence information:
%   -- coherenceMatrix: coherence spectra for each channel
%   -- ff: frequencies used
%   -- psd1/2_matrix: psd for 1st/2nd channel in xcorr
%   -- csd12_matrix: csd between two channels
%   -- theorCoherence theoretical coherence (1/sqrt(N))
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Read in channel names and sample rates from file
[channelNames channelSampleRates] = textread(channelListFile,'%s %d','commentstyle','matlab');

chnlnames = [];
coherence_matrix = [];
psd1_matrix = [];
psd2_matrix = [];
csd12_matrix = [];
theorCoherence = 1;
% read in parameters
params = readParamsFromFilePEM(pemParamsFile);

% find main directory where channel data is
if strcmp(params.outputFilePrefix,'/');
  mainDirectory = [params.outputFilePrefix num2str(startGPS) '_' num2str(endGPS) ...
		 '/' strrep(channelNames{1},':','-')];
else
  mainDirectory = [params.outputFilePrefix '/' num2str(startGPS) '_' num2str(endGPS) ...
                 '/' strrep(channelNames{1},':','-')];
end

% set up frequency spectrum
ff = [params.flow:params.deltaF:params.fhigh];
index=1;
for ii = 1:length(channelNames)
  % define file name
  coherenceFile = [mainDirectory '/' strrep(channelNames{ii},':','-') '/coherence.mat'];
  if exist(coherenceFile) % check if data exists
    chnlnames{index} = channelNames{ii};
    index=index+1;
    tempData = load(coherenceFile);% load data
    if all(isnan(tempData.coherence)) % continue of no file
      continue;
    end 
    % plot spectrum for this channel
    try 
      tempData.psd1;
    catch
      tempData.psd1 = NaN(length(tempData.coherence),1);
      tempData.psd2 = NaN(length(tempData.coherence),1);
      tempData.csd12 = NaN(length(tempData.coherence),1);
    end 
    theorCoherence = tempData.theorCoherence(1);
%end
    if ~(length(ff) == length(tempData.coherence))
      % for csd and coherence
      % add in zeros or NaNs so everything is the right length
      % since some channels don't sample high enough
      extra = zeros(length(ff)- ... 
                    length(tempData.coherence),1); 
      % for psd so combining coherences later we don't run into divergences. 
      extranans = NaN(length(ff) - length(tempData.coherence),1);
      tempData.coherence = [tempData.coherence;extra];
      tempData.psd1 = [tempData.psd1;extranans];
      tempData.psd2 = [tempData.psd2;extranans];
      tempData.csd12 = [tempData.csd12;extranans];
    end 

    % put data into a matrix
    coherence_matrix = [coherence_matrix tempData.coherence];
    psd1_matrix = [psd1_matrix tempData.psd1];
    psd2_matrix = [psd2_matrix tempData.psd2];
    csd12_matrix = [csd12_matrix tempData.csd12];
  else
    % if coherence file doesn't exist move on to next
    % channel.
    continue;
  end 
end
coherenceMatFile = [params.outputFilePrefix '/' num2str(startGPS) '_' ...
		    num2str(endGPS) '/coherence.mat'];

				flow = params.flow;
				fhigh = params.fhigh;
				deltaF = params.deltaF;
fprintf('saving %s...\n',coherenceMatFile);
save(coherenceMatFile,...
     'ff','chnlnames','coherence_matrix','psd1_matrix',...
     'psd2_matrix','csd12_matrix','theorCoherence','flow','fhigh','deltaF');






