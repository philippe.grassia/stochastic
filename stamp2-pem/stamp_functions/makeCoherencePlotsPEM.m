function makeCoherencePlotsPEM_test(coherenceData,output_directory,plot_title,subsystem,do_log)
% MAKECOHERENCEPLOTSPEM creates a .png coherence matrix and calls the bokeh 
% plotting software to create a bokeh html coherence matrix as well
%	 = MAKECOHERENCEPLOTSPEM(COHERENCE_MAT_FILE,OUTPUT_DIRECTORY,PLOT_TITLE,DOLOG)
%%%%%%%% INPUTS	%%%%%%%%%%%%%%%%%%%%%%%%%%%
% coherenceData : struct containing following information:
% 		--------> coherence_matrix : coherence data, columns are spectra
% 		--------> chnlnames : channel names corresponding to columns in coherence_matrix
% 		--------> ff : frequence vector used in analysis (not used in this function)
% 		--------> flow : lowest frequency value
% 		--------> fhigh : highest frequency value
% 		--------> deltaF : frequency resolution
% 		--------> theorCoherence : theoretical coherence value based on # of time segments
% output_directory - directory where we'll hsave the coherence matrix
% plot_title ------- the title of the plot (and also the name of the .png file)
% subsystem string - string used to isolate only certain channles ('all' or 'All' will choose all channels)
% do_log ----------- do log frequency scale? useful for large frequency plots
%%%%%%%% OUTPUTS/Files Created %%%%%%%%%%%%%
% creates a .png coherence matrix found at:
% -------- output_directory/plot_title.png -----------
% 
% creates a .html bokeh coherence matrix found at:
% -------- output_directory/plot_title_(frequency info).html
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% MAIN Function %%%%

% load coherence data
coherence = load(coherenceData);

if ~(strcmp(subsystem,'all') | strcmp(subsystem,'All'))
	% take only those channels with "subsystem" string in appearing
	% somewhere in their names
	fprintf('Only using channels with %s in their names...\n',subsystem);
	coherence = isolateSubsystem(coherence,subsystem);
end

% test to make sure this file is formatted properly (subroutine, see below)
[coherence,output_directory,plot_title,do_log] = ...
test_coherence_file(coherence,output_directory,plot_title,do_log);
try 
	ff=coherence.ff;
catch
	ff = [coherence.flow:coherence.deltaF:coherence.fhigh];
end

% create coherence matrix (subroutine, see below)
makeCoherenceMatrix(coherence.coherence_matrix,coherence.chnlnames,ff,output_directory,plot_title,do_log);

% create bokeh plots


end % function

function [coherence,output_directory,plot_title,do_log] = test_coherence_file(coherence,output_directory,plot_title,do_log)
% TEST_COHERENCE_FILE tests the coherence file and other inputs for this program
% to make sure they make sense, and that "coherence" has proper fields
%	[COHERENCE,OUTPUT_DIRECTORY,PLOT_TITLE,DO_LOG] = TEST_COHERENCE_FILE(COHERENCE,OUTPUT_DIRECTORY,PLOT_TITLE,DO_LOG)
%
% %%%%%%% INPUTS %%%%%%%%
% coherence ---------- coherence data (struct)
% output_directory --- output directory (string)
% plot_title --------- output directory (string)
% do_log ------------- do log axis or not (boolean)
%%%%%%%%% OUTPUTS %%%%%%%
% same as inputs, will reconfigure
% a few things if necessary
%%%%%%%%%%%%%%%%%%%%%%%%%

% coherenceMatrix
try
	coherence.coherence_matrix;
catch
	error('coherenceMatrix not in .mat file')
end

% channel names
try
	coherence.chnlnames;
catch
	error('chnlnames not in .mat file')
end

% theorCoherence
try
	coherence.theorCoherence;
catch
	error('theoretical coherence (theorCoherence) not in .mat file')
end

% flow,fhigh,deltaF

% check if output_directory exists. if not,
% make it.
if ~exist(output_directory)
	cmd = sprintf('mkdir -p %s',output_directory)
	[junk] = system(cmd);
end

% do_log set to 1 by default
try 
  do_log;
catch 
  do_log = 1;
end

end % function


function makeCoherenceMatrix(coherence_matrix,channel_names,ff,outputDirectory,matrixPlotTitle,do_log)
% MAKECOHERENCEMATRIX makes coherence matrix in .png and .html (using bokeh)
% formats
% MAKECOHERENCEMATRIX(COHERENCE_MATRIX,OUTPUT_DIRECTORY,PLOT_TITLE,DO_LOG)
%
%%%%%% INPUTS %%%%%%%
% coherence matrix-- len(ff) x len (channel_names) array of coherence values
% ff --------------- frequencies used in plotting. 
% channel_names----- channel names 
% output_directory-- directory where things will be saved
% plot_title-------- title of plot (used in saving as well)
% do_log------------ do log frequency axis?

% add extra fake channel and extra fake frequency because pcolor cuts off ends
% of whatever you give it.
channel_names{length(channel_names)+1} = 'fake';
deltaF = ff(2)-ff(1);
fhigh = ff(end);
flow = ff(1);
ff = [ff,fhigh+deltaF];
val = (fhigh+deltaF)*ones(1,length(channel_names)-1);
coherence_matrix = [coherence_matrix;(fhigh+deltaF)*ones(1,length(channel_names)-1)];

coherence_matrix = [coherence_matrix ff'];



numFreq = length(ff);
numChannels = length(channel_names);

[numFreq,numChannels] = size(coherence_matrix);
if numChannels > 1
	channelIndexes = 1:numChannels;
  [X,Y] = meshgrid(ff,channelIndexes);
  figure;
  set(gcf,'PaperUnits','inches')
  set(gcf,'PaperSize',[30 24])
  set(gcf,'PaperPositionMode','manual')
  set(gcf,'PaperPosition',[0 0 40 34])
	
  hC = pcolor(X,Y,coherence_matrix');
  % for bokeh plots
  % save coherence data and create bokeh plot
%	if doSaveCoherenceData
	  % save .mat file
%	  save([outputDirectory '/' coherenceDataSaveTitle '.mat'],...
%	 'ff','chnlnames','coherence_matrix','psd1_matrix',...
%	 'psd2_matrix','csd12_matrix','theorCoherence');
%	  % create bokeh command to call from command line
%	  bokeh_cmd = sprintf('python bokeh/stampcoh_html.py -i %s -o %s',...
%			[outputDirectory '/' coherenceDataSaveTitle '.mat'],...
%			[outputDirectory '/' coherenceDataSaveTitle]);
%	  % call bokeh plotting code
%	  [junk] = system(bokeh_cmd);
%	end

  set(gcf,'Renderer','zbuffer');
  colormap(jet)
  %shading interp
  t = colorbar('peer',gca);
  set(get(t,'ylabel'),'String','Coherence', 'FontSize', 20);
  set(t,'fontsize',20);
  set(hC,'LineStyle','none');
  grid
  set(gca,'clim',[0 1])
  if do_log
    set(gca,'xscale','log');
    ticks = logspace(log10(flow),log10(fhigh),10);
    set(gca,'XTick',ticks)
  else
    set(gca,'XTick',min(ff):1:max(ff))
  end
  set(gca,'YTick',channelIndexes+0.5)
  set(gca,'YTickLabel',channel_names)
  pretty;
  h_xlabel = get(gca, 'XLabel');
  set(h_xlabel, 'FontSize', 20);
  h_ylabel = get(gca, 'YLabel');
  set(h_ylabel, 'FontSize', 10);
  xlabel('Frequency [Hz]')
  % save plots.
  print('-dpng',[outputDirectory '/' matrixPlotTitle]);
%  print('-depsc2',[outputDirectory '/' matrixPlotTitle]);
else
  fprintf('Coherence matrix not produced because number of channels with coherence data <=1\n You can find the cohernece data for the one channel in the coherence spectrum on that channel''s summary page');
end
end % function

function coherence = isolateSubsystem(coherence,subsystem)
% ISOLATESUBSYSTEM remakes the coherence struct to isolate
% a specific subsystem by isolating channels that have
% the string "subsystem" appear somewhere in their names.
%
%	COHERENCE = ISOLATESUBSYSTEM(COHERENCE,SUBSYSTEM)
%
% coherence ---- struct containing needed information
% subsystem ---- string used to isolate channels that contain
% -------------- info on subsystem to isolate

chnlnamesSub = coherence.chnlnames(find(~cellfun('isempty',regexp(coherence.chnlnames,subsystem))));

cohMatrixSub = coherence.coherence_matrix(:,find(~cellfun('isempty',regexp(coherence.chnlnames,subsystem))));

coherence.chnlnames = chnlnamesSub;

coherence.coherence_matrix = cohMatrixSub;

end % function

