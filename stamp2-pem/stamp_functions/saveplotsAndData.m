function saveplotsAndData(params, map)
% function saveplotsAndData(params, map)

% determine x-values and y-values
map.xvals = map.segstarttime+params.segmentDuration/4 - map.segstarttime(1);
map.yvals = map.f;

% Output directory
if params.runCondor
  [junk, user] = system('echo $USER');
  outDir = ['/usr1/' user(1:end-1) '/' strrep(params.channelName2,':','-')];
else
  outDir = [params.outputFilePrefix '/' num2str(params.startGPS) '_' num2str(params.endGPS) '/' strrep(params.channelName1,':','-') '/' strrep(params.channelName2,':','-')];
end

if exist(outDir)~=7 
  mkdir ([outDir]);
end

if params.saveMat
  save([outDir '/map'],'params','map');
end

xlabel_text = 't (s)';
ylabel_text = 'f (Hz)';
if ~any(find(map.P2(~isnan(map.P2))))
  fprintf('map.P2 is empty! Skipping plotting and saving of data for this channel!\n');
  return;
end
if params.doPlots
  % SNR map of cross-power spectrum normalized by auto power spectrum
  printmap(abs(map.snr), map.xvals, map.yvals, xlabel_text, ylabel_text, 'SNR', [-10 10]);
  print('-dpng',[outDir '/snr'],1);
  %print('-depsc2',[outDir '/snr']);
  % y map; cross power map
  cut = map.f == round(params.fReference);
  printmap(map.y, map.xvals, map.yvals, xlabel_text, ylabel_text, ...
	   'y (strain^2)',[min(map.y(cut,:))*2 max(map.y(cut,:))*2]);
  print('-dpng',[outDir '/y_map']);
  %print('-depsc2',[outDir '/y_map']);
  if params.doClusterSearch
    % Xi map ; statstic used for removing glitchy time segments
    printmap(map.Xi_snr, map.xvals, map.yvals, xlabel_text, ylabel_text, ...
	     'SNR_{\Xi}', [-1 1]);
    print('-dpng',[outDir '/Xi_snr_map']);
    %print('-depsc2',[outDir '/Xi_snr_map']);
  end
  
  % sigma map; product of auto powers with a scaling factor
  printmap(map.sigma, map.xvals, map.yvals, xlabel_text, ylabel_text, ...
	   '-log_{10}(\sigma)', [min(map.sigma(cut,:))*2 max(map.sigma(cut,:))*2]);
  print('-dpng',[outDir '/sig_map']);
  %print('-depsc2',[outDir '/sig_map']);
  
  % P1 map; auto power of first channel (reference channel)
  map.P1_log10 = log10(map.P1);
  P1_min = mean(map.P1_log10(~isnan(map.P1_log10))) - 2*std(map.P1_log10(~isnan(map.P1_log10)));
  P1_max = mean(map.P1_log10(~isnan(map.P1_log10))) + 2*std(map.P1_log10(~isnan(map.P1_log10)));
  %map.P1_log10(isnan(map.P1_log10)) = 0;
  printmap(map.P1_log10, map.xvals, map.yvals, xlabel_text, ylabel_text, ... 
	   strrep(params.channelName1,'_','\_'), [P1_min P1_max]);
  print('-dpng',[outDir '/P1']);
  %print('-depsc2',[outDir '/P1']);
  
  % P2 map; auto power of second channel
  map.P2_log10 = log10(map.P2);
  P2_min = mean(map.P2_log10(~isnan(map.P2_log10))) - 2*std(map.P2_log10(~isnan(map.P2_log10)));
  P2_max = mean(map.P2_log10(~isnan(map.P2_log10))) + 2*std(map.P2_log10(~isnan(map.P2_log10)));
  %map.P2_log10(isnan(map.P2_log10)) = 0;
  printmap(map.P2_log10, map.xvals, map.yvals, xlabel_text, ylabel_text, ... 
	   strrep(params.channelName2,'_','\_'), [P2_min P2_max]);
  print('-dpng',[outDir '/P2']);
  %print('-depsc2',[outDir '/P2']);
  
  range_binning = logspace(min(log10(map.P1(~isnan(map.P1)))),max(log10(map.P1(~isnan(map.P1)))),50);
  which_spectra = 1:length(map.P1(1,:));
  map.P1_spectral_variation = hist(map.P1(:,which_spectra)',range_binning)';
  np = sum(map.P1_spectral_variation(1,:),2);
  map.P1_spectral_variation_norm = map.P1_spectral_variation * 100 / np;
  
  for i = 1:length(map.f)
    map.P1_spectral_variation_norm_1per(i) = ...
	stamp_pem_calculate_percentiles(map.P1_spectral_variation_norm(i,:),range_binning,1);
    
    map.P1_spectral_variation_norm_10per(i) =...
	stamp_pem_calculate_percentiles(map.P1_spectral_variation_norm(i,:),range_binning,10);
    
    map.P1_spectral_variation_norm_50per(i) =...
	stamp_pem_calculate_percentiles(map.P1_spectral_variation_norm(i,:),range_binning,50);
    
    map.P1_spectral_variation_norm_90per(i) = ...
	stamp_pem_calculate_percentiles(map.P1_spectral_variation_norm(i,:),range_binning,90);
    
    map.P1_spectral_variation_norm_99per(i) = ...
	stamp_pem_calculate_percentiles(map.P1_spectral_variation_norm(i,:),range_binning,99);
  end
  [X,Y] = meshgrid(map.f,range_binning);
  figure;
  colormap(jet);
  pcolor(X,Y,map.P1_spectral_variation_norm');
  caxis([0 1])
  set(gcf,'Renderer','zbuffer');
  shading interp
  set(gca,'xscale','log','XLim',[map.f(1) map.f(end)])
  set(gca,'yscale','log','YLim',[range_binning(1) range_binning(end)])
  set(gca,'clim',[0 25])
  xlabel('Frequency [Hz]')
  ylabel('Spectrum')
  grid
  set(gca,'Layer','top')
  hold on
  %loglog(map.f,map.P1_spectral_variation_norm_10per,'w',map.f,map.P1_spectral_variation_norm_90per,'w',map.f,map.P1_spectral_variation_norm_50per,'w','LineWidth',2)
  loglog(map.f,map.P1_spectral_variation_norm_50per,'w','LineWidth',1)
  hold off
  print('-dpng',[outDir '/P1_specvar.png']);
  close;
  range_binning = logspace(min(log10(map.P2(~isnan(map.P2)))),max(log10(map.P2(~isnan(map.P2)))),50);
  which_spectra = 1:length(map.P2(1,:));
  map.P2_spectral_variation = hist(map.P2(:,which_spectra)',range_binning)';
  np = sum(map.P2_spectral_variation(1,:),2);
  map.P2_spectral_variation_norm = map.P2_spectral_variation * 100 / np;
  for i = 1:length(map.f)
    map.P2_spectral_variation_norm_1per(i) =...
	stamp_pem_calculate_percentiles(map.P2_spectral_variation_norm(i,:),range_binning,1);
    
    map.P2_spectral_variation_norm_10per(i) = ...
	stamp_pem_calculate_percentiles(map.P2_spectral_variation_norm(i,:),range_binning,10);
    
    map.P2_spectral_variation_norm_50per(i) =...
	stamp_pem_calculate_percentiles(map.P2_spectral_variation_norm(i,:),range_binning,50);
    
    map.P2_spectral_variation_norm_90per(i) =...
	stamp_pem_calculate_percentiles(map.P2_spectral_variation_norm(i,:),range_binning,90);
    
    map.P2_spectral_variation_norm_99per(i) = ...
	stamp_pem_calculate_percentiles(map.P2_spectral_variation_norm(i,:),range_binning,99);
  end
  %[data.fl, data.low, data.fh, data.high] = NLNM(2);
  [X,Y] = meshgrid(map.f,range_binning);
  figure;
  colormap(jet);
  pcolor(X,Y,map.P2_spectral_variation_norm');
  caxis([0 1])
  set(gcf,'Renderer','zbuffer');
  shading interp
  set(gca,'xscale','log','XLim',[map.f(1) map.f(end)])
  set(gca,'yscale','log','YLim',[range_binning(1) range_binning(end)])
  set(gca,'clim',[0 25])
  xlabel('Frequency [Hz]')
  ylabel('Spectrum')
  grid
  set(gca,'Layer','top')
  hold on
  loglog(map.f,map.P2_spectral_variation_norm_50per,'w','LineWidth',1)
  %hold on
  %if regexp(params.channelName2,'SEIS')
  %   loglog(data.fl,data.low,'k-.',data.fh,data.high,'k-.','LineWidth',3)
  %end
  hold off
  print('-dpng',[outDir '/P2_specvar.png']);
  close;
end
% Calculate coherence
indexes = intersect(find(~isnan(max(map.fft1))),find(~isnan(max(map.fft2))));
a1 = map.fft1(:,indexes);
psd1 = mean(abs(a1).^2,2);
a2 = map.fft2(:,indexes);
psd2 = mean(abs(a2).^2,2);
csd12 = mean(a1.*conj(a2),2);
map.coherence = abs(csd12)./sqrt(psd1.*psd2);
if strcmp(params.channelName1,params.channelName2)
  map.theorCoherence = ones(size(map.coherence));
else
  map.theorCoherence = ones(size(map.coherence)) * sqrt(1/length(a1(1,:)));
end
loud_coh = findLoudCoh(map,params);
labels = num2str(map.f(loud_coh));
figure
loglog(map.f,map.coherence,'b','Linewidth',2)
hold on
loglog(map.f,map.theorCoherence,'r--','Linewidth',2)
hold off
xlabel('f (Hz)')
ylabel('Coherence')
xlim([min(map.f),max(map.f)])
ylim([5e-3 2])
legend({'true','theoretical'});
text(map.f(loud_coh), map.coherence(loud_coh), labels, 'horizontal','center','vertical','bottom');
grid on
pretty;
print('-dpng',[outDir '/coherence']);
%print('-depsc2',[outDir '/coherence']);

% save coherence data to make master coherence plot
ff = map.f;
coherence = map.coherence;
theorCoherence = map.theorCoherence;
save([outDir '/coherence.mat'],'ff','coherence','theorCoherence','csd12','psd1','psd2');

if params.doBicoherence
  fprintf('doing bicoherence. WARNING: This can take a LONG time (and a lot of memory) for large frequency arrays!');
  map.bicoherence = NaN * ones(length(map.f),length(map.f));
  map.bicoherence_pvalue = NaN * ones(length(map.f),length(map.f));
  map.bicoherence_threshold = NaN * ones(length(map.f),length(map.f));

  for ii = 1:length(map.f) 
    for jj = 1:length(map.f)
      a1 = map.fft1(ii,indexes);
      psd1 = mean(abs(a1).^2,2);
      a2 = map.fft2(jj,indexes);
      psd2 = mean(abs(a2).^2,2);
      csd12 = mean(a1.*conj(a2),2);
      %map.bicoherence(ii,ij) = abs(csd12)./sqrt(psd1.*psd2);
      map.bicoherence(ii,jj) = abs(xcorr(a1-mean(a1),a2-mean(a2),0,'coeff'));
      rho2 = map.bicoherence(ii,jj).^2;
      prho2 = (1-rho2).^(length(a1)-1);
      %map.bicoherence_pvalue(ii,jj) = 1-prho2;
      map.bicoherence_pvalue(ii,jj) = rho2 * length(a1);
      [junk,a1_indexes] = sort(a1,'descend');
      [junk,a2_indexes] = sort(a2,'descend');
      N = floor(length(a1) * 1);
      a1_indexes = a1_indexes(1:N);
      a1 = a1(a1_indexes);
      psd1 = mean(abs(a1).^2,2);
      a2 = a2(a1_indexes);
      psd2 = mean(abs(a2).^2,2);
      csd12 = mean(a1.*conj(a2),2);
      %map.bicoherence(ii,jj) = abs(csd12)./sqrt(psd1.*psd2);
      map.bicoherence_threshold(ii,jj) = ...
               abs(xcorr(a1-mean(a1),a2-mean(a2),0,'coeff'));
    end
  end

  rho2 = logspace(-7,0,500);
  prho2 = (1-rho2).^(length(a1)-1);
  threshold = 0.001;
  [junk,threshold_index] = min(abs(prho2-threshold));
  prho2_threshold = prho2(threshold_index);
  ticks = logspace(log10(min(map.f)),log10(max(map.f)),15);
  ticks = 10.^ticks;
  ticks = unique(floor(ticks));
  %ticks =min(map.f):2:max(map.f);

  [X,Y] = meshgrid(map.f,map.f);
  figure;
  hC = pcolor(X,Y,map.bicoherence');
  set(gcf,'Renderer','zbuffer');
  colormap(jet)
  shading interp
  t = colorbar('peer',gca);
  set(get(t,'ylabel'),'String','Coherence');
  set(t,'fontsize',25);
  set(hC,'LineStyle','none');
  grid
  set(gca,'clim',[0 0.1])
  %set(gca,'clim',[threshold 1])
  %set(gca,'clim',[min(min(map.bicoherence)) max(max(map.bicoherence))])
  xlabel(sprintf('Frequency [Hz] [%s]',strrep(params.channelName1,'_','\_')))
  ylabel(sprintf('Frequency [Hz] [%s]',strrep(params.channelName2,'_','\_')));
  set(gca,'xscale','log');
  set(gca,'yscale','log');
  set(gca,'XTick',ticks)
  set(gca,'YTick',ticks)
  pretty;
  print('-dpng',[outDir '/bicoherence']);
  %print('-depsc2',[outDir '/bicoherence']);

  figure;
  hC = pcolor(X,Y,map.bicoherence_pvalue');
  set(gcf,'Renderer','zbuffer');
  colormap(jet)
  shading interp
  t = colorbar('peer',gca);
  set(get(t,'ylabel'),'String','1 - cdf(coherence^2)');
  set(t,'fontsize',25);
  set(hC,'LineStyle','none');
  grid
  set(gca,'clim',[0 1])
  %set(gca,'clim',[0.999 1])
  if ~isnan(min(min(map.bicoherence_pvalue))) && ~isnan(max(max(map.bicoherence_pvalue)))
    set(gca,'clim',[min(min(map.bicoherence_pvalue)) max(max(map.bicoherence_pvalue))])
  end
  xlabel(sprintf('Frequency [Hz] [%s]',strrep(params.channelName1,'_','\_')))
  ylabel(sprintf('Frequency [Hz] [%s]',strrep(params.channelName2,'_','\_')));
  set(gca,'xscale','log');
  set(gca,'yscale','log');
  set(gca,'XTick',ticks)
  set(gca,'YTick',ticks)
  pretty;
  print('-dpng',[outDir '/bicoherence_pvalue']);
  %print('-depsc2',[outDir '/bicoherence_pvalue']);

  figure;
  hC = pcolor(X,Y,map.bicoherence_threshold');
  set(gcf,'Renderer','zbuffer');
  colormap(jet)
  shading interp
  t = colorbar('peer',gca);
  set(get(t,'ylabel'),'String','Coherence');
  set(t,'fontsize',25);
  set(hC,'LineStyle','none');
  grid
  set(gca,'clim',[0 0.1])
  %set(gca,'clim',[0.999 1])
  %set(gca,'clim',[min(min(map.bicoherence_pvalue)) max(max(map.bicoherence_pvalue))])
  xlabel(sprintf('Frequency [Hz] [%s]',strrep(params.channelName1,'_','\_')))
  ylabel(sprintf('Frequency [Hz] [%s]',strrep(params.channelName2,'_','\_')));
  set(gca,'xscale','log');
  set(gca,'yscale','log');
  set(gca,'XTick',ticks)
  set(gca,'YTick',ticks)
  pretty;
  print('-dpng',[outDir '/bicoherence_threshold']);
  %print('-depsc2',[outDir '/bicoherence_threshold']);
  close;
end

if params.doFrequencyBins 
  stamp_pem_frequencybins(params,map);
end

if params.runCondor
  [nouse1, nouse2] = system(['mv ' outDir ' ' params.outputFilePrefix '/' num2str(params.startGPS) '_' num2str(params.endGPS) '/' strrep(params.channelName1,':','-') '/']);
end

return;
