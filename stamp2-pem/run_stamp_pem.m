function run_stamp_pem(startGPS, endGPS, pemParamsFile, channlesListFile, ... 
                                                        doPostProc) 
% function run_stamp_pem(startGPS, endGPS, pemParamsFile, channlesListFile, ... 
%                                                         doPostProc)
% This code either submits jobs for stamp_pem or runs post processing on jobs 
% that are already done and have this input info. It does so in a way that 
% submits jobs so they do not require too much memory (breaks it into hours).
% Post processing will then loop over those same hours and combine the data 
% and create a nice looking coherence matrix

% Long description

dataSegmentSize = 3600;

if doPostProc
  % subroutine. see below.
  run_stamp_pem_pproc(startGPS, endGPS, pemParamsFile, channlesListFile, ...
                                                          dataSegmentSize)
else 
  % subroutine. see below.
  run_stamp_pem_submit(startGPS, endGPS, pemParamsFile, channlesListFile, ...
                                                        dataSegmentSize)
end

end % function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function run_stamp_pem_submit(startGPS, endGPS, pemParamsFile, ...
                                            channlesListFile, dataSegmentSize)
% function run_stamp_pem_submit(startGPS, endGPS, pemParamsFile, ...
%                                            channlesListFile, dataSegmentSize)
% Wrapper script for running stamp_pem.m or post processing!
% Runs stamp_pem.m for given start, end gps times, channel list and paramsfile.
% Long description

% Break up data into relatively long chunks.
GPSTimes = [startGPS:dataSegmentSize:endGPS];
if isempty(GPSTimes) % if less than an hour of data
  GPSTimes = [startGPS endGPS];
end

% add endGPS Time to GPSTimes if GPSTimes(end)-endGPS is > 240 s
if GPSTimes(end) ~= endGPS
  if endGPS-GPSTimes(end) > 240
    GPSTimes = [GPSTimes endGPS];
  end
end
	
for ii=1:length(GPSTimes)-1
  stamp_pem(pemParamsFile, channlesListFile, GPSTimes(ii), GPSTimes(ii+1));
end

end % submit jobs function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function run_stamp_pem_pproc(startGPS, endGPS, pemParamsFile, ...
                                            channlesListFile, dataSegmentSize)
% function run_stamp_pem_pproc(startGPS, endGPS, pemParamsFile, ...
%                                            channlesListFile, dataSegmentSize)
% Runs post processing on results produced by running stamp_pem.m
%
% Long description

% Break up data into relatively long chunks
GPSTimes = [startGPS:dataSegmentSize:endGPS];

% add endGPS Time to GPSTimes if GPSTimes(end)-endGPS is > 240 s
if GPSTimes(end) ~= endGPS
  if endGPS-GPSTimes(end) > 240
    GPSTimes = [GPSTimes endGPS];
  end
end

for ii=1:length(GPSTimes)-1
  stamp_pem_summary_html(pemParamsFile, channlesListFile, GPSTimes(ii), ...
                                                     GPSTimes(ii+1));
end

% read in params and channels names
params = readParamsFromFilePEM(pemParamsFile);
[channelNames sampleRates] = textread(channlesListFile,'%s %d');
darmChannel = strrep(channelNames{1},':','-');

% Combine coherence matrices; start times corresponding to first vector, 
% end times to second vector.
lockfiles = [];
for ii=1:length(GPSTimes)-1
  lockfiles{ii} = [params.outputFilePrefix '/' num2str(GPSTimes(ii)) '_' ...
                  num2str(GPSTimes(ii+1)) '/' darmChannel '/lock_' ...
                  num2str(GPSTimes(ii)) '_' num2str(GPSTimes(ii+1)) '.mat'];
end

% combines coherence data for all times. 
combineCoherenceData(GPSTimes(1), GPSTimes(end), lockfiles, ...
                                  channlesListFile, params.outputFilePrefix);

% create bokeh plots
bokeh_cmd = ['python bokeh/stampcoh_html.py -i ' params.outputFilePrefix ...
            '/combined_lock_' num2str(GPSTimes(1)) '_' ...
            num2str(GPSTimes(end)) '.mat -o ' params.outputFilePrefix ...
            '/STAMP_COH_combined_' num2str(GPSTimes(1)) '_' ...
            num2str(GPSTimes(end))];
[junk] = system(bokeh_cmd);

end % postproc function

