% This is a wrapper script that produces STAMP-PEM coherence results
%

% Input
pemParamsFile = 'input/stamp_pem_params.txt';
channlesListFile = 'input/channel_list_H1IMC.txt';
startGPS = 1082972416;
startGPS = 1105460000;
endGPS = startGPS+5*100;

stamp_pem_anteproc(pemParamsFile, channlesListFile, startGPS, endGPS);
