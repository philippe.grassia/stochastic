close all;
clear;

% Input files path. These large files are part of the sgwb repository
% Change to point to your local installation
inputFilesPath = '/home/charlton/Devel/sgwb/trunk/S4/input';

f=(00:0.25:2000)';
source=[ inputFilesPath '/' 'radiometer/sourceDirection.txt' ];
HS=[ inputFilesPath '/' 'radiometer/Hf.txt' ];
target=[0 45];
target=[ inputFilesPath '/' 'radiometer/sourceDirection.txt' ];

H =[ inputFilesPath '/' 'radiometer/Hf.txt' ];
Hg=[ inputFilesPath '/' 'radiometer/Hgaussian.txt' ];


calH1=[ inputFilesPath '/' 'calibration/S04-H1-CAL-DERRRESPONSE-793099715.mat' ];
calL1=[ inputFilesPath '/' 'calibration/S04-L1-CAL-DERRRESPONSE-792579613.mat' ];

s=load(source);



Hf=load(H);

H1=load(calH1);
L1=load(calL1);
H1.R0(1)=Inf;
L1.R0(1)=Inf;

flow=f(1);
deltaF=0.25;
fhigh=f(end);
numFreqs=length(f);

dur=2;
fsample=4096;
det1=getdetector('LHO');
det2=getdetector('LLO');
ra=[s(1,1)];
decl=[s(1,2)];
time=0;

[hp,hx]=randomTS(dur,fsample,Hf,true,2,[0,0]);

xf=constructFreqSeries(hp(1:numFreqs,1)+i*hx(1:numFreqs,1),flow,deltaF,1);
tmax=0.01;


[xt,N]=invFFT(xf,tmax,2^15);
t=0:xt.deltaT:(tmax-xt.deltaT);
t=[(flipud(fliplr(-t))-xt.deltaT),t];
fd=unique([-f;f]);
NN=length(fd);

clear xfd
fourierkernel=exp(i*2*pi*fd*t);
xfd(1:numFreqs,1)=flipud(fliplr(conj(xf.data)));
xfd(NN-numFreqs+1:NN,1)=(xf.data);
xtc=deltaF*real(transpose(fourierkernel)*xfd);
norm(xt.data-xtc)/sqrt(norm(xt.data)*norm(xtc))
