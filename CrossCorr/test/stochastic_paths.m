%
% This is an example of a startup.m file which you can run automatically 
% every time you launch matlab to set your matlab paths.  These commands will
% set up the paths needed for the stochastic pipeline.
% Just edit the subsequent lines and modify for your own home
% directory.
%

% Edit this to suit your local installation
% Assuming the current directory is test, we need to go up two directories
stoch_install = [ '../..' ];
stoch_util = [ stoch_install '/Utilities'  ];

% Add the required paths
fprintf('loading stochastic packages...');

% Main code for stochastic pipeline and post-processing
addpath([ stoch_install '/CrossCorr/src_cc' ]);
addpath([ stoch_install '/PostProcessing' ]);

% Utilities used by pipeline
addpath([ stoch_util ]);
addpath([ stoch_util '/Channel' ]);
addpath([ stoch_util '/misc' ]);
addpath([ stoch_util '/FTSeries' ]);
addpath([ stoch_util '/detgeom/matlab' ]);
addpath([ stoch_util '/ligotools/matlab' ]);

fprintf('done.\n');
