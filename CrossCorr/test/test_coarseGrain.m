%
% Function to test coarse-graining
%
function test_coarseGrain()

  global TOL;

  TOL = 3.0E-5;
  reps = 10;
  pass = true;

  if (exist('test_coarseGrain.out'))
    delete('test_coarseGrain.out');
  end;
  diary('test_coarseGrain.out');

  % The first set of tests checks that the values returned by the new function have
  % the expected properties.
  %
  % Need to test edge cases and generic cases
  %
  % Set 1: same df
  % - Identical length for Nx = 1, 2, and a couple of others
  % - Different length, equal lower edge
  % - Different length, equal upper edge
  % - Different length, equal interior edges
  % - Different length, unequal interior edges
  % Set 2: different df
  %
  % - Empty array (error)
  % - Generic cases

  %
  % Set 1: same df
  %

  % Nx = Ny = 1
  f0_x = 1.0;
  df_x = 1;
  Nx = 1;
  f0_y = 1.0;
  df_y = 1;
  Ny = 1;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = Ny = 2
  f0_x = 1.0;
  df_x = 1;
  Nx = 2;
  f0_y = 1.0;
  f0_y = 1;
  Ny = 2;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = Ny = 8
  f0_x = 1.0;
  df_x = 1;
  Nx = 8;
  f0_y = 1.0;
  df_y = 1;
  Ny = 8;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 2, Ny = 1, equal lower edge
  % This case exposes a bug in the original coarseGrain
  f0_x = 1.0;
  df_x = 1;
  Nx = 2;
  f0_y = 1.0;
  Ny = 1;
  df_y = 1;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 2, Ny = 1, equal upper edge
  % This case exposes a bug in the original coarseGrain
  f0_x = 1.0;
  df_x = 1;
  Nx = 2;
  f0_y = 2.0;
  df_y = 1;
  Ny = 1;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 8, Ny = 5, equal interior edges
  f0_x = 1.0;
  df_x = 1;
  Nx = 8;
  f0_y = 2.0;
  df_y = 1;
  Ny = 5;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 8, Ny = 5, different interior edges
  f0_x = 1.0;
  df_x = 1;
  Nx = 8;
  f0_y = 1.25;
  df_y = 1;
  Ny = 5;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 8193, same starting frequency
  % some starting values like f0_x = 0 don't allow the start of the coarse series
  % to be exactly the start of the fine series
  f0_x = 0.3; 
  df_x = 2048/(60*4096);
  Nx = 60*4096;
  df_y = 0.25;
  Ny = 6824;
  x_start = f0_x - 0.5*df_x;
  f0_y = x_start + 0.5*df_y;
  y_start = f0_y - 0.5*df_y;
  if (y_start ~= x_start)
    error('Start frequencies are different');
  end;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 8193, same ending frequency
  f0_x = 0.0;
  df_x = 2048/(60*4096);
  Nx = 60*4096;
  df_y = 0.25;
  Ny = 6824;
  x_end = f0_x + (Nx - 0.5)*df_x;
  f0_y = x_end - (Ny - 0.5)*df_y;
  y_end = f0_y + (Ny - 0.5)*df_y;
  % Check that ends are the same
  if (x_end ~= y_end)
    error('End frequencies are different');
  end;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 8193, similar to real params for deltaF = 0.25
  f0_x = 0.0;
  df_x = 2048/(60*4096);
  Nx = 60*4096;
  f0_y = 20.0;
  df_y = 0.25;
  Ny = 6824;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 8193, similar to real params for deltaF = 0.03125
  f0_x = 0.0;
  df_x = 2048/(60*4096);
  Nx = 60*4096;
  f0_y = 20;
  df_y = 0.03125;
  Ny = 54592;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Check bad parameters

  % Nx = Ny = 0
  f0_x = 1.0;
  df_x = 1;
  Nx = 0;
  f0_y = 1.0;
  df_y = 1;
  Ny = 0;
  try
    test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);
    pass = false;
    fprintf('FAIL catch Nx = 0\n');
  catch me
    pass = pass & true;
    fprintf('PASS caught exception ''%s''\n', me.message);
  end;

  % df_y < df_x
  f0_x = 1.0;
  df_x = 1;
  Nx = 8;
  f0_y = 1.0;
  df_y = 0.5;
  Ny = 8;
  try
    test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);
    pass = false;
    fprintf('FAIL catch df_y < df_x\n');
  catch me
    pass = pass & true;
    fprintf('PASS caught exception ''%s''\n', me.message);
  end;

  % f0_y < f0_x
  f0_x = 1.0;
  df_x = 1;
  Nx = 8;
  f0_y = 0.5;
  df_y = 1.0;
  Ny = 8;
  try
    test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);
    pass = false;
    fprintf('FAIL catch f0_y < f0_x\n');
  catch me
    pass = pass & true;
    fprintf('PASS caught exception ''%s''\n', me.message);
  end;

  % y_end > x_end
  f0_x = 1.0;
  df_x = 1;
  Nx = 8;
  f0_y = 1.5;
  df_y = 1.0;
  Ny = 8;
  try
    test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);
    pass = false;
    fprintf('FAIL catch y_end > x_end\n');
  catch me
    pass = pass & true;
    fprintf('PASS caught exception ''%s''\n', me.message);
  end;

  % The next set compares the new function with the old function.
  % The original function has some incorrect behaviour for edge cases
  % so those situations are not tested.

  % Nx = Ny = 8
  f0_x = 0.0;
  df_x = 1;
  Nx = 8;
  f0_y = 0.25;
  df_y = 1;
  Ny = 6;
  pass = pass & test_v3(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 8193, similar to real params for deltaF = 0.25
  f0_x = 0.0;
  df_x = 2048/(60*4096);
  Nx = 60*4096;
  f0_y = 20.0;
  df_y = 0.25;
  Ny = 6824;
  pass = pass & test_v3(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % Nx = 8193, similar to real params for deltaF = 0.03125
  f0_x = 0.0;
  df_x = 2048/(60*4096);
  Nx = 60*4096;
  f0_y = 20;
  df_y = 0.03125;
  Ny = 54592;
  pass = pass & test_v3(f0_x, df_x, Nx, f0_y, df_y, Ny);

  % The next set of tests compares execution time

  % Similar to real params for deltaF = 0.25
  f0_x = 0.0;
  df_x = 2048/(60*4096);
  Nx = 60*4096;
  f0_y = 20.0;
  df_y = 0.25;
  Ny = 6824;
  pass = pass & test_time(f0_x, df_x, Nx, f0_y, df_y, Ny, reps);

  % Similar to real params for deltaF = 0.03125
  f0_x = 0.0;
  df_x = 2048/(60*4096);
  Nx = 60*4096;
  f0_y = 20;
  df_y = 0.03125;
  Ny = 54592;
  pass = pass & test_time(f0_x, df_x, Nx, f0_y, df_y, Ny, reps);

  if (pass)
    fprintf('PASS: all\n');
  else
    fprintf('FAIL\n');
  end;

  diary off;

return;

function pass=test_general(f0_x, df_x, Nx, f0_y, df_y, Ny)
  
  pass = true;
  pass = pass & test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny);
  pass = pass & test_v3(f0_x, df_x, Nx, f0_y, df_y, Ny);

return;

% Test the new coarse-graining
function pass=test_coarse(f0_x, df_x, Nx, f0_y, df_y, Ny)

  global TOL;

  fprintf('** Testing f0_x = %.2f, df_x = %.2f, Nx = %d, f0_y = %.2f, df_y = %.2f, Ny = %d\n', f0_x, df_x, Nx, f0_y, df_y, Ny);

  pass = true;

  x = generate_data(f0_x, df_x, Nx);

  [y, index1, index2, frac1, frac2] = coarseGrain(x, f0_y, df_y, Ny);
  
  % Check the characteristics of the coarse-grained series

  if (length(y.data) == Ny)
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' coarse length, expected %d, received %d\n', Ny, length(y.data));

  % Note that the index1 and index2 are indices from 0 not from 1 as would normally
  % be the case for Matlab arrays, so we do  not subtract 1
  xLowerEdge0 = x.flow + (index1 - 0.5)*x.deltaF;
  xLowerEdge1 = x.flow + (index1 + 0.5)*x.deltaF;

  xUpperEdge0 = x.flow + (index2 - 0.5)*x.deltaF;
  xUpperEdge1 = x.flow + (index2 + 0.5)*x.deltaF;

  yLowerEdge = y.flow - 0.5*y.deltaF;
  yUpperEdge = y.flow + (Ny - 0.5)*y.deltaF;

  % Check ends of y correctly contained in range of x
  if ((xLowerEdge0 <= yLowerEdge) & (yLowerEdge < xLowerEdge1))
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' lower edge %f <= %f < %f\n', xLowerEdge0, yLowerEdge, xLowerEdge1);

  if ((xUpperEdge0 < yUpperEdge) & (yUpperEdge <= xUpperEdge1))
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' upper edge %f < %f <= %f\n', xUpperEdge0, yUpperEdge, xUpperEdge1);

  % check fractional contribution
  frac1exp = (xLowerEdge1 - yLowerEdge)/x.deltaF;
  if (frac1 == frac1exp)
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' lower fraction, received %f, expected %f\n', frac1, frac1exp);

  frac2exp = (yUpperEdge - xUpperEdge0)/x.deltaF;
  if (frac2 == frac2exp)
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' upper fraction, received %f, expected %f\n', frac2, frac2exp);

return;

% Compare with old version v3
function pass=test_v3(f0_x, df_x, Nx, f0_y, df_y, Ny)

  global TOL;

  fprintf('** Testing f0_x = %.2f, df_x = %.2f, Nx = %d, f0_y = %.2f, df_y = %.2f, Ny = %d\n', f0_x, df_x, Nx, f0_y, df_y, Ny);

  pass = true;

  x = generate_data(f0_x, df_x, Nx);

  [y, index1, index2, frac1, frac2] = coarseGrain(x, f0_y, df_y, Ny);
  [y1, i1, i2, f1, f2] = coarseGrain_v3(x, f0_y, df_y, Ny);

  if (length(y.data) == length(y1.data))
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' coarse length, expected %d, received %d\n', length(y.data), length(y1.data));

  diff = max(abs(y.data - y1.data));
  if (diff < TOL)
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' with max difference %.4e\n', diff);

  if (index1 == i1)
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' with index1 = %d and old index1 = %d\n', index1, i1);

  if (index2 == i2)
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' with index2 = %d and old index2 = %d\n', index2, i2);

  if (frac1 == f1)
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' with frac1 = %.2f and old frac1 = %.2f\n', frac1, f1);

  if (frac2 == f2)
    fprintf('PASS');
  else
    fprintf('FAIL');
    pass = false;
  end;
  fprintf(' with frac2 = %.2f and old frac2 = %.2f\n', frac2, f2);

return;

function pass=test_time(f0_x, df_x, Nx, f0_y, df_y, Ny, reps)

  global TOL;

  fprintf('** Testing time for f0_x = %.2f, df_x = %.2f, Nx = %d, f0_y = %.2f, df_y = %.2f, Ny = %d\n', f0_x, df_x, Nx, f0_y, df_y, Ny);

  pass = true;

  x = generate_data(f0_x, df_x, Nx);

  tic;
  for k = 1:reps
    [y, index1, index2, frac1, frac2] = coarseGrain_v3(x, f0_y, df_y, Ny);
  end;
  t = toc;
  fprintf('V3s time = %f secs/use\n', t/reps);

  tic;
  for k = 1:reps
    [y, index1, index2, frac1, frac2] = coarseGrain(x, f0_y, df_y, Ny);
  end;
  t = toc;
  fprintf('New time = %f secs/use\n', t/reps);

return;

function x=generate_data(flow, df, N)

  f = flow + df*[0:N-1].';
  symmetric = false;
  x = constructFreqSeries(f.^2+1, flow, df, symmetric);

return;

% Original version of coarseGrain up to stochastic v3
function [y, index1, index2, frac1, frac2]=coarseGrain_v3(x, flowy, deltaFy, Ny)
%
%  coarseGrain --- coarse grain a frequency-series 
%
%  coarseGrain(x, flowy, deltaFy, Ny) returns a frequency-series structure 
%  coarse-grained to the frequency values f = flowy + deltaFy*[0:Ny-1].  
%
%  coarseGrain also returns the indices of the lowest and highest frequency 
%  bins of x that overlap with y (0 <= index1 <= length(x); 
%  1 <= index2 <= length(x)+1) and the fractional contributions from these 
%  frequency bins.
%
%  Routine written by Joseph D. Romano.
%  Contact Joseph.Romano@astro.cf.ac.uk
%
%  $Id: coarseGrain.m,v 1.4 2005-02-24 14:36:27 jromano Exp $
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% extract metadata
Nx = length(x.data);
flowx = x.flow;
deltaFx = x.deltaF;
% error checking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check that frequency spacings and number of elements are > 0
if ( (deltaFx <= 0) | (deltaFy <= 0) | (Ny <= 0) )
  error('bad input arguments');
end

% check that freq resolution of x is finer than desired freq resolution
if ( deltaFy < deltaFx )
  error('deltaF coarse-grain < deltaF fine-grain');
end

% check desired start frequency for coarse-grained series
if ( (flowy - 0.5*deltaFy) < (flowx - 0.5*deltaFx) )
  error('desired coarse-grained start frequency is too low');
end

% check desired stop frequency for coarse-grained series
fhighx = flowx + (Nx-1)*deltaFx;
fhighy = flowy + (Ny-1)*deltaFy;

if ( (fhighy + 0.5*deltaFy) > (fhighx + 0.5*deltaFx) )
  error('desired coarse-grained stop frequency is too high');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% indices for coarse-grained series
i = transpose([0:1:Ny-1]);

% calculate the low and high indices of fine-grained series 
jlow  = 1 + floor1( (flowy + (i-0.5)*deltaFy - flowx - 0.5*deltaFx)/deltaFx ); 
jhigh = 1 + floor1( (flowy + (i+0.5)*deltaFy - flowx - 0.5*deltaFx)/deltaFx ); 

index1 = jlow(1);
index2 = jhigh(end);

% calculate fractional contributions
fraclow  = (flowx + (jlow+0.5)*deltaFx - flowy - (i-0.5)*deltaFy)/deltaFx;
frachigh = (flowy + (i+0.5)*deltaFy - flowx - (jhigh-0.5)*deltaFx)/deltaFx;
  
frac1 = fraclow(1);
frac2 = frachigh(end);

% calculate coarse-grained values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sum of middle terms
% NOTE:  since this operation takes more time than anything else, i made 
% it into separate routine called sumTerms() which is then converted into
% a MEX file
jtemp = jlow+2;
midsum = sumTerms(x.data, jtemp, jhigh);

% calculate all but final value of y
ya = (deltaFx/deltaFy)*(x.data(jlow (1:Ny-1)+1).*fraclow (1:Ny-1) + ...
                        x.data(jhigh(1:Ny-1)+1).*frachigh(1:Ny-1) + ...
                        midsum(1:Ny-1) );

% calculate final value of y
if (jhigh(Ny) > Nx-1)
  % special case when jhigh exceeds maximum allowed value
  yb = (deltaFx/deltaFy)*(x.data(jlow(Ny)+1)*fraclow(Ny) + midsum(Ny));

else
  yb = (deltaFx/deltaFy)*(x.data(jlow (Ny)+1)*fraclow (Ny) + ...
                          x.data(jhigh(Ny)+1)*frachigh(Ny) + ...
                          midsum(Ny) );
end

% fill structure for coarsed-grained frequency series
y.data = [ya; yb];
y.flow = flowy;
y.deltaF = deltaFy;
try
  y.symmetry = x.symmetry;

 
 catch 
 

  y.symmetry = 0;
end

return

function indexC = floor1(index)
% function floor1(index)
% modified floor function to account for precision error 
  if abs(index-round(index)) < 1e-8
    indexC = round(index);
  else
    indexC = floor(index);
  end
return
