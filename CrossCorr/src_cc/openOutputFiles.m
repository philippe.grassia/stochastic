function params=openOutputFiles(params)

% Opens output files for writing
% 
% input and output: params struct
%
% Routine copied from stochastic.m and modified my Stefan Ballmer 
% sballmer@caltech.edu 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if (params.writeOutputToMatFile)
    params = openOutputMatFile(params);
  else
    params = openOutputTextFiles(params);
  end;

end
