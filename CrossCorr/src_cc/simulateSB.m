function [o1, o2] = simulateSB(tlow, deltaT1, deltaT2, N1, N2, ...
                               signalType, powerIndex, fRef, detector1, detector2, ...
			       transfer1, transfer2, nResample1, betaParam1, ...
                               nResample2, betaParam2, fbase1, fbase2, seed)
%
% simulateSB --- simulates a stochastic background signal
%
% simulateSB(tlow, deltaT1, deltaT2, N1, N2, signalType, powerIndex, fRef,
%   detector1, detector2, transfer1, transfer2,
%   nResample1, betaParam1, nResample2, betaParam2,
%   fbase1, fbase2, seed)
%
% Generates N1 and N2 samples of strain data for detectors1 and 2 respectively of
% two simulated unpolarized, isotropic stochastic background signals. The strain
% data is then convolved with the instrument transfer functions to produce data with
% units the same as the data being analyzed. In the typical case that calibrated
% strain is being analyzed, the transfer functions will be just be identity functions
% so o1 and o2 will have units of strain.
%
%  signalType:
%  - 'white' produces a stochastic background signal with flat spectrum
%       S_gw(f) = (3*H_100^2)/(10*pi^2)
%
%  - 'const' produces a power law stochastic background signal given by
%       S_gw(f) = (3*H_100^2)/(10*pi^2) * (f/fRef)^(alpha)/f^3
%
%  powerIndex - the exponent (alpha) used for the dependency of the
%    stochastic background on frequency. Used for const only, as the
%    white spectrum is independent of frequency
%
%  fRef - the reference frequency in Hz.
%
% The white case corresponds to h_100^2 omegaGW(f)/f^3 = 1/Hz^3
% or h_100^2 omegaGW(fRef) = (fRef/Hz)^3.
% 
% This routine works for any GW detector pair, namely, ifo-ifo, ifo-bar,
% bar-ifo, or bar-bar.
%
% It still requires testing for the case of nonzero tlow.
%
% transfer1,2 = instrument transfer functions (units: counts/strain)
%
% Routine written by Joseph D. Romano, Sukanta Bose, John T. Whelan
%  Contact Joseph.Romano@astro.cf.ac.uk%
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set seed, if passed as an argument
try 
  seed;
  randn('state',seed);
catch
  % do nothing
end

% discrete positive frequencies (not including DC component and Nyquist)
if (N1 >= N2)
  N = N1;
  deltaT = deltaT1;
else
  N = N2;
  deltaT = deltaT2;
end

if (mod(N,2) == 0)
  numFreqs = N/2 - 1;
else
  numFreqs = (N-1)/2;
end

deltaF = 1/(N*deltaT);
f = deltaF*[1:1:numFreqs].';
H100 = HubbleConstant;

switch signalType
 case 'white'
  % S_gw(f) = (3 H_100^2/10 pi^2) 
  norm = sqrt(N/(2*deltaT)) * sqrt((3*H100^2)/(10*pi^2));
 case 'const'
  % h_100^2 omegaGW(f) = 1 
  % S_gw(f) = (3*H_100^2)/(10*pi^2) * (f/fRef)^(alpha)/f^3
  norm = sqrt(N/(2*deltaT)) * sqrt((3*H100^2)/(10*pi^2));
  % frequency dependence is f^((powerIndex-3)/2)/(fRef^(powerIndex/2))
  norm = norm*f.^((powerIndex-3)/2)/(fRef^(powerIndex/2));
 otherwise
  error(sprintf('Unknown signal type \''%s\''.', signalType));
end;

% construct real and imaginary parts, with random phases
re1 = (norm/sqrt(2)).* randn(numFreqs, 1);
im1 = (norm/sqrt(2)).* randn(numFreqs, 1);
z1  = re1 + 1i*im1;

re2 = (norm/sqrt(2)).* randn(numFreqs, 1);
im2 = (norm/sqrt(2)).* randn(numFreqs, 1);
z2  = re2 + 1i*im2;

% Fourier components at site 2 are correlated with those at site 1
% via the overlap reduction functions          
% Generalized the way the strains are generated to account for possible 
% ifo-bar analysis
gamma12 = constructFreqSeries(overlapreductionfunction(f, detector1, detector2), ...
                              f(1), deltaF);
gamma11 = constructFreqSeries(overlapreductionfunction(f, detector1, detector1), ...
                              f(1), deltaF);
gamma22 = constructFreqSeries(overlapreductionfunction(f, detector2, detector2), ...
                              f(1), deltaF);
sDenom = gamma11.data .* gamma22.data;
s = sqrt(1 - gamma12.data.^2./sDenom);

a = (1/sqrt(2)) * sqrt(1+s);
b = (1/sqrt(2)) * gamma12.data./(sqrt((1+s).*sDenom));

htilde1 = sqrt(gamma11.data).*(a.*z1 + b.*z2);
htilde2 = sqrt(gamma22.data).*(b.*z1 + a.*z2);

% interpolate detector1 transfer functions to appropriate frequencies
f_transfer1 = transfer1.flow + transfer1.deltaF*[0:length(transfer1.data)-1]';
f_transfer2 = transfer2.flow + transfer2.deltaF*[0:length(transfer2.data)-1]';
transfer1_i = interp1(f_transfer1, transfer1.data, f, 'spline');
transfer2_i = interp1(f_transfer2, transfer2.data, f, 'spline');

% Check that the reference frequency is in the domain of the transfer functions
if (fRef < f_transfer1(1) | fRef > f_transfer1(end))
  error('Reference frequency is outside the domain of transfer function for channel 1.');
end;
if (fRef < f_transfer2(1) | fRef > f_transfer2(end))
  error('Reference frequency is outside the domain of transfer function for channel 2.');
end;

% convolve data with instrument transfer function
otilde1 = htilde1.*transfer1_i;
otilde2 = htilde2.*transfer2_i;

% set DC and Nyquist = 0, then add negative freq parts in proper order
if (mod(N,2) == 0)
  % note that most negative frequency is -f_Nyquist when N=even 
  otilde1 = [ 0; otilde1; 0; flipud(conj(otilde1)) ];
  otilde2 = [ 0; otilde2; 0; flipud(conj(otilde2)) ];
else
  % no Nyquist frequency when N=odd
  otilde1 = [ 0; otilde1; flipud(conj(otilde1)) ];
  otilde2 = [ 0; otilde2; flipud(conj(otilde2)) ];
end;

% fourier transform back to time domain and take the real part
o1_data = real(ifft(otilde1));
o2_data = real(ifft(otilde2));

if (~isnan(fbase1))
  % heterodyne the waveform
  % t should really be tlow plus this, and phase should be included
  t = deltaT*[0:1:(N-1)]';
  o1_data = o1_data.*(cos(2*pi*fbase1.*t) - 1i*sin(2*pi*fbase1.*t));
end

if (N1 ~= N)
  o1_data = resample(o1_data, N1, N, nResample1, betaParam1);
end

% fill structures for detector1
o1 = constructTimeSeries(o1_data, tlow, deltaT1);

if (~isnan(fbase2))
  % heterodyne the waveform
  % t should really be tlow plus this, and phase should be included
  t = deltaT*[0:1:(N-1)]';
  o2_data = o2_data.*(cos(2*pi*fbase2.*t) - 1i*sin(2*pi*fbase2.*t));
end

if (N2 ~= N)
  o2_data = resample(o2_data, N2, N, nResample2, betaParam2);
end

% fill structures for detector2
o2 = constructTimeSeries(o2_data, tlow, deltaT2);

return;
