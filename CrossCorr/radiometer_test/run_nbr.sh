#! /bin/bash
# set up environment
. ./matlab_script_2015a.sh
# define arguments
parameterfile=nbr/example_params.txt
jobfile=example_jobs.txt
jobNumber=1
# run pipeline
../src_cc/stochastic $parameterfile $jobfile $jobNumber
