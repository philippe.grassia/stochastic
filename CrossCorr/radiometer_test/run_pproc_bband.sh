#!/bin/bash

# source seispy
source ~/opt/seispy/bin/activate

# define parameters
output_pref='bbr/example_'
job1=1
jobN=1
checkpoint_file='bbr/bbr_chkpoint.pkl'
segdur=60
deltaF=0.25
output_mat='bbr/bbr_out.mat'
search_type='bbr'
output_prefix='bbr/bbr_out_final'


# call pproc

python ../../PostProcessing/run_pproc.py --output-prefix $output_pref\
                       --job1 $job1\
                       --jobN $jobN\
                       --checkpoint-file $checkpoint_file\
                       --segment-duration $segdur\
                       --deltaF $deltaF\
                       --search-type $search_type\
                       --output-matfile $output_mat\
                       --load-checkpoint

