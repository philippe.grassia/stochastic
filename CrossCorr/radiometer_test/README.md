# Narrowband Radiometer Examples

* You should be able to run this just like the isotropic examples.
* In the example paramfile there is a list of radiometer specific parameters.
* For the narrowband a few of these need to be specified but just aren't used. 

## `stochastic.m` compilation
you can compile `stochastic.m` by going to `matapps/packages/stochastic/trunk/CrossCorr/src_cc` and running `make`.

## Example useage
```bash
# set up environment
. matlab_script_2015a.sh

# run stochastic
./stochastic example_params.txt example_jobs.txt 1
```

## External files
`test_coords.txt`  -- list of directions. tab separated columns: `raHr degDeg`
`Hf.txt` -- H(f) power spectrum model file. Not actually used for narrowband radiometer but needs to be set anyway.

## Post Processing

The post processing code is here: https://git.ligo.org/stochastic/post_processing

* Currently there is no installation process (forthcoming)
* For now the recommendation is to clone the repo and softlink `pproc.py`, `run_pproc.py` and `split_up_results_matfile.py` to this directory.
