function [] = bundle(filelist, jobNumber);
%
% jobNumber does not do anything
% filelist is a list of *SpHSet*.mat files

filenames=textread(filelist,'%s');

% The following is based on sumXFisher.
%   -look directly at SpHSet files
%   -try/tcatch syntax for corrupt files
%   -do not calculate invfisher, p, or chisquared 
%   -added mar9: calculations for overlapping frames
%   -added mar11: calculate nsegs
epsilon=3/70;                                                          %mar9
nsegs=0;                                                               %mar11
for kk=1:length(filenames)
  filename=filenames{kk};
  if exist(filename) ~= 0
    try % in case the file is corrupt
      index = load(filename);
      imax = length(index.Sky);
      nsegs = nsegs + imax;                                            %mar11
      if kk==1
        x=zeros(size(index.Sky{1}.X));
        fisher=zeros(size(index.Sky{1}.Fisher));
        coh=zeros(size(index.Sky{1}.coh));
        xP=zeros(size(index.Sky{1}.X));                                %mar9
        fisherP=zeros(size(index.Sky{1}.Fisher));                      %mar9
      end

      % Sum the projection X and the Fisher matrix for each iteration
      for ii=1:imax
        x = x + index.Sky{ii}.X;
        fisher = fisher + index.Sky{ii}.Fisher;
        coh = coh + index.Sky{ii}.coh;
	%mar9----------------------------------------------------------------
	if ((ii==1)&&(imax==1))
	    fisherP = fisherP + index.Sky{ii}.Fisher;
	    xP = xP + index.Sky{ii}.X;
        elseif (ii==1)
          fisherP = fisherP + ...
	    index.Sky{ii}.Fisher-epsilon*index.Sky{ii+1}.Fisher;
          xP = xP + index.Sky{ii}.X-epsilon*(index.Sky{ii+1}.X);
	elseif (ii==imax)
          fisherP = fisherP + ...
	    index.Sky{ii}.Fisher-epsilon*index.Sky{ii-1}.Fisher;
          xP = xP + index.Sky{ii}.X-epsilon*(index.Sky{ii-1}.X);
	else
          fisherP = fisherP + index.Sky{ii}.Fisher - ...
	    epsilon*(index.Sky{ii-1}.Fisher+index.Sky{ii+1}.Fisher);
          xP = xP + index.Sky{ii}.X - ...
	    epsilon*(index.Sky{ii-1}.X+index.Sky{ii+1}.X);
	end
      %----------------------------------------------------------------------
      end % for ii=1:imax
    catch
      fprintf('%s is corrupt.\n',filename);
    end % try-catch
  else 
    fprintf('File %s not found: skipping.\n', filename);
  end % if exist(filename) ~= 0
end % for kk=1:length(filenames)

% Create *.bunch* file in directory of *SpHSet* file
%mat=regexprep(filenames{1},'trial1','bunch');
% Create *.bunch2* file in directory of *SpHSet* file
mat=regexprep(filenames{1},'trial1','bunch2');
clear kk filename index imax ii;
save(mat);

return
