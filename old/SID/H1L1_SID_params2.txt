% parameters for stochastic search (name/value pairs)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
intermediate true
intFrameCachePath /archive/home/ethrane/SID/cache/
%SpH--------------------------------------------------------------------
simOmegaRef 0
doSphericalHarmonics true
SpHLmax 20
gammaLM_coeffsPath /archive/home/ethrane/matapps/src/searches/stochastic/CrossCorr
useSignalSpectrumHfFromFile true
HfFile /archive/home/ethrane/sgwb/S5/input/simfiles/Hf.txt
HfFileInterpolateLogarithmic true

% flags for optional operations
doFreqMask true
doHighPass1 true
doHighPass2 true
doOverlap true

doSidereal true

minDataLoadLength 200
 
doBadGPSTimes true
badGPSTimesFile /archive/home/shivaraj/sgwb/S5/output/S5H1L1_badGPSsegments.dat
maxDSigRatio 1.2
minDSigRatio 0.8

% ifo names
ifo1 H1
ifo2 L1

% segment duration (sec)
segmentDuration 52

% parameters for sliding psd estimation:
% numSegmentsPerInterval should be odd; ignoreMidSegment is a flag 
% that allows you to ignore (if true) or include (if false) the 
% analysis segment when estimating power spectra
numSegmentsPerInterval 3
ignoreMidSegment true

% freq resolution and freq cutoffs for CC statistic sum (Hz)
flow 40
fhigh 1800
deltaF 0.25

% params for Omega_gw (power-law exponent and reference freq in Hz)
alphaExp 0
fRef 100

% resample rate (Hz)
resampleRate1 4096
resampleRate2 4096

% buffer added to beginning and end of data segment to account for
% filter transients (sec)
bufferSecs1 2
bufferSecs2 2

% ASQ channel
ASQchannel1 LSC-STRAIN
ASQchannel2 LSC-STRAIN

% frame type and duration
frameType1 HL-SIDv1_H1L1_250mHz
frameType2 none
frameDuration1 -1
frameDuration2 -1

% duration of hann portion of tukey window 
% (hannDuration = segmentDuration is a pure hann window)
hannDuration1 52
hannDuration2 52

% params for matlab resample routine
nResample1 10
nResample2 10
betaParam1 5
betaParam2 5

% params for high-pass filtering (3db freq in Hz, and filter order) 
highPassFreq1 32
highPassFreq2 32
highPassOrder1 6
highPassOrder2 6

% coherent freqs and number of freq bins to remove if doFreqMask=true;
% NOTE: if an nBin=0, then no bins are removed even if doFreqMask=true
% (coherent freqs are typically harmonics of the power line freq 60Hz
% and the DAQ rate 16Hz)
freqsToRemove 1799
nBinsToRemove 1
 
% calibration filenames
alphaBetaFile1 none
alphaBetaFile2 none
calCavGainFile1 none
calCavGainFile2 none
calResponseFile1 none
calResponseFile2 none

% path to cache file
%gpsTimesPath1 /archive/home/ethrane/these-are-just-dummy-values
%gpsTimesPath2 /archive/home/ethrane/cet
%frameCachePath1 /archive/home/ethrane/cet
%frameCachePath2 /archive/home/ethrane/cet

% output directory prefix (ends with '/')
% not used for SID frames
% outputDirPrefix /archive/home/ethrane/SID/

%output filename prefix
%CET: Note that the prefix includes the path!
%outputFilePrefix /archive/home/ethrane/SID/out/HL-SIDv1_H1L1_250mHz
outputFilePrefix /usr1/ethrane/HL-SpHv1_H1L1_250mHz
