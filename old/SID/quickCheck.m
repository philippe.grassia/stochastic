function [] = quickCheck(lmax);
% Runs peelback algorithms with a limited lmax.
% Run again and it makes maps of saved parameters.
if (~exist('lmax'))
  lmax=5;
end

if (~exist('S5_bayes.mat'))
  load('S5_final.mat');
  if (lmax<sqrt(length(x))-1)
    x0=subX(x,lmax);
    fisher0=subFisher(fisher,lmax);
  else
    x0=x;  fisher0=fisher;
  end
  fprintf('SpH basis\n');
  [invfisherF,nkeptF]=peelBack(x0,fisher0,coh);   %SpH basis-------------
  pF=invfisherF*x0;
  fprintf('natural basis\n');
  [invfisherN,nkeptS]=peelBack(x0,fisher0,coh,2); %Natural basis---------
  pN=invfisherN*x0;
  save('S5_bayes.mat','pF','invfisherF','nkeptF','pN','invfisherN','nkeptN');
  return
else
  load('S5_bayes.mat');
  if (exist('inc')) %keep the figures fresh
    inc=inc+10;
  else
    inc=1;
  end
  %%%SpH-------------------------------------------------------
  if (any(sum(pF)))
    fprintf('Some SpH modes kept.\n');
    figure(inc)
    plotPlmMap(pF);
    title('clean map from peelback SpH basis');
    print('-depsc2','S5_peelSpH.eps');
  else
    fprintf('No SpH modes kept\n');
  end
  %%%Nat-------------------------------------------------------
  if (any(sum(pN)))
    fprintf('Some Nat modes kept.\n');
    figure(inc+1)
    plotPlmMap(pN);
    title('clean map from peelback natural basis');
    print('-depsc2','S5_peelNat.eps');
  else
    fprintf('No Nat modes kept\n');
  end
end
