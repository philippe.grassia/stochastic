function [invfisherF, nkept] = peelback(x,fisher,coh,basis);
% basis = 1 (SpH), 2 (natural)
% Eric Thrane - University of Minnesota

if (~exist('basis'))
  basis=1;
end

if (basis==2)
  [R,fisher]=eig(fisher);            %work in natural basis
  x=R*x;                             %linearly transform x-vectors
end

OmegaUL=1.20e-4;                     %Bayesian UL from S4
h0=0.72;                             %Hubbles constant
H0=100*(h0/3.08568E19);              %
fref=100;                            %reference frequency
DeltaP=OmegaUL*(3*H0^2)/(2*pi^2*fref^3*sqrt(4*pi)); %prior
sanity = 10^-48;                     %regulating factor not physical
N=length(x);                         %initial # of complex params
done = false;                        %Is algorithm finished? bool
NParms = 2*N;                        %total # of real parameters
lmax=sqrt(N)-1;
[lvec, mvec] = getLMvec(lmax);       %l and m as a function of index
jmax=(lmax+1)^2;
jvec=lvec.^2+lvec+mvec+1;            %each lm pair has a unique j
kept=diag(ones(jmax));                         %is parameter_j kept?

x0=x;
fisher0=fisher;
invfisher0=inv(fisher0);
p0=invfisher0*x0;
p=p0;
chisquared0 = coh - ctranspose(p0)*x0 - ctranspose(x0)*p0 + ...
  real(ctranspose(p0)*fisher0*p0);
lbayes = -0.5*chisquared0 + 0.5*log(det(invfisher0/sanity^2)) - ...
  NParms*log(DeltaP/sanity);
if(~isfinite(lbayes))
  fprintf('lbayes is not finite; adjust sanity factor\n');
  return;
end
initial = real(lbayes);

while ~done
  worst=-1E99;
  iiworst=0;
  NParms = 2*(N-1);
  for ii=1:N
    x0=x;
    x0(ii,:)=[];
    fisher0=fisher;
    fisher0(:,ii)=[];
    fisher0(ii,:)=[];
    invfisher0 = inv(fisher0);
    p0 = invfisher0*x0;
    chisquared0 = coh - ctranspose(p0)*x0 - ctranspose(x0)*p0 + ...
      real(ctranspose(p0)*fisher0*p0);
    lbayes = -0.5*chisquared0 + 0.5*log(det(invfisher0/sanity^2)) - ...
      NParms*log(DeltaP/sanity);
    lbayes = real(lbayes);
    if(~isfinite(lbayes))
      fprintf('lbayes is not finite; adjust sanity factor\n');
      return;
    end
    if(lbayes>worst)
      worst=lbayes;
      iiworst=ii;
    end
  end
  N=N-1;
  if worst>initial
    x(iiworst,:)=[];
    p(iiworst,:)=[];
    lvec(iiworst,:)=[];
    mvec(iiworst,:)=[];
    kept(jvec(iiworst))=0;
    jvec(iiworst,:)=[];
    fisher(:,iiworst)=[];
    fisher(iiworst,:)=[];
    initial=worst;
  else
    done=true;
  end
end

%--Reconstruct invfisher matrix with only saved parameters
%--If fisher_j1j2 = 0 iF j1 or j2 is thrown Out
nkept=length(fisher);
invfisher=inv(fisher);
invfisherF=zeros(jmax);
[lvec, mvec] = getLMvec(lmax);
for mm=1:length(fisher)
  for nn=1:length(fisher)
    JROW=jvec(mm);  JCOL=jvec(nn);
for kk=1:jmax
  for ll=1:jmax
  jrow=lvec(kk).^2+lvec(kk)+mvec(kk)+1;
jcol=lvec(ll).^2+lvec(ll)+mvec(ll)+1;
if ((jrow==JROW)&&(jcol==JCOL))
  invfisherF(kk,ll) = invfisher(mm,nn);
        end
      end
    end
  end
end


if (basis==2)
  invfisherF=inv(R)*invfisherF*R;
end

return
