%Checks if there are jobs that did not run.
%
%input
job_file='/archive/home/shivaraj/sgwb/S5/input/jobfiles/S5H1H2/S5H1H2_full_run_jobs.txt';
param_file='/archive/home/shivaraj/sgwb/S5/input/paramfiles/S5H1H2_timeshift/S5H1H2_strain_plus1sec_params_full_run.txt';
stoc_file_loc='/archive/home/shivaraj/sgwb/S5/output/S5H1H2_plus1sec_timeshift/output_H1H2_full_run_plus1sec';
fid=fopen('new_S5H1H2_plus1sec_shift_jobs.dag','w');

%Parameters
jobs=load(job_file);
num_jobs=size(jobs,1);
buff_sec=5;
seg_duration=1;
num_seg_per_interval=15;

%process
string='home="/archive/home/shivaraj" ld_library_path="/opt/lscsoft/lal/lib64:/opt/lscsoft/glue/lib64/python2.4/site-packages:/opt/lscsoft/libframe/lib64:/opt/lscsoft/libmetaio/lib64:/opt/lscsoft/framecpp/lib64:/opt/lscsoft/dol/lib64:/opt/lscsoft/root/lib64:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/tclglobus/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/apache/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/glite/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/myodbc/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/unixodbc/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/mysql/lib/mysql:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/jdk1.5/jre/lib/i386:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/jdk1.5/jre/lib/i386/server:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/jdk1.5/jre/lib/i386/client:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/berkeley-db/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/expat/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/globus/lib:/ldcg/ldg/vdt/globus/lib:/ldcg/ldg/vdt/globus/lib:/ldcg/ldg/vdt/globus/lib:/ligotools/lib"';

dag_jobs=[];
for i=1:num_jobs
  fprintf('loading Job No %d \n',i);
  if(jobs(i,4)>199)
    if(exist([stoc_file_loc,'_ccstats.job',num2str(i),'.trial1.dat'])==2)
    ccstat=load([stoc_file_loc,'_ccstats.job',num2str(i),'.trial1.dat']);
    naive_sigmas=load([stoc_file_loc,'_naivesigmas.job',num2str(i),'.trial1.dat']);
    com_ccstat=load([stoc_file_loc,'_combined_ccstats.job',num2str(i),'.trial1.dat']);
    badGPS=load([stoc_file_loc,'_badGPSTimes.job',num2str(i),'.trial1.dat']);

    myvar=floor((jobs(i,4)-2*buff_sec)/seg_duration);
    tot_no_segments=2*(myvar-(num_seg_per_interval-1))-1;
    if(length(ccstat) ~=0 && length(naive_sigmas) ~=0)
       if(tot_no_segments ~= length(ccstat(:,1)) || tot_no_segments ~= length(naive_sigmas(:,1)))
       dag_jobs=[dag_jobs i];
       fprintf('length of ccstat or naivesigma');
       end  
    else
      dag_jobs=[dag_jobs i]; 
    end
    if(length(ccstat)~=0 && length(com_ccstat)==0 && length(badGPS)==0)
      dag_jobs=[dag_jobs i];
      fprintf('length of com_ccstat or badGPS is zero');  
    end
  else
    dag_jobs=[dag_jobs i];
    fprintf('no file exist for job %d \n',i);
  end
 elseif(exist([stoc_file_loc,'_ccstats.job',num2str(i),'.trial1.dat'])~=2)
  dag_jobs=[dag_jobs i];
 end
end

%sorting missing jobs and making dag file
if(length(dag_jobs)~=0)
 new_dag=[];
 for i=1:length(dag_jobs)
  if(i==1)
   new_dag=[new_dag dag_jobs(i)];
  else
   if(dag_jobs(i)~=dag_jobs(i-1));
    new_dag=[new_dag dag_jobs(i)];
   end
  end
 end
dag_jobs=new_dag;
end

if(length(dag_jobs)~=0)
 for i=1:length(dag_jobs)
   fprintf(fid,'JOB %d stochastic_shift_pipe.sub \n',dag_jobs(i));
   fprintf(fid,'VARS %d paramsFile="%s" jobsFile="%s" jobNumber="%d" %s \n\n',dag_jobs(i),param_file,job_file,dag_jobs(i),string);
 end
end

if(length(dag_jobs)==0)
 fprintf('There is no file to re-run');
elseif(length(dag_jobs)==1)
 fprintf('There is only one job (no: %d), so no dag file is created',dag_jobs(1));
elseif(length(dag_jobs)>1)
 fprintf(fid,'PARENT %d CHILD ',dag_jobs(1));
 for j=2:length(dag_jobs)
  fprintf(fid,'%d ',dag_jobs(j));
 end
end
fclose(fid);
fprintf('The no. of jobs to re-run is %d \n',length(dag_jobs))
