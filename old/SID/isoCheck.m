%postBundle
load('S5_final.mat');

fisher=fisherP;                        %mar9
x=xP;                                  %mar9

x0=subX(x,0);
fisher0=subFisher(fisher,0);
invfisher0=inv(fisher0);
p=invfisher0*x0;
sigma=sqrt(invfisher0);
h0=0.72;
H0 =100*(h0/3.08568E19);
fref=100;
Omega=((2*pi^2)/(3*H0^2))*fref^3*sqrt(4*pi)*p; % Eq 2.21 in SpH paper
bias=1.02;
sigma_Omega=bias*((2*pi^2)/(3*H0^2))*fref^3*sqrt(4*pi)*sigma;

fprintf('Omega=%s +/- %s\n',Omega,sigma_Omega);
fprintf('isotropic result = (2.1 +/- 2.7) * 10^-6\n');

sigma_Omega_iso=2.7E-6;
epsilon=3/70;
wfac=(1-2*epsilon);
