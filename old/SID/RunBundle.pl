#! /usr/bin/perl
#
# First run this.  Then condor_submit_dag bundle.dag

#Make sure logs/ directory exists.
if (-e "logs/") {}
else {system "mkdir logs";}
#Make sure nqs/ directory exists.
if (-e "nqs/") {}
else {system "mkdir nqs";}

#Create S5_fileList.txt has been written if necessary.
$outfile="S5_fileList.txt";
if (-e $outfile) {}
else {
    $NNodes=331;
    $prefix="HL-SpHv1_H1L1_250mHz_SpHSet";
#old/wrong:    $prefix="HL-SpHv1_H1L1_250mHz_SpHSet1";
    $user="ethrane";
    $MaxMove=500000;
    open(outfile,">".$outfile);   
    $n=1;
    while ($n<=$NNodes) {
	foreach $file (</data/node$n/$user/$prefix*>) {
#TEMPORARY FIX FOR SpHSet1 TYPO------------------------------
#	    $setNumber=$file;  
#	    $setNumber =~ s/\/data\/node$n\/$user\/$prefix//;
#	    $setNumber =~ s/(.).*/\1/;
#	    if ($setNumber==1) {$file="";}
#-bash-3.00$ cat S5_fileList.txt | sed '/^ *$/d' > temp
#-bash-3.00$ mv temp S5_fileList.txt 
#-----------------------------------------------------------
	    print outfile "$file\n";
	}
	$n=$n+1;
    }
    close(outfile);
}

#Next, prepare for condor submission of bundle jobs.
$spiel = "JOB Z bundle.sub \nVARS Z filelist=\"/archive/home/ethrane/SID/nqs/Z.txt\" jobNumber=\"Z\" ld_library_path=\"/ldcg/matlab_r2007a/sys/os/glnx86:/ldcg/matlab_r2007a/bin/glnx86:/ldcg/matlab_r2007a/sys/java/jre/glnx86/jre1.5.0/lib/i386/native_threads:/ldcg/matlab_r2007a/sys/java/jre/glnx86/jre1.5.0/lib/i386/server:/ldcg/matlab_r2007a/sys/java/jre/glnx86/jre1.5.0/lib/i386:/ldcg/matlab_r2007a/sys/opengl/lib/glnx86:/ldcg/matlab_r2007a/sys/os/glnx86:/ldcg/matlab_r2007a/bin/glnx86:/ldcg/matlab_r2007a/sys/java/jre/glnx86/jre1.5.0/lib/i386/native_threads:/ldcg/matlab_r2007a/sys/java/jre/glnx86/jre1.5.0/lib/i386/server:/ldcg/matlab_r2007a/sys/java/jre/glnx86/jre1.5.0/lib/i386:/ldcg/matlab_r2007a/sys/opengl/lib/glnx86:/opt/lscsoft/lal/lib:/opt/lscsoft/glue/lib/python2.4/site-packages:/opt/lscsoft/libframe/lib:/opt/lscsoft/libmetaio/lib:/opt/lscsoft/framecpp/lib:/opt/lscsoft/dol/lib:/opt/lscsoft/root/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/tclglobus/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/apache/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/glite/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/myodbc/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/unixodbc/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/mysql/lib/mysql:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/jdk1.5/jre/lib/i386:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/jdk1.5/jre/lib/i386/server:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/jdk1.5/jre/lib/i386/client:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/berkeley-db/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/expat/lib:/ldcg/stow_pkgs/ldg-4.5/ldg/vdt/globus/lib:/ldcg/ldg/vdt/globus/lib:/ligotools/lib\"\n\n";
$out = ">S5_bundle.dag";
open(out,$out);

$in = "./S5_fileList.txt";  open(in,$in);
$n=1;   #files in fileList
$N=100; #number of files in a bundle
$i=0;   #jobs
$txt=">./nqs/$i.txt";  open(txt,$txt);
$home="/archive/home/ethrane/SID";
$max=1E99;
while(<in>) {
    if ($i>$max) {}
    else {
	print txt $_; #write .mat files to each bunch list
	$R=$n%$N;
	if($R==0) {
	    $i=$n/$N;
	    $txt=">./nqs/$i.txt";
	    close(txt); open(txt,$txt);
	    
	    #Write job to dagfile
	    $tempspiel = $spiel;  $tempspiel =~ s/Z/$i/g;
	    print out "$tempspiel";
	}
	$n++;
    }
}
close(in);  close(txt);

$tempspiel = $spiel;  $tempspiel =~ s/Z/0/g;
print out "$tempspiel";
print out "PARENT 0 CHILD";
for ($j=1; $j<=$i; $j++) {
    print out " $j";
}
print out "\n";
close(out);
