fprintf('Matlab must be run w/o -nodisplay or this routine will crash.\n');

clear all;
load('S5_final.mat');

if (exist('inc')) %keep the figures fresh
  inc=inc+10;
else
  inc=0;
end

%%%SNR map not regularized%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p=invfisher*x;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(inc+1) %SNR map
res=1;
if (exist('S5_mapdata.mat'))
  load('S5_mapdata.mat');
else
  [sigma,sigmaPix,RA,DECL,dOmg,U]=getSigmaMap(pCovar,res);
  [map,ra,decl]=makemap(p,res);
  save('S5_mapdata.mat','sigma','sigmaPix','RA','DECL','dOmg','U',...
       'map','ra','decl');
end
%--cet: 360 and 181 need to be changed if res ~= 1.
plotMapAitoff(map./sigmaPix,360,181,-1);
title('SNR map');
print('-depsc2','S5_SNR.eps');

figure(inc+2)
sn=[map,sigmaPix];
plotHistogram(sn,360,181);
%hfit=histfit(map./sigmaPix,20);   %cet
%[mu,sigma]=normfit(map./sigmaPix) %cet
title('sigma map');
print('-depsc2','S5_SNRhisto.eps'); %The one-sigma normal fit is off

figure(inc+3)
plotMapAitoff(sigmaPix,360,181,-1);
print('-depsc2','S5_sigma.eps');

%%%Next the regularized maps%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p=rinvfisher*x;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(inc+4)
plotPlmMap(p);
title('clean map');
print('-depsc2','S5_clean.eps');

figure(inc+5)
plotPlmMap(x)
title('dirty map');
print('-depsc2','S5_dirty.eps');

%%%Running peelback algorithms%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[invfisherF,nkeptF]=peelBack(x,fisher,coh);   %SpH basis
pF=invfisherF*x;

[invfisherN,nkeptN]=peelBack(x,fisher,coh,2); %Natural basis
pN=invfisherN*x;

save('S5_bayes.mat','pF','invfisherF','nkeptF','pN','invfisherN','nkeptN');
