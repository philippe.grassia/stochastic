#! /usr/bin/env perl
#####################################################################
# Use full (absolute) path names.
# Written by Tanner Prestegard, based on a script by Eric Thrane.
# Contact: prestegard@physics.umn.edu
# Last updated: 06/20/2011, v1.02.
# Designed to make cachefiles for running stochmap.m on STAMP frames.
# Subroutines for this script can be viewed at the end of the file.
#
# INSTRUCTIONS:
# Modify the variables in the 'PARAMETERS' section as needed.
# Run the script as directed below.
# There are various debugging lines throughout the code, which may
# be helpful.  Search for 'Debugging line' to find them.
#
# Usage:   ./frame_pipe.pl [option]
# Example: ./frame_pipe.pl 1
# OPTIONS:
# 1 - use this option when you have a jobfile with many jobs in it.
# 2 - use this option when you have a jobfile with one large job that
#     you would like to split into smaller jobs.
# 3 - use this option when your frames are on the nodes, NOT in a 
#     single, local directory.
#####################################################################
# INITIAL PROCESSES #################################################
#####################################################################
use strict;
use warnings;

my $option = $ARGV[0];
die("ERROR: Option must be 1, 2, or 3.  Exiting...\n")
    if ( !(defined($option)) || !($option =~ m/^(1|2|3)$/) );

my $home = $ENV{'HOME'}; # Gets home directory from shell environment variable.
#####################################################################
# PARAMETERS ########################################################
#####################################################################
#--------------------------------------------------------------------
# $preproc    -  your PreProcessing directory.  
# $paramfile  -  parameter file from preproc, used to get IFO names.
# $jobfile    -  your jobfile from preproc.
# $framedir   -  the directory containing your frames from preproc.
# $cachedir   -  the directory where your cachefiles will be created.
# $nStart     -  first job in your jobfile that you want to make
#                cachefiles for.
# $nMax       -  the last job in your jobfile that you want to make
#                cachefiles for.
#
# Note: Set $nStart = 1 and $nMax to a large number if you want all jobs.
#--------------------------------------------------------------------
my $preproc = $home."/matapps/packages/stochastic/trunk/PreProcessing";
my $paramfile = $preproc."/injections/params.txt";
my $jobfile = $preproc."/injections/jobfile.txt";
my $framedir = $home."/frames/tp_test";
my $cachedir = $home."/SID/cache/injection_test3";
my $nStart = 1;
my $nMax = 1;

# The following parameters are necessary for option 2 ONLY.
#--------------------------------------------------------------------
# $jobfile_out  -  specifies location of the new jobfile for option 2.
# $jobTime      -  specifies the length (in seconds) of the smaller
#                  jobs you want to split the large job into.
#--------------------------------------------------------------------
my $jobfile_out = $framedir."/jobfile.txt";
my $jobTime = 400; 

#####################################################################
# PARAMETER CHECKS ##################################################
#####################################################################
die("Path to PreProcessing (".$preproc.") is incorrect!  Exiting...\n")
    if !(-d $preproc);

die("Paramfile ".$paramfile." doesn't exist!  Exiting...\n")
    if !(-e $paramfile);

die("Jobfile ".$jobfile." doesn't exist!  Exiting...\n")
    if !(-e $jobfile);

die("Frame directory ".$framedir." doesn't exist!  Exiting...\n")
    if !(-d $framedir);

#####################################################################
# CODE ##############################################################
#####################################################################
# Gets path to location where cachefiles will be created.
my $intFrameCachePath = cache_path($cachedir);

# This checks if you already have cachefiles in this directory.
# If you do, the script exits so you don't overwrite them.
check_cachefiles($intFrameCachePath);

# Gets $BL ('HH', 'HL', etc.), which specifies ifos.
# Also gets prefix of frames (used to get frame list).
my ($BL,$prefix) = get_bl($paramfile);
my $fname = $prefix."*.gwf";

# Put contents of framedir (your frames) into an array.
my @frame_array;
if ( $option =~ m/^(1|2)$/ ) {
    @frame_array = qx( find $framedir -name $fname );
    @frame_array = sort(@frame_array);
}
elsif ( $option =~ m/^3$/ ) {
    # Get last name from 'HOME' to use in searching nodes.
    my $lname = $home;
    $lname =~ s/.*\/(.*)$/$1/;
    for (my $n=1; $n<=500; $n++) {
	my $dirname = "/data/node".$n."/".$lname;
	if ( -d $dirname ) {
	    my @temp_array = qx( find $dirname -name $fname );
	    push(@frame_array,@temp_array);
	}
	if ( ($n % 100) == 0 ) {
	    print "Done finding frames on nodes ".($n-99)."-".$n.".\n";
	}
    }

    # Sort frame array by GPS time instead of node.
    @frame_array = map { $_->[0] }
    sort { $a->[1] <=> $b->[1] }
    map { [$_, /-(\d+)-/] } @frame_array;

    # Remove GPS time repeats.
    my %seen;
    @frame_array = grep{ m{ (-\d+-) }xms; !$seen{$1}++ } @frame_array;
}

# Open jobfile and put it into an array.
open(JOBS,"<".$jobfile);
my @temp_job_data = <JOBS>;
close(JOBS);

my @job_data;
if ( $option =~ m/^(1|3)$/ ) {
    @job_data = @temp_job_data;
}
elsif ( $option =~ m/^2$/ ) {
    @job_data = split_job(@temp_job_data,$jobfile_out,$jobTime); 
}

# Loop through jobs in your jobfile.
my $n = 1; # Counter for jobs.
my $y = 0; # Counter for frames.
# Total number of jobs for which cachefiles are being generated.
# Used for updating status.
my $tot_jobs = ( scalar(@job_data) <= ($nMax-$nStart+1) ? scalar(@job_data) : ($nMax-$nStart+1) );

foreach my $JOB (@job_data) {
    # Skip all jobs before $nStart
    if ($n < $nStart) { $n++; next; }

    my $temp = $JOB;
    chomp($temp);

    # Match each column element of the jobfile to jobnum, startGPS, endGPS, and duration.
    my ($jobnum,$GPSstart,$GPSend,$duration) = match_columns($temp);

    # Define names of cachefiles: frameFile and gpsTimes.
    my $frameFile = $intFrameCachePath."/frameFiles".$BL.".".$n.".txt";
    my $gpsTimes = $intFrameCachePath."/gpsTimes".$BL.".".$n.".txt";

    open(FFILE,">".$frameFile);  open(GFILE,">".$gpsTimes);
    for (my $i=$y; $i<scalar(@frame_array); $i++) {
	my $frame_name = $frame_array[$i];
	chomp($frame_name);
	
	# GPS time comes straight from frame name.
	my $frameGPStime = $frame_name;
	$frameGPStime =~ s/.*-(\d+|\d+.\d+)-(\d+|\d+.\d+)\.gwf/$1/;

	# Debugging lines.
	#print $frame_name."\n".$frameGPStime."\n";
	#print $n." ".$y." ".$frameGPStime." ".$GPSstart." ".$GPSend."\n";
	
	# If the frame GPS time is between GPSstart and GPSend,
	# and not equal to the GPS time of the last frame, we 
	# print it to the current cachefile.  Otherwise, we exit this for loop.
	if( ($frameGPStime >= $GPSstart) && ($frameGPStime < $GPSend) ) {
	    print FFILE $frame_name."\n";
	    print GFILE $frameGPStime."\n";
	}
	elsif( $frameGPStime >= $GPSend ) {
	    last;
	}
	$y++;
    }
    close(FFILE);  close(GFILE);
    
    # Debugging lines.
    #print @frame_array."\n";
    #print $jobnum." ".$GPSstart." ".$GPSend." ".$duration."\n";

    # If we are using frames on the nodes, prints total jobs complete
    # after every 100 completions.
    if ( ($option == 3) && ( (($n-$nStart+1) % 100) == 0  || $n == ($tot_jobs+$nStart-1) ) ) {
	print "Done making cachefiles for ".($n-$nStart+1)." jobs of ";
	print $tot_jobs." total.\n";
    }
    $n++;
    if( $n > $nMax ) { last; }
}

#####################################################################
# SUBROUTINES #######################################################
#####################################################################

# Used if option 2 is specified.  Creates a new jobfile that 
# contains the large job split into smaller jobs.
sub split_job {
    my @temp_job_data = $_[0];
    my $jobfile_out = $_[1];
    my $jobTime = $_[2];
    
    foreach my $JOB (@temp_job_data) {

	my $temp = $JOB;
	chomp($temp);
	
	# Get jobnum, startGPS, endGPS, and duration from jobfile.
	my ($jobnum,$GPSstart,$GPSend,$duration) = match_columns($temp);
	
	my ($nJobs,$lastjobDur);
	if ( $duration % $jobTime == 0 ) {
	    $nJobs = $duration/$jobTime;
	    $lastjobDur = $jobTime;
	}
	else {
	    $nJobs = int($duration/$jobTime - 0.5) + 1;
	    $lastjobDur = $duration % $jobTime;
	}

	# Make new job file, if it doesn't exist already.
	if( !(-e $jobfile_out) ) {
	    open(NJFILE,">".$jobfile_out);
	    my $GPSend = $GPSstart + $jobTime;
	    for (my $n = 1; $n <= $nJobs; $n++) {
		
		my $jobDur;
		if ( $n < $nJobs ) { $jobDur = $jobTime };
		if ( $n == $nJobs ) { $jobDur = $lastjobDur };
		$GPSend = $GPSstart + $jobDur;
		
		if ( $n >= 0 && $n < 10 ) { print NJFILE "   ".$n; }
		if ( $n >= 10 && $n < 100 ) { print NJFILE "  ".$n; }
		if ( $n >= 100 && $n < 1000 ) { print NJFILE " ".$n; }
		if ( $n >= 1000 && $n < 10000 ) { print NJFILE $n; }
		
		print NJFILE "  ".$GPSstart."  ".$GPSend."  ".$jobDur."\n";
		
		$GPSstart = $GPSstart + $jobDur;
	    }
	    close(NJFILE);
	} else {
	    print "Jobfile exists already - delete it and re-run the script.  Exiting...\n";
	    exit;
	}
    }

    # Re-open new jobfile and put it into an array.
    open(JOBFILE2,"<".$jobfile_out);
    my @job_data = <JOBFILE2>; close(JOBFILE2);
    return @job_data;
}

# This checks if you already have cachefiles in your cachefile directory.
# If you do, the script exits, so that you don't overwrite them.
sub check_cachefiles {
    my $path = $_[0];
    my $num_files = qx( ls -l $path | grep -c ".txt\$" );
    if ( $num_files > 0 ) {
	print "Cachefiles already exist in this directory.  Move or delete them and try again.\n";
	print "Exiting...\n";
	exit;
    }
}

# Get $BL from paramfile.  $BL is 'HH', 'HL', etc.  It specifies
# which ifos your frames contain data from.
sub get_bl {
    my $paramfile = $_[0];
    my ($BL1, $BL2, $prefix);
    open(PFILE,"<".$paramfile);
    my @params = <PFILE>;  close(PFILE);
    foreach my $LINE (@params) {
	if( $LINE =~ m/^ifo1.*/ ) {
	    $BL1 = $LINE;
	    $BL1 =~ s/ifo1*\W(\w)\d/$1/;
	    chomp($BL1);
	}
	elsif( $LINE =~ m/^ifo2.*/ ) {
	    $BL2 = $LINE;
	    $BL2 =~ s/ifo2*\W(\w)\d/$1/;
	    chomp($BL2);
	}
	elsif( $LINE =~ m/^outputFilePrefix.*/ ) {
	    $prefix = $LINE;
	    $prefix =~ s/.*\/(.*)$/$1/;
	    chomp($prefix);
	}
    }
    my $BL = $BL1.$BL2;
    # Debugging line.
    #print $BL1." ".$BL2."\n";
    
    return ($BL,$prefix);
}

# Makes directory for holding cachefiles.
sub cache_path {
    my $cachedir = $_[0];

    # Create a folder in the "cachefiles" directory
    # if the folder doesn't already exist.
    my $intFrameCachePath = $cachedir;
    if ( !(-d $intFrameCachePath) ) {
	qx(mkdir $intFrameCachePath);
    }

    return $intFrameCachePath;
}    

# Used many times to match columns of a jobfile.
sub match_columns {
    my $line = $_[0];

    my ($jobnum,$GPSstart,$GPSend,$duration) = ($line,$line,$line,$line); 
    $jobnum =~ s/^\s*(\d+)\s+.*/$1/;
    $GPSstart =~ s/^\s*\d+(\s+|\t+)(\d+)(\s+|\t+).*/$2/;
    $GPSend =~ s/^\s*\d+(\s+|\t+)\d+(\s+|\t+)(\d+)(\s+|\t+).*/$3/;
    $duration =~ s/.*(\s+|\t+)(\d+)(\s*|\z)/$2/;

    # Debugging line.
    #print $jobnum." ".$GPSstart." ".$GPSend." ".$duration."\n";

    # Make sure duration is correct
    my $test_dur = $GPSend - $GPSstart;
    if( $test_dur != $duration ) {
        print "ERROR: Duration is not equal to";
        print " GPSstart - GPSend!  Exiting...\n";
        exit;
    }

    return ($jobnum,$GPSstart,$GPSend,$duration);
}

# EOF
