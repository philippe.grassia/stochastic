function run_background_sing(preproc_path,user)

randn('state',0);

data_type = 'Real'; % 'Real' or 'MC'
glitchCut = 0; % 0, 1, 2, or 3
segmentDur = 1;

mkdir_command=['mkdir Background_' data_type '_' num2str(glitchCut) ... 
   '_' num2str(segmentDur)];
system(mkdir_command);

% You must put your jobfile and params file in the Background folder
% The params file will be made for you below 
% A sample sub file is in this folder



