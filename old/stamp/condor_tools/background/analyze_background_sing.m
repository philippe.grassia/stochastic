function analyze_background

plotnum =1;

folder_location='/archive/home/mcoughlin/IM/GRB/Document/ADI_Point5/Run_Background_ADI';
folder_plot_name='Glitch_2_Burst';

mkdir_command=['mkdir ' folder_plot_name];
system(mkdir_command);

search_algorithm='Radon';
bins=0:0.1:50;

params.doReal=1;
params.doMC=1;

%params.Real_MC=[params.doReal params.doMC];
%Real or MC
params.Real_MC_name='Real';
params.glitchCutVals=2;
params.segmentDurVals=1;

folder_count=1;
data(folder_count).data_type=params.Real_MC_name{i};
data(folder_count).glitchCut=params.glitchCutVals(j);
data(folder_count).segmentDur = params.segmentDurVals(k);
data(folder_count).name = [data(folder_count).data_type '_' ...
   num2str(data(folder_count).glitchCut) '_' num2str(data(folder_count).segmentDur)];
folder_name=['Background_' data(folder_count).name];
    
location=[folder_location '/' folder_name]
data(folder_count).background=get_background_data(location);
data(folder_count).background_hist=hist(data(folder_count).background(:,2),bins);
background_hist_norm=data(folder_count).background_hist/ ... 
   length(data(folder_count).background(:,2));
data(folder_count).p_values=[];

for k=1:length(bins)
   data(folder_count).p_values=[data(folder_count).p_values; ... 
      sum(background_hist_norm(k:end))];
end

background_sort=sort(data(folder_count).background(:,2),'descend');
background_sort_01 = background_sort(ceil(length(background_sort)*.01));
background_sort_001 = background_sort(ceil(length(background_sort)*.001));
sprintf('Number of Ft-maps = %d\n',length(data(folder_count).background))
sprintf('P Value = 0.01 for %s : %.2f\n',data(folder_count).name,background_sort_01)
sprintf('P Value = 0.001 for %s : %.2f\n',data(folder_count).name,background_sort_001)
     
set(0,'defaultaxesfontsize',20);
%daily spectral-variation plots

figure(plotnum)
set(gcf, 'PaperSize',[10 8])
set(gcf, 'PaperPosition', [0 0 10 8])
clf

hold all
for i=1:folder_count
   errorbar(bins,data(i).background_hist/length(data(i).background(:,2)),sqrt(data(i).background_hist)/length(data(i).background(:,2)),'x')
end
hold off

xlabel([search_algorithm ' SNR'])
ylabel('Normalized Counts')

hleg1 = legend(data(:).name);
set(hleg1,'Location','NorthEast')
set(hleg1,'Interpreter','none')

saveas(gcf,[folder_plot_name '/snr_hist.jpeg']);
print('-depsc2',[folder_plot_name '/snr_hist.eps']);
close(plotnum);
plotnum = plotnum+1;

save('background','data','bins','params');

figure(plotnum)
set(gcf, 'PaperSize',[10 8])
set(gcf, 'PaperPosition', [0 0 10 8])
clf

hold all
for i=1:folder_count
   plot(bins,data(i).p_values);
end
hold off

ylim([0 1])

xlabel([search_algorithm ' SNR'])
ylabel('P Value')

hleg1 = legend(data(:).name);
set(hleg1,'Location','NorthEast')
set(hleg1,'Interpreter','none')

saveas(gcf,[folder_plot_name '/p_value.jpeg']);
print('-depsc2',[folder_plot_name '/p_value.eps']);
close(plotnum);
plotnum = plotnum+1;

figure(plotnum)
set(gcf, 'PaperSize',[10 8])
set(gcf, 'PaperPosition', [0 0 10 8])
clf

semilogy(bins,data(1).p_values);

hold all
for i=2:folder_count
   semilogy(bins,data(i).p_values);
end
hold off

xlabel([search_algorithm ' SNR'])
ylabel('P Value')

ylim([1e-5 1])

hleg1 = legend(data(:).name);
set(hleg1,'Location','NorthEast')
set(hleg1,'Interpreter','none')

saveas(gcf,[folder_plot_name '/p_value_log.jpeg']);
print('-depsc2',[folder_plot_name '/p_value_log.eps']);
close(plotnum);
plotnum = plotnum+1;

