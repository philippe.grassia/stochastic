#!/bin/bash

source /archive/home/mcoughlin/matlab_script_64bit.sh

matlab -q<<EOF
 run_background
EOF

for folder in Background_*
do
   cd $folder 
   source compile.sh
   condor_submit_dag -maxjobs 100 stochastic_pipe.dag
   cd ..
 
done

