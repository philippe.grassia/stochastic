function background=get_background_data(location)

x=dir([location '/results/*.txt']);

background=[];
i=0;
for i=1:length(x)
 try
  while length(background)<10000
    i=i+1;
    y=load([location '/results/' x(i).name]);
    gps_time_file=x(i).name;
    gps_time_file_gps=str2num(gps_time_file(1:9));
    for j=1:length(y)
      try
        background=[background;gps_time_file_gps+(j-1)*10 y(j,3)];
      catch
      end
    end
   end
 catch
 end
end
%end

