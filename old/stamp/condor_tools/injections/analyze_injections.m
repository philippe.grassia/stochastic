function analyze_injections

folder_plot_name='Plots';

mkdir_command=['mkdir ' folder_plot_name];
system(mkdir_command);

distances=linspace(10,200,1000);

plotnum=1;

data=load('/archive/home/mcoughlin/IM/GRB/Document/ADI_Point5/Run_Background/Compare_Background/background.mat');

which_background='Real_2';

for i=1:length(data.data)
   if strcmp(which_background,data.data(i).name)
      which_list=i;
   end
end

background_snr=data.data(which_list).background(:,2);
distance_snr=[];

for i=1:length(distances)
%  try
    which_folder=sprintf('%.3f',distances(i));
    which_folder=str2num(num2str(which_folder));
    y=load(['results/' num2str(which_folder) '/snr_value.txt']);
    if length(y(:,2))>0
       distance_snr=[distance_snr;distances(i) median(y(:,2)) length(y(:,2))];
    end
%  catch
%  end
end

background_cdf=[];

background_snr=sort(background_snr,'ascend');
background_keep=unique(background_snr);

for i=1:length(background_keep)
   which_vals=i:length(background_keep);
   cdf=length(which_vals)/length(background_keep);
   background_cdf=[background_cdf; background_keep(i) cdf];
end

p_values=[];
for i=1:length(distance_snr(:,2))
   p_values=[p_values; interp1(background_cdf(:,1),background_cdf(:,2),distance_snr(i,2))];
end

search_algorithm='Radon';

bins=0:0.1:25;

set(0,'defaultaxesfontsize',20);

figure(plotnum)
set(gcf, 'PaperSize',[10 8])
set(gcf, 'PaperPosition', [0 0 10 8])
clf

semilogy(distance_snr(:,1),distance_snr(:,2),'*');
hold on
semilogy(distance_snr(:,1),distance_snr(:,2));
%semilogy(x2(:,1),16.2397,'r');
hold off

%errorbar(x2(:,1),z2,sqrt(z2),'bx')
%set(gca,'XScale','log')

xlabel('Distance (Mpc)')
ylabel([search_algorithm ' SNR'])
saveas(gcf,[folder_plot_name '/snr_distance.jpeg']);
print('-depsc2',[folder_plot_name '/snr_distance.eps']);
close(plotnum);
plotnum = plotnum+1;

snr_thresh=11.71;
which_dist=min(abs(distance_snr(:,2)-snr_thresh));
which_p=find(abs(distance_snr(:,2)-snr_thresh)==which_dist)
which_d=distance_snr(which_p(1),1)

last_p_value=p_values(end)

figure(plotnum)
set(gcf, 'PaperSize',[10 8])
set(gcf, 'PaperPosition', [0 0 10 8])
clf

semilogy(distance_snr(:,1),p_values,'*');
hold on
semilogy(distance_snr(:,1),p_values);
semilogy(distance_snr(:,1),0.5,'r');
plot(which_d,0:.001:1,'k');
hold off
ylim([1e-4 1e0])

xlabel('Distance (Mpc)')
ylabel('P Value')
saveas(gcf,[folder_plot_name '/p_distance.jpeg']);
print('-depsc2',[folder_plot_name '/p_distance.eps']);
close(plotnum);
plotnum = plotnum+1;


