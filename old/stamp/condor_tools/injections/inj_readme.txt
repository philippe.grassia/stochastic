Instructions:

1.) Run inj_make_dag. It requires your username to know where to put the log file.

2.) Make a results and logs directory

3.) Change any parameters you want in inj_params.m, as this will create your params files.

4.) Compile run_injections

5.) condor_submit_dag -maxjobs 50 inj_pipe.dag 
