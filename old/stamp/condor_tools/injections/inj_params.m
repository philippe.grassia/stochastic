function inj_params(injection_size,job_start,job_end,injection_dur,inj_time)

%loads channel list

%fid=fopen(['files/' num2str(injection_size) '/PSD_test_file.dat'],'w+');
%fprintf(fid,'0.0001 %e\n',injection_size);
%fprintf(fid,'100 %e\n',injection_size);
%fprintf(fid,'8192 %e\n',injection_size);
%fclose(fid);

inj_data=load('M10a0.95eps0.2.dat');

fid=fopen(['results/' num2str(injection_size) '/time_test_file.dat'],'w+');

for i=1:length(inj_data(:,2))
   fprintf(fid,'%.8f %.12e %.12e\n',inj_data(i,1),inj_data(i,2)/injection_size,inj_data(i,3)/injection_size);
end
fclose(fid);

fid=fopen(['results/' num2str(injection_size) '/jobfile.txt'],'w+');
fprintf(fid,'1 %.0f %.0f %.0f\n',job_start,job_end,job_end-job_start);
fclose(fid);

fid=fopen(['results/' num2str(injection_size) '/params.txt'],'w+');

fprintf(fid,'% parameters for stochastic search (name/value pairs)\n');
fprintf(fid,'%\n');
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'\n');
fprintf(fid,'% flags for optional operations\n');
fprintf(fid,'doFreqMask false\n');
fprintf(fid,'doHighPass1 true\n');
fprintf(fid,'doHighPass2 true\n');
fprintf(fid,'doOverlap true\n');
fprintf(fid,'\n');
fprintf(fid,'doSidereal true\n');
fprintf(fid,'\n');
fprintf(fid,'minDataLoadLength 200\n');
fprintf(fid,' \n');
fprintf(fid,'doBadGPSTimes false\n');
fprintf(fid,'%badGPSTimesFile \n');
fprintf(fid,'maxDSigRatio 1.2\n');
fprintf(fid,'minDSigRatio 0.8\n');
fprintf(fid,'\n');

fprintf(fid,'doShift1 true\n');
fprintf(fid,'ShiftTime1 1\n');
fprintf(fid,'doShift2 false\n');
fprintf(fid,'ShiftTime2 0\n');
fprintf(fid,'\n');


fprintf(fid,'% ifo names\n');
fprintf(fid,'ifo1 H1\n');
fprintf(fid,'ifo2 L1\n');
fprintf(fid,'\n');
fprintf(fid,'% segment duration (sec)\n');
fprintf(fid,'segmentDuration 1\n');
fprintf(fid,'\n');
fprintf(fid,'% parameters for sliding psd estimation:\n');
fprintf(fid,'% numSegmentsPerInterval should be odd; ignoreMidSegment is a flag \n');
fprintf(fid,'% that allows you to ignore (if true) or include (if false) the \n');
fprintf(fid,'% analysis segment when estimating power spectra\n');
fprintf(fid,'numSegmentsPerInterval 9\n');
fprintf(fid,'ignoreMidSegment true\n');
fprintf(fid,'\n');
fprintf(fid,'% freq resolution and freq cutoffs for CC statistic sum (Hz)\n');
fprintf(fid,'flow 40\n');
fprintf(fid,'fhigh 2000\n');
fprintf(fid,'deltaF 1\n');
fprintf(fid,'\n');
fprintf(fid,'% params for Omega_gw (power-law exponent and reference freq in Hz)\n');
fprintf(fid,'alphaExp 0\n');
fprintf(fid,'fRef 100\n');
fprintf(fid,'\n');
fprintf(fid,'% resample rate (Hz)\n');
fprintf(fid,'resampleRate1 4096\n');
fprintf(fid,'resampleRate2 4096\n');
fprintf(fid,'\n');
fprintf(fid,'% buffer added to beginning and end of data segment to account for\n');
fprintf(fid,'% filter transients (sec)\n');
fprintf(fid,'bufferSecs1 2\n');
fprintf(fid,'bufferSecs2 2\n');
fprintf(fid,'\n');
fprintf(fid,'% ASQ channel\n');
fprintf(fid,'ASQchannel1 LSC-STRAIN\n');
fprintf(fid,'ASQchannel2 LSC-STRAIN\n');
fprintf(fid,'\n');
fprintf(fid,'% frame type and duration\n');
fprintf(fid,'frameType1 H1_RDS_C03_L2\n');
fprintf(fid,'frameType2 L1_RDS_C03_L2\n');
fprintf(fid,'frameDuration1 -1\n');
fprintf(fid,'frameDuration2 -1\n');
fprintf(fid,'\n');
fprintf(fid,'% duration of hann portion of tukey window \n');
fprintf(fid,'% (hannDuration = segmentDuration is a pure hann window)\n');
fprintf(fid,'hannDuration1 1\n');
fprintf(fid,'hannDuration2 1\n');
fprintf(fid,'\n');
fprintf(fid,'% params for matlab resample routine\n');
fprintf(fid,'nResample1 10\n');
fprintf(fid,'nResample2 10\n');
fprintf(fid,'betaParam1 5\n');
fprintf(fid,'betaParam2 5\n');
fprintf(fid,'\n');
fprintf(fid,'% params for high-pass filtering (3db freq in Hz, and filter order) \n');
fprintf(fid,'highPassFreq1 32\n');
fprintf(fid,'highPassFreq2 32\n');
fprintf(fid,'highPassOrder1 6\n');
fprintf(fid,'highPassOrder2 6\n');
fprintf(fid,'\n');
fprintf(fid,'% coherent freqs and number of freq bins to remove if doFreqMask=true;\n');
fprintf(fid,'% NOTE: if an nBin=0, then no bins are removed even if doFreqMask=true\n');
fprintf(fid,'% (coherent freqs are typically harmonics of the power line freq 60Hz\n');
fprintf(fid,'% and the DAQ rate 16Hz)\n');
fprintf(fid,'freqsToRemove \n');
fprintf(fid,'nBinsToRemove \n');
fprintf(fid,' \n');
fprintf(fid,'% calibration filenames\n');
fprintf(fid,'alphaBetaFile1 none\n');
fprintf(fid,'alphaBetaFile2 none\n');
fprintf(fid,'calCavGainFile1 none\n');
fprintf(fid,'calCavGainFile2 none\n');
fprintf(fid,'calResponseFile1 none\n');
fprintf(fid,'calResponseFile2 none\n');
fprintf(fid,'\n');
fprintf(fid,'% stochastic test\n');
fprintf(fid,'simOmegaRef 0\n');
fprintf(fid,'heterodyned false\n');

fprintf(fid,'\n');

fprintf(fid,'% path to cache files\n');
fprintf(fid,'gpsTimesPath1 /archive/home/ethrane/cache/S5H1L1/\n');
fprintf(fid,'gpsTimesPath2 /archive/home/ethrane/cache/S5H1L1/\n');
fprintf(fid,'frameCachePath1 /archive/home/ethrane/cache/S5H1L1/\n');
fprintf(fid,'frameCachePath2 /archive/home/ethrane/cache/S5H1L1/\n');
fprintf(fid,'\n');

fprintf(fid,'% STAMP injection\n');
fprintf(fid,'DoStampInj true\n');
fprintf(fid,'StampInjRA 6\n');
fprintf(fid,'StampInjDECL 30\n');
fprintf(fid,'% StampInjStart coincides with the start of job #3\n');
%fprintf(fid,'StampInjStart 816070756\n');
fprintf(fid,'StampInjStart %.0f\n',inj_time+1);
fprintf(fid,'\n');

fprintf(fid,'% PNS convection toy model\n');
fprintf(fid,'%StampInjType PSD\n');
fprintf(fid,'%StampInjFile results/%s/PSD_test_file.dat\n',num2str(injection_size));
fprintf(fid,'%StampInjDur 10\n');
fprintf(fid,'\n');

fprintf(fid,'% 300s sin wave with tapered beginning and end\n');
fprintf(fid,'StampInjType time_series\n');
fprintf(fid,'StampInjFile results/%s/time_test_file.dat\n',num2str(injection_size));
fprintf(fid,'\n');

fprintf(fid,'doDetectorNoiseSim false\n');
fprintf(fid,'DetectorNoiseFile LIGOsrdPSD_40Hz.txt\n');
fprintf(fid,'\n');

fprintf(fid,'%output filename prefix\n');
fprintf(fid,'outputFilePrefix results/%s/frames/S5H1L1_inj\n',num2str(injection_size));
fprintf(fid,'\n');

fprintf(fid,'stochmap true\n');

fprintf(fid,'fft1dataWindow -1\n');
fprintf(fid,'fft2dataWindow -1\n');

fprintf(fid,'startGPS %.0f\n',inj_time);
fprintf(fid,'endGPS %.0f\n',inj_time+injection_dur);

fprintf(fid,'% kludge factor (temporary)\n');
fprintf(fid,'kludge 1\n');

fprintf(fid,'% search direction (ra in hours, dec in degrees)\n');
fprintf(fid,'ra 6\n');
fprintf(fid,'dec 30\n');

fprintf(fid,'% frequency range\n');
fprintf(fid,'fmin 100\n');
fprintf(fid,'fmax 250\n');

fprintf(fid,'% other\n');
fprintf(fid,'doPolar false\n');
fprintf(fid,'saveMat false\n');
fprintf(fid,'savePlots false\n');
fprintf(fid,'debug false\n');
fprintf(fid,'doRadon true\n');
fprintf(fid,'doBoxsearch false\n');
fprintf(fid,'doLH false\n');
fprintf(fid,'doRadiometer false\n');

fprintf(fid,'doRadonReconstruction false\n');
fprintf(fid,'saveAutopower false\n');

fprintf(fid,'fixAntennaFactors true\n');
fprintf(fid,'doClusterSearch false\n');
fprintf(fid,'phaseScramble 0\n');
fprintf(fid,'glitchCut 2\n');
fprintf(fid,'bknd_study false\n');
fprintf(fid,'bknd_study_dur 10\n');

fprintf(fid,'pp_seed -1\n');

fclose(fid);

end
