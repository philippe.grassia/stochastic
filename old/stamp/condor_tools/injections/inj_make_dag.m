function inj_make_dag(user)

%[status ld_library_path]=system('echo $LD_LIBRARY_PATH')

%loads channel list

gps_times=linspace(10,200,1000);

%fid=fopen('jobfile.txt','w+');
%for i=1:length(gps_times)-1
%   fprintf(fid,'%d %.0f %.0f %.0f\n',i,gps_times(i),gps_times(i+1),gps_times(i+1)-gps_times(i));
%end
%fclose(fid);

rownum=length(gps_times);

fid=fopen('inj_pipe.dag','w+');

job_number=0;

for j=1:rownum
   job_number=job_number+1;
   fprintf(fid,'JOB %d inj_pipe.sub\n',job_number)
   fprintf(fid,'RETRY %d 3\n',job_number)
   fprintf(fid,'VARS %d jobNumber="%d" amplitude="%0.3f"',job_number,job_number,gps_times(j)); 
   fprintf(fid,'\n\n');
end

job_number=0;

fprintf(fid,'JOB %d inj_pipe.sub\n',0)

fprintf(fid,'VARS %d jobNumber="0" amplitude="0"',0);
fprintf(fid,'\nPARENT 0 CHILD');

for i=1:rownum
   fprintf(fid,' %d ',i);
end

fclose(fid);

fid=fopen('inj_pipe.sub','w+');
fprintf(fid,'executable = run_injections\n');
fprintf(fid,'output = logs/out.$(jobNumber)\n');
fprintf(fid,'error = logs/err.$(jobNumber)\n');
fprintf(fid,'arguments = $(amplitude)\n');
fprintf(fid,'requirements = Memory >= 128 && OpSys == "LINUX"\n');
fprintf(fid,'#universe = local\n');
fprintf(fid,'notification = never\n');
fprintf(fid,'getenv = True\n');
fprintf(fid,'#environment = HOME=$(home);LD_LIBRARY_PATH=$(ld_library_path)\n');
%fprintf(fid,'log = stochastic_pipe.log\n');
fprintf(fid,['log = /usr1/' user '/adi_injections.log\n']);
fprintf(fid,'+MaxHours = 24\n');
fprintf(fid,'queue 1\n');
fclose(fid);

