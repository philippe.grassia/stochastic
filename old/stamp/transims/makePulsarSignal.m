#!/usr/bin/env octave
# Octave wrapper for lalapps_Makefakedata_v4 and lalapps_ComputeFStatistic_v2
# This code simulates some Gaussian noise + transient pulsar producing both  
# time series data and SFTs. 
# lalapps and octapps are needed
# S. Giampanis <gstef{at}gravity.phys.uwm.edu

#myhome = getenv("HOME");

# pass input arguments
ARGV    = argv();
sftdir  = ARGV{1};
atoms   = ARGV{2};
results = ARGV{3};

# load in parameters 
params = makePulsarSignalParams();

# add octapps path
addpath(params.Utilities);

# parse parameters
Alpha = HMS2Rad ( params.RAJ );
Delta = Deg2Rad ( params.DECJ );

F0     = params.F0;
F1     = params.F1;
F2     = params.F2;
PEPOCH = params.PEPOCH;

cosi   = params.cosi;
psi    = params.psi; 
phi0   = params.phi0;
sqrtSn = params.sqrtSn;
h0     = params.h0;

if length(ARGV) == 4
  h0Hat = str2num(ARGV{4});
  h0    = h0Hat * params.sqrtSn; %# rss amplitude (strain)
  fprintf('Non-default signal amplitude: INJECTING from CLA');
else
  h0Hat  = params.h0Hat;
  fprintf('Default signal amplitude: INJECTING from param-file');
end

Tsft   = params.Tsft;

t0   = params.t0;
dur0 = params.dur0;
t1   = params.t1;
dur1 = params.dur1;
t2   = params.t2;
dur2 = params.dur2;

#sftdir  = params.sftdir;
#atoms   = params.atoms;
#results = params.results; 
tref    = params.tref;

t0DaysBand  = params.t0DaysBand;
tauDays     = params.tauDays;
tauDaysBand = params.tauDaysBand;


# injection code
mfd_code = "lalapps_Makefakedata_v4";

# MFDv4 injection code
mfd_code = "lalapps_Makefakedata_v4";

# CFSv2 code
cfs_code = "lalapps_ComputeFStatistic_v2";

# Clean up old files first
clean_code = sprintf("rm -rf %s/* %s/* %s/*",sftdir,atoms,results);
[status out] = system( clean_code );
if ( status != 0 )
  error ("Failed to clean up '%s'. error = %s\n", sftdir, out );
else
  printf ("OK.\n");
endif


#----------------------------------          Noise only      --------------------------------#
mfd_cmdline_H1 = sprintf ("%s --outSFTbname=%s -t %s/H1noise1TimeSeries --IFO=H1 --Band=100 --generationMode=1 --startTime=%d --duration=%d --h0=%g --cosi=%g --psi=%g --phi0=%g --Alpha=%f --Delta=%f --noiseSqrtSh=%g --Freq=%f --f1dot=%g --f2dot=%g --Tsft=%d -y '05-09'", mfd_code, sftdir, sftdir, t0, dur0, 0, cosi, psi, phi0, Alpha, Delta, sqrtSn, F0, F1, F2, Tsft );

printf ( "%s\n", mfd_cmdline_H1 );
[status, out] = system ( mfd_cmdline_H1 );
if ( status != 0 )
  error ("Failed to run cmdline '%s'. error = %s\n", mfd_cmdline_H1, out );
else
  printf ("OK.\n");
endif


mfd_cmdline_L1 = sprintf ("%s --outSFTbname=%s -t %s/L1noise1TimeSeries --IFO=L1 --Band=100 --generationMode=1 --startTime=%d --duration=%d --h0=%g --cosi=%g --psi=%g --phi0=%g --Alpha=%f --Delta=%f --noiseSqrtSh=%g --Freq=%f --f1dot=%g --f2dot=%g --Tsft=%d -y '05-09'", mfd_code, sftdir, sftdir, t0, dur0, 0, cosi, psi, phi0, Alpha, Delta, sqrtSn, F0, F1, F2, Tsft );

printf ( "%s\n", mfd_cmdline_L1 );
[status, out] = system ( mfd_cmdline_L1 );
if ( status != 0 )
  error ("Failed to run cmdline '%s'. error = %s\n", mfd_cmdline_L1, out );
else
  printf ("OK.\n");
endif


#---------------------------------          Signal + noise    ------------------------------#
mfd_cmdline_H1 = sprintf ("%s --outSFTbname=%s -t %s/H1signalTimeSeries --IFO=H1 --Band=100 --generationMode=1 --startTime=%d --duration=%d --h0=%g --cosi=%g --psi=%g --phi0=%g --Alpha=%f --Delta=%f --refTimeMJD=%g --noiseSqrtSh=%g --Freq=%f --f1dot=%g --f2dot=%g --Tsft=%d -y '05-09'", mfd_code, sftdir, sftdir, t1, dur1, h0, cosi, psi, phi0, Alpha, Delta, PEPOCH, sqrtSn, F0, F1, F2, Tsft );

printf ( "%s\n", mfd_cmdline_H1 );
[status, out] = system ( mfd_cmdline_H1 );
if ( status != 0 )
  error ("Failed to run cmdline '%s'. error = %s\n", mfd_cmdline_H1, out );
else
  printf ("OK.\n");
endif


mfd_cmdline_L1 = sprintf ("%s --outSFTbname=%s -t %s/L1signalTimeSeries --IFO=L1 --Band=100 --generationMode=1 --startTime=%d --duration=%d --h0=%g --cosi=%g --psi=%g --phi0=%g --Alpha=%f --Delta=%f --refTimeMJD=%g --noiseSqrtSh=%g --Freq=%f --f1dot=%g --f2dot=%g --Tsft=%d -y '05-09'", mfd_code, sftdir, sftdir, t1, dur1, h0, cosi, psi, phi0, Alpha, Delta, PEPOCH, sqrtSn, F0, F1, F2, Tsft );

printf ( "%s\n", mfd_cmdline_L1 );
[status, out] = system ( mfd_cmdline_L1 );
if ( status != 0 )
  error ("Failed to run cmdline '%s'. error = %s\n", mfd_cmdline_L1, out );
else
  printf ("OK.\n");
endif


#--------------------------------     Noise only     -------------------------------#
mfd_cmdline_H1 = sprintf ("%s --outSFTbname=%s -t %s/H1noise2TimeSeries --IFO=H1 --Band=100 --generationMode=1 --startTime=%d --duration=%d --h0=%g --cosi=%g --psi=%g --phi0=%g --Alpha=%f --Delta=%f --noiseSqrtSh=%g --Freq=%f --f1dot=%g --f2dot=%g --Tsft=%d -y '05-09'", mfd_code, sftdir, sftdir, t2, dur2, 0, cosi, psi, phi0, Alpha, Delta, sqrtSn, F0, F1, F2, Tsft );

printf ( "%s\n", mfd_cmdline_H1 );
[status, out] = system ( mfd_cmdline_H1 );
if ( status != 0 )
  error ("Failed to run cmdline '%s'. error = %s\n", mfd_cmdline_H1, out );
else
  printf ("OK.\n");
endif


mfd_cmdline_L1 = sprintf ("%s --outSFTbname=%s -t %s/L1noise2TimeSeries --IFO=L1 --Band=100 --generationMode=1 --startTime=%d --duration=%d --h0=%g --cosi=%g --psi=%g --phi0=%g --Alpha=%f --Delta=%f --noiseSqrtSh=%g --Freq=%f --f1dot=%g --f2dot=%g --Tsft=%d -y '05-09'", mfd_code, sftdir, sftdir, t2, dur2, 0, cosi, psi, phi0, Alpha, Delta, sqrtSn, F0, F1, F2, Tsft );

printf ( "%s\n", mfd_cmdline_L1 );
[status, out] = system ( mfd_cmdline_L1 );
if ( status != 0 )
  error ("Failed to run cmdline '%s'. error = %s\n", mfd_cmdline_L1, out );
else
  printf ("OK.\n");
endif

# CFSv2
cfs_cmdline = sprintf ("%s --Alpha=%f --Delta=%f --Freq=%f --f1dot=%g --f2dot=%g --DataFiles='%s/*.sft' --ephemYear='05-09' --refTime=%f --outputFstat='%s/Fstat' --outputLoudest='%s/FstatLoud' --outputFstatAtoms='%s/FstatAtoms' --outputTransientStats='%s/FstatTransStats.dat' --transient_WindowType='rect' --transient_t0DaysBand=%f --transient_tauDays=%f --transient_tauDaysBand=%f --transient_useFReg=TRUE", cfs_code, Alpha, Delta, F0, F1, F2, sftdir, tref, results, results, atoms, results, t0DaysBand, tauDays, tauDaysBand);

printf ( "%s\n", cfs_cmdline );
[status, out] = system ( cfs_cmdline );
if ( status != 0 )
  error ("Failed to run cmdline '%s'. error = %s\n", cfs_cmdline, out );
else
  printf ("OK.\n");
endif

# CFSv2 
cfs_cmdline = sprintf ("%s --Alpha=%f --Delta=%f --Freq=%f --f1dot=%g --f2dot=%g --DataFiles='%s/*.sft' --ephemYear='05-09' --refTime=%f --outputFstat='%s/FstatBACK' --outputLoudest='%s/FstatLoudBACK' --outputFstatAtoms='%s/FstatAtomsBACK' --outputTransientStats='%s/FstatTransStatsBACK.dat' --transient_WindowType='rect' --transient_t0DaysBand=%f --transient_tauDays=%f --transient_tauDaysBand=%f --transient_useFReg=TRUE", cfs_code, Alpha, Delta, F0+1, F1, F2, sftdir, tref, results, results, atoms, results, t0DaysBand, tauDays, tauDaysBand);

printf ( "%s\n", cfs_cmdline );
[status, out] = system ( cfs_cmdline );
if ( status != 0 )
  error ("Failed to run cmdline '%s'. error = %s\n", cfs_cmdline, out );
else
  printf ("OK.\n");
endif


exit(0);
