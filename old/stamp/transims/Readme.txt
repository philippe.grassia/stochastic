

## Functions used in this pipeline ##

A. "makePulsarSignal.m" 

head -6 makePulsarSignal.m
#!/usr/bin/env octave
# Octave wrapper for lalapps_Makefakedata_v4 and lalapps_ComputeFStatistic_v2
# This code simulates some Gaussian noise + transient pulsar producing both  
# time series data and SFTs. 
# lalapps and octapps are needed
# S. Giampanis <gstef{at}gravity.phys.uwm.edu


B. "makePulsarFrames.c"
lalapps_Makefakedata_v4 does not output gwf frames so I have written this simple C code

[gstef@ldas-pcdev1 transims]$ ./makePulsarFrames -h
Arguments are :
          --time-series-file (-f)               CHAR     Time series file to use.
          --observatory (-i)                    CHAR     Observatory (H,L,etc).
          --channel (-c)                        CHAR     Channel name (L1:STRAIN, etc).
          --outdir (-o)                         CHAR     Output frames directory  name (./frames).
          --startTime (-s)                      FLOAT    Start time.
          --rate (-r)                           FLOAT    Time-series sampling frequency.
          --fMin (-m)                           FLOAT    Minimum detector frequency.
          --Tobs (-t)                           FLOAT    Frame duration.

which is compiled via "make" (needs lal, lalframe and libframe)

cat Makefile  
all:
        gcc -W -Wall -std=gnu99 makePulsarFrames.c -o makePulsarFrames $(shell pkg-config --libs --cflags lal lalframe libframe)

clean:
        rm -rf frames/*


C. "run_all.py" 
python wrapper for use with makePulsarFrames, to turn ascii time series into gwf frames (again, hardcoded variables need to be removed)

cat run_all.py 
#!/usr/bin/env python

import os

directory = './sftdir/'

ts = [directory+x for x in os.listdir(directory) if 'TimeSeries' in x]

for f in ts:
    time = open(f).readline().split('.')[0]
    os.system('./makePulsarFrames -f %s -i %s -c %s -o %s -s %s -r 200 -m 40 -t 100' % (f, f[-21], f[-21]+'1:STRAIN', './frames/',time))



D.



