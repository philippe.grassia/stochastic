#!/usr/bin/env python

import os
import scipy

N = 1000 # number of h0hat pts

h0hatmax = 0.6 # max(h0hat)
h0hatmin = 0.6  # min(h0hat) 

# h0hat array
b = scipy.array([h0hatmin+i*(h0hatmax-h0hatmin)/(N-1) for i in range(N)])

dagfile = open("trans_pipeline.dag",'w')

for i in range(N):
    dagfile.write("JOB\t%d trans_pipeline.sub\n" % i)
    dagfile.write("VARS\t%d JOB=\"%d\" h0HAT=\"%g\"\n" % (i,i,b[i]))

dagfile.close()



