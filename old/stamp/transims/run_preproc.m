function run_preproc(paramfile, jobfile, jobnumber, varargin)

optargin = size(varargin,2); % the optional additional parameters

if optargin == 0
  preproc(paramfile, jobfile, jobnumber);
elseif optargin == 1
  parmsfile = varargin{1};
  try
    parms = readParamsFromFile(parmsfile);
  catch 
    error('could not parse in extra param file');
  end
  try
    preproc(paramfile, jobfile, jobnumber, parms);
  catch
    error('preproc failure, consult preproc error messages');
  end
else
  error('optargin has to be either 0 or 1');
end