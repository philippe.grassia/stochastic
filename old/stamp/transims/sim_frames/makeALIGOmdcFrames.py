#!/usr/bin/env python
# Wrapper for creating ALIGO simulation frames
# This code requires LALsimulation function lalsim-ligo-noise
# and home-brewed frame writer "makeFrames.c" 
# as well as pylal to be installed on your machine
# See README and Makefile for more info on how to use it
# example: ./makeALIGOmdcFrames.py 900000000 4 2 900000001 1 1.571 0.524 H1 ./tmp/
# Stefanos Giampanis <gstef@gravity.phys.uwm.edu>


# Some imports
import os
import sys
import time
import random


#from pylal import antenna

# input arguments
starttime = float(sys.argv[1]) # start time of simulation data
Tobs      = float(sys.argv[2]) # duration of simulation data
Tframe    = float(sys.argv[3]) # duration of frames
INJ       = float(sys.argv[4]) # start time of injection
Tinj      = float(sys.argv[5]) # duration of injection
RA        = float(sys.argv[6]) # injection right ascention in radians
DEC       = float(sys.argv[7]) # injection declination in radians
IFO       = sys.argv[8]        # interferometer
directory = sys.argv[9]        # output directory for frame writing

t    = starttime

##########  step 1: simulation code for producing ALIGO time series (ascii) ########


# cache files for preproc 
gpscachefile   = "gpsTimes"+str(IFO[0])+".1.txt"

try:
        f1 = open(gpscachefile,'r+')
        os.system("rm "+ gpscachefile)    # remove existing file and
        os.system("touch "+ gpscachefile) # create new (empty) one
except:
        os.system("touch "+ gpscachefile) # create new (empty) one
        
f1 = open(gpscachefile,'r+')


framecachefile   = "frameTimes"+str(IFO[0])+".1.txt"

try:
        f2 = open(framecachefile,'r+')
        os.system("rm "+ framecachefile)  # remove existing file and
        os.system("touch "+ framecachefile) # create new (empty) one
except:
        os.system("touch "+ framecachefile) # create new (empty) one 

f2 = open(framecachefile,'r+')


# OS command for lalsim-ligo-noise 
while t < starttime+Tobs:
    rstring = "GSL_RNG_SEED=" + str(int(time.time())) + str(random.randint(1, 100000))
    filename = "Sim-" + str(IFO) + "-" + str(t) + "-" + str(Tframe) + ".dat"
    cmd = " lalsim-ligo-noise --aligo-nosrm -s " + str(t) + " -t " + str(Tframe) + " > " + str(directory) + filename
    print ""
    print "*************** ALIGO noise simulation *********************"
    print cmd
    print "************************************************************"
    os.system(rstring + cmd)
    cmd1 = "./makeFrames -f " + str(directory) + filename + " -j inj_file_resamp.txt" 
    cmd2 = " -g " + str(INJ) + " -d " + str(Tinj) + " -a " + str(RA) + " -l " + str(DEC)
    cmd3 = " -i " + str(IFO) + " -c " + str(IFO)+":STRAIN" + " -o " + str(directory) 
    cmd4 = " -s " + str(t) + " -t " + str(Tframe) + " -r 16384 -m 9"
    print "****************  ALIGO frame writing  *********************"
    print cmd1+"\n"+cmd2+"\n"+cmd3+"\n"+cmd4
    print "************************************************************"
    print ""
    os.system(cmd1+cmd2+cmd3+cmd4)
    t = t + Tframe
    f1.write(str(t)+" \n") # cache file writing
    f2.write(str(os.getcwd())+"/"+str(directory[2:])+"Sim"+str(IFO)+"-"+str(IFO)+":STRAIN"+"-"+str(int(t))+"-"+str(int(Tframe))+".gwf"+" \n")

f1.close()
f2.close()


