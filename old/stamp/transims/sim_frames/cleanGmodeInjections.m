#!/usr/bin/env octave
# This script massages the injection file (input argument) 
# by resampling it to 16384 Hz sampling frequency.
# The new data is saved as "inj_file_resamp.txt"
# exaple: ./cleanGmodeInjections.m m15b6.h.dat
# Stefanos Giampanis <gstef@gravity.phys.uwm.edu>

ARGV    = argv();

file = ARGV{1};

data = load(file);

t  = data(:,1);
hp = 1000*data(:,2);
hx = data(:,3);

# interpolate data to 16384 Hz sampling rate

times = linspace(t(1),t(end),(t(end)-t(1))*16384);

hp_resamp = interp1 (t, hp, times, "spline");
hx_resamp = interp1 (t, hx, times, "spline");

#newdata = [t(~isnan(t)),hp(~isnan(hp)),hx(~isnan(hx))];

newdata = [times',hp_resamp',hx_resamp'];

save -ascii "inj_file_resamp.txt" newdata

printf("\"%s\" -> \"inj_file_resamp.txt\"\n",file)
printf("injection file has %2.2f sec duration",times(end)-times(1))