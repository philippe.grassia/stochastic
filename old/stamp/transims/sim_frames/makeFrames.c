/* This function reads in existing (noise and signal) time series (ascii) and writes out frames (gwf)*/
/* S. Giampanis gstef@gravity.phys.uwm.edu */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <getopt.h>

#include <lal/LALStdio.h>
#include <lal/AVFactories.h>
#include <lal/ConfigFile.h>
#include <lal/LALFrameIO.h>
#include <lal/FrameStream.h>
#include <lal/LALDetectors.h>
#include <lal/TimeSeries.h>
#include <lal/Units.h>
#include <lal/LALFrameL.h>
#include <lal/DetResponse.h>

struct CommandLineArgsTag {
    char  *timeseriesfile;
    char  *injectionfile;
    char  *observatory;
    char  *channel;
    char  *outdirectory;
    UINT4 startTime;
    UINT4 INJstartTime;
    REAL4 rate;
    REAL4 fMin;
    REAL4 Tobs;
    REAL4 Tinj;
    double ra;
    double dec;
} CLA;


/***************************************************************************/
/* Reads the command line */
int ReadCommandLine(int argc,char *argv[],struct CommandLineArgsTag *CLA);

/************************************* MAIN PROGRAM *************************************/

int main( int argc, char *argv[] )
{

    /* read the command line */
    if (ReadCommandLine(argc,argv,&CLA)) return 1;
    
    /* define and initialize status pointer */
    static LALStatus status;
    status.statusPtr = NULL;

    /* pointer to input file */
    FILE *pFile1 = NULL;

    /* pointer to injection file */
    FILE *pFile2 = NULL;

    /* time series file name */
    CHAR fileName1[LALNameLength];

    /* injection file name */
    CHAR fileName2[LALNameLength];

    /* parameters for frame generation */
    LIGOTimeGPS gpsStartTime,injStartTime;
    UINT4 Nseg1 = floor(CLA.Tobs*CLA.rate);
    UINT4 Nseg2 = floor(CLA.Tinj*CLA.rate);

    /* time-series file */
    snprintf(fileName1, LALNameLength,"%s",CLA.timeseriesfile);
   
    /* injection file */
    snprintf(fileName2, LALNameLength,"%s",CLA.injectionfile);

    /* LIGO gps time structures */
    gpsStartTime.gpsSeconds = CLA.startTime;
    gpsStartTime.gpsNanoSeconds = 0.;

    injStartTime.gpsSeconds = CLA.INJstartTime;
    injStartTime.gpsNanoSeconds = 0.;
    

    /* noise time series */ /* we are assuming that both time series have same sampling rate */
    REAL8TimeSeries *series1;
    series1 = XLALCreateREAL8TimeSeries(CLA.channel, &gpsStartTime, CLA.fMin, 1./CLA.rate, &lalStrainUnit, Nseg1);  

    /* injection time series */
    REAL8TimeSeries *series2;
    series2 = XLALCreateREAL8TimeSeries(CLA.channel, &injStartTime, CLA.fMin, 1./CLA.rate, &lalStrainUnit, Nseg2);

    /* Detector antenna pattern function (F+, Fx) */
    REAL4TimeSeries *Fplus;
    REAL4TimeSeries *Fcross;
    LALDetector d;
    char ifo1[] = "H1";
    char ifo2[] = "L1";
    char ifo3[] = "V1";

    if ( strcmp(CLA.observatory,ifo1) == 0 )
    {
        d = lalCachedDetectors[4];
    }
    else if ( strcmp(CLA.observatory,ifo2) == 0  )
    {
        d = lalCachedDetectors[5];
    }
    else if ( strcmp(CLA.observatory,ifo3) == 0  )
    {
        d = lalCachedDetectors[1];
    }
    else
    {
        fprintf(stderr,"Unrecognized detector %s [specify either H1, L1 or V1]\n",CLA.observatory);
        exit(1);
    }
   
    XLALComputeDetAMResponseSeries(&Fplus,&Fcross,d.response,CLA.ra,CLA.dec,0,&injStartTime,1./CLA.rate,Nseg2);

    
    /* read files */
    pFile1 = fopen(fileName1,"r"); /* col1: GPStime       | col2: strain           */
    pFile2 = fopen(fileName2,"r"); /* col1: time (0-?sec) | col2: h+    | col3: hx */

    if (pFile1==NULL || pFile2==NULL) {fputs ("File error\n",stderr); exit (1);}
   
    /* fill up REAL8TimeSeries strain data */
    UINT4 i;
    for (i = 0; i < Nseg1; i++)
        if (fscanf(pFile1, "%*g %lg", &series1->data->data[i]) == EOF)
            break;

    /* fill up REAL8TimeSeries injection-data */
    UINT4 j;
    double times; 
    for (j = 0; j < Nseg2; j++)
    {
        fscanf(pFile2, "%*g %lg %*g", &series2->data->data[j]);
        series2->data->data[j] *= Fplus->data->data[j];
        /* fprintf(stdout,"%f \n",Fplus->data->data[j]); */
        if (fscanf(pFile2, "%lg %*g %*g", &times) == EOF)
            break;
    }
   
    fclose(pFile1);
    fclose(pFile2);

    
    /* frame parameters - structure in FrameStream.h */ 
    /* noise time series frame */
    char namePrefix[] = "Sim";
    /*FrOutPar frSeries = {strcat(CLA.outdirectory,namePrefix), CLA.channel, SimDataChannel, 1, 0, 0 };*/

    /* write noise-only time series output (frames) */
    /*LALFrWriteREAL8TimeSeries(&status,series1,&frSeries);*/


    /* Add time series here */
    //  if ( CLA.startTime >= CLA.INJstartTime - CLA.Tobs && CLA.startTime <= CLA.INJstartTime + CLA.Tinj )
    // {
    XLALAddREAL8TimeSeries(series1,series2);
    // }

    /* write noise+signal output (frames) */
    FrOutPar newfrSeries = {strcat(strcat(CLA.outdirectory,namePrefix),CLA.observatory), CLA.channel, SimDataChannel, 1, 0, 0 };
    LALFrWriteREAL8TimeSeries(&status,series1,&newfrSeries);

    return 0;

}



/*******************************************************************************/
int ReadCommandLine(int argc,char *argv[],struct CommandLineArgsTag *CLA)
{
  int errflg = 0;
  optarg = NULL;

  struct option long_options[] = {
    {"time-series-file",          required_argument, NULL,           'f'},
    {"injection-file",            required_argument, NULL,           'j'},
    {"observatory",               required_argument, NULL,           'i'},
    {"channel",                   required_argument, NULL,           'c'},
    {"outdir",                    required_argument, NULL,           'o'},
    {"startTime",                 required_argument, NULL,           's'},
    {"INJstartTime",              required_argument, NULL,           'g'},
    {"rate",                      required_argument, NULL,           'r'},
    {"fMin",                      required_argument, NULL,           'm'},
    {"Tobs",                      required_argument, NULL,           't'},
    {"Tinj",                      required_argument, NULL,           'd'},
    {"ra",                        required_argument, NULL,           'a'},
    {"dec",                       required_argument, NULL,           'l'},
    {0, 0, 0, 0}
  };
  char args[] = "hf:j:i:c:o:s:g:r:m:t:d:a:l:";

  CLA->timeseriesfile = NULL;  
  CLA->injectionfile  = NULL;
  CLA->observatory    = NULL;
  CLA->channel        = NULL;
  CLA->outdirectory   = NULL;
  CLA->startTime      = 0;
  CLA->INJstartTime   = 0;
  CLA->rate           = -1;          
  CLA->fMin           = -1;
  CLA->Tobs           = -1;
  CLA->Tinj           = -1;
  CLA->ra             = -1;
  CLA->dec            = -1;
 /* Scan through list of command line arguments */
  while ( 1 )
  {
    int option_index = 0; /* getopt_long stores long option here */
    int c;

    c = getopt_long_only( argc, argv, args, long_options, &option_index );
    if ( c == -1 ) /* end of options */
      break;

    switch ( c )
    {

    case 'f':
      /* time series file  */
        CLA->timeseriesfile = optarg;
      break;
    case 'j':
        /* injection file  */
        CLA->injectionfile = optarg;
        break;
    case 'i':
      /* observatory  */
        CLA->observatory = optarg;
      break;
    case 'c':
      /* channel */
        CLA->channel = optarg;
      break;
    case 'o':
        /* output directory */
        CLA->outdirectory = optarg;
        break;
    case 's':
      /* time series start time  */
        CLA->startTime = atoi(optarg);
      break;
    case 'g':
        /* injection start time  */
        CLA->INJstartTime = atoi(optarg);
        break;
    case 'r':
      /* sampling rate */
        CLA->rate = atof(optarg);
      break;
    case 'm':
      /* lowest detector frequency */
        CLA->fMin = atoi(optarg);
      break;
    case 't':
      /* frame duration (observation time) */
        CLA->Tobs = atof(optarg);
      break;
    case 'd':
        /* injection duration */
        CLA->Tinj = atof(optarg);
        break;
    case 'a':
        /* injection right ascention (radians) */
        CLA->ra = atof(optarg);
        break;
    case 'l':
        /* injection declination (radians) */
        CLA->dec = atof(optarg);
        break;

    case 'h':
      /* print usage/help message */
      fprintf(stdout,"Arguments are :\n");
      fprintf(stdout,"\t--time-series-file (-f)\t\tCHAR\t Time series file to use.\n");
      fprintf(stdout,"\t--injection-file (-j)\t\tCHAR\t Injection file to use.\n");
      fprintf(stdout,"\t--observatory (-i)\t\tCHAR\t Observatory (H,L,etc).\n");
      fprintf(stdout,"\t--channel (-c)\t\t\tCHAR\t Channel name (L1:STRAIN, etc).\n");
      fprintf(stdout,"\t--outdir (-o)\t\t\tCHAR\t Output frames directory  name (./frames).\n");
      fprintf(stdout,"\t--startTime (-s)\t\tFLOAT\t Time series start time.\n");
      fprintf(stdout,"\t--INJstartTime (-g)\t\tFLOAT\t Injection start time.\n");
      fprintf(stdout,"\t--rate (-r)\t\t\tFLOAT\t Time-series sampling frequency.\n");
      fprintf(stdout,"\t--fMin (-m)\t\t\tFLOAT\t Minimum detector frequency.\n");
      fprintf(stdout,"\t--Tobs (-t)\t\t\tFLOAT\t Frame duration.\n");
      fprintf(stdout,"\t--Tinj (-d)\t\t\tFLOAT\t Injection duration.\n");
      fprintf(stdout,"\t--ra (-a)\t\t\tFLOAT\t Injection right ascention in radians.\n");
      fprintf(stdout,"\t--dec (-l)\t\t\tFLOAT\t Injection declination in radians.\n");
      exit(0);
      break;
    default:
      /* unrecognized option */
      errflg++;
      fprintf(stderr,"Unrecognized option argument %c\n",c);
      exit(1);
      break;
    }
    }

  if(CLA->timeseriesfile == NULL)
    {
      fprintf(stderr,"No time series file specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->injectionfile == NULL)
  {
      fprintf(stderr,"No injection file specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
  }
  if(CLA->observatory == NULL)
    {
      fprintf(stderr,"No observatory specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->channel == NULL)
    {
      fprintf(stderr,"No channel name specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->outdirectory == NULL)
  {
      fprintf(stderr,"No output frame directory name specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
  }
  if(CLA->startTime == 0)
    {
      fprintf(stderr,"No start time specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->INJstartTime == 0)
  {
      fprintf(stderr,"No injection start time specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
  }
  if(CLA->rate == -1)
    {
      fprintf(stderr,"No sampling frequency specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->fMin == -1)
    {
      fprintf(stderr,"No minimum frequency specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->Tobs == -1)
    {
      fprintf(stderr,"No frame duration specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
    }
  if(CLA->Tinj == -1)
  {
      fprintf(stderr,"No injection duration specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
  }
  if(CLA->ra == -1)
  {
      fprintf(stderr,"No injection right ascention specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
  }
  if(CLA->dec == -1)
  {
      fprintf(stderr,"No injection declination specified.\n");
      fprintf(stderr,"Try %s -h \n",argv[0]);
      return 1;
  }


  return errflg;
}
