#!/usr/bin/env python

import os
import sys
import re

directory = './results/'

subdirectory = [directory+x for x in os.listdir(directory) if 'h0Hat' in x]

#print subdirectory


resultsfile = open("results_trans_pipelineH0HAT0.6.dat",'w')
resultsfile.write('%% t0_ML[d]  tau_ML[d]     maxTwoF        logBstat      t0_MP[d]      tau_MP[d] Bt0_ML[d]   Btau_ML[d]      BmaxTwoF     BlogBstat     Bt0_MP[d]     Btau_MP[d] STAMPboxSNR     STAMPboxSNRback h0HAT\n')

for i in subdirectory:
    transfile      = [ i+"/"+x for x in os.listdir(i) if 'FstatTransStats.dat' in x ]
    transfile_back = [ i+"/"+x for x in os.listdir(i) if 'FstatTransStatsBACK.dat' in x ]
    stampfile      = [ i+"/"+x for x in os.listdir(i) if 'maxSNR.dat' in x ]
    stampfile_back = [ i+"/"+x for x in os.listdir(i) if 'maxSNRbackground.dat' in x ]
    sub =  i.split('h0Hat')
    h0hat = sub[-1]
    f1 = open(transfile[0]).readlines()
    sf1 = f1[-1].split('\t')
    f2 = open(transfile_back[0]).readlines()
    sf2 = f2[-1].split('\t')
    f3 = open(stampfile[0]).readlines()
    f4 = open(stampfile_back[0]).readlines()
    resultsfile.write(str(sf1[0][101:-1])+"\t"+str(sf2[0][101:-1])+"\t"+str(f3[-1][3:-1])+"\t"+str(f4[-1][3:-1])+"\t"+h0hat+"\n")

resultsfile.close()


sys.exit()


