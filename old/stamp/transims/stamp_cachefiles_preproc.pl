#! /usr/bin/perl
# E. Thrane:  Define the variables at the head of this script.
# STAMP cachefiles will be created in cachefiles/.  If you want to do the
# entire jobfile, set $maxjobs to a very large number.
# S. Giampanis, August 2011 modifications

# location of frames
$FrDir = $ARGV[0];

# sites is a two letter code for the IFO locations.  HL=Hanford-Livingston.
$sites = $ARGV[1]; # e.g. "H";

# frame string indentifier 
$str = $ARGV[2]; # e.g. "H-H1:STRAIN";

# jobfile (same format as stochastic jobfiles)
$jobfile = "pulsarJobfile.txt";

# cachefiles directory
$cache = $ARGV[3]; # e.g. "./cachefiles/"

# Max number of jobs for which you will create cachefiles.
$maxjobs = 10;


$i = 1;
open(jobfile, $jobfile);
while(<jobfile>) {                      # Loop over jobs
    if ($i>$maxjobs) {exit;}

    # Create frameFiles and gpsTime files
    $framefile = "$cache/frameFiles$sites.$i.txt";
    open(framefile,">$framefile");
    $gpsfile = "$cache/gpsTimes$sites.$i.txt";
    open(gpsfile,">$gpsfile");

    # Parse jobfile
    $start = $_;  $stop = $_;
    $start =~ s/ *[0-9]* *([0-9]*) *([0-9]*) *[0-9]* */\1/;
    $stop =~ s/ *[0-9]* *([0-9]*) *([0-9]*) *[0-9]* */\2/;
    chomp($start);  chomp($stop);
    print "$start - $stop\n";

    foreach $file (<$FrDir/$str*>) {
	$gps = $file;
	$gps =~ s/$FrDir\/$str-(.*)-[0-9]*.gwf/\1/;
	print "$gps\n"; 	print "$file\n";
	if ($gps>=$start && $gps<=$stop) {
          # Print data to cachefiles
	  print framefile "$file\n";
	  print gpsfile "$gps\n";  
	}
    }

    # Clean up
    close(framefile);  close(gpsfile);
    $i = $i+1;
}
close(jobfile);
