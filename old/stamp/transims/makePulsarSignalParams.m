function params = makePulsarSignalParams()
%# Parameters for makePulsarSignalParams.m

%# Crab pulsar parameters
params.F0     = 2*29.74654212201602;    %# twice the NS spin frequency
params.F1     = -3.719908752949545e-10; %# spindown (1st derivative)
params.F2     = 1.175816039273666e-20;  %# 2nd derivative
params.PEPOCH = 54632.00000022558;      %# pulsar timing epoch
params.RAJ    = '05:34:31.97232';       %# right ascension
params.DECJ   = '22:00:52.069';         %# declination

%# Octave Utilities directory (octapps)
params.Utilities = '~/octapps/src/pulsar/utilities';

%# Signal parameters                                                                                                                    
params.h0Hat  = 10;     %# rss amplitude (unitless)
params.cosi   = -0.5;  %# inclination angle
params.psi    = 0.3;   %# polarisation angle
params.phi0   = 0.111; %# initial phase
params.sqrtSn = 1e-23; %# noise level (strain)
params.h0     = params.h0Hat * params.sqrtSn; %# rss amplitude (strain)

params.Tsft = 100; %# duration of SFTs (short Fourier transforms)

params.t0 = 800000000;               %# data starting time
params.dur0 = 1000;                  %# duration on noise only data (1st part)
params.t1 = params.t0 + params.dur0; %# transient signal starting time
params.dur1 = 500;                   %# transient signal duration
params.t2 = params.t1 + params.dur1; %# transient signal end time 
params.dur2 = 1000;                  %# duration of noise only data (2nd part)

%# CFSv2 params
%#params.sftdir      = './sftdir'; 
%#params.atoms       = './atoms';
%#params.results     = './results';
params.t0DaysBand  = 0.01;
params.tauDays     = 0.01;
params.tauDaysBand = 0.003;
params.tref        = 897523214 + 0.00000022558*24*3600; %# this should be PEPOCH, check!
