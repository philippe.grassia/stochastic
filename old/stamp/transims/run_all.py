#!/usr/bin/env python

import os

directory = './sftdir/'

ts = [directory+x for x in os.listdir(directory) if 'TimeSeries' in x]

for f in ts:
    time = open(f).readline().split('.')[0]
    os.system('./makePulsarFrames -f %s -i %s -c %s -o %s -s %s -r 200 -m 40 -t 100' % (f, f[-21], f[-21]+'1:STRAIN', './frames/',time))
