#!/usr/bin/env python
# Stefanos Giampanis <gstef@gravity.phys.uwm.edu>

# Example calling of python script:
# ./trans_pipeline.py "/archive/home/gstef/atoms" "/archive/home/gstef/results" "/usr1/gstef"

# Some imports
import os
import sys
import time

# PIPELINE STEPS

######################################## STEP 0 ###########################################
# Pass input arguments, (Note: atoms-files have unique names while results-files don't)
try:
    atoms   = sys.argv[1] # e.g. ./atoms
except Exception:
    print('Need to specify "atoms" directory'), sys.exit()

try:    
    results = sys.argv[2] # e.g. ./results
except Exception:
    print('Need to specify "results" directory'), sys.exit()

try:
    local   = sys.argv[3] # e.g. /usr1/gstef
except Exception:
    print('Need to specify "local" directory'), sys.exit()

try:    
    h0HAT   = sys.argv[4] # input signal strenght (unitless)
except Exception: #print('Using Default input signal amplitude (from param-file)')
    h0HAT = 'DEFAULT'


os.system('hostname -f')
######################################## STEP 1 ###########################################

# Create subdirectory for results (should be under $HOME)
# We are creating a UNIQUE subdirectory since results-files don't have unique names [outdated]
results   = results + "h0Hat" + str(h0HAT) + "/"  
os.system('mkdir -p %s' % results)

# Create temporary directories for SFTs/frames (call it "data") and cachefiles (should be LOCAL to the machine/node)
# Each condor-job should output SFTs and frames in temporary unique directories
tmpstring = str(time.time())
data = local + "/" + "tmpDATA" + tmpstring + "/"
os.system('mkdir -p %s' % data)

cachedir = local + "/" + "tmpCache" + tmpstring + "/"
os.system('mkdir -p %s' % cachedir)

#print atoms, results, data, cachedir


######################################## STEP 2 ###########################################
# makePulsarSigmal.m (octave) -> noise+signal injections, ascii file generation + CFSv2 "sfts", "results" and "atoms"
if h0HAT == 'DEFAULT':
    os.system('./makePulsarSignal.m %s %s %s' % (data,atoms,results))
else:
    os.system('./makePulsarSignal.m %s %s %s %s' % (data,atoms,results,h0HAT))
# step 2: c-code to produce frames
ts = [data+x for x in os.listdir(data) if 'TimeSeries' in x]

for f in ts:
    t = open(f).readline().split('.')[0]
    os.system('./makePulsarFrames -f %s -i %s -c %s -o %s -s %s -r 200 -m 40 -t 100' % (f, f[-21], f[-21]+'1:STRAIN', data,t))


######################################## STEP 3 ###########################################
# Create preproc cachefiles
os.system('./stamp_cachefiles_preproc.pl %s %s %s %s' % (data, "H", "H-H1:STRAIN", cachedir))
os.system('./stamp_cachefiles_preproc.pl %s %s %s %s' % (data, "L", "L-L1:STRAIN", cachedir))


######################################## STEP 4 ###########################################
# create extra set of params to pass in to run_preproc
extra_preproc_parms = cachedir + "extra_preproc_parms.txt"
extraparmsfile      = open(extra_preproc_parms,'w')
extraparmsfile.write("gpsTimesPath1 " + cachedir + " \n")
extraparmsfile.write("gpsTimesPath2 " + cachedir + " \n")
extraparmsfile.write("frameCachePath1 " + cachedir + " \n")
extraparmsfile.write("frameCachePath2 " + cachedir + " \n")
extraparmsfile.write("outputFilePrefix " + data + "S5H1L1")
extraparmsfile.close()

# run_preproc (compiled matlab)
myhome = os.environ.get("HOME")
#os.system('cat %s' % extraparms)
#os.system('mv %s %s/text.txt' % (extraparms,myhome))
#os.system('ls -f %s' % extraparms)
cmd_preproc = 'source %s/MatlabSetup_R2007a_glnxa64.sh && ./run_preproc "pulsarParams.txt" "pulsarJobfile.txt" 1 %s' % (myhome,extra_preproc_parms)
#print cmd_preproc
os.system(cmd_preproc)

# stamp_cachefiles.py
os.system('./stamp_cachefiles_preproc.pl %s %s %s %s' % (data, "HL", "S5H1L1", cachedir))



######################################## STEP 5 ###########################################
# create extra set of params to pass in to run_stochmap
extra_stochmap_parms = cachedir + "extra_stochmap_parms.txt"
extraparmsfile       = open(extra_stochmap_parms,'w')
extraparmsfile.write("intFrameCachePath " + cachedir)
extraparmsfile.close()

# step 6: run_stochmap (compiled matlab)
cmd_stochmap = 'source %s/MatlabSetup_R2007a_glnxa64.sh && ./run_stochmap "pulsarJobfile.txt" 1 %s' % (myhome,extra_stochmap_parms)
os.system(cmd_stochmap)

os.system('cp %s/maxSNR.dat %s' % (cachedir,results))

os.system('cp %s/maxSNRbackground.dat %s' % (cachedir,results))

# Cleaning
os.system('rm -rf %s %s' % (data,cachedir))
