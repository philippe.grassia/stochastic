function cut_col = DQ_cut(map,params)
% function cut_col = DQ_cut(map,params)
% Written by M. Coughlin (coughlim@carleton.edu)
%
% Takes a STAMP map struct and identifies time segments
% which are vetoed by CBC HM vetoes
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% params.DQcut from params file determines which flags are included.

% DQ_flags.mat contains structures start_time and end_time (corresponding to
% the start and end veto flags) for H1, H2, L1, V1 for the CAT categories

load([params.DQmatfile '/DQ_flags.mat']);

% Check if IFO's have CAT flags
ifos_with_veto_segments = {'H1','H2','L1','V1'};

% Check first ifo
if sum(strcmpi(params.ifo1,ifos_with_veto_segments))==0
   fprintf('Warning: no DQ information for %s',params.ifo1);
end

% Check second ifo
if sum(strcmpi(params.ifo2,ifos_with_veto_segments))==0
   fprintf('Warning: no DQ information for %s',params.ifo2);
end

% Segment Times
map_start_times = map.xvals;
map_end_times = map_start_times + map.segDur;

% Loop through the ft-map segment start times and check if they fall within
% veto times. If so, add to array which determines columns to cut

cut_col = zeros(size(map_start_times));

for i=1:length(map.segstarttime)

   isVetoed = 0;

   for j=1:length(params.DQcut)

      % Create veto_segments array which contain the start and stop time of
      % all CAT segments requested
      veto_segments = [];

      if (strcmp(params.ifo1,'H1') || strcmp(params.ifo2,'H1'))

          veto_segments = [veto_segments; H1(params.DQcut(j)).start_time H1(params.DQcut(j)).end_time];

      end

      if (strcmp(params.ifo1,'H2') || strcmp(params.ifo2,'H2'))

          veto_segments = [veto_segments; H2(params.DQcut(j)).start_time H2(params.DQcut(j)).end_time];

      end

      if (strcmp(params.ifo1,'L1') || strcmp(params.ifo2,'L1'))

          veto_segments = [veto_segments; L1(params.DQcut(j)).start_time L1(params.DQcut(j)).end_time];

      end

      if (strcmp(params.ifo1,'V1') || strcmp(params.ifo2,'V1'))

          veto_segments = [veto_segments; V1(params.DQcut(j)).start_time V1(params.DQcut(j)).end_time];

      end

      % Check if the column is vetoed (Astart < Bend & Aend > Bstart)
      isVetoed = isVetoed || any(map_start_times(i) < veto_segments(:,2) & map_end_times(i) > veto_segments(:,1));

   end

   % If the column is vetoed, add its index to the cut column array
   if isVetoed
      cut_col(i) = 1;
   end

end   

return
