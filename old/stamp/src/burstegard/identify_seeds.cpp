#include <iostream>
#include <cmath>
#include <stdlib.h>
#include "cluster.h"

using namespace std;

// INPUTS:
// in_array - vector of numbers.
// seed_vec - vectors of ones and zeros.  Ones are seed pixels.  More of an "output" of the code.
// x        - map size in x direction.
// y        - map size in y direction.
// params   - params struct.
// nSeeds   - total number of seeds.  More of an "output" of the code.

void identify_seeds(double* in_array, int* seed_vec, int x, int y, Params params, int &nSeeds) {

  // LOOP OVER ENTIRE 1D ARRAY
  for (int k = 0; k < (x*y); k++) {
    
    // IF PIXEL SNR IS LARGER THAN THRESHOLD, SET THAT ELEMENT OF SEED_VEC TO 1.
    // OTHERWISE, SET IT TO ZERO.
    if (in_array[k] >= params.snr_threshold) {
      seed_vec[k] = 1;
      nSeeds++;
    }
    else {
      seed_vec[k] = 0;
    }
  }

}
