#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include <map>
#include "cluster.h"

using namespace std;

// DEFINE OTHER FUNCTIONS CALLED FROM THIS FUNCTION.
void expand_cluster(vector<Cluster> &clusters, Seed* seeds, Params params, map<int,int> &seedlist, int seedNum);

// PURPOSE:
// FUNCTION FOR GROUPING PIXELS INTO CLUSTERS.  RETURNS A VECTOR OF ALL CLUSTERS
// FOUND IN THE INPUT MAP.
//
// INPUTS:
// clusterList - a vector of cluster objects.
// seeds       - an array of Seed objections.
// params      - Params object.
// nSeeds      - total number of seeds in the input map. 

void cluster_pixels(vector<Cluster> &clusterList, Seed* seeds, Params params, int nSeeds) {

  //DEFINE A MAP OF ALL SEEDS.
  map<int,int> seed_x;
  for (int i = 0; i < nSeeds; i++) {
    seed_x[i] = i;
  }
  map<int,int>::iterator iter;
  //map<int,int>::iterator iter2;

  // START FINDING CLUSTERS, BEGINNING WITH THE FIRST SEED PIXEL (SEED 0).
  // WITHIN expand_cluster, PIXELS ARE DELETED FROM THE MAP seed_x SO THAT
  // THEY AREN'T USED AS SEEDPOINTS ONCE THEY ARE ALREADY IN A CLUSTER.
  iter = seed_x.begin();
  while (iter != seed_x.end()) {
    if (seed_x.empty()) { break; }

    // DEBUGGING
    /*
      cout << "seed " << iter->first << endl;
      cout << "seeds left before: ";
      for (iter2 = seed_x.begin(); iter2 != seed_x.end(); iter2++) {
        cout << iter2->first << " ";
      }
    */

    expand_cluster(clusterList,seeds,params,seed_x,iter->first);

    // DEBUGGING.
    /*
      cout << "seeds left after: ";
      for (iter2 = seed_x.begin(); iter2 != seed_x.end(); iter2++) {
        cout << iter2->first << " ";
      }
      cout << ": " << iter->first << " ";
      iter2 = seed_x.end();
      cout << iter2->first;
      cout << endl;
    */

    // RESET THE ITERATOR TO THE FIRST ELEMENT OF THE MAP.  THE PREVIOUS
    // FIRST ELEMENT SHOULD BE DELETED FROM THE MAP WITHIN expand_cluster
    // SINCE IT IS ALREADY PART OF A CLUSTER.
    iter = seed_x.begin();
  }

}
