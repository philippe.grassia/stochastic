#include <iostream>
#include <fstream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include <map>
#include <string>
#include "cluster.h"

using namespace std;

// PURPOSE:
// RECONSTRUCT THE ORIGINAL INPUT MAP, EXCEPT WITH ONLY THE MAXIMUM CLUSTER.
// WRITE THE OUTPUT MAP TO A TAB-DELIMITED TEXT FILE.
//
// INPUT:
// maxCluster - Cluster object representing the cluster with the largest SNR.
// seeds      - array of Seed objects.
// x          - size of input (and output) map in x direction.
// y          - size of input (and output) map in y direction.
// filename   - name of output file

void reconstruct_cluster(Cluster &maxCluster, vector<Cluster> &clusters, Seed* seeds, const int x, const int y, double* output_max, double* output_all) {
  
  //double reconMap[y][x];
  //double reconMap[y*x];
  //double* reconMap = new double[x*y];
  for (int i = 0; i < y; i++) {
    for (int j = 0; j < x; j++) {
      //reconMap[i][j] = 0;
      //reconMap[i*x+j] = 0;
      output_max[i*x+j] = 0;
      output_all[i*x+j] = 0;
    }
  }

  Seed tempSeed;
  for (int k = 0; k < maxCluster.seedlist.size(); k++) {
    tempSeed = seeds[maxCluster.seedlist[k]];
    //reconMap[tempSeed.y_loc][tempSeed.x_loc] = tempSeed.snr;
    //reconMap[tempSeed.y_loc*x + tempSeed.x_loc] = tempSeed.snr;
    output_max[tempSeed.y_loc*x + tempSeed.x_loc] = tempSeed.snr;
    output_all[tempSeed.y_loc*x + tempSeed.x_loc] = tempSeed.snr;
    //cout << maxCluster.seedlist[k] << " " << tempSeed.snr << endl;
  }

  for (int i = 0; i < clusters.size(); i++) {

    // DON'T WANT TO ADD MAXCLUSTER AGAIN.
    if (i == maxCluster.clusterIndex) { continue; }

    for (int j = 0; j < clusters[i].seedlist.size(); j++) {
      tempSeed = seeds[clusters[i].seedlist[j]];
      output_all[tempSeed.y_loc*x + tempSeed.x_loc] = tempSeed.snr;
    }
  }

  /*
  ofstream outfile;
  outfile.open(filename.c_str());

   if (outfile.is_open()) {
     for (int i = 0; i < y; i++) {
       for (int j = 0; j < x; j++) {
	 //outfile << reconMap[i][j] << " ";
	 outfile << reconMap[i*x+j] << " ";
       }
       outfile << endl;
     }
     outfile << endl;
   outfile.close();
   }
   else {
     cout << "Unable to open output file." << endl;
   }
  */

  //delete[] reconMap;

}
