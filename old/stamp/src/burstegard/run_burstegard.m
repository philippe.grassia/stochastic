function max_cluster = run_burstegard(map,params)

% Arrange params into a numeric array for passing to cluster code.
paramArray = [params.burstegard.NCN params.burstegard.NR ...
	      params.burstegard.pixelThreshold params.burstegard.tmetric ...
	      params.burstegard.fmetric];

% Run cluster code, assign outputs properly.
[a,b,c,d,e,f] = burstegard(map.snr',map.y',map.sigma',size(map.snr),paramArray);
max_cluster.nPix = a;
max_cluster.snr_gamma = b;
max_cluster.y_gamma = c;
max_cluster.sigma_gamma = d;
max_cluster.reconMax = e';
max_cluster.reconAll = f';

if (params.savePlots)
  printmap(max_cluster.reconMax,map.xvals,map.yvals,'t (s)','f (Hz)','SNR',[-5 ...
		    5],0,0);
  print('-dpng','large_cluster_plot.png');
  printmap(max_cluster.reconAll,map.xvals,map.yvals,'t (s)','f (Hz)','SNR',[-5 ...
		    5],0,0);
  print('-dpng','all_clusters_plot.png');
end

return;