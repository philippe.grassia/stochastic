#include "mex.h"

void tree(double* input_snr, double* input_y, double* input_sigma, double* output_max, double* output_all, int &nPix, double &snr_gamma, double &y_gamma, double &sigma_gamma, const int size_x, const int size_y, int num_pix, double rad, double snr_threshold, double x_metric, double y_metric);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    /* usage: test(M size(M,1), size(M,2)) where M is some matrix */
    /* note: plhs = left-hand side and prhs = right-hand side */

    /* initialize variables */
    double *in_snr;        /* values of input snr array */
    double *in_y;        /* values of input y array */
    double *in_sigma;        /* values of input sigma array */
    int x,y;             /* input matrix is x x y */
    double *t;           /* size of M variable */
    double *paramArray;  /* input array of parameters */
    int num_pix;
    double rad, snr_threshold, x_metric, y_metric;
    
    /* outputs */
    mxArray *X0,*X1,*X2,*X3,*X4,*X5;
    double *a,*b,*c,*d;
    double *out_max;      /* values of output array (max cluster) */
    double *out_all;      /* values of output array (all clusters) */
    int nPix;
    double snr_gamma, y_gamma, sigma_gamma;

    /* determine matrix dimensions */
    t = mxGetPr(prhs[3]);
    /*
    mexPrintf("The matrix has dimensionality %i x %i.\n", (int)t[0], (int)t[1]);
    */
    y = (int)t[0];
    x = (int)t[1];

    /* get params array */
    paramArray = mxGetPr(prhs[4]);
    num_pix = (int)paramArray[0];
    rad = paramArray[1];
    snr_threshold = paramArray[2];
    x_metric = paramArray[3];
    y_metric = paramArray[4];

    mexPrintf("Params: num_pix=%i rad=%1.2f snr_thresh=%1.2f x_met=%1.2f y_met=%1.2f\n",num_pix,rad,snr_threshold,x_metric,y_metric);

    /* retrieve the input data */
    in_snr = mxGetPr(prhs[0]);
    in_y = mxGetPr(prhs[1]);
    in_sigma = mxGetPr(prhs[2]);


    /* prepare other outputs */
    X0 = plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
    X1 = plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);   
    X2 = plhs[2] = mxCreateDoubleMatrix(1, 1, mxREAL);
    X3 = plhs[3] = mxCreateDoubleMatrix(1, 1, mxREAL);
    X4 = plhs[4] = mxCreateDoubleMatrix(x, y, mxREAL);
    X5 = plhs[5] = mxCreateDoubleMatrix(x, y, mxREAL);

    a = mxGetPr(X0);
    b = mxGetPr(X1);
    c = mxGetPr(X2);
    d = mxGetPr(X3);
    out_max = mxGetPr(X4);
    out_all = mxGetPr(X5);

    /* To access the arrays, use linear indexing. */
    /* You can't use [x][y], you have to use [y+x*dimy]. */
    /* Here is an example, which we will over-write below. */
    /*
    d[0] = 17;
    d[2] = 18;
    d[1] = 19;
    d[3] = 20;
    */

    tree(in_snr,in_y,in_sigma,out_max,out_all,nPix,snr_gamma,y_gamma,sigma_gamma,x,y,num_pix,rad,snr_threshold,x_metric,y_metric);

    *a = nPix;
    *b = snr_gamma;
    *c = y_gamma;
    *d = sigma_gamma;

    /*mexPrintf("nPix = %i\nsnr_gamma = %f\n", nPix, snr_gamma);*/

    /* print an element of the array */
    /*
    mexPrintf("data0 =  %g\n", data[0]);
    mexPrintf("data1 =  %g\n", data[1]);
    mexPrintf("data2 =  %g\n", data[2]);
    mexPrintf("data3 =  %g\n", data[3]);
    */
    /* fill output matrix with input matrix minus one */
    /*    int i,j;
    for (i=0; i<y; i++) {
        for (j=0; j<x; j++) {
            d[i*x+j] = data[i*x+j]*2;
        }
    }
    */
    
}
