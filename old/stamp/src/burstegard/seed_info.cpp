#include <iostream>
#include <cmath>
#include <stdlib.h>
#include "cluster.h"

using namespace std;

void seed_info(double* snr_vec, double* y_vec, double* sigma_vec, int* seed_vec, Seed* seeds, int x, int y, int nSeeds) {

  int n = 0;
  for (int k = 0; k < (x*y); k++) {
      if (seed_vec[k] == 1) {
	seeds[n].num = n;
	seeds[n].x_loc = k % x;
	seeds[n].y_loc = k / x;
	seeds[n].snr = snr_vec[k];
	seeds[n].y = y_vec[k];
	seeds[n].sigma = sigma_vec[k];
	n++;
      }
  }

}
