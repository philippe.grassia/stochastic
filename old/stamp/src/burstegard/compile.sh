#! /usr/bin/env bash
rm -f *.o

g++ -fPIC -c analyze_clusters.cpp -o analyze_clusters.o
g++ -fPIC -c expand_cluster.cpp -o expand_cluster.o
g++ -fPIC -c calc_dists.cpp -o calc_dists.o
g++ -fPIC -c reconstruct_cluster.cpp -o reconstruct_cluster.o
g++ -fPIC -c seed_info.cpp -o seed_info.o
g++ -fPIC -c identify_seeds.cpp -o identify_seeds.o
g++ -fPIC -c cluster_pixels.cpp -o cluster_pixels.o
g++ -fPIC -c tree.cpp -o tree.o

ar rcs libburstegard.a *.o

echo "LIBRARY COMPILED."

rm -f *.o

#g++ run_tree.cpp libburstegard.a -o run_tree
#echo "C EXECUTABLES COMPILED."

# MEX
mex burstegard.cpp ./libburstegard.a
mex burstegard.cpp ./libburstegard.a

echo "MEXED."

# EOF