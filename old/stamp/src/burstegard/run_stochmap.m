function stoch_out = run_stochmap(job);
% function stoch_out = run_stochmap(job);
% E. Thrane
% This code is designed to do condor batch sensitivity studies. Make sure to
% set the str variable to an appropriate output filename before compiling.
% Note that diagnostic information such as plots and maps are not saved for 
% each job; rather a mat file with stoch_out is saved.

% output file prefix
str ='adi_D';

stoch_out.start=true;
job = strassign(job);
if job==0
  return;
end

% initialize default parameters
params = stampDefaults;

% local cachefiles (aLIGO noise)
params.intFrameCachePath = '/archive/home/ethrane/iligo_mc/cachefiles/';
% S5 jobfile
jobsFile='/home/ethrane/sensitivity/S5H1L1_full_run.txt';

% interferometer pair
params.ifo1='H1';
params.ifo2='L1';

% search direction-------------------------------------------------------------
params.ra=6;   %right ascenion in hours (for search direction)
params.dec=30; %declination in degrees (for search direction)

% frequency range--------------------------------------------------------------
params.fmin = 40;
params.fmax = 500;

% override default parameters--------------------------------------------------
%params.saveMat=true;
%params.savePlots=true;
params.saveMat=false;
params.savePlots=false;


% notches
params.doStampFreqMask=false;

% auto-power
params.Autopower=true;
%params.yMapScale = 5e-45;
%params.fluence=1;
%params.FMapScale=0.05;

% loud pixel diagnostic
params.loudPixel=true;

% burstCluster params
params.doClusterSearch=true;
%params = clusterDefaults_eht(params);
params.cluster.NCN = 80;
params.cluster.NR = 2;
params.cluster.pixelThreshold = 0.75;
params.cluster.tmetric = 1;
params.cluster.fmetric = 1;
%params.glitchCut = 5;

% power injection
params.powerinj = true;
params.injfile = '/home/ethrane/sensitivity/adi_D.mat';
params.inj_trials=1;
%params.alpha_n=1;
%params.alpha_min=5e-7;
%params.alpha_max=params.alpha_min;
params.alpha_n=12;
params.alpha_min=0;
params.alpha_max=5e-7;

% fixed sensitivity
params.fixAntennaFactors = true;
fprintf('params.fixAntennaFactors = %i\n', params.fixAntennaFactors);

% make sure there is enough time to do the injection
dur = 271;
f = load(jobsFile);
start = f(job, 2) + 10;
stop = start + dur;
job_end = f(job, 3);
if job_end>stop
  % call stochMap--------------------------------------------------------------
  stoch_out=stochmap(params, jobsFile, start, stop);
else
  fprintf('not enough time for injection...quitting\n.');
  return;
end

% save output files
save(['/usr1/ethrane/' str '_' num2str(job) '.mat'], 'stoch_out');
%save(['/usr1/prestegard/' str '_' num2str(job) '.mat'], 'stoch_out');
