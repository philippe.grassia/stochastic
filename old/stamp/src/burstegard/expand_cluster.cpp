#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include <map>
#include "cluster.h"

using namespace std;

// DEFINE OTHER FUNCTIONS CALLED FROM THIS FUNCTION.
double calc_dists(Seed seed1, Seed seed2, Params params);

// PURPOSE:
// TAKES AN INPUT SEED (SEEDPOINT) AND EXPANDS A CLUSTER AROUND IT.
// ADDS FINAL CLUSTER TO clusters VECTOR.
//
// INPUT:
// clusters - a vector of Cluster objects.  Will have Clusters added to it by this function.
// seeds    - an array of Seed objects.  Used in calc_dists.
// params   - Params object.
// seed_x   - a map of seeds to be clustered to a seedpoint.  Once a seed is clustered, it is removed from seed_x.
// seedNum  - the number (Seed.num) of the original seedpoint passed to this function by cluster_pixels.

void expand_cluster(vector<Cluster> &clusters, Seed* seeds, Params params, map<int,int> &seed_x, int seedNum) {

  // ERASE THE CURRENT SEED PIXEL FROM seed_x.
  seed_x.erase(seedNum);
  
  // DEFINE A TEMPORARY Cluster OBJECT AND RESERVE SPACE FOR IT.
  // ADD THE CURRENT SEEDPOINT TO THIS CLUSTER.
  Cluster tempCluster;
  tempCluster.seedlist.reserve(seed_x.size());
  tempCluster.seedlist.push_back(seedNum);

  map<int,int>::iterator mapiter;

  // EXPAND A CLUSTER - LOOP OVER CLUSTERED PIXELS.
  double distance;
  
  // TRY TO CLUSTER ALL PIXELS CURRENTLY IN THE CLUSTER WITH OTHER PIXELS IN THE MAP,
  // ONE AT A TIME.  THE CLUSTER STARTS OUT WITH ONLY THE SEEDPOINT IN IT, BUT CAN 
  // ADD MORE PIXELS TO THE CLUSTER WITHIN THE LOOP.
  for (int i = 0; i < tempCluster.seedlist.size(); i++) {
    //cout << "\nSeed #" << tempCluster.seedlist[i] << endl;

    mapiter = seed_x.begin();
    while (mapiter != seed_x.end()) {

      // DON'T CALCULATE SELF-DISTANCE.
      if (mapiter->first == seedNum) { continue; }

      // CALCULATE DISTANCE BETWEEN CURRENT SEEDPOINT AND ANOTHER PIXEL (FROM seed_x).
      distance = calc_dists(seeds[tempCluster.seedlist[i]],seeds[mapiter->first],params);

      // DEBUGGING.
      /*
	cout << "Match #" << mapiter->first << ": ";
	cout << tempCluster.seedlist[i] << " " << mapiter->first << " " << distance << endl;
	cout << seeds[tempCluster.seedlist[i]].x_loc << seeds[tempCluster.seedlist[i]].y_loc << " " << seeds[mapiter->first].x_loc << seeds[mapiter->first].y_loc << endl;
      */

      // IF CALC_DISTS RETURNS -1, WE ARE TOO FAR "DOWN" IN THE MAP TO CLUSTER ANY MORE PIXELS,
      // SINCE WE LOOP OVER SEED PIXELS FROM LEFT -> RIGHT, TOP -> BOTTOM IN THE 2D MAP.
      // SO WE WANT TO QUIT TRYING TO FIND MORE PIXELS TO CLUSTER WITH THIS SEED.
      if (distance == -1 ) { 
	break;
      }

      // IF PIXELS ARE CLOSE ENOUGH TOGETHER, ADD THE PIXEL TO THE CLUSTER.
      // THEN IT WILL BE USED AS A SEEDPOINT IN A LATER ITERATION.
      // WE ALSO REMOVE IT FROM THE LIST OF SEEDS FOR CLUSTERING (seed_x) -
      // BECAUSE IT IS ALREADY PART OF THE CLUSTER WE DON'T WANT TO
      // TRY TO CLUSTER IT WITH OTHER PIXELS IN THE CLUSTER AGAIN.
      if (distance <= params.rad && distance > 0) {
	tempCluster.seedlist.push_back(mapiter->second);
	//cout << "erasing " << mapiter->first << endl;
	seed_x.erase(mapiter++);
      }
      else {
	++mapiter;
      }
    }
  }

  // AT THIS POINT, CLUSTERING IS COMPLETE FOR A SET OF PIXELS.
  // IF THE CLUSTER CONTAINS ENOUGH PIXELS, WE ADD IT TO THE 
  if (tempCluster.seedlist.size() >= params.num_pix) {
    tempCluster.nPix = tempCluster.seedlist.size();
    clusters.push_back(tempCluster);
  }

}
