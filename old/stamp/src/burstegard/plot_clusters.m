function plot_clusters()

segDur = 1;
fmin = 40;

dirname = 'eric_recovery';
files = dir(dirname);
filenames = {files.name};

all_arrays = [];

n = 1;
for i=1:length(filenames)
  test = regexpi(filenames{i},'cluster');

  if ( isempty(test) )
    continue;
  end

  fname = sprintf('%s/%s',dirname,filenames{i});
  all_arrays(:,:,n) = load(fname);

  fprintf('%i ',n);
  n = n + 1;
end
fprintf('\n');

times = (segDur/2):(segDur/2):(size(all_arrays(:,:,1),2)*segDur/2);
times = times-(segDur/2/2);
freqs = fmin:(1/segDur):(size(all_arrays(:,:,1),1)/segDur+fmin - 1);

markers = {'ro', 'go', 'bo', 'co', 'mo', 'ko', ...
	   'rs', 'gs', 'bs', 'cs', 'ms', 'ks', ...
	   'rd', 'gd', 'bd', 'cd', 'md', 'kd', ...
	   'rv', 'gv', 'bv', 'cv', 'mv', 'kv', ...
	   'r^', 'g^', 'b^', 'c^', 'm^', 'k^', ...
	   'r<', 'g<', 'b<', 'c<', 'm<', 'k<', ...
	   'r>', 'g>', 'b>', 'c>', 'm>', 'k>', ...
	   'rp', 'gp', 'bp', 'cp', 'mp', 'kp', ...
	   'rh', 'gh', 'bh', 'ch', 'mh', 'kh'};
figure;
axis([times(1) times(end) freqs(1) freqs(end)]);
xlabel('t (s)');
ylabel('f (Hz)');
hold on;

for i=1:size(all_arrays,3)
  fprintf('%i ',i);
  [r,c] = find(all_arrays(:,:,i) > 0);
  handle = plot(times(c), freqs(r), markers{mod(i,length(markers))+1});
  set(handle, 'MarkerSize', 10);
  set(handle, 'MarkerFaceColor', get(handle, 'Color'));
end
fprintf('\n');

hold off;
pretty;

title(['All clusters, number of clusters = ' ...
       num2str(size(all_arrays,3))], 'FontSize', 20)

print('-dpng','eric_all_clusters3.png')
!ftwp *.png;



return;