#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <vector>
#include <map>
#include <string>
#include "cluster.h"

using namespace std;

// DECLARE FUNCTIONS FROM OTHER .cpp FILES.
void identify_seeds(double* in_array, int* seed_vec, int x, int y, Params params, int &nSeeds);
void seed_info(double* snr_vec, double* y_vec, double* sigma_vec, int* seed_vec, Seed* seeds, int x, int y, int nSeeds);
double calc_dists(Seed seed1, Seed seed2, Params params);
void cluster_pixels(vector<Cluster> &clusterList, Seed* seeds, Params params, int nSeeds);
void expand_cluster(vector<Cluster> &clusters, Seed* seeds, Params params, map<int,int> &seed_x, int seedNum);
void analyze_clusters(vector<Cluster> &clusters, Seed* seeds, Params params, Cluster &maxCluster);
void reconstruct_cluster(Cluster &maxCluster, vector<Cluster> &clusters, Seed* seeds, const int x, const int y, double* output_max, double* output_all);

// PURPOSE:
// CLUSTER PIXELS IN A 2D MAP OF SIZE size_x BY size_y.  THIS MAP
// HAS BEEN CONVERTED TO 1D ARRAY BEFORE BEING PASSED TO THIS FUNCTION.
//
// INPUT:
// params   - Params object, set in wrapper script.
// out_file - name of output file (where reconstructed array will be written).
// input_1d - pointer to 1D array.
// size_x   - size of original 2D array in x direction.
// size_y   - size of original 2D array in y direction.

// START MAIN FUNCTION
void tree(double* input_snr, double* input_y, double* input_sigma, double* output_max, double* output_all, int &nPix, double &snr_gamma, double &y_gamma, double &sigma_gamma, const int size_x, const int size_y, int num_pix, double rad, double snr_threshold, double x_metric, double y_metric) {

  Params params;
  params.set_values(num_pix,rad,snr_threshold,x_metric,y_metric);

  srand(time(NULL)); // SEED RANDOM NUMBER GENERATOR (USED IF MAKING A RANDOM INPUT MAP).
  int debug = 0; // SET TO 1 OR 2 FOR DIFFERENT LEVELS OF DEBUG INFO.

  // IDENTIFY SEED PIXELS, TOTAL NUMBER.
  int nSeeds = 0;
  int seed_vec[size_x*size_y];
  identify_seeds(input_snr,seed_vec,size_x,size_y,params,nSeeds);
//  cout << "GOT THE SEEDS" << endl;

  //PRINT INPUT FOR DEBUGGING.
  if (debug >= 1) {
    for (int i = 0; i < size_y; i++) {
      for (int j = 0; j < size_x; j++) {
	//cout << fixed << setprecision(2) << i << j << " ";
      }
      cout << "\t";
      for (int j = 0; j < size_x; j++) {
	cout << seed_vec[i*size_x + j] << " ";
      }
      cout << endl;
    }
    cout << endl;
  }

  // DEFINE ARRAY OF Seed OBJECTS.
  Seed all_seeds[nSeeds];

  // ASSIGN SEED PROPERTIES TO SEED OBJECT.
  seed_info(input_snr,input_y,input_sigma,seed_vec,all_seeds,size_x,size_y,nSeeds);
//  cout << "GOT SEED INFORMATION" << endl;

  // PRINTING FOR DEBUG PURPOSES
  if (debug >= 2) {
    for (int k = 0; k < size_x*size_y; k++) {
      cout << k << " " << k%size_x << k/size_x << " " << input_snr[k] << " " << seed_vec[k] << endl;
    }
    cout << endl;
    
    cout << "seed #\t" << "xy\t" << "val" << endl;  
    for (int i = 0; i < nSeeds; i++) {
      cout << all_seeds[i].num << "\t" << all_seeds[i].x_loc << all_seeds[i].y_loc << "\t" << all_seeds[i].snr << endl;
    }
  }

  // DEFINE A VECTOR OF CLUSTERS.
  vector<Cluster> allClusters;

  // MAX. # OF CLUSTERS = # OF SEEDS / # OF PIXELS REQ. FOR A CLUSTER.
  int nClustersMax = nSeeds/params.num_pix;
  allClusters.reserve(nClustersMax); // RESERVE MEMORY FOR THIS VECTOR.

  // CLUSTER PIXELS.
  cluster_pixels(allClusters,all_seeds,params,nSeeds);
//  cout << "GOT CLUSTERS" << endl;

  // IF NO CLUSTERS FOUND:
  if (allClusters.size() == 0) {
    cout << "No significant clusters found." << endl;
    
    for (int i = 0; i < size_x*size_y; i++) {
      output_max[i] = 0;
    }
    nPix = 0;
    snr_gamma = 0;
    y_gamma = 0;
    sigma_gamma = 0;
    return;
  }
  
  // CALCULATE SNR_GAMMA OF ALL CLUSTERS AND FIND CLUSTER WITH HIGHEST SNR_GAMMA.
  Cluster maxCluster;
  analyze_clusters(allClusters,all_seeds,params,maxCluster);
//  cout << "CLUSTERS ANALYZED, GOT MAX CLUSTER" << endl;

  // PRINT CLUSTER INFO (PIXELS + SNR).
  if (debug >= 1) {
    for (int i = 0; i < allClusters.size(); i++) {
      Cluster temp1 = allClusters[i];
      cout << "Cluster " << i << ": SNR = " << temp1.snr_gamma << " NPix = " << temp1.nPix << "\n\t";
      for (int j = 0; j < temp1.seedlist.size(); j++) {
	cout << temp1.seedlist.at(j) << " ";
      }
      cout << endl;
    }
  }

  // OUTPUT OVERALL CLUSTERING RESULTS.
  cout << "Clusters found: " << allClusters.size() << endl;
  cout << "Max cluster is cluster " << maxCluster.clusterIndex<< "." << endl;

  // MAKE A RECONSTRUCTION OF THE MAXIMUM CLUSTER IN A 2D MAP AND OUTPUT IT TO A FILE.
  //reconstruct_cluster(maxCluster,all_seeds,size_x,size_y,out_file);
  reconstruct_cluster(maxCluster,allClusters,all_seeds,size_x,size_y,output_max,output_all);

  // ASSIGN VALUES TO OTHER OUTPUTS.
  nPix = maxCluster.nPix;
  snr_gamma = maxCluster.snr_gamma;
  y_gamma = maxCluster.y_gamma;
  sigma_gamma = maxCluster.sigma_gamma;

  /*
  if (params.save_all_clusters) {
    string out_pref = out_file.substr(0, out_file.size()-4);
    string tempfile;
    stringstream ss;

    cout << "SAVING ALL CLUSTERS" << endl;
    for (int i = 0; i < allClusters.size(); i++) {
      ss << out_pref << "_cluster" << i << ".dat";
      ss >> tempfile;
      
      cout << i << " " << flush;

      reconstruct_cluster(allClusters[i],all_seeds,size_x,size_y,tempfile);
      ss.clear();

    }
    cout << endl;
  }
  */

} // END OF TREE.CPP

