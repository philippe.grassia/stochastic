function cut_col = Xi_cut(map,params)
% function cut_col = Xi_cut(map)
% Written by T. Prestegard (prestegard@physics.umn.edu)
%
% Takes a STAMP map struct and identifies time segments
% where the autopower in the two detectors is not consistent
% with noise + a GW signal.
%
% For additional documentation, see
% https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=71680
% Specifically, refer to Eqs. 23 and 24.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% PARAMETERS
% Define "glitch-like" Xi SNR range.
cut_low = 0.95;
cut_high = 1.05;

% Fraction of Xi pixels that must be in that range to identify
% a column as glitch-like.
Xi_frac = 0.027;

% Autopower stationarity ratio must exceed this value for a
% column to be identified as glitch-like.
AP_cut = 2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find "hot" columns in autopower in each detector.
% AP_det1 is the autopower stationarity ratio for detector 1 (see Eq. 22).
% Averages over frequency.
AP_det1 = mean(map.naiP1./map.P1); AP_cols_det1 = AP_det1 > AP_cut;
AP_det2 = mean(map.naiP2./map.P2); AP_cols_det2 = AP_det2 > AP_cut;

% Find "hot" positive or negative columns in Xi_snr.
cut_map_pos = ( (map.Xi_snr > cut_low) & (map.Xi_snr < cut_high) );
cols_pos = sum(cut_map_pos)/length(cut_map_pos(:,1));
Xi_cols_pos = cols_pos > Xi_frac;

cut_map_neg = ( (map.Xi_snr < -cut_low) & (map.Xi_snr > -cut_high) );
cols_neg = sum(cut_map_neg)/length(cut_map_neg(:,1));
Xi_cols_neg = cols_neg > Xi_frac;

% Positive Xi SNR indicates a glitch in det1.
cut_det1 = Xi_cols_pos & AP_cols_det1 & ~AP_cols_det2;

% Negative Xi SNR indicates a glitch in det2.
cut_det2 = Xi_cols_neg & AP_cols_det2 & ~AP_cols_det1;

% Combine results from each detector to obtain all columns that
% have been identified as glitch-like.
cut_col = (cut_det1 | cut_det2);

% Coincident checks.
if (params.doCoincidentCut)
  Xi_cols_pos_co = cols_pos > Xi_frac*2;
  Xi_cols_neg_co = cols_neg > Xi_frac*2;
  cut_both1 = ((Xi_cols_pos_co & AP_cols_det1) | (Xi_cols_neg_co & AP_cols_det2));
  cut_both2 = Xi_cols_pos & AP_cols_det1 & Xi_cols_neg & AP_cols_det2;

  cut_col = (cut_col | cut_both1);
end


return