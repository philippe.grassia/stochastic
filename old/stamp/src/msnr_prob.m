function prob = msnr_prob(maxsnr, T, varargin)
  % E. Thrane on Nov 18, 2010
  % msnr_prob(maxsnr, T, varargin)
  % T is the number of trials, maxsnr is the measured max SNR
  % prob = is the confidence with with which we can reject the null H.
  % Small values of prob mean we can strongly reject the null H (noise model
  % fails).
  % if verbose==true, diagnostic plots will be generated
  verbose = 0;  % set default
  if length(varargin)>0
    verbose = varargin{1};
  end

  da = 0.00001;
  a = -5:da:10;
  mpdf = normcdf(a).^(T-1) .* exp(-a.^2/2);
  norm = sum(mpdf);
  mpdf = mpdf/norm;

  mcdf = cumsum(mpdf);

  q = a( abs(a-maxsnr) == min(abs(a-maxsnr)) );
  prob = 1 - mcdf(a==q);

  if verbose
    figure;
    subplot(2,1,1);
    plot(a, mpdf, 'b');
    hold on;
    plot(q, mpdf(a==q), 'ro');
    ylabel('PDF');
    subplot(2,1,2);
    plot(a, mcdf);
    hold on;
    plot(q, mcdf(a==q), 'ro');
    ylabel('CDF');
    print('-djpeg', 'msnr_prob.jpg');

  end

end
