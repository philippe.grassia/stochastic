function source = brutepixel(thetamax, dtheta, params)
% function source = brutepixel(thetamax, dtheta, params)
% E. Thrane
% thetamax is the maximum angular separation in degrees.
% dtheta is the grid spacing in degrees
% params is the parameter struct.
% Returns an array of (ra, dec) values that fall inside a search
% cone centered on params.ra and params.dec with a radius of 
% thetamax (degrees) and an angular separation of dtheta (degrees).
%

% Factor for deg. -> rad. conversion.
k = pi/180;

% Initial search direction.
ra0 = params.ra*15; % Convert ra from hours to degrees.
dec0 = params.dec;

% Define a unit vector in the original search direction.
r0 = [-cos(k*dec0)*sin(k*ra0) sin(k*dec0)*sin(k*ra0) cos(k*ra0)];

% Preallocate space for the source array (increases speed).
% Each array element is set to -1.
source = zeros(floor(360/dtheta)*(floor(180/dtheta)+1),2) - 1;

% Loop over all possible search directions with resolution 'dtheta'.
counter = 1;
for ra = 0:dtheta:(360-dtheta)
  for dec = -90:dtheta:90
    
    % Define a unit vector in this search direction.
    r = [-sin(k*ra)*cos(k*dec) sin(k*ra)*sin(k*dec) cos(k*ra)];

    % Use dot product of r and r0 to determine angle between them.
    angsep = acos(r*r0');

    % Store directions that are within 'thetamax' of the
    % original search direction.
    if ( angsep <= (thetamax*k) )
      counter = counter + 1;
      source(counter,:) = [ra/15 dec];
    end

  end
end

% Find the stored sky locations from the loop and
% send them to the final output (source).
sl = (source(:,1) >= 0);
source = source(sl,:);

return
