function map = clean_maps(map, badindex)
% function map = clean_maps(map)
% clean_maps fills the map struct with 0's and ones at values of badindex,
% thereby replacing values of NaN.  This is used by stochmap in order to test
% if there are any unexpected NaN's.
% E. Thrane
  map.sigma(badindex,:) = 1;
  map.y(badindex,:) = 0;
  map.z(badindex,:) = 0;
  map.sigF(badindex,:) = 1e99;
  map.F(badindex,:) = 0;

  % check for unexpected isNaN's.  Expected isNaN's from vetos or missing 
  % science data are handled later in glitchcut and addMisingDatatoMap 
  % respectively.
  if any(isnan(map.sigma)), error('Unexpected isnan in map.sigma'); end
  if any(isnan(map.y)), error('Unexpected isnan in map.y'); end
  if any(isnan(map.z)), error('Unexpected isnan in map.z'); end
  if any(isnan(map.sigF)), error('Unexpected isnan in map.sigF'); end
  if any(isnan(map.F)), error('Unexpected isnan in map.F'); end

return
