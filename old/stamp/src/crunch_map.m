function map_out = crunch_map(map)
% function map_out = crunch_map(map)
% E. Thrane
% remove rows and columns of NaN's to make a smaller array.

% copy over generic metadata
map_out = map;

% define cuts for NaN in f- and t- directions
fcut = ~(sum(isnan(map.snr),2)==size(map.snr,2));
tcut = ~(sum(isnan(map.snr),1)==size(map.snr,1));

% list of all ft-maps
maps = {'cc', 'sensInt', 'naiP1', 'naiP2', 'P1', 'P2', 'y', 'z', 'sigma', ...
  'eps_12', 'eps_11', 'eps_22', 'F', 'sigF', 'snr', 'snrz', 'Xi', ...
  'sigma_Xi', 'sigma_Xi', 'Xi_snr'};
% list of all f-vectors
fvecs = {'yvals'};
% list of all t-vectors
tvecs = {'segstarttime', 'ccVar', 'xvals'};

% now crop everything
for ii=1:length(maps);
  map_out.(maps{ii}) = map.(maps{ii})(fcut,tcut);
end

for jj=1:length(fvecs);
  map_out.(fvecs{jj}) = map.(fvecs{jj})(fcut);
end

for kk=1:length(tvecs);
  map_out.(tvecs{kk}) = map.(tvecs{kk})(tcut);
end

return;
