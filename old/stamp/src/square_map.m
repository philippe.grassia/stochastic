function map_data = square_map(map_data, fill)
%function map_data = square_map(map_data, fill)
% fill should be 0 or 1

x = size(map_data,1);
y = size(map_data,2);
if x>y
  map_data = [map_data fill*ones(x, x-y) ];
elseif x<y
  map_data = [map_data ; fill*ones(y-x, y)];
end


