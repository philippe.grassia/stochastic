function quickplots(map);
% function mkplots(map);
% This function is just like saveplots except it can be called outside of 
% stochmap to make plots associated with a saved mat file.

% set default parameters
params = stampDefaults;

printmap(map.snr, map.xvals, map.yvals, 'time (s)', 'f (Hz)', 'SNR', [-5 5]);
print('-dpng','snr.png');
print('-depsc2','snr.eps');

ymax = max(abs(map.y(:)));
printmap(map.y, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'y (strain^2)', [-ymax ymax]);
print('-dpng','y_map.png');
print('-depsc2','y_map.eps');

p1max = max(abs(map.P1(:)));
p1min = min(abs(map.P1(:)));
printmap(map.P1, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'y (strain^2)', [p1min p1max]);
print('-dpng','p1_map.png');

p2max = max(abs(map.P2(:)));
p2min = min(abs(map.P2(:)));
printmap(map.P2, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'y (strain^2)', [p2min p2max]);
print('-dpng','p2_map.png');

naip1max = max(abs(map.naiP1(:)));
naip1min = min(abs(map.naiP1(:)));
printmap(map.naiP1, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'y (strain^2)', [naip1min naip1max]);
print('-dpng','naip1_map.png');

naip2max = max(abs(map.naiP2(:)));
naip2min = min(abs(map.naiP2(:)));
printmap(map.naiP2, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'y (strain^2)', [naip2min naip2max]);
print('-dpng','naip2_map.png');

eps12max = max(abs(map.eps_12(:)));
printmap(map.eps_12, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'y (strain^2)', [-eps12max eps12max]);
print('-dpng','eps12_map.png');

eps11max = max(abs(map.eps_11(:)));
printmap(map.eps_11, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'y (strain^2)', [-eps11max eps11max]);
print('-dpng','eps11_map.png');

eps22max = max(abs(map.eps_22(:)));
printmap(map.eps_22, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
  'y (strain^2)', [-eps22max eps22max]);
print('-dpng','eps22_map.png');

return;
