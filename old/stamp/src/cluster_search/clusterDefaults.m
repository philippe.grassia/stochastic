function params = clusterDefaults(params)
% function params = clusterDefaults(params)
% Sets parameters for cluster search.  Eric: this file was last modified on
% March 14.  These values represent our current best guess for the most
% sensitive values for LGRB signals in 1s x 1Hz maps.  Thus users should be %
% ware: these values are only "defaults" for a particular analysis.
%
% Routine written by shivaraj kandhasamy
% Contact shivaraj@lphysics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters and their values
params.cluster.doCombineCluster = true;
params.cluster.NN = 15;    % minimum number of neighbours
params.cluster.NR = 4;     % neighbor radius as determined by metric
% minimum number of pixels in a cluster
%params.cluster.NCN = 3*params.cluster.NN;
params.cluster.NCN = 80;
% distance between
%params.cluster.NCR = 2*params.cluster.NR;
params.cluster.NCR = 4*params.cluster.NR;
%neighbouring clusters
params.cluster.pixelThreshold = 0.75;
params.cluster.tmetric = 2;
params.cluster.fmetric = 1;

return;
