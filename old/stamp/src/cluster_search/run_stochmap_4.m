clear all;
fr_path='/archive/home/mcoughlin/IM/GRB/S5H1L1_4s_shift_ftmaps/';

%im_setDefaults; %Set the narrow band search defaults
params = stampDefaults;

params.intFrameCachePath= [fr_path 'Example_Runs/'];
jobsFile=[fr_path 'jobfile.txt'];

% Search direction-------------------------------------------------------------
params.ra = 6;   %right ascenion in hours (for search direction)
params.dec = 30; %declination in degrees (for search direction)

% Frequency range--------------------------------------------------------------
params.fmin = 100;
params.fmax = 250;

% parameters related to cluster search-----------------------------------------
params = clusterDefaults(params);

% Override default parameters--------------------------------------------------
params.debug = false;
params.savePlots = true;

% Call stochMap----------------------------------------------------------------
stochmap(params, jobsFile, 869996000, 869996041);
