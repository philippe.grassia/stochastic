function distance = clusterDistancestamp(tiles, tcoeff, fcoeff)
% function distance = clusterDistancestamp(tiles, tcoeff, fcoeff)
%           calculate distance between tiles using specified metric.
% INPUT:
%      tiles   -> matrix with columns of times, frequencies, snr, y, sigma
%      tcoeff  -> metric multiplier for times
%      fcoeff  -> metric multiplier for frequencies
% OUTPUT:
%      distance   <- distance between tiles as defined by metric
%
% Routine written by shivaraj Kandhasamy (based on the code by Peter Kalmus)
% Contact shivaraj@physics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning off;
distance = pdist(tiles, @burstMetric);
warning on;

% function used inside the code
function Y = burstMetric(tiles1, tiles2)
  t1 = tiles1(:, 1);
  f1 = tiles1(:, 2);

  t2 = tiles2(:, 1);
  f2 = tiles2(:, 2);

  % note:  the lower the coefficient, the more the cluster goes in that
  % direction
  df = fcoeff*(f1 - f2); % fcoeff = \beta
  dt = tcoeff*(t1 - t2); % tcoeff = \alpha

   Y = sqrt(dt.^2 + df.^2);  
end

return;
end
