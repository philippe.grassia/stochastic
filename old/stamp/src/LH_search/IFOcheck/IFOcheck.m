function IFOcheck(gpsminus,gpsplus)
% This application cheks if there are available time segments from different 
% detectors around a specified time interval of a given GRB time. 
%
%
% Written by Banto Balazs
% E-mail: bantobalazs@yahoo.com
% Last updated: 29.06.2011.
% Inputs: gpsminus= time window duration BEFORE the GRB time;
%         gpsminus= time window duration AFTER the GRB time;
% Output: a .txt file that contains the name of the GRBs and the corresponding
%         detectors (denoted by H1, L1, V1) for which data are fully available within
%         the specified time window around the GRB time 


% Reading in 'gps.txt', which contains two columns. The first column are the GRBs names and
% the second column are the GPS times of the GRBs. 'gps.txt' must be within the same directory
% from which the IFOcheck script is compiled.
[grbname,grbgps]=textread('gpst.txt','%s%f');

% Loading the segment GPS times data for the H1, L1, V1 interferometers. The three data files
% must be within the same directory from which IFOcheck is compiled.
load H1segments.txt;
load L1segments.txt;
load V1segments.txt;

% Setting the starting ('grbstart') and end ('grbend') GPS time of the time window.
grbstart=grbgps-gpsminus;
grbend=grbgps+gpsplus;

% Loading column 2 from the segment data files, which gives the starting time of the segment,
% and column 3 representing the end GPS time of the segment.
H1start= H1segments(:,2);
H1end= H1segments(:,3);
L1start= L1segments(:,2);
L1end= L1segments(:,3);
V1start= V1segments(:,2);
V1end= V1segments(:,3);

% % The number of GRBs, that have at least two detectors with available data, is stored as 'p'.
% % This GRB count function is optional, should be left commented if it is not necessary.
% p=0;

% Defining the output file name, and opening the file for writing.
FileName=sprintf('detectors_%i_%i.txt',gpsminus,gpsplus);
fid=fopen(FileName,'w');

% Defining string variables to be filled with detector availability data.
responseH1='  ';
responseL1='  ';
responseV1='  ';


% This is the main cycle of the code. 
% Checking time segment availability 
for k=1:length(grbgps)
   
    for h=1:length(H1start) 
   	
      if grbstart(k)>=H1start(h) && grbend(k)<=H1end(h)
          responseH1='H1';end
    end

    for l=1:length(L1start)
          
      if grbstart(k)>=L1start(l) && grbend(k)<=L1end(l)
         responseL1='L1';end

    end

    for v=1:length(V1start)
          
      if grbstart(k)>=V1start(v) && grbend(k)<=V1end(v)
         responseV1='V1';end

    end

    % Printing data avalibility info to output file.
    fprintf(fid, '%s %s%s%s\n',grbname{k}, responseH1, responseL1, responseV1);


    % % Checking if there are time segments available from at least 2 detectors
    % % (optional, should be left commented)
    % if ((responseH1=='H1' & responseL1=='L1') |  (responseH1=='H1' & %responseV1=='V1')...
    % ... |( responseL1=='L1' & responseV1=='V1'))
    % p=p+1;
    % end

    % Redefining string variables to be filled with detector availability data.
    responseH1='  ';
    responseL1='  ';
    responseV1='  ';

end
fclose(fid);

% % Printing GRB number data to screen (optional)
% p %data from at least two detectors

end


