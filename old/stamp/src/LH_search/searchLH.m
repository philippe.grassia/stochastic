function searchLH(params,map)
% Shell program for running Locust and Hough search algorithms on ft-maps
% produced by the Stochastic Transient Analysis Multi-detector Pipeline
% (STAMP).
%
% Other scripts that are part of the Locust and Hough search package are
% locustSearch.m, houghSearch.m and CCalculator.m . These code files by
% default must be in the same directory.
%
% written by Peter Raffai, Eotvos University (praffai_"at"_bolyai.elte.hu)
%
% Last modified: 13.07.2011.
%
%
% Input:
% - map: structure that contains the ft-maps produced by stochMap
% - params: structure that contains parameters for the Locust and Hough
% search
%
%
% Output:
% The program creates a directory and outputs a
% .mat file for each detection candidate found by the Locust and/or Hough
% algorithms. The output .mat file stores all relevant variables
% corresponding to the detection candidate event trace.


% % Measuring running time with tic...toc
% tic

% Setting detection Threshold parameter for the Locust and Hough algorithms
LocustAlarmThreshold=params.LocustDetectionThreshold;
%alarm threshold for Locust algorithm
HoughAlarmThreshold=params.HoughDetectionThreshold;
%alarm threshold for Hough algorithm

% Initializing ft-map
SNRMap = map.y .* (map.sigma.^-2);

% Setting a threshold for the Locust algorithm at which following a trace
% of local maxima is stopped
% Note: params.LocustCutThreshold should be given as a percentage of how
% many lowest elements should be removed
SNRVect=[];
for i=1:size(SNRMap,1) SNRVect=[SNRVect,SNRMap(i,:)]; end
SNRVect=sort(SNRVect,'ascend');
LocustCutThreshold=SNRVect(1+floor(params.LocustCutThreshold*length(SNRVect)));


% % Getting rid of warning messages (optional)
warning off all;


% Creating output directory
OutputDir = params.OutputDirectory;
mkdir(OutputDir);


% Removing vertial glitches forom the input ft-map.
% Exchanging all (SNRMap(Row,Col)<'LocustCutThreshold') elements
% to SNRMap(Row,Col)=0
CutMode=1;
GlitchCutLevel = params.GlitchCutThreshold;
[LocustMap,Glitches]=LocustGlitchCutter(SNRMap,GlitchCutLevel,CutMode);
for Row=1:size(LocustMap,1)
    for Col=1:size(LocustMap,2)
        if (LocustMap(Row,Col)<LocustCutThreshold) LocustMap(Row,Col)=0; end
    end
end


Error=0;

% Performing Locust search
if params.doLocust
    disp('Running a Locust search...');
    RadiusX=params.LocustRadiusX;
    RadiusY=params.LocustRadiusY;
    RadSig=params.LocustRadSig;
    locustSearch(LocustMap,map,LocustAlarmThreshold,OutputDir,RadiusX, ...
        RadiusY,RadSig,Glitches);

    % Checking if any detection candidates were found
    try
        FileName=sprintf('%s/SignalLocust1.mat',OutputDir);
        load(FileName);
    catch
        disp('No signals found by Locust algorithm.');
        Error=Error+1;
        if(params.doHough==0)
            rmdir(OutputDir);
        end
    end

end

% Performing Hough search
if params.doHough
    disp('Running a Hough search...');
    HoughMap=SNRMap; %%%%% We should call a glitch rejection function here!!!!
    CutNumber=params.HoughCutNumber;
    RadSig=params.HoughRadSig;
    houghSearch(HoughMap,map,HoughAlarmThreshold,OutputDir,2, ...
        CutNumber,RadSig);

    % Checking if any detection candidates were found
    try
        FileName=sprintf('%s/SignalHough.mat',OutputDir);
        load(FileName);
    catch
        disp('No signals found by Hough algorithm.');
        Error=Error+1;
        if(params.doLocust==0)
            rmdir(OutputDir);
        end
    end

end


% If no detection candidates were found by the Locust and Hough algorithms
% the output directory is deleted
if(Error==2)
    rmdir(OutputDir);
end



% % Finishing running time measure tic...toc
% toc
    
