function signalViewer(InputDir)
% This standalone application can be used to visualize Locust and/or Hough
% tracks. Note, that by default, you need to have both 'SignalLocust%i.mat'
% and 'SignalHough.mat' files in the input directory 'InputDir'.
%
% Written by Peter Raffai
% E-mail: praffai_"at"_bolyai.elte.hu
%
% Last updated: 07.21.2010.
%
% Inputs:
% 'InputDir' - the name of the directory, that contains the the
% 'SignalLocust%i.mat' and 'SignalHough.mat' files.
%
% Outputs:
% Matlab figures that visualize the Locust track(s), the Hough track, and
% the raw input SNR map.
%



%Defining parameters
LocustTrackNumber=1;%number of Locust tracks to be visualized
HoughPercent=1;%the percentage of Hough track elements to be visualized



%Visualizing Locust track(s)
ReferenceFile=sprintf('%s/SignalLocust1.mat',InputDir);
load(ReferenceFile);
LocustMap=zeros(size(SNRMap,1),size(SNRMap,2));
for i=1:LocustTrackNumber
    LocustTrackFile=sprintf('%s/SignalLocust%i.mat',InputDir,i);
    load(LocustTrackFile);
    for j=1:size(Signal,1)
        LocustMap(Signal(j,2),Signal(j,3))=1;%Signal(j,1);%
    end
    clear Signal;
end


%Visualizing Hough track
ReferenceFile=sprintf('%s/SignalHough.mat',InputDir);
load(ReferenceFile);
HoughMap=zeros(size(SNRMap,1),size(SNRMap,2));

%The next part is for visualizing the top 'HoughPercent' percentage of
%track elements
SortedHoughTrack=sort(Signal(:,1),'descend');
SortedHoughTrackIndex=ceil(HoughPercent*length(SortedHoughTrack));
HoughTrackTreshold=SortedHoughTrack(SortedHoughTrackIndex);
Signal(find(Signal(:,1)<HoughTrackTreshold),:)=[];
for j=1:size(Signal,1)
    HoughMap(Signal(j,2),Signal(j,3))=1;%Signal(j,1);%
end
clear Signal;

%Plotting the Locust tracks
figure100=figure(100);
axes1 = axes('Parent',figure100,'Layer','top','FontSize',24);
box(axes1,'on');
hold(axes1,'all');
axis xy;
image(t,f,LocustMap,'Parent',axes1,'CDataMapping','scaled');
ylim([f(1) f(length(f))]);
xlabel('t [sec]','FontSize',24);
ylabel('f [Hz]','FontSize',24);
colorbar('peer',axes1,'EastOutside','FontSize',24);
hold off;
box off;

%Plotting the Hough tracks
figure200=figure(200);
axes2 = axes('Parent',figure200,'Layer','top','FontSize',24);
box(axes2,'on');
hold(axes2,'all');
axis xy;
image(t,f,HoughMap,'Parent',axes2,'CDataMapping','scaled');
ylim([f(1) f(length(f))]);
xlabel('t [sec]','FontSize',24);
ylabel('f [Hz]','FontSize',24);
colorbar('peer',axes2,'EastOutside','FontSize',24);
hold off;
box off;

%Plotting Hough curve
deltaT=t(2)-t(1);
tstart=t(1)-deltaT/2;
tend=t(length(t))+deltaT/2;
tH=tstart:(tend-tstart)/1000:tend;
fH=params_secHz(3)*tH.^2+params_secHz(2)*tH+params_secHz(1);
figure201=figure(201);
axes4 = axes('Parent',figure201,'Layer','top','FontSize',24);
box(axes4,'on');
hold(axes4,'all');
plot(tH,fH,'LineWidth',3,'Color',[1 0 0]);
Title=sprintf('f(t) = [%f Hz/s^2]*t^2 + [%f Hz/s]*t + [%f Hz]',params_secHz(3),params_secHz(2),params_secHz(1));
title(Title,'FontSize',24);
ylim([f(1) f(length(f))]);
xlabel('t [sec]','FontSize',24);
ylabel('f [Hz]','FontSize',24);
hold off;
box off;

%Plotting the raw input SNR map
figure1=figure(1);
axes3 = axes('Parent',figure1,'Layer','top','FontSize',24);
box(axes3,'on');
hold(axes3,'all');
axis xy;
image(t,f,SNRMap,'Parent',axes3,'CDataMapping','scaled');
ylim([f(1) f(length(f))]);
xlabel('t [sec]','FontSize',24);
ylabel('f [Hz]','FontSize',24);
colorbar('peer',axes3,'EastOutside','FontSize',24);
hold off;
box off;


