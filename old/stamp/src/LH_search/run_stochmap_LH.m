clear all;

%Set the narrow band search defaults
params = stampDefaults;

params.segmentDuration=4;
params.intFrameCachePath='/archive/home/praffai/STAMP_SVN/trunk/stamp/LH_search/cachefiles/';
jobsFile=['/archive/home/praffai/STAMP_SVN/trunk/stamp/LH_search/test_run.txt'];

% Search direction-------------------------------------------------------------
params.ra=6;   %right ascenion in hours (for search direction)
params.dec=30; %declination in degrees (for search direction)

% Frequency range--------------------------------------------------------------
params.fmin = 500;
params.fmax = 600;

% Override default parameters--------------------------------------------------
params.debug=true;

% Call stochMap----------------------------------------------------------------
params = LHDefaults(params);
stochmap(params, jobsFile, 816065660, 816065860);

% %GRB search (alternative to calling stochmap above)
% Tminus=350;%in seconds
% Tplus=150;%in seconds
% TriggerGPStimes=load('S5_GPSlist.dat');
% for i=1:size(TriggerGPStime,1)
%     GPS=TriggerGPStimes(i);
%     stochmap(params, jobsFile, GPS-Tminus, GPS+Tplus);
% end
