function houghSearch(SNRMap,map,Threshold,OutputDir,order,CutNumber,RadSig)
% Program that carries out the so called Hough search on an input
% ft-map and may output a detection candidate corresponding
% to the best Hough fit.
%
% This program is called by searchLH.m, and therefore
% by default must be in the same directory.
%
% written by Peter Raffai, Eotvos University (praffai_"at"_bolyai.elte.hu)
%
% Last modified: 10.01.2011.
%
%
% Input:
% - SNRMap: the ft-map produced by the searchLH code
% - map: the structure that contains all ft-mapss produced by the stochMap
% code
% - Threshold: the detection threshold parameter which the test
% statistic is compared to.
% - OutputDir: name of the directory where files corresponding to possible
% event candidates are dumped.
% - order: order of polynomial curve used in Hough fit.
% - CutNumber: Hough transform is only carried out for the top 'CutNumber'
% number of ft-map elements
% - RadSig: radius of the environment of a trace for which the test
% statistic is calculated
%
%
% Output:
% The program outputs a .mat file for the most probable detection candidate
% if its detection probability exceeds 'Threshold' (otherwise the search
% ends with a no detection statement on the screen). The output .mat file
% stores all relevant parameters of the candidate.


% Initializing sigma map
SNRSigmaMap = map.sigma.^-2;


%Creating an ft-map that only contains the top 'CutNumber' number of
%matrix elements
A=SNRMap;
SGN = zeros(size(A,1),size(A,2));
for i = 1:CutNumber
    [MaxCol,MaxRowIndex] = max(A);
    [Max,MaxColIndex] = max(MaxCol);
    SGN(MaxRowIndex(MaxColIndex),MaxColIndex) = Max;
    A(MaxRowIndex(MaxColIndex),MaxColIndex) = 0;
end


%Checking the number of non-zero elements in the matrix to be Hough
%transformed
Number = length(find(SGN));
if(Number==0) 
    disp('Warning: No matrix elements to be Hough transformed. Returning.');
    return;
end

%Calculating the number of Hough transformations to be made
CycleMax = 1;
for i = 0:order
    CycleMax = CycleMax*(Number-i)/(i+1); %Proportional to Number^(order+1)
end


%Storing coordinates of matrix elements to be Hough transformed
[Y,X] = find(SGN);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Transforming matrix elements into Hough space and storing Hough parameters
%in matrix 'P'
cycle = 0;
for i=1:Number-2
    for j=i+1:Number-1
        for k=j+1:Number
            cycle = cycle+1;
            FitMatrix = [X(i),Y(i);X(j),Y(j);X(k),Y(k)];
            P(cycle,:) = polyfit(FitMatrix(:,1),FitMatrix(:,2),order);
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
%Deleting rows from 'P' containing Inf or NaN or equal to [0,0,0,...]
for i=1:size(P,1)
    if( isequal(P(i,:),zeros(1,size(P,2))) ) P(i,1)=NaN; end
end
[x,y]=find(isnan(P));
P(x,:)=[];
clear x y;
[x,y]=find(isinf(P));
P(x,:)=[];
clear x y;
if(size(P,1)==0) return; end


%Calculating test statistic for all the fitted curves
for k=1:size(P,1)
    for i = 1:size(P,2)
        params(i) = P(k,size(P,2)+1-i);
    end 
    params=params(:);
    params=params';

    clear x;
    Signal=[];
    SignalIndex=1;
    for j = 1:size(SGN,2)
        for i = 1:order+1
            x(i) = j^(i-1);
        end
        Col = j;
        Row = round(params*x');
        if( (Row>0) & (Row<=size(SNRMap,1)) )
            Signal(SignalIndex,:)= ...
                [SNRMap(Row,Col),Row,Col,SNRSigmaMap(Row,Col)];
            SignalIndex=SignalIndex+1;
        end
    end

    %Calculating the test statistic
 %   if(SignalIndex>1)
        C(k)=CCalculator(Signal,SNRMap,SNRSigmaMap,RadSig);
        %calculating the test statistic
 %   end
    clear Signal SignalIndex;
end

[MaxC,MaxCInd]=max(C);

%Defining the parameters of the best fit
for i = 1:size(P,2)
    params(i) = P(MaxCInd,size(P,2)+1-i);
end 
params=params(:);
params=params';

% Calculating the parameters of the best fit in sec&Hz
t=map.xvals;
f=map.yvals;
deltaT=t(2)-t(1);
deltaF=f(2)-f(1);
TT=t(1)-deltaT;
params_secHz(3)=(deltaF/deltaT^2)*params(3);
params_secHz(2)=(deltaF/deltaT)*params(2)-2*TT*params_secHz(3);
params_secHz(1)=deltaF*params(1)-(TT^2)*params_secHz(3)...
     -TT*params_secHz(2)+f(1)+deltaF/2;

% Storing signal candidate
clear x;
SignalIndex=1;
for j = 1:size(SGN,2)
    for i = 1:order+1
        x(i) = j^(i-1);
    end
    Col = j;
    Row = round(params*x');
    if( (Row>0) & (Row<=size(SNRMap,1)) )
        Signal(SignalIndex,:)=[SNRMap(Row,Col),Row,Col,SNRSigmaMap(Row,Col)];
        SignalIndex=SignalIndex+1;
    end
end

CTemp=C(MaxCInd);
clear C;
Integral=CTemp;
p = msnr_prob(Integral,map.nindep);
if((1-p)> Threshold)
    %if trace is taken to be an event candidate, properties of its
    %elements are stored in an output .mat file
    FileNameStr = sprintf('%s/SignalHough.mat',OutputDir);
    t=map.xvals;
    f=map.yvals;
    save(FileNameStr,'Signal','SNRMap','SNRSigmaMap','t','f','Integral','p','params','params_secHz');
end



