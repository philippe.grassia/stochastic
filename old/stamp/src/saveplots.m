function saveplots(params, map)

  printmap(map.snr, map.xvals, map.yvals, 'time (s)', 'f (Hz)', 'SNR', [-5 5]);
  print('-dpng','snr.png');
  print('-depsc2','snr.eps');

  printmap(map.snr/params.wingfac, ...
           map.xvals, map.yvals, 'time (s)', 'f (Hz)', 'SNR', [-5 5]);
  print('-dpng','eff_snr.png');
  print('-depsc2','eff_snr.eps');

  printmap(map.y, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
    'y (strain^2)', [-params.yMapScale params.yMapScale]);
  print('-dpng','y_map.png');
  print('-depsc2','y_map.eps');

  map.w = map.snr./map.sigma;
  printmap(map.w, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
         'weight', [min(min(map.w)) max(max(map.w))]);
  print('-dpng','weight_map.png');

  if params.Autopower
    printmap(map.Xi_snr, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
	     'SNR_{\Xi}', [-1 1]);
    print('-dpng','Xi_snr_map.png');
    print('-depsc2','Xi_snr_map.eps');
  end

  missing_data_cut=(map.sigma==1);
  logSigMap=zeros(size(map.sigma));
  logSigMap(~missing_data_cut)=-log10(map.sigma(~missing_data_cut));
  printmap(logSigMap, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
    '-log_{10}(\sigma)', [min(-log10(map.sigma(~missing_data_cut))) ...
    max(-log10(map.sigma(~missing_data_cut)))]);
  print('-dpng','sig_map.png');
  print('-depsc2','sig_map.eps');

  % fluence maps
  printmap(map.F, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
    'F (ergs/cm^2)', [-params.FMapScale params.FMapScale]);
  print('-dpng','F_map.png');
  print('-depsc2','F_map.eps');

  logSigFMap=zeros(size(map.sigma));
  logSigFMap(~missing_data_cut)=-log10(map.sigF(~missing_data_cut));
  printmap(logSigFMap, map.xvals, map.yvals, 'time (s)', 'f (Hz)', ...
    '-log_{10}(\sigma)', [min(-log10(map.sigF(~missing_data_cut))) ...
    max(-log10(map.sigF(~missing_data_cut)))]);
  print('-dpng','sigF_map.png');
  print('-depsc2','sigF_map.eps');

return
