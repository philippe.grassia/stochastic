function [new_map, params] = addMissingDatatoMap(params, ...
  old_map, f, freqbins, segstarttime, Allsegstarttime)
%function [new_map, params] = addMissingDatatoMap(params, ...
%  old_map, f, freqbins, segstarttime, Allsegstarttime)
% E. Thrane - adds missing data to an ft-map and crop frequencies.  The missing
% data gets a value of NaN.
%
% INPUT ARGUMENTS:
%  * old_map is the original ft-map that will be cropped and parsed.
%  * f is an array of frequencies for the pre-processed data.
%  * freqbins is an array of frequencies to be analyzed by STAMP.  This is some
%    subset of the frequencies in f.
%  * segstarttime is GPS start time of segments for available data.
%  * Allsegstarttime is GPS start time of segments in the map whether or not
%    they have data.

  %crop between fmin and fmax-----------------------------------
  cutfreq = (f >= freqbins(1)) & (f <= freqbins(end));
  map2= old_map(cutfreq,:);

  % create a new properly sized map of NaN's
  map_size=[length(freqbins), length(Allsegstarttime)];
  new_map = NaN*ones(map_size);

  % ctimeindex is an array of indices for data in Allsegstarttime that are also
  % in segstarttime, i.e., data is present
  [commontime, ctimeindex] = intersect(Allsegstarttime, segstarttime);

% This portion of code handles is relevant for the case where the ft-map spans
% more than one jobs, the user is using segmentDurations>1s, and this causes a 
% skip in the expected segment start times.  Here is an example
%   firstjobtimes = 10010 10015 10020 10025 10030
%   secondjobtimes= 10041 10046 10051 10056 10061
% In this case, job2 segments are 1s off from what one would expect by 
% extrapolating from job1.
% 
% To get around this problem, we round the rogue segments to the nearest
% extrapolated times.  A warning message is issued to alert the user to this
% approximation.
if length(ctimeindex)~=length(segstarttime)
  if params.discontinuity==false
    warning('Map discontinuity; approximating to nearest expected segment.');
    params.discontinuity=true;
  end
  if params.twojoblimit
    [roguesegs, rogue_idx] = setdiff(segstarttime, Allsegstarttime);
    % calculate the smallest possible shift for first rogue segment
    shift_bins = min(abs(roguesegs(1)-Allsegstarttime)) == ...
      abs(roguesegs(1)-Allsegstarttime);
    shift = roguesegs(1)-Allsegstarttime(shift_bins);
    % in case you are exactly in between two bins
    shift = shift(1);
    [dummy, ctimeindex_shift] = intersect(Allsegstarttime, roguesegs-shift);
    ctimeindex = [ctimeindex ctimeindex_shift];
    % check if this has accounted for all the segments
    if length(ctimeindex)~=length(segstarttime)
      error(['Missing segments unaccounted for in addMissingDatatoMap.  ' ...
             'Try using params.twojoblimit==false.']);
    end
  else
  % This solution by Shivaraj is less safe because it can loop forever if
  % Allsegstarttime and segstarttime do not overlap as they are supposed to.
  % Also, it always rounds the later segment times *up* instead of to the
  % nearest bin.  However, it will work for an arbitrary number of jobs.
    Allsegstarttime_shift = Allsegstarttime;
    while(length(ctimeindex)~=length(segstarttime))
      Allsegstarttime_shift = Allsegstarttime_shift + 1;
      [no_use, ctimeindex_shift] = intersect(Allsegstarttime_shift, ...
        segstarttime);
      ctimeindex = [ctimeindex ctimeindex_shift];
    end
  end
end

% replace NaN's with data
new_map(:,ctimeindex) = map2;

return
