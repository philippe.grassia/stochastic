function [ rmap, B ] = mylilradon(map, theta, exp)
%function [ rmap, B ] = mylilradon(map, theta, exp)
  % E. Thrane
  % map: ft-map array.  theta: array of Radon angle. 
  % exp: raise weight factors to this exponent
  % Example test:
  %{
      map = zeros(100);
      map(30,:)=1;
      theta = [2:2:180];
      [rmapdata, b] = mylilradon(map, theta, 1);
      rmap.snr = rmapdata;  
      rmap.xvals = theta;
      rmap.yvals = b; % impact parameter
      printmap(rmap.snr, rmap.xvals, rmap.yvals, '\theta (deg)', 'b', ...
        'SNR', [-5 5]);
      print('-dpng','temp.png');     
  %}

  if any(theta>180) || any(theta<=0)
    error('mylilradon: theta must be >0 and <=180.');
  end

  [N M] = size(map);

  % center of the image
  m = round(M/2);
  n = round(N/2);

  % The total number of rho's is the number of pixels on the diagonal, since
  % this is the largest straight line on the image when rotating
  rhomax = ceil(sqrt(M^2 + N^2));

  rc = round(rhomax/2);
  mt = max(theta);
% EHT: made this change on May 11, 2011 so that b and rmap would be consistent
% in dimensions.
%  rmap = cast(zeros(rhomax+1, mt), 'double');
  rmap = cast(zeros(rhomax, mt), 'double');

  for t = 1:max(theta)-1
    a = -cot(t*pi/180);
    for r = 1:rhomax
      rho = r - rc;
      b = rho/sin(t*pi/180);  % y = ax + b
      B(r) = b;               % EHT record b-values

      if t<=45
        qmax = min(round(-a*m+b), n-2);
        qmin = max(round(a*m+b), -n);
      elseif t>45 && t<=90
        qmax = min(round((-n-b)/a),m-2);
        qmin = max(round((n-b)/a),-m);
      elseif t>90 && t<=135
        qmax = min(round((n-b)/a),m-2);
        qmin = max(round((-n-b)/a),-m);
      else % t>135 && t<180
        qmax = min(round(a*m+b),n-2);
        qmin = max(round(-a*m+b),-n);
      end

      for q = qmin:qmax
        if t<=45 || t>135
          y = q;
          p = floor( (y-b)/a );
          x = p;
        else
          x = q;
          p = floor( a*x+b );
          y = p;
        end

        pup = p - floor(p);
        plow = 1 - pup;

        if x>=m-3, x=m-3; end;
        if x<-m, x=-m; end;
        if y>=n-2, y=n-2; end;
        if y<-n, y=-n; end;

        rmap(rhomax-r+1, mt-t) = rmap(rhomax-r+1, mt-t) + ...
          plow^exp * map(y+n+1, x+m+1) + pup^exp * map(y+n+1,x+m+1+1);

     end
  end
end

for t = 180 % the sum-line is vertical
  rhooffset = round((rhomax - M)/2);
  % cannot use r as x in both rmap and f since they are not the same size
  for x = 1:M
        r = x+rhooffset;
        r = rhomax - r + 1;
        for y = 1:N
            rmap(r,t) = rmap(r,t) + map(y,x);
        end
    end
end

rhoaxis = (1:rhomax+1) - rc;

