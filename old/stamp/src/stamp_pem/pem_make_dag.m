function pem_make_dag(folder,user,frame_size,ifo)

%[status ld_library_path]=system('echo $LD_LIBRARY_PATH')

%loads channel list
[chanlist,sample] = textread('pem_channel_list.txt','%s %d');
chanlistsize=size(chanlist);

gps_times_file=load('jobfile_all.txt');
gps_times=gps_times_file(:,1);
condor_jobs = 1:(ceil(length(gps_times)/100)+1);

rownum=length(gps_times);
num_of_jobs = length(condor_jobs);

mkdir_command='mkdir logs/';
system(mkdir_command);

fid=fopen('pem_pipe.dag','w+');

job_number=0;

for i=1:chanlistsize
   channel_split=regexp(chanlist{i},':','split');
   channel_split=cellstr(channel_split);
   channel_name=[channel_split{1} '_' channel_split{2}];
   channel_name_short=channel_split{2};

   mkdir_command=['mkdir files/' channel_name];
   system(mkdir_command);

   for j=1:rownum
    if ismember(j,condor_jobs)

      job_number=job_number+1;
      fprintf(fid,'JOB %d pem_pipe.sub\n',job_number)
      fprintf(fid,'RETRY %d 3\n',job_number)
      fprintf(fid,'VARS %d channel_name="%s" lineNumber="%d" jobNumber="%d" folder="%s" user="%s" frame_size="%s" ifo="%s"',job_number,channel_name,job_number,job_number,folder,user,frame_size,ifo); 
      fprintf(fid,'\n\n');
    end

      mkdir_command=['mkdir files/' channel_name '/' num2str(j)];
      system(mkdir_command);

   end

end

job_number=job_number+1;
job_number_last=job_number;
fprintf(fid,'JOB %d pem_pipe.sub\n',job_number)
fprintf(fid,'RETRY %d 3\n',job_number)
fprintf(fid,'VARS %d channel_name="%s" lineNumber="%d" jobNumber="%d" folder="%s" user="%s" frame_size="%s" ifo="%s"',job_number,channel_name,j,job_number,folder,user,frame_size,ifo);
fprintf(fid,'\n\n');

job_number=0;

fprintf(fid,'JOB %d pem_pipe.sub\n',0)

fprintf(fid,'VARS %d channel_name="%s" lineNumber="0" jobNumber="0" folder="%s" user="%s" frame_size="%s" ifo="%s"',0,channel_name,folder,user,frame_size,ifo);
fprintf(fid,'\nPARENT 0 CHILD');

for i=1:job_number_last
   fprintf(fid,' %d ',i);
end

for i=1:chanlistsize
   fprintf(fid,'\nPARENT');
   for j=(i-1)*num_of_jobs+1:i*num_of_jobs-1
      fprintf(fid,' %d ',j);
   end
   fprintf(fid,'CHILD %d',i*num_of_jobs);
end

fprintf(fid,'\nPARENT');
for i=1:(job_number_last-1)
   fprintf(fid,' %d ',i);
end
fprintf(fid,'CHILD %d',job_number_last);


fclose(fid);

fid=fopen('jobnumbers.txt','w+');
for i=1:chanlistsize
   fprintf(fid,'%d\n',i*num_of_jobs);
end
fclose(fid);

fid=fopen('pem_pipe.sub','w+');
fprintf(fid,'executable = pem_run_stamp\n');
fprintf(fid,'output = logs/out.$(jobNumber)\n');
fprintf(fid,'error = logs/err.$(jobNumber)\n');
fprintf(fid,'arguments = $(channel_name) $(lineNumber) $(folder) $(user) $(frame_size) $(ifo)\n');
fprintf(fid,'requirements = Memory >= 128 && OpSys == "LINUX"\n');
fprintf(fid,'#universe = local\n');
fprintf(fid,'notification = never\n');
fprintf(fid,'getenv = True\n');
fprintf(fid,'#environment = HOME=$(home);LD_LIBRARY_PATH=$(ld_library_path)\n');
fprintf(fid,'#log = stochastic_pipe.log\n');
fprintf(fid,'log = /usr1/%s/radon_%s.log\n',user,folder);
fprintf(fid,'+MaxHours = 24\n');
fprintf(fid,'queue 1\n');
fclose(fid);

