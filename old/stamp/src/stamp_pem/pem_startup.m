verNum = str2num(version('-release'));
if(~isempty(verNum))
  verOld = verNum < 14;
 else
   verOld = false;
end
if( verOld || (~isdeployed))

addpath(genpath('/archive/home/mcoughlin/IM/matapps/packages/stochastic/trunk/'));
addpath(genpath('/archive/home/mcoughlin/IM/matapps/releases/Utilities/current/'));
addpath(genpath('/archive/home/mcoughlin/IM/matapps/vendor'));
addpath(genpath('/archive/home/mcoughlin/IM/sgwb/'));
addpath(genpath('/archive/home/mcoughlin/IM/svn/'));
addpath /archive/home/mcoughlin/git-repo/im/

end
clear verNum;
clear verOld;
