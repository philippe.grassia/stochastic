#!/bin/bash

cd ${HOME}/STAMP/STAMP-PEM/compiled_radon_code/
cp /archive/home/emacayeal/matapps/packages/stochastic/trunk/stamp/stamp_pem/* .

matlab -q<<EOF
 pem_startup_evan
 mcc -R -nojvm -m pem_run_stamp
EOF

source ${HOME}/MatlabSetup_R2007a_glnxa64.sh

./pem_run_stamp 1 1 1 1 1 1

cd /archive/home/emacayeal/matapps/packages/stochastic/trunk/stamp/stamp_pem/

