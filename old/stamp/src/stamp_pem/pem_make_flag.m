function pem_make_flag(folder,user,ifo)

ifo='H1';
user='mcoughlin';
folder='2010_01_08';

gps_times_file=load('jobfile.txt');
gps_begin=gps_times_file(2);
gps_end=gps_times_file(3);
gps_duration=gps_times_file(4);

[chanlist,sample] = textread('pem_channel_list.txt','%s %d');
chanlistsize=size(chanlist);

total_segment=[];
for i=1:chanlistsize
 try
   count=0;
   channel_split=regexp(chanlist{i},':','split');
   channel_split=cellstr(channel_split);
   channel_name=[channel_split{1} '_' channel_split{2}];
   channel_name_short=channel_split{2};

   x=load(['files/' channel_name '/segments.txt']);
   A=x;
   [y, j] = sort(A(:,1),'ascend');
   x = A(j,:);

   total_time=0;
   for j=1:length(x(:,1))
      segment=x(j,1):1:x(j,2);
      total_segment=[total_segment segment];
   end
 catch
 end
end

A=total_segment;
[b1, m1, n1] = unique(A(:,1), 'first');
x=A(m1,:);

a=unique(total_segment);
a=sort(a,'ascend');
segments=[];
start_time=a(1);
end_time=a(1);

for i=2:length(a)
   if (a(i)==end_time+1)
      end_time=a(i);
   else
      segments=[segments; start_time end_time];
      start_time=a(i);
      end_time=a(i);
   end
end

fid=fopen('segments.txt','w+');
for i=1:length(segments(:,1))
   fprintf(fid,'%.0f %.0f\n',segments(i,1),segments(i,2)); 
end
fclose(fid);

fid=fopen('summary.txt','w+');
fprintf(fid,'%.0f %.0f\n',gps_begin,gps_end);
fclose(fid);

segment_insert_name='APC-FLAG';
xml_name=[ifo '-APC_FLAG-' num2str(gps_begin) '-' num2str(gps_duration)  '.xml'];

if ifo=='H1'
   site='https://ldas-jobs.ligo-wa.caltech.edu';
elseif ifo=='L1'
   site='https://ldas-jobs.ligo-la.caltech.edu';
else
   site='https://ldas-jobs.ligo.caltech.edu';
end

segments='segments.txt';
summary='summary.txt';
output=[ifo '-' segment_insert_name '.xml'];

system_command=sprintf('ligolw_segment_insert --segment-url=https://segdb.ligo.caltech.edu --ifos=%s --name="%s" --version=1 --explain="Airplane Coherence derived segments" --comment="%s/~%s/STAMP/STAMP-PEM/%s/daily/%s/4/pem_summary_page.html" --summary-file=%s --segment-file=%s --append',ifo,segment_insert_name,site,user,ifo,folder,summary,segments);

fid=fopen('segment_insert.sh','w+');
fprintf(fid,'#!/bin/bash\n\n');
fprintf(fid,'%s',system_command);
fclose(fid);

