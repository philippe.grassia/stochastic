function do_pem(params, map, stoch_out)
% function do_pem(params, map)
 params.channelName2 = [params.ifo2 '_' params.ASQchannel2];
   params.which_folder=['files/' params.channelName2 '/' ...
     num2str(params.jobNumber) '/'];
   [rmax, time_begin_place, time_end_place] = pem_searchRadon(params, map);
   if rmax>0
      params.savePlots=1;
      if params.savePlots,  pem_saveplots(params, map); end

      stoch_out.time_begin=map.segstarttime(time_begin_place);
      stoch_out.time_end=map.segstarttime(time_end_place);
      stoch_out.rmax = rmax;
   end
   stoch_out.pulsar_lines = pem_lines(params, map);

   params.saveMat=1;
   if params.saveMat
      save([params.which_folder 'map.mat'], 'map','stoch_out');
   end
return
