verNum = str2num(version('-release'));
if(~isempty(verNum))
  verOld = verNum < 14;
 else
   verOld = false;
end
if( verOld || (~isdeployed))

addpath(genpath('/archive/home/emacayeal/matapps/packages/stochastic/'));
addpath(genpath('/archive/home/emacayeal/matapps/releases/Utilities/current/'));
addpath(genpath('/archive/home/emacayeal/matapps/vendor'));
addpath(genpath('/archive/home/emacayeal/matapps/packages/stochastic/trunk/'));

end
clear verNum;
clear verOld;
