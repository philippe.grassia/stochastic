function meta_check(params, pp, inputfile)
% function meta_check(params, pp, inputfile)
% E. Thrane.  Checks to make sure that the metadata in inputfile matches the
% metadata in params and pp.

% make a copy of parameter values
params0 = params;
pp0 = pp;

pp.flow = frgetvect(inputfile,[params.ifo1 params.ifo2 ':flow'], 0, ...
  params.segmentDuration);
pp.fhigh = frgetvect(inputfile,[params.ifo1 params.ifo2 ':fhigh'], ...
  0, params.segmentDuration);
pp.deltaF = frgetvect(inputfile,[params.ifo1 params.ifo2 ':deltaF'], ...
  0, params.segmentDuration);
pp.w1w2bar = frgetvect(inputfile,[params.ifo1 params.ifo2 ...
  ':w1w2bar'], 0, params.segmentDuration);
pp.w1w2squaredbar = frgetvect(inputfile, [params.ifo1 ...
  params.ifo2 ':w1w2squaredbar'], 0, params.segmentDuration);
pp.w1w2ovlsquaredbar = frgetvect(inputfile, [params.ifo1 ...
  params.ifo2 ':w1w2ovlsquaredbar'], 0, params.segmentDuration);
params.numSegmentsPerInterval = frgetvect(inputfile, ...
  [params.ifo1 params.ifo2 ':numSegmentsPerInterval'], 0, ...
  params.segmentDuration);

params.segmentDuration = frgetvect(inputfile, ...
  [params.ifo1 params.ifo2 ':segmentDuration'], 0, 10000);

% check pp consistency
if pp.flow~=pp0.flow, error('metadata problem: pp.flow.'); end;
if pp.fhigh~=pp0.fhigh, error('metadata problem: pp.fhigh.'); end;
if pp.deltaF~=pp0.deltaF, error('metadata problem: pp.deltaF.'); end;
if pp.w1w2bar~=pp0.w1w2bar, error('metadata problem: pp.w1w2bar.'); end;
if pp.w1w2squaredbar~=pp0.w1w2squaredbar, ...
  error('metadata problem: pp.w1w2squaredbar.'); end;
if pp.w1w2ovlsquaredbar~=pp0.w1w2ovlsquaredbar, ...
  error('metadata problem: pp.w1w2ovlsquaredbar.'); end;

% check param consistency
if params.numSegmentsPerInterval~=params0.numSegmentsPerInterval, ...
  error('metadata problem: params.numSegmentsPerInterval.'); end;
if params.segmentDuration~=params0.segmentDuration, ...
  error('metadata problem: params.segmentDuration.'); end;

return
