function [varargout] = scrambleMap(varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Written by Tanner Prestegard
% email: prestegard@physics.umn.edu
%
% Takes in any number of arrays as arguments
% and permutes them in the same way.
% Arrays MUST be the same size.
%
% INPUT: 
% Any number of arrays of numbers.
%
% OUTPUT:
% The same number of arrays.
%
% SAMPLE USAGE:
% [map1_scram,map2_scram]=scrambleMap(map1,map2)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% seed random number generator
rand('state',sum(100*clock+pi()));

if (nargin == 0)
  fprintf('Please use at least one array as the argument for this function.  Exiting...\n');
  return;
end

nargout=nargin;
rowNum=size(varargin{1},1);
colNum=size(varargin{1},2);

permute=randperm(rowNum*colNum);

for i=1:size(varargin,2)
  i
  size(varargin{i})
  temp=zeros(rowNum,colNum);
  temp(permute)=varargin{i};
  varargout{i}=temp;
end
return
