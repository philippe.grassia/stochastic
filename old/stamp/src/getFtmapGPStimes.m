function Allsegstarttime = getFtmapGPStimes(params,freqbins, segstarttime)
% function Allsegstarttime = getFtmapGPStimes(params,freqbins, segstarttime)
% Allsegstarttimes = start times of segments that fall within startGPS and 
% endGPS, regardless of whether or not they have data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  try 
    params.doOverlap;
  catch 
    params.doOverlap = true; % default value
  end

  if params.doOverlap
    segdur = params.segmentDuration/2;
  else
    segdur = params.segmentDuration;
  end

  if(segstarttime(1)-params.startGPS >= 0)
    roundedStart = params.startGPS + ...
         mod(segstarttime(1)-params.startGPS, segdur);
  else
    roundedStart = params.startGPS - ...
         mod(segstarttime(1)-params.startGPS, segdur);
  end

  if(params.endGPS-segstarttime(end) >= 0)
    roundedEnd = params.endGPS - mod(params.endGPS-segstarttime(end),segdur);
  else
    roundedEnd = params.endGPS + mod(params.endGPS-segstarttime(end),segdur);
  end
  Allsegstarttime = roundedStart:segdur:roundedEnd;
return
