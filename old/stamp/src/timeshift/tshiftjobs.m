function tshiftjobs(inputfile, ouputfile, secstocrop, shiftsec)
% function tshiftjobs(inputfile,ouputfile,tshiftsec)
% This routine to be used in assocition with shiftsec in readTimeSeriesData2.m
% INPUT: inputfile -> input job file e.g., 'S5H1L1_full_run.txt'
%        ouputfile -> output file name (with new list of jobs)
%                     e.g., 'S5H1L1_tshift1500.txt'
%        secstocrop -> total secs to crop from the job
%                      (to remove small jobs that don't have useful data)
%                      e.g., numSegmentsPerInterval - 1 + 2 * buffersec
%        shiftsec -> secs to shift the second detector
% Routine by shivaraj
% Contact shivaraj@physcis.umn.edu

tic;

data = load(inputfile);
tt = data(1,2):data(end,3);

% to reduce the run time, the original job is divided into smaller ones
% assumes the length of the job >15000; for other jobs modify this part
temp1 = [];
for ii = 1:5000
  %fprintf('%d \n',ii);
  temp1 = [temp1 data(ii,2):data(ii,3)];
end
temp2 = [];
for ii = 5001:10000
  %fprintf('%d \n',ii);
  temp2 = [temp2 data(ii,2):data(ii,3)];
end
temp3 = [];
for ii = 10001:15000
  %fprintf('%d \n',ii);
  temp3 = [temp3 data(ii,2):data(ii,3)];
end
temp4 = [];
for ii = 15001:length(data(:,1))
  %fprintf('%d \n',ii);
  temp4 = [temp4 data(ii,2):data(ii,3)];
end
temp = [temp1 temp2 temp3 temp4];

cut = ismember(tt,temp);
newcut = cut(shiftsec+1:end);
finalcut = cut(1:end-shiftsec) & newcut;
newtt = tt(finalcut);

kk = 1;
job = [newtt(1)];
fid = fopen(ouputfile,'w');
for jj = 2:length(newtt)
  if(newtt(jj)-job(end) > 1)
      if(job(end)-job(1) > secstocrop)
        fprintf(fid,'%5d  %9d  %9d  %4d \n', kk, job(1), job(end), job(end)-job(1));
      end
      job = [newtt(jj)];
      kk = kk + 1;
  else
    job = [job newtt(jj)];
  end
end

fclose(fid);
fprintf('%.2f sec\n',toc);
