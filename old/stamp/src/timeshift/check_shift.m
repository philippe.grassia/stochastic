params.doDetectorNoiseSim = false;
params.stampinj = false;

% Using old routine (through cache files)
channelName = 'L1:LSC-STRAIN';
dataStartTime = 816070181;
dataDuration = 10;
frameType = 'L1_RDS_C03_L2';
frameDuration = -1;
gpsTimesFile = '/archive/home/ethrane/cache/S5H1L1/gpsTimesL.2.txt';
frameCacheFile = '/archive/home/ethrane/cache/S5H1L1/frameFilesL.2.txt';
mc.init = 0;
mc.tranfer = [1 1];
[adcdata1, dataOK, params] =  ...
   readTimeSeriesData2(channelName, dataStartTime, dataDuration, ...
                      frameType, frameDuration, gpsTimesFile, ...
                      frameCacheFile, params, mc);

% Using the new code
params.largeShift = false;
params.largeShiftTime2 = 1500;
params.cacheFile = '/home/shivaraj/GWHEN/stamp2/src/preproc/cache.mat';
dataStartTime = 816070181;

[adcdata2, dataOK, params] =  ...
   readTimeSeriesData2t(channelName, dataStartTime, dataDuration, ...
                      frameType, frameDuration, gpsTimesFile, ...
                      frameCacheFile, params, mc);



params.largeShift = true;

[adcdata3, dataOK, params] =  ...
   readTimeSeriesData2t(channelName, dataStartTime, dataDuration, ...
                      frameType, frameDuration, gpsTimesFile, ...
                      frameCacheFile, params, mc);
