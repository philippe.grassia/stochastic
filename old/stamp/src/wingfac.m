function params = wingfac(params, pp)
% function params = wingfac(params)
% calculates params.wingfac = <std(SNR)>
  
  if length(params.numSegmentsPerInterval)==0
    fprintf('Caution: unable to determine numSegmentsPerInterval.\n');
    params.wingfac = 1;
  else
    % N=deltaF*deltaT     NSPI     std(SNR)
    table = [
             1 35 0.6731 ;
             1 17 0.6973 ;
             1 13 0.7101 ;
             1 9 0.7464 ;
             1 5 0.8702 ;
             1 3 1.2992 ;
             13 3 0.9877 ;
             15 3 0.9864 ;
             ];
    N = table(:,1);
    NSPI = table(:,2);
    N0 = pp.deltaF*params.segmentDuration;
    NSPI0 = params.numSegmentsPerInterval;
    if sum(N==N0&NSPI==NSPI0)==1
      params.wingfac = table(N==N0&NSPI==NSPI0, 3);
    else
      fprintf('Caution: look-up table incomplete; setting wingfac=1.\n');
      params.wingfac=1;
    end
  end
  
return;
