function params = stampDefaults()
% Sets default params.
% by E. Thrane

  % default name for output mat files
  params.outputfilename = 'map';

  % default is to do a frequency mask with no notches
  params.doStampFreqMask = 1;
  params.StampFreqsToRemove = [];
  params.StampnBinsToRemove = [];

  % The coarsegrained factor applies an additional normalization to sigma(f).
  % The default value is off.
  params.coarsegrained=0;

  % Physical constants
  params.G = 6.673e-11; % gravitational constant in m^3/kg/s^2
  params.c = 299792458;       % speed of light in m/s
  params.ErgsPerSqCM = 1000; % convert Joules/m^2 to ergs/cm^2

  % Define typical ranges for ft-map plots.
  params.yMapScale = 1e-43;
  params.FMapScale = 1e3;

  % Perform searches and set limits with fluence or power?  Default is power.
  params.fluence = 0;

  % Use glitch identification algorithm?  If so, which one?  The default is
  % to not perform any glitch identification.  glitchCut can take on values
  % of 0 through 8.  The standard Xi cut is 5.  The Xi cut plus DQ flags is
  % 8.
  params.glitchCut = 0;
  params.doCoincidentCut = 0;
  params.DQcut = [];

  % Diagnostic options.
  % multiply CSD by array of random phases
  params.phaseScramble = 0;
  % randomly scrambles ft-map pixels
  params.pixelScramble = 0;
  % {F} set to values for (ra,dec,lst) = (6,30,8.55)
  params.fixAntennaFactors=false;
  % Artificially set time-delay between IFOs to zero.
  params.NoTimeDelay=false;

  % Output and debug options.
  % save a mat file after running
  params.saveMat=false;
  % save eps and png images of ft-map plots.
  params.savePlots=true;
  % displays verbose error messages
  params.debug=false;
  % compare with radiometer algorithm
  params.doRadiometer=false;
  % calculate auto-power maps
  params.Autopower=false;

  % Record which segments have been vetoed with the glitch cut.
  params.recordCuts = 0;

  % Use simulated Monte Carlo noise
  params.doMC=false;

  % Polarization options
  % use a polarized filter
  params.doPolar=false;
  % use a pure plus filter
  params.purePlus=false;
  % use a pure cross filter
  params.pureCross=false;

  % Search and simulation options.
  % Radon transform
  params.doRadon=false;
  params.doRadonReconstruction=false;
  % box search
  params.doBoxSearch=false;
  % Locust and Hough search
  params.doLH=false;
  % density-based clustering (burstCluster)
  params.doClusterSearch=false;  
  % crunch_map is an option for burstCluster
  params.crunch_map = false;
  % burstegard search
  params.doBurstegard = false;

  % loop over sky position - requires that you also define params.dtheta
  % (search resolution) and params.thetamax (search cone size)
  params.skypatch=false;
  % loop over only directions with different phases at fmax
  params.fastring=false;

  % frompreproc option allows for stochmap to be called by preproc for MC
  % studies where we do not wish to write to / read from frames.  Normal 
  % usage (reading from frames) sets frompreproc=false.
  params.frompreproc = false;

  % stamp_pem option allows for strain - PEM correlation for detchar studies
  params.stamp_pem = false;

  % a discontinuitity is detected in the map (see addMissingDatatoMap)
  % this param is for internal use only
  params.discontinuity=false;

  % params.twojoblimit is used in addMissingDatatoMap.  It can be turned off if
  % you are requesting data that spans many maps and you are using segment
  % duration >2.  Otherwise, it should probably be left on.
  params.twojoblimit=true;

  % add injected power to map
  params.powerinj=false;

  % number of trials due to injection studies
  params.inj_trials=1;
  % number of injection strengths
  params.alpha_n=1;

  % background study
  params.bkndstudy=false;

  % record the loudest pixel
  params.loudPixel=false;

return;
