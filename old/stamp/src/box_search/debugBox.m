function debugBox(params,AllStartTimes,freq_list)
% function debugBox(params,AllStartTimes,freq_list)
% This code is used for debugging box search code. It can be used to test
% the injection by comparing the strength of the recovered injection
% and the position of injection in FT map.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% converting absolute GPS segment times to relative times
time_list = AllStartTimes - AllStartTimes(1);

% size of pixels in ft map
deltaF = freq_list(2) - freq_list(1);
deltaT = time_list(2) - time_list(1);

% size of search box
f_box = ceil(params.box.freq_width/deltaF);
t_box = ceil(params.box.time_width/deltaT);

nearest_start = max(AllStartTimes(AllStartTimes<=params.injection.start));
nearest_end_st = min(AllStartTimes(AllStartTimes+params.segmentDuration ...
                     >=params.injection.start+params.injection.duration))+ ...
                     params.segmentDuration;
tt = time_list(1):t_box*deltaT:time_list(end);
inj_box_t1 = max(tt(tt <= nearest_start - AllStartTimes(1)));
inj_box_t2 = min(tt(tt >= nearest_end_st - AllStartTimes(1)) - time_width);
cut_time = tt >= inj_box_t1 & tt <= inj_box_t2;
ff = freq_list(1):f_box*deltaF:freq_list(end);
cut_freq = ff >= params.injection.flow & ff <= params.injection.fhigh;
SNR_th =  SNR_Y(cut_freq,cut_time);
tt_print = tt(cut_time);
ff_print = ff(cut_freq);
fid = fopen('box_data.dat','w');
fprintf(fid,'%% The absolute GPS times can be obtained by adding %d \n', ...
         AllStartTimes(1));
fprintf(fid,'    F-T SNR\t\t');
for ii = 1:length(tt_print)-1
  fprintf(fid,'%d - %d \t', tt_print(ii), tt_print(ii+1));
end
fprintf(fid,'%d - %d \t',tt_print(end), tt_print(end) + time_width);
fprintf(fid,'\n');
for zz = 1: length(SNR_th(:,1))-1
  fprintf(fid,'%7.2f - %7.2f Hz\t', ff_print(zz),ff_print(zz+1));
  fprintf(fid, '%6.2f \t\t', SNR_th(zz,:));
  fprintf(fid, '\n');
end
  fprintf(fid,'%7.2f - %7.2f Hz\t', ff_print(end),ff_print(end)+freq_width);
  fprintf(fid, '%6.2f \t\t', SNR_th(end,:));
  fclose(fid);
end  

