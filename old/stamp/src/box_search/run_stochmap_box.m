clear all;
%path='/archive/home/mcoughlin/IM/GRB/Large_Wings/frames/';
%channel_name='L1_LSC-STRAIN';

%im_setDefaults; %Set the narrow band search defaults
params = stampDefaults;

params.segmentDuration=4;
params.intFrameCachePath='/archive/home/mcoughlin/IM/GRB/Gaussian_Noise/frames/';
jobsFile=['/archive/home/shivaraj/sgwb/S5/input/jobfiles/' ...
  'S5H1L1/S5H1L1_full_run.txt'];

% Search direction-------------------------------------------------------------
params.ra=6;   %right ascenion in hours (for search direction)
params.dec=30; %declination in degrees (for search direction)

% Frequency range--------------------------------------------------------------
params.fmin = 40;
params.fmax = 2000;

% Override default parameters--------------------------------------------------
params.debug=false;
params.kludge = sqrt(2);

% Call stochMap----------------------------------------------------------------
params = boxDefaults(params);
%stochMap(params, jobsFile, 866865447, 866866047);
kk = 1;
for ii=869994047:400:869994047+400
  max_SNR(kk) = my_stochmap(params, jobsFile, ii, ii+399) 
  startT = ii;
  endT = ii+399;
  kk = kk+1;
end
save('output1.mat','max_SNR','startT','endT');

