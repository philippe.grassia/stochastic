function [F1p, F1x, F2p, F2x, tau] = calF(det1, det2, GPSTimes, source, params)
% function [F1p, F1x, F2p, F2x, tau] = calF(params, det1, det2, GPS, source)
% E. Thrane - moved from ccSpecReadout_stamp
% calculates the antenna factors, e.g., F1p = F(det1, plus polarization)
% det1 and det2 are detector structs.
% GPSTimes is the array of mid-segment GPS times.
% source is an Nx2 matrix containing right ascension and declination for all N
% sky locations to look at.

try
  params.c;
catch
  params.c = 299792458;
end

% calculate sidereal time
time = GPStoGreenwichMeanSiderealTime(GPSTimes);

% convert hours into radians: 24 hrs / 2pi rad
w = pi/12;
N = length(GPSTimes);

% calculate ingredients needed for antenna factors
psi = w*(time-source(1,1));
theta = -pi/2+pi/180*source(1,2);
theta = theta*ones(size(psi));
ctheta = cos(theta);
stheta = sin(theta);
cpsi = cos(psi);
spsi = sin(psi);
Omega1 = -cpsi .* stheta;
Omega2 = spsi .* stheta;
Omega3 = ctheta;
pp11 = (ctheta.^2) .* (cpsi.^2) - (spsi.^2);
pp12 = -(ctheta.^2+1) .* cpsi .* spsi;
pp13 = ctheta .* cpsi .* stheta;
pp22 = (ctheta.^2) .* (spsi.^2) - (cpsi.^2);
pp23 = -ctheta .* spsi .* stheta;
pp33 = stheta.^2;
px11 = -2*ctheta .* cpsi .* spsi;
px12 = ctheta .* ((spsi.^2)-(cpsi.^2));
px13 = -stheta .* spsi;
px22 = 2*ctheta .* cpsi .* spsi;
px23 = -stheta .* cpsi;
%px33 = 0;

% calculate vector for the separation between sites.
s = det2.r - det1.r;

% distance between sites
distance = norm(s);

% time delay between detectors
tau=(Omega1*s(1)+Omega2*s(2)+Omega3*s(3))/params.c;
  
% define antenna factors
F1p = det1.d(1,1)*pp11+2*det1.d(1,2)*pp12 + 2*det1.d(1,3)*pp13+ ...
      det1.d(2,2)*pp22+2*det1.d(2,3)*pp23+det1.d(3,3)*pp33;
F2p = det2.d(1,1)*pp11+2*det2.d(1,2)*pp12 + 2*det2.d(1,3)*pp13+ ...
      det2.d(2,2)*pp22+2*det2.d(2,3)*pp23+det2.d(3,3)*pp33;
F1x = det1.d(1,1)*px11+2*det1.d(1,2)*px12 + 2*det1.d(1,3)*px13+ ...
      det1.d(2,2)*px22+2*det1.d(2,3)*px23;%+det1.d(3,3)*px33;
F2x = det2.d(1,1)*px11+2*det2.d(1,2)*px12 + 2*det2.d(1,3)*px13+ ...
      det2.d(2,2)*px22+2*det2.d(2,3)*px23;%+det2.d(3,3)*px33;

return;
