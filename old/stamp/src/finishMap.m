function [cmap, params] = finishMap(params, map)
  % copy over meta data
  cmap = map;

  % create array of frequency and times in final ft-map

  freqbins = [params.fmin:map.deltaF:params.fmax];
  Allsegstarttime=getFtmapGPStimes(params, freqbins, cmap.segstarttime);

  % standard ft-maps. addMissingDatatoMap cuts the map to the correct
  % frequency range and time segments 
  [cmap.y, params] = addMissingDatatoMap(params, map.y, map.f, freqbins, ...
    cmap.segstarttime, Allsegstarttime);
  cmap.z = addMissingDatatoMap(params, map.z, map.f, freqbins, ...
    cmap.segstarttime, Allsegstarttime);
  cmap.snr = addMissingDatatoMap(params, map.snr, map.f, freqbins, ...
    cmap.segstarttime, Allsegstarttime);
  cmap.snrz = addMissingDatatoMap(params, map.snrz, map.f, freqbins, ...
    cmap.segstarttime, Allsegstarttime);
  cmap.sigma = addMissingDatatoMap(params, map.sigma, map.f, freqbins, ...
    cmap.segstarttime, Allsegstarttime);

  if params.Autopower
    cmap.eps_12 = addMissingDatatoMap(params, map.eps_12, map.f, freqbins, ...
      cmap.segstarttime, Allsegstarttime);
    cmap.eps_11 = addMissingDatatoMap(params, map.eps_11, map.f, freqbins, ...
      cmap.segstarttime, Allsegstarttime);
    cmap.eps_22 = addMissingDatatoMap(params, map.eps_22, map.f, freqbins, ...
      cmap.segstarttime, Allsegstarttime);
  end

  % fluence maps
  cmap.F = addMissingDatatoMap(params, map.F, map.f, freqbins, ...
    cmap.segstarttime, Allsegstarttime);
  cmap.sigF = addMissingDatatoMap(params, map.sigF, map.f, freqbins, ...
    cmap.segstarttime, Allsegstarttime);

  % number of independent measurements
  map.nindep = sum(sum(map.sigma ~= 1));

return
