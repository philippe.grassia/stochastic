function [cmap, params] = finishMapRawData(params, map)
% function cmap = finishMapRawData(params, map)
% adds missing data to ft-maps for auto-power maps.

  cmap = map;

  % ************************
  % Add missing data to maps
  % ************************
  freqbins = [params.fmin:map.deltaF:params.fmax];

  Allsegstarttime=getFtmapGPStimes(params, freqbins, cmap.segstarttime);

  [cmap.naiP1, params] = addMissingDatatoMap(params, map.naiP1, map.f, ...
    freqbins, cmap.segstarttime, Allsegstarttime);
  cmap.naiP2 = addMissingDatatoMap(params, map.naiP2, map.f, freqbins, ...
                                   cmap.segstarttime, Allsegstarttime);
  cmap.P1 = addMissingDatatoMap(params, map.P1, map.f, freqbins, ...
                                cmap.segstarttime, Allsegstarttime);
  cmap.P2 = addMissingDatatoMap(params, map.P2, map.f, freqbins, ...
                                cmap.segstarttime, Allsegstarttime);

  cmap.xvals =  Allsegstarttime-Allsegstarttime(1)+map.segDur/4;
  cmap.yvals = freqbins;

return
