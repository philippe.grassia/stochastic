STEPS FOR DOING A FREQ-DOMAIN POWER INJECTION FROM SCRATCH
by Eric Thrane

* Run injproc.m to create a zero-noise version of the injection.  The example
in this directory can be run like this:

   injproc('inj_params.txt', 'S5H1L1_full_run.txt', 18798)

injproc is basically a modified version of preproc.  It is almost identical,
except it never reads in frame data.  Instead, it just creates arrays filled
with zeros to represent h(t) in each interferometer.  This way, when the
injection is added, we are left with pure injection.

* A word of CAUTION: I have hard-coded injproc to work for 1s x 1Hz pixels.  If
you want to look at a different pixel size, you will need to modify it to be
more general.  Search for the string "EHT" to see the four places you will need
to make edits.

* The paramfile above is set up to call stochmap from preproc.  stochmap will
create a file called: map_1.mat file.  You should rename this file to be
   M10a0.95eps0.1_10Mpc.mat 
or whatever.  The file name should have something to do with the injection that
is stored within.

* The paramfile is also set to make diagnostic plots.  Take a look at y_map.png
and make sure that you can see the injection and that it looks correct.

* Note: make sure to leave the Autopower flag on in the paramfile.  We will
need the autopower saved in order to do injections later.

-------------------------------------------------------------------------------

OK, we have created a mat file characterizing the injection, and now we're
ready to use it to perform an injection.
