clear all;

% initialize default parameters
params = stampDefaults;

params.intFrameCachePath = '../../test/cachefiles/';
jobsFile=['./S5H1L1_full_run.txt'];

% interferometer pair
params.ifo1='H1';
params.ifo2='L1';

% search direction-------------------------------------------------------------
params.ra=6;   %right ascenion in hours (for search direction)
params.dec=30; %declination in degrees (for search direction)

% frequency range--------------------------------------------------------------
params.fmin = 100;
params.fmax = 500;

% override default parameters--------------------------------------------------
params.debug=true;
params.saveMat=true;
params.savePlots=true;

% testing isnan effect from notching
params.doStampFreqMask=true;
params.StampFreqsToRemove = [119 120 121];
params.StampnBinsToRemove = [1 1 1];

params.Autopower=true;
params.yMapScale = 5e-45;
params.fluence=1;
params.FMapScale=0.05;

% burstCluster params
params.doClusterSearch=true;
params.glitchCut = 5;
params.cluster.doCombineCluster = true;
params.cluster.NN = 14;    % minimum number of neighbours
params.cluster.NR = 4;     % neighbor radius as determined by metric
params.cluster.NCN = 10;   % minimum number of pixels in a cluster
params.cluster.NCR = 8;   % distance between neighbouring clusters
params.cluster.pixelThreshold = 1;
params.cluster.tmetric = 2.4;
params.cluster.fmetric = 0.75;

% create loop over search directions
params.skypatch = false;
params.fastring = false;
params.dtheta=0.25;
params.thetamax = 10;

% power injection
params.powerinj=true;
params.injfile = 'M10a0.95eps0.1_10Mpc.mat';
params.inj_trials=1;
params.alpha_n=1;
params.alpha_min=6;
params.alpha_max=6;

% Call stochMap----------------------------------------------------------------
stoch_out=stochmap(params, jobsFile, 816065699, 816065810);  % job 1

