%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

doFreqMask false
doHighPass1 true
doHighPass2 true
doOverlap true

doSidereal false

minDataLoadLength 200
 
doBadGPSTimes false
maxDSigRatio 1.2
minDSigRatio 0.8

doShift1 true
ShiftTime1 1
doShift2 false
ShiftTime2 0

ifo1 H1
ifo2 L1

segmentDuration 1.00

numSegmentsPerInterval 9
ignoreMidSegment true

flow 40
fhigh 2000
deltaF 1.00

alphaExp 0
fRef 100

resampleRate1 4096
resampleRate2 4096

bufferSecs1 2
bufferSecs2 2

ASQchannel1 LSC-STRAIN
ASQchannel2 LSC-STRAIN

frameType1 H1_RDS_C03_L2
frameType2 L1_RDS_C03_L2
frameDuration1 -1
frameDuration2 -1

hannDuration1 1.00
hannDuration2 1.00

nResample1 10
nResample2 10
betaParam1 5
betaParam2 5

highPassFreq1 32
highPassFreq2 32
highPassOrder1 6
highPassOrder2 6

freqsToRemove 
nBinsToRemove 
 
alphaBetaFile1 none
alphaBetaFile2 none
calCavGainFile1 none
calCavGainFile2 none
calResponseFile1 none
calResponseFile2 none

simOmegaRef 0
heterodyned false

gpsTimesPath1 /home/ethrane/cache/S5H1L1/
gpsTimesPath2 /home/ethrane/cache/S5H1L1/
frameCachePath1 /home/ethrane/cache/S5H1L1/
frameCachePath2 /home/ethrane/cache/S5H1L1/

DoStampInj false
StampInjRA 6
StampInjDECL 30


doDetectorNoiseSim false
DetectorNoiseFile LIGOsrdPSD_40Hz.txt

outputFilePrefix results/0/frames/S5H1L1_inj

stochmap true
fft1dataWindow -1
fft2dataWindow -1
startGPS 816065659
endGPS 877591542
kludge 1
ra 6
dec 30
fmin 100
fmax 1200
doPolar false
saveMat false
savePlots false
debug false
doRadon false
doBoxsearch false
doLH false
doRadiometer false
doRadonReconstruction false
saveAutopower false
fixAntennaFactors false
doClusterSearch true
phaseScramble 0
glitchCut 5
bknd_study true
bknd_study_dur 100
Autopower true 
pp_seed -1

skypatch false
fastring true
dtheta 0.25
thetamax 10

cluster.NN 14
cluster.NR 4
cluster.NCN 10
cluster.NCR 8
cluster.pixelThreshold 1
cluster.tmetric 2.4
cluster.fmetric 0.75
cluster.doCombineCluster true

skypatch false
fastring false
dtheta 0.25
thetamax  5
