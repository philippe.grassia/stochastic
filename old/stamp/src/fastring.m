function source = fastring(det1, det2, GPS, source, params)
% function source = fastring(det1, det2, GPS, source, params, fmax)
% E. Thrane - modified code from S. Ballmer.  fastring takes an
% array of ra,dec and shortens the list by excising all entries 
% that are close enough to another search direction such that the 
% phase error is less than pi/4 at fmax.  This corresponds to a 
% 30% loss of power.
%
% Arguments: det1,det2  -  structures for the 2 detectors
%            GPS        -  GPS time used to calculate antenna
%                          pattern
%            source     -  Nx2 matrix containing right ascension and
%                          declination for all N points to be looked at.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Require that loss of power is less than 30%.
DPhaseMax = pi/4;

% M = Number of search directions.
M = size(source,1);

% Define vector separation of the two detectors.
s = det2.r - det1.r;

% Convert GPS time to hours.
time = GPStoGreenwichMeanSiderealTime(GPS);

% Other parameters
c = params.c;
w = pi/12;

% Preallocate 'phase' and 'phaseR' arrays.
phase = zeros(size(M));
phaseR = zeros(size(M));

% Loop over search directions.
for ii=1:M
  % Calculate search angles and unit vector in that direction.
  psi = w*(time-source(ii,1));
  theta = -pi/2+pi/180*source(ii,2);
  r0 = [-cos(psi)*sin(theta) sin(psi)*sin(theta) cos(theta)];

  % Time delay between detectors.
  tau = (r0*s)/c;
  
  % Calculate phases at fmax.
  phase(ii) = 2*pi*i*params.fmax*tau;

  % Round phase to nearest DPhaseMax.
  phaseR(ii) = round(phase(ii)/DPhaseMax)*DPhaseMax;
end

% Find unique rounded phase values.
[dummy,idx] = unique(phaseR);
idx = sort(idx);

% Keep only directions associated with unique phases.
source = source(idx,:);
  
return