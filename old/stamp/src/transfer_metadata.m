function map = transfer_metadata(params, map)
% function map = transfer_metadata(params, map)
% E. Thrane: this function performs book-keeping operations on map.Sky{kk}.map
% by referring map metadata.
%
  for kk=1:length(map.Sky)
    % copy metadata
    map.Sky{kk}.map.xvals = map.xvals;
    map.Sky{kk}.map.yvals = map.yvals;
    map.Sky{kk}.map.deltaF = map.deltaF;
    map.Sky{kk}.map.segstarttime = map.segstarttime;
    map.Sky{kk}.map.f = map.f;
    map.Sky{kk}.map.segDur = map.segDur;

    % leave NaN values in place
%eht
%    map.Sky{kk}.map.y(isnan(map.Sky{kk}.map.y)) = 0;
%    map.Sky{kk}.map.sigma(isnan(map.Sky{kk}.map.sigma)) = 1;
%    map.Sky{kk}.map.z(isnan(map.Sky{kk}.map.z)) = 0;
%    map.Sky{kk}.map.sigF(isnan(map.Sky{kk}.map.sigF)) = 1e99;
%    map.Sky{kk}.map.F(isnan(map.Sky{kk}.map.F)) = 0;
    
    % calculate SNRs
    map.Sky{kk}.map.snr = map.Sky{kk}.map.y./map.Sky{kk}.map.sigma;
    map.Sky{kk}.map.snrz = map.Sky{kk}.map.z./map.Sky{kk}.map.sigma;

    % finish book-keeping
    try
    map.Sky{kk}.map = finishMap(params, map.Sky{kk}.map);
    catch
      warning('Unable to execute finishMap in multi-direction mode.');
    end
  end
return;


