clear all;

% initialize cachefiles
%!./chk_cache.pl
%fprintf('\nYour cachefiles have been initialized. ');

% initialize default parameters
params = stampDefaults;

params.intFrameCachePath = './cachefiles/';
jobsFile=['./S5H1L1_full_run.txt'];

% interferometer pair
params.ifo1='H1';
params.ifo2='L1';

% search direction-------------------------------------------------------------
params.ra=6;   %right ascenion in hours (for search direction)
params.dec=30; %declination in degrees (for search direction)

% frequency range--------------------------------------------------------------
params.fmin = 100;
params.fmax = 250;

% override default parameters--------------------------------------------------
params.debug=true;
params.saveMat=true;
params.savePlots=true;
params.doFreqMask=false;
params.Autopower=true;
params.yMapScale = 5e-42;

% burstCluster params
params.doClusterSearch=true;
params.doGlitchcut = false;
params.cluster.doCombineCluster = true;
params.cluster.NN = 14;    % minimum number of neighbours
params.cluster.NR = 4;     % neighbor radius as determined by metric
params.cluster.NCN = 10;   % minimum number of pixels in a cluster
params.cluster.NCR = 8;   % distance between neighbouring clusters
params.cluster.pixelThreshold = 1;
params.cluster.tmetric = 2.4;
params.cluster.fmetric = 0.75;

% create loop over search directions
params.skypatch = true;
params.fastring = true;
params.dtheta=0.25;
params.thetamax = 10;

% Call stochMap----------------------------------------------------------------
stoch_out=stochmap(params, jobsFile, 816065699, 816065710);  % job 1

