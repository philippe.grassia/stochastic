function [muhat,sigmahat] = stampHist(map,minimum,maximum,binwidth,xstring,ystring,log,norm)

% This routine will make histogram of a given data set and is
% able to display it in semilog format.
% log = 1 for semilog, log = 0 for normal.
% norm = 1 for normalized, norm = 0 for non-normalized
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Remove non-finite values from the array; set range.
map_vec = map(:); 
cut = isfinite(map_vec);
map_vec = map_vec(cut);
muhat = mean(map_vec);
sigmahat = std(map_vec);

% Define histogram range.
full_range = (floor(min(map_vec))-10):binwidth:(ceil(max(map_vec))+10);
xrange = (minimum:binwidth:maximum);

% Make histogram.
out_pdf = hist(map_vec,full_range);


% Normalize (if specified).
if (norm == 1)
  out_pdf = out_pdf/(length(map_vec)*binwidth);
end

% Chop pdf to specified range.
x1 = find(full_range==xrange(1));
xend = find(full_range==xrange(end));
out_pdf = out_pdf(x1:xend);

figure
hb = bar(xrange,out_pdf);

fsize = 14;
set(gca,'FontSize',fsize);
h_xlabel = get(gca,'XLabel');
set(h_xlabel,'FontSize',fsize);
%line([11 11],[0 1],'Color','k','LineStyle','--')


% Problems occur with semilog bar plots since the bottoms
% of the bars are set to 0.  This changes the bottoms to 
% a reasonable value and sets the log scale.
if (log == 1)
  hbars = get(hb,'children');
  verts = get(hbars,'vertices');
  bot_cut = (out_pdf==0);
  col_bot = min(out_pdf(~bot_cut))/2;
  verts(verts==0) = col_bot;
  set(hbars,'vertices',verts);
  set(gca,'yscale','log');
  axis([minimum maximum min(out_pdf*0.5) max(out_pdf*1.4)]);
else
  axis([minimum maximum 0 max(out_pdf)*1.1]);
end;

% Label x and y axes.
xlabel(xstring);
ylabel(ystring);

% Place the mean and rms text appropriately.
x_loc = minimum + 0.78*(maximum-minimum);
if (log == 1)
  y_mu = max(out_pdf)*0.90;
  y_sig = max(out_pdf)*0.58;
else
  y_mu = max(out_pdf)*1.02;
  y_sig = max(out_pdf)*0.97;
end
t1=text(x_loc,y_mu,['\mu = ' num2str(muhat)]);
t2=text(x_loc,y_sig,['\sigma = ' num2str(sigmahat)]);
set(t1,'FontSize',fsize*6/7);
set(t2,'FontSize',fsize*6/7);

pretty;

return
