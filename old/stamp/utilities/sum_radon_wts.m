function npix = sum_radon_wts(map, theta, b)
% function sum_radon_wts(map, theta, b)
% (theta,b) = location or Radon map maxima
%

  if any(theta>=180) || any(theta<=0)
    error('sum_radon_wts: theta must be >0 and <=180.');
  end

  npix = 0;

  [N M] = size(map);

  % center of the image
  m = round(M/2);
  n = round(N/2);

  % The total number of rho's is the number of pixels on the diagonal, since
  % this is the largest straight line on the image when rotating
  rhomax = ceil(sqrt(M^2 + N^2));

  rc = round(rhomax/2);
  mt = max(theta);
  rmap = cast(zeros(rhomax, mt), 'double');

  t = theta
    a = -cot(t*pi/180);

      rho = b*sin(t*pi/180);

      if t<=45
        qmax = min(round(-a*m+b), n-2);
        qmin = max(round(a*m+b), -n);
      elseif t>45 && t<=90
        qmax = min(round((-n-b)/a),m-2);
        qmin = max(round((n-b)/a),-m);
      elseif t>90 && t<=135
        qmax = min(round((n-b)/a),m-2);
        qmin = max(round((-n-b)/a),-m);
      else % t>135 && t<180
        qmax = min(round(a*m+b),n-2);
        qmin = max(round(-a*m+b),-n);
      end

      for q = qmin:qmax
        if t<=45 || t>135
          y = q;
          p = floor( (y-b)/a );
          x = p;
        else
          x = q;
          p = floor( a*x+b );
          y = p;
        end

        pup = p - floor(p);
        plow = 1 - pup;

        if x>=m-3, x=m-3; end;
        if x<-m, x=-m; end;
        if y>=n-2, y=n-2; end;
        if y<-n, y=-n; end;

%        rmap(rhomax-r+1, mt-t) = rmap(rhomax-r+1, mt-t) + ...
%          plow * map(y+n+1, x+m+1) + pup * map(y+n+1,x+m+1+1);
        npix = npix + plow + pup;

     end

rhoaxis = (1:rhomax+1) - rc;

