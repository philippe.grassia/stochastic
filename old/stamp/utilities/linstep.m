function [xo,yo] = linstep(f, hbinning, islog, style, th_detect)
% function [xo,yo] = linstep(f, hbinning, islog, style, th_detect)
% This function produces stair plot, also has the option to make log
% stair plot, works the same way as hist(), but has more options.
% hbinning is about how to bin the data; it can be just a number of bins,
%          a vector of two elements(min and max of binning) or a vector 
%          of three elements(min and max of binning and # of bins);
%          default is 100 bins
% if islog =1, it produces log stair plot
% style is the color plot, e.g., 'g-'; default is 'b-'
% if th_detec (detection threshold given), it produces the plot above
%        that threshold red in color.
% If the output results [xo,yo] are not requested it makes the plot 
% else it gives xo and yo which can then be used to produce the plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% checking the argumnents
if(nargin < 1)  % error
  error('step() needs at least 1 argument');
return;
end;	
if(nargin == 3) 
  style = 'b-';
end;
if(nargin == 2)
  islog = 0;
  style = 'b-';
end;
if(nargin ==1)
  if(length(f)>=10000)
    hbinning = 100;
  elseif(length(f)>=500)
    hbinning = ceil(f/20)
  else
   hbinning = 10;
  end
  islog = 0;
  style = 'b-';
end;
if(isempty(hbinning))
  if(length(f)>=2000)
    hbinning = 100;
  elseif(length(f)<2000 && length(f)>=500)
    hbinning = ceil(length(f)/20);
  else
   hbinning = 10;
  end
end;
styleLinewidth = 2;     % Default linewidth
if( length(hbinning)<4 )
  if( length(hbinning)==1 ) % the parameter hbinning is number of bins
     hbinning = linspace( min(f(isfinite(f))) , max(f(isfinite(f))) , hbinning );
  elseif( length(s)==2 ) % hbinning is min and max 
     hbinning = linspace( hbinning(1) , hbinning(2) , 100);
  elseif( length(s)==3 ) % hbinning is min and max and # of steps
     hbinning = linspace( hbinning(1) , hbinning(2) , hbinning(3));
  end
  % Add edge bins, since they are removed later when tidying plot up
  hbinning = [2*hbinning(1)-hbinning(2) hbinning 2*hbinning(end)-hbinning(end-1)];
  hbinning = [2*hbinning(1)-hbinning(2) hbinning 2*hbinning(end)-hbinning(end-1)];
end

[n , x] = hist(f, hbinning);
	
% Histogram routine returns binned data such that (i)th bin contains
% data in range f(i)-1/2 delta -> f(i)+1/2 delta where delta represents
% half the gap to the adjacent bins
% stairs() is dumb and puts the left hand end of the flat part of n(i) at the
% value x(i). We would rather have x(i) at the middle.
x = x(:)';
x = x - [0 diff(x)/2];
 
% Drop the bins at end since they contain overflow points
n = n(2:length(n)-1);
x = x(2:length(x)-1);
	
[xx,yy] = stairs(x, n);	% Convert x and y points into stair structure

if( exist('th_detect') ~=0 )
  xx_bad = xx(xx >= th_detect);
  yy_bad = yy(xx >= th_detect);
end

if (nargout == 0)
  if( ~islog )
     plot(xx, yy, style, 'linewidth', styleLinewidth);
     if( exist('th_detect') ~=0 )
       hold on
       plot(xx_bad, yy_bad, 'r-', 'linewidth', styleLinewidth);  
       hold off
     end
  else
     % logy plot
     semilogy(xx, yy+0.1, style, 'linewidth', ...
			styleLinewidth);
     if( exist('th_detect') ~=0 )
       hold on
       semilogy(xx_bad, yy_bad+0.1, 'r-', 'linewidth', ...
                        styleLinewidth);
       hold off
     end
   end
   axis([min(xx) max(xx)*1.2 5e-1 1.1*max(yy)]);
else
  xo = xx; 
  yo = yy;
end

return;
