clear all;
datafile = 'M10a0.95eps0.2.dat';

file = load(datafile);
t=file(:,1);  % time
hp=file(:,2); % h_+
hx=file(:,3); % h_x

% check for even sampling rate
tmp = (t(2:end)-t(1:end-1)) - (t(2)-t(1));
if any(tmp>=1/16384)>0,  error('Data must be evenly sampled.');  end

% calculate PSDs
Fs=1/(t(2)-t(1));  nfft=length(t);
[Pp,f] = psd(hp,nfft,Fs);  [Px,f] = psd(hx,nfft,Fs);
Pp=Pp/Fs;  Px=Px/Fs;

% calculate frequency resolution
deltaF = f(2)-f(1);

% consider only initial LIGO frequencies
cut2 = f>=40&f<=1800;
f2=f(cut2);  Pp2=Pp(cut2);  Px2=Px(cut2);

% sum over frequencies
fac = deltaF;
H = fac*sum(Pp+Px);
E_GW = fac*sum(f.^2.*(Pp + Px));
E_band = fac*sum(f2.^2.*(Pp2 + Px2));

% scale by appropriate constants
c=3e8;  G=6.67e-11;
D = 3.09e24;  % 1 Mpc in cm
ErgsPerSqCM = 1000; % convert Joules/m^2 to ergs/cm^2
E_GW = ErgsPerSqCM*(pi^2*c^3*D^2/G) * E_GW;
E_band = ErgsPerSqCM*(pi^2*c^3*D^2/G) * E_band;

% output result
fprintf('H = %1.1e strain^2 at %1.1e Mpc\n', H,D/3.09e24);
fprintf('E_GW = %1.1e ergs at %1.1e Mpc\n', E_GW,D/3.09e24);
fprintf('%1.1e%% is out of band\n',100*(1-E_band/E_GW));

