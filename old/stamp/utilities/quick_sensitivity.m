function g = quick_sensitivity(gps, ra, dec)
%function g = quick_sensitivity(gps, ra, dec)
% returns antenna factors for LHO and LLO
  det1 = getdetector('LHO');
  det2 = getdetector('LLO');
  siderealtime = GPStoGreenwichMeanSiderealTime(gps);
  g=orfIntegrandSymbolic(det1,det2,siderealtime, ra, dec);

return
