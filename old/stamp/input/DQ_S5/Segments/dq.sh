#! /bin/bash


# This script concatenates the S5 HM DQ flags.
# H1, H2, and L1 were obtained from /home/mabizoua/review_hm/dq/dq_padded/ with commands like:
# [mcoughlin@ldas-grid Segments]$ gsiscp -r sugar-dev1.phy.syr.edu:/home/mabizoua/review_hm/dq/dq_padded/cat2 .
# V1 were obtained from https://wwwcascina.virgo.infn.it/DataAnalysis/VDBdoc/INSPIRAL/VSR1_DQ_INSPIRAL_LIST_20081126.html
# For consistency with LIGO vetoes, I am including V1 injection flags in CAT2

for ifo in 'H1' 'H2' 'L1' 'V1'
do

    for cat in '1' '2' '3' '4'
    do 
        echo ${ifo}:'cat'${cat}

        segexpr "union(cat${cat}/${ifo}*)" > S5-${ifo}_cat${cat}.txt
    done
done

