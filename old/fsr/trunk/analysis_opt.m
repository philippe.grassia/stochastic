function result = analysis_opt(infile)
% Optimal Filter analysis program
%
% $Id: analysis_opt.m,v 1.1 2006-10-18 16:40:25 tobin Exp $

%% Load the input

% Load the results of the data reduction stage.
if isstr(infile),
    vars = load(infile);
else
    vars = infile;
end

version = vars.version;
version.analysis = '$Id: analysis_opt.m,v 1.1 2006-10-18 16:40:25 tobin Exp $';
parameters = vars.parameters;

%% Compute some standard variables

n_frames     = length(vars.frameList);
n_segments   = n_frames * parameters.segs_per_frame;
seg_duration = parameters.frame_duration / parameters.segs_per_frame;
seg_samples  = seg_duration * parameters.samprate;

bw = 1/seg_duration;
f  = linspace(-parameters.samprate/2, parameters.samprate/2, seg_samples);
f  = f + parameters.het;
x  = find(abs(f - parameters.fsr) <= parameters.roi_halfwidth);

%% Look up and apply relative calibrations (Alpha coefficients)

% Get the timestamp from the filename

filename = vars.frameList{1};

if strcmp(filename,'DUMMY'),
    gpstime = NaN;
    if parameters.real_data,
        error('Real data desired, but dummy filename given.');
    end
else
    pattern = ['^(.*/)*(?<observatory>[A-Z]+)-(?<frame_type>[A-Z0-9_]+)-' ...
        '(?<gpstime>[0-9]+)-(?<duration>[0-9]+)\.gwf$'];
    n = regexp(filename, pattern, 'names');
    if (length(n)~=1),
        warning('Could not parse frame filename.');
        gpstime = 0;
    else
        gpstime = str2num(n(1).gpstime) + parameters.frame_duration/2;
        if (str2num(n(1).duration) ~= parameters.frame_duration)
            warning('Frame duration inferred from filename contradicts parameter.');
        end
    end
end

% Look up the alpha coefficients for H1 and H2 for this time stamp

if parameters.real_data,
    [calib_gpstime1 alpha1] = get_calib_coeff(gpstime, 'H1');
    [calib_gpstime2 alpha2] = get_calib_coeff(gpstime, 'H2');

    % Verify that we retrieved sufficiently recent calibration coefficients

    if (abs(calib_gpstime1 - gpstime) > 300) || ...
       (abs(calib_gpstime1 - gpstime) > 300),
        warning('Calibration gpstime differs from frame gpstime by more than 5 minutes.');
    end
else
    alpha1 = 1;
    alpha2 = 1;
end

% If either of the alpha coefficients is out of range, we exclude this data.

if (alpha1 < 0.5 || alpha2 < 0.5)
    result = 0;
    return;
end

% Divide the cross correlation by the product of the alpha coefficients

vars.cross             = vars.cross/(alpha1*alpha2);
vars.cross_parts_sigma = vars.cross_parts_sigma/(alpha1*alpha2);

% Divide each PSD by the pertinent alpha^2

vars.psd1 = vars.psd1/alpha1^2;
vars.psd2 = vars.psd2/alpha2^2;

%% Optimal Filter

% Get the interferometers' response functions

resp1   = HL(parameters.H1.cavpole, f, parameters.H1.armlength);
resp2   = HL(parameters.H2.cavpole, f, parameters.H2.armlength);
resp_cc = conj(resp1) .* resp2; 

% Form "calibrated" power spectral densities

cpsd1  = vars.psd1  ./ transpose(abs(resp1).^2);
cpsd2  = vars.psd2  ./ transpose(abs(resp2).^2);
ccross = vars.cross ./ transpose(resp_cc);

% Declare the width of our rectangles for numerical integration

df = (max(f(x))-min(f(x))) / length(x);

% Form the optimal filter 

% QT = transpose(abs(resp_cc).^2);  % Theoretical optimal filter
% QO = 1 ./ (cpsd1 .* cpsd2);       % Measured optimal filter (BROKEN)
% QU = ones(size(cpsd1));           % Unity filter (not optimal)

% Load the filter

load(['filter-' (parameters.n_fsr+'0') 'fsr'],'Q');

% Normalize the optimal filter over the region of interest

Q = Q / sum(Q(x) * df);

% Apply the filter

filtered_cross = ccross .* Q;

% Compute our statistic by integrating over the region of interest

stat = sum(filtered_cross(x) * df);

% Compute the theoretical error on the statistic

total_time = n_segments * seg_duration;
sigma_integrand = cpsd1 .* cpsd2 .* abs(Q).^2;
sigma = sqrt(sum(sigma_integrand(x) * df) / total_time)

%% Apply the final calibrations to stat and sigma

strainsq_per_countsq = 1/(parameters.H1.C * parameters.H2.C);

stat  = stat * strainsq_per_countsq;
sigma = sigma * strainsq_per_countsq;

%% Produce some other diagnostics

% Integrate the power for H1 and H2 to produce some diagnostics

ps1 = sum(vars.psd1(x)) * df;
ps2 = sum(vars.psd2(x)) * df; 

%% Compose our return value

result.n_frames   = n_frames;
result.stat       = stat;
result.sigma      = sigma;
result.gpstime    = gpstime;
result.alpha1     = alpha1;
result.alpha2     = alpha2;
result.ps1        = ps1;
result.ps2        = ps2;

% Additionally, output the results to standard output (so that it will
% appear in the log files)

fprintf('ANALYSIS_OPT\t%d,%g,%g,%g,%g,%g,%g,%g,%g\n', ...
	result.n_frames, real(result.stat), imag(result.stat), ...
	result.sigma, result.gpstime, result.alpha1, result.alpha2, ...
	result.ps1, result.ps2);

return;
