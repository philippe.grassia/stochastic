%% Compute our variances

parameters = physparms();

f = linspace(-parameters.samprate/2, parameters.samprate/2, ...
             parameters.samprate * parameters.frame_duration);

resp1   = HL(parameters.H1.cavpole, f, parameters.H1.armlength);
resp2   = HL(parameters.H2.cavpole, f, parameters.H2.armlength);
resp_cc = conj(resp1) .* resp2; 

T = parameters.frame_duration;
%%
midpoint = floor(length(resp_cc)/2);
variances = abs(resp_cc(midpoint:length(resp_cc))).^2;
N = length(variances);

inverse_variances = 1./variances;

total_sigmas_optimal = 1./cumsum(inverse_variances);

total_sigmas_unity = (1./(1:N).^2).*cumsum(variances);

%% Make a display
max_variance=max(max(total_sigmas_optimal),max(total_sigmas_unity));
semilogy((1:N)/T,total_sigmas_optimal/max_variance, '-', ...
       (1:N)/T,total_sigmas_unity/max_variance, '--');
legend('optimal filter','unity filter');
xlabel('roi halfwidth');
ylabel('relative variance');
grid on;

%%

theoretical_ratio = ((1:N).^-2).*cumsum(variances).*cumsum(inverse_variances);
calculated_ratio = total_sigmas_unity./total_sigmas_optimal;

%plot((1:N)/T, calculated_ratio);

if any(abs(calculated_ratio - theoretical_ratio) > 1e-10)
    warning('theoretical ratio and calculated ratio do NOT agree');
else
    fprintf('theoretical ratio and calculated ratio DO agree\n');
end