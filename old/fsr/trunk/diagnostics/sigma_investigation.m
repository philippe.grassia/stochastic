parameters = physparms();
parameters.real_data = 0;
strainsq_per_countsq = 1/(parameters.H1.C * parameters.H2.C);

input  = 1e-43;

%stats = [];
%sigmas = [];

measurements = 500;

for j=1:measurements,
    parameters.injection_fd = input / strainsq_per_countsq;
    results = leafnode(parameters,'DUMMY');
    stats = [stats results.analysis_opt.stat];
    sigmas = [sigmas results.analysis_opt.sigma];
    
    angle   = linspace(0,2*pi,1000);
    
    circle_x = std(real(stats)) * cos(angle) + real(mean(stats));
    circle_y = std(imag(stats)) * sin(angle) + imag(mean(stats));
    
    subplot(2,1,1);
    plot(real(stats),imag(stats),'x', ...
         circle_x, circle_y, '-', ...
         mean(real(stats)) + [-mean(sigmas), mean(sigmas)], [1 1]*mean(imag(stats)),'x-');
    h1 = gca();
    legend(sprintf('measurements (mean = %g)',mean(stats)), '"one sigma', ...
        sprintf('"theoretical" error bar (mean \\sigma = %g)', mean(sigmas)), ...
        'Location','Best');
    subplot(2,1,2);
    histfit(real(stats)/input);
%    set(h1,'XLim',get(gca, 'XLim'));
    legend(...
         sprintf('measurements (%d)', length(stats)), ...
         sprintf('\\mu = %g\n \\sigma=%g',mean(real(stats)),std(real(stats))));
    drawnow;
end