function mkfigures_inj(cinput, stats, sigmas)

%%

n_frames     = size(stats, 1);
n_injections = size(stats, 2);

combined_stats  = sum(stats .* sigmas.^-2, 1) ./ sum(sigmas.^-2, 1);
combined_sigmas = sum(sigmas.^-2, 1).^-(1/2);

uncombined_cinput = ones(n_frames, 1) * cinput;

%%

all_stats  = reshape(stats, [1 prod(size(stats))]);
all_sigmas = reshape(sigmas, [1 prod(size(sigmas))]);
uncombined_cinput = reshape(uncombined_cinput, [1 prod(size(uncombined_cinput))]);

rstats = real(all_stats);
istats = imag(all_stats);

subplot(1,1,1);
% Put on "experimental" error bars

upper_limit = mean(real(stats),1) + 1.645 * std(real(stats),0,1);
lower_limit = mean(real(stats),1) - 1.645 * std(real(stats),0,1);

upper_limit(find(upper_limit<0))=1e-200;
lower_limit(find(lower_limit<0))=1e-200;

shadeplot(cinput, upper_limit, lower_limit, [0.8 0.8 0.8]);
hold on;

% Put on "theoretrical" error bars
upper_limit = real(combined_stats) + 1.645 * combined_sigmas;
lower_limit = real(combined_stats) - 1.645 * combined_sigmas;

upper_limit(find(upper_limit<0))=1e-200;
lower_limit(find(lower_limit<0))=1e-200;

shadeplot(cinput, upper_limit, lower_limit, [0.5 0.5 0.5]);

set(gca, 'YScale', 'log', 'XScale', 'log');
set(gca, 'YLim', get(gca, 'XLim'));

hold all;
%plot(uncombined_cinput, rstats,    '.',  'MarkerSize', 10, 'Color', 'b');
plot(cinput, real(combined_stats), '.-', 'MarkerSize', 20, 'Color', 'g');
plot(cinput, cinput, 'k--');
%plot(cinput, combined_sigmas, '-');
hold off;        

grid on;    
legend('standard deviation of statistics',  ...
       '90% confidence interval', ...
       'point estimates', ...
       'perfect recovery', ...       
       'Location','NorthWest');
title(sprintf('Injections (%d frames)', n_frames));
xlabel('Injected strain^2/Hz');
ylabel('Recovered strain^2/Hz');
drawnow;

print(gcf, '-dpng',   'injections-series.png');
print(gcf, '-depsc2', 'injections-series.eps');

%% Check recovery of sigmas

% The square root of two in the following comes because 'stats' is a
% complex quantity.

measured_sigmas_ci = zeros(2, n_injections);
for inj = 1:n_injections,
      [foo,foo,foo,SIGMACI] = normfit(stats(:,inj));
      measured_sigmas_ci(:, inj) = SIGMACI;
end           
clear foo;
clear SIGMACI;

measured_sigmas_ci =  measured_sigmas_ci / (sqrt(2 * size(stats,1)));
measured_sigmas    =  std(stats) / (sqrt(2 * size(stats,1)));
subplot(2,1,1);
h = shadeplot(cinput, measured_sigmas_ci(2,:), measured_sigmas_ci(1,:), [0.5 1 0.5]); hold on;
set(h,'LineStyle','none');
semilogx(cinput, combined_sigmas, 'x-', ...
         cinput, measured_sigmas, 'o-');
set(gca,'XScale','log');     
hold off;
xlim([min(cinput) max(cinput)]);
legend('"experimental sigma" -- 95% confidence limits', '"theoretical sigma"','"experimental sigma"', 'Location','NorthWest');   
ylabel('\sigma');
%xlabel('injected signal strength (strain/Hz^2)');
title('Injections -- estimation of \sigma');

subplot(2,1,2);
h = shadeplot(cinput, combined_sigmas ./ measured_sigmas_ci(2,:), ...
                      combined_sigmas ./ measured_sigmas_ci(1,:), [0.5 0.5 1]); hold on;
set(h,'LineStyle','none');                  
semilogx(cinput, combined_sigmas ./ measured_sigmas, '.-');
xlim([min(cinput) max(cinput)]);
set(gca,'XScale','log');
hold off;
ylabel('\sigma_T / \sigma_E');
xlabel('injected signal strength (strain/Hz^2)');
line(xlim(),[1 1],'LineStyle','--','Color','k');
legend('ratio -- confidence interval', 'ratio', 'unity', 'Location','NorthWest');
%%
print(gcf, '-dpng',   'injections-sigmas.png');
print(gcf, '-depsc2', 'injections-sigmas.eps');


