
%%
parameters = physparms();
parameters.real_data = 0;
strainsq_per_countsq = 1/(parameters.H1.C * parameters.H2.C);

n_amplitudes = 50;
n_frames = 1;

input  = logspace(-47, -43, n_amplitudes) / strainsq_per_countsq;
cinput = input * strainsq_per_countsq;

stats  = [];
sigmas = [];

for j=1:n_amplitudes,
    parameters.injection_fd = input(j);
    trial_stats = [];
    trial_sigmas = [];
    for f=1:n_frames
        fprintf('Performing injection %d of %d.\n', (j-1)*n_frames+f, n_frames*n_amplitudes);
        results = leafnode(parameters,'DUMMY');
        trial_stats  = [trial_stats ; results.analysis_opt.stat];        
        trial_sigmas = [trial_sigmas ; results.analysis_opt.sigma];
    end
    stats  = [stats  trial_stats];
    sigmas = [sigmas trial_sigmas];      
    mkfigures_inj(cinput(1:j), stats, sigmas);
end

