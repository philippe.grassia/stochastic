function [varargout] = rootnode(outfile, infile)
% Rootnode takes in the final recombined output of a
% tree of processes involving many instances of 'intnode' and
% originating with many 'leafnode' processes and writes the final
% reduced data, suitable for processing by the analysis program. 
%
% Two arguments are required, giving the names of the output file and the input
% file, respectively.  The output filename may be set to the empty string
% if no output file is desired, and a data structure containing the
% appropriate variables may be substituted for the name of the input file.
% If an output argument is given, results will be returned in a data
% structure in addition to being written to the output file.
%
% Tobin Fricke <tobin@pas.rochester.edu>, July 2005
% Department of Physics and Astronomy
% University of Rochester
%
% $Id: rootnode.m,v 1.1 2006-10-18 16:40:25 tobin Exp $

myversion = '$Id: rootnode.m,v 1.1 2006-10-18 16:40:25 tobin Exp $';

% Interpret the command line arguments

if isstruct(infile),
    % Infile argument is a structure --> it contains the input variables
    vars = infile;
else
    % Infile argument is a string --> it is the filename of a mat file
    vars = load(infile);
end

% Read the parameters file

parameters = physparms();  %FIXME%
parameters = vars.parameters;

% The following lines are necessary to satisfy the matlab compiler that these variables exist.

cross = 0;

% Load the input file into the workspace

version = vars.version;
version.rootnode = myversion;

seg_duration = parameters.frame_duration / parameters.segs_per_frame;
seg_samples  = seg_duration * parameters.samprate;
bw = 1/seg_duration;

samprate = parameters.samprate;

f = linspace(-samprate/2, samprate/2, seg_samples);
f = f + parameters.het;

frame_duration = parameters.frame_duration;
frame_samples = frame_duration * samprate;
n_segments = length(vars.frameList) * parameters.segs_per_frame;

if parameters.keep_hires,
    hires_bw = 1/frame_duration;
    hires_f = linspace(-samprate/2, samprate/2, frame_samples);
    hires_f = hires_f + parameters.het;
end

% Divide by N to get averages (instead of sums)

if parameters.keep_hires,
    hires_psd1 = vars.hires_psd1 / length(vars.frameList);
    hires_psd2 = vars.hires_psd2 / length(vars.frameList);
    hires_psd3 = vars.hires_psd3 / length(vars.frameList);
end

psd1           = vars.psd1           / n_segments;
psd2           = vars.psd2           / n_segments;
psd3           = vars.psd3           / n_segments;
cross          = vars.cross          / n_segments;
psd1_sq        = vars.psd1_sq        / n_segments;
psd2_sq        = vars.psd2_sq        / n_segments;
psd3_sq        = vars.psd3_sq        / n_segments;
cross_sq       = vars.cross_sq       / n_segments;
cross_sq_parts = vars.cross_sq_parts / n_segments;

%* \item Multiplies $cc$ by the \texttt{cross\_normalization} parameter.

cross    = cross    * parameters.cross_normalization;
cross_sq = cross_sq * parameters.cross_normalization^2;
cross_sq_parts = cross_sq_parts * parameters.cross_normalization^2;

% Compute the variances

% The "sigma_unbiasing_factor" debiases the estimate of the variance. 
% Without it, we underestimate the variance when processing a small number
% of segments.  This amounts to dividing by (N-1) instead of N when
% computing sigma.

sigma_unbiasing_factor = 1 / (1 - 1/n_segments);

%* \item Computes a standard deviation $\sigma = \sqrt{(\langle x^2 \rangle - \langle x \rangle^2)\cdot\frac{n}{n-1}}$ for each quantity

psd1_sigma  = sqrt( (psd1_sq  - psd1.^2) * sigma_unbiasing_factor);
psd2_sigma  = sqrt( (psd2_sq  - psd2.^2) * sigma_unbiasing_factor);
psd3_sigma  = sqrt( (psd3_sq  - psd3.^2) * sigma_unbiasing_factor);
cross_sigma = sqrt( (cross_sq - abs(cross).^2) * sigma_unbiasing_factor);

cross_parts_sigma = (sqrt(      (real(cross_sq_parts) - real(cross).^2) * sigma_unbiasing_factor) + ...
                    sqrt( -1 * (imag(cross_sq_parts) - imag(cross).^2) * sigma_unbiasing_factor));

%* \item Calculates the H1-H2 coherence, $coh = |cc|^2 / (psd_1 \cdot psd_2)$.

coh = abs(cross).^2 ./ (psd1 .* psd2);

%% Output
% Rootnode's output is typically read by Analysis.

% Prepare the output data structure

results.version            = version;
results.frameList          = vars.frameList;
results.seg_samples        = seg_samples;
results.psd1               = psd1;
results.psd2               = psd2;
results.psd3               = psd3;
results.psd1_sigma         = psd1_sigma;
results.psd2_sigma         = psd2_sigma;
results.psd3_sigma         = psd3_sigma;
results.cross              = cross;
results.cross_sigma        = cross_sigma;
results.coh                = coh;
results.cross_parts_sigma  = cross_parts_sigma;
results.parameters         = parameters;

if parameters.keep_hires,
    results.hires_psd1 = highres_psd1;
    results.hires_psd2 = highres_psd2;
    results.hires_psd3 = highres_psd3;
end

% Write the output file if one has been requested.
if outfile,
    save(outfile, '-struct', 'results')
end

% Return the results to the calling function if requested.
if nargout == 1,
    varargout{1} = results;
end

return;

