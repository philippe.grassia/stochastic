function y = reverse(x)
n = length(x);
y = x(n:-1:1);