function out = ind(f,f0)
% index(array,value) 
% input:   1) array of values
%          2) value of interest
% output:     index of the input array giving the location of the value of
%             interest.
% note: index returned is simply the first instance if multiple instances
%       if value is not in the array then 0 is returned

result = find(abs((f-f0)) == min(abs(f-f0)));
out = result(1);