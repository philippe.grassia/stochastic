function [bincenter, binval, binerr] = bin(x,y,n_bins)
% Given a timeseries of values y sampled at times x, this function
% resamples the time series to give a total number of points "bins" by
% dividing the original timeseries up into "n_bins" chunks and computing
% the mean and sigma/sqrt(n) for each bin.   TF @ LHO 2006-03-21
binwidth = (max(x)-min(x))/n_bins;
xbinned = floor((x-min(x))/binwidth)+1;

for bin=1:n_bins,
    bincenter(bin) = (bin-1/2)*binwidth + min(x);    
    binindexes = find(xbinned == bin);
    binval(bin) = mean(y(binindexes));
    binerr(bin) = std(y(binindexes))/sqrt(length(binindexes));
end


