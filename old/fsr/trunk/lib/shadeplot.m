function h = shadeplot(x, upper_limit, lower_limit, colorspec);
h = fill([x reverse(x)], [upper_limit reverse(lower_limit)], colorspec);
    