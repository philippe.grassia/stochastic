
%%
parameters = physparms();
parameters.real_data = 0;
strainsq_per_countsq = 1/(parameters.H1.C * parameters.H2.C);

n_injections = 20;
input  = logspace(-44, -40, n_injections) / strainsq_per_countsq;
rstats = [];
istats = [];
sigmas = [];

for A=input,
    parameters.injection_fd = A;
    results = leafnode(parameters,'DUMMY');
    stats = [rstats results.analysis_opt.stat];
    sigmas = [sigmas results.analysis_opt.sigma];
end

%%

rstats = real(stats);
istats = imag(stats);

cinput = input * strainsq_per_countsq;
sinput = sqrt(cinput);

loglog( sinput, sqrt(rstats), 'x-', ...
        sinput, sqrt(rstats + sigmas), 'x-', ...
        sinput, sqrt(rstats - sigmas), 'x-', ...
        sinput, sinput, 'k--');    
grid on;    
legend('rstat','rstat+sigma','rstat-sigma','perfect recovery','Location','NorthWest');
title('Injection recovery with Unity Filter');
xlabel('Injected strain');
ylabel('Recovered strain');

