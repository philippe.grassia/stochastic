function y = HL(fc, f, L)

c = 299792458;          % speed of light in m/s
T = L/c;                % light transit time
a = - 4 * pi * T;
y = (1 - exp(a * fc)) ./ (1 - exp(a * (fc + i*f)));
