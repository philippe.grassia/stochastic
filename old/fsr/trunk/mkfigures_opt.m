function mkfigures_opt(out_path, infile)
% Generate figures for the Optimal Filter analysis
%
% Tobin Fricke <tobin@pas.rochester.edu>, August 2006
% Department of Physics and Astronomy
% University of Rochester
%
% $Id: mkfigures_opt.m,v 1.4 2007-04-26 00:34:02 tobin Exp $

%% Read the data

data = dlmread(infile);

% The data is written into the analysis_opt.csv file in the following
% format:
%
%fprintf('ANALYSIS_OPT\t%d,%g,%g,%g,%g,%g,%g,%g,%g\n', ...
%	result.n_frames, result.rstat, result.istat, ...
%	result.sigma_real, result.gpstime, result.alpha1, result.alpha2, ...
%	result.ps1, result.ps2);

%% Apply data cuts

%sigma_threshold = 3e-45;
sigma_threshold = Inf;

sigma = data(:,4);
cuts = find(sigma > sigma_threshold);
data(cuts,:) = [];

fprintf('%d samples cut\n', length(cuts));

%% Sort the data by gpstime

data = sortrows(data, 5);

%% Pull out the fields of interest
rstat   = data(:,2);
istat   = data(:,3);
sigma   = data(:,4);
gpstime = data(:,5);
alpha1  = data(:,6);
alpha2  = data(:,7);
ps1     = data(:,8);
ps2     = data(:,9);
days = (gpstime - min(gpstime)) / (60 * 60 * 24);

%% Compute some statistics

fprintf('Computing unweighted averages:\n');
rstat_mean = mean(rstat)
rstat_sigma= std(rstat)
istat_mean = mean(istat)
istat_sigma= std(istat)

% Weighted averages

fprintf('Computing weighted averages:\n');
weighted_mean = @(x, w) (x' * w)/sum(w);
rstat_wmean = weighted_mean(rstat, sigma.^(-2))
istat_wmean = weighted_mean(istat, sigma.^(-2))

sigma_error = sum(sigma.^(-2))^(-1/2)

%save([out_path 'analysis_opt']);
clear data;
pack;

%% Perform the kstest on rstat and istat

fprintf('Performing the Lilliefors hypothesis test on real(stat):\n');
h = lillietest(rstat)
fprintf('Performing the Lilliefors hypothesis test on imag(stat):\n');
h = lillietest(istat)

fprintf('Performing the Kolmogorov-Smirnov test on rstat:\n');
[h, p] = kstest((rstat - mean(rstat)) / std(rstat))
fprintf('Performing the Kolmogorov-Smirnov test on istat:\n');
[h, p] = kstest((istat - mean(istat)) / std(istat))

%% Set up some graphics properties

set(0, ...
'defaultAxesFontSize',   14, ...
'defaultAxesLineWidth',   0.7, ...
'defaultLineLineWidth',   0.8, ...
'defaultPatchLineWidth',  0.7, ...
'defaultAxesXGrid',       'on', ...
'defaultAxesYGrid',       'on', ...
'defaultTextFontName',    'Times', ...
'defaultAxesFontName',    'Times');
%% Make cumulative plot

rstat_wmeans = zeros(size(rstat));
rstat_sigmas = zeros(size(rstat));
for n=1:length(rstat),
    rstat_wmeans(n) = weighted_mean(rstat(1:n), sigma(1:n).^-2);
 
    rstat_sigmas(n) = sum(sigma(1:n).^(-2))^(-1/2);
end
%%  Cumulative estimate of rstat

close all;

cumulative_rstat_wmeans = cumsum(rstat .* sigma.^-2) ./ cumsum(sigma.^-2);
cumulative_istat_wmeans = cumsum(istat .* sigma.^-2) ./ cumsum(sigma.^-2);
cumulative_sigmas       = cumsum(sigma.^-2).^(-1/2);

cumulative_upper_limit = cumulative_rstat_wmeans + 1.645*cumulative_sigmas;
cumulative_lower_limit = cumulative_rstat_wmeans - 1.645*cumulative_sigmas;

n = length(days);

fill([days ; days(n:-1:1)],[cumulative_upper_limit ; cumulative_lower_limit(n:-1:1)], [0.5 0.5 0.5], 'EdgeColor', 'none');
hold all;
plot(days, rstat_wmeans, 'k.');
line(get(gca,'XLim'),[0 0],'Color','k','LineStyle','--');

LegendHandle = legend('90% confidence interval', 'point estimate', ...
                      'zero', 'Location', 'SouthEast');
set(LegendHandle','box','off');

title('evolution of rstat estimate');
xlabel('days into run');
ylabel('\langlerstat\rangle \pm 1.645 \sigma');

% Exclude the first 1.5 days so that the cumulative estimate can settle
% down a little
%set(gca,'XLim',[1.5 max(days)]);

print(gcf, '-depsc2', [out_path 'rstat-cumulative']);
print(gcf, '-dpng',   [out_path 'rstat-cumulative']);

%%  Cumulative estimate of istat
range = 1:n;

h = figure();
plot(days, cumulative_istat_wmeans(range), '.', ...
     days, cumulative_istat_wmeans(range) - 1.645 * cumulative_sigmas(range), '.', ...
     days, cumulative_istat_wmeans(range) + 1.645 * cumulative_sigmas(range), '.', ...
     days, zeros(1,length(range)), '--');
                    
legend('\langleistat\rangle', ...
       '\langleistat\rangle - 1.645 \sigma', ...
       '\langleistat\rangle + 1.645 \sigma', 'zero'); 
  
% errorbar(range, rstat_wmeans(range), rstat_sigmas(range));
title('evolution of istat estimate');
xlabel('days into run');
ylabel('\langleistat\rangle \pm \sigma');
set(gca,'YLim',[-1 1] * 1e-45);  % Force the Y axis

print(h, '-depsc2', [out_path 'istat-cumulative']);
print(h, '-dpng',   [out_path 'istat-cumulative']);

%%
close all;
clear('range','rstat_wmeans','rstat_sigmas');
%% Examine rstat

h = figure();
histfit(rstat, 50);
title('rstat');
legend('rstat',sprintf('\\mu = %0.2g, \n\\sigma = %0.2g',mean(rstat),std(rstat)),'Location','NorthEast')

print(h, '-depsc2', [out_path 'rstat-hist']);
print(h, '-dpng',  [out_path 'rstat-hist']);

%% Examine rstat (attempt at logarithmic histogram)

h = figure();
close all;
[n, x] = hist(rstat,50);
semilogy(x, n, '.');
title('rstat');

%% Examine istat

close all;
h = figure();
histfit(istat, 50);
legend('istat',sprintf('\\mu = %0.2g, \n\\sigma = %0.2g',mean(istat),std(istat)),'Location','NorthEast')
title('istat');
print(h, '-depsc2', [out_path 'istat-hist']);
print(h, '-dpng', [out_path 'istat-hist']);

%% Plot rstat versus istat 

close all;
h = figure();
plot(rstat, istat,'.','MarkerSize',1);
hold all;
axis equal;

plot_circle([rstat_wmean istat_wmean], 1.645 * sigma_error, 'r-');
plot_circle([rstat_mean  istat_mean], [std(rstat) std(istat)], 'k--');

title('distribution of estimates Y');
xlabel('real part');
ylabel('imaginary part');

L = legend(sprintf('measurements (%d)', length(rstat)),'combined estimate','variance of measurements');
%set(L, 'Color', 'None', 'Box', 'On');

% Superimpose some histograms

% Figure out where the existing plot window is located
MainAxesHandle = gca();
MainAxesPos = get(MainAxesHandle,'Position');

% Create new axes for the histograms
VertAxesHandle = axes('Position', [MainAxesPos(1) MainAxesPos(2) 0.20 MainAxesPos(4)]);                      
HorzAxesHandle = axes('Position', [MainAxesPos(1) MainAxesPos(2) MainAxesPos(3) 0.20]);                    

% Make the new axes invisible
set(VertAxesHandle, 'Color', 'None', 'XTick', [], 'YTick', [], 'Box', 'Off');
set(HorzAxesHandle, 'Color', 'None', 'XTick', [], 'YTick', [], 'Box', 'Off');

% Don't let Matlab mess with our invisibility
hold(VertAxesHandle, 'all');
hold(HorzAxesHandle, 'all');

% Compute the histograms
[bi, xi] = hist(istat, sqrt(length(istat)));
[br, xr] = hist(rstat, sqrt(length(rstat)));

% Plot the histograms
plot(VertAxesHandle, bi/sum(bi), xi, 'k-',     normpdf(xi, mean(istat), std(istat))*(xi(2)-xi(1)), xi, 'k--');
plot(HorzAxesHandle, xr, br/sum(br), 'k-', xr, normpdf(xr, mean(rstat), std(rstat))*(xr(2)-xr(1)),     'k--');

% Make sure that the ranges are coincident
set(HorzAxesHandle, 'XLim', get(MainAxesHandle, 'XLim'));
set(VertAxesHandle, 'YLim', get(MainAxesHandle, 'YLim'));

% Save the figures
print(h, '-depsc2', [out_path 'rstat-vs-istat']);
print(h, '-dpng',   [out_path 'rstat-vs-istat']);

%% Make a histogram of sigma

close all;
h = figure();
hist(sigma, sqrt(length(sigma)));
title('distribution of sigmas');
print(h, '-depsc2', [out_path 'sigma-hist']);
print(h, '-dpng', [out_path 'sigma-hist']);

%% Make a histogram of sigma^
% FIXME This should follow the chi-sq distribution.

close all;
h = figure();
histfit(sigma.^2);
title('distribution of \sigma^2');
print(h, '-depsc2', [out_path 'sigmasq-hist']);
print(h, '-dpng', [out_path 'sigmasq-hist']);

%% Plot the sigma versus time
close all;
h = figure();
plot((gpstime-min(gpstime))/(60*60*24),sigma,'.');
title('sigma versus time');
xlabel('days into run');
ylabel('sigma');
print(h, '-depsc2', [out_path 'sigma-series']);
print(h, '-dpng', [out_path 'sigma-series']);

%% Plot the power versus time

close all;
h = figure();
plot((gpstime-min(gpstime))/(60*60*24),ps1,'r.',(gpstime-min(gpstime))/(60*60*24),ps2,'b.');
xlabel('days into run');
ylabel('band-limited power');
legend('H1','H2');
title('Power versus time');
print(h, '-depsc2', [out_path 'power-series']);
print(h, '-dpng', [out_path 'power-series']);

%% Plot the calibration factors versus time

close all;
plot((gpstime-min(gpstime))/(60*60*24),alpha1,'r.',(gpstime-min(gpstime))/(60*60*24),alpha2,'b.');
xlabel('days into run');
ylabel('alpha');
legend('H1','H2');
title('Calibration factors versus time');
print(h, '-depsc2', [out_path 'alpha-series']);
print(h, '-dpng', [out_path 'alpha-series']);

%% Plot the rstat and istat versus time

close all;
plot((gpstime-min(gpstime))/(60*60*24),rstat,'.',(gpstime-min(gpstime))/(60*60*24),istat,'.');
xlabel('days into run');
legend('rstat','istat');
title('rstat and istat versus time');
print(h, '-depsc2', [out_path 'stat-series']);
print(h, '-dpng', [out_path 'stat-series']);

%% End
close all;
