#!/usr/bin/python
# $Id: divvy.py,v 1.1 2006-10-18 16:40:25 tobin Exp $

# This program produces a Condor DAG file describing a binary
# tree via which a set of frames can be reduced into a single
# mean result.

import sys
import os
import math

jobid = 0

# We give each of our jobs a unique identifier.  This function
# produces those identifiers; they are just integers, generated
# in sequence.

def make_job_id():
    global jobid
    jobid = jobid + 1
    return jobid - 1

# The recursive function 'divvy' takes a list of frames as an
# argument and produces (to standard output) the Condor DAG
# that connects these frames in a binary tree.  Divvy operates
# by successively dividing the list into halves and then calling
# itself recursively, specifying calls to LEAFNODE as its base
# case and calls to INTNODE in the recursive cases.

def divvy(items):
    my_job_id = make_job_id()
    if len(items)==1:
	task = items[0]
        print("JOB    %s leafnode.sub" % my_job_id)
	print("VARS   %s JOB=\"%s\" TASK=\"%s\"" % (my_job_id, my_job_id, task))
    else:
        pivot = int(math.ceil(len(items)/2))
        child1 = divvy(items[0:pivot])
        child2 = divvy(items[pivot:])
        task = "output/%d output/%d" % (child1, child2)
        print("JOB    %s intnode.sub" % my_job_id)
	print("VARS   %s JOB=\"%s\" TASK=\"%s\"" % (my_job_id, my_job_id, task))
        print("PARENT %d %d CHILD %s" % (child1, child2, my_job_id))
    return my_job_id


# Read from stdin a list of frame files to be processed
frame_files = [file.strip() for file in sys.stdin.readlines()]

print("DOT herbert.dot UPDATE")
final_job = divvy(frame_files)
print("JOB rootnode rootnode.sub")
print("PARENT %s CHILD rootnode" % final_job)
