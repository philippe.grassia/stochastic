function [gpstime alpha gamma] = get_calib_coeff(gpstime, IFO);

data = dlmread(['data/alpha-gamma-' IFO '.txt']);
found = find(abs(data(:,1)-gpstime) == min(abs(data(:,1)-gpstime)));
result = data(found(1),:);

gpstime = result(1);
alpha = result(2);
gamma = result(3);
