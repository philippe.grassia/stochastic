#!/bin/sh
wget --quiet -O - http://ligo.phys.lsu.edu/sung/Factors/S4/V4/H-H1_CAL_FAC_S4_V4_060-793152133-2834520.txt | grep --invert-match --extended-regexp ^% > alpha-gamma-H1.txt
wget --quiet -O - http://ligo.phys.lsu.edu/sung/Factors/S4/V4/H-H2_CAL_FAC_S4_V4_060-793132513-2892480.txt | grep --invert-match --extended-regexp ^% > alpha-gamma-H2.txt
wget --quiet -O - http://ligo.phys.lsu.edu/sung/Factors/S4/V4/L-L1_CAL_FAC_S4_V4_060-793128493-2800980.txt | grep --invert-match --extended-regexp ^% > alpha-gamma-L1.txt

