function [varargout] = leafnode(parameters, varargin)
% This function performs the lowest level of analysis in this program,
% reading and digesting the individual frame files.  The results of this
% program are read by "intnode" which combines partial results.  This is
% intended to be run as a compiled matlab program, in a "DAG" job under
% the Grid Computing program CONDOR.  The program "divvy.py" in this
% directory generates the job description suitable for CONDOR.
%
% Leafnode can write its output to a MAT file or return a data structure,
% depending on how it is called:
%
% leafnode(outfile, framefile1, framefile2, ..., framefileN)
% results = leafnode('', framefile1, framefile2, ..., framefileN)
%
% Tobin Fricke <tobin@pas.rochester.edu>, July 2005
% Department of Physics and Astronomy
% University of Rochester
%
% $Id: leafnode.m,v 1.2 2006-12-06 17:18:14 gstef Exp $

% We record in the output the version number of this program used to compute it.
version.leafnode = '$Id: leafnode.m,v 1.2 2006-12-06 17:18:14 gstef Exp $';

if isstruct(parameters),
    if isfield(parameters, 'outfile'),
        outfile = parameters.outfile;
    else
        outfile = '';
    end
else
    % Load the parameters from the parameters file.
    outfile = parameters;
    parameters = physparms();
end

% If the user doesn't call this program with an appropriate number of
% arguments, we print out a (hopefully) helpful message, and exit.
if (nargin < 1),
        error('Usage: leafnode(outfile, framefile1, framefile2, ..., framefileN)');
end

if (nargout > 1),
        error('Too many output arguments.');
end

% Any argument after the several required arguments is assumed to be the name
% of a frame file that we are intended to process.

frameListInput = varargin;
frameList = {};

%% Initialization

% addpath([getenv('LIGOTOOLS') filesep 'matlab']);

% If we are doing software injections, then seed the random number generator from the system clock.

if (~isnan(parameters.injection_fd)) | (~isnan(parameters.injection_td)),
	randn('state',sum(100*clock));    
end

if parameters.keep_hires, 
  hires_psd1   = 0;
  hires_psd2   = 0;
  hires_psd3   = 0;
end
  
psd1           = 0;
psd2           = 0;
psd3           = 0;
cross          = 0;
psd1_sq        = 0;
psd2_sq        = 0;
psd3_sq        = 0;
cross_sq       = 0;
cross_sq_parts = 0;

% Compute some useful quantities

frame_samples = parameters.frame_duration * parameters.samprate;
seg_duration  = parameters.frame_duration / parameters.segs_per_frame;
seg_samples   = seg_duration * parameters.samprate;

% Compute the Hanning window

window = hanning(seg_samples);
repwin = repmat(window, 1, parameters.segs_per_frame);
winfactor = 1/mean(window.^2);
hires_window = hanning(frame_samples);

% Process each frame

for frame=frameListInput,
    
    if ~isstr(frame),
        frame=cell2mat(frame);
    end
    
    % If the frame file is in our list of known bad frames, we exclude it.
    if string_is_in_list(frame, parameters.badFrames),
        continue;
    end    

    frameList = horzcat(frameList, {frame});
    
    % We can either use real data, or we can generate some fake data.

    if parameters.real_data,
      
      % For S4 and earlier runs, data were recorded separately for each of the photodiodes and
      % each quadrature; we have to combine them ourselves using the "extract_and_combine_channel"
      % function.  For S5 (and later runs, presumably), the signals from the photodiodes and
      % quadratures are pre-combined and the resulting signal is recorded.
      
      if parameters.run == 'S4',
        h1 = extract_and_combine_channel(frame, parameters.H1, parameters.n_fsr);
        h2 = extract_and_combine_channel(frame, parameters.H2, parameters.n_fsr);
        l1 = extract_and_combine_channel(frame, parameters.L1, parameters.n_fsr);      
      
      elseif parameters.run == 'S5',              
        h1 = frextract(frame, [parameters.H1.name ':LSC-AS_Q_1FSR']);
        h2 = frextract(frame, [parameters.H2.name ':LSC-AS_Q_1FSR']);
        l1 = frextract(frame, [parameters.L1.name ':LSC-AS_Q_1FSR']);    
        
      else
        error('parameters.run is not a recognized run.');
        
      end        
    else
    
      % We may also wish to generate completely random data for testing purposes.  This will be
      % useful in conjunction with software injections (done futher below in this program.)
      % However, this feature is not yet implemented.
    
      if ~strcmp(frame,'DUMMY')
          warning('Random data being generated, but filename isn''t "DUMMY"');
      end
      
      h1_variance = 11.85;      h1_mean =  0.00670 + 0.00036i;
      h2_variance =  7.54;      h2_mean =  -0.0041 + 0.00110i;
      
      h1 = (randn(1, frame_samples) + i*randn(1, frame_samples)) * h1_variance + h1_mean;
      h2 = (randn(1, frame_samples) + i*randn(1, frame_samples)) * h2_variance + h2_mean;
      l1 =  zeros(1, frame_samples);
    end

    % h2 = circshift(h1, parameters.shift_samples);
    
    % If we were to do time-domain signal injection, we would do it here, before we compute
    % the fourier transforms.
    
    if ~isnan(parameters.injection_td),
    	warning('Time-domain signal injection is not yet validated.');
        % Do time-domain signal injection here
        highres_f = linspace(-samprate/2, samprate/2, frame_samples);
        
        resp1 = HL(parameters.H1.cavpole, highres_f, parameters.H1.armlength);
        resp2 = HL(parameters.H2.cavpole, highres_f, parameters.H2.armlength);
        
        rs = (randn(size(h1)) +i*randn(size(h1)))*parameters.injection_td;
        rs = fftshift(fft(rs)); % Not necessary
        
        inj1 = ifft(ifftshift(rs .* resp1));
        inj2 = ifft(ifftshift(rs .* resp2));
        
        h1 = h1 + inj1;
        h2 = h2 + inj2;
    end
    
    if ~isnan(parameters.timeshift_td),
        if ~isnan(parameters.injection_fd),
            error('Can''t inject in frequency domain while timeshifting in time domain.');
        end;
        t = linspace(0, parameters.frame_duration, frame_samples);
        h2 = interp1(t, h2, t + parameters.timeshift_td, 'cubic', 'extrap');
        clear t;
    end
        
    % Compute high-resolution power spectal densities.  These are the spectra before we fold the data
    % into segments of the desired length.  Not all analyses require these high-resolution spectra,
    % so we don't always compute and record them.
   
    if parameters.keep_hires, 
      hires_psd1 = hires_psd1 + abs(fft(h1 .* hires_window) * sqrt(2 * parameters.frame_duration) / frame_samples).^2;
      hires_psd2 = hires_psd2 + abs(fft(h2 .* hires_window) * sqrt(2 * parameters.frame_duration) / frame_samples).^2;
      hires_psd3 = hires_psd3 + abs(fft(l1 .* hires_window) * sqrt(2 * parameters.frame_duration) / frame_samples).^2;
    end

    % Reshape into segments of the desired length.  This folds the time series into a 2D array
    
    h1 = reshape(h1, [seg_samples parameters.segs_per_frame]);
    h2 = reshape(h2, [seg_samples parameters.segs_per_frame]);
    l1 = reshape(l1, [seg_samples parameters.segs_per_frame]);
    
    h1 = fft(h1 .* repwin) * sqrt(winfactor) * sqrt(2 * seg_duration)/seg_samples;        
    h2 = fft(h2 .* repwin) * sqrt(winfactor) * sqrt(2 * seg_duration)/seg_samples;
    l1 = fft(l1 .* repwin) * sqrt(winfactor) * sqrt(2 * seg_duration)/seg_samples;

    % Injection in frequency domain
    
    if ~isnan(parameters.injection_fd),

        f = linspace(-parameters.samprate/2, parameters.samprate/2, seg_samples);
        f = f + parameters.het;

        resp1 = HL(parameters.H1.cavpole, f, parameters.H1.armlength);
        resp2 = HL(parameters.H2.cavpole, f, parameters.H2.armlength);

        A = sqrt(parameters.injection_fd / 2);        
        rs = A * (    randn(seg_samples,parameters.segs_per_frame) ...
                   +i*randn(seg_samples,parameters.segs_per_frame));
%       mean(abs(rs).^2) is approximately equal to injection_fd            
        
        h1 = h1 + rs.*repmat(ifftshift(resp1).',1, parameters.segs_per_frame);
        h2 = h2 + rs.*repmat(ifftshift(resp2).',1, parameters.segs_per_frame);    
    end    
    
    % Time shift in frequency domain
    
    if ~isnan(parameters.timeshift_fd),
        phasor = exp(i * 2 * pi * (f - parameters.het) * timeshift_fd);
        h2 = h2 .* phasor;
    end
    
    % Done with data acquisition/preparation.  Begin analysis!
    
    psd1    = psd1      + sum(abs(h1).^2, 2);
    psd2    = psd2      + sum(abs(h2).^2, 2);
    psd3    = psd3      + sum(abs(l1).^2, 2);

 %   psd1_sq = psd1_sq   + sum(abs(h1).^4, 2);
 %   psd2_sq = psd2_sq   + sum(abs(h2).^4, 2); 
 %   psd3_sq = psd3_sq   + sum(abs(l1).^4, 2);

    this_cross = conj(h1) .* h2;
    
    cross     = cross     + sum( this_cross    , 2);
    cross_sq  = cross_sq  + sum((abs(this_cross)).^2, 2);
    
    cross_sq_parts = cross_sq_parts + ...
                     sum((real(this_cross)).^2, 2) + ...
                     (sum((imag(this_cross)).^2, 2) * sqrt(-1));

end

if parameters.keep_hires, 
    hires_psd1 = fftshift(hires_psd1);
    hires_psd2 = fftshift(hires_psd2);
    hires_psd3 = fftshift(hires_psd3);
end

psd1       = fftshift(psd1);
psd2       = fftshift(psd2);
psd3       = fftshift(psd3);
cross      = fftshift(cross);
psd1_sq    = fftshift(psd1_sq);
psd2_sq    = fftshift(psd2_sq);
psd3_sq    = fftshift(psd3_sq);
cross_sq   = fftshift(cross_sq);
cross_sq_parts = fftshift(cross_sq_parts);

n_frames = length(frameList);
n_segments = n_frames * parameters.segs_per_frame;

%% Output
% Now we save all the interesting variables, for later digestion by
% the 'intnode' program.

% Prepare the output data structure
results.version   = version;
results.frameList = frameList;
results.psd1      = psd1;
results.psd2      = psd2;
results.psd3      = psd3;
results.psd1_sq   = psd1_sq;
results.psd2_sq   = psd2_sq;
results.psd3_sq   = psd3_sq;
results.cross     = cross;
results.cross_sq  = cross_sq;
results.cross_sq_parts  = cross_sq_parts;

if parameters.keep_hires,
    results.hires_psd1 = highres_psd1;
    results.hires_psd2 = highres_psd2;
    results.hires_psd3 = highres_psd3;
end

% As a sort of kludge, I am grafting in the optimal filter
% analysis right here:
results.parameters = parameters;
results.analysis_opt = analysis_opt(rootnode('',results));

% Write the output file if one has been requested.
if length(outfile) ~= 0,
  save(outfile, '-struct', 'results');
end

% Return the results to the calling function if requested.
if nargout == 1,
    varargout{1} = results;
end

return;
