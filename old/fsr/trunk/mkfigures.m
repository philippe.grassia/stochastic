function mkfigures(output_path, infile)
% $Id: mkfigures.m,v 1.7 2007-03-15 03:18:17 tobin Exp $

%% Initialization
if (nargin ~= 2),
  error('Usage: mkfigures [output path] [input file]');
end

cross = 0;
load(infile);

warning('off', 'MATLAB:print:FileName') 

%% Make up some standard variables
seg_duration = parameters.frame_duration / parameters.segs_per_frame;
seg_samples  = seg_duration * parameters.samprate;
n_segments = length(frameList) * parameters.segs_per_frame;
bw = 1/seg_duration;
f = ((-seg_samples/2):(seg_samples/2-1)) * bw;
f = f + parameters.het;

edge_effect = 125; % Hz to cut off on right and left
x = find(abs(f - parameters.fsr) < parameters.roi_halfwidth);
z = find((f - min(f)) > edge_effect & (max(f) - f) > edge_effect);

roi = [(parameters.fsr - parameters.roi_halfwidth) ...
       (parameters.fsr + parameters.roi_halfwidth)];

%% Apply Calibrations

psd1 = psd1   / (parameters.H1.C ^ 2);
psd2 = psd2   / (parameters.H2.C ^ 2);
%psd3 = psd3   / (parameters.L1.C ^ 2);
cross = cross / (parameters.H1.C * parameters.H2.C);

%% Response functions

resp1   = HL(parameters.H1.cavpole, f, parameters.H1.armlength);
resp2   = HL(parameters.H2.cavpole, f, parameters.H2.armlength);
resp_cc = conj(resp1) .* resp2;

% In this program, we only use the magnitude response.

resp1   = abs(resp1);
resp2   = abs(resp2);
resp_cc = abs(resp_cc);

%% Set up graphics defaults

target = 'presentation';

if strcmp(target, 'paper'),
    set(0, ...
        'defaultAxesFontSize',   14, ...
        'defaultAxesLineWidth',   0.7, ...
        'defaultLineLineWidth',   0.8, ...
        'defaultPatchLineWidth',  0.7, ...
        'defaultAxesXGrid',       'on', ...
        'defaultAxesYGrid',       'on', ...
        'defaultTextFontName',    'Times', ...
        'defaultAxesFontName',    'Times');
elseif strcmp('target', 'presentation'),
    set(0, ...
        'defaultAxesFontSize',   25, ...
        'defaultAxesLineWidth',   5.0, ...
        'defaultLineLineWidth',   5.0, ...
        'defaultPatchLineWidth',  0.7, ...
        'defaultAxesXGrid',       'on', ...
        'defaultAxesYGrid',       'on', ...
        'defaultTextFontName',    'SansSerif', ...
        'defaultAxesFontName',    'SansSerif');
end




%% Plot response functions
h = figure();
semilogy(f/1000, parameters.H1.C * resp1, 'r-', ...
         f/1000, parameters.H2.C * resp2, 'b-', ...
         f/1000, sqrt(parameters.H1.C * parameters.H2.C * resp_cc), 'k-');
legend('H1 response','H2 response', 'sqrt(product)');
title('Response functions');
xlabel('Frequency (kHz)');
ylabel('Gain');
set(gca, 'XLim', [min(f/1000) max(f/1000)]);
%%
print(h, '-depsc2', [output_path 'resp']);
print(h, '-dpng', [output_path 'resp']);

%%
set(gca, 'XLim', roi/1000);
title('Response functions (detail)');
print(h, '-depsc2', [output_path 'resp-zoom']);
print(h, '-dpng', [output_path 'resp-zoom']);
close all;

%% Make a plot of the response function for H1 from DC to fsr

c = 299792458;

f_fsr  = c / (2 * parameters.H1.armlength);
f_fsr2 = c / (2 * parameters.H2.armlength);

f_min = 10;
f_max = 100e3;

f_bb = logspace(log(f_min)/log(10), log(f_max)/log(10), 1000);
%f_bb = linspace(f_min, f_max, 10000);

% Make sure that the exact fsrs are represented
f_bb = [f_bb f_fsr f_fsr2 f_fsr*2];
f_bb = sort(f_bb);

resp_bb   = HL(parameters.H1.cavpole, f_bb, parameters.H1.armlength);
 
%subplot(2,1,1);
loglog(f_bb, abs(resp_bb));
ylim([min(abs(resp_bb)) 1]);
ylabel('magnitude');
title('4km interferometer response');
%subplot(2,1,2);
%semilogx(f_bb, angle(resp_bb));
%ylabel('phase (radians)');

print(gcf, '-depsc2', [output_path 'resp_bb.eps']);
print(gcf, '-dpng',   [output_path 'resp_bb.png']);
%%
resp_bb2 = HL(parameters.H2.cavpole, f_bb, parameters.H2.armlength);
subplot(2,1,1);
loglog(f_bb, abs(resp_bb), 'r-', ...
       f_bb, abs(resp_bb2), 'b-');
legend('H1','H2', 'Location', 'SouthWest');   

subplot(2,1,2);
loglog(f_bb, abs(resp_bb) .* abs(resp_bb2), 'm-');
legend('H1 H2 product', 'Location', 'SouthWest');   


%% Coherence

close all;
h = figure();

mu = mean(coh(z));                % Actual mean
mu_exp = 1/n_segments;            % Expected mean
[mu_hat, mu_ci]=expfit(coh(z));   % Best-fit mean

plot(f(z)/1000, coh(z),                          '.', 'MarkerSize', 1); hold all;
xlim([min(f(z)/1000) max(f(z)/1000)]);
%plot(f(z)/1000, ones(size(coh(z)))*mu, 'y--', 'LineWidth', 5);
title(sprintf('H1-H2 coherence at FSR (%d segments in average)', n_segments));
%legend('measured coherence','mean'); %sprintf('mean = %e \n(expected = %e)', mu, mu_exp));
xlabel('frequency (kHz)');
ylabel('coherence');

% Make an inset with the distribution
MainAxesHandle = gca();
MainAxesPos = get(MainAxesHandle, 'Position');
SubAxesHandle = axes('Position', MainAxesPos + [0.03, 0.35, -2*0.03,-0.35 - 0.03]);
[n,Gamma]=hist(coh(z),sqrt(length(z)));
semilogy(Gamma, n/sum(n), 'm.');
lims = get(SubAxesHandle,'YLim');
hold all;
dGamma = Gamma(2) - Gamma(1);

semilogy(Gamma, exppdf(Gamma, 1/n_segments)*dGamma, '-');
semilogy(Gamma, exppdf(Gamma, mu_hat)*dGamma, '--');
set(SubAxesHandle, 'YLim', lims);
set(SubAxesHandle, 'Color', 'None');
%set(SubAxesHandle,'XTick',[],'YTick',[],'Color','None');
%title('Distribution of Coherence');

% Let's use the Kolmogorov-Smirnov test to verify that the Coherence
% follows an exponential distribution

GammaSamp = linspace(0, max(coh(z)), sqrt(length(coh(z))));
[h, p] = kstest(coh(z), [GammaSamp ; expcdf(GammaSamp, 1/n_segments)]')

legend(sprintf('Distribution of Coherence\n \\mu = %3.3e', mu), ...
       sprintf('Expected exponential distribution\n\\mu = %3.3e\nKolmogorov-Smirnov p = %3.2f',  mu_exp, p), ...
       sprintf('Best-fit exponential distribution\n\\mu \\epsilon [%3.3e, %3.3e]', mu_ci(1), mu_ci(2)));

print(gcf, '-depsc2', [output_path 'coh']);
print(gcf, '-dpng', [output_path 'coh']);
%% Coherence (detail)

set(MainAxesHandle, 'XLim', roi/1000);
title(sprintf('H1-H2 coherence at FSR (detail) (%d segments in average)', n_segments));
print(gcf, '-depsc2', [output_path 'coh-zoom']);
print(gcf, '-dpng', [output_path 'coh-zoom']);

%%
close all;

%% Coherence (distribution)

h = figure;
[n,c]=hist(coh(z),sqrt(length(z)));
semilogy(c,n,'.');
xlabel('coherence');
ylabel('occurrances');
title('distribution of coherence');

%r = find(n ~= 0);
%r = 1:25;
%p = polyfit(c(r), log(n(r)), 1);
%semilogy(c,n,'.',c,exp(polyval(p, c)),'-');
%grid on;
%clear r, c;

print(h, '-depsc2', [output_path 'coh-dist']);
print(h, '-dpng', [output_path 'coh-dist']);


%% Correlation 
close all;
h = figure();
plot(f(z), real(cross(z)), f(z), imag(cross(z)));
title(sprintf('H1-H2 correlation at FSR (%d segments in average)', n_segments));
legend('real part','imaginary part');
xlabel('frequency (hertz)');
ylabel('strain correlation');
print(h, '-depsc2', [output_path 'cross']);
print(h, '-dpng', [output_path 'cross']);

%% Correlation (detail)

set(gca, 'XLim', roi);
title(sprintf('H1-H2 correlation at FSR (%d segments in average) (detail)', n_segments));
print(h, '-depsc2', [output_path 'cross-zoom']);
print(h, '-dpng', [output_path 'cross-zoom']);
close all;

%% H1 PSD
psdlimits = sqrt([min(min(psd1(z)),min(psd2(z))) max(max(psd1(z)),max(psd2(z)))]);
psdlimits_zoom = sqrt([min(min(psd1(x)),min(psd2(x))) max(max(psd1(x)),max(psd2(x)))]);

h = figure();
semilogy(f(z)/1000, sqrt(psd1(z)));
title(sprintf('H1 power spectrum at FSR -- averaged over %d segments',  n_segments));
xlabel('frequency (kHz)');
ylabel('strain');

set(gca, 'YLim', psdlimits);
print(h, '-depsc2', [output_path 'psd1']);
print(h, '-dpng', [output_path 'psd1']);

%% H1 PSD (detail)
set(gca, 'XLim', roi/1000);
title(sprintf('H1 power spectrum at FSR (detail) -- averaged over %d segments',  n_segments));
set(gca, 'YLim', psdlimits_zoom);
print(h, '-depsc2', [output_path 'psd1-zoom']);
print(h, '-dpng', [output_path 'psd1-zoom']);
%%
close all;

%% H2 PSD 
h = figure();
semilogy(f(z)/1000, sqrt(psd2(z)));
title(sprintf('H2 power spectrum at FSR -- averaged over %d segments',  n_segments));
xlabel('frequency (kHz)');
ylabel('strain');
set(gca, 'YLim', psdlimits);
print(h, '-depsc2', [output_path 'psd2']);
print(h, '-dpng', [output_path 'psd2']);

%% H2 PSD (detail)
set(gca, 'XLim', roi/1000);
title(sprintf('H2 power spectrum at FSR (detail) -- averaged over %d segments',  n_segments));
set(gca, 'YLim', psdlimits_zoom);
print(h, '-depsc2', [output_path 'psd2-zoom']);
print(h, '-dpng', [output_path 'psd2-zoom']);
%%
close all;

%% H1 PSD with response
h = figure();
semilogy(f(z)/1000, sqrt(psd1(z)) ./ resp1(z)','r');
xlabel('frequency (kHz)');
set(gca, 'XLim', [min(f(z)/1000) max(f(z)/1000)]);
print(h, '-depsc2', [output_path 'psd1-calib']);
print(h, '-dpng', [output_path 'psd1-calib']);

%% H1 PSD with response (detail)
set(gca, 'XLim', roi/1000);
print(h, '-depsc2', [output_path 'psd1-withresp-zoom']);
print(h, '-dpng', [output_path 'psd1-withresp-zoom']);
%%
close all;

%% H2 PSD (Calibrated)
h = figure();
semilogy(f(z)/1000, sqrt(psd2(z)) ./ resp2(z)','b');
set(gca, 'XLim', [min(f(z)/1000) max(f(z)/1000)]);
xlabel('frequency (kHz)');

if parameters.n_fsr == 1,           % KLUDGE
    ylim([1e-20, 1000 * 1e-20]);    % KLUDGE
end                                 % KLUDGE

print(h, '-depsc2', [output_path 'psd2-calib']);
print(h, '-dpng', [output_path 'psd2-calib']);
%%
ylim('auto');

%% H2 PSD (Calibrated) (detail)
set(gca, 'XLim', roi/1000);
print(h, '-depsc2', [output_path 'psd2-calib-zoom']);
print(h, '-dpng', [output_path 'psd2-calib-zoom']);
%%
close all;

%% Cross-Spectral density
h = figure();
semilogy(f(z)/1000, ...
         (sqrt(psd1(z)) ./ resp1(z)') .* (sqrt(psd2(z)) ./ resp2(z)'), ...
         'm');
set(gca, 'XLim', [min(f(z)/1000) max(f(z)/1000)]);   
xlabel('frequency (kHz)');
print(h, '-depsc2', [output_path 'cpsd-calib']);
print(h, '-dpng', [output_path 'cpsd-calib']);    
%%
set(gca, 'XLim', roi/1000);
print(h, '-depsc2', [output_path 'cpsd-calib-zoom']);
print(h, '-dpng', [output_path 'cpsd-calib-zoom']);
%%
close all;
     

