function [varargout] = intnode(outfile, varargin)
% Intnode takes output files from 'leafnode' and/or other instances of
% itself, combines them, and writes resulting output files which are either
% read and futher combined by another instance of 'intnode', or ultimately
% consumed by 'rootnode'.
%
% The first file listed on the command line is the file into which results
% should be written.  The remaining files are the intermediate results we
% are combining into the output.
%
% Tobin Fricke <tobin@pas.rochester.edu>, July 2005
% Department of Physics and Astronomy
% University of Rochester

% $Id: intnode.m,v 1.1 2006-10-18 16:40:25 tobin Exp $

%% Initialization

version.intnode = '$Id: intnode.m,v 1.1 2006-10-18 16:40:25 tobin Exp $';

% Initialize some variables

parameters = physparms();  %FIXME

if parameters.keep_hires,
    hires_psd1 = 0;
    hires_psd2 = 0;
    hires_psd3 = 0;
end

psd1       = 0;
psd2       = 0;
psd3       = 0;
cross      = 0;
psd1_sq    = 0;
psd2_sq    = 0;
psd3_sq    = 0;
cross_sq   = 0;
cross_sq_parts   = 0;
% n_frames   = 0;
% n_segments = 0;
frameList  = {};

%% Calculation
% Process the incoming files

for infile = varargin,

    if isstruct(infile),
        vars = infile;
    else
        vars = load(cell2mat(infile));
    end;
    
    version.leafnode = vars.version.leafnode;
   
%    n_frames   = n_frames   + vars.n_frames;
%    n_segments = n_segments + vars.n_segments;
    
    frameList = horzcat(frameList, vars.frameList);
    
    if parameters.keep_hires,
        hires_psd1 = hires_psd1 + vars.hires_psd1;
        hires_psd2 = hires_psd2 + vars.hires_psd2;
        hires_psd3 = hires_psd3 + vars.hires_psd3;
    end
    
    psd1  = psd1  + vars.psd1;
    psd2  = psd2  + vars.psd2;
    psd3  = psd3  + vars.psd3;
    cross = cross + vars.cross;    

    psd1_sq  = psd1_sq  + vars.psd1_sq;
    psd2_sq  = psd2_sq  + vars.psd2_sq;
    psd3_sq  = psd3_sq  + vars.psd3_sq;

    cross_sq = cross_sq + vars.cross_sq;
    cross_sq_parts = cross_sq_parts + vars.cross_sq_parts;
end

%% Output
% Write the output file

results.version   = version;
results.frameList = frameList;
results.psd1      = psd1;
results.psd2      = psd2;
results.psd3      = psd3;
results.psd1_sq   = psd1_sq;
results.psd2_sq   = psd2_sq;
results.psd3_sq   = psd3_sq;
results.cross     = cross;
results.cross_sq  = cross_sq;
results.cross_sq_parts  = cross_sq_parts;
results.parameters= parameters;

if parameters.keep_hires,
    results.hires_psd1 = highres_psd1;
    results.hires_psd2 = highres_psd2;
    results.hires_psd3 = highres_psd3;
end

if outfile,
  save(outfile, '-struct', 'results');
end

if nargout == 1,
  varargout{1} = results;
end

if parameters.intnode.delete_input,
    for infile = varargin,
	delete([cell2mat(infile) '.mat']);
    end
end

return;
