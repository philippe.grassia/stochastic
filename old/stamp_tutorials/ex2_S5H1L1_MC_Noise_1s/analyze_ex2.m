clear all;

% load data
load map.mat;

% create 1D arrays of y and sigma
yvals=map.yvals;
sigma=map.sigma(:);
y=map.y(:);

% remove buffer segments and masked data
sigma=sigma(y~=0);
y=y(y~=0);

% define SNR
snr = y./sigma;

% print the standard deviation of SNR to the screen
fprintf('std(snr)=%1.3f\n***\n', std(snr));

% compare Y^2 with sigma^2
fprintf('mean(y.^2)=%1.3e\n', mean(y.^2));
fprintf('mean(sigma.^2)=%1.3e\n', mean(sigma.^2));
fprintf('mean(y.^2)/mean(sigma.^2) = %1.3f\n', mean(y.^2)/mean(sigma.^2));

% make SNR histogram
figure;
[n b] = hist(snr(:), [-7:0.5:7]);
hist(snr(:), [-7:0.5:7]);
axis([-7 7 0 1.15*max(n)]);
pretty;
print('-djpeg', 'stamp_histo.jpg');
print('-depsc2', 'stamp_histo.eps');
