clear all;

params = stampDefaults;

params.intFrameCachePath = './cachefiles/';
jobsFile=['./S5H1L1_full_run.txt'];

% search direction-------------------------------------------------------------
params.ra=6;   %right ascenion in hours (for search direction)
params.dec=30; %declination in degrees (for search direction)

% frequency range--------------------------------------------------------------
params.fmin = 100;
params.fmax = 260;

% override default parameters--------------------------------------------------
params.purePlus=true;
%params.pureCross=true;
params.debug=true;
params.saveMat=true;
params.savePlots=true;
params.doFreqMask=false;
params.Autopower=true;
params.yMapScale = 5e-42;
params.coarsegrain = 1;

% Call stochMap----------------------------------------------------------------
%stochmap(params, jobsFile, 816065699, 816065710);  % job 1
stochmap(params, jobsFile, 816065695, 816065710);  % job 1


% rename file
!mv map.mat map_plus.mat
