clear all;

study = 2;

if study==1      % pure plus ADI injection ------------
  outfile = 'map_plus.mat';
  injfile = 'plus.dat';
elseif study==2  % plus and cross ADI injection -------
  outfile = 'map_both.mat';
  injfile = 'M10a0.95eps0.2.dat';
elseif study==3  % pure cross ADI injection -----------
  outfile = 'map_cross.mat';
  injfile = 'cross.dat';
elseif study==4  % pure plus sinusoid -----------------
  outfile = 'map_wavep.mat';
  injfile = 'wave_plus.dat';
elseif study==5  % pure plus sinusoid -----------------
  outfile = 'map_test.mat';
  injfile = 'M10a0.95eps0.2.dat';
else
  return;
end

fprintf('loading %s...\n', outfile);
load(outfile);

% convert from power density to power
%map.y = map.y*(pp.deltaF/params.segmentDuration);
%map.sigma = map.sigma*(pp.deltaF/params.segmentDuration);

% compute from power density to power taking into account overlapping segments
map.y = map.y*(pp.deltaF);
map.sigma = map.sigma*(pp.deltaF);

% overlap factor
Ns = size(map.y,2);
z = (Ns+1)/(2*Ns);

% calculate total power
Ytot = z*sum(sum(map.y));

% load in injection file
fprintf('loading %s...\n', injfile)
g=load(injfile);
% get hp and hx
hp=g(:,2);
hx=g(:,3);
% time step
dt = g(2,1)-g(1,1);
% calculate time-domain Ytot0
Ytot0 = sum(hp.^2 + hx.^2)*dt;

fprintf('Ytot in freq domain = %1.3e strain^2\n', Ytot)
fprintf('Ytot in freq domain / Ytot in time domain = %2.1f%%\n', ...
  100*Ytot/Ytot0);

%---------now calculate energy------------------------------------------------
% total fluence
Ftot = z*sum(sum(map.F));

% 1 Mpc in cm
D = 3.08568025e24;
Etot = 4*pi*D^2*Ftot;
fprintf('Isotropic Energy = %1.3e erg\n', Etot);

