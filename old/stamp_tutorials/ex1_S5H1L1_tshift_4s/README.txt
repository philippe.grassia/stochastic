Instructions

1. Look inside frames/.  There should be a bunch of *.gwf files downloaded from
svn.  Now you can try making your own version of these frames.  Open matlab.
Run: preproc('ex1_params.txt', 'S5H1L1_full_run.txt', 1)
This will create a bunch of frames.  Try ls -hl frames/ to see if you made new
ones.

2. Use more to look at the contents of cachefiles/.  These are the cachefiles 
used by the main STAMP code stochmap.  They point to frames created by preproc.
Now create your own copies.  Open stamp*.pl and modify $FrDir to point to your
working directory.  Then run stamp*.pl.  Check to see if the cachefiles 
updated.

3. Now you are ready to run stochmap.  Run: run_stochmap_ex1.m from matlab.
You will create a bunch of .png file plots and a summary output map.mat file.

-------------------------------------------------------------------------------

4. The rest of these instructions are to carry out the comparison with the 
stochastic radiometer.  Not everyone will find this exercize useful; it's
mostly for the review process.  To run the radiometer analysis open matlab and
run: stochastic('ex1_params_radio.txt', 'S5H1L1_full_run.txt', 1)
This will analyze the same data analyzed by stochmap, but using preproc.  You 
can diff ex1_params.txt and ex1_params_radio.txt to see how the two paramfiles
differ.  The output radiometer files will be created in radio_out/.

5. To compare the radiometer result files in radio_out/ with the STAMP results
in map.mat, run: analyze_ex1.m.  This (short) script will output a description
of how the radiometer and STAMP output differs.
