clear all;

params = stampDefaults;

params.intFrameCachePath = './cachefiles/';
jobsFile=['./S5H1L1_full_run.txt'];

% search direction-------------------------------------------------------------
params.ra=6;   %right ascenion in hours (for search direction)
params.dec=30; %declination in degrees (for search direction)

% interferometer pair
params.ifo1='H1';
params.ifo2='L1';

% frequency range--------------------------------------------------------------
params.fmin = 40;
params.fmax = 500;

% override default parameters--------------------------------------------------
params.debug=true;
params.saveMat=true;
params.savePlots=true;
params.doFreqMask=false;
params.Autopower=true;
%params.doRadiometer=true;

% Call stochMap----------------------------------------------------------------
stochmap(params, jobsFile, 1);  % job 1
