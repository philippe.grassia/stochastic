This folder is designed to run preproc -> stochmap and produce ft-maps that clearly display a glitch and the effectiveness of the Xi statistic.
Contact: Tanner Prestegard (prestegard@physics.umn.edu)
Last updated: 8/12/201

INSTRUCTIONS
1. Open params.txt and modify the four lines under "% path to cache files" to accurately reflect your path to this directory.
2. Open Matlab in this directory.
3. In Matlab, run preproc with this line: preproc('params.txt','jobfile.txt',1)
4. Look at the plots that were produced, especially the SNR and Xi_snr plots.  The glitch should be evident as two columns of black and white pixels in the SNR plot, and as two columns of black pixels in the Xi_snr plot.
5. You can also load the .mat file into Matlab ( use: load('map.mat') ) to do more investigating.