Instructions

0. In this example we inject a loud Accretion Disk Instability (ADI) signal and
recover it with the burstCluster algorithm.  The injection file is 
ADI_4Mpc.dat.  The burstCluster parameters are specified in run_stochmap_ex5.m.

1. Run: preproc('ex5_params.txt', 'S5H1L1_full_run.txt', 1)

2. Open stamp*.pl and modify $FrDir to point to your working directory.  Then 
run stamp*.pl.  Check to see if the cachefiles updated.

3. Now you are ready to run stochmap.  Run: run_stochmap_ex5.m from 
matlab.

-------------------------------------------------------------------------------

4. In addition to making the usual stamp plots, this time you will also
generate:
        all_clusters_plot.png
        plots/large_cluster_plot.png
The former shows all the clusters that burstCluster was able to find while the
latter shows the most significant cluster, which in this case corresponds to
the ADI injection.

