clear all;

% load data
load map.mat;

% convert from power density to power
map.y = map.y*(pp.deltaF/params.segmentDuration);
map.sigma = map.sigma*(pp.deltaF/params.segmentDuration);

% create 1D arrays
yvals=map.yvals;
sigma=map.sigma(:);
y=map.y(:);

% remove buffer segments
sigma=sigma(y~=0);
y=y(y~=0);

fprintf('Y = %1.3e +/- %1.3e\n', mean(y), std(y)/sqrt(length(y)));
fprintf('sigma = %1.3e +/- %1.3e\n', mean(sigma), std(sigma));
fprintf('### Measured/Observed %1.3e\n', mean(y)/1e-5);

% to make spectrum, look at just one segment
yf = map.y(:,50);
sigmaf = map.sigma(:,50);

figure;
semilogy(yvals,yf,'b');
hold on;
semilogy([yvals(1) yvals(end)], 1e-5*[1 1], 'k');
hold on;
semilogy([yvals(1) yvals(end)], mean(y)*[1 1], 'r--');
xlabel('f (Hz)');
ylabel('Y (strain^2)');
%legend('Y','\sigma','Y_{avg}','\sigma_{avg}','inj');
legend('Y', 'Y_{avg}', 'inj');
pretty;
print('-djpeg', 'stamp_power.jpg');
print('-depsc2', 'stamp_power.eps');

% plot Y
printmap(map.y, map.xvals,map.yvals, 'time (s)', 'f (Hz)', ...
         'Y (strain^2)', 1e-5*[0.1 10]);
print('-dpng','h_map.png');
print('-depsc2','h_map.eps');

