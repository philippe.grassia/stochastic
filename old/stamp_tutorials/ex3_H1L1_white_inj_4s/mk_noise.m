function mk_noise()
% This script generates wn_1e-5.dat.  This dat file is an injection of white
% noise with power equal to 1e-5 on average in each frequency bin.  The
% waveform is pure plus polarized.

NF = 1e-5;  % desired power in each bin
dur = 500;  % injection duration
segdur = 4;  % segment duration
Fs = 16384; % sampling rate
N = Fs*segdur; % number of samples

df = 1/segdur;    % frequency resolution of processed data

% scale noise amplitude so that <power>=NF in each ft-map bin
noise_amp = sqrt(NF*N/2);

hp = noise_amp*randn(1,N);
hx = zeros(size(hp));
t = 1:length(hp);
t = t/Fs;

power_f = NF*N/2;    % theoretical power in freq domain
power_t = std(hp)^2; % measured power in time domain

% yet another cross-check: measured power in freq domain
window1=ones(size(hp));
hp_k=fft(hp,N);           
NX = N*norm(window1)^2; % unbiased
hp_ps = 2*abs(hp_k(1:N/2+1)).^2/NX;
mpower_f = sum(hp_ps);

fprintf('Desire h_rms^2 = %1.1e in each bin.\n', NF);
fprintf('Should be %i bins with 0.25 Hz resolution.\n', N);
fprintf('By Parsevals theorem, the');
fprintf(' total h_rms^2 summed over all bins should be\n');
fprintf('   (1/2) * %1.1e * %1.2e = %1.3e\n', NF, N, power_f);
fprintf('   (The half is because we use single-sided spectra.)\n');
fprintf('t-domain broadband value = %1.3e\n', power_t);
fprintf('#  ratio = power_t/power_f = %1.2e\n', power_t/power_f);
fprintf('#  ratio = mpower_f/power_f = %1.2e', mpower_f/power_f);
fprintf(' (measured over expected in freq domain)\n');

% now keep the same power level but lengthen the signal
N0 = Fs*dur;
hp = noise_amp*randn(1,N0);
hx = zeros(size(hp));
t = 1:length(hp);
t = t/Fs;

out = [t' hp' hx']; 
save wn_1e-5.dat out -ASCII; 
