#!/usr/bin/perl -w

#clean path from back to front (ie keeps earliest version of directory in path)
#assumes path split with :
#take path to clean as arguments
#returns command to set the new version of path

if ($#ARGV<0){exit;} #don't do anything if no first argument
$pathname=shift(@ARGV);
@newpath=();
$curpath=$ENV{$pathname};
@path=split(/\:+/,$curpath);
while($dir=shift(@path)){
    $found=0;
    chomp($dir);
    foreach(@newpath){
	chomp;
	if ($_ eq $dir){
	    $found=1;
	}
    }
    if(0==$found){
	push(@newpath,$dir);
    }
}
$path=join(':',@newpath);
print "$path\n";
