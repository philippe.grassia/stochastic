#!/bin/csh
#sets matapps and matlab environment variables
#   Can be inserted in .cshrc initialization code

# IF input parameter
#   SET top directory from input
# ENDIF
# IF top directory is defined
#   SET path to SDE sub-directory
# ENDIF
if ($#argv>0) then
  setenv MATAPPS_TOP $1
  setenv MATAPPS_TOP `echo "$MATAPPS_TOP" | sed 's/\/$//' `
endif
# if $?MATAPPS_TOP then
#   setenv MATAPPS_SDE ${MATAPPS_TOP}/SDE
# endif
#
# IF SDE sub-directory defined
#   IF MATLAB path already defined
#       ADD SDE sub-directory to path
#   ELSE
#       SET MATLAB path to SDE sub-directory
#   ENDIF
#   LOOP over directories in Matapps path list
#       ADD directory to MATLAB path
#   ENDLOOP
#
#  -- Define Octave path = Matlab path
# ENDIF
if $?MATAPPS_SDE then
  if $?MATLABPATH then
    setenv MATLABPATH ${MATAPPS_SDE}\:${MATLABPATH}
  else
    setenv MATLABPATH ${MATAPPS_SDE}
  endif
  foreach i(`cat ${MATAPPS_SDE}/matapps_paths.txt`)
    setenv MATLABPATH ${MATAPPS_TOP}/{$i}\:${MATLABPATH}
#clean the matlabpath variable from the back
# do this each step to avoid "Word too long." error
    setenv MATLABPATH `${MATAPPS_SDE}/cleanpath.pl MATLABPATH`
  end
#
  setenv OCTAVE_PATH ${MATLABPATH}
endif


