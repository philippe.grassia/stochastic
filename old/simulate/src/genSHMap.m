function [ map ] = genSHMap (P, pixelFile, mapFile, projType)
%
% [ map ] = genSHMap (P, pixelFile, mapFile, projType)
%
% P          Real vector. Real spherical harmonic moments
%            Length of this must be (lmax + 1)^2
%            Arrange data from l=0 to l=lmax, -l to l for each l
%            that is, P = (P_00;P_1-1;P_10;P_11; ...; P_lm; ...; P_lmaxlmax)
% pixelFile  String. File containing the pixels. 
%            Column 1: longitude, Column 2: latitude
% mapFile    String. Optional. File to write the map to a file
% projType   String. Optional. projection type to plot the map
%
% map        Real 3 column matrix.
%            Real matrix: Col 1: theta, Col 2: phi, Col 3: strength

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%

% Open the output file first, if specified
enableWrite = false;
if (nargin > 2 && ischar(mapFile))
    mapFileID = fopen (mapFile, 'w');
    if (mapFileID == -1)
        error(['Could not open file: ' mapFile]);
    else
        enableWrite = true;
    end
end


% Load the pixel file
fprintf ('\nLoading and processing pixels ');
pixels = load(pixelFile);

% Convert pixels to spherical angles
theta = pi/2 - pixels(:,2);
phi = pixels(:,1);
% We use phi = [-pi,pi)
for iPixel=1:length(phi)
    if (phi(iPixel)) >= pi
        phi(iPixel) = phi(iPixel) - 2.0*pi;
    end
end
fprintf('DONE\n');


% Find the highest order of spherical harmonics
lmax = sqrt(length(P)) - 1;
if ((lmax + 1)^2 ~= length(P))
    error(['Number of elements in P is not a perfect square']);
end

% Evaluate REAL spherical harmonic moments and add
tic; fprintf ('\nGenerating map by combining real spherical harmonics ');
map = zeros(length(theta),3);
map(:,1) = theta;
map(:,2) = phi;
for l=0:lmax

    Pl = P(l^2+1:(l+1)^2);
    NP = legendre(l,cos(theta),'norm');

    % m = 0
    map(:,3) = map(:,3) + (1/sqrt(2*pi))*Pl(l+1)*NP(1,:)';
    % m = 1 to l
    for m=1:l
        map(:,3) = map(:,3) + ((-1)^m/sqrt(pi))*NP(1+m,:)'.* ...
            (Pl(l+1-m)*cos(m*phi) - Pl(l+1+m)*sin(m*phi));
    end
end
fprintf('DONE\n'); toc;


% Write map to a file
% Write maps to mapFile
if (enableWrite)
    fprintf ('\nWriting map file ');
    fprintf (mapFileID,'%f\t%f\t%e\n', [ map(:,1)'; map(:,2)'; map(:,3)' ]);
    fclose(mapFileID);
    fprintf('DONE\n');
end


% Plot if asked
if (nargin > 3 && ischar(projType))
    xproject ([map(:,1),map(:,2)],map(:,3),projType);
end

return
