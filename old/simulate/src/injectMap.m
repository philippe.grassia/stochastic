function [ h1h2, map ] = ...
    injectMap (detector1, detector2, sourceFile, timeBins, freqBins,alpha,scale)

% Routine to simulate cross spectra <h_1*(f) h_2(f)> from a given SGWB
%
% [ h1h2, source ] = injectMap (detector1, detector2, sourceFile, ...
%    timeBins, freqBins, alpha, scale)
%
% detector1      String. Detector name 1 (like 'L1', 'H1', ...)
% detector2      String. Detector name 2 (like 'L1', 'H1', ...)
% sourceFile     Real 3 column matrix OR string.
%                Real matrix: Col 1: theta, Col 2: phi, Col 3: strength 
%                String: File containing source map in the above format
% timeBins       Real row vector. Central points of the time segments
% freqBins       Real row vector. Central points of the frequency bins
% alpha          Real. Power law index of source PSD H(f)
% scale          Real. Optional. Overall multiplicative factor for the sourceMap
%
% h1h2           Complex matrix. Column: frequency, Row: time

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%  

% Assuming integration for one day and 192sec chunk
% Assuming one sidereal day is 86400s, instead of 86164s
%totalTime = 86400.0;
%deltaTime = 192.0;
%timeBins = deltaTime/2:deltaTime:totalTime;


% Assuming 512Hz upper cut frequency and 1/4Hz bin size
%upperFreq=512.0;
%deltaFreq=0.25;
%freqBins = deltaFreq/2:deltaFreq:upperFreq;


% Load/process injected map
fprintf ('\nLoading and processing pixels ');
if (ischar(sourceFile))
    source = load(sourceFile);
else
    source = sourceFile;
end
clear sourceFile;

theta = source(:,1);
phi = source(:,2);

% Check if an overall scale factor is supplied
if ((nargin > 7) && isnumeric(scale))
    sourceMap = scale * source(:,3);
else
    sourceMap = source(:,3);
end

clear source;
map = [theta, phi, sourceMap];

% We use phi = [-pi,pi)
for iPixel=1:length(phi)
    if (phi(iPixel)) >= pi
        phi(iPixel) = phi(iPixel) - 2.0*pi;
    end
end


% Convert angles to direction cosines
pixOmega = [sin(theta').*cos(phi'); sin(theta').*sin(phi'); cos(theta')];
fprintf('DONE\n');


% Get detector response matrices "d^{ab}_i".
%      If input argument is a detector/site name, retrieve this data by
%      calling LoadDetectorData.  If the input argument is a 3x3 numerical
%      array, use that for the detector response matrix.
fprintf ('\nLoading and processing detector data ');
% Detector 1
if (ischar(detector1))
    DetData = LoadDetectorData(detector1);
    d1 = DetData.d;
    theta1 = (90-DetData.phi)*pi/180;
    phi1 = (DetData.lambda)*pi/180;
elseif (isnumeric(detector1) && isequal(size(detector1),[3 3]))
    % Keeping the option of a customized detector
    d1 = detector1.d;
    theta1 = (90-detector1.phi)*pi/180;
    phi1 = (detector1.lambda)*pi/180;
else
   error('Detector-1 not recognized. 4th argument should be a detector/site name.');  
end

% Detector 2
if (ischar(detector2))
    DetData = LoadDetectorData(detector2);
    d2 = DetData.d;
    theta2 = (90-DetData.phi)*pi/180;
    phi2 = (DetData.lambda)*pi/180;
elseif (isnumeric(detector2) && isequal(size(detector2),[3 3]))
    % Keeping the option of a customized detector
    d2 = detector2;
    theta2 = (90-detector2.phi)*pi/180;
    phi2 = (detector2.lambda)*pi/180;
else
    error('Detector-2 not recognized. 4th argument should be a detector/site name.');  
end

% Detector separation vector
earthRadius = 635675000.0;
detSep0 = [ sin(theta1)*cos(phi1) - sin(theta2)*cos(phi2); ...
            sin(theta1)*sin(phi1) - sin(theta2)*sin(phi2); ...
            cos(theta1) - cos(theta2) ] * earthRadius;

fprintf('DONE\n');


% If all the above inputs are read successfully...
% check the remaining arguments before starting costly computation


% Calculate the Euler rotation matrices at all times
tic; fprintf ('\nConstructing Euler rotation matrices ');
EulerZ = EulerRotationZ (timeBins);
fprintf('DONE\n'); toc;


% Get the detector separation vector at all times
tic; fprintf ('\nConstructing detector separation time array ');
detSep = rotateDetSep (detSep0, EulerZ);
fprintf('DONE\n'); toc;


% Get the overlap function Gamma for each pixel and time bin
% CAUTION: NOT the overlap reduction function "small" gamma
tic; fprintf ('\nConstructing capital Gamma ');
Gamma = overlap (theta, phi, EulerZ, d1, d2);
fprintf('DONE\n'); toc;


% Inject sources in the source map - it's fast
% can inject a full sky instead of only the injection points

% Generating signal and signal PSD
tic; fprintf ('\nGetting getting source PSD and cross spectra');
[ h1h2, H, invH ] = ...
    injectSignal (sourceMap, pixOmega, freqBins, alpha, detSep, Gamma);
fprintf('DONE\n'); toc;


return
