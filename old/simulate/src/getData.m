function [ s1s2, P1, P2, invP1, invP2 ] = ...
   getData (detector1, detector2, timeBins, freqBins)

% *** To use real data this function should be modified ***
% Presently this function generates noise only
%
% [ s1s2, P1, P2, invP1, invP2 ] =
%  getData (detector1, detector2, timeBins, freqBins)
%
% detector1    String. Detector name 1 (like 'L1', 'H1', ...)
% detector2    String. Detector name 2 (like 'L1', 'H1', ...)
% timeBins     Real vector. Position of time bins
% freqBins     Real vector. Position of frequency bins
%
% P1, P2       Real matrix. Noise PSDs of detectors at all time bins
%              Index 1: time, Index 2: frequency
% invP1,invP2  Real matrix. Inverse of detector noise PSDs at all time bins
%              Index 1: time, Index 2: frequency
% s1s2         Complex matrix. s1*(f)s2(f) at all time bins
%              Index 1: time, Index 2: frequency

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


P1 = zeros (length(timeBins), length(freqBins));
P2 = zeros (length(timeBins), length(freqBins));
invP1 = zeros (length(timeBins), length(freqBins));
invP2 = zeros (length(timeBins), length(freqBins));
s1s2 = complex (zeros(size(P1)),zeros(size(P1)));


lowFreqCutOff = 40.0;

% Change the random number state depending on the detector
% Useful for generating noise for a network
if (ischar(detector1))
   if (strcmp(detector1,'LHO') | strcmp(detector1,'H1'))
      state1 = 110; % some seed
      PSDDataFile1 = '../data/SRD.dat';
   elseif (strcmp(detector1,'LLO') | strcmp(detector1,'L1'))
      state1 = 22; % some seed
      PSDDataFile1 = '../data/SRD.dat';
   elseif (strcmp(detector1,'Virgo') | strcmp(detector1,'V'))
      state1 = 303; % some seed
      PSDDataFile1 = '../data/Virgo10.dat';
   else
      error(['Noise PSD file not specified for detector: ' sprintf('%s',detector1)]);
   end
end
if (ischar(detector2))
   if (strcmp(detector2,'LHO') | strcmp(detector2,'H1'))
      state2 = 110; % some seed
      PSDDataFile2 = '../data/SRD.dat';
   elseif (strcmp(detector2,'LLO') | strcmp(detector2,'L1'))
      state2 = 22; % some seed
      PSDDataFile2 = '../data/SRD.dat';
   elseif (strcmp(detector2,'Virgo') | strcmp(detector2,'V'))
      state2 = 303; % some seed
      PSDDataFile2 = '../data/Virgo10.dat';
   else
      error(['Noise PSD file not specified for detector: ' sprintf('%s',detector2)]);
   end
end

% Get the noise PSDs of the detectors
PSDData1 = load(PSDDataFile1); % (Un)comment according to scheme
PSDData2 = load(PSDDataFile2); % (Un)comment according to scheme
for iFreq=1:length(freqBins)
    if freqBins(iFreq) >= lowFreqCutOff

        % For PSDs from file
        % Uncomment the load data file and clear data statements before/after the loop
        P1(:,iFreq) = ones(length(timeBins),1) * ...
            10.0^interp1(log10(PSDData1(:,1)),2.0*log10(PSDData1(:,2)),log10(freqBins(iFreq)));
        P2(:,iFreq) = ones(length(timeBins),1) * ...
            10.0^interp1(log10(PSDData2(:,1)),2.0*log10(PSDData2(:,2)),log10(freqBins(iFreq)));
        
        % LIGO-I SRD function
        % P1(:,iFreq) = 9e-46*((4.49*freqBins(iFreq)/lowFreqCutOff)^(-56) + ...
        %    0.16*(freqBins(iFreq)/lowFreqCutOff)^(-4.52) + 0.52 + ...
        %    0.32*(freqBins(iFreq)/lowFreqCutOff)^2)*ones (length(timeBins),1);
        % P2(:,iFreq) = 9e-46*((4.49*freqBins(iFreq)/lowFreqCutOff)^(-56) + ...
        %    0.16*(freqBins(iFreq)/lowFreqCutOff)^(-4.52) + 0.52 + ...
        %    0.32*(freqBins(iFreq)/lowFreqCutOff)^2)*ones (length(timeBins),1);
        
        % White noise
        % P1(:,iFreq) = 1.0 * ones (length(timeBins),1);
        % P2(:,iFreq) = 1.0 * ones (length(timeBins),1);

        % Inverse of noise PSD ***extremely useful***
        invP1(:,iFreq) = 1.0 ./ P1(:,iFreq);
        invP2(:,iFreq) = 1.0 ./ P2(:,iFreq);

    end
end
clear PSDData1;  % (Un)comment according to scheme
clear PSDData2;  % (Un)comment according to scheme


% Generate Gaussian random noise
randn('state',state1); n1 = complex (randn(size(P1)), randn(size(P1)));
randn('state',state2); n2 = complex (randn(size(P2)), randn(size(P2)));


% Color noise using one sided detector noise PSDs
n1 = sqrt(P1)/2 .* n1;
n2 = sqrt(P2)/2 .* n2;


% Add only noise to data
s1s2 = conj(n1) .* n2;


return
