function Gamma = overlap (theta, phi, EulerZ, d1, d2)

% Compute "Gamma" - a combination of the antenna pattern functions for the
% all the pixels and a pair of detectors at all sidereal times  
%
% Gamma = overlap (theta, phi, EulerZ, d1, d2)
%
% theta    Real vector. Polar angles of the pixels
% phi      Real vector. Azimuthal angles of the pixels
% EulerZ   3-dimensional array. Last index is for sidereal time and 
%          the first two are for euler matrices
% d1       3x3 real array.  The response matrix d_{ab} of detector 1
% d2       3x3 real array.  The response matrix d_{ab} of detector 2
%
% Gamma    Real matrix. Combination of antenna patterns for all directions
%          and sideral times. Index 1: pixel, Index 2: sidereal time
%          *** CAUTION: Gamma is NOT the full overlap reduction function ***

%
% Formula copied from a code originally written by P. Sutton, 26 Apr 2006
%

% This function is written using loop structure because
% it takes very small time and this way memory cost is minimized


Gamma = zeros (length(theta), length(EulerZ(1,1,:)));


for iPixel=1:length(theta)
    
    % Compute polarization tensors
    m = [ sin(phi(iPixel)); -cos(phi(iPixel)); 0.0 ];
    n = [ cos(phi(iPixel))*cos(theta(iPixel)); ...
          sin(phi(iPixel))*cos(theta(iPixel)); ...
          - sin(theta(iPixel)) ];
    e_plus = m*m' - n*n';
    e_cross = m*n' + n*m';
    
    % Compute "Gamma" for all the directions for each time bin
    for iTime=1:length(EulerZ(1,1,:));

        % Detector 1
        dRotated = EulerZ(:,:,iTime)'*d1*EulerZ(:,:,iTime);
        F1_plus = trace(e_plus*dRotated);
        F1_cross = trace(e_cross*dRotated);

        % Detector 2
        dRotated = EulerZ(:,:,iTime)'*d2*EulerZ(:,:,iTime);
        F2_plus = trace(e_plus*dRotated);
        F2_cross = trace(e_cross*dRotated);

        % Overlap
        Gamma(iPixel,iTime) = F1_plus*F2_plus + F1_cross*F2_cross;
    end
end


return
