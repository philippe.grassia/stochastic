function handle = xproject(skyPositions, skyMap, projectionType, channelNames)
% XPROJECT Perform spherical or equal area projection of sky map data
%
% XPROJECT projects the given sky map data for viewing and printing.  Two types
% of projections are provided: A three dimensional projection onto a sphere,
% which may be interactively manipulated in the Matlab figure window, and a
% large variety of two dimensional projections.
%
% usage: 
%
%   handle = xproject(skyPositions, skyMap, projectionType, channelNames);
%
%   skyPositions       matrix of sky positions [radians]
%   skyMap             vector of sky map data
%   projectionType     projection type string (e.g. 'sphere' or 'mollweid')
%   channelNames       cell array of channel name strings
%
%   handle             handle to projected image
%
% The sky positions should be provided as a two column matrix of the form [theta
% phi], where theta is the geocentric colatitude running from 0 at the North
% pole to pi at the South pole and phi is the geocentric longitude in Earth
% fixed coordinates with 0 on the prime meridian.  The range of theta is [0, pi]
% and the range of phi is [-pi, pi).
%
% A projection type of 'sphere' produces an interactive three dimensional plot
% of the data on a sphere.  A projection type of 'unwrapped' produces an
% interactive three dimensional plot of the data in cos(baseline angle) space.
% A variety of two dimensional projections are also
% available through the Matlab mapping toolbox.  In particular, the projection
% type 'mollweid' produces the standard equal area Mollweide projection that is
% commonly used in astrophysical publications.  If no projection type is
% specified, XPROJECT assumes type 'sphere' by default.
%
% The optional cell array of channel name strings is used to label the detector
% zenith directions and pairwise detector baseline directions on spherical sky
% map plots.  If no channel names are specified, only the cartesian coordinate
% axes are labelled.
%
% See also XPIPELINE, XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, XMAPSKY
% XINTERPRET, and MAP.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id: xproject.m,v 1.1 2007-05-17 15:49:38 sanjit Exp $

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                detector data                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% labelling parameters
vectorRadius = 1.15;
labelRadius = 1.20;

% cartesian coordinate axis
X.zenith = [1 0 0];
Y.zenith = [0 1 0];
Z.zenith = [0 0 1];

% cartesian direction of detector zeniths
G.zenith = [+0.603505859; +0.104172766; +0.790524328;];
H.zenith = [-0.338402190; -0.599658144; +0.725185541;];
L.zenith = [-0.011751435; -0.861335199; +0.507901150;];
T.zenith = [-0.618003751; +0.527187743; +0.583219039;];
V.zenith = [+0.711661388; +0.131946680; +0.690020392;];

% cartesian location of detector corner stations
G.location = [+3.85631120e+06; +6.66597800e+05; +5.01964060e+06;];
H.location = [-2.16141493e+06; -3.83469518e+06; +4.60035022e+06;];
L.location = [-7.42760419e+04; -5.49628372e+06; +3.22425702e+06;];
T.location = [-3.94640900e+06; +3.36625900e+06; +3.69915100e+06;];
V.location = [+4.54637400e+06; +8.42990000e+05; +4.37857700e+06;];

% detector baselines
GH.baseline = G.location - H.location;
GL.baseline = G.location - L.location;
GT.baseline = G.location - T.location;
GV.baseline = G.location - V.location;
HL.baseline = H.location - L.location;
HT.baseline = H.location - T.location;
HV.baseline = H.location - V.location;
LT.baseline = L.location - T.location;
LV.baseline = L.location - V.location;
TV.baseline = T.location - V.location;
GH.baseline = GH.baseline / sqrt(sum(GH.baseline.^2));
GL.baseline = GL.baseline / sqrt(sum(GL.baseline.^2));
GT.baseline = GT.baseline / sqrt(sum(GT.baseline.^2));
GV.baseline = GV.baseline / sqrt(sum(GV.baseline.^2));
HL.baseline = HL.baseline / sqrt(sum(HL.baseline.^2));
HT.baseline = HT.baseline / sqrt(sum(HT.baseline.^2));
HV.baseline = HV.baseline / sqrt(sum(HV.baseline.^2));
LT.baseline = LT.baseline / sqrt(sum(LT.baseline.^2));
LV.baseline = LV.baseline / sqrt(sum(LV.baseline.^2));
TV.baseline = TV.baseline / sqrt(sum(TV.baseline.^2));

% plot arguments for cartesian coordinates
X.x = [0 1];
X.y = [0 0];
X.z = [0 0];
Y.x = [0 0];
Y.y = [0 1];
Y.z = [0 0];
Z.x = [0 0];
Z.y = [0 0];
Z.z = [0 1];

% plot arguments for detector zenith vectors
G.x = G.zenith(1) * [0 1];
G.y = G.zenith(2) * [0 1];
G.z = G.zenith(3) * [0 1];
H.x = H.zenith(1) * [0 1];
H.y = H.zenith(2) * [0 1];
H.z = H.zenith(3) * [0 1];
L.x = L.zenith(1) * [0 1];
L.y = L.zenith(2) * [0 1];
L.z = L.zenith(3) * [0 1];
T.x = T.zenith(1) * [0 1];
T.y = T.zenith(2) * [0 1];
T.z = T.zenith(3) * [0 1];
V.x = V.zenith(1) * [0 1];
V.y = V.zenith(2) * [0 1];
V.z = V.zenith(3) * [0 1];

% plot arguments for detector baseline vectors
GH.x = GH.baseline(1) * [-1 +1];
GH.y = GH.baseline(2) * [-1 +1];
GH.z = GH.baseline(3) * [-1 +1];
GL.x = GL.baseline(1) * [-1 +1];
GL.y = GL.baseline(2) * [-1 +1];
GL.z = GL.baseline(3) * [-1 +1];
GT.x = GT.baseline(1) * [-1 +1];
GT.y = GT.baseline(2) * [-1 +1];
GT.z = GT.baseline(3) * [-1 +1];
GV.x = GV.baseline(1) * [-1 +1];
GV.y = GV.baseline(2) * [-1 +1];
GV.z = GV.baseline(3) * [-1 +1];
HL.x = HL.baseline(1) * [-1 +1];
HL.y = HL.baseline(2) * [-1 +1];
HL.z = HL.baseline(3) * [-1 +1];
HT.x = HT.baseline(1) * [-1 +1];
HT.y = HT.baseline(2) * [-1 +1];
HT.z = HT.baseline(3) * [-1 +1];
HV.x = HV.baseline(1) * [-1 +1];
HV.y = HV.baseline(2) * [-1 +1];
HV.z = HV.baseline(3) * [-1 +1];
LT.x = LT.baseline(1) * [-1 +1];
LT.y = LT.baseline(2) * [-1 +1];
LT.z = LT.baseline(3) * [-1 +1];
LV.x = LV.baseline(1) * [-1 +1];
LV.y = LV.baseline(2) * [-1 +1];
LV.z = LV.baseline(3) * [-1 +1];
TV.x = TV.baseline(1) * [-1 +1];
TV.y = TV.baseline(2) * [-1 +1];
TV.z = TV.baseline(3) * [-1 +1];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        process command line arguments                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for sufficient command line arguments
error(nargchk(2, 4, nargin));

% provide default arguments 
defaultProjectionType = 'sphere';
defaultChannelNames = {};
if nargin == 2,
  projectionType = defaultProjectionType;
  channelNames = defaultChannelNames;
elseif nargin == 3,
  channelNames = defaultChannelNames;
end

% validate sky positions
if size(skyPositions, 2) < 2,
  error('sky positions must be at least a two column matrix [theta phi]');
end

% extract spherical coordinates
theta = skyPositions(:, 1);
phi = skyPositions(:, 2);

% validate spherical coordinates
if any((theta < 0) | (theta > pi)),
  error('theta outside of [0, pi]');
end
if any((phi < -pi) | (phi >= pi)),
  error('phi outside of [-pi, pi)');
end

% force lowercase projection name
projectionType = lower(projectionType);

% if channel names is not a cell array,
if ~iscell(channelNames),

  % insert channel names into a single cell
  channelNames = mat2cell(channelNames, size(channelNames, 1), ...
                          size(channelNames, 2));

% otherwise, continue
end

% force one dimensional cell array
channelNames = channelNames(:);

% number of channel names
numberOfChannels = length(channelNames);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               determine labels                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% number of labels
numberOfLabels = 3 + numberOfChannels + ...
    numberOfChannels * (numberOfChannels - 1) / 2;

% initialize labels
labels = cell(numberOfLabels, 1);

% add cartesian coordinate axes to labels
labels{1} = 'X';
labels{2} = 'Y';
labels{3} = 'Z';

% add detector zeniths to labels
labelNumber = 4;
for channelNumber = 1 : numberOfChannels,
  labels{labelNumber} = channelNames{channelNumber}(1);
  labelNumber = labelNumber + 1;
end

% add detector baselines to labels
for primaryChannelNumber = 2 : numberOfChannels,
  for secondaryChannelNumber = 1 : primaryChannelNumber - 1,
    labels{labelNumber} = [channelNames{primaryChannelNumber}(1) ...
                           channelNames{secondaryChannelNumber}(1)];
    labelNumber = labelNumber + 1;
  end
end

% force uppercase labels
labels = upper(labels);

% alphabetically order baseline labels
for labelNumber = 1 : numberOfLabels,
  labels{labelNumber} = sort(labels{labelNumber});
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                 plot sphere                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(projectionType, 'sphere'),

  % cartesian coordinates of sphere
  x = sin(theta) .* cos(phi);
  y = sin(theta) .* sin(phi);
  z = cos(theta);
  
  % tesselate sphere
  triangulation = convhulln([x, y, z]);

  % plot sphere
  cla;
  handle = trisurf(triangulation, x, y, z, skyMap);
  shading interp;
  axis image;
  axis vis3d
  axis off;

  % add labels
  hold on;
  for labelNumber = 1 : numberOfLabels,
    eval(['plot3(vectorRadius * ' labels{labelNumber} '.x, ' ...
          '      vectorRadius * ' labels{labelNumber} '.y, ' ...
          '      vectorRadius * ' labels{labelNumber} '.z, ''k-'');']);;
    eval(['textHandle = text(labelRadius * ' labels{labelNumber} '.x, ' ...
          '                  labelRadius * ' labels{labelNumber} '.y, ' ...
          '                  labelRadius * ' labels{labelNumber} '.z, ' ...
          '                  labels{labelNumber});']);;
    set(textHandle, 'HorizontalAlignment', 'center');
  end
  hold off;
  
  % enable 3d rotation
  rotate3d on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          unwrapped cos(baseline theta)                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elseif strcmp(projectionType, 'unwrapped'),


  % unwrapped coordinates of space
  CP = xSP2CP(skyPositions, channelNames);
  
  % tesselate space
  triangulation = delaunay(CP(:,1),CP(:,2));

  % plot sphere
  cla;
  handle = trisurf(triangulation, CP(:,1), CP(:,2), CP(:,3), skyMap);
  shading interp;
  axis image;
  axis vis3d
  %axis off;
  
  % enable 3d rotation
  rotate3d on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          two dimensional projection                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% otherwise, perform two dimensional projection
else,

  % compute sinusoidal projection
  mapstruct = defaultm(projectionType);
  mapstruct.angleunits = 'radians';
  mapstruct.origin = [0 0 0];
  mapstruct.falseeasting = 0;
  mapstruct.falsenorthing = 0;
  mapstruct.scalefactor = 1;
  [x, y] = mfwdtran(mapstruct, pi / 2 - theta, phi);
  z = zeros(size(x));

  % display projection
  cla;
  triangulation = delaunay(x, y);
  handle = trisurf(triangulation, x, y, z, skyMap);
  shading interp;
  view([0 0 1]);
  axis image;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

return;
