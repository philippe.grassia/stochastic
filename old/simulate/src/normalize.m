function beamNorm = normalize (fSpectrum, Gamma)

% Calculate the normalization constant for the beam at each pixel
%
% beamNorm = normalize (fSpectrum, Gamma)
%
% fSpectrum  Real matrix. Combination of the source & detector PSDs 
%            [GTilde(t,f) in text]. Index-1: Time, Index-2: frequency
% Gamma      Real matrix. Combination of antenna patterns for all directions
%            and sideral times. Index 1: pixel, Index 2: sidereal time
%            *** CAUTION: Gamma is NOT the full overlap reduction function ***
%
% beamNorm   Real vector. Normalization constant for each pixel

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


beamNorm = zeros (length(Gamma(:,1)),1);


fSum = (2.0 * sum (fSpectrum,2));
tSum = (Gamma.*Gamma) * fSum;


beamNorm = 1.0 ./ tSum;


return
