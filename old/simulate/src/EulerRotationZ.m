function EulerZ = EulerRotationZ (timeBins)

% Compute Euler rotation matrices about the Z-axis at all sidereal times
%
% EulerZ = EulerRotationZ (timeBins)
%
% timeBins Real vector. The array of midpoints of sidereal time bins
%
% EulerZ   3-dimensional array. Last index is for sidereal time and 
%          the first two are for Euler matrices

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


% Earth's rotation velocity (actually 1 day is 86164s, assuming 86400s)
omegaEarth = 2.0*pi/86400;

% Time to rotation angle
rotAngle = omegaEarth*timeBins;

% Caculate the Euler Z matrix for all sidereal time intervals
for iTime=1:length(timeBins)
    EulerZ(:,:,iTime) = [cos(rotAngle(iTime)), sin(rotAngle(iTime)), 0.0; ...
                        -sin(rotAngle(iTime)), cos(rotAngle(iTime)), 0.0; ...
                                 0.0,                  0.0,          1.0];
end


return
