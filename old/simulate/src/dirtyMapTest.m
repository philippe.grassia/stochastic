function [ theta, phi, sourceMap, dirtyMap, beamNorm ] = ...
    dirtyMapTest (detector1, detector2, pixelFile, sourceFile, mapFile,projType)

% Only calculates the dirty map
%
% [ theta, phi, sourceMap, dirtyMap, beamNorm ] = dirtyMapTest ...
%  (detector1, detector2, pixelFile, sourceFile, mapFile, projType)
%
% detector1      String. Detector name 1 (like 'L1', 'H1', ...)
% detector2      String. Detector name 2 (like 'L1', 'H1', ...)
% pixelFile      String. File containing the pixels. 
%                Column 1: longitude, Column 2: latitude
% sourceFile     String. File containing the source map to be injected
%                Column 1: theta, Column 2: phi, Column 3: strength
% mapFile        String. File to write map to a file
% projType       String. Optional. If specifiedcthe skymaps will be plotted
%                using that projType as the projection type
%                Currently supported projectionTypes: 'sphere', 'mollweid'
%
% theta          Real vector. Polar angles of the pixels
% phi            Real vector. Azimuthal angles of the pixels
% sourceMap      Real 3 column matrix.
%                Column 1: theta, Column 2: phi, Column 3: strength
% dirtyMap       Real vector. Raw/dirty/concolved map of the sky
%                Each component corresponds to a pixel
% beamNorm       Real vector. Normalization constants

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


% Open the output file first
mapFileID = fopen (mapFile, 'w');
if (mapFileID == -1)
    error(['Could not open file: ' mapFile]);
end


% Assuming integration for one day and 60sec chunk
% Assuming one day is 86400s, instead of 86164s
totalTime = 86400.0;
deltaTime = 192.0;
timeBins  = deltaTime/2:deltaTime:totalTime;


% Assuming 512Hz upper cut frequency and 1/4Hz bin size
upperFreq = 512.0;
deltaFreq = 2;
freqBins  = deltaFreq/2:deltaFreq:upperFreq;


% Load the pixel file
tic; fprintf ('\nLoading and processing pixels ');
pixels = load(pixelFile);

% Convert pixels to angles
theta = pi/2 - pixels(:,2);
phi = pixels(:,1);
% We use phi = [-pi,pi)
for iPixel=1:length(phi)
    if (phi(iPixel)) >= pi
        phi(iPixel) = phi(iPixel) - 2.0*pi;
    end
end

% Convert angles to direction cosines
pixOmega = [sin(theta').*cos(phi'); sin(theta').*sin(phi'); cos(theta')];

% Free memory
clear pixels;
fprintf('DONE\n'); toc;


% Get detector response matrices "d^{ab}_i".
%      If input argument is a detector/site name, retrieve this data by
%      calling LoadDetectorData.  If the input argument is a 3x3 numerical
%      array, use that for the detector response matrix.
tic; fprintf ('\nLoading and processing detector data ');
% Detector 1
if (ischar(detector1))
    DetData = LoadDetectorData(detector1);
    d1 = DetData.d;
    theta1 = (90-DetData.phi)*pi/180;
    phi1 = (DetData.lambda)*pi/180;
elseif (isnumeric(detector1) && isequal(size(detector1),[3 3]))
    % Keeping the option of a customized detector
    d1 = detector1.d;
    theta1 = (90-detector1.phi)*pi/180;
    phi1 = (detector1.lambda)*pi/180;
else
   error('Detector-1 not recognized. 4th argument should be a detector/site name.');  
end

% Detector 2
if (ischar(detector2))
    DetData = LoadDetectorData(detector2);
    d2 = DetData.d;
    theta2 = (90-DetData.phi)*pi/180;
    phi2 = (DetData.lambda)*pi/180;
elseif (isnumeric(detector2) && isequal(size(detector2),[3 3]))
    % Keeping the option of a customized detector
    d2 = detector2;
    theta2 = (90-detector2.phi)*pi/180;
    phi2 = (detector2.lambda)*pi/180;
else
    error('Detector-2 not recognized. 4th argument should be a detector/site name.');  
end

% Detector separation vector
earthRadius = 6.4e8;
detSep0 = [ sin(theta1)*cos(phi1) - sin(theta2)*cos(phi2); ...
            sin(theta1)*sin(phi1) - sin(theta2)*sin(phi2); ...
            cos(theta1) - cos(theta2) ] * earthRadius;

fprintf('DONE\n'); toc;


% If all the above inputs are read successfully...
% check the remaining arguments before starting costly computation

% Check if plotting is enabled
if (nargin > 5 && ischar(projType))
    fprintf ('\nMap will be plotted using projection type: %s\n\n',projType);
else
    fprintf ('\nPlotting is disabled\n');
end
        

% Calculate the Euler rotation matrices at all times
tic; fprintf ('\nConstructing Euler rotation matrices ');
EulerZ = EulerRotationZ (timeBins);
fprintf('DONE\n'); toc;


% Get the detector separation vector at all times
tic; fprintf ('\nConstructing detector separation time array ');
detSep = rotateDetSep (detSep0, EulerZ);
fprintf('DONE\n'); toc;


% Get the overlap function Gamma for each pixel and time bin
% CAUTION: NOT the overlap reduction function "small" gamma
tic; fprintf ('\nConstructing capital Gamma ');
Gamma = overlap (theta, phi, EulerZ, d1, d2);
fprintf('DONE\n'); toc;


% Get data, noise PSDs
tic; fprintf ('\nGenerate/read data, get noise PSDs');
[ s1s2,P1,P2,invP1,invP2 ] = getData (detector1,detector2,timeBins,freqBins);
fprintf('DONE\n'); toc;


% Inject sources in the source map - it's fast
% can inject a full sky instead of only the injection points

% Generating signal and signal PSD
HAlpha = 5e-47;
alpha = 0;

H = HAlpha*((freqBins'/100.0) .^ alpha);
invH = 1.0 ./ H;

tic; fprintf ('\nGetting and injecting signal in data, getting source PSD ');
[ h1h2, sourceMap ] = ...
    injectMap (detector1, detector2, sourceFile, timeBins, freqBins, alpha);

% Inject signal in data
% s1*(f) s2(f) = <h1*(f) h2(f)> + [pure detector noise n(f)]
% noise due to stochastic nature of h(f) is neglected, as, statistically:
% h1*(f) h2(f) + n1*(f) h2(f) + h1*(f) n2(f) << n1*(f) n2(f)
s1s2 = s1s2 + h1h2;

fprintf('DONE\n'); toc;


% Precompute the spectral info
tic; fprintf ('\nPrecomputing spectral info ');
fSpectrum = repmat((H.*H)',length(timeBins),1).*(invP1.*invP2);
fprintf('DONE\n'); toc;


% Get normalization constant vector
tic; fprintf ('\nComputing normalization coefficients ');
beamNorm = normalize (fSpectrum, Gamma) /deltaTime;
fprintf('DONE\n'); toc;


% Make unnormalized "dirty" map
% Map will be normalized later, efficient for deconvolution
tic; fprintf ('\nMaking UNNORMALIZED dirty map ');
dirtyMap = makeRawMap (s1s2,pixOmega, freqBins, detSep,Gamma, H,invP1,invP2);
fprintf('DONE\n'); toc;


% If 6th argument is specified plot the maps
if (nargin > 5 && ischar(projType))
    
    % Plot the source map
    figure;
    xproject ([sourceMap(:,1),sourceMap(:,2)], sourceMap(:,3), projType);
    grid off;
    axis ('off');
    colorbar('SouthOutside');
    
    % Plot the dirty map
    figure;
    xproject ([theta,phi], beamNorm.*dirtyMap, projType);
    grid off;
    axis ('off');
    colorbar('SouthOutside');
    
end


% Write maps to mapFile
tic; fprintf ('\nWriting map file ');
fprintf (mapFileID,'%f\t%f\t%e\t%e\t%e\n', ...
    [ theta'; phi'; sourceMap'; (beamNorm.*dirtyMap)'; beamNorm' ]);
fclose(mapFileID);
fprintf('DONE\n'); toc;


return
