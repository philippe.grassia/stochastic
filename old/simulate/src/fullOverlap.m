function [ gammaFull ] = ...
   fullOverlap (Gamma, sourceMap, pixOmega, detSep, freqBins)
%
% Calculates the full overlap reduction function
%
% [ gammaFull ] = fullOverlap (Gamma, sourceMap, pixOmega, detSep, freqBins)
%
% Gamma      Real matrix. Combination of antenna patterns for all directions
%            and sideral times. Index 1: pixel, Index 2: sidereal time
%            *** CAUTION: Gamma is NOT the full overlap reduction function ***
% sourceMap  Real vector. Injected map of the sky
% pixOmega   Real matrix. Direction cosines of the pixels
%            Index 1: pixel number, Index 2: axis index
% detSep     Real matrix. Detector separation vector at all sideral times
%            Index 1: Vector component, Index 2: sidereal time
%
% gammaFull  Complex Matrix. Full overlap reduction function
%            Index 1: time, Index 2: frequency

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%

gammaFull = complex(zeros(length(Gamma(1,:)),length(freqBins)),...
   zeros(length(Gamma(1,:)),length(freqBins)));


velLight = 2.99792458e10;
pixelSize = 4.0 * pi / length(Gamma(:,1));


for iPixel = 1:length(sourceMap)
   gammaFull = gammaFull + ...
      sourceMap(iPixel) * repmat(Gamma(iPixel,:)',1,length(freqBins)) .* ...
      exp(2.0*pi*i*((detSep'*pixOmega(:,iPixel))*freqBins)/velLight);
end

gammaFull = pixelSize * gammaFull;


return
