function detSep = rotateDetSep (detSep0, EulerZ)

% Compute the detector separation vector at each sidereal time
%
% detSep = rotateDetSep (detSep0, EulerZ)
%
% detSep0   Real vector. Initial detector separation vector
% EulerZ    3-dim array. First two indices are for Euler matrix
%           Third index is for sidereal time
%
% detSep    Real matrix. Detector separation vector at all sideral times
%           Index 1: Vector component, Index 2: sidereal time

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


detSep = zeros (3,length(EulerZ(1,1,:)));


for iTime = 1:length(EulerZ(1,1,:))
    detSep(:,iTime) = EulerZ(:,:,iTime)'*detSep0;
end


return
