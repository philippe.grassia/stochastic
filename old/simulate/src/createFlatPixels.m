function createFlatPixels (longBin, latBin, outFile)

% Create a two dimensional array of pixels by dividing
% longitude and latitude in equal bins
%
% createFlatPixels (longBin, latBin, outFile)
%
% longBin   Integer. Number of longitude bins
% latBin    Integer. Number of latitude bins
%
% outFile   String. Output file.
%           Column 1: longitude, Column 2: latitude (both in radians)

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%

% All sky
longMin = -pi;
longMax = pi;

latMin = -pi/2;
latMax = pi/2;


% Part sky
longMin = -pi/12;
longMax = pi/12;

latMin = pi/15 - pi/12;
latMax = pi/15 + pi/12;

% Pixel size
longBinSize = (longMax - longMin)/longBin;
latBinSize = (latMax - latMin)/latBin;

longitudes = (longMin + longBinSize/2):longBinSize:longMax;
latitudes = (latMax-latBinSize/2):(-latBinSize):latMin;


fid = fopen (outFile,'w');

for iLat = 1:latBin
    for iLong = 1:longBin
        fprintf (fid, '%f\t%f\n', longitudes(iLong), latitudes(iLat));
    end
end
