#!/usr/bin/python

"""
%prog

Michael Coughlin (michael.coughlin@ligo.org)

This program does waveform comparisons.

"""

import os, sys, pickle, math, optparse, glob, time, optparse, getopt, glob
import numpy as np
import pycbc.waveform, pycbc.fft, pycbc.types, pycbc.filter

from pylal import Fr
import mdc_plot

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def plot_waveform(data,outputFolder):

    plotName = os.path.join(outputFolder,"strain.png")
    mdc_plot.plot_strain(data["t"],data["h"],plotName)

    plotName = os.path.join(outputFolder,"amplitude.png")
    mdc_plot.plot_frequency(data["f"],data["hamp"],plotName)

    plotName = os.path.join(outputFolder,"phase.png")
    mdc_plot.plot_phase(data["f"],data["hphase"],plotName)

def read_frames(start_time,end_time,channel,cache):
    time = []
    data = []

    #== loop over frames in cache
    for frame in cache:
        frame_data,data_start,_,dt,_,_ = Fr.frgetvect1d(frame,channel)
        frame_length = float(dt)*len(frame_data)
        frame_time = data_start+dt*np.arange(len(frame_data))

        for i in range(len(frame_data)):
            if frame_time[i] <= start_time:  continue
            if frame_time[i] >= end_time:  continue
            time.append(frame_time[i])
            data.append(frame_data[i])

    return time,data

def fft_strain(t,h):

    data = {}
    data["t"] = t
    data["h"] = h

    delta_t = t[1] - t[0]

    ts = pycbc.types.TimeSeries(h,delta_t=delta_t,dtype=pycbc.types.float64)
    htilda = pycbc.types.FrequencySeries(np.zeros(len(h)/2 + 1), delta_f=1, dtype=pycbc.types.complex128)
    pycbc.fft.fft(ts, htilda)

    htilda_real = np.real(htilda)
    htilda_imag = np.imag(htilda)
    hamp = np.sqrt(htilda_real**2 + htilda_imag**2)
    hphase = np.arctan(np.divide(htilda_imag,htilda_real))

    f = []
    for freq in htilda.sample_frequencies:
        f.append(freq)
    f = np.array(f)

    data = {}
    data["t"] = t
    data["h"] = h
    data["f"] = f
    data["htilda"] = np.array(htilda)
    data["htilda_real"] = htilda_real
    data["htilda_imag"] = htilda_imag
    data["hamp"] = hamp
    data["hphase"] = hphase

    return data

# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    start_time = 800001000
    end_time =   800001500

    fileLocation = "./output"
    ifos = ["H1","H2","L1"]
    #ifos = ["E1"]

    outputLocation = "./plots"

    for ifo in ifos:
        print "Running %s\n"%(ifo)
        ifoLocation = "%s/%s"%(fileLocation,ifo)
        frames = glob.glob("%s/*.gwf"%(ifoLocation))

        channel = "%s:STRAIN"%(ifo)
        t, x = read_frames(start_time,end_time,channel,frames)
        data = fft_strain(t,x)

        outputFolder = "%s/%s"%(outputLocation,ifo)
        if not os.path.isdir(outputFolder):
            os.mkdir(outputFolder)

        plot_waveform(data,outputFolder)


