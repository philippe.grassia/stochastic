#!/usr/bin/python

"""
%prog

Michael Coughlin (michael.coughlin@ligo.org)

This program does waveform comparisons.

"""

import os, math
import numpy as np
import matplotlib
matplotlib.rc('text', usetex=True)
import matplotlib.pyplot as plt

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def plot_strain(t,x,plotName):

    plt.plot(t-np.min(t),x)

    plt.xlabel("Time [s] [%f]"%(np.min(t)))
    plt.ylabel("Amplitude")

    #plt.xlim([0,60])

    plt.show()
  
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_frequency(f,x,plotName):

    amp = []
    for y in x:
        if y == 0.0:
            amp.append(10**-30)
        else:
            amp.append(y)

    plt.loglog(f,amp)

    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Amplitude")

    plt.xlim([10,1024])

    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')

def plot_phase(f,x,plotName):

    plt.semilogx(f,x)

    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Amplitude")

    plt.show()
    plt.savefig(plotName,dpi=200)
    plt.close('all')

