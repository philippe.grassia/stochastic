#!/usr/bin/python

"""
%prog

Michael Coughlin (michael.coughlin@ligo.org)

This program does waveform comparisons.

"""

import os
import numpy as np
import subprocess
from subprocess import Popen


lalapps_inspinj = "/home/mcoughlin/LAL/master/src/lscsoft/lalsuite/lalapps/src/inspiral/lalapps_inspinj"
lalapps_mdc_ninja = "/home/mcoughlin/LAL/master/opt/lscsoft/lalapps/bin/lalapps_mdc_ninja" 

gpsStart = 900000000
gpsEnd =   900002048
minDistance = 1
maxDistance = 10

waveformInject = "TaylorF2threePointFivePN"
#waveformInject = "EOBNRpseudoFourPN"
waveformInject = "TaylorT4threePointFivePN"
minMass1 = 1
maxMass1 = 10
minMass2 = 1
maxMass2 = 10
minMassTotal = minMass1 + minMass2
maxMassTotal = maxMass1 + maxMass2

outputdir = "/home/mcoughlin/Stochastic/MDC/frames"
outputxml = "/home/mcoughlin/Stochastic/MDC/output.xml"

injectionFileCommand = "%s --gps-end-time %d --min-distance %s --max-distance %s --gps-start-time %d --waveform %s --f-lower 30 --time-interval 0 --seed 0 --verbose --disable-spin --l-distr random --d-distr log10  --i-distr uniform --m-distr componentMass --min-mass1 %f --max-mass1 %f --min-mass2 %f --max-mass2 %f --min-mtotal %f --max-mtotal %f --disable-spin --time-step 1000 --taper-injection start --seed 1201 --output %s"%(lalapps_inspinj,gpsEnd,minDistance,maxDistance,gpsStart,waveformInject,minMass1,maxMass1,minMass2,maxMass2,minMassTotal,maxMassTotal,outputxml)

p = subprocess.Popen(injectionFileCommand.split(" "),stdout=subprocess.PIPE,stderr=subprocess.PIPE)
output, errors = p.communicate()
print injectionFileCommand
print output, errors

#frameCommand = "%s --write-frame --gps-start-time %d --gps-end-time %d --sample-rate 16384 --all-ifos --injection-file %s --fr-out-dir %s --injection-type approximant --frame-type MDC --debug-level 1 --simulate-noise --freq-low-cutoff 10 --strain-lowpass-freq 10"%(lalapps_mdc_ninja,gpsStart,gpsEnd,outputxml,outputdir)
frameCommand = "%s --write-frame --gps-start-time %d --gps-end-time %d --sample-rate 16384 --all-ifos --injection-file %s --fr-out-dir %s --injection-type approximant --frame-type MDC --debug-level 1 --freq-low-cutoff 10 --strain-lowpass-freq 10"%(lalapps_mdc_ninja,gpsStart,gpsEnd,outputxml,outputdir)


p = subprocess.Popen(frameCommand.split(" "),stdout=subprocess.PIPE,stderr=subprocess.PIPE)
output, errors = p.communicate()
print frameCommand
print output, errors

