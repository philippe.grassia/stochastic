#!/usr/bin/python

import os, sys, glob, optparse, warnings, datetime, time, matplotlib, math, random
import numpy as np
from pylal import Fr

__author__ = "Michael Coughlin <michael.coughlin@ligo.org>"
__date__ = "2012/8/26"
__version__ = "0.1"

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser(usage=__doc__,version=__version__)

    parser.add_option("-s", "--gps_start_time",default=800000000,type=int)
    parser.add_option("-d", "--time_interval",default=1000,type=int)
    parser.add_option("-l", "--segment_duration",default=2048,type=int)
    parser.add_option("-n", "--number_of_segments",default=1,type=int)

    parser.add_option("-v", "--verbose", action="store_true", default=False,
                      help="Run verbosely. (Default: False)")

    opts, args = parser.parse_args()

    # show parameters
    if opts.verbose:
        print >> sys.stderr, ""
        print >> sys.stderr, "running network_eqmon..."
        print >> sys.stderr, "version: %s"%__version__
        print >> sys.stderr, ""
        print >> sys.stderr, "***************** PARAMETERS ********************"
        for o in opts.__dict__.items():
          print >> sys.stderr, o[0]+":"
          print >> sys.stderr, o[1]
        print >> sys.stderr, ""

    return opts

def read_frames(start_time,end_time,channel,cache):

    fs = 1024

    time = np.arange(start_time,end_time,1.0/fs)
    data = np.zeros(len(time))

    #print time, data
    #print penis

    #== loop over frames in cache
    for frame in cache:

        frameSplit = frame.replace(".gwf","").split("-")
        gps = float(frameSplit[-2])
        dur = float(frameSplit[-1])

        framegpsStart = gps
        framegpsEnd = gps + dur
        if framegpsStart > end_time or framegpsEnd < start_time:
            continue

        frame_data,data_start,_,dt,_,_ = Fr.frgetvect1d(frame,channel)
        frame_length = float(dt)*len(frame_data)
        frame_time = data_start+dt*np.arange(len(frame_data))

        #time = np.concatenate([time,frame_time[(frame_time >= start_time) & (frame_time <= end_time)]])
        #data = np.concatenate([data,frame_data[(frame_time >= start_time) & (frame_time <= end_time)]])

        print frame, "loaded"

        frame_data_interp = np.interp(time,frame_time,frame_data,left=0,right=0)

        data = data + frame_data_interp

    return time,data

# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    warnings.filterwarnings("ignore")

    # Parse command line
    opts = parse_commandline()

    baseDir = "/home/mcoughlin/Stochastic/MDC/aLIGO_Gaussian" 

    MDC_Generation = "/home/mcoughlin/matapps/packages/stochastic/trunk/MDC_Generation"
    list2 = "%s/list2"%(MDC_Generation)
    Mdc_AL = "%s/Mdc_AL"%(MDC_Generation)

    mdc_create_xml = "/home/mcoughlin/matapps/packages/stochastic/trunk/MDC/mdc_create_xml"
    lalapps_mdc_ninja = "/home/mcoughlin/LAL/master/opt/lscsoft/lalapps/bin/lalapps_mdc_ninja"

    gpsStart = 800000000
    numberFrames = 1
    frameDur = 2048
    fs = 2048
    flow = 40
    #flow = 100
    timeIntervalBetweenEvents = 500
    gpsEnd = gpsStart + frameDur * numberFrames

    gpsFrames = np.arange(gpsStart,gpsEnd,frameDur)
   
    if not os.path.isdir("input"): 
        copyInput = "cp -r %s/input ."%(MDC_Generation)
        os.system(copyInput)
    if not os.path.isdir("lists"):
        os.mkdir("lists")

    list2Command = "%s -t %d -n %d -l %d -r %d -f %d -d %d"%(list2,gpsStart,numberFrames,frameDur,fs,flow,timeIntervalBetweenEvents)
    os.system(list2Command)

    if not os.path.isdir(baseDir):
        os.mkdir(baseDir)

    noiseFrameDir = os.path.join(baseDir,"noiseframes")
    if not os.path.isdir(noiseFrameDir):
        os.mkdir(noiseFrameDir)
    injectionFrameDir = os.path.join(baseDir,"injectionframes")
    outputDir = os.path.join(baseDir,"output")
    if not os.path.isdir(outputDir):
        os.mkdir(outputDir)

    rmLists = "rm -r %s/lists"%(baseDir)
    os.system(rmLists)
    rmInput = "rm -r %s/input"%(baseDir)
    os.system(rmInput)

    mvLists = "mv lists %s"%(baseDir)
    os.system(mvLists)
    mvInput = "mv input %s"%(baseDir)
    os.system(mvInput)

    listFile = "%s/lists/list_cbc.txt"%(baseDir)
    injectionFile = "%s/injection.xml"%(baseDir)

    mdc_create_xml_command = "%s -l %s -i %s -f %d"%(mdc_create_xml,listFile,injectionFile,flow)
    os.system(mdc_create_xml)    

    for i in xrange(numberFrames):

        frameCommand = "%s --verbose --cbc -s 0 -j %d -J 0 -t %d -l %d -r %d -f %d -R %s/"%(Mdc_AL,i,gpsStart,frameDur,fs,flow,baseDir)
        os.system(frameCommand)

    ifos = ["H1","H2","L1"]
    channels = {'H1':'H1:STRAIN','H2':'H2:STRAIN','L1':'L1:STRAIN'}
    for ifo in ifos:
        ifoDir = os.path.join(outputDir,ifo)
        if not os.path.isdir(ifoDir):
            os.mkdir(ifoDir)
        mvFrames = "mv %s/%s* %s"%(outputDir,ifo,ifoDir)
        os.system(mvFrames)
    
    rmInjectionFrameDir = "rm -r %s"%(injectionFrameDir)
    os.system(rmInjectionFrameDir)
    mvOutputDir = "mv %s %s"%(outputDir,injectionFrameDir)
    os.system(mvOutputDir) 

    for ifo in ifos:
        if ifo == "V1":
            data_source = "AdvVirgo"
        else:
            data_source = "AdvLIGO"

        #frameCommand = "gstlal_fake_aligo_frames --gps-start-time %d --gps-end-time %d --channel-name %s=FAKE-STRAIN --verbose --data-source AdvVirgo --frame-type FAKE --injections %s"%(gpsStart,gpsEnd,ifo,injectionFile)
        frameCommand = "gstlal_fake_aligo_frames --gps-start-time %d --gps-end-time %d --channel-name %s=FAKE-STRAIN --verbose --data-source %s --frame-type GAUSSIAN --output-path %s --duration %d --output-channel-name %s"%(gpsStart,gpsEnd,ifo,data_source,noiseFrameDir,frameDur,"STRAIN")
        os.system(frameCommand)

    frameDir = os.path.join(baseDir,"frames")
    if not os.path.isdir(frameDir):
        os.mkdir(frameDir)

    for ifo in ifos:
         
        ifoFrameDir = os.path.join(frameDir,ifo)
        if not os.path.isdir(ifoFrameDir):
            os.mkdir(ifoFrameDir)

        ifoNoiseFrameDir = os.path.join(noiseFrameDir,ifo)
        ifoInjectionFrameDir = os.path.join(injectionFrameDir,ifo)
 
        noiseCache = [os.path.join(root, name)
             for root, dirs, files in os.walk(ifoNoiseFrameDir)
             for name in files]
        injCache = [os.path.join(root, name)
             for root, dirs, files in os.walk(ifoInjectionFrameDir)
             for name in files]
 
        for injframe in injCache:

            injframeSplit = injframe.replace(".gwf","").split("-")
            gps = float(injframeSplit[-2])
            dur = float(injframeSplit[-1])

            noisetime, noisedata = read_frames(gps,gps+dur,channels[ifo],noiseCache)
            injtime, injdata = read_frames(gps,gps+dur,channels[ifo],[injframe])

            frametime = noisetime
            framedata = noisedata + injdata

            out_dict = {'name'  : channels[ifo],
                'data'  : framedata,
                'start' : frametime[0],
                'dx'    : frametime[1]-frametime[0],
                'kind'  : 'ADC'}

            frameName = "%s/%s-GAUSSIAN-%d-%d.gwf"%(ifoFrameDir,ifo[0],gps,dur)

            Fr.frputvect(frameName,[out_dict])

            print frameName, "completed"

