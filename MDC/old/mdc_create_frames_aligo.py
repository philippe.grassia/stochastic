#!/usr/bin/python

import os, sys, glob, optparse, warnings, datetime, time, matplotlib, math, random
import numpy as np

__author__ = "Michael Coughlin <michael.coughlin@ligo.org>"
__date__ = "2012/8/26"
__version__ = "0.1"

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser(usage=__doc__,version=__version__)

    parser.add_option("-s", "--gps_start_time",default=800000000,type=int)
    parser.add_option("-d", "--time_interval",default=1000,type=int)
    parser.add_option("-l", "--segment_duration",default=2048,type=int)
    parser.add_option("-n", "--number_of_segments",default=1,type=int)

    parser.add_option("-v", "--verbose", action="store_true", default=False,
                      help="Run verbosely. (Default: False)")

    opts, args = parser.parse_args()

    # show parameters
    if opts.verbose:
        print >> sys.stderr, ""
        print >> sys.stderr, "running network_eqmon..."
        print >> sys.stderr, "version: %s"%__version__
        print >> sys.stderr, ""
        print >> sys.stderr, "***************** PARAMETERS ********************"
        for o in opts.__dict__.items():
          print >> sys.stderr, o[0]+":"
          print >> sys.stderr, o[1]
        print >> sys.stderr, ""

    return opts

def rmode_struct(gps,length):

    rmode = {}
    rmode["gps"] = gps
    rmode["dist"] = random.uniform(1e-1, 1e-0)
    #rmode["ra"] = random.uniform(0, 2*np.pi)
    #rmode["dec"] = 0.5*math.pi - math.acos(random.uniform(-1.0,1.0))
    #rmode["psi"] = random.uniform(0, 2*np.pi)
    rmode["inc"] = math.acos(random.uniform(-1.0, 1.0))
    rmode["ra"] = 0
    rmode["dec"] = 0
    rmode["psi"] = 0
    rmode["inc"] = 0
    rmode["length"] = length
    rmode["model"] = 1
    
    return rmode

# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    warnings.filterwarnings("ignore")

    # Parse command line
    opts = parse_commandline()

    MDC_Generation = "/home/mcoughlin/matapps/packages/stochastic/trunk/MDC/MDC_Generation"

    frameDir = "/home/mcoughlin/Stochastic/MDC/aLIGO_Gaussian" 

    list2 = "%s/list2"%(MDC_Generation)
    Mdc_AL = "%s/Mdc_AL"%(MDC_Generation)

    gpsStart = 800000000
    numberFrames = 5
    frameDur = 2048
    fs = 2048
    flow = 10
   
    if not os.path.isdir("input"): 
        copyInput = "cp -r %s/input ."%(MDC_Generation)
        os.system(copyInput)
    if not os.path.isdir("lists"):
        os.mkdir("lists")

    list2Command = "%s -t %d -n %d -l %d -r %d -f %d"%(list2,gpsStart,numberFrames,frameDur,fs,flow)
    os.system(list2Command)

    if not os.path.isdir(frameDir):
        os.mkdir(frameDir)

    outputDir = os.path.join(frameDir,"output")
    if not os.path.isdir(outputDir):
        os.mkdir(outputDir)
    rmLists = "rm -r %s/lists"%(frameDir)
    os.system(rmLists)
    rmInput = "rm -r %s/input"%(frameDir)
    os.system(rmInput)

    mvLists = "mv lists %s"%(frameDir)
    os.system(mvLists)
    mvInput = "mv input %s"%(frameDir)
    os.system(mvInput)

    for i in xrange(numberFrames):

        frameCommand = "%s --verbose --cbc -s 0 -j %d -J 0 -t %d -l %d -r %d -f %d -R %s/"%(Mdc_AL,i,gpsStart,frameDur,fs,flow,frameDir)
        os.system(frameCommand)
        #print frameCommand

    ifos = ["H1","H2","L1"]
    for ifo in ifos:
        ifoDir = os.path.join(outputDir,ifo)
        if not os.path.isdir(ifoDir):
            os.mkdir(ifoDir)
        mvFrames = "mv %s/%s* %s"%(outputDir,ifo,ifoDir)
        os.system(mvFrames)

