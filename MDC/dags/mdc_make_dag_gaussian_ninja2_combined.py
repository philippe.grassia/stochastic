#!/usr/bin/python

import os, sys, glob, optparse, warnings, datetime, time, matplotlib, math, random
import numpy as np
from pylal import Fr

from glue import iterutils
from glue import pipeline
from glue import lal
from glue.ligolw import lsctables
from glue import segments
import glue.ligolw.utils as utils
import glue.ligolw.utils.segments as ligolw_segments

__author__ = "Michael Coughlin <michael.coughlin@ligo.org>"
__date__ = "2012/8/26"
__version__ = "0.1"

# =============================================================================
#
#                               DEFINITIONS
#
# =============================================================================

def parse_commandline():
    """
    Parse the options given on the command-line.
    """
    parser = optparse.OptionParser(usage=__doc__,version=__version__)

    parser.add_option("-s", "--gps_start_time",default=800000000,type=int)
    parser.add_option("-d", "--time_interval",default=1000,type=int)
    parser.add_option("-l", "--segment_duration",default=2048,type=int)
    parser.add_option("-n", "--number_of_segments",default=1,type=int)

    parser.add_option("-v", "--verbose", action="store_true", default=False,
                      help="Run verbosely. (Default: False)")

    opts, args = parser.parse_args()

    # show parameters
    if opts.verbose:
        print >> sys.stderr, ""
        print >> sys.stderr, "running network_eqmon..."
        print >> sys.stderr, "version: %s"%__version__
        print >> sys.stderr, ""
        print >> sys.stderr, "***************** PARAMETERS ********************"
        for o in opts.__dict__.items():
          print >> sys.stderr, o[0]+":"
          print >> sys.stderr, o[1]
        print >> sys.stderr, ""

    return opts


def breakupsegs(seglists, min_segment_length):
        for instrument, seglist in seglists.iteritems():
                newseglist = segments.segmentlist()
                for seg in seglist:
                        if abs(seg) > min_segment_length:
                                newseglist.append(segments.segment(seg))
                seglists[instrument] = newseglist

# =============================================================================
#
#                                    MAIN
#
# =============================================================================

if __name__=="__main__":

    warnings.filterwarnings("ignore")

    # Parse command line
    opts = parse_commandline()

    baseDir = "/home/mcoughlin/Stochastic/MDC/NINJA2/Gaussian_Combined" 
    mdc_combine_frames = "/home/mcoughlin/matapps/packages/stochastic/trunk/MDC/mdc_combine_frames_ninja2"
   
    gpsStart = 800000000
    frameDur = 2048
    numFrames = 1000
    gpsEnd = gpsStart + numFrames*frameDur

    if not os.path.isdir(baseDir):
        os.mkdir(baseDir)
    frameDir = os.path.join(baseDir,"frames")
    if not os.path.isdir(frameDir):
        os.mkdir(frameDir)
    condorDir = os.path.join(baseDir,"condor")
    if not os.path.isdir(condorDir):
        os.mkdir(condorDir)
    if not os.path.isdir(os.path.join(condorDir,"logs")):
        os.mkdir(os.path.join(condorDir,"logs"))

    ifos = ["H1","L1","V1"]

    jobs = {}

    indexesShift = {'H1':0,'H2':0,'L1':0,'V1':0}

    for ifo in ifos:

        if ifo == "H1":
            noiseDir = "/archive/frames/NINJA2/G1000176_EARLY_GAUSSIAN/LHO/H-H1_NINJA2_G1000176_EARLY_GAUSSIAN-90"
        elif ifo == "L1":
            noiseDir = "/archive/frames/NINJA2/G1000176_EARLY_GAUSSIAN/LLO/L-L1_NINJA2_G1000176_EARLY_GAUSSIAN-90"
        elif ifo == "V1":
            noiseDir = "/archive/frames/NINJA2/G1000176_EARLY_RESCALED_GAUSSIAN/Virgo/V-V1_NINJA2_G1000176_EARLY_RESCALED_GAUSSIAN-90"

        jobs[ifo] = []

        frames = [os.path.join(root, name)
            for root, dirs, files in os.walk(noiseDir)
            for name in files]
        frames.sort()

        for frame in frames:
            frameSplit = frame.split("-")
            gps = float(frameSplit[-2])
            dur = float(frameSplit[-1].replace(".gwf",""))
            
            jobs[ifo].append([gps,gps+dur,0,0])

        #jobs[ifo] = random.sample(jobs[ifo], len(jobs[ifo]))
        frameStart = 800000000
        for i in xrange(len(jobs[ifo])):
            index = (i+indexesShift[ifo]) % len(jobs[ifo])

            if i == 0:
                print jobs[ifo][index][1]

            if frameStart > gpsEnd: 
                jobs[ifo].pop()
                continue
 
            jobs[ifo][index][2] = frameStart     
            jobs[ifo][index][3] = frameStart + (jobs[ifo][index][1] - jobs[ifo][index][0])
            frameStart = frameStart + (jobs[ifo][index][1] - jobs[ifo][index][0])

    condorFile = os.path.join(condorDir,"condor.dag")
    bashFile = os.path.join(condorDir,"bash.sh")
    f = open(condorFile,"w")
    g = open(bashFile,"w")

    noiseChannels = {'H1':'H1:GAUSSIAN','L1':'L1:GAUSSIAN','V1':'V1:GAUSSIAN'}
    injChannels = {'H1':'H1:STRAIN','L1':'L1:STRAIN','V1':'V1:STRAIN'}
    outChannels = {'H1':'H1:STRAIN','L1':'L1:STRAIN','V1':'V1:STRAIN'}

    jobNumber = 0
    for ifo in ifos:

        for job in jobs[ifo]:

            if ifo == "H1":
                noiseDir = "/archive/frames/NINJA2/G1000176_EARLY_GAUSSIAN/LHO/H-H1_NINJA2_G1000176_EARLY_GAUSSIAN-90"
            elif ifo == "L1":
                noiseDir = "/archive/frames/NINJA2/G1000176_EARLY_GAUSSIAN/LLO/L-L1_NINJA2_G1000176_EARLY_GAUSSIAN-90"
            elif ifo == "V1":
                noiseDir = "/archive/frames/NINJA2/G1000176_EARLY_RESCALED_GAUSSIAN/Virgo/V-V1_NINJA2_G1000176_EARLY_RESCALED_GAUSSIAN-90"              
 
            injectionDir = "/home/mcoughlin/Stochastic/MDC/S5_Injection_aLIGO/condor/output/%s"%(ifo)
            #injectionDir = "none"

            noiseChannel = noiseChannels[ifo]
            injChannel = injChannels[ifo]
            outChannel = outChannels[ifo]

            outputDir = "%s/%s"%(frameDir,ifo)
            if not os.path.isdir(outputDir):
                os.mkdir(outputDir)
        
            jobNumber = jobNumber + 1
            f.write('JOB %d mdc_combine_frames.sub\n'%(jobNumber))
            f.write('RETRY %d 3\n'%(jobNumber))
            f.write('VARS %d jobNumber="%d" gpsStart="%d" gpsEnd="%d" gpsFrameStart="%d" gpsFrameEnd="%d" injectionDir="%s" outputDir="%s" ifo="%s" noiseDir="%s" noiseChannel="%s" injChannel="%s" outChannel="%s"\n'%(jobNumber,jobNumber,job[0],job[1],job[2],job[3],injectionDir,outputDir,ifo,noiseDir,noiseChannel,injChannel,outChannel))
            f.write('\n')

            frameName = "%s/%s-NOISEplusINJ-%d-%d.gwf"%(outputDir,ifo[0],job[2],job[3]-job[2])
            if os.path.isfile(frameName):
                continue

            g.write("%s --gpsStart %d --gpsEnd %d --gpsFrameStart %d --gpsFrameEnd %d --injectionDir %s --outputDir %s --ifo %s --noiseDir %s --noiseChannel %s --injChannel %s --outChannel %s\n"%(mdc_combine_frames,job[0],job[1],job[2],job[3],injectionDir,outputDir,ifo,noiseDir,noiseChannel,injChannel,outChannel))

    f.close()
    g.close()

    combineSubFile = os.path.join(condorDir,"mdc_combine_frames.sub")
    f = open(combineSubFile,"w")
    f.write('executable = %s\n'%(mdc_combine_frames))
    f.write('arguments = " --gpsStart $(gpsStart) --gpsEnd $(gpsEnd) --gpsFrameStart $(gpsFrameStart) --gpsFrameEnd $(gpsFrameEnd) --injectionDir $(injectionDir) --outputDir $(outputDir) --ifo $(ifo) --noiseDir $(noiseDir) --noiseChannel $(noiseChannel) --injChannel $(injChannel) --outChannel $(outChannel)"\n')
    f.write('request_memory = 128\n')
    f.write('getenv = True\n')
    f.write('log = logs/mdc_combine_frames.log\n')
    f.write('error = logs/$(jobNumber).err\n')
    f.write('output = logs/$(jobNumber).out\n')
    f.write('notification = never\n')
    f.write('queue 1\n')
    f.close()
