function orient = createorientation(azDeg, altDeg);

%CREATEORIENTATION -- Create orientation structure
%                     (converting angles from degrees to radians)
%
%SYNOPSIS
%  ORITENT - createorientation(AZDEG, ALTDEG) 
%
%INPUT
%   AZDEG - azimuth angle in degrees
%   ALTDEG - (optional) altitude angle in degrees
%
%OUTPUT
%   ORIENT - structure with fields:
%      AZ: azimuth in radians East (clockwise) of North
%      ALT: altitude (tilt) angle in radians above the local tangent plane
%  From the azimuth and altitude (in degrees).  If the second argument
%  is omitted, the altitude angle is set to zero.
%
%  This function simply converts the input angles from degrees into
%  radians (setting altDeg to zero if it's not included in the
%  argument list) and then packs them into a structure.
%
%AUTHOR
%   John Whelan <john.whelan@ligo.org>

%  $Id$

if (nargin == 1)
  altDeg = 0;
end
orient = struct('az',azDeg*pi/180,'alt',altDeg*pi/180);
