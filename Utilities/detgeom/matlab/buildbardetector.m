function detector = buildbardetector(loc, axis);

%BUILDBARDETECTOR -- Build Cartesian structure describing bar geometry
%
%  buildbardetector(loc, axis) builds a detector structure
%  describing the Cartesian location and response of an resonant bar
%  gravitational wave detector, given its location in geodetic
%  coordinates (in the structure loc) and the azimuth and altitude
%  angles of its long axis (in the structure axis).
%
%SYNOPSIS
% DETECTOR - buildbardetector(LOC, AXIS)
%
%INPUT
%   LOC - structure containing location information with the followig fields:
%      LAT: geodetic latitude (measured North from the Equator) in radians
%      LON: geodetic longitude (measured East from the Prime
%             Meridian) in radians
%      HEIGHT: elevation in meters above the WGS-84 reference ellipsoid 
%     [Such a structure can be created from the geographic coordinates (in
%     degrees) using the function CREATELOCATION]
%   AXIS - structure axis with the fields:
%      AZ: azimuth in radians East (clockwise) of North
%      ALT: altitude (tilt) angle in radians above the local tangent plane
%      [Such a structure can be created from the local angles (in degrees)
%      using the function CREATEORIENTATION]
%
%OUTPUT
%   DETECTOR - structure with the fields:
%      R: [3x1 double] %  position vector (in units of meters)
%                         in Earth-based Cartesian coordinates
%      D: [3x3 double] %  response tensor in Earth-based Cartesian coordinates
% 
%  The function calls GETCARTESIANPOSITION to determine the position
%  vector and GETBARRESPONSE to determine the response tensor and
%  then packs the results into a structure.
%
%  See also GETCARTESIANPOSITION, GETBARRESPONSE, CREATELOCATION,
%  CREATEORIENTATION
%
%AUTHOR
%   John Whelan <john.whelan@ligo.org>

% $Id$

r = getcartesianposition(loc);
d = getbarresponse(loc,axis);

detector = struct('r',r,'d',d);
