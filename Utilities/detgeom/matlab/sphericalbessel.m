function y = sphericalbessel(n,z)

%SPHERICALBESSEL -- Calculate spherical Bessel functions
%
%SYNOPSIS
%   Y = sphericalbessel(N,Z)
%
%INPUTS
%   N - order of sperical bessel function
%   Z - point of evaluation
%
%OUTPUT
%   Y - values of the spherical Bessel function j_n(z) (ordern n,
%       evaluated at z)
%
%  Any zero elements of z have the corresponding output elements set
%  to z.^n (Matlab interprets 0^0 as 1); the results for non-zero 
%  arguments are calculated by calling BESSELJ with a fractional
%  order and multiplying by the appropriate factor, i.e., sqrt(pi/(2z))
% 
%  See also BESSELJ
%
%AUTHORS
%   John Whelan <john.whelan@ligo.org>

%  $Id$

% default value for zero elements of z
y = z.^n;

% correct value for non-zero elements of z
i = find(z);
y(i) = ((0.5*pi./z(i)).^0.5).*besselj(n+.5, z(i)); % see page 437 Abramowitz

return;

