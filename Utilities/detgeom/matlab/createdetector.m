function detector = createdetector(r, d);

%CREATEDETECTOR -- Create Cartesian detector structure from position
%                    and response
%
%SYNOPSIS
%   DETECTOR = createdetector(R,D)
%
%INPUTS
%   R - Cartesian postion vector
%   D - response tensor
%
%OUTPUTS
%   DETECTOR - detector structure
%
%AUTHOR
%   John Whelan <john.whelan@ligo.org>

%  $Id$


detector = struct('r',r,'d',d);
