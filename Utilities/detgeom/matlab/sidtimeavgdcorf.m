function gamma = sidtimeavgdcorf(det1, det2, dec);

%SIDTIMEAVGDCORF -- Calculate DC point sorce overlap reduction function
%                   averaged over siderial time
%
%SYNOPSIS
%   GAMMA = sidtimeavgdcorf(DET1, DET2, DEC)
%
%INPUTS
%   DET1, DET2 - Detector responses to consider; Both detectors are
%      structures in the standard Cartesian detector geometry format.
%      This structure has the fields
%        R: [3x1 double] % position vector (in units of meters)
%          in Earth-based Cartesian coordinates
%        D: [3x3 double] % response tensor in Earth-based Cartesian coordinates
%   DEC - source declination of pointlike unpolarized gravitational waves
%
%OUTPUT
%   GAMMA - overlap reduction function averaged over sidereal time
%
%  Given geographical location information, detector geometry structures
%  can be built using the functions buildifodetector and
%  buildbardetector.
% 
%  Note that the calculation of this code is done independently of that
%  in ORFINTEGRAND, but analytically speaking, the former result
%  should be obtainable from the latter by averaging over right
%  ascension or hour angle.
%
%  See also BUILDIFODETECTOR, BUILDBARDETECTOR, CREATEDETECTOR% 
%
%AUTHOR
%   John Whelan <john.whealan@ligo.org>

%  $Id$


% inputs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check number of inputs

error(nargchk(3,3,nargin));

LAL_C_SI = 2.99792458e8; % m/s

% trace-free response tensors
d1 = det1.d - eye(3).*trace(det1.d)/3;
d2 = det2.d - eye(3).*trace(det2.d)/3;

% calculate c1, c2, c3 coeffs.
c1 = sum(sum(d1.*d2));
c2 = d1(3,:) * d2(:,3);
c3 = d1(3,3) * d2(3,3);

cosdelta2 = (cos(dec*pi/180)).^2 ;
cosdelta4 = cosdelta2.^2;

zeta1 =   1.0  -       cosdelta2 + 0.125  * cosdelta4;
zeta2 = - 2.0  + 4.0 * cosdelta2 - 1.25   * cosdelta4;
zeta3 =   0.5  - 2.5 * cosdelta2 + 2.1875 * cosdelta4;

gamma = 5 * (c1*zeta1 + c2*zeta2 + c3*zeta3);

return;
