function detector = buildifodetector(loc, xarm, yarm);

%BUILDIFODETECTOR -- Build Cartesian structure describing IFO geometry
%
%  buildifodetector(loc, xarm, yarm) builds a detector structure
%  describing the Cartesian location and response of an interferometric
%  gravitational wave detector, given its location in geodetic
%  coordinates (in the structure loc) and the  azimuth and altitude
%  angles of its arms (in the structures xarm and yarm).
%
%SYNOPSIS
%   DETECTOR = buildifodetector(LOC, XARM, YARM)
%
%INPUT
%   LOC - structure containing location information with the followig fields:
%      LAT: geodetic latitude (measured North from the Equator) in radians
%      LON: geodetic longitude (measured East from the Prime
%             Meridian) in radians
%      HEIGHT: elevation in meters above the WGS-84 reference ellipsoid
%     [Such a structure can be created from the geographic coordinates (in
%     degrees) using the function CREATELOCATION]
%   XARM, YARM - arm directions structures with the fields:
%      AZ: azimuth in radians East (clockwise) of North
%      ALT: altitude (tilt) angle in radians above the local tangent plane
%      [Such a structure can be created from the local angles (in degrees)
%      using the function CREATEORIENTATION]
%
%OUTPUT
%   DETECTOR - structure with the fields:
%      R: [3x1 double] %  position vector (in units of meters)
%                         in Earth-based Cartesian coordinates
%      D: [3x3 double] %  response tensor in Earth-based Cartesian coordinates
%
%  The function calls GETCARTESIANPOSITION to determine the position
%  vector and GETIFORESPONSE to determine the response tensor and
%  then packs the results into a structure.
%
%  See also GETCARTESIANPOSITION, GETIFORESPONSE, CREATELOCATION,
%  CREATEORIENTATION
%
%AUTHOR
%   John Whelan <john.whelan@ligo.org>

% $Id$

r = getcartesianposition(loc);
d = getiforesponse(loc,xarm,yarm);

detector = struct('r',r,'d',d);
