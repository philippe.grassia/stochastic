function d = getBarResponse(loc,axis);
%  $Id$
%
%  getBarResponse(loc,axis) calculates the Cartesian response tensor
%  associated with a resonant bar gravitational wave detector at a
%  location loc with arms whose long axis has an orientation described \
%  by the structures axis.
%
%  The input location is a structure loc with fields
%        lat: geodetic latitude (measured North from the Equator) in radians
%        lon: geodetic longitude (measured East from the Prime
%            Meridian) in radians
%     height: elevation in meters above the WGS-84 reference ellipsoid 
%  Such a structure can be created from the geographic coordinates (in
%  degrees) using the function createLocation()
%
%  The input axis direction is a structure axis with the fields
%        az: azimuth in radians East (clockwise) of North
%       alt: altitude (tilt) angle in radians above the local tangent plane
%  Such a structure can be created from the local angles (in degrees)
%  using the function createOrientation()
% 
%  The function calls getCartesianDirection() to convert the
%  orientation angles for the axis into a Cartesian unit vector
%  pointing along the axis, then constructs the response tensor as
%  the outer product of this unit vector with itself.
%
%  Routine written by John T. Whelan.
%  Conact jtwhelan@loyno.edu
% 
%  See also GETCARTESIANDIRECTION
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

u = getCartesianDirection(axis,loc);

d = u * transpose(u);
