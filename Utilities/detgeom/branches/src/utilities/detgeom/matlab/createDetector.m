function detector = createDetector(r, d);
% Builds a detector structure out of the Cartesian position
% vector r and response tensor d
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

detector = struct('r',r,'d',d);
