<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html lang="en-us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>detgeom</title>
</head>
  <body>

    <h1>How to use the <code>detgeom</code> package</h1>

    <h2>Overview</h2>

    <p>
      The <code>detgeom</code> package contains matlab routines which
      define and manipulate the geometry of Earth-based gravitational
      wave (GW) detectors.  The functions take as arguments
      human-readable quantities such as locations specified by
      latitude and longitude in decimal degrees and height in meters
      above a reference ellipsoid (the WGS-84 Earth model), or
      orientations specified by azimuth (decimal degrees clockwise of
      local North) and an optional altitude (tilt) angle in decimal
      degrees.  Major existing earth-based GW detectors are
      pre-defined.  Internally, the geometry is manipulated via
      geographical structures which contain angles (latitude,
      longitude, azimuth, etc) in radians, as well as geometrical
      structures which describe the detector via a position vector and
      response tensor in Cartesian co&ouml;rdinates.
    </p>

    <p>
      The routines were designed for use in stochastic analysis, and a
      routine is included to calculate the overlap reduction function
      from a pair of suitable detector structures, but they could also
      be useful for other searches.  The detector response tensors
      included here can be contracted with polarization tensors to
      describe the response of a given detector to a given GW signal,
    </p>

    <h2>Examples</h2>

    <h3>To produce a semilog plot of the overlap reduction function
      between LIGO Hanford and GEO-600</h3>

    <h4><tt><a href="definedetectors.html">definedetectors</a></tt> method</h4>
    
<pre>
<a href="definedetectors.html">definedetectors</a>;
flog=10.^((0:400)./100);
gamma=<a href="overlapreductionfunction.html">overlapreductionfunction</a>(flog,detector_LHO.det,detector_GEO600.det);
semilogx(flog,gamma);
</pre>

    <h4><tt><a href="getdetector.html">getdetector</a></tt> method</h4>

<pre>
flog=10.^((0:400)./100);
gamma=<a href="overlapreductionfunction.html">overlapreductionfunction</a>(flog,<a href="getdetector.html">getdetector</a>('H'),<a href="getdetector.html">getdetector</a>('G'));
semilogx(flog,gamma);
</pre>

    <h3>To produce a plot of the overlap reduction function between
      Virgo and AURIGA (standard IGEC orientation)</h3>

    <h4><tt><a href="definedetectors.html">definedetectors</a></tt> method</h4>

<pre>
<a href="definedetectors.html">definedetectors</a>;
f=(0:2000);
gamma=<a href="overlapreductionfunction.html">overlapreductionfunction</a>(f,detector_VIRGO.det,detector_AURIGA.IGECdet);
plot(f,gamma);
</pre>

    <h4><tt><a href="getdetector.html">getdetector</a></tt> method</h4>

<pre>
f=(0:2000);
gamma=<a href="overlapreductionfunction.html">overlapreductionfunction</a>(f,<a href="getdetector.html">getdetector</a>('VIRGO'),<a href="getdetector.html">getdetector</a>('AURIGA'));
plot(f,gamma);
</pre>

    <h3>To calculate the overlap reduction function for LIGO
      Livingston with ALLEGRO (oriented due East)</h3>

    <h4><tt><a href="definedetectors.html">definedetectors</a></tt> method</h4>

<pre>
<a href="definedetectors.html">definedetectors</a>;
f=907+(-125:124);
eastAxis = <a href="createorientation.html">createorientation</a>(90);
ALLEGRO_east = <a href="buildbardetector.html">buildbardetector</a>(detector_ALLEGRO.loc,eastAxis);
gamma=<a href="overlapreductionfunction.html">overlapreductionfunction</a>(f,detector_LLO.det,ALLEGRO_east);
</pre>

    <h4><tt><a href="getdetector.html">getdetector</a></tt> method</h4>

<pre>
f=907+(-125:124);
gamma=<a href="overlapreductionfunction.html">overlapreductionfunction</a>(f,<a href="getdetector.html">getdetector</a>('LLO'),<a href="getdetector.html">getdetector</a>('ALLEGRO',90));
</pre>

    <h2>Routines</h2>

    <p>
      The package consists of a number of functions which manipulate
      detector geometry structures, and one script (<a
      href="definedetectors.html">definedetectors</a>) which populates
      geometry structures corresponding to the five large-scale
      ground-based interferometers and the five IGEC (<a
      href="http://igec.lnl.infn.it/">International Gravitational
      Event Collaboration</a>) resonant bar detectors.
    </p>

    <p>
      The functions are as follows.
    </p>

    <table border="1">
      <thead>
	<tr>
	  <th>Function</th>
	  <th>Inputs</th>
	  <th>Outputs</th>
	  <th>Calls</th>
	  <th>Synopsis</th>
	</tr>
      </thead>
      <tbody>
	<tr>
	  <td>
	    <a
	    href="overlapreductionfunction.html">overlapreductionfunction</a>
	  </td>
	  <td>
	    f&nbsp;{vector&nbsp;of&nbsp;Hertz}<br>
	    det1&nbsp;{<a href="#detector">detector</a>}<br>
	    det2&nbsp;{<a href="#detector">detector</a>}
	  </td>
	  <td>
	    gamma&nbsp;{vector&nbsp;of&nbsp;dimensionless}
	  </td>
	  <td>
	    <a
	    href="sphericalbessel.html">sphericalbessel</a>
	  </td>	  
	  <td>
	    Calculate overlap reduction function between two detectors
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="sphericalbessel.html">sphericalbessel</a>
	  </td>
	  <td>
	    n&nbsp;{dimensionless}<br>
	    z&nbsp;{vector&nbsp;of&nbsp;dimensionless}
	  </td>
	  <td>
	    y&nbsp;{vector&nbsp;of&nbsp;dimensionless}
	  </td>
	  <td></td>
	  <td>
	    Calculate spherical Bessel functions
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="getdetector.html">getdetector</a>
	  </td>
	  <td>
	    site&nbsp;{keyword&nbsp;or&nbsp;character}<br>
	    azDeg&nbsp;{degrees}
	  </td>
	  <td>
	    detector&nbsp;{<a href="#detector">detector</a>}
	  </td>
	  <td>
	    <a
	    href="getsitefromletter.html">getsitefromletter</a><br>
	    <a
	    href="createlocation.html">createlocation</a><br>
	    <a
	    href="createorientation.html">createorientation</a><br>
	    <a
	    href="buildifodetector.html">buildifodetector</a><br>
	    <a
	    href="buildbardetector.html">buildbardetector</a>
	  </td>
	  <td>
	    Construct detector geometry structure for major detector
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="getsitefromletter.html">getsitefromletter</a>
	  </td>
	  <td>
	    site&nbsp;{character}
	  </td>
	  <td>
	    site&nbsp;{keyword}
	  </td>
	  <td>
	  </td>
	  <td>
	    Convert one-letter abbreviation to standard keyword for
	    standard detector.
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="buildifodetector.html">buildifodetector</a>
	  </td>
	  <td>
	    loc&nbsp;{<a href="#location">location</a>}<br>
	    xarm&nbsp;{<a href="#orientation">orientation</a>}<br>
	    yarm&nbsp;{<a href="#orientation">orientation</a>}
	  </td>
	  <td>
	    detector&nbsp;{<a href="#detector">detector</a>}
	  </td>
	  <td>
	    <a
	    href="getcartesianposition.html">getcartesianposition</a><br>
	    <a
	    href="getiforesponse.html">getiforesponse</a>
	  </td>
	  <td>
	    Build Cartesian structure describing IFO geometry
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="buildbardetector.html">buildbardetector</a>
	  </td>
	  <td>
	    loc&nbsp;{<a href="#location">location</a>}<br>
	    axis&nbsp;{<a href="#orientation">orientation</a>}
	  </td>
	  <td>
	    detector&nbsp;{<a href="#detector">detector</a>}
	  </td>
	  <td>
	    <a
	    href="getcartesianposition.html">getcartesianposition</a><br>
	    <a
	    href="getbarresponse.html">getbarresponse</a>
	  </td>
	  <td>
	    Build Cartesian structure describing bar geometry
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="getcartesianposition.html">getcartesianposition</a>
	  </td>
	  <td>
	    loc&nbsp;{<a href="#location">location</a>}
	  </td>
	  <td>
	    r&nbsp;{[3x1] meters}
	  </td>
	  <td>
	  </td>
	  <td>
	    Construct Cartesian position vector from geographic coordinates
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="getiforesponse.html">getiforesponse</a>
	  </td>
	  <td>
	    loc&nbsp;{<a href="#location">location</a>}<br>
	    xarm&nbsp;{<a href="#orientation">orientation</a>}<br>
	    yarm&nbsp;{<a href="#orientation">orientation</a>}
	  </td>
	  <td>
	    d&nbsp;{[3x3] dimensionless}
	  </td>
	  <td>
	    <a
	    href="getcartesiandirection.html">getcartesiandirection</a>
	  </td>
	  <td>
	    Construct Cartesian response tensor from geographic
	    coordinates & IFO arm orientations
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="getbarresponse.html">getbarresponse</a>
	  </td>
	  <td>
	    loc&nbsp;{<a href="#location">location</a>}<br>
	    axis&nbsp;{<a href="#orientation">orientation</a>}
	  </td>
	  <td>
	    d&nbsp;{[3x3] dimensionless}
	  </td>
	  <td>
	    <a
	    href="getcartesiandirection.html">getcartesiandirection</a>
	  </td>
	  <td>
	    Construct Cartesian response tensor from geographic
	    coordinates & bar orientation
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="getcartesiandirection.html">getcartesiandirection</a>
	  </td>
	  <td>
	    ori&nbsp;{<a href="#orientation">orientation</a>}<br>
	    loc&nbsp;{<a href="#location">location</a>}
	  </td>
	  <td>
	    u&nbsp;{[3x1] dimensionless}
	  </td>
	  <td>
	  </td>
	  <td>
	    Construct Cartesian unit vector from Earth-based orientation
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="createdetector.html">createdetector</a>
	  </td>
	  <td>
	    r&nbsp;{[3x1]&nbsp;meters}<br>
	    d&nbsp;{[3x3]&nbsp;dimensionless}
	  </td>
	  <td>
	    detector&nbsp;{<a href="#detector">detector</a>}
	  </td>
	  <td>
	  </td>
	  <td>
	    Create Cartesian detector structure from position and response
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="createlocation.html">createlocation</a>
	  </td>
	  <td>
	    latDeg&nbsp;{degrees}<br>
	    lonDeg&nbsp;{degrees}<br>
	    h&nbsp;{meters}
	  </td>
	  <td>
	    loc&nbsp;{<a href="#location">location</a>}
	  </td>
	  <td>
	  </td>
	  <td>
	    Create geodetic location structure (converting angles from
	    degrees to radians)
	  </td>
	</tr>
	<tr>
	  <td>
	    <a
	    href="createorientation.html">createorientation</a>
	  </td>
	  <td>
	    azDeg&nbsp;{degrees}<br>
	    altDeg&nbsp;{degrees}
	  </td>
	  <td>
	    orient&nbsp;{<a href="#orientation">orientation</a>}
	  </td>
	  <td>
	  </td>
	  <td>
	    Create orientation structure (converting angles from
	    degrees to radians)
	  </td>
	</tr>
      </tbody>
    </table>

    <h2>Structures</h2>

    <p>
      The following structures are used by the routines in this
      package and passed between them as inputs and outputs.
    </p>

    <h3 id="detector">detector</h3>

    <h4>Fields</h4>

<pre>
    r: [3x1 double{meters}]
    d: [3x3 double{dimensionless}]
</pre>

    <h4>Synopsis</h4>

    <p>
      The position vector and response tensor of a GW detector, in an
      Earth-fixed right-handed Cartesian co&ouml;rdinate system.  The
      origin is at the center of the Earth, the z axis passes through
      the North Pole, and the x axis passes through the intersection
      of the Equator and the Prime Meridian.  The response tensor can
      be contracted with a metric perturbation to give the GW strain
      measured in that detector.
    </p>

    <h3 id="location">location</h3>

    <h4>Fields</h4>

<pre>
       lat: {floating pt radians}
       lon: {floating pt radians}
    height: {floating pt meters}
</pre>

    <h4>Synopsis</h4>

    <p>
      A geographical location on the surface of the earth, specified
      by latitude, longitude, and height above the WGS-84 reference
      ellipsoid.  (See LIGO Technical Document <a
      href="http://www.ligo.caltech.edu/docs/T/T980044-10.pdf">T980044</a>
      for details on the ellipsoidal co&ouml;rdinate system used.)
    </p>

    <h3 id="orientation">orientation</h3>

    <h4>Fields</h4>

<pre>
       az: {floating pt radians}
      alt: {floating pt radians}
</pre>

    <h4>Synopsis</h4>

    <p>
      A direction relative to the tangent plane to the WGS-84
      reference ellipsoid (see LIGO Technical Document <a
      href="http://www.ligo.caltech.edu/docs/T/T980044-10.pdf">T980044</a>
      for details), specified by an azimuth angle measured
      <strong>clockwise</strong> from North, and an altitude (tilt)
      angle above the horizon.
    </p>

    <h3 id="bardetector">bardetector</h3>

    <h4>Fields</h4>

<pre>
         loc: [1x1 struct{<a href="#location">location</a>}]
    IGECaxis: [1x1 struct{<a href="#orientation">orientation</a>}]
     IGECdet: [1x1 struct{<a href="#detector">detector</a>}]
</pre>

    <h4>Synopsis</h4>

    <p>
      The observing geometry for a resonant bar GW detector.  The
      <code>loc</code> field specifies its position on the Earth; the
      <code>IGECaxis</code> field specifies the orientation of its
      long axis when nearly parallel to the other IGEC bars; the
      <code>IGECdet</code> field gives the Cartesian detector geometry
      in this orientation.
    </p>

    <h3 id="ifodetector">ifodetector</h3>

    <h4>Fields</h4>

<pre>
     loc: [1x1 struct{<a href="#location">location</a>}]
    xarm: [1x1 struct{<a href="#orientation">orientation</a>}]
    yarm: [1x1 struct{<a href="#orientation">orientation</a>}]
     det: [1x1 struct{<a href="#detector">detector</a>}]
</pre>

    <h4>Synopsis</h4>

    <p>
      The observing geometry for an interferometric GW detector.  The
      <code>loc</code> field specifies its position on the Earth; the
      <code>xarm</code> and <code>yarm</code> fields specify the
      orientations of its arms; the <code>det</code> field gives the
      Cartesian detector geometry.
    </p>

    <hr title="footer follows">
    
      <address>
	<a href="http://www.loyno.edu/~jtwhelan/">John T. Whelan</a> 
	/ <a rev="made" href="mailto:jtwhelan@loyno.edu">jtwhelan@loyno.edu</a>
      </address>
      <p>
	<a class="external" title="HTML validation of this page"
	  href="http://validator.w3.org/check/referer"><img
	    src="http://validator.w3.org/images/vh40.gif"
	    alt="HTML 4.0 compliant"></a>
      </p>
    
  </body>
</html>
