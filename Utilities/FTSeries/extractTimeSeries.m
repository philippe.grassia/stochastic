function [data, tlow, deltaT, fbase, phase] = extractTimeSeries(x)
%
%  extractTimeSeries --- Extract time-series data from structure
%
%  extractTimeSeries(x) returns the data and metadata associated 
%  with a time series data structure.
%
%  Outputs:
%
%  data   =  array of time-series values
%  tlow   =  start time in seconds (usually GPS time)
%  deltaT =  sampling time in seconds (i.e., 1/the sampling frequency)
%  fbase  =  the base frequency (in Hz) used in heterodyning the data
%  phase  =  initial phase (in radians) of the complex exponential
%            used in heterodyning
%
%  If the time-series fbase and/or phase metadata are missing, they
%  are set to NaN.
%
%  Routine written by Joseph D. Romano and John T. Whelan.
%  Contact Joseph.Romano@astro.cf.ac.uk and/or john.whelan@ligo.org
%
%  $Id$
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data = x.data;
tlow = x.tlow;
deltaT = x.deltaT;

try
  fbase = x.fbase;
catch
  fbase = NaN;

try
  phase = x.phase;
catch
  phase = NaN;

return
