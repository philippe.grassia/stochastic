function t = gettimes(timeSeries)
% GETTIMES -- Generate time list for easy plotting
%                    of time series
% 
% gettimes(timeSeries) returns a vector of timeuencies
% so that a time series structure may easily be
% plotted, e.g. with plot(gettimes(timeSeries),timeSeries.data)
%
% Routine written by John T. Whelan.
% Contact john.whelan@ligo.org
%
% $Id$
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = timeSeries.tlow + transpose(0:(length(timeSeries.data)-1)) ...
                      * timeSeries.deltaT;
return;
