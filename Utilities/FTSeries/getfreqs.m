function f = getfreqs(freqSeries)
% GETFREQS -- Generate frequency list for easy plotting
%                    of frequency series
% 
% getfreqs(freqSeries) returns a vector of frequencies
% so that a frequency series structure may easily be
% plotted, e.g. with plot(getfreqs(freqSeries),freqSeries.data)
%
f = freqSeries.flow + transpose(0:(length(freqSeries.data)-1)) ...
                      * freqSeries.deltaF;
return;
