function [gzipFlag,unzipFile,tempDir] = gzipcheck(fileName,outDir)

%GZIPCHECK - checks for, expands GZIP archive of file
%
%SYNOPSIS
%   [GZIPFLAG,UNZIPFLAG,TEMPDIR] = gzipcheck(FILENAME[,OUTDIR])
%
%INPUTS
%    FILENAME - path name of original file
%    OUTDIR - [OPTIONAL] directory to expand GZIP to 
%                        <default = temporary directory>
%
%OUTPUTS
%    GZIPFLAG - true - found,expanded GZIP archive
%               false - no GZIP archive found
%    UNZIPFLAG - path name to un-zipped file
%    TEMPDIR - if not-empty, path to temporary directory
%
%   - assumes GZIP archive has .gz appended to existing file name
%
%     ** requires 'gunzipfile'
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id$

% IF output directory was not input
%   SET output directory to default
% ENDIF
% IF file exists
%   IF it is a gzip archive (.gz extensions)
%       SET gzip flag
%   ELSE
%       CLEAR gzip flag
%   ENDIF
% ELSE
%   ADD gzip extension to file name
%   IF file with gzip extension exists
%       SET gzip flag
%   ELSE
%       CLEAR gzip flag
%   ENDIF
% ENDIF
error(nargchk(1,2,nargin),'struct');
gzipFlag = false;
unzipFile = [];
tempDir = [];
if(exist(fileName,'file')==2)
    if(strcmp(fileName(end-2:end),'.gz')==true)
        gzFile = fileName;
        gzipFlag = true;
    else
        unzipFile = fileName;
        gzipFlag = false;
    end
else
    gzFile = sprintf('%s.gz',fileName);
    if(exist(gzFile,'file') == 2)
        gzipFlag = true;
    else
        unzipFile = fileName;
        gzipFlag = false;
    end
end

% IF gzip flag is set
%   IF output directory was input
%       SET output directory from input
%   ELSE
%       GET temporary directory
%   ENDIF
%   IF output directory is not the input directory
%       IF output directory does not exist
%           CREATE output directory
%       ENDIF
%       COPY GZIP file to output directory
%       UNZIP file destroying the 'copy' of the original
%   ELSE
%       UNZIP file without destroying the original
%   ENDIF
%   SET path to un-zipped file
% ENDIF
if(gzipFlag == true)
    [oldDir,inPre,inExt] = fileparts(gzFile);
    [tmpDir,outPre,outExt] = fileparts(gzFile(1:end-3));
    if(nargin > 1)
        outDir = char(outDir);
        tempDir = [];
    else
%
%  we will use MATLAB function to generate a temporary directory
%
        tempDir = tempname;
        outDir = tempDir;
    end
    if(strcmp(outDir,oldDir) == false)
        if(~isdir(outDir))
%D            fprintf('GZIPCHECK: Attempt to create directory %s\n',outDir);
            [successCode,failMsg,failMsgId] = mkdir(outDir);
            if(successCode ~= 1)
                msgId = 'gzipcheck:dirFail';
                error(msgId,'%s Creation of directory %s failed with %s %s',...
                    msgId,outDir,failMsgId,failMsg);
            end
        end
        copyCmd = sprintf('/bin/sh -c "cp %s %s"',gzFile,outDir);
        [unixError,unixOut] = unix(copyCmd);
        if(unixError ~= 0)
            msgId = 'gzipcheck:copyFail';
            error(msgId,'%s copy of %s to %s failed with error %d',...
                msgId,gzFile,outDir,unixError);
        end
        zipFile = fullfile(outDir,strcat(inPre,inExt));
        replaceGzFlag = true;
    else
        zipFile = gzFile;
        replaceGzFlag = false;
    end
    outFile = gunzipfile(zipFile,replaceGzFlag);
    unzipFile = fullfile(outDir,strcat(outPre,outExt));
end
return
