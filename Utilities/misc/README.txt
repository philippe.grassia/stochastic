Potentially obsolete functions:

  createTriples.m :
     Function to create a structure of triple coinc. events but only handles L1, H1 and H2 as the 3 detectors.  FYI: this function as never be edited for fuctionality since its inital commit in Feb. 2005.
   
   structcats.m :
      This function seems to be obsolete compared to structcat.m.

   structex.m :
      This function seems to be obsolete compared to structcat.m.