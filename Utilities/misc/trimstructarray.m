function trimStruct = trimstructarray(strArray,idxList)

% TRIMSTRUCTARRAY - reduce structure of arrays using index list
%
%SYNOPSIS
%   TRIMSTRUCT = trimstructarray(STRARRAY,IDXLIST)
%
%INPUTS
%   STRARRAY - a structure of arrays (where the array for
%              each field is the same length)
%   IDXLIST - list of indexes to array elements that we keep
%
%OUTPUT
%   TRIMSTRUCT - same structure as input, but with the array for 
%                each field in the structure reduced to indices in the list
%
%NOTS
%  A Structure of Arrays is much more memory efficient than
%  an Array of Structures.  We can use a structure of arrays
%  like an array of structure if we keep each array ( corresponding
%  to a field) the same length.
%
%  This function acts like  normalStruct(idxList)
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id$

% GET fields in structure of arrays
% CREATE empty output structure
% CHECK size of index list against structure arrays
% ALIGN rows,cols of index to those in the structure arrays
% LOOP over fields
%   GET array for field from original structure
%   SET field in output structure = array elements with indices
%           in the list
% ENDLOOP
fieldList = fieldnames(strArray);
numField = numel(fieldList);
dumCell = cell(numField,1);
trimStruct = cell2struct(dumCell,fieldList,1);
testArray = strArray.(char(fieldList(1)));
[nRow,nCol] = size(testArray);
[idxRow,idxCol] = size(idxList);
if(nCol > nRow)
    if(idxRow > idxCol)
        idxList = idxList';
    end
else
    if(idxCol > idxRow)
        idxList = idxList';
    end
end
for k=1:numField
    thisField = char(fieldList(k));
    origArray = strArray.(thisField);
    trimStruct.(thisField) = origArray(idxList);
end
return
