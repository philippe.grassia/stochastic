function finalData = addstructarray(origData,newData)

%ADDSTRUCTARRAY combines two structure arrays
%
%SYNOPSIS
%   FINALDATA = addstructarray(ORIGDATA, NEWDATA)
%
%INPUT
%   ORIGDATA - original structure array of data
%   NEWDATA - structure array of new data
%
%OUTPUT
%   FINALDATA - structure array with new data added to 
%                   fields of original data
%
%AUTHOR
%   Keith Throne <keith.thorne@ligo.org>

%  $Id$

error(nargchk(2,2,nargin),'struct');
finalData = [];
fieldList = fieldnames(origData);
numField = numel(fieldList);
colwiseFlag = [];
for k=1:numField
    fld = char(fieldList{k});
    sub1 = origData.(fld);
    sub2 = newData.(fld);
%
%  - if field is a character, make it a cell
    if(ischar(sub1))
        sub1 = cellstr(sub1);
    end
    if(ischar(sub2))
        sub2 = cellstr(sub2);
    end
%
%  check whether row or column-wise  default is row-wise (N Rows x 1 col)
%
    if(isempty(colwiseFlag))
        [nRow,nCol] = size(sub1);
        if(nCol > nRow)
            colwiseFlag = true;
        else
            colwiseFlag = false;
        end
    end
    if(colwiseFlag == true)
        finalData.(fld) = [(sub1) (sub2)];        
    else
        finalData.(fld) = [(sub1);(sub2)];
    end
end
return
