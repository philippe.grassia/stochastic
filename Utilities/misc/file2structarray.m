function [numRows,fileData,numCol,colHead] = file2structarray(colFile,...
    numCol,colHead)

%FILE2STRUCTARRAY - read data file by columns into structure of arrays
%
%SYNOPSIS 
%   [NUMROWS,FILEDATA,NUMCOL,COLHEAD] = file2structarray(COLFILE...
%                                           [,NUMCOL,COLHEAD])
%
%INPUTS
%   COLFILE - data file arranged in columns
%   NUMCOL - [OPTIONAL] # of columns/fields
%   COLHEAD - [OPTIONAL] string with field names (space-delimited)
%
%OUTPUTS
%   NUMROWS - # of rows of data
%   FILEDATA - structure where each field is a vector numRows in 
%           length of data for that field
%   NUMCOL - # of columns/fields
%   COLHEAD - string with field names (space-delimited)
%
%   - needs getcommentstyle,addstructarray,rdcolumnhead
%
%   - see related utility 'trimstructarray'
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id$

% IF file does not exist
%   SET error condition
%   EXIT
% ENDIF
% GET type of comment character
% OPEN file
% CREATE format for reading in data
numRows = 0;
fileData = [];
error(nargchk(1,3,nargin),'struct');
colFile = char(colFile);
if(exist(colFile,'file') < 2)
    msgId = 'file2structarray:badFile';
    warning(msgId,'%s: file %s not found\n',msgId,colFile);
    return
end
if(nargin > 2)
    numCol = strassign(numCol);
    if(isempty(numCol) || numCol < 1)
        msgId = 'file2structarray:badCol';
        warning(msgId,'%s: bad # of columns',msgId);
        return
    end
    colHead = strassign(colHead);
    if( (isempty(colHead)) || (numel(colHead)~=numCol))
        msgId = 'file2structarray:badCol';
        warning(msgId,'%s: column headings bad',msgId);
        return
    end
else
    [numCol,colHead] = rdcolumnhead(colFile);
end
[comStyle,comChar] = getcommentstyle(colFile);
fid = fopen(colFile,'r');
fmtStr = [];
for k=1:numCol
    fmtStr = [fmtStr '%s '];
end

% SET # of records per read-in
% WHILE # of records records = requested #
%   READ in a chunk of records
%   LOOP over columns
%       GET vector of data from column
%       IF data is numeric
%           CONVERT to numbers
%           ADD vector as field to structure array
%       ELSE
%           ADD cell-array of strings as field in array
%       ENDIF
%   END LOOP
%   IF output structure empty
%       SET output structure to data array
%   ELSE
%       APPEND new data to fields in existing structure
%   ENDIF
% END WHILE  
% CLOSE file
MAX_REC = 30000;
numRead = MAX_REC;
setNo = 0;
while (numRead == MAX_REC)
    setData = [];
    setNo = setNo + 1;
%D    fprintf('FILE2STRUCTARRAY - Read in set %d of %d records\n',...
%D        setNo,MAX_REC);
    colData = textscan(fid,fmtStr,MAX_REC,...
        'commentStyle',comChar,'returnOnError',0,...
        'emptyValue',0);
    for iCol = 1:numCol
        charColumn = false;
        colName = char(colHead(iCol));    
        vect = colData(:,iCol);
        vectData = vect{1,1};
        numRow = numel(vectData);
        if(numRow > 0)
            testDat = char(vectData{1,1});
            %D  fprintf(' Col %d data %s\n',iCol,char(vectData{1,1}));
            if(isempty(testDat))
                numRow = numel(vectData);
                vectData = zeros(numRow,1);
            else
                testNum = str2num(char(vectData{1,1}));
                if(~isempty(testNum))
                    vectData = str2num(char(vectData));
                end
            end    
        end
        fld = char(colHead(iCol));
%
% We can't have dashes in field names, so change any to underlines
        dashPos = strfind(fld,'-');
        numDash = numel(dashPos);
        if(numDash > 0)
            for k=1:numDash
                fld(dashPos(k):dashPos(k)) = '_';
            end
        end
%        
        setData = setfield(setData,fld,vectData);
    end
    testField = char(colHead(1));
    testVect = getfield(setData,testField);
    numRead = numel(testVect);
%D    fprintf('FILE2STRUCTARRAY - Actually read in %d rows\n',numRead);
    if(isempty(fileData))
        fileData = setData;
    else
        fileData = addstructarray(fileData,setData);
    end
    numRows = numRead + numRows;
end
fclose(fid);

return
