function [outPath,unixError] = gunzipfile(zipFilePath,replaceGzFlag)

%GUNZIPFILE - runs gunzip on a single file
%
%SYNOPSIS
%   [OUTFILE,UNIXERROR] = gunzipfile(ZIPFILEPATH,REPLACEGZFLAG)
%
%INPUTS
%   ZIPFILEPATH - path name of original file
%   REPLACEGZFLAG - [OPTIONAL] true if extract replaces GZIP file
%                              false if keeping GZIP file
%                              <default = false>
%
%OUTPUTS
%   OUTPATH - path name to un-zipped file
%   UNIXERROR - UNIX error code on 'gunzip'
%
%   - assumes GZIP archive has .gz appended to file name
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id$

% IF file does not exist
%   CREATE error message
%   RETURN
% ENDIF
% RUN GUNZIP on file
% REMOVE gzip extension to output file name
outPath = [];
error(nargchk(1,2,nargin),'struct');
zipFilePath = char(zipFilePath);
if(exist(zipFilePath,'file')~=2)
    msgId = 'gunzipfile:noFile';
    error(msgId,'%s file %s not found',msgId,zipFilePath);
end
DEF_FLAG = false;
if(nargin > 1)
    if(~isempty(replaceGzFlag))
        replaceGzFlag = strassign(replaceGzFlag);
    else
        replaceGzFlag = DEF_FLAG;
    end
else
    replaceGzFlag = DEF_FLAG;
end
[zipDir,zipPre,zipExt] = fileparts(zipFilePath);
zipFile = strcat(zipPre,zipExt);
if(strcmp(zipExt,'.gz')== true)
    outFile = zipPre;
else
    outFile = zipFile;
end
% -- the gunzip has to be done in the directory 
%  local to the file, so
%   SAVE where we are
%  SPLIT zip file into directory and file name
%  CHANGE to directory of ZIP file
%   -- do the unzip
%  CHANGE back to current directory
curDir = pwd;
cd(zipDir);
% IF replacing the gzip file
%   SET command to overwrite GZIP file
% ELSE
%   SET command to unzip without destroying the original
% ENDIF
%
%D fprintf('GUNZIPFILE: Unzip %s in directory %s\n',zipFile,zipDir);
if(replaceGzFlag == true)
    cmdStr = sprintf('/bin/sh -c "gunzip %s"',zipFile);
else
    cmdStr = sprintf('/bin/sh -c "gunzip -c %s"',zipFile);
end
[unixError,unixOut] = unix(cmdStr);
if(unixError ~= 0)
    msgId = 'gunzipfile:cmdFail';
    error(msgId,'%s: GUNZIP command failed on %s with error %d',...
        msgId,zipFilePath,unixError);
end
cd(curDir);
outPath = fullfile(zipDir,outFile);
return
