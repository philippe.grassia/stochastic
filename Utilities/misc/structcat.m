function out = structcat(struct1, struct2)

%STRUCTCAT Concatenate structures with identical field lists.
%
%SYNOPSIS
%   OUT = structcat(STRUCT1, STRUCT2)
%
%INPUTS
%   STRUCT1 - Base structure (see notes below)
%   STRUCT2 - Structure to be concatenated to STRUCT1 (see notes below)
%
%OUTPUT
%   OUT - Concatenated structure
%
%NOTES
%  Returns a concatenated structure.  Can be used to concatenate structures
%  returned by multiple invocations of the readMeta() function, for instance.
%
%  The input structures must have an identical set of fields.  The order of
%  the fields can be different for the two structures; the output structure
%  will have its fields in the same order as the first input structure.
%
%  Each field must be either a column vector (N rows by 1 column) or a row
%  vector (1 row by N columns), and the concatenation is done along the "long"
%  dimension.  If both inputs just have a single element, then the output is a
%  two-element column vector.  Both numeric arrays and cell arrays are allowed.
%  In particular, a vector of strings is a two-dimensional character array and
%  is not allowed, but a column-vector cell array with each cell containing a
%  string IS allowed.  The vector length and type (row vs. column) can differ
%  from one field to the next.
%
%AUTHOR
%   Peter Shawhan <peter.shawhan@ligo.org>

%  $Id$

%-- Make sure the inputs are both structures
if isstruct(struct1)==0 || isstruct(struct2)==0
  error('MATAPPS:STRUCTCAT:NotStructure','Both inputs must be structures');
end

%-- Make sure the structures contain the same lists of fields
fields1 = fieldnames(struct1);
fields2 = fieldnames(struct2);
if length(fields1) ~= length(fields2)
  error('MATAPPS:STRUCTCAT:DifferentFields',...
        'Input structures have different sets of fields');
end
if any(strcmp(sortrows(fields1),sortrows(fields2))==0)
  error('MATAPPS:STRUCTCAT:DifferentFields',...
        'Input structures have different sets of fields');
end

%-- Construct and execute Matlab commands to concatenate each field
for ifield = 1:length(fields1)
  %-- Look up the field name
  field = fields1{ifield};
  %-- Determine the size of the data array for this field in each structure.
  %-- Actually we do not need the individual sizes, we just need to know in
  %-- which dimension the sizes are different from 1.  So we simply add them.
  cmd = [ 'size(struct1.' field ')+size(struct2.' field ')' ];
  sizesum = eval(cmd);
  if sizesum(2) == 2
    %-- Both inputs are column vectors (or have only one element)
    catdim = 1;
  elseif sizesum(1) == 2
    %-- Both inputs are row vectors
    catdim = 2;
  else
    %-- Inputs are NOT both row vectors nor both column vectors
    error('MATAPPS:STRUCTCAT:DifferentFieldTypes', [ 'Field ''' field ...
          ''' is not the same type (row or column vector) for both inputs' ] );
  end

  cmd = sprintf( 'out.%s = cat(%d,struct1.%s,struct2.%s);',...
                 field, catdim, field, field );
%%  disp(cmd);
  eval(cmd);
end
