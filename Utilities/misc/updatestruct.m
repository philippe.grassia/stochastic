function outStruct = updatestruct(defStruct,inStruct)

%UPDATESTRUCT - updates default structure with new values
%
%SYNOPSIS
%   OUTSTRUCT = updatestruct(DEFSTRUCT,INSTRUCT)
%
%INPUTS
%   DEFSTRUCT - structure with default fields, values
%   INSTRUCT - structure with one (or more) fields
%
OUTPUT
%   OUTSTRUCTt - structure with fields updated from input
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%   $Id$

error(nargchk(2,2,nargin),'struct');
if(~isstruct(defStruct))
    msgId = 'updatestruct:badInput';
    error(msgId,'%s: default input must be structure',msgId);
end
outStruct = defStruct;
if(~isstruct(inStruct))
    msgId = 'updatestruct:noUpdate';
    warning(msgId,'%s: no data in update input',msgId);
    return
end
inFld = fieldnames(inStruct);
numFld = numel(inFld);
for k=1:numFld
    fld = char(inFld{k});
    outStruct.(fld) = inStruct.(fld);
end
return
