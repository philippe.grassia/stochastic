function mrgArray = addfields2structarray(strArray,fldArray)

%ADDFIELDS2STRUCTARRAY combine fields from two struct arrays
%
%SYNOPSIS
%   MRGARRAY = addfields2structarray(STRARRAY, FLDARRAY)
%
%INPUT
%   STRARRAY  - original structure array of data
%   FLDARRAY - structure array with new fields
%
%OUTPUT
%   MRGARRAY - structure array with fields from both
%                   structure arrays
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id$

error(nargchk(2,2,nargin),'struct');
mrgArray = strArray;
fieldList = fieldnames(fldArray);
numField = numel(fieldList);
for k=1:numField
    fld = char(fieldList{k});
    fldData = fldArray.(fld);
    mrgArray.(fld) = fldData;
end
return
