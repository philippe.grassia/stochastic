function [matFlag,matFile] = matfilecheck(filePath)

%MATFILECHECK - checks if file exists as MATLAB binary
%
%SYNOPSIS
%   [MATFLAG,MATFILE] = matfilecheck(FILEPATH)
%
%INPUT
%   FILEPATH - path to file (arbitrary extenion)
%
%OUTPUT
%   MATFLAG - true/false whether *.mat file found at path
%   MATFILE - path to MATLAB binary file found
%
%  - check for file that matches path but with 'mat' extension
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

% $Id$

matFlag = false;
matFile = [];
error(nargchk(1,1,nargin),'struct');
filePath = char(filePath);
if(~ischar(filePath))
    msgId = 'matfilecheck:badInput';
    error(msgId,'%s: file path must be string',msgId);
end
[fileDir,filePre,fileExt] = fileparts(filePath);
matPath = fullfile(fileDir,strcat(filePre,'.mat'));
if(exist(matPath,'file') == 2)
    matFlag = true;
    matFile = matPath;
else
    matFlag = false;
    matFile = [];
end
return

    
