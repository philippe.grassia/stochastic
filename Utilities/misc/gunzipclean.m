function gunzipclean(gzipFlag,unzipPath,tmpDir)

%GUNZIPCLEAN - clean up after temporary gunzip
%
%SYNOPSIS
%   gunzipclean(GZIPFLAG,UNZIPPATH,TMPDIR)
%
%INPUTS
%   GZIPFLAG - true/false flag whether file was GZIPed
%   UNZIPPATH - path to unzipped file
%   TMPDIR - if not empty, path to temporary directory
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>   

%  $Id$

% IF file was un-zipped
%   IF unzipped file still present
%       DELETE unzipped file
%   ENDIF
%   IF temporary directory in not empty
%       IF temp directory exists
%           DELETE temporary directory if empty
%       ENDIF
%   ENDIF
% ENDIF
error(nargchk(2,3,nargin),'struct');
if(nargin < 3)
    tmpDir = [];
end
if(gzipFlag == true)
    if(exist(unzipPath,'file')==2)
        delete(unzipPath);
    end
    if(~isempty(tmpDir))
        if(isdir(tmpDir))
%D            fprintf('GUNZIPCLEAN: delete temp directory %s\n',tmpDir);
            [successCode,failMsg,failMsgId] = rmdir(tmpDir);       
        end
    end
end
return
