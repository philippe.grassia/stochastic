function out = structcat(struct, index)
%STRUCTEX Extracts entries from a structure
%
%  Returns a structure as part of the input structure. A vector index
%  specifies the entries that are extracted.
%
%  Example: 
%     dataPart=structex(data, 1:100)
%
%  Extracts the first 100 entries of the structure 'data'
%
%  Written by Alexander Dietz, February 2005


%-- Make sure the input is a structure
if isstruct(struct)==0 
  error('MATAPPS:STRUCTEX:NotStructure','Input must be a structure');
end

%-- Get names of fields
fields = fieldnames(struct);

%-- Construct and execute Matlab commands to extract fields
for ifield = 1:length(fields)

  %-- Look up the field name
  field = fields{ifield};

  % Execute command
  cmd = [ 'out.' field '=[struct.' field '(index)];' ];
  %disp(cmd);
  eval(cmd);
end
