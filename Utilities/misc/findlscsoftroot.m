function lscsoftRoot = findlscsoftroot(matappsFile,lscsoftRoot)

%FINDLSCSOFTROOT - Find root directory for 'lscsoft' with 'matapps'
%
%SYNOPSIS
%   LSCSOFTROOT = findlscsoftroot(MATAPPSFILE,LSCSOFTROOT)
% 
%INPUTS
%   MATAPPSFILE - source code file under matapps in MATLAB path
%   LSCSOFTROOT - [OPTIONAL] current lscsoft root dir
%
%OUTPUTS
%   LSCSOFTROOT - appended lscsoft root dir
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

% $Id$

% IF no lscsoft root input
%   IF MATAPPS_TOP defined
%       GET lscsoft path from that
%   ELSE
%       GET full path to burstmdcpath directory
%       IF 'matapps' directory in the path
%           GET root of lscsoft 'sandbox'
%       ELSE
%           SET error for unknown lscsoft path
%           EXIT
%       ENDIF
%   ENDIF
% ENDIF
% IF path to repository is not valid
%   SET error for bad path
%   EXIT
% ENDIF
error(nargchk(1,2,nargin),'struct');
if(nargin < 2 || isempty(lscsoftRoot))
    topEnv = getenv('MATAPPS_TOP');
    if(~isempty(topEnv))
        [lscsoftRoot,matPath] = fileparts(topEnv);
    else
        appPath = which(matappsFile);
        if(isempty(appPath))
            msgId = 'findlscsoftroot:badFile';
            error(msgId,'%s: function %s not in MATLAB path',...
                msgId,matappsFile);
        end
        appPos = findstr(appPath,'matapps');
        if(appPos > 0);
            lscsoftRoot = mdcPath(1:(appPos-2));
        else
            msgId = 'findlscsoftroot:badRoot';
            error(msgId,'%s: can not find lscsoft root directory',msgId);
            return
        end
    end
end
if(~isdir(lscsoftRoot))
    msgId = 'findlscsoftroot:badRoot';
    error(msgId,'%s: lscroot directory %s not found',msgId,lscsoftRoot);
    return
end
