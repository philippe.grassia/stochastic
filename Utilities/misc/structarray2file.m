function errCode = structarray2file(txtFile,structArray,fieldList,addHead)

%STRUCTARRAY2FILE - write out verbose trigger list
%
%SYNOPSIS
%   ERRCODE = structarray2file(TXTFILE,STRUCTARRAY[,FIELDLIST,ADDHEAD])
%
%INPUTS
%   TXTFILE - path to output txt file OR fileId
%             if FileId, file is not opened or closed
%   STRUCTARRAY - structure of arrays of data
%   FIELDLIST - [OPTIONAL] fields to write out to file
%   ADDHEAD - [OPTIONAL] true/false flag whether to add headings
%OUPUTS
%    ERRCODE = 0 is success, > 0 failure
%   
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id$

% GET input arguments
% IF not a file ID
%   SET flag for file name input
%   OPEN file for writing
% ELSE
%   CLEAR flag for file name input
%   SET file identifier from input
% ENDIF
% IF field list was input
%   GET field list from input
% ELSE
%   GET field list from structure
% ENDIF
% IF heading flag was input
%   SET heading flag from input
% ELSE
%   SET heading flag to default
% ENDIF
error(nargchk(2,4,nargin),'struct');
errCode = 10000;
useFileName = true;
if(~isnumeric(txtFile))
    useFileName = true;
    txtFile = char(txtFile);
    txtFid = fopen(txtFile,'w');
else
    useFileName = false;
    txtFid = txtFile;
end
if(~isstruct(structArray))
    msgId = 'structarray2file:badInput';
    error(msgId,'%s: data input must be a structure',msgId);
end
DEF_FIELD = fieldnames(structArray);
if(nargin > 2)
    if(isempty(fieldList))
        fieldList = DEF_FIELD;
    end
else
    fieldList = DEF_FIELD;
end    
numField = numel(fieldList);
DEF_HEADFLAG = true;
if(nargin > 3)
    addHeadFlag = addHead;
else
    addHeadFlag = DEF_HEADFLAG;
end
%
fldVal = structArray.(char(fieldList(1)));
numRow = numel(fldVal);
if(numRow < 1)
    if(useFileName==true)
        txtFid = fopen(txtFile,'w');
    end
    if(txtFid == -1)
        msgId = 'structarray2file:badFile';
        warning(msgId,'%s: cannot write to file %s',msgId,txtFile);
        errCode = 1;
        return
    end    
    if(addHeadFlag == true)
        fprintf(txtFid,'#');
        for k=1:numField
            fprintf(txtFid,' %s',char(fieldList(k)));
        end
        fprintf(txtFid,'\n');
    end
    if(useFileName==true)
        fclose(txtFid);
    end
    return
end

% LOOP over fields
%   GET sample value
%   IF integer 
%       ADD integer format
%   ELSE IF large/small
%       ADD exponential format
%   ELSE
%       ADD float format
%   ENDIF
% ENDLOOP
headFmt = [];
trFmt = [];
outFmt = cell(1,numField);
numericFld = zeros(1,numField);
for k=1:numField
    thisField = char(fieldList(k));
    fldVal = structArray.(thisField);
    typVal = fldVal(1);
    numElem = numel(fldVal);
    if(isstruct(typVal))
       timeVal = transpose(gps2time(fldVal));
       structArray.(thisField) = timeVal;
       thisFmt = '%14.4f';
       numLen = 14;
       numericFld(1,k) = true;
    else
        if(~isnumeric(typVal))
            typVal = char(typVal);
            numLen = length(typVal);
            thisFmt='%s';
            numericFld(1,k) = false;
        else
            numericFld(1,k) = true;
%
%  To check for integer, try more than one value
%
            numVal = min(200,numElem);
            testVal = fldVal(1:numVal);
            testFloor = floor(testVal);
            residVal = testVal - testFloor;
            nonInt = find(residVal > 0);
            if(numel(nonInt) == 0)
                if((typVal > 100000000) || ...
                        ~isempty(strfind(thisField,'gps')))
                    thisFmt = '%010d';
                    numLen = 10;
                else
                    thisFmt = '%6d';
                    numLen = 6;
                end
            else
                if(abs(typVal) < 1e-4)
                    thisFmt = '%14.7g';
                else
                    thisFmt = '%14.4f';
                end
                numLen = 14;
            end
            
        end
    end
    fldLen = numel(thisField);
    headLen = max(numLen,fldLen);
    outFmt{1,k} = sprintf('%s ',thisFmt);
    if(k > 1)
        fldFmt = sprintf('%%s %%%ds',headLen);
        headFmt = sprintf(fldFmt,headFmt,thisField);
        trFmt = sprintf('%s %s',trFmt,thisFmt);
    else
        fldFmt = sprintf('# %%%ds',headLen);
        headFmt = sprintf(fldFmt,thisField);
        trFmt = sprintf('%s',thisFmt);
    end
end
% OPEN file
% WRITE header
% LOOP over records
%   WRITE record using format
% ENDLOOP
% CLOSE file
headFmt = sprintf('%s\n',headFmt);
trFmt = sprintf('%s\n',trFmt);
if(useFileName==true)
    txtFid = fopen(txtFile,'w');
    if(txtFid==-1)
        msgId = 'structarray2file:badFile';
        warning(msgId,'%s: Unable to create file %s',msgId,txtFile);
        errCode = 1;
        return
    end
end
if(addHeadFlag==true)
    fprintf(txtFid,headFmt);
end
fldVal = structArray.(char(fieldList(1)));
numRow = numel(fldVal);
totNumeric = sum(numericFld);
if(totNumeric==numField)
    for k=1:numRow
        outVal = zeros(1,numField);
        for iFld = 1:numField
            fNam  = char(fieldList(iFld));
            outVal(1,iFld) = structArray.(fNam)(k);
        end
        fprintf(txtFid,trFmt,outVal);
    end
else
    for k=1:numRow
        for iFld = 1:numField
            fNam  = char(fieldList(iFld));
            outVal = structArray.(fNam)(k);
            if(numericFld(1,iFld)==true)
                fprintf(txtFid,char(outFmt{1,iFld}),outVal);
            else
                fprintf(txtFid,char(outFmt{1,iFld}),char(outVal));
            end
        end
        fprintf(txtFid,'\n');
    end
end
if(useFileName)
    fclose(txtFid);
end
return
