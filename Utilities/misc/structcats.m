function out = structcats(structs)
%STRUCTCAT Concatenate structures with identical field lists.
%  Returns a concatenated structure.
%
%  The input must be a vector of structures and each structure must have identical fields.  
%  However, there is no check on the array length of each field.
%
%  Written by Alexander Dietz

len=length(structs);

%-- Make sure at least two structures has been assigned
if len<2
  error('MATLAB:STRUCTCATS:TooFew','At least two structures has to be assigned');
end

%-- Adding structures again and again
out=structs(1);
for i=2:len
  out=structcat(out, structs(i));
end
