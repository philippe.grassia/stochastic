function outStruct = adddummy2structarray(strArray)

%ADDDUMMY2STRUCTARRAY - appends dummy row to structure of arrays
%
%SYNOPSIS
%   OUTSTRUCT = adddummy2structarray(STRARRARY)
%
%INPUT
%   STRARRAY - a structure of arrays (where the array for
%                   each field is the same length)
%
%OUTPUT
%   OUTSTRUCT - same structure as input, but with the
%       array for each field having an additional empty/0 entry
%
%  - A 'Structure of Arrays' is much more memory efficient than
%   an 'Array of Structures'.  We can use a structure of arrays
%   like an array of structure if we keep each array ( corresponding
%   to a field) the same length.
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id$

% CREATE dummy row from first row in structure array
% LOOP over fields
%   IF value is numeric
%       SET array for field to zero
%   ELSE
%       SET array for field to empty set
%   ENDIF
% ENDLOOP
% ADD dummy row to structure array
dumStruct = trimstructarray(strArray,1);
fieldList = fieldnames(dumStruct);
numField = numel(fieldList);
for k=1:numField
    thisField = char(fieldList(k));
    origArray = strArray.(thisField);
    if(isnumeric(origArray))
        newArray = 0;
    else
        newArray = cell(1,1);
    end
    dumStruct.(thisField) = newArray;
end
outStruct = addstructarray(strArray,dumStruct);
return
