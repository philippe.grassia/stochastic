function [outFile,unixError] = gzipfile(fileName)

%GZIPFILE - runs gzip on a single file
%
%SYNOPSIS
%   [OUTFILE,UNIXERROR] = gzipfile(FILENAME)
%
%INPUT
%   FILENAME - path name of original file
%
%OUTPUTS
%   OUTFILE - path name to zipped file
%   UNIXERROR - UNIX error code on 'gzip'
%
%   - assumes GZIP archive has .gz appended to existing file name
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id$

% IF file does not exist
%   CREATE error message
%   RETURN
% ENDIF
% ADD gzip extension to output file name
% IF gzipped file already exists
%   DELETE gzipped file
% ENDIF
% RUN GZIP on file
outFile = [];
error(nargchk(1,1,nargin),'struct');
fileName = char(fileName);
if(exist(fileName,'file')~=2)
    msgId = 'gzipfile:noFile';
    error(msgId,'%s file %s not found',msgId,fileName);
end
outFile = strcat(fileName,'.gz');
if(exist(outFile,'file')==2)
    delete(outFile);
end
cmdStr = sprintf('/bin/sh -c "gzip %s"',fileName);
[unixError,unixOut] = unix(cmdStr);
if(unixError ~= 0)
    msgId = 'gzipfile:cmdFail';
    error(msgId,'%s: GZIP command failed on %s with error %d',...
        msgId,fileName,unixError);
end
return
