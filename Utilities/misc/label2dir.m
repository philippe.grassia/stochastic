function dirStr = label2dir(labelStr)

%NAME2DIR - Create directory string from label
%
%SYNOPSIS
%   DIRSTR = label2dir(labelStr)
%
%INPUT
%   LABELSTR - character string label
%
%OUTPUT
%    DIRSTR - potential directory name with spaces, special 
%             characters removed
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id$

%  - We need to remove special characters (spaces, commas, etc.)
%  For each, find where they are and replace them
spPos = isspace(labelStr);
dirStr = [];
% remove spaces
if(numel(spPos) > 0)
    for k = 1:numel(spPos)
        if(spPos(k)==0)
            dirStr = [dirStr labelStr(k:k)];
        end
    end
end
% replace commas with dashes
comPos = findstr(dirStr,',');
if(numel(comPos) > 0)
    for k = 1:numel(comPos)
        dirStr(comPos(k):comPos(k)) = '-';
    end
end
% replaces slashes with underscores
slPos = findstr(dirStr,'/');
if(numel(slPos) > 0)
    for k = 1:numel(slPos)
        dirStr(slPos(k):slPos(k)) = '_';
    end
end
% replace + with A
plusPos = findstr(dirStr,'+');
if(numel(plusPos) > 0)
    for k = 1:numel(plusPos)
        dirStr(plusPos(k):plusPos(k)) = 'A';
    end
end
% replace . with d
dotPos = findstr(dirStr,'.');
if(numel(dotPos) > 0)
    for k = 1:numel(dotPos)
        dirStr(dotPos(k):dotPos(k)) = 'd';
    end
end
% replace : with _
semiPos = findstr(dirStr,':');
if(numel(semiPos) > 0)
    for k = 1:numel(semiPos)
        dirStr(semiPos(k):semiPos(k)) = '_';
    end
end
return
