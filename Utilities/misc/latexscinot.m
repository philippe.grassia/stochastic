function scinotstring = latexscinot(format,number);

%LATEXSCINOT - Converts a number into a LaTeX scientific notation string
%
%SYNOPSIS
%   SCINOTSTRING = latexscinot(FORMAT,NUMBER)
%
%INPUTS
%   FORMAT - sptrintf format code 
%   NUMBER - number to be formated
%
%OUTPUT
%   SCINOTSTRING - formated number as a string
%
%  N.B. This function can be used for, e.g., formatting numbers for
%  inclusion in LaTeX tables.  It converts number to scientific
%  notation using LaTeX markup, according to sprintf format format.
%  For example:
%
% >> num2str(1.0239742425252)
% ans = 1.024
% >> latexscinot('%f',342)
% ans = 3.420000\times 10^{2}
% >> latexscinot('%.2f',3.849e-5)
% ans = 3.85\times 10^{-5}
% >> latexscinot('%.2f',0)
% ans = 0.00
% >> latexscinot('%.3f',pi)
% ans = 3.142
%
%AUTHOR
%  John Whelan <john.whelan@ligo.org>

%  $Id$

%% Backwards-compatibility in case someone reverses format and number
if ~isnumeric(number)
  foo = number;
  number = format;
  format = foo;
end

if number == 0
  scinotstring = sprintf(format, 0);
else
  exponent = floor(log10(abs(number)));
  if exponent == 0
    scinotstring = sprintf(format, number);
  else
    scinotstring = sprintf([format '\\times 10^{%d}'], ...
                           number/10^exponent, exponent);
  end;
end;
return;
