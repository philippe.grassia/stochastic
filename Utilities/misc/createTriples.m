function [newL1, newH1, newH2]=createTriples(dataL1, dataH1, dataH2, ...
                                deltaTimeLH, deltaTimeHH, deltaPsi0, deltaPsi3)

% CREATETRIPLES Create coincident tripple triggers for L1, H1 and H2
%
%  Returns three structures of same length containing coincident tripple triggers.
%  The first two input vectors must be structures containing L1 and H1 data (both same length) 
%  while the third input vector contain the H2 data. The last four parameters specify
%  the coincident parameters (time window for L-H and H-H, window for psi0 and psi3).
%
%  Written by Alexander Dietz, February 2005


%-- Make sure the inputs are both structures
if isstruct(dataL1)==0 || isstruct(dataH1)==0 || isstruct(dataH2)==0 
  error('MATLAB:CREATETRIPLES:NotStructures','Inputs must be structures');
end

%-- Make sure the structures contain the same lists of fields
fields1 = sortrows(fieldnames(dataL1));
fields2 = sortrows(fieldnames(dataH1));
fields3 = sortrows(fieldnames(dataH2));
if length(fields1) ~= length(fields2)
  error('MATLAB:CREATETRIPLES:DifferentFields','First two input structures have different lengths');
end
if any(strcmp(fields1,fields2)==0)
  error('MATLAB:CREATETRIPLES:DifferentFields','Input structures have different sets of fields');
end
if any(strcmp(fields1,fields3)==0)
  error('MATLAB:CREATETRIPLES:DifferentFields','Input structures have different sets of fields');
end

% clearing vectors
clear newL1 newH1 newH2

% get time
timeL1 = double(dataL1.end_time) + double(dataL1.end_time_ns)/1.0e9;
timeH1 = double(dataH1.end_time) + double(dataH1.end_time_ns)/1.0e9;
timeH2 = double(dataH2.end_time) + double(dataH2.end_time_ns)/1.0e9;

% loop over the entries in H2 vector set
for i=1:length(fields3)     

     % time coin. for data1
     indexa=find( abs(timeH2(number)-timeL1) <= deltaTimeLH );
     indexb=find( abs(dataH2.psi0(number)-dataL1.psi0(indexa)) <= deltaPsi0 );
     indexc=find( abs(dataH2.psi3(number)-dataL1.psi3(indexa(indexb))) <= deltaPsi3 );
     index1=(indexa(indexb(indexc)));

     % time coin. for data2
     indexa=find( abs(timeH2(number)-timeH1(index1)) < deltaTimeHH );
     indexb=find( abs(dataH2.psi0(number)-dataH1.psi0(index1(indexa))) <= deltaPsi0 );
     indexc=find( abs(dataH2.psi3(number)-dataH1.psi3(index1(indexa(indexb)))) <= deltaPsi3 );

     % 'sum' indices
     index=index1(indexa(indexb(indexc)));
     len=length(index);
     
     % storing data into new structures for L1, H1;
     if exist('newL1')
        newL1=structcat(newL1, structex(data1, index));
        newH1=structcat(newH1, structex(data2, index));
     else
	newL1= structex(data1, index);
        newH1= structex(data2, index);
     end

     % storing data for H2 into new structures (repeated the same data);
     for j=1:len
     
        if exist('newH2')
           newH2=structcat(newH2, structex(data3, i));
        else
           newH2= structex(data3, i)
        end
         
     end
     
     
     
end

