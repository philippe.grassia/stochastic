function idx = clusterBasic(x, y, Delta_x, type);

%CLUSTERBASIC finds minima or maxmia of events with a min. distance
%
%SYNOPSIS
%   IDX = clusterBasic(X, Y, DELTA_X, TYPE)
%
%INPUT
%   X - distance (time) vector
%   Y - event vector
%   DELTA_X - minimum distance (time) between events
%   TYPE - 'max' to find maxima or 'min' to find minima
%
%OUTPUT
%   IDX - indicies of found largest or smallest events
%
%AUTHORS
%   Anand Sengupta <anand.sengupta@ligo.org> and 
%   Alexander Dietz <alexander.dietz@ligo.org>

% $Id$

%%% This function finds the largest (smallest) events (y) such that the events are
%%% separated in x by at least Delta_x. It returns the index of these events.
%%% The 4th argument specifies if to find the maximum value ('max')
%%% or the minimum value ('min').
%%% %   BLOCKRESAMPLE reduces memory usage for re-sampling by doing it on fixed
%   length blocks and then stiching the time-series back together.  
%%% Example:
%%%    index=clusterBasic(time, xml.snr, 4.0, 'max');
%%%
%%% Written by Anand Sengupta (anandss@iucaa.ernet.in) 
%%%        and Alexander Dietz (dietz@rouge.phys.lsu.edu)
%%%

% Get the size of input vectors
n = length(x);

% We assume the x is sorted but that is not guaranteed. EHS triggers are never sorted 
% in x for example 

% Sort on x (x could be time of arrival)
[sorted_x, sort_idx] = sort(x);
sorted_y = y(sort_idx);

% I stores the flag (event or non event). Indices of I where I>0 are retuned at the end.% To begin with none of the points are classified, so init I to zero.
I = zeros(n,1);
J = 1:n;

% ok stores the flag if a point should be considered further for classification. 
% To begin with all the points must be considered for classification. So init ok to ones
ok = ones(n,1);

% sum over ok is the total number of points that remain to be classified.
unclassified = sum(ok);

if (type=='max')
% start main loop. Loop over till no points remains unclassified 
% (as genuine or fake events).
while (unclassified > 0) 
	clear maxVal maxPos tmpPos; 
	[mVal, mPos] = max (sorted_y (ok > 0)); 
	tmpPos = J (ok > 0); 
	pos = tmpPos(mPos); 
	I(sort_idx(pos)) = 1;
	
	% We have an event at x_i. Now all points with x such that 
	%        x_i - Delta_x <= x <= x_i + Delta_x
	% are fake events. They must drop out in the next iteration
	% of the loop. This is achived by zeroing out appropriate ok[] values.
	ok ( abs(sorted_x -  sorted_x(pos)) <= Delta_x )= 0;

	% Now there are fewer points that remain unclassified. This number is 
	% found by summing over the ok[] vector.
	unclassified = sum(ok); 
end
end


if (type=='min')
% start main loop. Loop over till no points remains unclassified 
% (as genuine or fake events).
while (unclassified > 0) 
	clear maxVal maxPos tmpPos; 
	[mVal, mPos] = min (sorted_y (ok > 0)); 
	tmpPos = J (ok > 0); 
	pos = tmpPos(mPos); 
	I(sort_idx(pos)) = 1;
	
	% We have an event at x_i. Now all points with x such that 
	%        x_i - Delta_x <= x <= x_i + Delta_x
	% are fake events. They must drop out in the next iteration
	% of the loop. This is achived by zeroing out appropriate ok[] values.
	ok ( abs(sorted_x -  sorted_x(pos)) <= Delta_x )= 0;

	% Now there are fewer points that remain unclassified. This number is 
	% found by summing over the ok[] vector.
	unclassified = sum(ok); 
end
end


%%% Return the index
idx = J (I>0);

%%% End of program
