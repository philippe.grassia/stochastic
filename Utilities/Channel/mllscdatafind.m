function [fileList,nFiles,status,gpsList,durList] = ...
    mllscdatafind(observatory,type,gpsStart,duration)
% MLLSCDATAFIND - MATLAB interface to gw_data_find
%      [fileList,nFiles,status,gpsList,durList] = ...
%              mllscdatafind(observatory,type,gpsStart,duration)
%
% INPUTS
%  observatory - IGWD site(s) - 'H' Hanford,'L' Livingston, 'HL' sim (STRING)
%  type - type of data 'R', 'RDS_R_L1', 'SG5', etc.
%  gpsStart - start time in GPS Seconds (NUMERIC)
%  duration - length of time series in seconds (NUMERIC)
%  -- NOTE that gpsStart and duration are truncated to whole seconds!
%
% OUTPUTS
%  fileList - array of files names (STRING)
%  numFiles - number of files (NUMERIC)  - optional
%  status - status from framequery (NUMERIC) - optional
%      0 - Succeeded
%      1 - gw_data_find calls failed
%      2 - No frame files returned in list
%      3 - LIGO_DATAFIND_SERVER not defined
%
%  gpsList - [OPTIONAL] array of frame GPS times
%  durList - [OPTIONAL] array of frame durations
%
% DEPENDENCIES
%   Requires gw_data_find from LSC DataGrid
%
% ERRORS
%   badServer
%   queryFail
%
% $Id: mllscdatafind.m,v 1.10 2007-10-10 12:49:38 kathorne Exp $

% CLEAR output arrays
% GET LDR address
% IF LDR address not defined
%   CREATE error and exit
% ENDIF
% IF no-proxy variable is present and set
%   SET no-proxy flag
% ELSE
%   CLEAR no-proxy flag
% ENDIF
status = -1;
gpsList = [];
durList = [];
fileList = [];
nFiles = 0;
error(nargchk(4,4,nargin),'struct');
%
GPS_JUMP_FACTOR = 600;
% - the gw_data_find server is set by LIGO_DATAFIND_SERVER
lscServer = getenv('LIGO_DATAFIND_SERVER');
if(isempty(lscServer))
    status = 13;
    msgId = 'mllscdatafind:badServer';
    warning(msgId,'%s: LIGO_DATAFIND_SERVER is not defined',msgId);
    return
end
%   -- Added October 9, 2007
% - We use the environment variable LIGO_DATAFIND_NOPROXY
%   to indicate that cert-less queries are to be used
%  - The variable needs to be present AND be = 1
lscNoProxy = getenv('LIGO_DATAFIND_NOPROXY');
noProxyFlag = false;
if(~isempty(lscNoProxy))
    flagValue = str2num(lscNoProxy);
    if(isnumeric(flagValue))
        if(flagValue == 1)
            noProxyFlag = true;
        else
            noProxyFlag = false;
        end
    else
        noProxyFlag = false;
    end
end

% CREATE gw_data_find message
% SET number of retries
% SET gw_data_find failed flag
% WHILE retries left and gw_data_find failed flag is set
%   SHELL out to call gw_data_find, retrieving standard output
%   DECREMENT number of retries
%   IF gw_data_find call succeeded
%       CLEAR gw_data_find failed flag
%   ENDIF
% ENDWHILE
gpsEnd = gpsStart + duration;
%  25 Oct 2006 KT - remove -m localhost as this adds in non-useful
%       locations
if(noProxyFlag == true)
    lscCom = sprintf(...
'/bin/sh -c "gw_data_find -r %s -o %s -t %s -s %09d -e %09d -u file --no-proxy -l 2>/dev/null"',...
        lscServer,observatory,type,gpsStart,gpsEnd);
else
    lscCom = sprintf(...
'/bin/sh -c "gw_data_find -r %s -o %s -t %s -s %09d -e %09d -u file -l 2>/dev/null"',...
        lscServer,observatory,type,gpsStart,gpsEnd);
end
MAX_RETRIES = 3;
numTry = MAX_RETRIES;
lscFail = true;
frameListData = [];
while((numTry > 0) && (lscFail==true))
    [unixError,unixOut]=unix(lscCom);
    numTry = numTry - 1;
    if(unixError == 0)
        lscFail = false;
        frameListData = unixOut;
    end
end

% IF gw_data_find failed flag still set
%   SET failed return status
%   CREATE warning and exit
% ENDIF
if(lscFail == true)
    status = 1;
    msgId = 'mllscdatafind:queryFail';
    warning(msgId,'%s: gw_data_find calls failed after %d retries',...
        msgId,MAX_RETRIES);
    return
end

% IF no frames returned
%   - redo query expanding GPS range so that frame boundaries
%   are crossed.  Specifically, reduce start time by a 
%   'jump' factor, and increase end time by same 'jump'factor
%  ** This is needed because gw_data_find indexes do not
%    handle cases where GPS interval with within a frame duration
% CREATE gw_data_find message
% SET number of retries
% SET gw_data_find failed flag
% WHILE retries left and gw_data_find failed flag is set
%   SHELL out to call gw_data_find, retrieving standard output
%   DECREMENT number of retries
%   IF gw_data_find call succeeded
%       CLEAR gw_data_find failed flag
%   ENDIF
% ENDWHILE
if(numel(frameListData) < 50)
    newStart = gpsStart - GPS_JUMP_FACTOR;
    newEnd = gpsEnd + GPS_JUMP_FACTOR;
%  2 July 2007 KT - remove -m localhost as this adds in non-useful
%       locations
    if(noProxyFlag == true)
       lscCom = sprintf(...
'/bin/sh -c "gw_data_find -r %s -o %s -t %s -s %09d -e %09d -u file -l --no-proxy 2>/dev/null"',...
        lscServer,observatory,type,newStart,newEnd);
    else
       lscCom = sprintf(...
'/bin/sh -c "gw_data_find -r %s -o %s -t %s -s %09d -e %09d -u file -l 2>/dev/null"',...
        lscServer,observatory,type,newStart,newEnd);
    end
    numTry = MAX_RETRIES;
    lscFail = true;
    frameListData = [];
    while((numTry > 0) && (lscFail==true))
        [unixError,unixOut]=unix(lscCom);
        numTry = numTry - 1;
        if(unixError == 0)
            lscFail = false;
            frameListData = unixOut;
        end
    end
%   IF gw_data_find failed flag still set
%       SET failed return status
%       CREATE warning and exit
%   ENDIF
    if(lscFail == true)
        status = 1;
        msgId = 'mllscdatafind:queryFail';
        warning(msgId,'%s: gw_data_find calls failed after %d retries',...
        msgId,MAX_RETRIES);
        return
    end
end   

% IF standard output is long enough
%   PARSE standard output (from gw_data_find) to arrays
%   GET # of files found
% ELSE
%   SET # of files to zero
% ENDIF
% SET output filelist from data
% SET output # of frame files from data 
% SET output GPS start times from data
% SET output durations from data
% IF no files were returned in list
%   SET failed return status
%   CREATE warning and exit
% ENDIF
numChar = numel(frameListData);
if(numChar > 50)
    [obs,typ,secs,durs,files]=strread(frameListData,'%s %s %d %d %s');
    nFiles = numel(secs);
else
    nFiles = 0;
end
if(nFiles > 0)
    gpsList = secs;
    durList = durs;
    fileList = files;
    status = 0;
else
    status = 2;
    msgId = 'mllscdatafind:noFiles';
    warning(msgId,'%s: gw_data_find query found no files',msgId);
end

% -- because the query time interval may now be larger
%  than the original GPS interval, we need to prune the
%  file list to remove frame files not overlapping the
%  original interval
% CALCULATE output GPS end times from data
% FIND indexes to frames which overlap GPS interval
%   'trick' is that max(frame start, GPS start) must
%   be < min(frame end, GPS end)
if(nFiles > 0)
    endList = secs + durs;
    goodIdx = find(max(gpsList,gpsStart) < min(endList,gpsEnd));
    nFiles = numel(goodIdx);
    gpsList = gpsList(goodIdx);
    durList = durList(goodIdx);
    fileList = fileList(goodIdx);
    if(nFiles < 1)
        status = 2;
        msgId = 'mllscdatafind:noFiles';
        warning(msgId,'%s: gw_data_find query found no files',msgId);
    end
end

return
