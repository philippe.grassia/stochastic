function [listError,gpsTimeTbl,frameTbl,durTbl] = ...
                        getframelistinfo(listData)
% GETFRAMELISTINFO - get frame list information
%   [listError,gpsTimeTbl,frameTbl,durTbl] = ...
%                        getframelistinfo(listData)
%
%       listData - 1) framelist structure (see mkframeliststruct)
%                  2) file with framelist information (LAL-cache)
%                  
%  listError - error code from framelist call
%  gpsTimes - array of GPS start times of frame files
%  frameFiles - cell-array of frame file paths
%  frameDurs - array of duration of frame files
%
%  Warnings
%       getframelistinfo:badList
%
%  $Id: getframelistinfo.m,v 1.1 2005-09-21 19:18:35 kathorne Exp $

% INITIALIZE outputs
% IF input path to a file
%   READ in frame list from (LAL Cache) file
%   IF output is not a structure
%       CREATE error
%       EXIT
%   ENDIF
% ELSEIF input is a structure
%   SET candidate structure from input
% ELSE
%   CREATE error
%   EXIT
% ENDIF
%   - now try to use data from candidate structure --
% IF any needed fields missing from structure
%   CREATE error
% ELSE
%   SET outputs from fields in structure
% ENDIF
error(nargchk(1,1,nargin),'struct');
error(nargchk(4,4,nargout),'struct');
listError = 1;
gpsTimeTbl = [];
frameTbl = [];
durTbl = [];
if(iscell(listData))
    listData = char(listData);
end
if(ischar(listData) && (exist(listData,'file') == 2))
    listStruct = lalcache2framelist(listData);
    if(~isstruct(listStruct))
        listError = 1;
        msgId = 'getframelistinfo:badList';
        warning(msgId,'%s: LAL cache %s was bad',msgId,listData);
        return
    end
else
    if(isstruct(listData))
        listStruct = listData;
    else
        listError = 1;
        msgId = 'getframelistinfo:badList';
        warning(msgId,'%s: input list is not a structure',msgId);
        return
    end
end
if(~isfield(listStruct,'listError') || ...
   ~isfield(listStruct,'gpsTimes') || ...
   ~isfield(listStruct,'frameFiles') || ...
   ~isfield(listStruct,'frameDurs'))
    listError = 1;
    msgId = 'getframelistinfo:badList';
    warning(msgId,'%s: input structure does not have correct fields',msgId);
else
    listError = listStruct.listError;
    gpsTimeTbl = listStruct.gpsTimes;
    frameTbl = listStruct.frameFiles;
    durTbl = listStruct.frameDurs;
end
return
