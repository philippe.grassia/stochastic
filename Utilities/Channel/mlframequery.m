function [fileList,nfiles,status,gpsList,durList] = ...
    mlframequery(site,gpsStart,duration) 
% MLFRAMEQUERY  - Matlab interface to framequery.c
% [fileList,nfiles,status[,gpsList,durList]] = ...
%                       mlframequery(site,gpsStart,duration)
%
% INPUTS
%  instrument - IGWD site(s) - 'H' Hanford,'L' Livingston, 'HL' sim (STRING)
%  type - type of data 'R', 'RDS_R_L1', 'SG5', etc.
%  gpsStart - start time in GPS Seconds (NUMERIC)
%  duration - length of time series in seconds (NUMERIC)
%  -- NOTE that gpsStart and duration are truncated to whole seconds!
%  -- NOTE that minimum expected frame file size is 16 seconds!
%
% OUTPUTS
%  fileList - array of files names (STRING)
%  numFiles - number of files (NUMERIC)  - optional
%  status - status from framequery (NUMERIC) - optional
%      0 - OK
%      1 - database connection failed
%      2 - no files returned
%      3 - mysql query failed in process_query
%      4 - problem processing result set in process_query
%      5 - query returned no data in process_query
%      8 - malloc failure on namlist in process_result_set
%      9 - malloc failure on gpslist in process_result_set
%      10 - malloc failure on durlist in process_result_set
%      11 - query returned too many rows in process_result_set
%      12 - malloc failure on fileptr in process_result_set
%      13 - malloc failure on gpsptr in process_result_set
%      14 - malloc failure on durptr in process_result_set
%  gpsList - [OPTIONAL] array of frame GPS times
%  durList - [OPTIONAL] array of frame durations
%
% DEPENDENCIES
%  Requires framequery.h, framequery.c, common_mysql.h, common_mysql.c
%
% $Id: mlframequery.m,v 1.1 2004-08-11 18:43:34 kathorne Exp $

%#external
%#mex
