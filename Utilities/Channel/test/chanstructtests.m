function [numFail,failedTests] = chanstructtests(frameType,ligoRun)
% CHANSTRUCTTESTS - Channel structure tests
%   [numFail,failedTests] = chanstructtests(frameType,ligoRun)
%       frameType [OPTIONAL] frame file type <default is RDS_R_L1>
%       ligoRun [OPTIONAL] LIGO run to use <default is S3>
%
%       numFail = 0 if all passed, otherwise array of error messages
%       failedTests = cell-array of failed tests
%
%  $Id: chanstructtests.m,v 1.4 2005-09-21 19:19:45 kathorne Exp $

% INITIALIZE outputs
if (nargin < 1)
    frameType = 'RDS_R_L1';
end
if (nargin < 2)
    ligoRun = 'S3';
end
if(strcmp(ligoRun,'S2') == true)
    GPS_START = 734360003;
    GPS_23=730033907;
    DUR_23=200;
    GPS_24=730051750;
    DUR_24=210;
    GPS_25=729711200;
    DUR_25=220;
    GPS_26 = 734360003.5;
    DUR_26 = 24.25;
elseif(strcmp(ligoRun,'S3') == true)
    GPS_START = 751976393;
    GPS_23=751593568;
    DUR_23=200;
    GPS_24=759311784;
    DUR_24=210;
    GPS_25=756676000;
    DUR_25=800;
    GPS_26 = 751976393.5;
    DUR_26 = 24.25;
else
    fprintf(' CHANCLASSTESTS: Bad LIGO run\n');
    return
end
fprintf(' Start tests of chanstruct, chanvector \n');
numFail = 0;
failedTests = [];

% Test 1 - Name is set from input
% Test 2 - Status = 0 if input name valid
% Test 3 - Name can not be set NOT CONTROLLED IN STRUCTURE
% Test 4 - Status can not be set NOT CONTROLLED IN STRUCTURE
nameA = 'H1:LSC-AS_Q';
chan1 = chanstruct(nameA);
name1A = chan1.name;
tst1A = strcmp(nameA,name1A);
if(tst1A ~= 1)
    [numFail,failedTests]=addfail(numFail,failedTests,...
     'FAILED Test 1 - Name set from chanstruct input');
end
tst2A = chan1.statusCode;
if(tst2A ~= 0)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 2 - Status = 0 if input name valid');
end
clear chan1;

% Test 5 - Default site
% Test 6 - Default type
% Test 7 - site can be set
% Test 8 - type can be set
nameB = 'L1:LSC-AS_Q';
chan5 = chanstruct(nameB);

site5 = 'LDR';
site5A = chan5.site;
tst5A = strcmp(site5A,site5);
if(tst5A ~= 1)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 5 - default site is not LDR');
end
type6 = 'RDS_R_L1';
type6A = chan5.type;
tst6A = strcmp(type6A,type6);
if(tst6A ~= 1)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 6 - default type is not RDS_R_L1');
end
siteA = 'siteA';
chan5.site = siteA;
site7A = chan5.site;
tst7A = strcmp(site7A,siteA);
if(tst7A ~= 1)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 7 - unable to set site');
end
typeA = 'typeA';
chan5.type = typeA;
type8A = chan5.type;
tst8A = strcmp(type8A,typeA);
if(tst8A ~= 1)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 8 - unable to set type');
end
clear chan5;

% Test 9 - default instrument
% Test 10 - instrument can be set
% Test 11 - default gps baseline
% Test 12 - gps baseline can be set
nameC = 'H2:LSC-ASC_Q';
chan9 = chanstruct(nameC);

instr9 = 'H';
instr9A = chan9.instrument;
tst9A = strcmp(instr9A,instr9);
if(tst9A ~= 1)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 9 - default site is not first letter of name');
end
instrA = 'HL';
instrB = 'HLT';
chan9.instrument = instrA;
instr10A = chan9.instrument;
tst10A = strcmp(instr10A,instrA);
if(tst10A ~= 1)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 10 - unable to set instrument');
end

tst11A = chan9.gpsStart;
if(tst11A ~= 0)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 11 - default gpsStart is not 0');
end
clear chan9;

gpsA = 729330101;
chan12 = chanstruct(nameA,gpsA);
gps12A = chan12.gpsStart;
gpsB = 734234980;
if(gps12A ~= gpsA)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 12 - gpsStart can not be set');
end
clear chan12;

% Test 13 - default rate
% Test 14 - rate can be set
% Test 15 - Status = 1 if input name invalid
chan13 = chanstruct(nameA);
rate13A = chan13.rate;
if(rate13A ~=0)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 13 - default rate = 0');
end

rateA = 16384;
chan13.rate = rateA;
rate14A = chan13.rate;
if(rate14A ~= rateA)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 14 - rate can not be set');
end
clear chan13;

nameD = 'BADNAME';
chan15 = chanstruct(nameD);
stat15A = chan15.statusCode;
if(stat15A ~= 1)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 15 - statusCode is not 1 if name bad');
end
clear chan15;

% Test 16 - time-series with GPS start, duration
%           we can't check the rate or error code
% Test 17 - time-series with GPS start GPS stop
chan16 = chanstruct('H2:LSC-AS_Q');
chan16.type = frameType;
fs = 16384;
gStart = GPS_START;
nDur = 24;
try
    [vect16,rate16,error16] = chanvector(chan16,gStart,nDur);
catch
    vect16 = [];
    error16 = 1;
    rate16 = 0;
end
siz16 = length(vect16);
if ( ((nDur*rate16) ~= siz16) || (0 ~= error16))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 16 - time-series using GPS start, duration is bad');
end
clear chan16;
clear vect16;
chan17 = chanstruct('H1:LSC-AS_Q');
chan17.type = frameType;
gEnd = gStart + (2*nDur);
try
    [vect17,rate17,error17] = chanvector(chan17,gStart:gEnd);
catch
    vect17 = [];
    error17 = 1;
    rate17 = 0;
end
siz17 = length(vect17);
if ( (((gEnd-gStart)*rate17) ~= siz17) || (0 ~= error17))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 17 - time-series using GPS start, end is bad');
end
clear chan17;
clear vect17;
% Test 19 - empty time-series if no duration
%       use try, catch to avoid stopping due to error
% Test 20 - empty time-series if duration = 0;
% Test 21 - empty time-series if GPS range not covered.
chan19 = chanstruct('L1:LSC-AS_Q');
chan19.type = frameType;
try
    [vect19,rate19,error19] = chanvector(chan19,gStart);
catch
    error19 = 1;
    vect19 = [];
    rate19 = 0;
end
if(0 == error19 )
   [numFail,failedTests]=addfail(numFail,failedTests,...
   'FAILED Test 19 - no failure on missing duration');
end
try
    [vect20,rate20,error20] = chanvector(chan19,gStart,0);
catch
    error20 = 1;
    vect20 = [];
    rate20 = 0;
end
if(0 == error20)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 20 - no failure on duration = 0');
end
warning off framelist:nofiles;
warning off fetchseries:emptyFrameList;
warning off chanvector:badframelist;
warning off chanvector:badtimeseries;
warning off mllscdatafind:queryFail
try
    [vect21,rate21,error21] = chanvector(chan19,98900,500);
catch
    error21 = 1;
    vect21 = [];
    rate21 = 0;
end
siz21 = length(vect21);
if((siz21 ~=0) || (0 == error21))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 21 - no failure if GPS range not covered');
end

% Test 22 - return 0-filled vector if rate set but
%               GPS range not coverd
chan19.rate = fs;
rate22 = 0;
vect22 = [];
try
    [vect22,rate22,error22] = chanvector(chan19,98900,500);
catch
    error22 = 1;
end
siz22 = length(vect22);
if(siz22 > 0)
    maxVal = max(vect22);
    minVal = min(vect22);
else
    maxVal = 0;
    minVal = 0;
end
tst22 = ((maxVal == minVal) || (0 == maxVal));
if((siz22 ~= (fs*500)) || (0 == error22) ||(false == tst22))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 22 - no failure if GPS range not covered');
end
warning on framelist:nofiles;
warning on fetchseries:emptyFrameList;
warning on chanvector:badframelist;
warning on chanvector:badtimeseries;
warning on mllscdatafind:queryFail
clear chan16;
clear chan19;
clear vect19;
clear vect20;
clear vect21;
clear vect22;

% Test 23 - Missing frames at start of interval
% Test 24 - Missing frames at end of interval
% Test 25 - Missing frames in middle of interval
warning off framelist:nofirst;
warning off chanvector:badframelist;
warning off fetchseries:missingData;
warning off chanvector:badtimeseries;
chan23 = chanstruct('H2:LSC-AS_Q');
chan23.type = frameType;
gps23=GPS_23;
dur23=DUR_23;
vect23 =[];
error23 = false;
rate23 = 0;
try
    [vect23,rate23,error23] = chanvector(chan23,gps23,dur23);
catch
    error23 = 1;
end
siz23 = length(vect23);
if((siz23 ~= (dur23*rate23)) || (0 == error23))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 23 - did not handle missing frame at beginning');
end
warning on framelist:nofirst;
warning off framelist:nolast;
gps24=GPS_24;
dur24=DUR_24;
chan23.rate = 0;
vect24 = [];
rate24 = 0;
try
    [vect24,rate24,error24]=chanvector(chan23,gps24,dur24);
catch
    error24 = 1;
end
siz24 = length(vect24);
if((siz24 ~= (dur24*rate24)) || (0 == error24))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 24 - did not handle missing frame at the end');
end
warning on framelist:nolast;
warning off framelist:nomiddle;
gps25=GPS_25;
dur25=DUR_25;
chan23.rate = 0;
vect25 = [];
rate25 = 0;
try
    [vect25,rate25,error25]=chanvector(chan23,gps25,dur25);
catch
    error25 = 1;
end
siz25 = length(vect25);
if((0 == error25) ||  (siz25 ~= (dur25*rate25)))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 25 - did not handle missing frame in the middle');
end
warning on framelist:nomiddle;
warning on chanvector:badframelist;
warning on fetchseries:missingData;
warning on chanvector:badtimeseries;
clear chan23;
clear vect23;
clear vect24
clear vect25;

%
% test26 with non-integer limits
%
chan26 =chanstruct('H1:LSC-AS_Q');
chan26.type = frameType;
gps26 = GPS_26;
dur26 = DUR_26;
rate26 = 0;
vect26 = [];
try
    [vect26,rate26,error26] = chanvector(chan26,gps26,dur26);
catch
    error26 = 1;
end
siz26 = length(vect26);
if ( ((dur26*rate26) ~= siz26) || (error26 ~= 0))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 26 - did not handle non-integer GPS, duration');
end

%
% test27 chanframes with 3 outputs
%
chan27 = chanstruct('L1:LSC-AS_Q');
chan27.type = frameType;
fs = 16384;
gStart = GPS_START;
nDur = 200;
try
    [gps27,frame27,error27,dur27]=chanframes(chan27,gStart,nDur);
catch
    gps27=[];
    frame27 = [];
    error27 = 1;
    dur27 = [];
end
if(isempty(gps27) || isempty(frame27) || ...
   isempty(dur27) || 0 ~= error27)
    [numFail,failedTests]=addfail(numFail,failedTests,...
        'FAILED Test 27 - intermediate frame list with chanframes');    
else
    try
        [vect27,rate27,error27] = chanvector(chan27,gStart,nDur,...
           gps27,frame27,dur27); 
    catch
        vect27 = [];
        rate27 = 0;
        error27 = 1;
    end
    siz27 = length(vect27);
    if ( ((nDur*rate27) ~= siz27) || (0 ~= error27))
        [numFail,failedTests]=addfail(numFail,failedTests,...
            'FAILED Test 27 - intermediate frame list');    
    end
    clear vect27;
end
clear chan27;
clear gps27;
clear frame27;
clear error28;
clear dur27;

%
% test28 chanframes with frameList structure
%
chan28 = chanstruct('H1:LSC-AS_Q');
chan28.type = frameType;
fs = 16384;
gStart = GPS_START;
nDur = 170;
try
    [struct28]=chanframes(chan28,gStart,nDur);
catch
    struct28=[];
end
error28 = 1;
if(isstruct(struct28))
    if(isfield(struct28,'listError'))
        error28 = struct28.listError;
    end
end
if(0 ~= error28)
    [numFail,failedTests]=addfail(numFail,failedTests,...
        'FAILED Test 28 - intermediate frame list as structure');    
else
    try
        [vect28,rate28,error28] = chanvector(chan28,gStart,nDur,...
           struct28);
    catch
        vect28 = [];
        rate28 = 0;
        error28 = 1;
    end
    siz28 = length(vect28);
    if ( ((nDur*rate28) ~= siz28) || (0 ~= error28))
        [numFail,failedTests]=addfail(numFail,failedTests,...
        'FAILED Test 28 - intermediate frame list as structure');    
    end
    clear vect28;
end
clear chan28;
clear struct28;
clear error28;
%
% test29 frameList structure with channel name
%
chan29 = chanstruct('L1:LSC-AS_Q');
chan29.type = frameType;
fs = 16384;
gStart = GPS_START;
nDur = 195;
gEnd = gStart + nDur;
try
    [struct29]=chanframes(chan29,gStart:gEnd);
catch
    struct29=[];
end
error29 = 1;
if(isstruct(struct29))
    if(isfield(struct29,'listError'))
        error29 = struct29.listError;
    end
end
if(0 ~= error29)
    [numFail,failedTests]=addfail(numFail,failedTests,...
        'FAILED Test 29 - intermediate frame list with channel name');    
else
    try
        [vect29,rate29,error29] = chanvector('L1:LSC-AS_Q',gStart:gEnd,...
           struct29);
    catch
        vect29 = [];
        rate29 = 0;
        error29 = 1;
    end
    siz29 = length(vect29);
    if ( ((nDur*rate29) ~= siz29) || (0 ~= error29))
        [numFail,failedTests]=addfail(numFail,failedTests,...
        'FAILED Test 29 - intermediate frame list with channel name');    
    end
    clear vect29;
end
clear chan29;
clear struct29;
clear error29;

%
%  Test30 = LAL Cache with channel name
%
observatory = 'H';
start30 = gStart;
nDur = 122;
end30 = start30 + nDur;
cache30 = 'LAL_Cache_30.txt';
lscCom = sprintf(...
'/bin/sh -c "gw_data_find -o %s -t %s -s %09d -e %09d -l > %s"',...
        observatory,frameType,start30,end30,cache30);
MAX_RETRIES = 3;
numTry = MAX_RETRIES;
lscFail = true;
while((numTry > 0) && (lscFail==true))
    [unixError,unixOut]=unix(lscCom);
    numTry = numTry - 1;
    if(unixError == 0)
        lscFail = false;
    end
end
if(lscFail ~= false)
    [numFail,failedTests]=addfail(numFail,failedTests,...
        'FAILED Test 30 - LAL cache file with channel name');    
else
    try
        [vect30,rate30,error30] = chanvector('H2:LSC-AS_Q',start30:end30,...
           cache30);
    catch
        vect30 = [];
        rate30 = 0;
        error30 = 1;
    end
    siz30 = length(vect30);
    if ( ((nDur*rate30) ~= siz30) || (0 ~= error30))
        [numFail,failedTests]=addfail(numFail,failedTests,...
        'FAILED Test 30 - LAL cache file with channel name');    
    end
    clear vect30;
    delete(cache30);
end
    
%
%  Test31 = intermediate framelist with LAL Cache
%
observatory = 'L';
start31 = gStart;
nDur = 78;
end31 = start30 + nDur;
cache31 = 'LAL_Cache_31.txt';
lscCom = sprintf(...
'/bin/sh -c "gw_data_find -o %s -t %s -s %09d -e %09d -l > %s"',...
        observatory,frameType,start31,end31,cache31);
MAX_RETRIES = 3;
numTry = MAX_RETRIES;
lscFail = true;
while((numTry > 0) && (lscFail==true))
    [unixError,unixOut]=unix(lscCom);
    numTry = numTry - 1;
    if(unixError == 0)
        lscFail = false;
    end
end
if(lscFail ~= false)
    [numFail,failedTests]=addfail(numFail,failedTests,...
        'FAILED Test 31 - intermediate framelist with LAL cache');    
else
    try
        [struct31]=lalcache2framelist(cache31);
    catch
        struct31=[];
    end
    error31 = 1;
    if(isstruct(struct31))
        if(isfield(struct31,'listError'))
            error31 = struct31.listError;
        end
    end
    delete(cache31);
    if(0 ~= error31)
        [numFail,failedTests]=addfail(numFail,failedTests,...
            'FAILED Test 31 - intermediate framelist with LAL cache');    
    else
        try
            [vect31,rate31,error31] = chanvector('L1:LSC-AS_Q',start31,nDur,...
                struct31);
        catch
            vect31 = [];
            rate31 = 0;
            error31 = 1;
        end
        siz31 = length(vect31);
        if ( ((nDur*rate31) ~= siz31) || (0 ~= error31))
            [numFail,failedTests]=addfail(numFail,failedTests,...
            'FAILED Test 31 - intermediate framelist with LAL cache');    
        end
        clear vect31;
    end
    clear struct31;
end

%
if(0 == numFail)
    fprintf('** Channel structure passes all tests! **\n');
else
    fprintf('&& ERROR && Channel structure failed %d tests. They were:\n',numFail);
    for iMsg = 1:numFail
        fprintf('%s\n',char(failedTests{iMsg}));
    end
end
return

function [oldFail,oldMes] = addfail(oldFail,oldMes,failMes)
% ADDFAIL add a test failure to output
oldFail = oldFail + 1;
if(oldFail < 2)
    oldMes = cell(1,1);
end
oldMes(oldFail) = {failMes};
return
