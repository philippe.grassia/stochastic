function [numFail,failedTests] = chanclasstests(frameType,ligoRun)
% CHANCLASSTESTS - Channel class tests
%   [numFail,failedTests] = chanclasstests(frameType,ligoRun)
%       frameType [OPTIONAL] frame file type <default is RDS_R_L1>
%       ligoRun [OPTIONAL] LIGO run to use <default is S3>
%
%       numFail = 0 if all passed, otherwiarray of error messages
%       failedTests = cell-array of failed tests
%
%  $Id: chanclasstests.m,v 1.4 2005-09-21 19:19:45 kathorne Exp $

% INITIALIZE outputs
if (nargin < 1)
    frameType = 'RDS_R_L1';
end
if (nargin < 2)
    ligoRun = 'S3';
end
if(strcmp(ligoRun,'S2') == true)
    GPS_START = 734360003;
    GPS_23=730033907;
    DUR_23=200;
    GPS_24=730051750;
    DUR_24=210;
    GPS_25=729711200;
    DUR_25=220;
    GPS_26 = 734360003.5;
    DUR_26 = 24.25;
elseif(strcmp(ligoRun,'S3') == true)
    GPS_START = 751976393;
    GPS_23=751976270;
    DUR_23=200;
    GPS_24=752384406;
    DUR_24=210;
    GPS_25=756709760;
    DUR_25=5900;
    GPS_26 = 751976393.5;
    DUR_26 = 24.25;
else
    fprintf(' CHANCLASSTESTS: Bad LIGO run\n');
    return
end

fprintf(' Start tests of Channel class\n');
numFail = 0;
failedTests = [];

% Test 1 - Name is set from input
% Test 2 - Status = 0 if input name valid
% Test 3 - Name can not be set
% Test 4 - Status can not be set
nameA = 'H1:LSC-AS_Q';
chan1 = Channel(nameA);
name1A = chan1.name;
tst1A = strcmp(nameA,name1A);
name1B = get(chan1,'name');
tst1B = strcmp(nameA,name1B);
if((tst1A ~= 1) || (tst1B ~= 1))
    [numFail,failedTests]=addfail(numFail,failedTests,...
        'FAILED Test 1 - Name set from Channel input');
end
tst2A = chan1.statusCode;
tst2B = get(chan1,'statusCode');
if((tst2A ~= 0) || (tst2B ~= 0))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 2 - Status = 0 if input name valid');
end
warning off Channel:readonly
chan1.name = 'H1:LSC-TST_NM';
name3A = chan1.name;
chan3B=set(chan1,'name','H2:LSC-TST_NM');
name3B = get(chan3B,'name');
tst3A = strcmp(name3A,nameA);
tst3B = strcmp(name3B,nameA);
if((tst3A ~= 1) || (tst3B ~= 1))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 3 - Name is read-only for Channel');
end
chan1.statusCode = 34;
tst4A = chan1.statusCode;
chan4B=set(chan1,'statusCode',39);
tst4B = get(chan4B,'statusCode');
if((tst4A ~= 0) || (tst4B ~= 0))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 4 - statusCode is read-only for Channel');
end
warning on Channel:readonly
clear chan1;
clear chan3B;
clear chan4B;

% Test 5 - Default site
% Test 6 - Default type
% Test 7 - site can be set
% Test 8 - type can be set
nameB = 'L1:LSC-AS_Q';
chan5 = Channel(nameB);

site5 = 'LDR';
site5A = chan5.site;
tst5A = strcmp(site5A,site5);
site5B = get(chan5,'site');
tst5B = strcmp(site5B,site5);
if((tst5A ~= 1) || (tst5B ~= 1))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 5 - default site is not LDR');
end
type6 = 'RDS_R_L1';
type6A = chan5.type;
tst6A = strcmp(type6A,type6);
type6B = get(chan5,'type');
tst6B = strcmp(type6B,type6);
if((tst6A ~= 1) || (tst6B ~= 1))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 6 - default type is not RDS_R_L1');
end
siteA = 'siteA';
siteB = 'siteB';
chan5.site = siteA;
site7A = chan5.site;
tst7A = strcmp(site7A,siteA);
chan7B = set(chan5,'site',siteB);
site7B = get(chan7B,'site');
tst7B = strcmp(site7B,siteB);
if((tst7A ~= 1) || (tst7B ~= 1))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 7 - unable to set site');
end
typeA = 'typeA';
typeB = 'typeB';
chan5.type = typeA;
type8A = chan5.type;
tst8A = strcmp(type8A,typeA);
chan8B = set(chan5,'type',typeB);
type8B = get(chan8B,'type');
tst8B = strcmp(type8B,typeB);
if((tst8A ~= 1) || (tst8B ~= 1))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 8 - unable to set type');
end
clear chan5;
clear chan7B;
clear chan8B;

% Test 9 - default instrument
% Test 10 - instrument can be set
% Test 11 - default gps baseline
% Test 12 - gps baseline can be set
nameC = 'H2:LSC-ASC_Q';
chan9 = Channel(nameC);

instr9 = 'H';
instr9A = chan9.instrument;
tst9A = strcmp(instr9A,instr9);
instr9B = get(chan9,'instrument');
tst9B = strcmp(instr9B,instr9);
if((tst9A ~= 1) || (tst9B ~= 1))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 9 - default site is not first letter of name');
end
instrA = 'HL';
instrB = 'HLT';
chan9.instrument = instrA;
instr10A = chan9.instrument;
tst10A = strcmp(instr10A,instrA);
chan10B = set(chan9,'instrument',instrB);
instr10B = get(chan10B,'instrument');
tst10B = strcmp(instr10B,instrB);
if((tst10A ~= 1) || (tst10B ~= 1))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 10 - unable to set instrument');
end

tst11A = chan9.gpsStart;
tst11B = get(chan9,'gpsStart');
if((tst11A ~= 0) || (tst11B ~= 0))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 11 - default gpsStart is not 0');
end
clear chan9;
clear chan10B;

gpsA = 729330101;
chan12 = Channel(nameA,gpsA);
gps12A = chan12.gpsStart;
gpsB = 734234980;
chan12.gpsStart = gpsB;
gps12B = chan12.gpsStart;
gpsC = 789840989;
chan12B=set(chan12,'gpsStart',gpsC);
gps12C = get(chan12B,'gpsStart');
if((gps12A ~= gpsA) || (gps12B ~= gpsB) || (gps12C ~= gpsC))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 12 - gpsStart can not be set');
end
clear chan12;
clear chan12B;

% Test 13 - default rate
% Test 14 - rate can be set
% Test 15 - Status = 1 if input name invalid
chan13 = Channel(nameA);
rate13A = chan13.rate;
rate13B = get(chan13,'rate');
if((rate13A ~=0) || (rate13B ~= 0))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 13 - default rate = 0');
end

rateA = 16384;
rateB = 4096;
chan13.rate = rateA;
rate14A = chan13.rate;
chan14B = set(chan13,'rate',rateB);
rate14B = get(chan14B,'rate');
if((rate14A ~= rateA) || (rate14B ~= rateB))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 14 - rate can not be set');
end
clear chan13;
clear chan14B;

nameD = 'BADNAME';
chan15 = Channel(nameD);
stat15A = chan15.statusCode;
stat15B = get(chan15,'statusCode');
if((stat15A ~= 1) || (stat15B ~= 1))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 15 - statusCode is not 1 if name bad');
end
clear chan15;

% Test 16 - time-series with GPS start, duration
%           we can't check the rate or error code
% Test 17 - time-series with GPS start GPS stop
chan16 = Channel('H2:LSC-AS_Q');
chan16.type = frameType;
fs = 16384;
gStart = GPS_START;
nDur = 24;
error16 = false;
try
    vect16 = chan16(gStart,nDur);
catch
    vect16 = [];
    error16 = true;
end
siz16 = size(vect16);
if ( ((nDur*fs) ~= siz16(2)) || (true == error16))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 16 - time-series using GPS start, duration is bad');
end
clear chan16;
clear vect16;
chan17 = Channel('H1:LSC-AS_Q');
chan17.type = frameType;
gEnd = gStart + (2*nDur);
error17 = false;
try
    vect17 = chan17(gStart:gEnd);
catch
    vect17 = [];
    error17 = true;
end
siz17 = size(vect17);
if ( (((gEnd-gStart)*fs) ~= siz17(2)) || (true == error17))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 17 - time-series using GPS start, end is bad');
end
clear chan17;
clear vect17;
% Test 19 - empty time-series if no duration
%       use try, catch to avoid stopping due to error
% Test 20 - empty time-series if duration = 0;
% Test 21 - empty time-series if GPS range not covered.
chan19 = Channel('L1:LSC-AS_Q');
chan19.type = frameType;
try
    error19 = false;
    vect19 = chan19(gStart);
catch
    error19 = true;
    vect19 = [];
end
if(error19 ~= true)
   [numFail,failedTests]=addfail(numFail,failedTests,...
   'FAILED Test 19 - no failure on missing duration');
end
try
    error20 = false;
    vect20 = chan19(gStart,0);
catch
    error20 = true;
    vect20 = [];
end
if(error20 ~= true)
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 20 - no failure on duration = 0');
end
warning off framelist:nofiles;
warning off fetchseries:emptyFrameList;
warning off Channel:badframelist;
warning off Channel:badtimeseries;
warning off mllscdatafind:queryFail
error21 = false;
vect21 = [];
try
    vect21 = chan19(98900,500);
catch
    error21 = true;
end
siz21 = length(vect21);
if((siz21 ~=0) || (true == error21))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 21 - no failure if GPS range not covered');
end

% Test 22 - return 0-filled vector if rate set but
%               GPS range not coverd
chan19.rate = fs;
error22 = false;
vect22 = [];
try
    vect22 = chan19(98900,500);
catch
    error22 = true;
end
siz22 = length(vect22);
if(siz22 > 0)
    maxVal = max(vect22);
    minVal = min(vect22);
else
    maxVal = 0;
    minVal = 0;
end
tst22 = ((maxVal == minVal) || (0 == maxVal));
if((siz22 ~= (fs*500)) || (true == error22) ||(false == tst22))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 22 - no failure if GPS range not covered');
end
warning on framelist:nofiles;
warning on fetchseries:emptyFrameList;
warning on Channel:badframelist;
warning on Channel:badtimeseries;
warning on mllscdatafind:queryFail
clear chan16;
clear chan19;
clear vect19;
clear vect20;
clear vect21;
clear vect22;

% Test 23 - Missing frames at start of interval
% Test 24 - Missing frames at end of interval
% Test 25 - Missing frames in middle of interval
warning off framelist:nofirst;
warning off Channel:badframelist;
warning off fetchseries:missingData;
warning off Channel:badtimeseries;
chan23 = Channel('H2:LSC-AS_Q');
chan23.type = frameType;
gps23=GPS_23;
dur23=DUR_23;
vect23 =[];
error23 = false;
try
    vect23 = chan23(gps23,dur23);
catch
    error23 = true;
end
siz23 = length(vect23);
if((siz23 ~= (dur23*fs)) || (true == error23))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 23 - did not handle missing frame at beginning');
end
warning on framelist:nofirst;
warning off framelist:nolast;
gps24=GPS_24;
dur24=DUR_24;
chan23.rate = 0;
vect24 = [];
error24 = false;
try
    vect24=chan23(gps24,dur24);
catch
    error24 = true;
end
siz24 = length(vect24);
if((siz24 ~= (dur24*fs)) || (true == error24))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 24 - did not handle missing frame at the end');
end
warning on framelist:nolast;
warning off framelist:nomiddle;
gps25=GPS_25;
dur25=DUR_25;
chan23.rate = 0;
vect25 = [];
error25 = false;
try
    vect25=chan23(gps25,dur25);
catch
    error25 = true;
end
siz25 = length(vect25);
if((true == error25) ||  (siz25 ~= (dur25*fs)))
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 25 - did not handle missing frame in the middle');
end
warning on framelist:nomiddle;
warning on Channel:badframelist;
warning on fetchseries:missingData;
warning on Channel:badtimeseries;
clear chan23;
clear vect23;
clear vect24
clear vect25;

%
% test26 with non-integer limits
%
chan26 =Channel('H1:LSC-AS_Q');
chan26.type = frameType;
gps26 = GPS_26;
dur26 = DUR_26;
error26 =false;
try
    vect26 = chan26(gps26,dur26);
catch
    vect26 = [];
    error26 = true;
end
siz26 = length(vect26);
if ( (dur26*fs) ~= siz26 )
    [numFail,failedTests]=addfail(numFail,failedTests,...
    'FAILED Test 26 - did not handle non-integer GPS, duration');
end
if(0 == numFail)
    fprintf('** Channel class passes all tests! **\n');
else
    fprintf('&& ERROR && Channel class failed %d tests. They were:\n',numFail);
    for iMsg = 1:numFail
        fprintf('%s\n',char(failedTests{iMsg}));
    end
end
return

function [oldFail,oldMes] = addfail(oldFail,oldMes,failMes)
% ADDFAIL add a test failure to output
oldFail = oldFail + 1;
if(oldFail < 2)
    oldMes = cell(1,1);
end
oldMes(oldFail) = {failMes};
return
