/*----------------------------------------------------
 *
 * File Name: framequery_test.c
 *
 * Author:  Mike Foster, Keith Thorne
 *
 * Revision: $Id: framequery_test.c,v 1.1 2004-08-11 18:44:13 kathorne Exp $
 *
 *----------------------------------------------------
 *
 * NAME
 * main
 *
 * DESCRIPTION
 * Run a test on framequery
 *
 *
 *----------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "common_mysql.h"

void pf (bool, char *);

#define DEF_FRAME_LEN 16;

int main (int argc, char *argv[])
{
    struct FNList fnlist;
    div_t st;
    div_t et;
    bool tflag;
    bool gflag; 
    int i = 0;
    int return_code1, return_code2;
    int maxF = 400;
    char instrument[20] = "";
    char type[20] = "RDS_R_L1";
    int nfiles;
    int stime = 734360009;
    int dur = 3600;
    int frameLen = DEF_FRAME_LEN;
/*
 * first, test with internal allocation
 * check return code, also correct number of files returned
 */
    strcpy(instrument,"H");
    fnlist.maxfiles = 0;
    st = div (stime, frameLen);
    et = div (stime + dur - 1, frameLen);
    nfiles = et.quot - st.quot + 1;	/* expected number of files */
    return_code1 = framequery (&fnlist, instrument, type, stime, dur);
    tflag = (return_code1 == 0) && (nfiles == fnlist.nfiles);
    gflag = tflag;
    pf (tflag, "query w/internal malloc on instrument H type RDS_R_L1 ");
    if (!tflag)
    {
        printf ("NOTE: framequery internal alloc - return code = %d\n",
            return_code1);
        printf ("NOTE: Number of files = %d\n\n", fnlist.nfiles);
        for (i = 0; i < fnlist.nfiles; i++)
        {
            printf ("NOTE: fnlist %d:gps %s file %s\n", i, fnlist.gpslist[i],fnlist.namlist[i]);
        }
    }
/*
 * now, test with external allocation
 * check return code, also correct number of files returned
 */
    free (fnlist.namlist);
    fnlist.namlist = malloc (maxF * sizeof (char **));
    if (NULL == fnlist.namlist)
    {
        pf (false, "malloc failure for file name list pointers in test harness");
        exit (1);
    }
    free (fnlist.gpslist);
    fnlist.gpslist = malloc (maxF * sizeof (char **));
    if (NULL == fnlist.gpslist)
    {
        pf (false, "malloc failure for GPS start list pointers in test harness");
        exit (1);
    }
    free (fnlist.durlist);
    fnlist.durlist = malloc (maxF * sizeof (char **));
    if (NULL == fnlist.durlist)
    {
        pf (false, "malloc failure for duration list pointers in test harness");
        exit (1);
    }
    fnlist.nfiles = 0;
    fnlist.maxfiles = maxF;

    strcpy(instrument,"L");
    return_code2 = framequery (&fnlist, instrument, type, 734360070, 3597);
    tflag = (return_code2 == 0) && (nfiles == fnlist.nfiles);
    gflag = gflag && tflag;
    pf (tflag, "query w/external malloc on instrument L");
    if (!tflag)
    {
        printf ("NOTE: query w/external malloc - return code = %d\n",
            return_code1);
        printf ("NOTE: Number of files = %d\n", fnlist.nfiles);
        for (i = 0; i < fnlist.nfiles; i++)
        {
            printf ("NOTE: fnlist %d:gps %s file %s\n", i, fnlist.gpslist[i],fnlist.namlist[i]);
        }
    }
    free (fnlist.namlist);
    free (fnlist.gpslist);
    free (fnlist.durlist);
    pf (gflag, "framequery test");
    exit ((gflag?0:1));
}

void
pf (bool flag, char *tname)
{
  if (flag == true)
    {
      printf ("PASS: %s\n", tname);
    }
  else
    {
      printf ("FAIL: %s\n", tname);
    }

  return;
}
