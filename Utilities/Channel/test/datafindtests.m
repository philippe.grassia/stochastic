function errorCode = datafindtests
% DATAFINDTESTS - test setup of gw_data_find for MATLAB Channel class
%   errorCode = function datafindtests
%       errorCode = 0 passed, > 0 otherwise
%           1 - LIGO_DATAFIND_SERVER not defined
%           2 - LSC DataGrid Client not setup
%
% $Id: datafindtests.m,v 1.1 2005-01-26 23:19:05 kathorne Exp $

% GET LIGO datafind server environment variable
% IF undefined
%   CREATE error
%   SET error code
% ENDIF
errorCode = 0;
lscData = getenv('LIGO_DATAFIND_SERVER');
if(isempty(lscData))
    fprintf('** LIGO_DATAFIND_SERVER needs to be defined\n');
    errorCode = 1;
end
dataGrid = getenv('GLOBUS_LOCATION');
if(isempty(dataGrid))
    fprintf('** GLOBUS setup not done\n');
    errorCode = 2;
end
if(0 == errorCode)
    fprintf('** Access using gw_data_find is setup!\n');
end
return
