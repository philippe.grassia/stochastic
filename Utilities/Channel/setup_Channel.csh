#!/bin/csh
#  setup_Channel.csh
#    -- create MEX file, check environment variables
# start matlab
matlab -nodisplay -nojvm <<EOF
makechannelmex;
cd test;
errorCode = ldrtests;
cd ..;
exit;
EOF
