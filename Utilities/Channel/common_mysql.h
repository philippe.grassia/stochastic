/*-------------------------------------------------------------------------
 *
 * File name: common_mysql.h
 *
 * Authors: M. Foster, K. Thorne
 *
 * Revision: $Id: common_mysql.h,v 1.1 2004-08-11 18:43:34 kathorne Exp $
 *
 *-------------------------------------------------------------------------
 *
 * NAME
 * common_mysql.h
 *
 * SYNPOSIS
 * #include "common_mysql.h"
 *
 * DESCRIPTION
 * Declares the prototypes for the mySQL routines in common_mysql.c:
 * Defines structure used to retrieve file list from query
 *
 * DEPENDENCIES
 *  mysql.h - needs pointer to mySQL includes
 *-------------------------------------------------------------------------
 */

#ifndef _COMMON_MYSQL_H_
#define _COMMON_MYSQL_H_

#ifndef _MYSQL_H_
#include <mysql.h>
#define _MYSQL_H_
#endif

static char *COMMONMYSQLH = "$Id: common_mysql.h,v 1.1 2004-08-11 18:43:34 kathorne Exp $";

/*------------------------------------------------------------------------_
 * Structure definitions
 *-------------------------------------------------------------------------
 */

/*
 *FNList - list of files retrieved from query
 *  maxfiles - max # of files ** SET this when doing a malloc before
 *             calling
 *  nfiles - # of files retrieved
 *  namlist - pointer array to file strings
 *  gpslist - pointer array to GPS start time strings
 *  durlist - pointer array to duration strings
 *  amount of memory is maxfiles*3*sizeof(char*) + 2*sizeof(int)
 *
 */
struct FNList
{
  int maxfiles;
  int nfiles;
  char **namlist;
  char **gpslist;
  char **durlist;
};

/*------------------------------------------------------------------------_
 * Function declarations
 *-------------------------------------------------------------------------
 */

void print_dashes (MYSQL_RES *res_set);

int process_result_set (MYSQL *conn, MYSQL_RES *res_set, struct FNList *fnlist);

void process_result_set_original (MYSQL *conn, MYSQL_RES *res_set);

void process_result_set_with_dashes (MYSQL *conn, MYSQL_RES *res_set);

int process_query (MYSQL *conn, char *query, struct FNList *fnlist);

void print_error (MYSQL *conn, char *message);

MYSQL *do_connect (char *host_name, char *user_name, char *password, char *db_name,
            unsigned int port_num, char *socket_name, unsigned int flags);

void do_disconnect (MYSQL *conn);

#endif
