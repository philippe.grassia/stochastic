function [structOrGpsTimeTbl,frameTbl,framesError,durTbl] = chanframes(chanObj,gpsRange,duration)
% CHANFRAMES return list of frame files for sub-section of IGWD channel
% [frameListStruct] = chanframes(chanObj,gpsStartSec,durationSec)
% [gpsTimeTbl,frameTbl[,framesError,durTbl]] = chanframes(chanObj,gpsStartSec,durationSec)
%    frame files for structure 'chanObj' starting at gpsStartSec
%       with a duration in durationSec.
%
% [frameListStruct] = chanframes(chanObj,gpsStartSec:gpsEndSec)
% [gpsTimeTbl,frameTbl[,framesError,durTbl]] = chanframes(chanObj,gpsStartSec:gpsEndSec)
%    frame files for structure 'chanObj' starting at gpsStartSec
%       and ending just before gpsEndSec.
%
% gpsTimeTbl - table of GPS start times (sec) for frame files
% frameTbl - table of paths to frame files
% framesError [OPTIONAL] error code from vector creation
%      = 0 if no problems
%        1-10 error from FRAMELIST
%        11-30 error from MLFRAMEQUERY (via FRAMELIST)
%      41 Channel input is not a structure
%      42 non-numeric GPS range
%      43 input structure is not a Channel structure
%      44 Bad GPS range
%   durTbl [OPTIONAL] table of durations of frame files
%
%   These can be retrieved individually (as 2-4 outputs) or as a 
%       single structure (1 output)
%
% $Id: chanframes.m,v 1.3 2006-06-12 14:05:37 kathorne Exp $

% IF invalid # of inputs, outputs
%   flag as error
%   exit
% ENDIF
% iF first input is not a structure
%    flag as error
%    exit
% ENDIF
% IF second input is not numeric
%    flag as error
%    exit
% ENDIF
error(nargchk(2,3,nargin),'struct');
error(nargchk(1,4,nargout),'struct');
gpsTimeTbl = [];
frameTbl = [];
durTbl = [];
structOrGpsTimeTbl = [];
framesError = 11;
if(~isa(chanObj,'struct'))
    framesError = 41;
    error('CHANFRAMES: Channel is not a structure');
end
if(~isa(gpsRange,'numeric'))
    framesError = 41;
    error('CHANFRAMES: GPS range not numeric');
end
% get Channel structure properties
% IF any are empty (i.e. not defined)
%   flag as non-channel struct error
%   return;
% ENDIF
name = chanObj.name;
instrument = chanObj.instrument;
type = chanObj.type;
site = chanObj.site;
if(isempty(name) || isempty(type) || isempty(site) ||...
         isempty(instrument))
    framesError = 43;
    error('CHANFRAMES: input is not a Channel structure');
end

% IF # columns in GPS Range entry = 1 (indicating #,#)
%   IF there is a duration entry
%       IF it is a valid number
%           SET gpsStartSec to first entry
%           SET durationSec to second entry
%           SET GPS range flag
%       ELSE
%           CREATE error about bad duration value
%           EXIT
%       ENDIF
%   ELSE
%       CREATE error about missing duration
%       EXIT
%   ENDIF
% ELSE
%   SET gpsOffsetSec to 1st element of input vector
%   SET gpsEndSec to last element of input vector
%   SET durationSec = gpsEndSec - gpsOffsetSec
%   SET GPS range flag
% ENDIF
isGpsRange = false;
[mrows,ncols] = size(gpsRange);
if(ncols == 1)
    if(nargin>2)
        if(isa(duration,'numeric'))
            gpsOffsetSec = gpsRange;
            durationSec = duration;
            isGpsRange = true;
        else
            framesError = 44;
            error('CHANFRAMES: duration input is not numeric');
        end
    else
        framesError = 44;
        error('CHANFRAMES: range input has GPS start, no duration');
    end
else
    gpsOffsetSec = gpsRange(1);
    gpsEndSec = gpsRange(ncols);
    durationSec = gpsEndSec - gpsOffsetSec;
    isGpsRange = true;
end

% IF gps Range flag not set
%   FLAG as error
%   exit
% ENDIF
% IF duration is not > 0
%   FLAG as error
%   exit
% END
% SET start time = channel start time + GPS offset
% USE framelist function to get list of frame files covering
%                     start time to (start time + duration)
% IF framelist was not successful
%   FLAG as error
%   exit
% ENDIF
if(isGpsRange ~= true)
    framesError = 44;
    error('CHANFRAMES: Bad GPS range format');
end
if(durationSec <= 0)
    framesError = 44;
    error('CHANFRAMES: GPS range duration is <= 0');
end
startTime = chanObj.gpsStart + gpsOffsetSec;
frameListStruct = framelist(instrument,type,startTime,durationSec);
if(frameListStruct.listError ~= 0)
    msgId = 'chanframes:badframelist';
    errStr = sprintf('CHANFRAMES: error %d in getting frame file list\n',...
        frameListStruct.listError);
    warning(msgId,errStr);
end
% IF asking for multiple outputs
%   SET outputs to fields of structure
% ELSE
%   SET output to structure
% ENDIF
if(nargout > 1)
    [framesError,structOrGpsTimeTbl,frameTbl,durTbl] = ...
        getframelistinfo(frameListStruct);
else
    structOrGpsTimeTbl = frameListStruct;
end

return
