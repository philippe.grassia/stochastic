/* mlframequery.c  - Matlab interface to framequery.c
 * [fileList,nfiles,status[,gpsList,durList]] = function mlframequery(instrument,type,gpsStart,duration)
 *
 * INPUTS
 *  instrument - IGWD data site - 'H' for Hanford,'L' for Livingston, 'HL' for sims (STRING)
 *  type - type of data 'R', 'RDS_R_L1', 'SG5', etc.
 *  gpsStart - start time in GPS Seconds (NUMERIC)
 *  duration - length of time series in seconds (NUMERIC)
 *   ** NOTE that gpsStart and duration are truncated to whole seconds!
 *   ** NOTE that minimum expected frame file size is 16 seconds!
 *
 * OUTPUTS
 *  fileList - array of files names (STRING)
 *  numFiles - number of files (NUMERIC)  - optional
 *  status - status from framequery (NUMERIC) - optional
 *      0 - OK
 *      1 - database connection failed
 *      2 - no files returned
 *      3 - mysql query failed in process_query
 *      4 - problem processing result set in process_query
 *      5 - query returned no data in process_query
 *      8 - malloc failure on namlist in process_result_set
 *      9 - malloc failure on gpslist in process_result_set
 *      10 - malloc failure on durlist in process_result_set
 *      11 - query returned too many rows in process_result_set
 *      12 - malloc failure on fileptr in process_result_set
 *      13 - malloc failure on gpsptr in process_result_set
 *      14 - malloc failure on durptr in process_result_set
 *
 *  gpsList - array of frame GPS times - optional
 *  durList - array of frame durations - optional
 *
 * DEPENDENCIES
 *  Requires framequery.h, framequery.c, common_mysql.h, common_mysql.c
 *
 * $Id: mlframequery.c,v 1.2 2005-10-21 01:54:36 charlton Exp $
 */
#include <string.h>
#include "mex.h"
#include "stdlib.h"
#include "common_mysql.h"
#include "framequery.h"

/* Define number of extra query rows to allow         */
/*   Use 5 to allow for query expansion               */
#define NUM_EXTRA_QUERY_ROWS 5;

/* Define maximum # of frame files                    */
/*  Set large enough for 12 hours at 16-sec/frame     */
#define MAX_FRAME_FILES 3000

#define GPS_STRING_SIZE 50 /* max size for GPS strings */
 void mexFunction (int nlhs, mxArray *plhs[],
                   int nrhs, const mxArray *prhs[])
{
    struct FNList fnlist;
    char *instrument;
    char *dataType;
    char site;
    double dbgps,dbdur;
    int gpsStart;
    int duration;
    int fqStatus;
    int numFiles;
    int tmpStat;
    int buflen;
    int iFile;
    int mrows,ncols;
    mxArray *fileString;
/* Note that this defines the expected minimum frame size
  for purposes of array allocation */
    const int MIN_FRAME_SEC = 16;
    int ndim = 1;
    int dims[1] = {1};
    double *numPtr;
    double *statPtr;
	double *gpsPtr;
	double *durPtr;
	double gpsVal[MAX_FRAME_FILES];
	double durVal[MAX_FRAME_FILES];
	int gpsSize;
	int durSize;
	char gpsString[GPS_STRING_SIZE];
	char durString[GPS_STRING_SIZE];
    int numFilesAlloc;

/*  IF # inputs is not 3                 */
/*      CREATE error message             */
/*      EXIT                             */
/*  ENDIF                                */
/*  IF no output arguments               */
/*      CREATE error message             */
/*      EXIT                             */
/*  ENDIF                                */
/*  IF more than 3 output arguments      */
/*      CREATE error message             */
/*      EXIT                             */
/*  ENDIF                                */
    if(nrhs != 4)
    {
/* NOTE: mexErrMsgTxt calls the MATLAB error function,
  so this forces a code exit */
        mexErrMsgTxt(" Four inputs are required");
    }
    if(nlhs < 1)
    {
        mexErrMsgTxt(" Too few output arguments");
    }
    if(nlhs > 5)
    {
        mexErrMsgTxt(" Too many output arguments");
    }
/*  IF first input (instrument) is not a string  */
/*      CREATE error message              */
/*      EXIT                              */
/*  ENDIF                                 */
/*  IF second input (type) is not a string*/
/*      CREATE error message              */
/*      EXIT                              */
/*  ENDIF                                 */
/*  IF third input is not a number        */
/*      CREATE error message              */
/*      EXIT                              */
/*  ENDIF                                 */
/*  IF fourth input is not a number       */
/*      CREATE error message              */
/*      EXIT                              */
/*  ENDIF                                 */
    if(!mxIsChar(prhs[0]))
    {
        mexErrMsgTxt("instrument is not a string");
    }
    if(!mxIsChar(prhs[1]))
    {
        mexErrMsgTxt("type is not a string");
    }
    if(!mxIsNumeric(prhs[2]))
    {
        mexErrMsgTxt("Start Time is not numeric");
    }
    if(!mxIsNumeric(prhs[3]))
    {
        mexErrMsgTxt("Duration is not numeric");
    }

/*  FIND length of instrument string (first input) */
/*  ALLOCATE storage space for it               */
/*  CREATE C string from input                  */
/*  FIND length of type string (second input)   */
/*  ALLOCATE storage space for it               */
/*  CREATE C string from input                  */
/*  IF third input is not a scalar              */
/*      CREATE error message and exit           */
/*  ENDIF                                       */
/*  SET start time = second parameter           */
/*  IF fourth input is not a scalar             */
/*      CREATE error message and exit           */
/*  ENDIF                                       */
/*  SET duration = third parameter              */

/* NOTE: in C we need to add a space for the null terminator
 in each string when we get it from MATLAB */
    buflen = mxGetN(prhs[0]) + 1;
    instrument = mxCalloc(buflen,sizeof(char));
    tmpStat = mxGetString(prhs[0],instrument,buflen);
    if(tmpStat !=0)
    {
        mexErrMsgTxt("Failure to get instrument string from input");
    }
    buflen = mxGetN(prhs[1]) + 1;
    dataType = mxCalloc(buflen,sizeof(char));
    tmpStat = mxGetString(prhs[1],dataType,buflen);
    if(tmpStat !=0)
    {
        mexErrMsgTxt("Failure to get type string from input");
    }
    mrows = mxGetM(prhs[2]);
    ncols = mxGetN(prhs[2]);
    if(!(mrows==1 && ncols==1))
    {
        mexErrMsgTxt(" Start not scalar");
    }
    dbgps = mxGetScalar(prhs[2]);
    gpsStart = (int)dbgps;

    mrows = mxGetM(prhs[3]);
    ncols = mxGetN(prhs[3]);
    if(!(mrows==1 && ncols==1))
    {
        mexErrMsgTxt(" duration not scalar");
    }
    dbdur = mxGetScalar(prhs[3]);
    duration = (int)dbdur;

/*  SET max # of frame files                        */
/*  ALLOCATE storage for file string pointers       */
/*  CREATE empty cell array for file list output    */
/*  ALLOCATE storage for GPS start string pointers  */
/*  ALLOCATE storage for duration string pointers   */
/*  IF # files is to be output                      */
/*      SET # files output = 0                      */
/*  ENDIF                                           */
/*  IF status is to be output                       */
/*      SET status output to failed                 */
/*  ENDIF                                           */
    numFilesAlloc = (int)(duration/MIN_FRAME_SEC) + NUM_EXTRA_QUERY_ROWS;
    if(numFilesAlloc > MAX_FRAME_FILES)
    {
        mexErrMsgTxt(" Duration is too long for query");
    }
    fnlist.maxfiles = numFilesAlloc;
    fnlist.namlist = mxCalloc(fnlist.maxfiles,sizeof(char**));
    fnlist.nfiles = 0;
    dims[0] = 1;
    plhs[0] = mxCreateCellArray(ndim,dims);
    fnlist.gpslist = mxCalloc(fnlist.maxfiles,sizeof(char**));
    fnlist.durlist = mxCalloc(fnlist.maxfiles,sizeof(char**));
    if (nlhs > 1)
    {
        plhs[1]= mxCreateDoubleMatrix(1,1,mxREAL);
        numPtr = mxGetPr(plhs[1]);
        numPtr[0] = (double)0;
        if (nlhs > 2)
        {
            plhs[2] = mxCreateDoubleMatrix(1,1,mxREAL);
            statPtr = mxGetPr(plhs[2]);
            statPtr[0] = (double)1;
        }
    }

/*  USE framequery to get list of frame files   */
/*  IF there is a status output argument        */
/*      SET status output = status from query   */
/*  ENDIF                                       */
/*  IF frame query was successful               */
/*      GET # of files returned from query      */
/*      IF there is a # of files output         */
/*          SET output = # of files             */
/*      ENDIF                                   */

    numFiles = 0;
    fqStatus = framequery(&fnlist,instrument,dataType,gpsStart,duration);
    if(nlhs > 2)
    {
        statPtr[0] = (double)fqStatus;
    }
    if(0 == fqStatus)
    {
        numFiles = fnlist.nfiles;
        if (nlhs > 1)
        {
            numPtr[0] = (double)numFiles;
        }

/*      IF there are files in the list                                  */
/*          CREATE Matlab cell array for first output sized to # of files*/
/*          GET pointer to the array                                    */
/*          LOOP over the files in the list                             */
/*              IF the filename pointer is not null                     */
/*                  IF the filename string is not empty                 */
/*                      CREATE Matlab string from the filename          */
/*                      SET cell of the Matlab array to the Matlab string */
/*                  ENDIF                                               */
/*                  FREE memory used for filename string                */
/*              ENDIF                                                   */
        if(numFiles > 0)
        {
            mxDestroyArray(plhs[0]);
            dims[0] = numFiles;
            plhs[0] = mxCreateCellArray(ndim,dims);
            for (iFile=0;iFile<numFiles;iFile++)
            {
                if(fnlist.namlist[iFile] != NULL)
                {
                    int strSize = strlen(fnlist.namlist[iFile]);
                    if(strSize < 1)
                    {
                        mexPrintf(" ERROR - MYSQL file %d string bad len %d\n",iFile,strSize);
                    }
                    else
                    {
                        fileString = mxCreateString(fnlist.namlist[iFile]);
                        mxSetCell(plhs[0],iFile,fileString);
                    }
                    free(fnlist.namlist[iFile]);
                }
                else
                {
                   mexPrintf(" ERROR - MySQL file %d has NULL pointer\n",iFile);
                }

/*              IF the GPS time pointer is not null                     */
/*                  IF the GPS time string is not empty                 */
/*                      CONVERT string to an integer                    */
/*                      STORE GPS time in array                         */
/*                  ELSE                                                */
/*                      STORE 0 for the GPS Time in array               */
/*                  ENDIF                                               */
/*                  FREE memory used for GPS time string                */
/*              ENDIF                                                   */
				if(fnlist.gpslist[iFile] != NULL)
                {
				    int strSize = strlen(fnlist.gpslist[iFile]);
                    if(strSize < 1)
                    {
                        mexPrintf(" ERROR - MYSQL gps %d string bad len %d\n",iFile,strSize);
                    }
                    else
                    {
						memset(gpsString,0,GPS_STRING_SIZE);
                        if(strSize > (GPS_STRING_SIZE-1))
                        {
                            mexPrintf(" ERROR - MYSQL gps %d string size %d is too long\n",iFile,strSize);
                            strSize = GPS_STRING_SIZE-1;
                        }
						memcpy(gpsString,fnlist.gpslist[iFile],strSize);
                        gpsVal[iFile] = atoi(gpsString);
					}
                    free(fnlist.gpslist[iFile]);
                }
                else
                {
                   mexPrintf(" ERROR - MySQL gps %d has NULL pointer\n",iFile);
                   gpsVal[iFile] = 0;
				}

/*              IF the duration pointer is not null                     */
/*                  IF the duration string is not empty                 */
/*                      CONVERT string to an integer                    */
/*                      STORE duration in array                         */
/*                  ELSE                                                */
/*                      STORE 0 for the duration in array               */
/*                  ENDIF                                               */
/*                  FREE memory used for duration string                */
/*              ENDIF                                                   */
/*          END LOOP                                                    */
				if(fnlist.durlist[iFile] != NULL)
                {
				    int strSize = strlen(fnlist.durlist[iFile]);
                    if(strSize < 1)
                    {
                        mexPrintf(" ERROR - MYSQL dur %d string bad len %d\n",iFile,strSize);
                    }
                    else
                    {
                        memset(durString,0,GPS_STRING_SIZE);
                        if(strSize > (GPS_STRING_SIZE-1))
                        {
                            mexPrintf(" ERROR - MYSQL dur %d string size %d is too long\n",iFile,strSize);
                            strSize = GPS_STRING_SIZE-1;
                        }
						memcpy(durString,fnlist.durlist[iFile],strSize);
                        durVal[iFile] = atoi(durString);
                    }
                    free(fnlist.durlist[iFile]);
                }
                else
                {
                   mexPrintf(" ERROR - MySQL dur %d has NULL pointer\n",iFile);
                   durVal[iFile] = 0;
				}
			}
        }
    }
    else
    {
        mexPrintf("ERROR - framequery reports error %d\n",fqStatus);
    }

/*  IF GPS start times are to be output                 */
/*      CREATE empty array for GPS start times          */
/*      SET GPS output array to internal GPS array      */
/*  ENDIF                                               */
/*  IF frame lengths are to be output                   */
/*      CREATE empty array for frame durations          */
/*      SET frame duration output to duration array     */
/*  ENDIF                                               */
	if (nlhs > 3)
	{
		if(numFiles > 0)
        {
            plhs[3]= mxCreateDoubleMatrix(numFiles,1,mxREAL);
            gpsPtr = mxGetPr(plhs[3]);
		    gpsSize = mxGetElementSize(plhs[3]);
	        memcpy(gpsPtr,&gpsVal[0],gpsSize*numFiles);
        }
        else
        {
            plhs[3]= mxCreateDoubleMatrix(1,1,mxREAL);
        }
        if (nlhs > 4)
        {
            if(numFiles > 0)
            {
                plhs[4] = mxCreateDoubleMatrix(numFiles,1,mxREAL);
                durPtr = mxGetPr(plhs[4]);
                durSize = mxGetElementSize(plhs[4]);
		        memcpy(durPtr,&durVal[0],durSize*numFiles);
            }
            else
            {
                plhs[4]= mxCreateDoubleMatrix(1,1,mxREAL);
            }
        }
    }
}
