/*-------------------------------------------------------------------------
 *
 * File name: framequery.h
 *
 * Authors: M. Foster, K. Thorne
 *
 * Revision: $Id: framequery.h,v 1.1 2004-08-11 18:43:34 kathorne Exp $
 *
 *-------------------------------------------------------------------------
 *
 * NAME
 * framequery.h
 *
 * SYNPOSIS
 * #include "framequery.h"
 *
 * DESCRIPTION
 * Declares the prototypes for framequery.c
 *
 * DEPENDENCIES
 *  common_mysql.h - needs FNList structure definition
 *-------------------------------------------------------------------------
 */

#ifndef _FRAMEQUERY_H_
#define _FRAMEQUERY_H_

#ifndef _COMMON_MYSQL_H_
#include "common_mysql.h"
#define _COMMON_MYSQL_H_
#endif

static char *FRAMEQUERYH = "$Id: framequery.h,v 1.1 2004-08-11 18:43:34 kathorne Exp $";

/*-------------------------------------------------------------------------
 *  default mySQL connection parameters
 * --> USED if following environment variables are not defined
 *	    LDR_SQL_HOST host name i.e. machine.gravity.psu.edu
 *      LDR_SQL_USER user name i.e. mysqlusr
 *	    LDR_SQL_PWD  password  i.e. PaSsWoRd
 *      LDR_METADATA metadata table i.e. ldr_table_PSU_HYDRA
 *      LDR_SQL_PORT port #    i.e. 0
 *-------------------------------------------------------------------------
 */

#define DEF_HOST_NAME   "hydra.gravity.psu.edu" /* host to connect to (default = localhost) */
#define DEF_USER_NAME   "gravityuser" 	/* user name (readonly user with access only to lrc1000 is specified here) */
#define DEF_PASSWORD    "weightless" 	/* password (default = none) */
#define DEF_METADATA    "ldr_table_PSU_HYDRA" /* table with metadata */
#define DEF_DB_NAME     "lrc1000"   /* LDR database with filename tables */
#define DEF_PORT_NUM    0    		    /* use default port */
#define DEF_SOCKET_NAME NULL 		    /* use default socket name */

/*-------------------------------------------------------------------------
 * default metadata values
 *-------------------------------------------------------------------------
 */
#define DEF_INSTRUMENT  "H"             /* default instrument - Hanford IFO */
#define DEF_DATA_TYPE   "RDS_R_L1"      /* default Interferometer data type */

/*-------------------------------------------------------------------------
 * string buffer sizes
 *-------------------------------------------------------------------------
 */
#define MAIN_QUERY_SIZE 1024
#define SUB_QUERY_SIZE 100
#define STATIC_QUERY_SIZE 512
#define FIELD_STRING_SIZE 50
#define LDR_STRING_SIZE 50
#define PORT_STRING_SIZE 10

/*-------------------------------------------------------------------------
 *  define search window jump factor
 *      Needs to be large enough to ensure that we expand
 *      beyond the frame size
 *   For now choose 21 seconds
 *		   (this covers standard 16-second frames and the
 *         640-second MDC frames <assumming 600-second
 *		   duration request for MCD playground>	)
 *-------------------------------------------------------------------------
 */
#define GPS_JUMP_FACTOR 21 /* # of seconds to stretch search window */

/*
/*------------------------------------------------------------------------_
 * Function declarations
 *-------------------------------------------------------------------------
 */

int framequery(struct FNList *fnlist, char* inputInstrument, char* inputType, int gpsFirst, int duration);

#endif
