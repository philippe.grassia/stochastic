/*-------------------------------------------------------------------------
 *
 * File Name: framequery.c
 *
 * Author: M. Foster, K. Thorne
 *
 * Revision: $Id: framequery.c,v 1.1 2004-08-11 18:43:34 kathorne Exp $
 *
 *-------------------------------------------------------------------------
 *
 * NAME
 * framequery
 *
 * SYNOPSIS
 * int framequery(struct FNList *fnlist, char* instrument, char* type, int gpsFirst, int duration)
 *
 * DESCRIPTION
 * returns a list of file names (in URL format) which contain data from
 * the given IGWD site in the given GPS range
 * arguments:
 *   FNList* fnlist
 *     on-call: NULL pointer to namlist,gpslist,dur OR non-NULL and maxfiles > 0
 *     on-rtn:  structure FNList with result in namlist,gpslist,durlist,nfiles
 *
 *   char instrument - Detector IFO data site
 *     on-call: 'H' for Hanford 'L' for Livingston, 'HL' for both
 *
 *   char type - type of interferometer data
 *     on-call: 'RDS_R_L1','R','SG5', etc.
 *
 *   int gpsFirst
 *     on-call: start time of query range in GPS seconds
 *
 *   int duration
 *     on-call: duration of query range in GPS seconds
 *
 * The range is interpreted as [gpsStart, gpsStart+duration): i.e.,
 * we do not include the file that covers the particular GPS
 * second gpsStart+duration
 *
 * GLOBALS
 *   Environment variables can be used to set the SQL host, user, password and port
 *	LDR_SQL_HOST host name i.e. machine.gravity.psu.edu
 *      LDR_SQL_USER user name i.e. mysqlusr
 *	LDR_SQL_PWD  password  i.e. PaSsWoRd
 *      LDR_SQL_PORT port #    i.e. 0
 *      LDR_METADATA metadata table i.e. ldr_table_PSU_HYDRA
 *	 -- defaults are used if any of these are not defined
 *
 * DIAGNOSTICS
 *   Returns 0 if available files cover the entire range
 *      1 - database connection failed
 *      2 - no files returned
 *      3 - mysql query failed in process_query
 *      4 - problem processing result set in process_query
 *      5 - query returned no data in process_query
 *      8 - malloc failure on namlist in process_result_set
 *      9 - malloc failure on gpslist in process_result_set
 *      10 - malloc failure on durlist in process_result_set
 *      11 - query returned too many rows in process_result_set
 *      12 - malloc failure on fileptr in process_result_set
 *      13 - malloc failure on gpsptr in process_result_set
 *      14 - malloc failure on durptr in process_result_set
 *
 * CALLS
 *   do_connect     - in common_mysql.c
 *   process_query  - in common_mysql.c
 *   do_disconnnect - in common_mysql.c
 *
 * NOTES
 *   09/29/2003 K. Thorne Version 1.3 - Removed test for coverage as
 *                                      query does not correctly parse
 *                                      file list
 *   11/16/2003 K. Thorne - Added LDR_SQL_* environment variables
 *   12/04/2003 K. Thorne Changed to use metadata in query
 *                        Added metadata table environment variable
 *   12/05/2003 K. Thorne Add type input, add GPS start, duration to output
 *   03/04/2004 K. Thorne Simplify SQL statement to make use of indices
 *	 04/16/2004 K. Thorne Make second indexed query using expanded
 *							frame start/stop times
 *   05/21/2004 K. Thorne Add fix for UWM metadata
 *   07/29/2004 K. Thorne Move defines, prototype to framequery.h
 *                        Add comments on LDR tables
 *-------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common_mysql.h"
#include "framequery.h"

MYSQL   *conn;  /* pointer to connection handler */

int framequery(struct FNList *fnlist, char* inputInstrument, char* inputType, int gpsFirst, int duration)
{
    int gpsLast,nfiles;
    float intervalsize;
    char staticbuf[STATIC_QUERY_SIZE];
    char queryAbuf[SUB_QUERY_SIZE];
    char queryBbuf[SUB_QUERY_SIZE];
    char queryCbuf[SUB_QUERY_SIZE];
	char queryDbuf[SUB_QUERY_SIZE];
	char queryEbuf[SUB_QUERY_SIZE];
    char querybuf[MAIN_QUERY_SIZE];
    char addbuf[SUB_QUERY_SIZE];
    char dataInstrument[FIELD_STRING_SIZE] = "";
    char dataType[FIELD_STRING_SIZE] = "";
    char sql_host_name[LDR_STRING_SIZE] = "";
    char sql_user_name[LDR_STRING_SIZE] = "";
    char sql_password[LDR_STRING_SIZE] = "";
    char str_port[PORT_STRING_SIZE] = "";
    unsigned int sql_port_num = 0;
    char ldr_metadata[LDR_STRING_SIZE] = "";
    char* envptr;
    char* uwmmatch;
    char uwmsql[10] = "UWM_LSC";
    int status1 = 0;
    int status2 = 0;
	int status3 = 0;
	int tmpFirst = gpsFirst;
	int tmpLast = gpsLast;

/*  IF Host Name environment variable						*/
/*     SET SQL host to that							        */
/*  ELSE									                */
/*     SET SQL host to default							    */
/*  END										                */
/*  IF User Name environment variable						*/
/* 		SET SQL user to that								*/
/* 	ELSE													*/
/* 		SET SQL user to default								*/
/*  END														*/
/*  IF Password environment variable						*/
/* 		SET SQL password to that							*/
/* 	ELSE													*/
/* 		SET SQL password to default							*/
/*  END														*/
    envptr = getenv("LDR_SQL_HOST");
    if(envptr != NULL)
    {
        strncpy(sql_host_name,envptr,sizeof(sql_host_name));
    }
    else
    {
        strncpy(sql_host_name,DEF_HOST_NAME,sizeof(sql_host_name));
    }
    sql_host_name[sizeof(sql_host_name)-1] = '\0';

    envptr = getenv("LDR_SQL_USER");
    if(envptr != NULL)
    {
        strncpy(sql_user_name,envptr,sizeof(sql_user_name));
    }
    else
    {
        strncpy(sql_user_name,DEF_USER_NAME,sizeof(sql_user_name));
    }
    sql_user_name[sizeof(sql_user_name)-1] = '\0';

    envptr = getenv("LDR_SQL_PWD");
    if(envptr != NULL)
    {
        strncpy(sql_password,envptr,sizeof(sql_password));
    }
    else
    {
        strncpy(sql_password,DEF_PASSWORD,sizeof(sql_password));
    }
    sql_password[sizeof(sql_password)-1] = '\0';

/*  IF Port environment variable							*/
/* 		SET SQL port to that								*/
/* 	ELSE													*/
/* 		SET SQLport to default								*/
/*  END														*/
/*  IF Metadata table environment variable                  */
/*  	SET Metadata table to that                          */
/*  ELSE                                                    */
/* 	SET Metadata table to default                           */
/*  END                                                     */
    envptr = getenv("LDR_SQL_PORT");
    if(envptr != NULL)
    {
        strncpy(str_port,envptr,sizeof(str_port));
        str_port[sizeof(str_port)-1] = '\0';
        sql_port_num = atoi(str_port);
    }
    else
    {
        sql_port_num = DEF_PORT_NUM;
    }
    envptr = getenv("LDR_METADATA");
    if(envptr != NULL)
    {
        strncpy(ldr_metadata,envptr,sizeof(ldr_metadata));
    }
    else
    {
        strncpy(ldr_metadata,DEF_METADATA,sizeof(ldr_metadata));
    }
    ldr_metadata[sizeof(ldr_metadata)-1] = '\0';

/*  IF input instrument is not NULL                         */
/*      SET instrument to input string                      */
/*  ELSE                                                    */
/*      SET instrument = default                            */
/*  END                                                     */
/*  IF input type is not NULL                               */
/*      SET type to input string                            */
/*  ELSE                                                    */
/*      SET type = default                                  */
/*  END                                                     */
    if(inputInstrument != NULL)
    {
        strncpy(dataInstrument,inputInstrument,sizeof(dataInstrument));
    }
    else
    {
        strncpy(dataInstrument,DEF_INSTRUMENT,sizeof(dataInstrument));
    }

    if(inputType != NULL)
    {
        strncpy(dataType,inputType,sizeof(dataType));
    }
    else
    {
        strncpy(dataType,DEF_DATA_TYPE,sizeof(dataType));
    }

/*---------------------------------------------------------------------
 * LDR Table Configuration for query
 *  The SQL query assumes the following database tables in LDR
 *
 *  database lrc1000
 *      table t_pfn  - Physical file names
 *          t_pfn.id - primary key for physical file name records
 *          t_pfn.name - physical file name
 *
 *      table t_map  - maps logical file names to physical file names
 *          t_map.pfn_id - primary key in t_pfn
 *          t_map.lfn_id - primary key in t_lfn
 *
 *      table t_lfn - logical file names
 *          t_lfn.id - primary key for logical file name records
 *          t_lfn.name - logical file name
 *
 *  NOTE:  There are multiple physical file name records for each
 *  logical file name.  One is the Globus URL, another is the local
 *  file path.  The query adds the requirement that the physical file name
 *  start with 'file' to only keep local file paths.
 *
 *  database lfnMetadataCatalog
 *      table LDR_METADATA (set by environment variable)
 *          LDR_METADATA.LDRlfn - logical file name
 *          LDR_METADATA.instrument - interferometer sites in file (H, L , HL)
 *          LDR_METADATA.frameType - frame type (RDS_R_L1, SG10)
 *          LDR_METADATA.gpsStart - start time of frame file (GPS seconds)
 *          LDR_METADATA.gpsEnd   - end time of frame file (GPS seconds)
 *
 *   The SQL requires joins to get from the metadata to the physical file name
 *          match LDR_METADATA.LDRlfn to t_lfn.name
 *          match t_lfn.if to t_map.lfn_id
 *          match t_map.pfn_id to t_pfn.id
 *
 *  The metadata is searched using gpsStart and gpsEnd
 *---------------------------------------------------------------------
 */

/*  INITIALIZE number of files returned                     */
/*  SET end of GPS range = beginning + duration             */
/*  BUILD static part of query string                       */
/*        must have intrument = data instrument             */
/*        and type = data type                              */
/*        and file name starting with 'file'                */
/*    ** If UWM require 'file://local' to get the right one */
/*     let gpsFirst, gpsLast are GPS range to cover         */
/*     use frame metadata gpsStart,gpsEnd                   */
    fnlist->nfiles = 0;
    gpsLast = gpsFirst + duration;
    strcpy(staticbuf,"SELECT P.name, T.gpsStart, T.duration ");
    sprintf(addbuf,"FROM lfnMetadataCatalog.%s T, ",ldr_metadata);
    strcat(staticbuf,addbuf);
    strcat(staticbuf,"lrc1000.t_pfn P, lrc1000.t_lfn L, lrc1000.t_map M ");
    sprintf(addbuf," WHERE T.instrument = \'%s\'",dataInstrument);
    strcat(staticbuf,addbuf);
    sprintf(addbuf," AND T.frameType = \'%s\'",dataType);
    strcat(staticbuf,addbuf);
    uwmmatch = strstr(ldr_metadata,uwmsql);
    if(NULL == uwmmatch)
    {
        strcpy(addbuf," AND LEFT(P.name,4)=\'file\'");
    }
    else
    {
        strcpy(addbuf," AND LEFT(P.name,12)=\'file://local\'");
    }
/* DEBUG    printf(" file string=%s\n",addbuf); */
    strcat(staticbuf,addbuf);
    strcat(staticbuf," AND T.LDRlfn = L.name AND L.id = M.lfn_id AND P.id = M.pfn_id");

/*  BUILD first query as union of two queries  which make   */
/*     use of indexes on gpsStart and gpsEnd                */
/*      Query = QueryA Union QueryB                         */
/*     QueryA = gpsStart >= gpsFirst AND gpsStart < gpsLast */
/*     QueryB = gpsEnd > gpsFirst AND gpsEnd <= gpsLast     */
    sprintf(queryAbuf," AND ((T.gpsStart >= '%d' ) AND (T.gpsStart < '%d'))",
                gpsFirst, gpsLast);
    sprintf(queryBbuf," AND ((T.gpsEnd > '%d' ) AND (T.gpsEnd <= '%d'))",
                gpsFirst, gpsLast);
    sprintf(querybuf,"(");
    strcat(querybuf,staticbuf);
    strcat(querybuf,queryAbuf);
    strcat(querybuf,") UNION (");
    strcat(querybuf,staticbuf);
    strcat(querybuf,queryBbuf);
    strcat(querybuf,")");
/* DEBUG	printf(" first querybuf=%s\n",querybuf);        */

/*  USE do_connect to connect to mySQL database             */
/*  IF connection fails                                     */
/*      RETURN indicating failure                           */
/*  ENDIF                                                   */
/*  USE process_query to get list of frame files from       */
/*          first query                                     */
/*  GET # of files in list from first query                 */
/*  IF there are NO records from first query                */
/*      CLEAR returned list                                 */
/*      BUILD new query which increases search window       */
/*          to ensure a frame boundary is crossed           */
/*			specifically decrease gpsFirst by jump factor   */
/*			 and increase gpsLast by same amount			*/
/*		USE process query to get list of frame files from   */
/*				second query								*/
/*		GET # of files in list from second query			*/
    conn = do_connect (sql_host_name, sql_user_name, sql_password,
              DEF_DB_NAME,sql_port_num, DEF_SOCKET_NAME, 0);
    if (conn == NULL)
    {
        nfiles = 0;
        return (1);
    }
    status1 = process_query(conn, querybuf, fnlist);
    nfiles=fnlist->nfiles;
	if (nfiles == 0)
	{
		tmpFirst = gpsFirst - GPS_JUMP_FACTOR;
		tmpLast = gpsLast + GPS_JUMP_FACTOR;
		sprintf(queryDbuf," AND ((T.gpsStart >= '%d' ) AND (T.gpsStart < '%d'))",
                tmpFirst, tmpLast);
		sprintf(queryEbuf," AND ((T.gpsEnd > '%d' ) AND (T.gpsEnd <= '%d'))",
                tmpFirst, tmpLast);
		sprintf(querybuf,"(");
		strcat(querybuf,staticbuf);
		strcat(querybuf,queryDbuf);
		strcat(querybuf,") UNION (");
		strcat(querybuf,staticbuf);
		strcat(querybuf,queryEbuf);
		strcat(querybuf,")");
/* DEBUG        printf(" second querybuf=%s\n",querybuf);   */
		status2 = process_query(conn,querybuf,fnlist);
		nfiles=fnlist->nfiles;

/*		IF there are NO records from second query			*/
/*			CLEAR returned list								*/
/*			BUILD simple query which does not use indexing  */
/*      		QueryC = (gpsFirst < gpsEnd) AND            */
/*           			 (gpsStart < gpsLast)               */
/*      	USE process_query to get list of frame files    */
/*          	from third query                            */
/*			GET # of files from third query					*/
/*		ENDIF												*/
/*  ENDIF                                                   */
/*  USE do_disconnect to disconnect from mySQL database     */
		if (nfiles == 0)
		{
			sprintf(queryCbuf," AND ((T.gpsStart < '%d' ) AND (T.gpsEnd > '%d'))",
                gpsFirst, gpsLast);
			sprintf(querybuf,"%s",staticbuf);
			strcat(querybuf,queryCbuf);
/* DEBUG       printf(" third querybuf=%s\n",querybuf);     */
			status3 = process_query(conn, querybuf, fnlist);
			nfiles=fnlist->nfiles;
		}
	}
    do_disconnect (conn);

/*  IF # of files = 0                                       */
/*      RETURN indicating failure                           */
/*  ENDIF                                                   */
    if (nfiles == 0)
    {
        return (2);
    }
    else
    {
        if(status1 > 0)
        {
            return(status1);
        }
        if(status2 > 0)
        {
            return(status2);
        }
		if(status3 > 0)
		{
			return(status3);
		}
        return(0);
    }
}
