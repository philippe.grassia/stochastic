function [subDir,frameGps] = getframesubdir(frameFile,numDigitDrop)
% GETFRAMESUBDIR - determine GPS-indexed sub-directory for frame file
%   [subDir,frameGps] = getframesubdir(frameFile[,numDigitDrop])
%       frameFile - name of frame file
%       numDigitDrop - [OPTIONAL] # of GPS digits to drop for sub-directory
%           <default = 5>
%
%       subDir - sub-directory
%       frameGps - GPS of frame file
%
%    We follow the convention 
%       frameFile =<obs>-<type>-123456789-<dur>.gwf
%
%    sub-directory = <obs>-<type>-1234 for numDigitDrop = 5
%
%   -- we use digits to drop to support move to 10-digit GPS
%
%   WARNINGS
%       getframesubdir:badName
%
%   $Id: getframesubdir.m,v 1.2 2006-08-22 21:00:20 kathorne Exp $

% IF # of GPS digits not input
%   SET to default
% ELSE
%   IF input # of digits bad
%       ISSUE warning
%       SET to default
%   ENDIF
% ENDIF
% GET location of dashes in frame file name
% IF not enough dashes (i.e. < 3)
%   ISSUE warning
%   SET sub-directory to default
%   EXIT
% ENDIF
% GET GPS digits in frame file name
% IF wrong number of digits (i.e. < 9)
%   ISSUE warning 
%   SET sub-directory to default
%   EXIT
% ENDIF
% CREATE sub-directory by cutting frame file name at 
%       correct GPS digit position
subDir = '.';
frameGps = [];
error(nargchk(1,2,nargin),'struct');
DEF_DIGIT = 5;
if(nargin < 2)
    numDigitDrop = DEF_DIGIT;
else
    if(~isnumeric(numDigitDrop))
        numDigitDrop = DEF_DIGIT;
    end
end
if(iscell(frameFile))
    frameFile = char(frameFile);
end
if(~ischar(frameFile))
    msgId = 'getframesubdir:badName';
    warning(msgId,'%s: frame file name is not a string',msgId);
    subDir = '.';
    return
end
dashPos = strfind(frameFile,'-');
numDash = numel(dashPos);
if(numDash < 3)
    msgId = 'getframesubdir:badName';
    warning(msgId,'%s: frame file name %s has wrong format',msgId,frameFile);
    subDir = '.';
    return
end
begGps = dashPos(numDash-1)+1;
endGps = dashPos(numDash)-1;
numGps = (endGps-begGps)+1;
gpsStr = frameFile(begGps:endGps);
gpsInt = str2num(gpsStr);
if(numGps < 9 || ~isnumeric(gpsInt))    
    msgId = 'getframesubdir:badName';
    warning(msgId,'%s: frame file name %s has bad GPS %s',...
        msgId,frameFile,gpsStr);
    subDir = '.';
    return    
end
frameGps = gpsInt;
subEndPos = dashPos(numDash) - numDigitDrop -1;
subDir = frameFile(1:subEndPos);
return
end
