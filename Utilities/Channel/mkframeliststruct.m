function frameStruct = mkframeliststruct(gpsTimes,frameFiles,...
    listError,frameDurs)
% MKFRAMELISTSTRUCT Create structure for Channel framelist data
%   frameStruct = mkframeliststruct(gpsTimes,framefiles,...
%                                   listError,frameDurs)
%
%   Packages the 'framelist' output into a single structure
%       to simplify transport
%
%   Fields
%       'gpsTimes' - array of GPS start times of frame files
%       'frameFiles' - cell-array of frame file paths
%       'listError' - error code from framelist call
%       'frameDurs' - array of duration of frame files
%
%  $Id: mkframeliststruct.m,v 1.1 2005-09-21 19:18:00 kathorne Exp $

error(nargchk(4,4,nargin),'struct');
frameStruct = struct('gpsTimes',[],...
    'frameFiles',[],...
    'listError',listError,...
    'frameDurs',[]);

frameStruct.gpsTimes = gpsTimes;
frameStruct.frameFiles = frameFiles;
frameStruct.frameDurs = frameDurs;
return
