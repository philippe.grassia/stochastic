function chanObj = Channel(chanName,startTime)
%CHANNEL - Channel class contructor
%  chanObj = Channel(chanName[,startTime]) creates an IGWD Channel object using
%         the channel name 'chanName' 
%     The syntax of valid channel names (i.e. H2:LSC-AS_Q) 
%     Xn:Name X is site (H Hanford, L Livingston, V Virgo)
%             n is detector number (0 for environment monitoring)
%             Name is detector channel name, usually a location and
%                   signal type
%  'startTime' [OPTIONAL] Set a base GPS Start time for TimeSeries objects 
%               pulled from the channel
%
% $Id: Channel.m,v 1.1 2004-08-11 18:45:12 kathorne Exp $

% IF no input arguments or more than 2
%   SET staus code =  1
%   SET string properties to empty strings
%   SET start time = 0
%   SET rate = 0
%   CREATE object
% ELSE 
%   IF first argument is a Channel object
%       SET new object to it
%   ELSE
%       SET name from first argument
%       SET default site
%       SET instrument from name
%       SET default type
%       SET start time = 0
%       SET rate = 0
%       IF there is a second argument
%           IF the startTime is valid (> 0)
%               SET start time to arguement value
%           ENDIF
%       ENDIF
%       IF name has correct syntax
%           SET status code = 0
%       ELSE
%           SET status code = 1
%       ENDIF
%       CREATE new Channel object
%   ENDIF
% ENDIF
if (nargin == 0 | nargin > 2)
    newObj.name = '';
    newObj.statusCode = 1;
    newObj.site = '';
    newObj.instrument = '';
    newObj.type = '';
    newObj.gpsStart = 0;
    newObj.rate = 0;
    chanObj = class(newObj,'Channel');
else
%  NOTE: this code is required byt MATLAB to support a Channel
%    call using an already-defined Channel object
    if isa(chanName,'Channel')
        newObj = chanName;
    else
        newObj.name = chanName;
        if(isletter(chanName(1:1)) & ~isletter(chanName(2:2)) & chanName(3:3) == ':')
            newObj.statusCode = 0;
        else
            newObj.statusCode = 1;
        end
        newObj.site = 'LDR';
        newObj.instrument = chanName(1);
        newObj.type = 'RDS_R_L1';
        if (nargin == 2)
            if (startTime > 0)
                newObj.gpsStart = startTime;
            else
                newObj.gpsStart = 0;
            end
        else
            newObj.gpsStart = 0;
        end
        newObj.rate = 0;
        chanObj = class(newObj,'Channel');
    end
end
return