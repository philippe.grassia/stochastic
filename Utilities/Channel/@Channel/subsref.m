function vector = subsref(chanObj,index)
% SUBSREF return sub-section,field of Channel object
% vector = chanObj(gpsStartSec,durationSec)
%    vector is time-series starting at gpsStartSec
%       with a duration in durationSec.
%
% vector = chanObj(gpsStartSec:gpsEndSec)
%    vector is time-series starting at gpsStartSec
%       and ending just before gpsEndSec.
%
% value = chanObj.'field'
%       returns value of field for Channel object
%
% $Id: subsref.m,v 1.1 2004-08-11 18:45:12 kathorne Exp $

% INITIALIZE outputs
% SWITCH on reference type
%   CASE reference type is ()
%       CLEAR GPS range flag
%       IF arguments are numeric
%           IF # rows in first entry = 1 (indicating #,#)
%               IF there is a second numeric entry
%                   SET gpsOffsetSec to first entry
%                   SET durationSec to second entry
%                   SET GPS range flag
%               ENDIF
%           ELSE
%               GET vector from first entry
%               SET gpsOffsetSec to 1st element of input vector
%               SET gpsEndSec to last element of input vector
%               SET durationSec = gpsEndSec - gpsOffsetSec
%               ENDIF
%           ENDIF
vector = [];
sampRate = 0;
vectorError = 42;
switch index.type
    case'()'
        isGpsRange = false;
        [inrows,numInputs] = size(index.subs);
        if(numInputs < 1)
            error(' need GPS range inputs');
        end
        if(isa(index.subs{1},'numeric'))
            [mrows,ncols] = size(index.subs{1});
            if(ncols == 1)
                if(numInputs > 1)
                    if(isa(index.subs{2},'numeric'))
                        gpsOffsetSec = index.subs{1};
                        durationSec = index.subs{2};
                        isGpsRange = true;
                    end
                end
            else
                gpsVect = index.subs{1};
                gpsOffsetSec = gpsVect(1);
                gpsEndSec = gpsVect(ncols);
                durationSec = gpsEndSec - gpsOffsetSec;
                isGpsRange = true;
            end
        end

%       IF gps Range flag not set
%           FLAG as error
%           return
%       END
%       IF duration is <= 0
%           FLAG as error
%           return
%       ENDIF
%       SET start time = channel start time + GPS offset
%       USE framelist function to get list of frame files covering
%                     start time to (start time + duration)
        if(isGpsRange ~= true)
            vectorError = 44;
            error('Bad GPS range format');
        end
        if(durationSec <= 0)
            vectorError = 44;
            error('GPS range duration is <= 0');
        end
        startTime = chanObj.gpsStart + gpsOffsetSec;
        name = chanObj.name;
        site = chanObj.site;
        instrument = chanObj.instrument;
        type = chanObj.type;
        rate = chanObj.rate;
        [gpsTimeTbl,frameTbl,listError,durTbl] =...
            framelist(instrument,type,startTime,durationSec);

%       IF framelist was not successful
%           flag as error
%       ENDIF
%       Use fetchseries to create series from file list
%       SET channel rate to frame rate
%       IF fetchseries was not successful
%           flag as error
%       ENDIF
        vectorError = listError;
        if(listError ~= 0)
            msgId = 'Channel:badframelist';
            errStr = sprintf('Channel: Error %d in frame file list',listError);
            warning(msgId,errStr);
        end
        [vector,trueRate,seriesError] = fetchseries(name,rate,...
            startTime,durationSec,gpsTimeTbl,frameTbl,durTbl);
        sampRate = trueRate;
        if(seriesError ~= 0)
            msgId = 'Channel:badtimeseries';
            errStr = sprintf('Channel: Error %d in retrieved time-series!\n',seriesError);
            if(0 == vectorError)
                vectorError = seriesError + 30;
            end
            warning(msgId,errStr);
        end

%   CASE '.' syntax
%       SWITCH on field name
%           CASE name
%               SET output to name of channel
%           CASE site
%               SET output to site of date (LDR, etc.)
%           CASE type
%               SET output to type of channel
%           CASE type
%               SET output to type of channel
%           CASE gpsStart
%               SET output to GPS Start value of channel
%           CASE rate
%               SET output to rate value of channel
%           OTHERWISE
%               FLAG as error
%       END SWITCH
    case '.'
        switch index.subs
            case 'name'
                vector = chanObj.name;
            case 'statusCode'
                vector = chanObj.statusCode;
            case 'site'
                vector = chanObj.site;
            case 'instrument'
                vector = chanObj.instrument;
            case 'type'
                vector = chanObj.type;
            case 'gpsStart'
                vector = chanObj.gpsStart;
            case 'rate'
                vector = chanObj.rate;
            otherwise
                warning('Not a Channel property');
        end

%   ELSE (input not the right syntax)
%       FLAG as error
% END SWITCH
    otherwise
        warning('Subscript out of range');
end
return
