function chanObj = set(chanObj,varargin)
% SET Set Channel property and return updated object
propArgin = varargin;
readonlyId = 'Channel:readonly';
badtypeId = 'Channel:badtype';
badpropId = 'Channel:badprop';
while length(propArgin) >= 2,
    chanProp = propArgin{1};
    propVal = propArgin{2};
    propArgin = propArgin(3:end);
    switch chanProp
        case 'name'
            msgTxt = 'name is read-only';
            warning(readonlyId,msgTxt);
        case 'statusCode'
            msgTxt = 'name is read-only';
            warning(readonlyId,msgTxt);
        case 'rate'
            if(isa(propVal,'numeric'))
                chanObj.rate = propVal;
            else
                msgTxt = 'rate must be numeric';
                warning(badtypeId,msgTxt);
            end
        case 'site'
            if(isa(propVal,'char'))
                chanObj.site = propVal;
            else
                msgTxt = 'site must be character';
                warning(badtypeId,msgTxt);
            end
        case 'instrument'
            if(isa(propVal,'char'))
                chanObj.instrument = propVal;
             else
                msgTxt = 'instrument must be character';
                warning(badtypeId,msgTxt);
            end
        case 'type'
            if(isa(propVal,'char'))
                chanObj.type = propVal;
            else
                msgTxt = 'type must be character';
                warning(badtypeId,msgTxt);
            end
       case 'gpsStart'
            if(isa(propVal,'numeric'))
                chanObj.gpsStart = propVal;
            else
                msgTxt = 'gpsStart must be numeric';
                warning(badtypeId,msgTxt);
            end
        otherwise
            msgTxt = 'set-able Channel properties: rate,site,instrument,type,gpsStart';
            warning(badpropId,msgTxt);
    end
end
return
