function chanObj = subsasgn(chanObj,index,propVal)
% SUBSASGN Define index assignment for Channel objects
readonlyId = 'Channel:readonly';
badtypeId = 'Channel:badtype';
badpropId = 'Channel:badprop';
switch index.type
case'.'
    switch index.subs
        case 'name'
            msgTxt = 'name is read-only';
            warning(readonlyId,msgTxt);
        case 'statusCode'
            msgTxt = 'name is read-only';
            warning(readonlyId,msgTxt);
        case 'rate'
            if(isa(propVal,'numeric'))
                chanObj.rate = propVal;
            else
                msgTxt = 'rate must be numeric';
                warning(badtypeId,msgTxt);
            end
        case 'site'
            if(isa(propVal,'char'))
                chanObj.site = propVal;
            else
                msgTxt = 'site must be character';
                warning(badtypeId,msgTxt);
            end
        case 'instrument'
            if(isa(propVal,'char'))
                chanObj.instrument = propVal;
             else
                msgTxt = 'instrument must be character';
                warning(badtypeId,msgTxt);
            end
        case 'type'
            if(isa(propVal,'char'))
                chanObj.type = propVal;
            else
                msgTxt = 'type must be character';
                warning(badtypeId,msgTxt);
            end
        case 'gpsStart'
            if(isa(propVal,'numeric'))
                chanObj.gpsStart = propVal;
            else
                msgTxt = 'gpsStart must be numeric';
                warning(badtypeId,msgTxt);
            end
        otherwise
            msgTxt = 'set-able Channel properties: rate,site,instrument,type,gpsStart';
            warning(badpropId,msgTxt);
    end
otherwise
    warning(' Invalid Channel ref');
end
return
