function [structOrGpsTimes,frameFiles,frameDurs,frameSizes]=...
    dir2framelist(dirPath,fileSite,fileType)
% DIR2FRAMELIST - create Channel framelist from frame file directory
%   [frameListStruct]=dir2framelist(dirPath[,fileSite,fileType])
%   [gpsTimes,frameFiles,frameDurs,frameSizes]=...
%       dir2framelist(dirPath[,fileSite,fileType])
%
%       dirPath - directory path (** WILL SEARCH ALL SUB-DIRECTORIES!!)
%       fileSite - [OPTIONAL] - 'site' in frame file name
%       fileType - [OPTIONAL] - 'type' in frame file name
%
%   Frame files are assumed to be <site>-<type>-GPStime-duration.gwf
%
%   gpsTimes  - vector of gps start times
%   frameFiles  - cell-array of paths to corresponding frames
%   frameDurs  - vector of frame file durations
%   frameSizes - vector of frame files sizes (in bytes)
%
%   These can be retrieved individually (as 2-3 outputs) or as a 
%       single structure (1 output)
%
%   error/warning messages
%       dir2framelist:badDir
%       dir2framelist:badName
%       dir2framelist:noFiles
%
% $Id: dir2framelist.m,v 1.7 2007-05-01 12:25:35 kathorne Exp $

% IF no info on frame file site and type input
%   SET frame file search to only restrict to frame files.
% ELSEIF only info on frame file site input
%   SET frame file search to have site defined
% ELSE
%   SET frame file search to have site,type defined
% ENDIF
% IF directory does not exist
%   CREATE error message
%   EXIT
% ENDIF
error(nargchk(1,3,nargin,'struct'));

structOrGpsTtimes = [];
gpsTimes = [];
frameFiles = [];
frameDurs = [];
frameSizes = [];
listError = 0;
if(nargin < 2)
    fileSite = [];
    fileType = [];
    fileSrch = '*.gwf';
elseif(nargin < 3)
    fileType = [];    
    fileSrch = sprintf('%s-*.gwf',fileSite);
else        
    if(~isempty(fileSite))
        fileSrch = sprintf('%s-%s-*.gwf',fileSite,fileType);
    else
        fileSrch = sprintf('*-%s-*.gwf',fileType);        
    end
end
dirPath = char(dirPath);
if(isdir(dirPath) ~= 1)
    msgId = 'dir2framelist:badDir';
    error(msgId,'%s: directory %s not found',msgId,dirPath);
end

% - new algorithm to search all sub-directories
% SET directory list to one entry parent directory
% SET directory index = 1
% WHILE directory index <= length of directory list
%   GET directory from list at index
%   CHANGE to that directory
%   GET frame file list from directory
baseDir = pwd;
dirList = cell(1,1);
dirList{1,1} = dirPath;
dirIdx = 1;
numDir = 1;
numFiles = 0;
while(dirIdx <= numDir)
    curPath = char(dirList{1,dirIdx});
    srchText = fullfile(curPath,fileSrch);
    fileList = dir(srchText);

%   IF file list is not empty
%       LOOP over files
%           Now parse filename to find GPS start time duration
%           Frame file names should end in -NNNNNNNNN-XXXXX.gwf
%           NNNNNNNNNN - GPS start time
%           XXXX - duration
%           FIND location of '-' in file name
%           GET GPS start time as characters between last two dashes
%           GET duration as characters from last dash to '.gwf'
%           ADD frame file, GPS start, duration to arrays
%       END LOOP
%   ENDIF
    if(~isempty(fileList))
        nFiles = length(fileList);
        if(numFiles < 1)
            frameList = cell(nFiles,1);
            gpsList = zeros(nFiles,1);
            durList = zeros(nFiles,1);
            sizeList = zeros(nFiles,1);
            numFiles = 0;
        end
        for iFile = 1:nFiles
            thisFile = fullfile(curPath,fileList(iFile).name);
            lastChar = length(thisFile);
            dashPos = strfind(thisFile,'-');
            numDash = numel(dashPos);
            if(numDash > 1)
                gpsBeg = dashPos(numDash-1) + 1;
                gpsEnd = dashPos(numDash) - 1;
                gpsStart = str2num(thisFile(gpsBeg:gpsEnd));
                durBeg = dashPos(numDash)+1;
                durEnd = lastChar - 4;
                durVal = str2num(thisFile(durBeg:durEnd));
            else
                msgId = 'dir2framelist:badName';
                warning(msgId,'%s: file %s not to frame-file name convention',...
                    msgId,fileList(iFile).name);
                listError = 6;
                gpsStart = 0;
                gpsEnd = 0;
            end
            fileIdx = numFiles+iFile;
            gpsList(fileIdx) = gpsStart;
            frameList{fileIdx,1} = thisFile;
            durList(fileIdx) = durVal;
            sizeList(fileIdx) = fileList(iFile).bytes;
        end
        numFiles = numFiles + nFiles;
    end

%   GET list of sub-directories in directory
%   IF sub-directory list not empty
%       LOOP over sub-directories
%           ADD sub-directory to directory list
%       ENDLOOP
%   ENDIF
%   INCREMENT directory index
%   GET length of directory list
% ENDWHILE
% CHANGE to original directory
% SORT GPS times
% SORT frame files, duration like GPS times
% IF only one output
%   CREATE output as structure
% ENDIF
    subList = dir(curPath);
    subPtr = find([subList.isdir] > 0);
    numSub = numel(subPtr);
    if(numSub > 0)
        subList = subList(subPtr);
        for iSub = 1:numSub
            subDir = subList(iSub).name;
            if( (strcmp(subDir,'..')==true) || (strcmp(subDir,'.')==true))
                continue
            end
            thisDir = fullfile(curPath,subList(iSub).name);
            numDir = numDir + 1;
            dirList{1,numDir} = thisDir;
        end        
    end
    dirIdx = dirIdx + 1;
end
cd(baseDir);
if(numFiles > 0)
    [gpsTimes,ndx] = unique(gpsList);
    frameFiles = {frameList{ndx}};
    frameDurs = durList(ndx);
    frameSizes = sizeList(ndx);
else
    listError = 1;
    msgId = 'dir2framelist:noFiles';
    warning(msgId,'%s: No frame files in %s',msgId,dirPath);
end
if(nargout == 1)
    structOrGpsTimes = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
else
    structOrGpsTimes = gpsTimes;
end    
return
