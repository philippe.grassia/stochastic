function [structOrGpsTimes,frameFiles,listError,frameDurs] = framelist(instrument,type,gpsStart,duration)
% FRAMELIST - returns list of frame files, times for GPS time interval
% [frameListStruct] = framelist(instrument,type,gpsStart,duration)
% [gpsTimes,frameFiles[,listError,frameDurs]] = framelist(instrument,type,gpsStart,duration)
%
% Given an instrument (IFO), a data type, and GPS start time and duration, return
%   gpsTimes  - vector of gps start times
%   frameFiles  - cell-array of paths to corresponding frames
%   listError - [OPTIONAL] Possible error code - 0 if no error
%   frameDurs  - [OPTIONAL] vector of frame file durations
%
%   These can be retrieved individually (as 2-4 outputs) or as a 
%       single structure (1 output)
%     
%  ** Uses gw_data_find, so requires LIGO_DATAFIND_SERVER to be defined
%
%   listError values
%   1-10 - from FRAMELIST locally
%   11-24 - passed from MLLSCDATAFIND
%      1 no frame files in list
%      2 first frame file missing from disk
%      3 last frame file missing on disk
%      4 interior frame file missing from disk
%      5 LIGO_DATAFIND_SERVER not defined
%      11 gw_data_find connection failed
%      12 no files returned from query
%      13 LIGO_DATAFIND_SERVER unknown
%
%   error/warning messages
%       framelist:noServer
%       frameList:noFiles
%       frameList:failedQuery
%       framelist:noFirst
%       framelist:noLast
%       framelist:noMiddle
%
% $Id: framelist.m,v 1.6 2005-09-21 19:19:17 kathorne Exp $

% IF # of inputs is wrong
%   PRINT error message
%   EXIT
% ENDIF
% IF # of outputs is wrong
%   PRINT error message
%   EXIT
% ENDIF
% INITIALIZE outputs
% DEFINE cache of last successful framelist call
error(nargchk(4,4,nargin),'struct');
error(nargchk(1,4,nargout),'struct');
listError = 0;
gpsTimes = [];
frameFiles = [];
frameDurs = [];
structOrGpsTimes = [];
persistent oldInstrument;
persistent oldType;
persistent oldStart;
persistent oldDur;
persistent oldFrameFiles;
persistent oldGpsTimes;
persistent oldFrameDurs;
persistent oldListError;

% SET Starting second = start time rounded down to nearest integer
% SET end time = start time + duration
% SET Ending second = end time rounded up to nearest integer
% SET Duration seconds = Ending second - Starting second
% IF instrument, type, start second and duration match those
%       of previous call to framelist
%   SET framelist outputs from the cache
%   EXIT
% ENDIF
intStart = floor(gpsStart);
gpsEnd = gpsStart + duration;
intEnd = ceil(gpsEnd);
intDur = intEnd - intStart;
if(strcmp(instrument,oldInstrument) && ...
   strcmp(type,oldType) && ...
   (intStart == oldStart) && ...
   (intDur == oldDur))
    frameFiles = oldFrameFiles;
    gpsTimes = oldGpsTimes;
    frameDurs = oldFrameDurs;
    listError = oldListError;
    if(nargout == 1)
        structOrGpsTimes = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
    else
        structOrGpsTimes = gpsTimes;
    end
    return
end
% --otherwise (i.e. a different frame list is needed) --
%
% IF LSC DataFind Server is NOT defined
%   SET list status to Failed
%   IF list status is an output
%       SET list status output
%       CREATE warning
%   ELSE
%       CREATE error message
%       EXIT
%   ENDIF
% ENDIF
lscServer = getenv('LIGO_DATAFIND_SERVER');
if(isempty(lscServer))
    msgId = 'framelist:noServer';
    tmp = sprintf('FRAMELIST: env variable LIGO_DATAFIND_SERVER not set');
    listError = 5;
    if(nargout == 1)
        structOrGpsTimes = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
        warning(msgId,tmp);
    else
        structOrGpsTimes = gpsTimes;
        if(nargout < 3)
            error(tmp);
        else
            warning(msgId,tmp);
        end
    end
    return
end

% GET list of files using gw_data_find
%       -- option using MySQL has been removed 
% IF no frame files were found
%   SET list status to Failed
%   IF list status is an output
%       SET list status output
%       CREATE warning
%   ELSE
%       CREATE error message
%       EXIT
%   ENDIF
% ENDIF
[frameList,numFiles,fqStat,gpsList,durList] = ... 
        mllscdatafind(instrument,type,intStart,intDur);
if(numFiles == 0)
    msgId = 'framelist:noFiles';
    tmp = sprintf('FRAMELIST: No frame files found\n');
    listError = 1;
    if(nargout == 1)
        structOrGpsTimes = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
        warning(msgId,tmp);
    else
        structOrGpsTimes = gpsTimes;
        if(nargout < 3)
            error(tmp);
        else
            warning(msgId,tmp);
        end
    end
    return
end

% IF error code returned from query
%   SET list status to Failed
%   IF list status is an output
%       SET list status output
%       CREATE warning
%   ELSE
%       CREATE error message
%       EXIT
%   ENDIF
% ENDIF
if(fqStat ~= 0)
    msgId = 'framelist:failedQuery';
    tmp=sprintf('FRAMELIST: gw_data_find failed with error code %d\n',fqStat);
    listError = fqStat + 10;
    if(nargout == 1)
        structOrGpsTimes = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
        warning(msgId,tmp);
    else
        structOrGpsTimes = gpsTimes;
        if(nargout < 3)
            error(tmp);
        else
            warning(msgId,tmp);
        end
    end
    return
end

% NOTE: the physical file list that comes from
% mlframequery will be of the format
%   'file://name.of.node/local/file/path'
%   We need to strip off the file: header
%       as well as the node name to
%       get the local file path
% LOOP over list of files
%	IF file: header is present
%       REMOVE file: header from file path
%	ENDIF
%   IF // is present in file path
%       REMOVE // from path (leaving node-name)
%   ENDIF
%   FIND the next / in path (indicating end of node-name
%       or beginning of local path)
%   IF a / exists in path
%       SET file path to string starting at this next /
%   ENDIF
% END LOOP
filSrch = 'file:';
lenFil = length(filSrch);
dslSrch = '//';
lenDsl = length(dslSrch);
for k = 1:numFiles
    fileName = char(frameList{k});
	filPos = findstr(filSrch,fileName);
	if(~isempty(filPos))
		fileName = fileName((filPos+lenFil):end);
    end
    dslPos = findstr('//',fileName);
	if(~isempty(dslPos))
        fileName = fileName((dslPos+lenDsl):end);
    end
    slPos = findstr('/',fileName);
    if(~isempty(slPos))
        fileName = fileName(slPos(1):end);
    end
	frameList{k} = fileName;
end

% SET GPS output to array from query
% SET frame duration output to array from query
% SORT GPS,file,duration arrays by ascending GPS Start Time
% REMOVE duplicate frame files in GPS,file,duration arrays
% CLEAR list error flag
[gpsTimes,ndx] = unique(gpsList);
frameFiles = {frameList{ndx}};
frameDurs = durList(ndx);
listError = 0;

% error checking: do the available frames cover the requested data?
% IF first frame is missing
%   CREATE warning message
%   SET list error flag
% ENDIF
% IF last frame is missing
%   CREATE warning message
%   SET list error flag
% ENDIF
if ((gpsStart-gpsTimes(1))<0)
    msgId = 'framelist:noFirst';
    tmp = sprintf('FRAMELIST: Missing first frame at GPS %d',...
        gpsStart-mod(gpsStart,frameDurs(1)));
    listError = 2;
    warning(msgId,tmp);
end
if ((gpsEnd-gpsTimes(end))>frameDurs(end))
    msgId = 'framelist:noLast';
    tmp = sprintf('FRAMELIST: Missing last frame at GPS %d',...
        gpsStart+duration-mod(gpsStart+duration,frameDurs(end)));
    listError = 3;
    warning(msgId,tmp);
end

% IF there are interior frames
%   LOOP over interior frames
%       IF frame is missing
%           CREATE warning message
%           SET list error flag
%       ENDIF
%   ENDLOOP
% ENDIF
if (length(gpsTimes) > 1)
    df = diff(gpsTimes);
	for ifr=1:length(df)
        if(df(ifr) ~= frameDurs(ifr))
            msgId = 'framelist:noMiddle';
            tmp = ...
                sprintf('FRAMELIST: Missing interior frame at GPS %d\n',...
                (gpsTimes(ifr)+frameDurs(ifr)));
            listError = 4;
            warning(msgId,tmp);
        end
    end
end

% ADD frame list, inputs to cache
% IF only one output
%   CREATE output as structure
% ENDIF
oldInstrument = instrument;
oldType = type;
oldStart = intStart;
oldDur = intDur;
oldFrameFiles = frameFiles;
oldGpsTimes = gpsTimes;
oldFrameDurs = frameDurs;
oldListError = listError;
if(nargout == 1)
    structOrGpsTimes = mkframeliststruct(gpsTimes,frameFiles,...
            listError,frameDurs);
else
    structOrGpsTimes = gpsTimes;
end
return
