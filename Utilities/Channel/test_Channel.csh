#!/bin/csh
#  test_Channel.csh
#    -- test Channel class automatically
#        from shell prompt
# start matlab
matlab -nodisplay -nojvm <<EOF
cd test;
addpath ..;
[numFail,failMess]=chanclasstests;
[numFail,failMess]=chanstructtests;
cd ..;
exit;
EOF
