function [site,type,gps,dur] = parseframefilename(frameFile)
% PARSEFRAMEFILENAME - parses frame file name
%   [site,type,gps,dur] = parseframefilename(frameFile)
%           frameFile - frame file name
%
%  * Assumes name has format
%       site-type-gps-dur.gwf
%
%   $Id: parseframefilename.m,v 1.1 2007-06-21 19:39:24 kathorne Exp $
site = [];
type = [];
gps = [];
dur = [];
error(nargchk(1,1,nargin),'struct');
if(iscell(frameFile))
    frameFile = char(frameFile);
end
if(~ischar(frameFile))
    msgId = 'PARSEFRAMEFILENAME:badInput';
    error(msgId,'%s: input was not a character string',msgId);
end
[frDir,frName,dum] = fileparts(frameFile);
numChar = numel(frName);
dashPos = strfind(frName,'-');
if(numel(dashPos) < 3)
    msgId = 'PARSEFRAMEFILENAME:badName';
    error(msgId,'%s: file %s must have >= 3 dashes (-)',msgId,frName);
end
dur = frName(dashPos(end)+1:numChar);
gps = frName(dashPos(end-1)+1:dashPos(end)-1);
dur = str2num(dur);
gps = str2num(gps);
type = frName(dashPos(1)+1:dashPos(end-1)-1);
site = frName(1:dashPos(1)-1);
return
