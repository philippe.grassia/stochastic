%
%  coarseGrain --- coarse grain a frequency-series 
%
%  coarseGrain(x, flowy, deltaFy, Ny) accepts a frequency-series structure
%  (usually a PSD) and returns a frequency-series structure which has been
%  coarse-grained to the frequency values f = flowy + deltaFy*[0:Ny-1].
%
%  coarseGrain also returns the frequency indices of the lowest and highest
%  frequency bins of x that overlap with y (0 <= index1 <= length(x);
%  1 <= index2 <= length(x)+1) and the fractional contributions from these 
%  frequency bins (note that these indices start from 0 and and are 1 less
%  than the corresponding Matlab indices). The fractional contribution is
%  the fraction of the fine bin that overlaps with any part of the coarse
%  series.
%
%  The method used is to treat the x-values
%
%    xc(k) = x.flow + (k - 1)*x.deltaF
%
%  as bin centres and the value x.data(k) as the average value over the
%  the bin ie.
%
%    (1/x.deltaF)*(integral of x.data from the lower edge of the bin to upper edge)
%
%  The coarse graining can then be performed by finding the integral
%  from the start of the fine series just below the coarse series (via
%  the cumulative sum), interpolating to the coarse series, and taking
%  differences to recover the average value of the function for each coarse bin.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [y, index1, index2, frac1, frac2]=coarseGrain_new(x, flowy, deltaFy, Ny)

  % Set the metadata for the coarse-grained frequency series
  y.flow = flowy;
  y.deltaF = deltaFy;
  if (isfield(x, 'symmetry'))
    y.symmmetry = x.symmetry;
  else
    y.symmetry = 0;
  end;

  % Length of coarse series
  Nx = length(x.data);

  % Lower edge of the first bin of the fine series
  xLowerEdge = x.flow - 0.5*x.deltaF;

  % Upper edge of the last bin of the fine series
  xUpperEdge = x.flow + (Nx - 0.5)*x.deltaF;

  % yi(k) is the lower edge of bin k for the coarse series
  yi = y.flow + y.deltaF*[-0.5:1:Ny-0.5].';

  % Lower edge of the first bin of the coarse series
  yLowerEdge = yi(1);

  % Upper edge of the last bin of the coarse series
  yUpperEdge = yi(end);

  % Error checking
  if (Nx <= 0)
    error('Length of fine series is invalid');
  end;

  if (Ny <= 0)
    error('Length of coarse series is invalid');
  end;

  if (x.deltaF <= 0)
    error('Spacing of fine series is invalid');
  end;

  if (y.deltaF <= 0)
    error('Spacing of coarse series is invalid');
  end;

  if (y.deltaF < x.deltaF)
    error('Frequency spacing of coarse series is smaller than fine series');
  end;

  if (yLowerEdge < xLowerEdge)
    error('Start of coarse series is less than start of fine series');
  end;

  if (yUpperEdge > xUpperEdge)
    error('End of coarse series is more than end of fine series');
  end;

  % xlow is the index of the last bin whose lower edge is <= the
  % lower edge of the coarse-grained sequence, that is
  %   x(xlow) <= y(1) < x(xlow+1)
  xlow = 1 + floor((yLowerEdge - xLowerEdge)/x.deltaF);

  % xhi is the index of the last bin whose upper edge is >= the
  % lower edge of the coarse-grained sequence, that is
  %   x(xhi-1) < y(end) <= x(xhi)
  xhi = ceil((yUpperEdge - xLowerEdge)/x.deltaF);

  % xi is the array of frequencies of the lower edge of each bin, that is,
  % xi(k) is the lower edge of bin k, which is
  %   x.flow + (k-1)*x.deltaF - 0.5*x.deltaF = x.flow + (k-1.5)*x.deltaF
  % This is only calculated for the bins that the coarse series overlaps with,
  % that is, bins [xlow:xhi]
  xi = x.flow + [(xlow-1.5):1:(xhi-0.5)].'*x.deltaF;

  % Integrate the original function so that the value fi(k) is
  % the integral from the lower edge of the fine series xi(1)
  % to xi(k). Since each bin represents the average value of the PSD
  % we just have to sum them to get the integral (with the appropriate df).
  % The 0 is inserted so that the fi(1) is 0, as it should be,
  % so that we can interpolate the integral to the coarse series
  fi = [0; x.deltaF*cumsum(x.data(xlow:xhi)) ];

  % Interpolate the integrated function using the lower edges of each bin in
  % the coarse series as the ordinates
  y.data = interp1(xi, fi, yi, 'linear');

  % Take the difference to obtain the integrals over each individual bin of the
  % coarse series and divide by deltaF to get the average value. Then y.data(k)
  % is the average value of the PSD over the interval y(k) to y(k+1) as desired
  y.data = (1/y.deltaF)*diff(y.data);

  % For compatability with the original coarseGrain, we also return the
  % frequency index of the first and last bins of the fine series that
  % overlap with the coarse series (these are the array index - 1)
  index1 = xlow - 1;
  index2 = xhi - 1;

  % Fraction of the end bins of the fine series that overlap with the
  % coarse series
  frac1 = (xi(2) - yLowerEdge)/x.deltaF;
  frac2 = (yUpperEdge - xi(end-1))/x.deltaF;

return;
