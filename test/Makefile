#
# Makefile for testing the stochastic pipeline and
# post-processing
#
# Useful targets:
#
#   make test - test complete pipeline
#   make cache - make the frame cache and DAG file
#   make dag - make the frame cache and DAG file
#   make output1 - run stochastic for job 1 (sim. for job 2)
#   make output - run stochastic for all jobs
#   make post - do postprocessing
#   make clean - remove all output (there are other clean
#                targets to remove subsets of output)
#
PARAMS_FILE = input/paramfiles/test_params.txt
JOBS_FILE = input/jobfiles/test_jobs.txt
CACHEDIR = input/cachefiles
OUTPUTDIR = output

# Parameters needed for post-processing
POSTDIR = postoutput
DSIGMACUT = 0.2
LARGESIGMACUTOFF = 1
DORENORMALIZE = false
MODIFYFILTER = 0
DISPLAYRESULTS = false

CACHEFILES = \
$(CACHEDIR)/frameFilesH.1.txt \
$(CACHEDIR)/frameFilesH.2.txt \

DAGFILES = stochastic_pipe.dag

OUTFILES1 = \
$(OUTPUTDIR)/test_naivesigmas.job1.trial1.dat \
$(OUTPUTDIR)/test_optimal.job1.trial1.dat \
$(OUTPUTDIR)/test_params.job1.dat \
$(OUTPUTDIR)/test_psd1.job1.trial1.dat \
$(OUTPUTDIR)/test_psd2.job1.trial1.dat \

OUTFILES2 = \
$(OUTPUTDIR)/test_naivesigmas.job2.trial1.dat \
$(OUTPUTDIR)/test_optimal.job2.trial1.dat \
$(OUTPUTDIR)/test_params.job2.dat \
$(OUTPUTDIR)/test_psd1.job2.trial1.dat \
$(OUTPUTDIR)/test_psd2.job2.trial1.dat \

CONCATFILES = \
$(OUTPUTDIR)/test_concatNaiveSigmas.dat \
$(OUTPUTDIR)/test_concatCCStats.dat

POSTFILES = \
$(POSTDIR)/H1H2_ccstats_renormalized.dat \
$(POSTDIR)/H1H2_FFTofPtEstIntegrand.dat \
$(POSTDIR)/H1H2_FFTofPtEstIntegrand.eps \
$(POSTDIR)/H1H2_FFTofPtEstIntegrand.fig \
$(POSTDIR)/H1H2_FFTofPtEstIntegrand.png \
$(POSTDIR)/H1H2_ptEstIntegrand.dat \
$(POSTDIR)/H1H2_runningPointEstimate.dat \
$(POSTDIR)/H1H2_runningPointEstimate.eps \
$(POSTDIR)/H1H2_runningPointEstimate.fig \
$(POSTDIR)/H1H2_runningPointEstimate.png \
$(POSTDIR)/H1H2_runningPtEstIntegrand.eps \
$(POSTDIR)/H1H2_runningPtEstIntegrand.fig \
$(POSTDIR)/H1H2_runningPtEstIntegrand.png \
$(POSTDIR)/H1H2_runningSigma.eps \
$(POSTDIR)/H1H2_runningSigma.fig \
$(POSTDIR)/H1H2_runningSigma.png \
$(POSTDIR)/H1H2_sensIntegrand.dat \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot11_H1-H2.eps \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot11_H1-H2.fig \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot11_H1-H2.png \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot4_H1-H2.eps \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot4_H1-H2.fig \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot4_H1-H2.png \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot7_H1-H2.eps \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot7_H1-H2.fig \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot7_H1-H2.png \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot8_H1-H2.eps \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot8_H1-H2.fig \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot8_H1-H2.png \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot9_H1-H2.eps \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot9_H1-H2.fig \
$(POSTDIR)/H1H2StatisticalAnalysis_PanelPlot9_H1-H2.png \
$(POSTDIR)/H1H2_stats.dat

exe :
mex :
docs :
test_exe :

test : post
post : $(POSTFILES)
output2 : $(OUTFILES2)
output1 : $(OUTFILES1)
output : output1 output2
cache dag : $(CACHEFILES)

ping :
	@echo "-- Checking LSC_DATAFIND_SERVER"; \
	if [ "$LSC_DATAFIND_SERVER" == "" ]; then \
		echo "Error: environment variable LSC_DATAFIND_SERVER is not set."; \
		echo "Please set it to the appropriate value for your system."; \
	exit 1; \
	fi; \
	LSCdataFind --ping

# The ability to make cachefiles depends on whether we can
# successfully ping the datafind server
$(CACHEFILES) $(DAGFILES) : $(PARAMS_FILE) $(JOBS_FILE)
	@echo "--  Preparing cache stochastic pipeline"; \
	../CrossCorr/stochastic_pipe.tclsh -v -f -p $(PARAMS_FILE) \
        -j $(JOBS_FILE) --add-post-processing -pp stochastic_postproc.sub \
        -t $(POSTDIR) -c $(DSIGMACUT) -l $(LARGESIGMACUTOFF) \
        -n $(DORENORMALIZE)

$(OUTFILES1) : $(CACHEFILES)
	@echo "-- Running stochastic for job 1"; \
	../CrossCorr/stochastic $(PARAMS_FILE) $(JOBS_FILE) 1

$(OUTFILES2) : $(CACHEFILES)
	@echo "-- Running stochastic for job 2"; \
	../CrossCorr/stochastic $(PARAMS_FILE) $(JOBS_FILE) 2

$(POSTFILES) : $(OUTFILES1) $(OUTFILES2)
	@echo "-- Running post-processing"; \
	./stochastic_postproc.sh $(PARAMS_FILE) $(JOBS_FILE) \
	$(POSTDIR) $(DSIGMACUT) $(LARGESIGMACUTOFF) $(DORENORMALIZE) \
	$(MODIFYFILTER) $(DISPLAYRESULTS)

clean-cache :
	@echo "-- Cleaning cache"; \
	rm -f $(CACHEDIR)/*.txt

clean-output :
	@echo "-- Cleaning output"; \
	rm -f $(OUTPUTDIR)/test_*

clean-postout :
	@echo "-- Cleaning postoutput"; \
	rm -f $(POSTDIR)/H1H2*

clean : clean-cache clean-output clean-postout
	@echo "-- Cleaning everything else"; \
	rm -f stochastic_pipe.dag \
	stochastic_pipe.dag.condor.sub \
	stochastic_pipe.dag.dagman.log \
	stochastic_pipe.dag.dagman.out \
	stochastic_pipe.dag.lib.out \
	stochastic_pipe.dag.rescue.* \
	stochastic_pipe.log \
        stochastic_postproc.log \
	err.* \
	out.*; \
        rm -rf bin


