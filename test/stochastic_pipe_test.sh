#!/bin/sh
#
# This is a simple test of the stochastic pipeline code. It requires that
# the environment variable LSC_DATAFIND_SERVER be set to an appropriate
# value.
#

PARAMS_FILE=input/paramfiles/test_params.txt
JOBS_FILE=input/jobfiles/test_jobs.txt

echo "-- Removing old files"
make clean

echo
echo "-- Checking LSC_DATAFIND_SERVER"
if [ "$LSC_DATAFIND_SERVER" == "" ]; then
  echo "Error: environment variable LSC_DATAFIND_SERVER is not set."
  echo "Please set it to the appropriate value for your system."
  exit 1
fi

LSCdataFind --ping

echo
echo "-- Preparing cache stochastic pipeline"
../CrossCorr/stochastic_pipe.tclsh -v -f -p $PARAMS_FILE -j $JOBS_FILE \
  --add-post-processing -pp stochastic_postproc.sub
res=$?
if [ $res -eq 1 ]; then
  echo "Error running stochastic_pipe.tclsh"
  exit $res
fi

echo
echo "-- Running stochastic pipeline"
if [ -x ../CrossCorr/stochastic ]; then
  ./stochastic.sh "$PARAMS_FILE" "$JOBS_FILE" "1"
  ./stochastic.sh "$PARAMS_FILE" "$JOBS_FILE" "2"
else
  echo "../stochastic either does not exist or is not executable"
  exit 1
fi

echo
echo "-- Performing post-processing"
if [ -x ../PostProcessing/postProcessScriptFull ]; then
  ./stochastic_postproc.sh "$PARAMS_FILE" "$JOBS_FILE" "postoutput" "0.2" "1" "false" "0" "0"
else
  echo "../PostProcessing/postProcessScriptFull either does not exist or is not executable"
  exit 1
fi

echo
echo "-- Interactive test complete!"
echo
echo "-- To test the pipeline in Condor, use the commands"
echo "     make clean"
echo "     make dag"
echo "     condor_submit_dag stochastic_pipe.dag"
echo
