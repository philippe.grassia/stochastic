This is the test directory for the stochastic pipeline. Before running,
please make sure that

1) the environment variable LSC_DATAFIND_SERVER is set to the appropriate
value for your local system

2) ../CrossCorr/stochastic.m has been compiled and the *full path* to
../CrossCorr is in your LD_LIBRARY_PATH

3) ../PostProcessing/postProcessScriptFull.m has been compiled and the *full
path* to ../PostProcessing is in your LD_LIBRARY_PATH

(Alternatively, you can use "make cache" to produce the cache files, then
run stochastic.m interactively in Matlab with the appropriate parameters)

There are two ways to run the test. The nicest method is to use

  make test

which executes the entire pipeline. Since this uses make and file
dependencies, it can be broken down into stages. That way if the
make fails at some point it is easy to fix the problem and run
make again without having to start over from scratch. Make test
is equivalent to running the three parts of the pipeline:

  make cache   - creates cache files and DAG
  make outputs - runs stochastic.m
  make post    - run post-processing

The other targets are

  make clean - remove all the files
  make clean-cache
  make clean-output
  make clean-postout

Another way to run the test is via the shell script
   
   ./stochastic_pipe_test.sh

This will run stochastic_pipe.tclsh to generate the cache files in
input/cachefiles, then run the stochastic executable, putting the results
into the output/ directory. After that, the post-processing will be
run with the results going into the postoutput/ directory.

The third way to run the test uses Condor. First create the DAG file using
one of the methods above, then submit the job eg.

   make clean
   make dag
   condor_submit_dag stochastic_pipe.dag
