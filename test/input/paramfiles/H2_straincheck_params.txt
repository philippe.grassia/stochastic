% flags for optional operations
doFreqMask true
doHighPass1 false
doHighPass2 false
writeResultsToScreen true

% ifo names
ifo1 H2 
ifo2 H2

% segment duration (sec)
segmentDuration 16

% freq resolution and freq cutoffs for CC statistic sum (Hz)
flow 50
%fhigh 5000
fhigh 1000

% resample rate (Hz)
%resampleRate1 2048
resampleRate1 16384

% ASQ channels
ASQchannel1 LSC-DARM_ERR
ASQchannel2 LSC-STRAIN

% frame type and duration
frameType1 RDS_R_L3
frameType2 H2_RDS_C03_L2
frameDuration1 256
frameDuration2 128

% params for matlab resample routine
nResample1 10
nResample2 10
betaParam1 5
betaParam2 5

% params for high-pass filtering (3db freq in Hz, and filter order) 
highPassFreq1 40
highPassFreq2 40
highPassOrder1 6
highPassOrder2 6

% coherent freqs and number of freq bins to remove if doFreqMask=true;
% NOTE: if an nBin=0, then no bins are removed even if doFreqMask=true
% (coherent freqs are typically harmonics of the power line freq 60Hz
% and the DAQ rate 16Hz)
% Calibration lines are at 54.1 Hz, 407.3 Hz, 1159.7 Hz
% The 54.1 Hz cal line needs to have 3 bins notched out
%freqsToRemove 108.75,120,148.75,180,194.25,240,265.5,300,360,376,407.3,420,480
%nBinsToRemove 1,1,1,1,1,1,1,1,1,3,1,1,1
%freqsToRemove 52.75,54.1,60,108.75,120,148.75,180,194.25,240,265.5,300,360,376,407.3,420,480,1159.7
%nBinsToRemove 15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15
freqsToRemove 54.1,407.3,1159.7
nBinsToRemove 25,25,25

% calibration filenames
alphaBetaFile1 ../../../../../../sgwb/S5/onasys/input/calibration/H-H2_CAL_FAC_S5_V3_060-846139213-7383120.mat
calCavGainFile1 ../../../../../../sgwb/S5/onasys/input/calibration/H-H2_CAL_REF_CAV_GAIN_DARM_ERR_S5_V3-846138794-999999999.mat
calResponseFile1 ../../../../../../sgwb/S5/onasys/input/calibration/H-H2_CAL_REF_RESPONSE_DARM_ERR_S5_V3-846138794-999999999.mat

alphaBetaFile2 none
calCavGainFile2 none
calResponseFile2 none

% prefix for output filename
outputFilePrefix ../../../../../../sgwb/S5/onasys/output/straincheck/S5_Epoch3_H2_1kHz

