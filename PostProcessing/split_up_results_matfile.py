import numpy as np
from scipy.io import loadmat, savemat
from pproc import (pick_out_nbr_direction, apply_freq_mask,
                   get_bad_frequencies)

import optparse
# done imports
def parse_command_line():
    """
    parse command line
    """
    parser = optparse.OptionParser()
    parser.add_option("--results-matfile",
        help="matfile with directions together", default=None,
        dest="results_matfile", type=str)
    parser.add_option("--output-prefix",
        help="output file prefix", default=None,
        dest="output_prefix", type=str)
    parser.add_option("--direction-names",
        help="comma separated list of direction names", default=None,
        dest="direction_names", type=str)
    parser.add_option("--ndirections",
        help="number of directions", default=None,
        dest="ndirections", type=int)
    parser.add_option("--notch-list",
        help="notch list", default=None,
        dest="notch_list", type=str)
    params, args = parser.parse_args()
    return params

def run(params):
    results_mat = loadmat(params.results_matfile, squeeze_me=True)
    direction_names = params.direction_names.split(',')
    if np.size(direction_names) != params.ndirections:
        raise ValueError('supplied different number of names than directions')
    for ii, name in enumerate(direction_names):
        Y, sigma, freqs = pick_out_nbr_direction(results_mat['ptEst'], results_mat['sig'], results_mat['f'], ii+1)
        if params.notch_list is not None:
            df =np.min(freqs[1:] - freqs[:-1])
            bad_freqs = get_bad_frequencies(params.notch_list, df)
            Y,mask = apply_freq_mask(Y, freqs, bad_freqs)
            sigma,mask = apply_freq_mask(sigma, freqs, bad_freqs)

        savemat(params.output_prefix + name + '.mat', {'ptEst': Y, 'sig':sigma,
                                                       'f':freqs})

if __name__=="__main__":
    params = parse_command_line()
    run(params)
