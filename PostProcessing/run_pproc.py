"""
Run post processing
"""
# TODO: test this with O1 data
# TODO: add notchlist
import pproc
import numpy as np
import optparse
from scipy.io import savemat

# done imports
def parse_command_line():
    """
    parse command line
    """
    parser = optparse.OptionParser()
    parser.add_option("--output-prefix",
        help="output file prefix", default=None,
        dest="output_file_prefix", type=str)

    parser.add_option("--job1",
        help="first job number to combine", default=1,
        dest="job1", type=int)

    parser.add_option("--jobN",
        help="highest job number to combine", default=None,
        dest="jobN", type=int)

    parser.add_option("--checkpoint-file",
        help="checkpointing file to reload", default='ckpoint.pkl',
        dest="checkpoint_file", type=str)

    parser.add_option("--load-checkpoint",
        help="add tag to load checkpoint file", default=False,
        dest="load_checkpoint", action="store_true")

    parser.add_option("--search-type",
        help="search type, choices: 'bbr','nbr','isotropic','sph'", default=None,
        dest="search_type", type=str)

    parser.add_option("--badtimes-file",
        help="bad gps times file", default=None,
        dest="badtimes_file", type=str)

    parser.add_option("--segment-duration",
        help="segment duration for analysis", default=None,
        dest="segdur", type=float)

    parser.add_option("--deltaF",
        help="frequency bin width for analysis", default=None,
        dest="deltaF", type=float)

    parser.add_option("--output-matfile",
        help="final output matfile name", default='results.mat',
        dest="output_matfile", type=str)

    parser.add_option("--notch-list",
        help="notch list in stoch paramfile format", default=None,
        dest="notch_list", type=str)

    params, args = parser.parse_args()
    return params

def check_params(params):
    # search types
    search_types = ['bbr', 'isotropic','nbr', 'sph']
    if params.output_file_prefix is None:
        raise ValueError("must supply location of files to load\
                         '--output-prefix'")
    if params.jobN is None:
        raise ValueError("must supply largest job number to load")
    # check that valid search type is supplied
    try:
        search_types.index(params.search_type)
    except ValueError:
        raise ValueError("Must supply a valid search type see '--help' for options")
    if params.badtimes_file is None:
        print("WARNING: no bad gps times supplied")
    if params.segdur is None:
        raise ValueError("Must supply segment duration")
    if params.deltaF is None:
        raise ValueError("Must supply frequency bin width")


def run(params):
    jobs = np.arange(params.job1, params.jobN+1)
    joblist = pproc.StochasticJobList(jobs, params.output_file_prefix,
                                    stoch_type=params.search_type)
    Y, sigma, dim2, dim2type = joblist.combine_jobs_together(params.segdur,
                                   params.deltaF, params.badtimes_file,
                                   load_checkpoint=params.load_checkpoint,
                                   checkpoint_filename=params.checkpoint_file)

    # apply frequency notches
    if dim2type=='freqs' and params.notch_list is not None:
        bad_freqs = pproc.get_bad_frequencies(params.notch_list, params.deltaF)
        Y,mask = pproc.apply_freq_mask(Y, dim2, bad_freqs)
        sigma,mask = pproc.apply_freq_mask(sigma, dim2, bad_freqs)


    # save rsults to matfile
    outmat = {}
    outmat['ptEst'] = Y
    outmat['sig'] = sigma
    outmat['checkpoint_file'] = params.checkpoint_file
    outmat['jobs_combined'] = jobs
    outmat['search_type'] = params.search_type
    if dim2type=='freqs':
        # narrowband radiometer
        outmat['f'] = dim2
        if params.notch_list is not None:
            outmat['freq_mask'] = mask
            outmat['bad_freqs'] = bad_freqs
    elif dim2type=='directions':
        # broadband radiometer
        outmat['directions'] = dim2
    savemat(params.output_matfile, outmat)


if __name__=="__main__":
    params = parse_command_line()
    check_params(params)
    run(params)
