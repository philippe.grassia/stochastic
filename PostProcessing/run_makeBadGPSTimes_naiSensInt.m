% Parameters which are the same for all runs of makeDeltaSigmaCutsInPostProc

jobfile='../input/jobs/JOB-FILE-1238166018-1239458471.dat';
dsc=0.2;
doOverlap=1;
segmentDuration=192;
deltaF=0.03125;


% For a=-5

stochOutputPrefix_aminus5='../output/a0/a0/H1L1';
outname_aminus5='./tmp_a-5.txt'; 


makeDeltaSigmaCutsInPostProc_naiSensInt(jobfile,stochOutputPrefix_aminus5,dsc,doOverlap,segmentDuration,deltaF,outname_aminus5,-5);


% For a=0

stochOutputPrefix_a0='../output/a0/a0/H1L1';
outname_a0='./tmp_a0.txt';


makeDeltaSigmaCutsInPostProc_naiSensInt(jobfile,stochOutputPrefix_a0,dsc,doOverlap,segmentDuration,deltaF,outname_a0,0);


% For a=3

stochOutputPrefix_a3='../output/a0/a0/H1L1';
outname_a3='./tmp_a3.txt';


makeDeltaSigmaCutsInPostProc_naiSensInt(jobfile,stochOutputPrefix_a3,dsc,doOverlap,segmentDuration,deltaF,outname_a3,3);


% Super cut

outname_supercut='./badGPSTimes_naiSensInt.dat'


produceSuperCut(outname_aminus5,outname_a0,outname_a3,outname_supercut);



% cleanup
delete(outname_aminus5,outname_a0,outname_a3);
