
paramsFile = '/home/mcoughlin/Stochastic/Bayesian/params/params.txt';
jobsFile = '/home/mcoughlin/Stochastic/Bayesian/jobfile.txt';
jobNumber = 1;

for i =1:1540
   jobNumber = i;
   file = sprintf('/home/mcoughlin/Stochastic/Bayesian/output/output_%d.mat',jobNumber);
   if exist(file)
      continue
   end
   fprintf('%d\n',jobNumber);
   bayesian(paramsFile, jobsFile, jobNumber);
end

