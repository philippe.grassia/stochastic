
jobNumber = 1;
type = 'MDC';
type = 'inj';
analysisTypes = {'powerlaw'};
analysisTypes = {'powerlawPSD'};
dataSet = 'MDC2all';
%dataSet = 'MDC';
doRun = 1;
%doRun = 0;

dataSets = {'MDC0all','MDC2all','MDC3all','MDC4all','MDC5all'};
%dataSets = {'MDC2all'};
%dataSets = {'MDC0all'};
%dataSets = {'MDC2inj_mid'}
%dataSets = {'3ideal'};
%dataSets = {'5ideal'};
%dataSets = {'SB','MDC2inj','MDC2inj_mid','MDC2inj_low'};
%analysisTypes = {'powerlaw'};
%dataSets = {'MDC0all','MDC2all'};
%dataSets = {'simple'};
%dataSets = {'simpleSB'};
analysisTypes = {'powerlaw','powerlawPSD'};
analysisTypes = {'powerlawPSD'};
dataSets = {'MDC0all','MDC2all'};
analysisTypes = {'DNSPSD'};
%dataSets = {'MDC2all'};


for i = 1:length(analysisTypes)
   %for j = 4
   for j = 1:length(dataSets)
      bayesian_mn(jobNumber,analysisTypes{i},dataSets{j},doRun);
   end
end

