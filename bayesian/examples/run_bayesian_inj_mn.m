
jobNumber = 1;
type = 'MDC';
type = 'inj';
analysisTypes = {'powerlaw'};
analysisTypes = {'powerlawPSD'};
dataSet = 'MDC2all';
%dataSet = 'MDC';
doRun = 1;
%doRun = 0;

dataSets = {'MDC2inj','MDC3inj','MDC4inj','MDC5inj'};
dataSets = {'MDC2inj'};
%dataSets = {'MDC2inj_mid'};
dataSets = {'MDC2inj','MDC2inj_mid','MDC2inj_low'};
%dataSets = {'MDC2inj_mid','MDC2inj_low'};
%dataSets = {'MDC2inj_low'};
%dataSets = {'MDC2all'};
%dataSets = {'3ideal'};
%dataSets = {'5ideal'};

for i = 1:length(analysisTypes)
   %for j = 4
   for j = 1:length(dataSets)
      bayesian_mn(jobNumber,analysisTypes{i},dataSets{j},doRun);
   end
end

