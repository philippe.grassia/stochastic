function mnPE(jobNumber,analysisTypes,dataSet,injSNR);

rng(1,'twister');

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_mn';
plotDir = [baseplotDir '/' dataSet '/' 'DNSPSDall' '/' num2str(jobNumber) '/compare'];
createpath(plotDir);

if strcmp(dataSet,'simple');
   trueval1 = 0; trueval2 = 0; trueval4 = 0;
elseif strcmp(dataSet,'simpleSB')
   trueval1 = -36.5; trueval2 = -3; trueval4 = 0;
elseif strcmp(dataSet,'simplePoint')
   trueval1 = 2; trueval2 = -3; trueval3 = 6.0; trueval4 = 30;
elseif strcmp(dataSet,'SBnoise') || strcmp(dataSet,'SBnoiseflat')
   trueval2 = -33.38; trueval1 = -3;
elseif strcmp(dataSet,'MDC2nonoise') || strcmp(dataSet,'MDC2nonoiseplus') || strcmp(dataSet,'MDC2noise') || strcmp(dataSet,'MDC2noiseflat') || strcmp(dataSet,'MDC2noise0')
   if strcmp(analysisType,'powerlaw')
      trueval1 = -46; trueval2 = 2/3 - 3; trueval3 = 0; trueval4 = 0;
   else
      trueval1 = 1.2188; trueval2 = log10(4.7933e-4); trueval3 = 0; trueval4 = 0;
   end
elseif strcmp(dataSet,'simpleCBC') || strcmp(dataSet,'simpleCBCNagamine')
   if strcmp(analysisTypes{1},'powerlaw')
      trueval1 = -46; trueval2 = 2/3 - 3; trueval3 = 0; trueval4 = 0;
   else
      trueval1 = 1.2188; trueval2 = log10(4.7933e-4); trueval3 = 0; trueval4 = 0;
   end
end

As = []; ks = [];
AsMarg = []; ksMarg = [];
np1Marg = []; np2Marg = [];
np3Marg = []; np4Marg = [];

for i = 1:length(analysisTypes)
   matDir = [baseplotDir '/' dataSet '/' analysisTypes{i} '/' num2str(jobNumber)];
   matFile = [matDir '/' num2str(injSNR) '/multinest.mat'];

   load(matFile,'logZ','nest_samples','post_samples','prior'); 
   [a,b] = size(prior);
   %post_samples = nest_samples(end-6000:end,:);
   %post_samples = nest_samples;

   wp = 1;
   bins1 = linspace(prior{1,3},prior{1,4},50);
   bins1 = linspace(trueval1-2,trueval1+2,20);
   n = hist(post_samples(:,wp),bins1);
   n = n/sum(n);
   ksMarg = [ksMarg; n];
   wp = 2;
   bins2 = linspace(prior{2,3},prior{2,4},50);
   bins2 = linspace(trueval2-0.5,trueval2+0.5,50);
   bins2 = linspace(trueval2-1,trueval2+2,20);
   n = hist(post_samples(:,wp),bins2);
   n = n/sum(n);
   AsMarg = [AsMarg; n];

   if a >= 3
      wp = 3;
      bins3 = linspace(prior{3,3},prior{3,4},20);
      n = hist(post_samples(:,wp),bins3);
      n = n/sum(n);
      np1Marg = [np1Marg; n];
   end

   if a >= 4
      wp = 4;
      bins4 = linspace(prior{4,3},prior{4,4},20);
      n = hist(post_samples(:,wp),bins4);
      n = n/sum(n);
      np2Marg = [np2Marg; n];
   end

   if a >= 5
      wp = 5;
      bins5 = linspace(prior{5,3},prior{5,4},20);
      n = hist(post_samples(:,wp),bins5);
      n = n/sum(n);
      np3Marg = [np3Marg; n];
   end

   if a >= 6
      wp = 6;
      bins6 = linspace(prior{6,3},prior{6,4},20);
      n = hist(post_samples(:,wp),bins6);
      n = n/sum(n);
      np4Marg = [np4Marg; n];
   end


end

legendNames = {'Hopkins & Beacom 2006','Fardal et al. 2007','Wilkins et al. 2008','Nagamine et al. 2006','Hernquist & Springel 2003'};

xlab = get_name(prior{1,1});
ylab = get_name(prior{2,1});

plotName = [plotDir '/' prior{1,1} '_marg'];
figure()
hold all
for i = 1:length(analysisTypes)
   plot(bins1,ksMarg(i,:));
end
plot([trueval1 trueval1],[0 1.1*max(ksMarg(:))],'k--');
hold off
xlabel(xlab);
ylabel('Marginalized Posteriors');
%ylim([0 0.1]);
leg1 = legend(legendNames);
set(leg1,'Location','northeast');
ylim([0 0.3]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{1,1} '_marg_exp'];
figure()
hold all
for i = 1:length(analysisTypes)
   plot(10.^bins1,ksMarg(i,:));
end
plot([trueval1 trueval1],[0 1.1*max(ksMarg(:))],'k--');
hold off
xlabel(xlab);
ylabel('Marginalized Posteriors');
%ylim([0 0.1]);
leg1 = legend(legendNames);
set(leg1,'Location','northeastoutside');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{2,1} '_marg'];
figure()
hold all
for i = 1:length(analysisTypes)
   plot(bins2,AsMarg(i,:));
end
plot([trueval2 trueval2],[0 1.1*max(AsMarg(:))],'k--');
hold off
xlabel(ylab);
ylabel('Marginalized Posteriors');
leg1 = legend(legendNames);
set(leg1,'Location','northeast');
ylim([0 0.3]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{2,1} '_marg_exp'];
figure()
hold all
for i = 1:length(analysisTypes)
   plot(bins2,AsMarg(i,:));
end
plot(bins2,AsMarg(i,:),'k--');
plot([trueval2 trueval2],[0 1.1*max(AsMarg(:))],'k--');
hold off
xlabel(ylab);
ylabel('Marginalized Posteriors');
%ylim([0 0.1]);
leg1 = legend(legendNames);
set(leg1,'Location','northeastoutside');
print('-dpng',plotName);
print('-depsc2',plotName);
close;
