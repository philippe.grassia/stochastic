
psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_test';
plotDir = baseplotDir;
createpath(plotDir);


spectraFile = '/home/mcoughlin/matapps/packages/stochastic/trunk/CrossCorr/noisecurves/ZERO_DET_high_P_psd.txt';
spectra = load(spectraFile);
spectra_f = spectra(:,1); spectra_h = spectra(:,2);

fs = 512;
T = 2048;
mc = [];
transfer = load(spectraFile);

mc.transfer(:,1) = spectra_f;
mc.transfer(:,2) = spectra_h;

%vector = create_noise(fs,T,mc);
averages = 100;
t = 0:(1/fs):T;

window = hann(fs*T);

for j = 1:averages

   vector = gaussian_noise(spectra,fs,T);
   %vector = vector.*window;

   L = length(t);
   NFFT = 2^nextpow2(L); % Next power of 2 from length of y
   f = fs/2*linspace(0,1,NFFT/2+1);

   if j == 1
      total_single_sided_unfiltered = zeros(length(f),1)';
   end

   sT = floor(NFFT/fs);
   windowconst = 0.375;

   Y = fft(vector,NFFT);

   r1 = constructTimeSeries(vector, 0, 1/fs, NaN, NaN);

   numPoints1    = length(r1.data);
   params.fft1.dataWindow   = hann(numPoints1);
   params.fft1.fftLength    = 2*numPoints1;
   Y = windowAndFFT(r1,params.fft1.dataWindow,params.fft1.fftLength);
   Y = Y.data;

   single_sided_unfiltered  = abs(Y(1:NFFT/2+1)).^2;
   %single_sided_unfiltered = 2*(abs(Y(1:NFFT/2+1)).^2)/(L*fs*windowconst);

   total_single_sided_unfiltered = total_single_sided_unfiltered + single_sided_unfiltered';
end
single_sided_unfiltered = total_single_sided_unfiltered / averages;
single_sided_unfiltered = total_single_sided_unfiltered;

   indexes = find(f <= 1024 & f>=1);
   f = f(indexes);
   single_sided_unfiltered = single_sided_unfiltered(indexes);

figure;
loglog(f,single_sided_unfiltered,'b')
hold on
loglog(spectra_f,spectra_h*averages*L*windowconst/(2*fs),'g')
hold off
hleg1 = legend('unfiltered','psd');
set(hleg1,'Location','NorthWest')
set(hleg1,'Interpreter','none')

xlim([10,256])
xlabel('Frequency [Hz]');
ylabel('');
print('-dpng',[plotDir '/psd.png'])
close;


