function bayesian(paramsFile, jobsFile, jobNumber)

tic;
ddmmyyyyhhmmss  = datestr(now);

% read in params structure from a file
params = readParamsFromFile(paramsFile);
params.ddmmyyyyhhmmss=ddmmyyyyhhmmss;
% add command line parameters to params struct
params.paramsFile = paramsFile;
params.jobsFile   = jobsFile;
% convert string to numeric input argument (needed for compiled matlab)
params.jobNumber = strassign(jobNumber);

jobs = load(params.jobsFile);
job = jobs(params.jobNumber,:);
gpsStart = job(2); gpsEnd = job(3); duration = job(4);

params.gpsTimesFile1 = ...
    [params.gpsTimesPath1 'gpsTimes' params.ifo1 '.' num2str(1) '.txt'];
params.gpsTimesFile2 = ...
    [params.gpsTimesPath2 'gpsTimes' params.ifo2 '.' num2str(1) '.txt'];
params.frameCacheFile1 = ...
    [params.frameCachePath1 'frameFiles' params.ifo1 '.' num2str(1) '.txt'];
params.frameCacheFile2 = ...
    [params.frameCachePath2 'frameFiles' params.ifo2 '.' num2str(1) '.txt'];

% channel names
params.channelName1 = [params.ifo1 ':' params.ASQchannel1];
params.channelName2 = [params.ifo2 ':' params.ASQchannel2];

try
  params.site1;
catch
  params.site1 = getsitefromletter(params.ifo1(1));
end;
try
  params.site2;
catch
  params.site2 = getsitefromletter(params.ifo2(1));
end;

% construct filter coefficients for high-pass filtering
if params.doHighPass1
  [params.filt1.b,params.filt1.a] = butter(params.highPassOrder1, params.highPassFreq1/(params.resampleRate1/2), 'high');
end

if params.doHighPass2
  [params.filt2.b,params.filt2.a] = butter(params.highPassOrder2, params.highPassFreq2/(params.resampleRate2/2), 'high');
end

params.detector1 = getdetector(params.site1);
params.detector2 = getdetector(params.site2);

[r1, data1OK] = readTimeSeriesData(params.channelName1,...
                              gpsStart, duration,...
                              params.frameType1, params.frameDuration1,...
                              params.gpsTimesFile1, params.frameCacheFile1);
[r2, data2OK] = readTimeSeriesData(params.channelName2,...
                              gpsStart, duration,...
                              params.frameType2, params.frameDuration2,...
                              params.gpsTimesFile2, params.frameCacheFile2);
adcdata1 = r1; adcdata2 = r2;
adcdata1.fbase = NaN; 
adcdata2.fbase = NaN;
adcdata1.phase = NaN;
adcdata2.phase = NaN;

% downsample the data
sampleRate1 = 1/adcdata1.deltaT;
sampleRate2 = 1/adcdata2.deltaT;
p1 = 1;
p2 = 1;
q1 = floor(sampleRate1/params.resampleRate1);
q2 = floor(sampleRate2/params.resampleRate2);
deltaT1 = 1/params.resampleRate1;
deltaT2 = 1/params.resampleRate2;

if sampleRate1 == params.resampleRate1
   data = adcdata1.data;
else
   data = resample(adcdata1.data, p1, q1, params.nResample1, params.betaParam1);
end
n1 = constructTimeSeries(data, adcdata1.tlow, deltaT1, ...
   adcdata1.fbase, adcdata1.phase);

if sampleRate2 == params.resampleRate2
   data = adcdata2.data;
else
   data = resample(adcdata2.data, p2, q2, params.nResample2, params.betaParam2);
end
n2 = constructTimeSeries(data, adcdata2.tlow, deltaT2, ...
   adcdata2.fbase, adcdata2.phase);

if params.doHighPass1
   r1 = constructTimeSeries(filtfilt(params.filt1.b,params.filt1.a,n1.data), ...
      n1.tlow, n1.deltaT, ...
      n1.fbase, n1.phase);
else
   r1 = n1;
end
        
if params.doHighPass2
   r2 = constructTimeSeries(filtfilt(params.filt2.b,params.filt2.a,n2.data), ...
      n2.tlow, n2.deltaT, ...
      n2.fbase, n2.phase);
else
   r2 = n2;
end

numPoints1    = length(r1.data);
params.fft1.dataWindow   = hann(numPoints1);
params.fft1.fftLength    = 2*numPoints1;

numPoints2    = length(r2.data);
params.fft2.dataWindow   = hann(numPoints2);
params.fft2.fftLength    = 2*numPoints2;

rbartilde1 = windowAndFFT(r1,params.fft1.dataWindow,params.fft1.fftLength);
rbartilde2 = windowAndFFT(r2, params.fft2.dataWindow, params.fft2.fftLength);

ff = rbartilde1.flow + (0:length(rbartilde1.data)-1)*rbartilde1.deltaF;
deltaF = rbartilde1.deltaF;
params.numFreqs = floor((ff(end) - params.flow - 1)/params.deltaF);

% coarse-grain noise power spectra to desired freqs
r1r1 = rbartilde1;
r1r1.data = rbartilde1.data.*conj(rbartilde1.data);
r1r1 = coarseGrain(r1r1, params.flow, params.deltaF, params.numFreqs);
r1r1.data = (params.deltaF/deltaF) * r1r1.data;

r1r2 = rbartilde1;
r1r2.data = rbartilde1.data.*conj(rbartilde2.data);
r1r2 = coarseGrain(r1r2, params.flow, params.deltaF, params.numFreqs);
r1r2.data = (params.deltaF/deltaF) * r1r2.data;

r2r1 = rbartilde1;
r2r1.data = rbartilde2.data.*conj(rbartilde1.data);
r2r1 = coarseGrain(r2r1, params.flow, params.deltaF, params.numFreqs);
r2r1.data = (params.deltaF/deltaF) * r2r1.data;

r2r2 = rbartilde1;
r2r2.data = rbartilde2.data.*conj(rbartilde2.data);
r2r2 = coarseGrain(r2r2, params.flow, params.deltaF, params.numFreqs);
r2r2.data = (params.deltaF/deltaF) * r2r2.data;

rbartilde1 = coarseGrain(rbartilde1, params.flow, params.deltaF, params.numFreqs);
rbartilde2 = coarseGrain(rbartilde2, params.flow, params.deltaF, params.numFreqs);
rbartilde1.data = (params.deltaF/deltaF) * rbartilde1.data;
rbartilde2.data = (params.deltaF/deltaF) * rbartilde2.data;

ff = rbartilde1.flow + (0:length(rbartilde1.data)-1)*rbartilde1.deltaF;
orf  = overlapreductionfunction(ff, params.detector1, params.detector2);

indexes = find(ff >= params.flow & ff<= params.fhigh);
ff = ff(indexes);
rbartilde1 = rbartilde1.data(indexes);
rbartilde2 = rbartilde2.data(indexes);
r1r1 = r1r1.data(indexes);
r1r2 = r1r2.data(indexes);
r2r1 = r2r1.data(indexes);
r2r2 = r2r2.data(indexes);
orf = orf(indexes);

filename = [params.outputFilePrefix '_' num2str(params.jobNumber) '.mat'];
save(filename,'ff','rbartilde1','rbartilde2','r1r1','r1r2','r2r1','r2r2','orf');

