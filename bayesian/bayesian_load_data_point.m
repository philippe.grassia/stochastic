function [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);

createpath(plotDir);
matFile = [plotDir '/data.mat'];
loadData = 0;
loadData = 1;

psd1 = []; psd2 = [];

if loadData

   %gpss = 0:60:86164;
   gpss = 0:30:86164-30;
   siderealtime=GPStoGreenwichMeanSiderealTime(gpss);

   outputfiles = dir([fileDir '/output_*.mat']);
   numsegs = zeros(length(siderealtime),1);
   numsegstot = 0;
   %outputfiles = outputfiles(1:10);

   %psd1 = []; psd2 = [];

   for i = 1:length(outputfiles)
   %for i = 1:100
      fprintf('%d\n',i);
      outputfile = [fileDir '/' outputfiles(i).name];
      data_out = load(outputfile);

      windowconst = 0.375;
      fs = 512; T = 20480;
      L = fs*T;
      deltaT = 1/fs; 
      %T = 1;

      if i==1
         ff = data_out.ff';
         orf = data_out.orf';

         rbartilde1 = zeros(length(ff),length(siderealtime));
         rbartilde2 = zeros(length(ff),length(siderealtime));
         r1r1 = zeros(length(ff),length(siderealtime));
         r1r2 = zeros(length(ff),length(siderealtime));
         r2r1 = zeros(length(ff),length(siderealtime));
         r2r2 = zeros(length(ff),length(siderealtime));
         psd1 = zeros(length(ff),1);
         psd2 = zeros(length(ff),1);
      end

      gpss = data_out.gpss;
      for j = 1:length(gpss)-2
         gps = gpss(j);
         [junk,index] = min(abs(siderealtime-GPStoGreenwichMeanSiderealTime(gps)));

         rbartilde1(:,index) = rbartilde1(:,index) + data_out.rbartilde1(:,j);
         rbartilde2(:,index) = rbartilde2(:,index) + data_out.rbartilde2(:,j);
         r1r1(:,index) = r1r1(:,index) + data_out.r1r1(:,j);
         r1r2(:,index) = r1r2(:,index) + data_out.r1r2(:,j);
         r2r1(:,index) = r2r1(:,index) + data_out.r2r1(:,j);
         r2r2(:,index) = r2r2(:,index) + data_out.r2r2(:,j);

         psd1 = psd1 + (2/T)*data_out.r1r1(:,j);
         psd2 = psd2 + (2/T)*data_out.r2r2(:,j);

         numsegs(index) = numsegs(index) + 1;
         numsegstot = numsegstot + 1;

      end

   end

   for i = 1:length(siderealtime)
      r1r1(:,i) = r1r1(:,i) / numsegs(i);
      r1r2(:,i) = r1r2(:,i) / numsegs(i);
      r2r1(:,i) = r2r1(:,i) / numsegs(i);
      r2r2(:,i) = r2r2(:,i) / numsegs(i);
   end
   psd1 = psd1 / numsegstot;
   psd2 = psd2 / numsegstot;

   save(matFile);
else
   load(matFile);
end

