function [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);

createpath(plotDir);
matFile = [plotDir '/data.mat'];
loadData = 0;
loadData = 1;

psd1 = []; psd2 = [];

if loadData
   outputfiles = dir([fileDir '/output_*.mat']);
   %outputfiles = outputfiles(1:10);

   %psd1 = []; psd2 = [];

   for i = 1:length(outputfiles)
   %for i = 1:100
      fprintf('%d\n',i);
      outputfile = [fileDir '/' outputfiles(i).name];
      data_out = load(outputfile);

      windowconst = 0.375;
      fs = 512; T = 20480;
      L = fs*T;
      deltaT = 1/fs; 
      T = 1;

      if i==1
         ff = data_out.ff';
         rbartilde1 = data_out.rbartilde1;
         rbartilde2 = data_out.rbartilde2;
         r1r1 = data_out.r1r1;
         r1r2 = data_out.r1r2;
         r2r1 = data_out.r2r1;
         r2r2 = data_out.r2r2;
         orf = data_out.orf';
    
         %psd1 = 2*(abs(data_out.rbartilde1).^2)/(L*fs*windowconst); 
         %psd2 = 2*(abs(data_out.rbartilde2).^2)/(L*fs*windowconst);

         %psd1 = (2/T)*(abs(data_out.rbartilde1).^2);
         %psd2 = (2/T)*(abs(data_out.rbartilde2).^2);

         psd1 = (2/T)*data_out.r1r1;
         psd2 = (2/T)*data_out.r2r2;

      else
         rbartilde1 = rbartilde1 + data_out.rbartilde1;
         rbartilde2 = rbartilde2 + data_out.rbartilde2;
         r1r1 = r1r1 + data_out.r1r1;
         r1r2 = r1r2 + data_out.r1r2;
         r2r1 = r2r1 + data_out.r2r1;
         r2r2 = r2r2 + data_out.r2r2;

         %psd1 = psd1 + 2*(abs(data_out.rbartilde1).^2)/(L*fs*windowconst);
         %psd2 = psd2 + 2*(abs(data_out.rbartilde2).^2)/(L*fs*windowconst);

         %psd1 = psd1 + (2/T)*(abs(data_out.rbartilde1).^2);
         %psd2 = psd2 + (2/T)*(abs(data_out.rbartilde2).^2);

         psd1 = psd1 + (2/T)*data_out.r1r1;
         psd2 = psd2 + (2/T)*data_out.r2r2;

      end

   end

   ff = ff';
   orf = orf';

   r1r1 = r1r1 / (length(outputfiles) * windowconst);
   r1r2 = r1r2 / (length(outputfiles) * windowconst);
   r2r1 = r2r1 / (length(outputfiles) * windowconst);
   r2r2 = r2r2 / (length(outputfiles) * windowconst);
   psd1 = psd1 / (length(outputfiles) * windowconst);
   psd2 = psd2 / (length(outputfiles) * windowconst);

   save(matFile);
else
   load(matFile);
end

