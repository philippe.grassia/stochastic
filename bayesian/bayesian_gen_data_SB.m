function [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = ...
   bayesian_gen_data_SB(injSNR);

sampleRate = 512;
deltaT = 1/sampleRate;
duration = 1;
T = duration;
N = sampleRate * duration;
ff = 1:1024;
psd_vals = ones(size(ff));
noise = [ff; psd_vals]';
fac = sqrt(3.7941e-39/injSNR);
%fac = 1;

det1 = getdetector('LHO');
det2 = getdetector('LLO');

h = gaussian_noise(noise, sampleRate, duration);
[h1, h2] = stoch_simulateSB(0, 1/sampleRate, 1/sampleRate, N, N,...
                               'const', det1, det2, ...
                               1, 1, 0, 0,...
                               0, 0, NaN, NaN, 1);
h1 = h1.data; h2 = h2.data;
h = h1;
n1 = gaussian_noise(noise, sampleRate, duration)*fac;
n2 = gaussian_noise(noise, sampleRate, duration)*fac;
t = (0:length(h)-1)/sampleRate;

%h = zeros(size(h));
h = h;
s1 = n1+h1; s2 = n2+h2;

L = length(t);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
ff = sampleRate/2*linspace(0,1,NFFT/2+1);

rbartilde1 = fft(s1,NFFT); 
rbartilde2 = fft(s2,NFFT);
rbartilde1 = rbartilde1(1:NFFT/2+1)*deltaT; 
rbartilde2 = rbartilde2(1:NFFT/2+1)*deltaT;
psd1 = (2/T)*(abs(rbartilde1).^2);
psd2 = (2/T)*(abs(rbartilde2).^2);

nbartilde1 = fft(n1,NFFT);
nbartilde2 = fft(n2,NFFT);
nbartilde1 = nbartilde1(1:NFFT/2+1)*deltaT;
nbartilde2 = nbartilde2(1:NFFT/2+1)*deltaT;
npsd1 = (2/T)*(abs(nbartilde1).^2);
npsd2 = (2/T)*(abs(nbartilde2).^2);

hbartilde1 = fft(h,NFFT);
hbartilde1 = hbartilde1(1:NFFT/2+1)*deltaT;
hpsd1 = (2/T)*(abs(hbartilde1).^2);

r1r1 = rbartilde1.*conj(rbartilde1);
r1r2 = rbartilde1.*conj(rbartilde2);
r2r1 = rbartilde2.*conj(rbartilde1);
r2r2 = rbartilde2.*conj(rbartilde2);

ff = ff';
%orf = ones(size(ff));
orf = overlapreductionfunction(ff,det1,det2);

H0 = 100e3/3.09e22;
df = ff(2)-ff(1);
indexes = find(ff >= 10 & ff<=255);

Omega_GW = ((10*pi^2)/(3*H0^2)).*ff.^3 .*hpsd1;
SNR = ((3*H0^2)/(10*pi^2))*sqrt(T)* ...
   sqrt(2*sum(df * (orf(indexes).^2 .* Omega_GW(indexes).^2) ...
   ./ (ff(indexes).^6 .* npsd1(indexes) .* npsd2(indexes))));
%SNR = SNR/fac^2;
fprintf('SNR optimal: %.3e\n',SNR);

ff = ff(indexes);
rbartilde1 = rbartilde1(indexes);
rbartilde2 = rbartilde2(indexes);
r1r1 = r1r1(indexes);
r1r2 = r1r2(indexes);
r2r1 = r2r1(indexes);
r2r2 = r2r2(indexes);
orf = orf(indexes);
psd1 = psd1(indexes);
psd2 = psd2(indexes);
npsd1 = npsd1(indexes);
npsd2 = npsd2(indexes);
hpsd1 = hpsd1(indexes);

