function mnPE(jobNumber,type);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots';
plotDir = [baseplotDir '/' type '/' num2str(jobNumber)];
createpath(plotDir);

if strcmp(type,'MDC')
   outputfile = ['/home/mcoughlin/Stochastic/Bayesian/output/output_' num2str(jobNumber)];
elseif strcmp(type,'inj')
   outputfile = ['/home/mcoughlin/Stochastic/Bayesian/output_inj/output_' num2str(jobNumber)];
end

data_out = load(outputfile);

ff = data_out.ff';
rbartilde1 = data_out.rbartilde1;
rbartilde2 = data_out.rbartilde2;
orf = data_out.orf';

Sh = powerlaw(ff,-24,2/3);
%Sh = zeros(size(Sh));

psd = load(psdfile);
Sn = interp1(psd(:,1),psd(:,2),ff);

C11 = Sn + Sh; 
C12 = orf.*Sh;
C21 = orf.*Sh;
C22 = Sn + Sh;

L = length(C11);
C = zeros(2,2,L);
C(1,1,:) = C11;
C(1,2,:) = C12;
C(2,1,:) = C21;
C(2,2,:) = C22;
Cinv = zeros(size(C));

ri = zeros(L,2);
ri(:,1) = rbartilde1;
ri(:,2) = rbartilde2;
rj = conj(ri)';

Mat=reshape(C,[],L);
detC =Mat(1,:).*Mat(4,:) - Mat(2,:).*Mat(3,:);

logLike = 0;
for i = []
%for i = 1:length(ff)
   Cinv = inv(C(:,:,i));
   expTerm = -2 * ri(i,:) * Cinv * rj(:,i);
   logLike = logLike + log((1/(2*pi)^2)*(1/detC(i))) + expTerm;
end

logL = real(logLike);

%rbartilde1_res = data_out.rbartilde1 - Sh;
%rbartilde2_res = data_out.rbartilde2 - Sh;

params.savePlots = 1;

if params.savePlots

   plotName = [plotDir '/absfft'];
   figure;
   loglog(ff,abs(rbartilde1).*(ff.^3),'k--');
   hold on
   loglog(ff,abs(rbartilde2),'b');
   loglog(ff,powerlaw(ff,-6,3-(2/3)),'r');
   hold off
   xlabel('Frequency [Hz]');
   ylabel('FFT');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   plotName = [plotDir '/absC'];
   figure;
   loglog(ff,abs(C11),'k--');
   hold on
   loglog(ff,abs(C21),'b');
   hold off
   xlabel('Frequency [Hz]');
   ylabel('PSD');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;
end




