function mnPE(jobNumber,analysisType,dataSet,doRun,injSNR);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_mn';
plotDir = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber) '/' num2str(injSNR)];
createpath(plotDir);

if strcmp(dataSet,'simple')
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = bayesian_gen_data(injSNR);
elseif strcmp(dataSet,'simpleSB')
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = bayesian_gen_data_SB(injSNR);
elseif strcmp(dataSet,'simplePoint')
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = bayesian_gen_data_point(injSNR,plotDir);
elseif strcmp(dataSet,'simpleCBC')
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = bayesian_gen_data_CBC(injSNR);
elseif strcmp(dataSet,'simpleCBCNagamine')
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = bayesian_gen_data_CBC_Nagamine(injSNR);
else
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_get_data(jobNumber,dataSet,plotDir);
   npsd1 = 1; npsd2 = 1; hpsd1 = 1;
end

[model,prior] = bayesian_get_model(dataSet,analysisType);
[ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,Sn1,Sn2,psd1,psd2,orf] = ...
   bayesian_get_Sn(jobNumber,dataSet,plotDir,injSNR,ff,rbartilde1,rbartilde2,...
      r1r1,r1r2,r2r1,r2r2,psd1,psd2,npsd1,npsd2,orf);
bayesian_get_diagnostic_plots(jobNumber,dataSet,plotDir,injSNR,ff,rbartilde1,rbartilde2,...
      r1r1,r1r2,r2r1,r2r2,psd1,psd2,npsd1,npsd2,orf,Sn1,Sn2);

global verbose;
verbose = 1;
global DEBUG;
DEBUG = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% define nested sampling parameters
Nlive = 200;

Nmcmc = 0;
%Nmcmc = 100;
%tolerance = 0.0001;
%tolerance = 1e-8;
%tolerance = 0.1;
%tolerance = 1000000;
%tolerance = 200000;
%tolerance = 20000;
%tolerance = 15000;
%tolerance = 50000;
%tolerance = 500;
%tolerance = 200;
%tolerance = 100;
%tolerance = 1;
%tolerance = 0.1;
tolerance = 0.01;
%tolerance = 1e-3;
%tolerance = 1e-4;
%tolerance = 1e-6;
%tolerance = 1e-10;
%tolerance = 100;

if strcmp(dataSet,'simplePoint') || strcmp(dataSet,'SBpoint')
   if strcmp(analysisType,'powerlawPSDpowerlaw')
      likelihood = @bayesian_logL_psd_multi_point;
   else
      likelihood = @bayesian_logL_multi_point;
   end
else
   if strcmp(analysisType,'powerlawPSDpowerlaw')
      likelihood = @bayesian_logL_psd_multi;
   elseif strcmp(analysisType,'powerlawPSD') || strcmp(analysisType,'DNSPSD')
      likelihood = @bayesian_logL_psdscale_multi;
   else
      likelihood = @bayesian_logL_multi;
   end
end

data = {};
data{1} = Sn1;
data{2} = Sn2;
data{3} = r1r1;
data{4} = r1r2;
data{5} = r2r1;
data{6} = r2r2;
data{7} = orf;
data{8} = ff;

extraparams = {};

params.plotdir = plotDir;
params.savePlots = 1;

params.doGPU = false;
params.doParallel = false;
if params.doParallel
  if matlabpool('size') == 0
    if params.doGPU
      fprintf('setting matlabpool open local 4\n');
      matlabpool open local 4
    else
      fprintf('setting matlabpool open local 8\n');
      matlabpool open local 8
    end
  end
end

[size_prior_x,size_prior_y] = size(prior);

matFile = [params.plotdir '/multinest.mat'];
if doRun
   % called nested sampling routine
   [logZ, nest_samples, post_samples] = bayesian_nested_sampler(data, Nlive, ...
      tolerance, likelihood, model, prior, extraparams, 'Nmcmc', Nmcmc);
   save(matFile);
else
   %load(matFile);
   load(matFile,'logZ','nest_samples','post_samples');
end

if strcmp(dataSet,'SBpoint') || strcmp(dataSet,'simplePoint')
   logL_background = 0;
else
   if strcmp(analysisType,'powerlaw');
      parnames = {'a','k'};
      parvals = {-100,0};
      logL_background = bayesian_logL_multi(data, model, parnames, parvals);
   elseif strcmp(analysisType,'powerlawPSD');
      parnames = {'a','k','A1','A2'};
      parvals = {-100,0,0,0};
      logL_background = bayesian_logL_psdscale_multi(data, model, parnames, parvals);
   else
      logL_background = 0;
   end
end

if params.savePlots

   % plot posterior distributions
   for i = 1:size_prior_x
      plotName = [params.plotdir '/PE_' prior{i,1}];
      bayesian_posteriors(post_samples, i, {prior{i,1}}, plotName, analysisType,dataSet);

      plotName = [params.plotdir '/PE_nest_post_' prior{i,1}];
      bayesian_nest_posteriors(nest_samples, i, {prior{i,1}}, plotName, analysisType,dataSet);

      plotName = [params.plotdir '/PE_nest_' prior{i,1}];
      bayesian_nest_samples_plot(nest_samples, i, {prior{i,1}}, plotName,analysisType,dataSet);

      for j = 1:size_prior_x
         if j<=i
            continue
         end

         plotName = [params.plotdir '/PE_' prior{i,1} '_' prior{j,1}];
         bayesian_posteriors(post_samples, [i j], {prior{i,1}, prior{j,1}}, plotName,analysisType,dataSet);
         plotName = [params.plotdir '/PE_logl_' prior{i,1} '_' prior{j,1}];
         %posteriors_logl(post_samples, [i j], {prior{i,1}, prior{j,1}}, plotName);

      end

   end

   %nest_samples = nest_samples(end-3000:end,:);
   %nest_samples = nest_samples(end-1000:end,:);
   nest_samples = nest_samples(floor(length(nest_samples(:,1))*0.1):end,:);
   if strcmp(analysisType,'powerlaw');

      a = median(nest_samples(:,1));
      k = median(nest_samples(:,2));
      %k = 2/3 -3;
      powerlaw_data = powerlaw(ff,a,k);

      [junk,index] = max(nest_samples(:,3));
      a = nest_samples(index,1);
      k = nest_samples(index,2);
      %k = 2/3 -3;
      %a = -39;
      powerlaw_data = powerlaw(ff,a,k);

      plotName = [plotDir '/omega_best_mdc'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,2*abs(rbartilde1.*rbartilde2),'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      loglog(ff,Sn1,'g');
      loglog(ff,Sn1,'r');
      %loglog(ff_omega,omega_psd_data,'r');
      hold off
      xlim([0 256]);
      %ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      [junk,index] = max(nest_samples(:,3));
      fprintf('a: %.5f, k: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3));
      fprintf('k: %.5f +- %.5f\n',median(nest_samples(:,2)),std(nest_samples(:,2)));
      fprintf('a: %.5e +- %.5e\n',10^median(nest_samples(:,1)),10^(median(nest_samples(:,1)))*log(10)*std(nest_samples(:,1)));
      fprintf('likelihood: %.5e, likelihood background: %.5e, likelihood ratio: %.5e\n',nest_samples(index,3),logL_background,nest_samples(index,3)-logL_background);

   elseif strcmp(analysisType,'powerlawPSD');

      a = median(nest_samples(:,1));
      k = median(nest_samples(:,2));
      A = median(nest_samples(:,3));
      %k = 2/3 -3;
      powerlaw_data = powerlaw(ff,a,k);

      [junk,index] = max(nest_samples(:,4));
      a = nest_samples(index,1);
      k = nest_samples(index,2);
      A = nest_samples(index,3);
      %k = 2/3 -3;
      %a = -39;
      powerlaw_data = powerlaw(ff,a,k);

      [junk,index] = max(nest_samples(:,4));
      fprintf('a: %.5f, k: %.5f, A: %.5f, likelihood: %.5e\n',nest_samples(index,1),nest_samples(index,2),nest_samples(index,3),nest_samples(index,4));
      fprintf('k: %.5f +- %.5f\n',median(nest_samples(:,2)),std(nest_samples(:,2)));
      fprintf('a: %.5e +- %.5e\n',10^median(nest_samples(:,1)),10^(median(nest_samples(:,1)))*log(10)*std(nest_samples(:,1)));
      fprintf('A: %.5e +- %.5e\n',10^median(nest_samples(:,3)),10^(median(nest_samples(:,3)))*log(10)*std(nest_samples(:,3)));
      fprintf('likelihood: %.5e, likelihood background: %.5e, likelihood ratio: %.5e\n',nest_samples(index,4),logL_background,nest_samples(index,4)-logL_background);

   elseif strcmp(analysisType,'powerlawPSDpowerlaw');

      a = median(nest_samples(:,1));
      k = median(nest_samples(:,2));
      A = median(nest_samples(:,3));
      K = median(nest_samples(:,4));
      %k = 2/3 -3;
      powerlaw_data = powerlaw(ff,a,k);

      [junk,index] = max(nest_samples(:,4));
      a = nest_samples(index,1);
      k = nest_samples(index,2);
      A = nest_samples(index,3);
      K = nest_samples(index,4);
      %k = 2/3 -3;
      %a = -39;
      powerlaw_data = powerlaw(ff,a,k);
      PSD_data = powerlaw(ff,A,K);

   end

   xlab = get_name(prior{1,1});
   ylab = get_name(prior{2,1});

   plotName = [plotDir '/contour'];
   % make 2-d histogram plot
   wp = [1 2];
   p1 = wp(1);
   p2 = wp(2);
   nbins = 50;
   edges1 = linspace(min(nest_samples(:,p1)), max(nest_samples(:,p1)), nbins);
   edges2 = linspace(min(nest_samples(:,p2)), max(nest_samples(:,p2)), nbins);
   histmat = hist2(nest_samples(:,p1), nest_samples(:,p2), edges1, edges2);

   figure()
   numcontours = 5;
   contourf(edges1, edges2, transpose(histmat), numcontours);
   %xlim([0 2e-9]);
   %ylim([0.4 0.7]);
   xlabel(xlab);
   ylabel(ylab);
   set(gca,'XScale','lin');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   plotName = [plotDir '/loglikelihood'];
   figure()
   scatter(nest_samples(:,1), nest_samples(:,2), 20, nest_samples(:,end),'filled');
   xlabel(xlab);
   ylabel(ylab);
   %cbar = colorbar;
   %set(get(cbar,'ylabel'),'String','Log Likelihood');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   lik = nest_samples(:,end);
   lik = lik - max(lik(:));
   Ls = 10.^lik;

   lik = 10
   plotName = [plotDir '/likelihood'];
   figure()
   scatter(nest_samples(:,1), nest_samples(:,2), 20, Ls,'filled');
   xlabel(xlab);
   ylabel(ylab);
   hold on
   m = 1.2188; lambda = log10(4.7933e-4);
   plot(m,lambda,'kX','markersize',20);
   hold off
   %cbar = colorbar;
   %set(get(cbar,'ylabel'),'String','Log Likelihood');
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

end

