
baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_2d';
plotDir = [baseplotDir];
createpath(plotDir);

ff = [1 10 100];
si = powerlaw(ff,0.5,1);
si = sqrt(si/2);
Sn = 0*ones(size(si));
%si = 2;
orf = 0.1;

model = @powerlaw_model;

data = {};
data{1} = Sn;
data{2} = si;
data{3} = si;
data{4} = orf;
data{5} = ff;

Sh_true = 2*si.*conj(si);

as = linspace(-4,4,100);
ks = linspace(-2,2,150);
logLs = [];

for i = 1:length(as)
   for j = 1:length(ks)
      a = as(i); k = ks(j);
      Sh = 2*powerlaw(ff,a,k).*powerlaw(ff,a,k);
      Sh = powerlaw(ff,a,k);
      %logLs(i,j) = sum((-4*si.*conj(si)./(Sh + orf*Sh + Sn)));
      logLs(i,j) = sum(log((1/(2*pi)^2) * 1./((1-orf^2).*Sh.^2 + 2*Sh.*Sn + Sn.^2)) + (-4*si.*conj(si)./(Sh + orf*Sh + Sn)));

      parnames = {'a','k'};
      parvals{1} = a; parvals{2} = k;

      logL = bayesian_logL(data, model, parnames, parvals);
      %fprintf('%.5e %.5e\n',logLs(i,j),logL);

      logLs(i,j) = logL;

   end
end

logLs = real(logLs);
Ls = 10.^logLs;
%Ls = logLs;

[X,Y] = meshgrid(as,ks);

plotName = [plotDir '/contour'];
figure()
pcolor(X,Y,Ls');
set(gca,'YDir','normal')
set(gca,'XScale','lin');
cbar = colorbar;
set(get(cbar,'title'),'String','log10(Likelihood)');
colorbar();
print('-dpng',plotName);
print('-depsc2',plotName);
close;

[C,I] = max(logLs(:));
[I1,I2] = ind2sub(size(logLs),I);
logLs_max = logLs(I1,I2);

as(I1)
ks(I2)


