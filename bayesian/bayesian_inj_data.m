function val = bayesian_inj_data(analysisType,dataSet,param);

val = NaN;
if strcmp(dataSet,'MDC2inj')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = 8 + -39.6 - 2.3424 + log10(0.0003662109) + log10(2); 
      elseif strcmp(param,'k')
         val = (2/3)-3;
      end
   end
elseif strcmp(dataSet,'MDC2inj_mid')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -41.6 - 2.3424 + log10(0.0003662109) + log10(2);
      elseif strcmp(param,'k')
         val = (2/3)-3;
      end
   end
elseif strcmp(dataSet,'MDC2inj_low')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -43.6 - 2.3424 + log10(0.0003662109) + log10(2);
      elseif strcmp(param,'k')
         val = (2/3)-3;
      end
   end
elseif strcmp(dataSet,'MDC2all')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -46.5;
      elseif strcmp(param,'k')
         val = (2/3)-3;
      elseif strcmp(param,'A1')
         val = 0;
      elseif strcmp(param,'A2')
         val = 0;
      end
   end
elseif strcmp(dataSet,'MDC2noise')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -46.5;
      elseif strcmp(param,'k')
         val = (2/3)-3;
      elseif strcmp(param,'A1')
         val = 0;
      elseif strcmp(param,'A2')
         val = 0;
      end
   elseif strcmp(analysisType,'DNSPSDH')
      if strcmp(param,'M')
         val = 1.2188;
      elseif strcmp(param,'lam')
         val = log10(4.7933e-4);
      elseif strcmp(param,'A1')
         val = 0;
      elseif strcmp(param,'A2')
         val = 0;
      end

   end
elseif strcmp(dataSet,'SB')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -30;
      elseif strcmp(param,'k')
         val = -3;
      end
   end
elseif strcmp(dataSet,'simplePoint')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD') || strcmp(analysisType,'powerlawPSDpowerlaw')
      if strcmp(param,'a')
         val = 4;
      elseif strcmp(param,'k')
         val = -3;
      elseif strcmp(param,'r')
         val = 6;
      elseif strcmp(param,'d')
         val = 30;
      end
   end
elseif strcmp(dataSet,'SBpoint')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = 4;
      elseif strcmp(param,'k')
         val = -3;
      elseif strcmp(param,'r')
         val = 6;
      elseif strcmp(param,'d')
         val = 30;
      end
   end
elseif strcmp(dataSet,'simpleCBC') || strcmp(dataSet,'simpleCBCNagamine')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      if strcmp(param,'a')
         val = -50.8;
      elseif strcmp(param,'k')
         val = 2/3 - 3;
      elseif strcmp(param,'A1')
         val = 0;
      elseif strcmp(param,'A2')
         val = 0;
      end
   elseif strcmp(analysisType,'DNSPSD')
      if strcmp(param,'M')
         val = 1.2188;
      elseif strcmp(param,'lam')
         val = log10(4.7933e-4);
      elseif strcmp(param,'A1')
         val = 0;
      elseif strcmp(param,'A2')
         val = 0;
      end
   end
end

