function mnPE(dataSet,gpsStart,duration);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_gendata';
plotDir = [baseplotDir '/' dataSet ];
createpath(plotDir);

frameDir = [baseplotDir '/' dataSet '/frames'];
createpath(frameDir);

sampleRate = 512;
%duration = 20480;
T = 60;
N = sampleRate * duration;
frameLength = duration;

ff = 1:1024;
det1 = getdetector('LHO');
det2 = getdetector('LLO');
gamma = overlapreductionfunction(ff,det1,det2);

h0 = 0.7;

if strcmp(dataSet,'SB')
   [h1, h2] = stoch_simulateSB(0, 1/sampleRate, 1/sampleRate, N, N,...
                               'const', det1, det2, ...
                               1, 1, 0, 0,...
                               0, 0, NaN, NaN, 1);
   h1 = h1.data; h2 = h2.data;
   h = h1;
   psd_data = zeros(size(ff));
   
elseif strcmp(dataSet,'SBpoint')

   Hf = [ff; powerlaw(ff,1,-3)]';
   Hf = [0 0; Hf];

   h1 = zeros(N,1); h2 = zeros(N,1);
   gpss = gpsStart:T:gpsStart+duration;

   for i = 1:length(gpss)-1
      intLog = 0;
      gps = gpss(i);
      siderealtime=GPStoGreenwichMeanSiderealTime(gps);
      ra = 6;
      decl = 30;
      power = 1;

      [h1_temp, h2_temp] = simulatePointSource(T,sampleRate,Hf,intLog,det1,det2,siderealtime,ra,decl,power);
      indexes = (i-1)*T*sampleRate + 1:i*T*sampleRate;
      h1(indexes) = h1_temp;
      h2(indexes) = h2_temp;
   end
   h = h1;
   psd_data = zeros(size(ff));
elseif strcmp(dataSet,'SBpointnone')
   h1 = zeros(N,1); h2 = zeros(N,1);
   
   psd = load(psdfile);
   psd_data = interp1(psd(:,1),psd(:,2).^2,ff);
   psd_data(isnan(psd_data)) =0;

   h = h1;

else
   h1 = h;
   h2 = h;
end

n1 = gaussian_noise([ff; psd_data]', sampleRate, duration);
n2 = gaussian_noise([ff; psd_data]', sampleRate, duration);
t = (0:length(h)-1)/sampleRate;

%h = zeros(size(h));
h = h;
s1 = n1+h1; s2 = n2+h2;

L = length(t);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
f = sampleRate/2*linspace(0,1,NFFT/2+1);

%s1 = fft(s1,NFFT); s2 = fft(s2,NFFT);
%s1 = 2*s1(1:NFFT/2+1); s2 = 2*s2(1:NFFT/2+1);

n1 = fft(n1,NFFT); n2 = fft(n2,NFFT);
n1 = n1(1:NFFT/2+1); n2 = n2(1:NFFT/2+1);

h = fft(h.*hann(length(h)),NFFT);
h = h(1:NFFT/2+1);

windowconst = 0.375;
h_psd = 2*(abs(h).^2)/(L*sampleRate*windowconst);

indexes = find(f >= 10 & f<=256);

f = f(indexes);
h = h(indexes);
n1 = n1(indexes);
n2 = n2(indexes);
h_psd = h_psd(indexes);

plotName = [plotDir '/fft'];
figure;
loglog(f,abs(h),'k');
hold on
loglog(f,abs(n1),'b');
loglog(f,abs(n2),'r');
%loglog(f,powerlaw(f,-3,((2/3)-3)/2),'m--');
hold off
xlabel('Frequency [Hz]');
ylabel('FFT');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/psd'];
figure;
loglog(f,abs(h_psd),'k');
hold on
%loglog(ff,omega_psd_data,'b');
loglog(ff,psd_data,'r');
hold off
xlim([10 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('FFT');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

%Y = abs(sum(h));
%sigma = abs(sum(n1));
%fprintf('Y: %.5e, sigma: %.5e, SNR: %.5f\n',Y,sigma,Y/sigma);

indexes = find(ff >= 10 & 256 >= ff);
Y = sum(h_psd(indexes));
sigma = sum(psd_data(indexes));
fprintf('Y: %.5e, sigma: %.5e, SNR: %.5f\n',Y,sigma,Y/sigma);

mode = 'n';
data = [];
data.type = 'd';
data.mode = 'a';

file = sprintf('%s/%s/frames/H1-%d-%d.gwf',baseplotDir,dataSet,gpsStart,duration);
data.channel = 'H1:STRAIN';
data.data = s1;
mkframe(file, data, mode, frameLength, gpsStart);

file = sprintf('%s/%s/frames/L1-%d-%d.gwf',baseplotDir,dataSet,gpsStart,duration);
data.channel = 'L1:STRAIN';
data.data = s2;
mkframe(file, data, mode, frameLength, gpsStart);

file = sprintf('%s/%s/frames/H2-%d-%d.gwf',baseplotDir,dataSet,gpsStart,duration);
data.channel = 'H2:STRAIN';
data.data = s2;
mkframe(file, data, mode, frameLength, gpsStart);

file = sprintf('%s/%s/frames/V1-%d-%d.gwf',baseplotDir,dataSet,gpsStart,duration);
data.channel = 'V1:STRAIN';
data.data = s2;
mkframe(file, data, mode, frameLength, gpsStart);

%S1 = interp1(psd(:,1),psd(:,2),f)'; S2 = interp1(psd(:,1),psd(:,2),f)';
%N = sampleRate*duration;
%indexes = find(f >= 10 & f<=256);

%f = f(indexes);
%S1 = S1(indexes);
%S2 = S2(indexes);
%s1 = s1(indexes);
%s2 = s2(indexes);

%gamma = overlapreductionfunction(f,det1,det2)';
%Q = N*gamma./((f').^3 .* S1 .* S2);
%Q(1) = 0; Q(end) = 0;
%Y = sum(conj(s1).*s2.*Q);
%sigma = sqrt((1/2) * sum(S1.^2.*S2.^2.*abs(Q.^2)));

%fprintf('Y: %.5e, sigma: %.5e, SNR: %.5f\n',Y,sigma,Y/sigma);

