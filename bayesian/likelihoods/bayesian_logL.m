function logL = stamp_logL(data, model, parnames, parvals)

% logL = logL_gaussian(data, model, parnames, parvals)
%
% This function will compute the log likelihood of a multivariate
% gaussian:
%
%     L = 1/sqrt((2 pi)^N det C)
%         exp[-0.5*(y - model(x,params))^T * inv(C) * (y - model(x,params))]
%
% The input parameters are:
%     data - a cell array with three columns
%            { x values, y values, C: covariance matrix }
%     NOTE: if C is a single number, convert to a diag covariance matrix
%     model - the function handle for the signal model.
%     parnames - a cell array listing the names of the model parameters
%     parvals - a cell array containing the values of the parameters given
%         in parnames. These must be in the same order as in parnames. 
%         If parvals is an empty vector the noise-only likelihood will be 
%         calculated.
%
% -------------------------------------------------------------------------
%           This is the format required by nested_sampler.m.
% -------------------------------------------------------------------------

% check whether model is a string or function handle
if ischar(model)
    fmodel = str2func(model);
elseif isa(model, 'function_handle')
    fmodel = model;
else
    error('Error... Expecting a model function!');
end

% get data values from cell array
Sn = data{1};
rbartilde1 = data{2};
rbartilde2 = data{3};
orf = data{4};
ff = data{5};

% evaluate the model
if isempty(parvals)
    % if parvals is not defined get the null likelihood (noise model
    % likelihood)
    Sh = zeros(size(YY));
else
    Sh = feval(fmodel, ff, parnames, parvals);
    
    % if the model returns a NaN then set the likelihood to be zero (e.g. 
    % loglikelihood to be -inf
    if isnan(Sh)
        logL = -inf;
        return;
    end
end

params.doGPU = 1;
params.gpu.precision = 'double';
Sn = gArray(Sn,params);
Sh = gArray(Sh,params);
orf = gArray(orf,params);

C11 = Sn + Sh;
C12 = orf.*Sh;
C21 = orf.*Sh;
C22 = Sn + Sh;

ri1 = rbartilde1;
ri2 = rbartilde2;
rj1 = conj(ri1);
rj2 = conj(ri2);

detC =C11.*C22 - C12.*C21;
Cinv11 = C22./detC;
Cinv12 = -C12./detC;
Cinv21 = -C21./detC;
Cinv22 = C11./detC;

expTerm = -2*(Cinv11.*ri1.*rj1 + Cinv21.*ri2.*rj1 + Cinv12.*ri1.*rj2 + Cinv22.*ri2.*rj2);
%expTerm = -2*(Cinv12.*ri1.*rj2);
% Single sided FFT
expTerm = real(expTerm);
logLike = log((1/(pi/2)^2).*(1./detC)) + expTerm;
%logLike = log(sqrt((1/(2*pi)^2).*(1./detC))) + expTerm;

logL = real(sum(logLike));
logL = gGather(logL,params);

%fprintf('%.5e %.5e\n',sum(Sn),sum(Sh));

if isnan(logL)
   logL = -1e300;
%    error('Error: log likelihood is NaN!');
end

if logL < -1e300
   logL = -1e300;
end

%fprintf('%.2f %.2f %.2f %.5e\n',parvals{1},parvals{2},parvals{3},logL);

%logL = round(logL/10)*10;

%figure;
%semilogx(ff,logLike,'k*');
%print('-dpng','test.png');
%close;

return
