function bayesian_get_injplot(analysisType,dataSet);

hold on
if strcmp(dataSet,'MDC2inj')
   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD')
      a = -41.6 + log10(384); k = 2/3 - 3;
      plot(a,k,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC2inj_mid')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -43.6 + log10(384); k = 2/3 - 3;
      plot(a,k,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC2inj_low')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -44.6 + log10(384); k = 2/3 - 3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      a = log10(2e-8); k = 2/3;
      plot(a,k,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC2all')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
%      a = -45.6 + log10(0.0563964844) + log10(2); k = 2/3 - 3;
      a = -35.2; k = 2/3 - 3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.2188; lam = log10(4.7933e-4);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC3all')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -9.5; k = 2/3 - 3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 8.7055; lam = log10(1.2653e-5);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC4all')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -9.5; k = 2/3 - 3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.3715; lam = log10(4.6981e-4);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC5all')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -9.5; k = 2/3 - 3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.3715; lam = log10(4.6981e-4);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC6all')
   if strcmp(analysisType,'powerlaw');
      a = -9.5; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.3715; lam = log10(4.6981e-4);
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'ET')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -10.7; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.4; lam = -4.9;
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'ET_noDetect')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -12.9; k = 2/3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.4; lam = -7.1;
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'SB') || strcmp(dataSet,'SBnone') || strcmp(dataSet,'SBnoneflat')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -32.7; k = -3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.4; lam = -7.1;
      plot(M,lam,'kx','markersize',20);
   end
elseif strcmp(dataSet,'MDC2nonoise') || strcmp(dataSet,'MDC2nonoiseplus') || strcmp(dataSet,'MDC2noise') || strcmp(dataSet,'MDC2noiseflat') || strcmp(dataSet,'MDC2noise0')
   if strcmp(analysisType,'powerlaw')|| strcmp(analysisType,'powerlawPSD')
      a = -38.6; k = 2/3 - 3;
      plot(a,k,'kx','markersize',20);
   elseif strcmp(analysisType,'DNS');
      M = 1.2188; lam = log10(4.7933e-4);
      plot(M,lam,'kx','markersize',20);
   end
end
hold off

