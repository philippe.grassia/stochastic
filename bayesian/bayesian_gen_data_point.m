function [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = ...
   bayesian_gen_data_point(injSNR,plotDir);

createpath(plotDir);
matFile = [plotDir '/data.mat'];
loadData = 0;
loadData = 1;

if loadData

   sampleRate = 512;
   deltaT = 1/sampleRate;
   duration = 4;
   duration = 60;
   T = duration;
   N = sampleRate * duration;
   ff = 1:1024;
   psd_vals = ones(size(ff));
   noise = [ff; psd_vals]';
   fac = sqrt(3.489e+29/injSNR);
   fac = sqrt(2.846e+30/injSNR);
   fac = sqrt(2.185e-02/injSNR);
   %fac = sqrt(6.308e-02/injSNR);
   %fac = 1;
   
   det1 = getdetector('LHO');
   det2 = getdetector('LLO');
   
   h = gaussian_noise(noise, sampleRate, duration);
   Hf = [ff; powerlaw(ff,1,-3)]';
   Hf = [0 0; Hf];
   
   intLog = 0;
   ra = 6;
   decl = 30;
   power = 1;
   
   gpss = 0:30:86164-30;
   siderealtimes=GPStoGreenwichMeanSiderealTime(gpss);
   
   adcdata = [];
   adcdata.data   = 0;
   adcdata.flow   = 0;
   adcdata.deltaF = 0;
   
   %for i = 1:length(siderealtimes)
   for i = 1:10
      fprintf('%d/%d\n',i,length(siderealtimes));
   
      siderealtime = siderealtimes(i);
      [h1, h2] = simulatePointSource(T,sampleRate,Hf,intLog,det1,det2,siderealtime,ra,decl,power);
   
      h = h1;
      n1 = gaussian_noise(noise, sampleRate, duration)*fac;
      n2 = gaussian_noise(noise, sampleRate, duration)*fac;
      t = (0:length(h)-1)/sampleRate;
   
      %h = zeros(size(h));
      h = h;
      s1 = n1+h1; s2 = n2+h2;
   
      L = length(t);
      NFFT = 2^nextpow2(L); % Next power of 2 from length of y
      ff = sampleRate/2*linspace(0,1,NFFT/2+1);
      orf = overlapreductionfunction(ff,det1,det2);  
    
      rbartilde1 = fft(s1,NFFT); 
      rbartilde2 = fft(s2,NFFT);
      rbartilde1 = rbartilde1(1:NFFT/2+1)*deltaT; 
      rbartilde2 = rbartilde2(1:NFFT/2+1)*deltaT;
      psd1 = (2/T)*(abs(rbartilde1).^2);
      psd2 = (2/T)*(abs(rbartilde2).^2);
      
      nbartilde1 = fft(n1,NFFT);
      nbartilde2 = fft(n2,NFFT);
      nbartilde1 = nbartilde1(1:NFFT/2+1)*deltaT;
      nbartilde2 = nbartilde2(1:NFFT/2+1)*deltaT;
      npsd1 = (2/T)*(abs(nbartilde1).^2);
      npsd2 = (2/T)*(abs(nbartilde2).^2);
      
      hbartilde1 = fft(h,NFFT);
      hbartilde1 = hbartilde1(1:NFFT/2+1)*deltaT;
      hpsd1 = (2/T)*(abs(hbartilde1).^2);
      
      r1r1 = rbartilde1.*conj(rbartilde1);
      r1r2 = rbartilde1.*conj(rbartilde2);
      r2r1 = rbartilde2.*conj(rbartilde1);
      r2r2 = rbartilde2.*conj(rbartilde2);
  
      doCoarseGrain = 1;
      if doCoarseGrain
 
         y = r1r1;
         adcdata.data = rbartilde1;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         deltaF = ff(2) - ff(1);
         params.deltaF = 0.25;
         params.flow = 10;
         params.numFreqs = floor((ff(end) - params.flow - 1)/params.deltaF);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         rbartilde1 = (params.deltaF/deltaF) * adcdata.data;
         %rbartilde1 = adcdata.data;

         adcdata.data = rbartilde2;
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         rbartilde2 = (params.deltaF/deltaF) * adcdata.data;
         %rbartilde2 = adcdata.data;     

         adcdata.data = r1r1;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         r1r1 = (params.deltaF/deltaF) * adcdata.data;
         %r1r1 = adcdata.data;    

         adcdata.data = r1r2;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         r1r2 = (params.deltaF/deltaF) * adcdata.data;
         %r1r2 = adcdata.data;      

         adcdata.data = r2r1;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         r2r1 = (params.deltaF/deltaF) * adcdata.data;
         %r2r1 = adcdata.data;      

         adcdata.data = r2r2;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         %r2r2 = (params.deltaF/deltaF) * adcdata.data;
         r2r2 = adcdata.data;      

         adcdata.data = psd1;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         psd1 = (params.deltaF/deltaF) * adcdata.data;
         %psd1 = adcdata.data;      

         adcdata.data = psd2;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         psd2 = (params.deltaF/deltaF) * adcdata.data;
         %psd2 = adcdata.data;      

         adcdata.data = npsd1;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         npsd1 = (params.deltaF/deltaF) * adcdata.data;
         %npsd1 = adcdata.data;      

         adcdata.data = npsd2;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         npsd2 = (params.deltaF/deltaF) * adcdata.data;
         %npsd2 = adcdata.data;      

         adcdata.data = hpsd1;
         adcdata.deltaF = ff(2) - ff(1);
         adcdata.flow   = ff(1);
         adcdata = coarseGrain(adcdata, params.flow, params.deltaF, params.numFreqs);
         hpsd1 = (params.deltaF/deltaF) * adcdata.data;
         %hpsd1 = adcdata.data;

         %figure;
         %loglog(ff,abs(y),'r')
         %hold on
         %loglog(adcdata.flow + (0:length(adcdata.data)-1)*adcdata.deltaF,abs(r1r1),'b');
         %hold off
         %print('-dpng','test.png');
         %close;

         ff = adcdata.flow + (0:length(adcdata.data)-1)*adcdata.deltaF;
      end
      indexes = find(ff >= 10 & ff<=255);
   
      ff = ff(indexes);
      rbartilde1 = rbartilde1(indexes);
      rbartilde2 = rbartilde2(indexes);
      r1r1 = r1r1(indexes);
      r1r2 = r1r2(indexes);
      r2r1 = r2r1(indexes);
      r2r2 = r2r2(indexes);
      orf = orf(indexes);
      psd1 = psd1(indexes);
      psd2 = psd2(indexes);
      npsd1 = npsd1(indexes);
      npsd2 = npsd2(indexes);
      hpsd1 = hpsd1(indexes);
   
      if i==1
         ff_all = ff;
         orf_all = orf;
   
         rbartilde1_all = zeros(length(ff),length(siderealtimes));
         rbartilde2_all = zeros(length(ff),length(siderealtimes));
         r1r1_all = zeros(length(ff),length(siderealtimes));
         r1r2_all = zeros(length(ff),length(siderealtimes));
         r2r1_all = zeros(length(ff),length(siderealtimes));
         r2r2_all = zeros(length(ff),length(siderealtimes));
         psd1_all = zeros(length(ff),1);
         psd2_all = zeros(length(ff),1);
         npsd1_all = zeros(length(ff),1);
         npsd2_all = zeros(length(ff),1);
         hpsd1_all = zeros(length(ff),1);
      end
   
      rbartilde1_all(:,i) = rbartilde1;
      rbartilde2_all(:,i) = rbartilde2;
      r1r1_all(:,i) = r1r1;
      r1r2_all(:,i) = r1r2;
      r2r1_all(:,i) = r2r1;
      r2r2_all(:,i) = r2r2;
      psd1_all = psd1_all + psd1;
      psd2_all = psd2_all + psd2;
      npsd1_all = npsd1_all + npsd1;
      npsd2_all = npsd2_all + npsd2;
      hpsd1_all = hpsd1_all + hpsd1;

   end
   
   ff = ff_all';
   orf = orf_all';
   rbartilde1 = rbartilde1_all;
   rbartilde2 = rbartilde2_all;
   r1r1 = r1r1_all;
   r1r2 = r1r2_all;
   r2r1 = r2r1_all;
   r2r2 = r2r2_all;
   psd1 = psd1_all/length(siderealtimes);
   psd2 = psd2_all/length(siderealtimes);
   npsd1 = npsd1_all/length(siderealtimes);
   npsd2 = npsd2_all/length(siderealtimes);
   hpsd1 = hpsd1_all/length(siderealtimes);
   
   H0 = 100e3/3.09e22;
   df = ff(2)-ff(1);
   
   Omega_GW = ((10*pi^2)/(3*H0^2)).*ff.^3 .*hpsd1;
   SNR = ((3*H0^2)/(10*pi^2))*sqrt(T)* ...
      sqrt(2*sum(df * (orf.^2 .* Omega_GW.^2) ...
      ./ (ff.^6 .* npsd1 .* npsd2)));
   %SNR = SNR/fac^2;
   fprintf('SNR optimal: %.3e\n',SNR);
   save(matFile);
else
   load(matFile);
end   
