function mnPE(jobNumber,analysisType,dataSet,doRun,injSNR);

warning('off','all');
rng(1,'twister');

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_bf_1d';
plotDir = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber) '/' num2str(injSNR)];
createpath(plotDir);

%rbartilde1 = 150*rbartilde1;
%rbartilde2 = 150*rbartilde2;
if strcmp(dataSet,'simple')
   [ff,rbartilde1,rbartilde2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = bayesian_gen_data(injSNR);
elseif strcmp(dataSet,'simpleSB')
   [ff,rbartilde1,rbartilde2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = bayesian_gen_data_SB(injSNR);
else
   [ff,rbartilde1,rbartilde2,orf,psd1,psd2] = bayesian_get_data(jobNumber,dataSet,plotDir);
   npsd1 = 1; npsd2 = 1; hpsd1 = 1;
end

if strcmp(dataSet,'MDC0all') || strcmp(dataSet,'MDC2all') || strcmp(dataSet,'MDC3all') || strcmp(dataSet,'MDC4all') || strcmp(dataSet,'MDC5all')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDCDiff02all')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2nonoise')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2nonoiseplus')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2noise') || strcmp(dataSet,'MDC2noiseflat') || strcmp(dataSet,'MDC2noise0')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2inj') || strcmp(dataSet,'MDC3inj') || strcmp(dataSet,'MDC4inj') || strcmp(dataSet,'MDC5inj') || strcmp(dataSet,'MDC2inj_mid') || strcmp(dataSet,'MDC2inj_low') || strcmp(dataSet,'none')
   windowconst = 0.375;
   L = 2048*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1;
   deltaFconstant = 1;
   %deltaFconstant = 1024;
   orf = 1;
elseif strcmp(dataSet,'SB') || strcmp(dataSet,'SBnoise') || strcmp(dataSet,'SBnoiseflat')
   windowconst = 0.375;
   L = 2048*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1;
   deltaFconstant = 1;
   %deltaFconstant = 1024;
else
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1540;
   deltaFconstant = 10240;
end

a = -35;
k = 2/3 - 3;
%ff = [10 100 1000];
powerlaw_data = powerlaw(ff,a,k);
%rbartilde1 = powerlaw(ff,-40,2/3 - 3);
%rbartilde1 = sqrt(rbartilde1/2);
%rbartilde2 = rbartilde1;

%ff = [1 10 100];
si = powerlaw(ff,-40,2/3 - 3);
si = sqrt(si/2);
%rbartilde1 = si; rbartilde2 = si;
%rbartilde1 = abs(rbartilde1); rbartilde2 = abs(rbartilde2);
%Sn = 0*ones(size(si));
%Sn = 0.01*rbartilde1.^2;
%si = 2;
%orf = 1;
%rbartilde1 = si;
%rbartilde2 = si;
powerlaw_data = powerlaw(ff,a,k);

factor = averages*L*(windowconst/(2*fs*deltaFconstant.^2));
%fprintf('%.10f\n',factor);
psd = load(psdfile);
Sn = interp1(psd(:,1),psd(:,2),ff).^2;
Sn = Sn*factor;

if strcmp(dataSet,'MDC0all')
   Sn = Sn .* 10^0.92800;
elseif strcmp(dataSet,'MDC2all')
   Sn = Sn .* 10^0.92800;
elseif strcmp(dataSet,'MDC2inj')
   Sn = Sn .* 10^0;
elseif strcmp(dataSet,'MDC2inj_mid')
   Sn = Sn .* 10^0.82800;
elseif strcmp(dataSet,'MDC2inj_low')
   Sn = Sn .* 10^0.81600;
elseif strcmp(dataSet,'MDC2nonoise')
   Sn = Sn .* 10^-10;
elseif strcmp(dataSet,'MDC2nonoiseplus')
   Sn = Sn .* 10^-10;
end

[model,prior] = bayesian_get_model(dataSet,analysisType);
[rbartilde1,rbartilde2,Sn1,Sn2,Sn] = ...
   bayesian_get_Sn(jobNumber,dataSet,plotDir,injSNR,ff,rbartilde1,rbartilde2,psd1,psd2,npsd1,npsd2,orf);

%Sn = Sn*8;
%Sn = Sn*10;
%Sn = zeros(size(Sn));
%Sn = 0.01*abs(rbartilde1).^2;
%orf = 0;
%orf = ones(size(rbartilde1));
omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/omega.txt';
omega = load(omegafile);
ff_omega = omega(:,1);
omega_data = omega(:,2);
omega_psd_data = omega(:,3);
omega_psd_data = omega_psd_data*averages*L*(windowconst/(2*fs*deltaFconstant.^2));

%plotName = [plotDir '/semifft'];
%figure;
%semilogx(ff,real(rbartilde1.*rbartilde2),'k--');
%hold on
%semilogx(ff,orf*1e-42,'r');
%hold off
%xlim([0 256]);
%%ylim([1e5 1e15]);
%xlabel('Frequency [Hz]');
%ylabel('S_{h}');
%print('-dpng',plotName);
%print('-depsc2',plotName);
%close;

plotName = [plotDir '/fft'];
figure;
loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
%loglog(ff,abs(rbartilde1),'k--');
hold on
loglog(ff,abs(rbartilde1.*rbartilde1),'b');
loglog(ff,abs(rbartilde2.*rbartilde2),'r');
%loglog(ff,sqrt(powerlaw(ff,-39,2/3 - 3)/2),'r')
loglog(ff,Sn,'g');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/psd'];
figure;
loglog(ff,Sn1,'k--');
hold on
loglog(ff,Sn2,'b');
%loglog(ff,sqrt(powerlaw(ff,-39,2/3 - 3)/2),'r')
loglog(ff,psd1,'g');
loglog(ff,psd2,'m');
hold off
xlim([0 256]);
if strcmp(dataSet,'MDC2nonoise') || strcmp(dataSet,'MDC2nonoiseplus') || strcmp(dataSet,'MDC2noise') || strcmp(dataSet,'MDC2noiseflat') || strcmp(dataSet,'MDC2noise0')
   ylim([1e-48 1e-38]);
end
xlabel('Frequency [Hz]');
ylabel('PSD');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/omega'];
figure;
%loglog(ff,abs(rbartilde1).*abs(rbartilde2),'k--');
loglog(ff,2*abs(rbartilde1.*rbartilde2)/(L*fs*windowconst*deltaT*deltaT),'k--');
hold on
loglog(ff,powerlaw_data,'b');
loglog(ff_omega,omega_psd_data*1e10,'r');
loglog(ff,Sn,'g');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/omega_mdc'];
figure;
%loglog(ff,abs(rbartilde1).*abs(rbartilde2),'k--');
loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
hold on
%loglog(ff,powerlaw_data,'b');
loglog(ff_omega,omega_psd_data*averages*L*(windowconst/(2*fs*deltaFconstant.^2)),'r');
loglog(ff,Sn,'g');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

data = {};
data{1} = Sn1;
data{2} = Sn2;
data{3} = rbartilde1;
data{4} = rbartilde2;
data{5} = orf;
data{6} = ff;

%data = {};
%data{1} = Sn;
%data{2} = rbartilde1;
%data{3} = rbartilde2;
%data{4} = orf;
%data{5} = ff;

extraparams = {};

params.plotdir = plotDir;
params.savePlots = 1;

[size_prior_x,size_prior_y] = size(prior);

matFile = [params.plotdir '/multinest.mat'];
if doRun
   parnames = prior(:,1);
   parvalsmin = cell2mat(prior(:,3));
   parvalsmax = cell2mat(prior(:,4));
   num = 100;
   %num = 10;

   for i = 1:length(parnames)
      prior{i,6} = linspace(parvalsmin(i),parvalsmax(i),num+i);
   end

   lik1 = zeros(num+1,1);
   for i = 1:num+1
      parvals = {prior{1,6}(i) 0};
      %logL = bayesian_logL(data, model, parnames, parvals);
      logL = bayesian_logL_multi(data, model, parnames, parvals);
      lik1(i) = logL;
   end

   lik2 = zeros(num+2,1);
   for i = 1:num+2
      parvals = {0 prior{2,6}(i)};
      %logL = bayesian_logL(data, model, parnames, parvals);
      logL = bayesian_logL_multi(data, model, parnames, parvals);
      lik2(i) = logL;
   end

   save(matFile);
else
   %load(matFile);
   load(matFile,'prior','lik1','lik2');
end

parnames = {'a','k'};
parvals = {-100,0};
logL_background = bayesian_logL(data, model, parnames, parvals);

if params.savePlots

   norm = max(lik1(:));
   lik1 = exp(lik1 - norm);

   plotName = [plotDir '/' prior{1,1}];
   figure()
   plot(prior{1,6},lik1)
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   norm = max(lik2(:));
   lik2 = exp(lik2 - norm);

   plotName = [plotDir '/' prior{2,1}];
   figure()
   plot(prior{2,6},lik2)
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

end



