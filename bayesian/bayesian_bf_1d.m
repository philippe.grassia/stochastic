function mnPE(jobNumber,analysisType,dataSet,doRun);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_bf_1d';
plotDir = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber)];
createpath(plotDir);

%rbartilde1 = 150*rbartilde1;
%rbartilde2 = 150*rbartilde2;

[ff,rbartilde1,rbartilde2,orf,psd1,psd2] = bayesian_get_data(jobNumber,dataSet,plotDir);

if strcmp(dataSet,'MDC0all') || strcmp(dataSet,'MDC2all') || strcmp(dataSet,'MDC3all') || strcmp(dataSet,'MDC4all') || strcmp(dataSet,'MDC5all')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDCDiff02all')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2nonoise')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2nonoiseplus')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2inj') || strcmp(dataSet,'MDC3inj') || strcmp(dataSet,'MDC4inj') || strcmp(dataSet,'MDC5inj') || strcmp(dataSet,'MDC2inj_mid') || strcmp(dataSet,'MDC2inj_low') || strcmp(dataSet,'none')
   windowconst = 0.375;
   L = 2048*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1;
   deltaFconstant = 1;
   %deltaFconstant = 1024;
   orf = 1;
elseif strcmp(dataSet,'SB')
   windowconst = 0.375;
   L = 2048*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1;
   deltaFconstant = 1;
   %deltaFconstant = 1024;
else
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1540;
   deltaFconstant = 10240;
end

a = -35;
k = 2/3 - 3;
%ff = [10 100 1000];
powerlaw_data = powerlaw(ff,a,k);
%rbartilde1 = powerlaw(ff,-40,2/3 - 3);
%rbartilde1 = sqrt(rbartilde1/2);
%rbartilde2 = rbartilde1;

%ff = [1 10 100];
si = powerlaw(ff,-40,2/3 - 3);
si = sqrt(si/2);
%rbartilde1 = si; rbartilde2 = si;
%rbartilde1 = abs(rbartilde1); rbartilde2 = abs(rbartilde2);
%Sn = 0*ones(size(si));
%Sn = 0.01*rbartilde1.^2;
%si = 2;
%orf = 1;
%rbartilde1 = si;
%rbartilde2 = si;
powerlaw_data = powerlaw(ff,a,k);

factor = averages*L*(windowconst/(2*fs*deltaFconstant.^2));
fprintf('%.10f\n',factor);
psd = load(psdfile);
Sn = interp1(psd(:,1),psd(:,2),ff).^2;
Sn = Sn*factor;

if strcmp(dataSet,'MDC0all')
   Sn = Sn .* 10^0.92800;
elseif strcmp(dataSet,'MDC2all')
   Sn = Sn .* 10^0.92800;
elseif strcmp(dataSet,'MDC2inj')
   Sn = Sn .* 10^0;
elseif strcmp(dataSet,'MDC2inj_mid')
   Sn = Sn .* 10^0.82800;
elseif strcmp(dataSet,'MDC2inj_low')
   Sn = Sn .* 10^0.81600;
elseif strcmp(dataSet,'MDC2nonoise')
   Sn = Sn .* 10^-10;
elseif strcmp(dataSet,'MDC2nonoiseplus')
   Sn = Sn .* 10^-10;
end

if strcmp(dataSet,'MDC2nonoiseplus')
   [ff_noise,rbartilde1_noise,rbartilde2_noise,orf_noise,psd1_noise,psd2_noise] = ...
      bayesian_get_data(jobNumber,'MDC0all',strrep(plotDir,dataSet,'MDC0all'));

   fac = 1e-5;
   %fac = 1e-100;
   fac = 1e-2;
   %fac = 5e-2;
   fac = 1e-1;
   %fac = 1.5e-1;
   %fac = 2e-1;
   %fac = 1;  

   rbartilde1_noise = rbartilde1_noise*fac;
   rbartilde2_noise = rbartilde2_noise*fac;

   rbartilde1 = rbartilde1 + rbartilde1_noise;
   rbartilde2 = rbartilde2 + rbartilde2_noise;
   Sn = abs(rbartilde1_noise.*conj(rbartilde1_noise)) + abs(rbartilde2_noise.*conj(rbartilde2_noise));
   Sn = 1.23*0.73*sqrt(2)*4*Sn;
   %Sn = 4*Sn;

   Sn1 = (2)*0.73*sqrt(2)*4*abs(rbartilde1_noise.*conj(rbartilde1_noise));
   Sn2 = (2)*0.73*sqrt(2)*4*abs(rbartilde2_noise.*conj(rbartilde2_noise));

   Sn1 = (2)*0.73*sqrt(2)*4*abs(rbartilde1_noise.*conj(rbartilde1_noise));
   Sn2 = (2)*0.73*sqrt(2)*4*abs(rbartilde2_noise.*conj(rbartilde2_noise));

   Sn1 = (2)*4*abs(rbartilde1_noise.*conj(rbartilde1_noise));
   Sn2 = (2)*4*abs(rbartilde2_noise.*conj(rbartilde2_noise));

   Sn1 = (2)*abs(rbartilde1_noise.*conj(rbartilde1_noise));
   Sn2 = (2)*abs(rbartilde2_noise.*conj(rbartilde2_noise));

   Sn1 = (2)*abs(rbartilde1_noise.*conj(rbartilde1_noise))/(20480*1540);
   Sn2 = (2)*abs(rbartilde2_noise.*conj(rbartilde2_noise))/(20480*1540);

   Sn1 = (2)*abs(rbartilde1_noise).^2;
   Sn2 = (2)*abs(rbartilde2_noise).^2;

   %Sn1 = psd1_noise*fac^2;
   %Sn2 = psd2_noise*fac^2;

   %Sn1 = (2*1540)^2*psd1_noise*fac;
   %Sn2 = (2*1540)^2*psd2_noise*fac;

   %Sn1 = Sn;
   %Sn2 = Sn;

elseif strcmp(dataSet,'MDC2nonoise')
   Sn = Sn .* 10^-10;
   Sn1 = Sn;
   Sn2 = Sn;

else
   Sn = abs(rbartilde1.*conj(rbartilde1)) + abs(rbartilde2.*conj(rbartilde2));
   Sn = 0.73*sqrt(2)*4*Sn;

   Sn1 = Sn;
   Sn2 = Sn;

   Sn1 = (2)*4*abs(rbartilde1.*conj(rbartilde1));
   Sn2 = (2)*4*abs(rbartilde2.*conj(rbartilde2));


end



%Sn = Sn*8;
%Sn = Sn*10;
%Sn = zeros(size(Sn));
%Sn = 0.01*abs(rbartilde1).^2;
%orf = 0;
%orf = ones(size(rbartilde1));
omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/omega.txt';
omega = load(omegafile);
ff_omega = omega(:,1);
omega_data = omega(:,2);
omega_psd_data = omega(:,3);
omega_psd_data = omega_psd_data*averages*L*(windowconst/(2*fs*deltaFconstant.^2));

%plotName = [plotDir '/semifft'];
%figure;
%semilogx(ff,real(rbartilde1.*rbartilde2),'k--');
%hold on
%semilogx(ff,orf*1e-42,'r');
%hold off
%xlim([0 256]);
%%ylim([1e5 1e15]);
%xlabel('Frequency [Hz]');
%ylabel('S_{h}');
%print('-dpng',plotName);
%print('-depsc2',plotName);
%close;

plotName = [plotDir '/fft'];
figure;
loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
%loglog(ff,abs(rbartilde1),'k--');
hold on
loglog(ff,abs(rbartilde1.*rbartilde1),'b');
loglog(ff,abs(rbartilde2.*rbartilde2),'r');
%loglog(ff,sqrt(powerlaw(ff,-39,2/3 - 3)/2),'r')
loglog(ff,Sn,'g');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/psd'];
figure;
loglog(ff,abs(rbartilde1).^2,'k--');
hold on
loglog(ff,abs(rbartilde2).^2,'b');
%loglog(ff,sqrt(powerlaw(ff,-39,2/3 - 3)/2),'r')
loglog(ff,Sn1,'g');
loglog(ff,Sn2,'m');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('PSD');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/omega'];
figure;
%loglog(ff,abs(rbartilde1).*abs(rbartilde2),'k--');
loglog(ff,2*abs(rbartilde1.*rbartilde2)/(L*fs*windowconst*deltaT*deltaT),'k--');
hold on
loglog(ff,powerlaw_data,'b');
loglog(ff_omega,omega_psd_data*1e10,'r');
loglog(ff,Sn,'g');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/omega_mdc'];
figure;
%loglog(ff,abs(rbartilde1).*abs(rbartilde2),'k--');
loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
hold on
%loglog(ff,powerlaw_data,'b');
loglog(ff_omega,omega_psd_data*averages*L*(windowconst/(2*fs*deltaFconstant.^2)),'r');
loglog(ff,Sn,'g');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

if strcmp(dataSet,'MDC2') || strcmp(dataSet,'MDC3') || strcmp(dataSet,'MDC4') || strcmp(dataSet,'MDC5') || strcmp(dataSet,'none')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -55, -35, 'fixed'; ...
           'k', 'uniform', -6, 2, 'fixed'};
   end
elseif strcmp(dataSet,'MDC0all') || strcmp(dataSet,'MDC2all') || strcmp(dataSet,'MDC3all') || strcmp(dataSet,'MDC4all') || strcmp(dataSet,'MDC5all')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -50, -30, 'fixed'; ...
           'k', 'uniform', -8, 8, 'fixed'};
      %prior = {'a', 'uniform', -39, -38, 'fixed'; ...
      %     'k', 'uniform', -2.5, -2.3, 'fixed'};
   end

elseif strcmp(dataSet,'MDC2inj') || strcmp(dataSet,'MDC3inj') || strcmp(dataSet,'MDC4inj') || strcmp(dataSet,'MDC5inj') 
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -40, -38, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   end
elseif strcmp(dataSet,'MDC2inj_mid')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -42, -38, 'fixed'; ...
           'k', 'uniform', -4, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   end
elseif strcmp(dataSet,'MDC2inj_low')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -42, -38, 'fixed'; ...
           'k', 'uniform', -4, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   end
elseif strcmp(dataSet,'SB')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -33.5, -32.5, 'fixed'; ...
           'k', 'uniform', -2.5, -2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   end
elseif strcmp(dataSet,'MDCDiff02all')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -50, -35, 'fixed'; ...
           'k', 'uniform', -6, 2, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   end
elseif strcmp(dataSet,'MDC2nonoise')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -39, -37, 'fixed'; ...
           'k', 'uniform', -2.5, -2.3, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};
   end
elseif strcmp(dataSet,'MDC2nonoiseplus')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -39, -37, 'fixed'; ...
           'k', 'uniform', -2.5, -2.3, 'fixed'};
      %prior = {'a', 'uniform', -31, -29, 'fixed'; ...
      %     'k', 'uniform', -2.6, -2.2, 'fixed'};

      %prior = {'a', 'uniform', -44, -36, 'fixed'; ...
      %     'k', 'uniform', -4, 2, 'fixed'};

   end
end

data = {};
data{1} = Sn1;
data{2} = rbartilde1;
data{3} = ff;

%data = {};
%data{1} = Sn;
%data{2} = rbartilde1;
%data{3} = rbartilde2;
%data{4} = orf;
%data{5} = ff;

extraparams = {};

params.plotdir = plotDir;
params.savePlots = 1;

[size_prior_x,size_prior_y] = size(prior);

matFile = [params.plotdir '/multinest.mat'];
if doRun
   parnames = prior(:,1);
   parvalsmin = cell2mat(prior(:,3));
   parvalsmax = cell2mat(prior(:,4));
   num = 100;
   %num = 10;

   for i = 1:length(parnames)
      prior{i,6} = linspace(parvalsmin(i),parvalsmax(i),num+i);
   end

   lik = zeros(num,num);
   for i = 1:num+1
      fprintf('%d\n',i);
      for j = 1:num+2
         parvals = {prior{1,6}(i) prior{2,6}(j)};
         %logL = bayesian_logL(data, model, parnames, parvals);
         %logL = bayesian_logL_multi(data, model, parnames, parvals);
         logL = bayesian_logL_1d(data, model, parnames, parvals);
         lik(i,j) = logL;
      end
   end

   save(matFile);
else
   %load(matFile);
   load(matFile,'prior','lik');
end

parnames = {'a','k'};
parvals = {-100,0};
logL_background = bayesian_logL_1d(data, model, parnames, parvals);

if params.savePlots

   [C,I] = max(lik(:));
   [I1,I2] = ind2sub(size(lik),I);
   lik_max = lik(I1,I2);

   if strcmp(analysisType,'powerlaw');

      a = prior{1,6}(I1);
      k = prior{2,6}(I2);
      powerlaw_data = powerlaw(ff,a,k);

      [junk,index] = max(lik(:));
      fprintf('a: %.5f, k: %.5f, likelihood: %.5e\n',a,k,lik(index));

      plotName = [plotDir '/omega_best'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,2*abs(rbartilde1).^2/(L*fs*windowconst*deltaT*deltaT),'k--');
      hold on
      loglog(ff,2*powerlaw_data/(L*fs*windowconst*deltaT*deltaT),'b');
      loglog(ff_omega,omega_psd_data*1e60,'r');
      hold off
      xlim([0 256]);
      ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      plotName = [plotDir '/omega_best_mdc'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      loglog(ff,Sn,'g');
      loglog(ff_omega,omega_psd_data,'r');
      hold off
      xlim([0 256]);
      %ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

   end

   xlab = get_name(prior{1,1});
   ylab = get_name(prior{2,1});

   %lik = lik-max(lik(:));
   lik = real(lik);
   Ls = 10.^lik;

   %plotName = [plotDir '/contour'];
   %figure()
   %imagesc(prior{1,6},prior{2,6},lik');
   %set(gca,'YDir','normal')
   %set(gca,'XScale','lin');
   %xlabel(xlab);
   %ylabel(ylab);
   %cbar = colorbar;
   %set(get(cbar,'title'),'String','log10(Likelihood)');
   %vmin = max(lik(:))*0.9; vmax = max(lik(:));
   %caxis([vmin vmax]);
   %colorbar();
   %print('-dpng',plotName);
   %print('-depsc2',plotName);
   %close;

   vmax = max(lik(:));
   if vmax > 0
      vmin = 0.9*vmax;
   else
      vmin = 1.1*vmax; 
   end

   [Xq, Yq] = meshgrid(prior{1,6},prior{2,6});
   Vq = lik';

   plotName = [plotDir '/loglikelihood'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   norm = max(lik(:));
   lik = exp(lik - norm);
   Vq = lik';

   plotName = [plotDir '/likelihood'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   %caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   plotName = [plotDir '/likelihood_cbar'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   caxis([1-1e-11 1.0]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   zmax = max(lik(:));
   zvals = 0:zmax/2000:zmax;
   znorm = sum(lik(:));
   for ll=1:length(zvals)
      prob(ll) = sum(sum(lik(lik>=zvals(ll))));
   end
   prob = prob/znorm;

   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD') || strcmp(analysisType,'DNS');
      prob = fliplr(prob);
   end
   zvals68 = zvals(abs(prob-0.68)==min(abs(prob-0.68)));
   zvals95 = zvals(abs(prob-0.95)==min(abs(prob-0.95)));
   zvals99 = zvals(abs(prob-0.99)==min(abs(prob-0.99)));
   zcontours = [zvals68(1) zvals95(1) zvals99(1)];
   zvals10 = zvals(abs(prob-0.10)==min(abs(prob-0.10)));
   zcontours = [zvals10(1) zvals68(1) zvals95(1) zvals99(1)];
   %[C, h1] = contour(aa, epsilon, lik, zvals95);

   plotName = [plotDir '/likecontour'];
   figure;
   [C, h] = contour(Xq, Yq, Vq, zcontours(1),'k');
   hold on
   [C, h] = contour(Xq, Yq, Vq, zcontours(2),'b');
   [C, h] = contour(Xq, Yq, Vq, zcontours(3),'r');
   [C, h] = contour(Xq, Yq, Vq, zcontours(4),'m');
   hold off
   xlabel(xlab);
   ylabel(ylab);
   %cbar = colorbar;
   %set(get(cbar,'ylabel'),'String','Probability');

   bayesian_get_injplot(analysisType,dataSet);

   %legend('68 CL','95 CL','99 CL','True value','Location','northeast','Orientation','horizontal')
   legend('10 CL','68 CL','95 CL','99 CL','True value','Location','northeast','Orientation','horizontal')
   pretty;
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

end




