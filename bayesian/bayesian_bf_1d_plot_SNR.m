function mnPE(jobNumber,analysisType,dataSet,injSNRs);

rng(1,'twister');

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_bf_1d';
matDir = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber)];
plotDir = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber) '/all'];
createpath(plotDir);

if strcmp(dataSet,'simple');
   trueval1 = 0; trueval2 = 0;
elseif strcmp(dataSet,'simpleSB')
   trueval2 = -33.38; trueval1 = -3;
elseif strcmp(dataSet,'SBnoise') || strcmp(dataSet,'SBnoiseflat')
   trueval2 = -33.38; trueval1 = -3;
elseif strcmp(dataSet,'MDC2nonoise') || strcmp(dataSet,'MDC2nonoiseplus') || strcmp(dataSet,'MDC2noise') || strcmp(dataSet,'MDC2noiseflat') || strcmp(dataSet,'MDC2noise0')
   trueval2 = -38.6; trueval1 = 2/3 - 3;
end

As = []; ks = [];
AsMarg = []; ksMarg = [];
for i = 1:length(injSNRs)
   injSNR = injSNRs(i);
   matFile = [matDir '/' num2str(injSNR) '/multinest.mat'];
   load(matFile,'prior','lik1','lik2');

   norm = max(lik1);
   lik1 = exp(lik1-norm);
   lik1 = lik1 / sum(lik1);
   As = [As; lik1'];

   norm = max(lik2);
   lik2 = exp(lik2-norm);
   lik2 = lik2 / sum(lik2);
   ks = [ks; lik2'];

end

xlab = get_name(prior{1,1});
ylab = get_name(prior{2,1});

Colors = jet;
NoColors = length(Colors);
I = log10(injSNRs);
Ireduced = (I-min(I))/(max(I)-min(I))*(NoColors-1)+1;
RGB = interp1(1:NoColors,Colors,Ireduced);
set(gcf,'DefaultAxesColorOrder',RGB)

plotName = [plotDir '/' prior{1,1} '_marg'];
figure()
hold all
for i = 1:length(I)
   plot(prior{1,6},As(i,:),'color',RGB(i,:));
end
hold off
cbar = colorbar;
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
xlabel(xlab);
ylabel('Marginalized Likelihood');
%ylim([0 0.1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{1,1} '_marg_exp'];
figure()
hold all
for i = 1:length(I)
   plot(10.^prior{1,6},As(i,:),'color',RGB(i,:));
end
hold off
cbar = colorbar;
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
xlabel(xlab);
ylabel('Marginalized Likelihood');
%ylim([0 0.1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{2,1} '_marg'];
figure()
hold all
for i = 1:length(I)
   plot(prior{2,6},ks(i,:),'color',RGB(i,:));
end
hold off
cbar = colorbar;
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
xlabel(ylab);
ylabel('Marginalized Likelihood');
%ylim([0 0.1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/' prior{2,1} '_marg_exp'];
figure()
hold all
for i = 1:length(I)
   plot(10.^prior{2,6},ks(i,:),'color',RGB(i,:));
end
hold off
cbar = colorbar;
ylabel(cbar,'log10(SNR)');
caxis([min(log10(injSNRs)) max(log10(injSNRs))])
xlabel(ylab);
ylabel('Marginalized Likelihood');
%ylim([0 0.1]);
print('-dpng',plotName);
print('-depsc2',plotName);
close;
