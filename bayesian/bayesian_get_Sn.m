function [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,Sn1,Sn2,psd1,psd2,orf] = ...
   bayesian_get_Sn(jobNumber,dataSet,plotDir,injSNR,ff,rbartilde1,rbartilde2,...
      r1r1,r1r2,r2r1,r2r2,psd1,psd2,npsd1,npsd2,orf);

if strcmp(dataSet,'MDC2noise0')
   [ff_noise,rbartilde1_noise,rbartilde2_noise,r1r1_noise,r1r2_noise,r2r1_noise,r2r2_noise,orf_noise,psd1_noise,psd2_noise] = ...
      bayesian_get_data(jobNumber,'MDC0all',strrep(plotDir,dataSet,'MDC0all'));

   fac = 1e-5;
   %fac = 1e-100;
   fac = 1e-2;
   %fac = 5e-2;
   %fac = 1e-1 * 1.121/5;
   %fac = 1.5e-1;
   fac = 1;
   fac = 1 * sqrt(9.476/injSNR);

   rbartilde1_noise = rbartilde1_noise*fac;
   rbartilde2_noise = rbartilde2_noise*fac;

   rbartilde1 = rbartilde1 + rbartilde1_noise;
   rbartilde2 = rbartilde2 + rbartilde2_noise;

   psd1_noise = psd1_noise*fac^2;
   psd2_noise = psd2_noise*fac^2;
   r1r1 = r1r1 + r1r1_noise*fac^2;
   r1r2 = r1r2 + r1r2_noise*fac^2;
   r2r1 = r2r1 + r2r1_noise*fac^2;
   r2r2 = r2r2 + r2r2_noise*fac^2;
   deltaF = ff(2)-ff(1);

   H0 = 100e3/3.09e22;
   T = 3.1e7;
   df = ff(2)-ff(1);

   Omega_GW = ((10*pi^2)/(3*H0^2)).*ff.^3 .*psd1;
   SNR = ((3*H0^2)/(10*pi^2))*sqrt(T)* ...
      sqrt(2*sum(df * (orf.^2 .* Omega_GW.^2) ./ (ff.^6 .* psd1_noise .* psd2_noise)));
   %SNR = SNR/fac^2;
   fprintf('SNR optimal: %.3e\n',SNR);

   Sn1 = psd1_noise;
   Sn2 = psd2_noise;

elseif strcmp(dataSet,'MDC2noise')
   [ff_noise,rbartilde1_noise,rbartilde2_noise,r1r1_noise,r1r2_noise,r2r1_noise,r2r2_noise,orf_noise,psd1_noise,psd2_noise] = ...
      bayesian_get_data(jobNumber,'none',strrep(plotDir,dataSet,'none'));

   fac = 1;
   fac = 1*sqrt(9.489/injSNR);

   rbartilde1_noise = rbartilde1_noise*fac;
   rbartilde2_noise = rbartilde2_noise*fac;

   rbartilde1 = rbartilde1 + rbartilde1_noise;
   rbartilde2 = rbartilde2 + rbartilde2_noise;

   psd1_noise = psd1_noise*fac^2;
   psd2_noise = psd2_noise*fac^2;
   r1r1 = r1r1 + r1r1_noise*fac^2;
   r1r2 = r1r2 + r1r2_noise*fac^2;
   r2r1 = r2r1 + r2r1_noise*fac^2;
   r2r2 = r2r2 + r2r2_noise*fac^2;

   deltaF = ff(2)-ff(1);
   H0 = 100e3/3.09e22;
   T = 3.1e7;
   df = ff(2)-ff(1);

   Omega_GW = ((10*pi^2)/(3*H0^2)).*ff.^3 .*psd1;
   SNR = ((3*H0^2)/(10*pi^2))*sqrt(T)* ...
      sqrt(2*sum(df * (orf.^2 .* Omega_GW.^2) ./ (ff.^6 .* psd1_noise .* psd2_noise)));
   fprintf('SNR optimal: %.3e\n',SNR);

   Sn1 = psd1_noise;
   Sn2 = psd2_noise;

   indexes = find(ff >= 15 & ff <= 200);
   ff = ff(indexes);
   rbartilde1 = rbartilde1(indexes);
   rbartilde2 = rbartilde2(indexes);
   r1r1 = r1r1(indexes);
   r1r2 = r1r2(indexes);
   r2r1 = r2r1(indexes);
   r2r2 = r2r2(indexes);
   Sn1 = Sn1(indexes);
   Sn2 = Sn2(indexes);
   psd1 = psd1(indexes);
   psd2 = psd2(indexes);
   orf = orf(indexes);

   %h0 = 0.7;
   %omega_to_psd = 3.95364e-37*(h0/0.704)*(h0/0.704)*(1./ff.^3);
   %m = 1.2188; lambda = log10(4.7933e-4);
   %m = 2.5;
   %Omega = DNS(ff,m,lambda) .* omega_to_psd;

   %omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/omega.txt';
   %omega = load(omegafile);
   %ff_omega = omega(:,1);
   %omega_data = omega(:,2);
   %omega_psd_data = omega(:,3);

   %figure;
   %loglog(ff_omega,omega_psd_data,'k');
   %hold on
   %loglog(ff,Omega,'k--');
   %loglog(ff,psd1,'b');
   %hold off
   %xlim([10 256]);
   %print('-dpng','test.png');
   %close;

elseif strcmp(dataSet,'MDC2noiseflat')
   [ff_noise,rbartilde1_noise,rbartilde2_noise,r1r1_noise,r1r2_noise,r2r1_noise,r2r2_noise,orf_noise,psd1_noise,psd2_noise] = ...
      bayesian_get_data(jobNumber,'noneflat',strrep(plotDir,dataSet,'noneflat'));

   fac = sqrt(1.798e-01/injSNR);
   %fac = 1;

   rbartilde1_noise = rbartilde1_noise*fac;
   rbartilde2_noise = rbartilde2_noise*fac;

   rbartilde1 = rbartilde1 + rbartilde1_noise;
   rbartilde2 = rbartilde2 + rbartilde2_noise;

   psd1_noise = psd1_noise*fac^2;
   psd2_noise = psd2_noise*fac^2;
   r1r1 = r1r1 + r1r1_noise*fac^2;
   r1r2 = r1r2 + r1r2_noise*fac^2;
   r2r1 = r2r1 + r2r1_noise*fac^2;
   r2r2 = r2r2 + r2r2_noise*fac^2;
   deltaF = ff(2)-ff(1);

   H0 = 100e3/3.09e22;
   T = 3.1e7;
   df = ff(2)-ff(1);

   Omega_GW = ((10*pi^2)/(3*H0^2)).*ff.^3 .*psd1;
   SNR = ((3*H0^2)/(10*pi^2))*sqrt(T)* ...
      sqrt(2*sum(df * (orf.^2 .* Omega_GW.^2) ./ (ff.^6 .* psd1_noise .* psd2_noise)));
   %SNR = SNR / fac^2;
   fprintf('SNR optimal: %.3e\n',SNR);

   Sn1 = psd1_noise;
   Sn2 = psd2_noise;


elseif strcmp(dataSet,'SBnoiseflat')
   [ff_noise,rbartilde1_noise,rbartilde2_noise,r1r1_noise,r1r2_noise,r2r1_noise,r2r2_noise,orf_noise,psd1_noise,psd2_noise] = ...
      bayesian_get_data(jobNumber,'noneflat',strrep(plotDir,dataSet,'noneflat'));

   fac = 1e-5;
   %fac = 1e-100;
   fac = 1e-2;
   %fac = 5e-2;
   %fac = 1e-5;
   %fac = 1.5e-1;
   %fac = 2e-1;
   %fac = 1000*4.284/5;
   fac = 1*sqrt(979.9109868603/injSNR);
   %fac = 1;

   rbartilde1_noise = rbartilde1_noise*fac;
   rbartilde2_noise = rbartilde2_noise*fac;

   rbartilde1 = rbartilde1 + rbartilde1_noise;
   rbartilde2 = rbartilde2 + rbartilde2_noise;
   Sn = abs(rbartilde1_noise.*conj(rbartilde1_noise)) + abs(rbartilde2_noise.*conj(rbartilde2_noise));
   Sn = 1.23*0.73*sqrt(2)*4*Sn;
   %Sn = 4*Sn;

   psd1_noise = (2)*abs(rbartilde1_noise).^2;
   psd2_noise = (2)*abs(rbartilde2_noise).^2;
   deltaF = ff(2)-ff(1);

   H0 = 100e3/3.09e22;
   T = 20480;
   df = ff(2)-ff(1);

   Omega_GW = ((10*pi^2)/(3*H0^2)).*ff.^3 .*psd1;
   SNR = ((3*H0^2)/(10*pi^2))*sqrt(T)* ...
      sqrt(2*sum(df * (orf.^2 .* Omega_GW.^2) ./ (ff.^6 .* psd1_noise .* psd2_noise)));
   %SNR = SNR / fac^2;
   fprintf('SNR optimal: %.3e\n',SNR);

   Sn1 = psd1_noise;
   Sn2 = psd2_noise;

elseif strcmp(dataSet,'SBnoise')
   [ff_noise,rbartilde1_noise,rbartilde2_noise,r1r1_noise,r1r2_noise,r2r1_noise,r2r2_noise,orf_noise,psd1_noise,psd2_noise] = ...
      bayesian_get_data(jobNumber,'none',strrep(plotDir,dataSet,'none'));

   fac = 1e-5;
   %fac = 1e-100;
   fac = 1e-2;
   %fac = 5e-2;
   %fac = 1e-5;
   %fac = 1.5e-1;
   %fac = 2e-1;
   fac = 10000;
   fac = 10000*8.794/5;
   fac = 1;
   fac = 1 * sqrt(82975.8554412948/injSNR);

   rbartilde1_noise = rbartilde1_noise*fac;
   rbartilde2_noise = rbartilde2_noise*fac;

   rbartilde1 = rbartilde1 + rbartilde1_noise;
   rbartilde2 = rbartilde2 + rbartilde2_noise;
   Sn = abs(rbartilde1_noise.*conj(rbartilde1_noise)) + abs(rbartilde2_noise.*conj(rbartilde2_noise));
   Sn = 1.23*0.73*sqrt(2)*4*Sn;
   %Sn = 4*Sn;

   psd1_noise = (2)*abs(rbartilde1_noise).^2;
   psd2_noise = (2)*abs(rbartilde2_noise).^2;
   deltaF = ff(2)-ff(1);

   H0 = 100e3/3.09e22;
   T = 20480;
   df = ff(2)-ff(1);

   Omega_GW = ((10*pi^2)/(3*H0^2)).*ff.^3 .*psd1;
   SNR = ((3*H0^2)/(10*pi^2))*sqrt(T)* ...
      sqrt(2*sum(df * (orf.^2 .* Omega_GW.^2) ./ (ff.^6 .* psd1_noise .* psd2_noise)));
   %SNR = SNR / fac^2;
   fprintf('SNR optimal: %.3e\n',SNR);

   Sn1 = psd1_noise;
   Sn2 = psd2_noise;

elseif strcmp(dataSet,'SBpoint')
   [ff_noise,rbartilde1_noise,rbartilde2_noise,r1r1_noise,r1r2_noise,r2r1_noise,r2r2_noise,orf_noise,psd1_noise,psd2_noise] = ...
      bayesian_get_data(jobNumber,'SBpointnone',strrep(plotDir,dataSet,'SBpointnone'));

   fac = sqrt(3.008e+04/injSNR);
   %fac = 1;

   rbartilde1_noise = rbartilde1_noise*fac;
   rbartilde2_noise = rbartilde2_noise*fac;

   rbartilde1 = rbartilde1 + rbartilde1_noise;
   rbartilde2 = rbartilde2 + rbartilde2_noise;

   psd1_noise = psd1_noise*fac^2;
   psd2_noise = psd2_noise*fac^2;
   r1r1 = r1r1 + r1r1_noise*fac^2;
   r1r2 = r1r2 + r1r2_noise*fac^2;
   r2r1 = r2r1 + r2r1_noise*fac^2;
   r2r2 = r2r2 + r2r2_noise*fac^2;
   deltaF = ff(2)-ff(1);

   H0 = 100e3/3.09e22;
   T = 3.1e7;
   df = ff(2)-ff(1);

   Omega_GW = ((10*pi^2)/(3*H0^2)).*ff.^3 .*psd1;
   SNR = ((3*H0^2)/(10*pi^2))*sqrt(T)* ...
      sqrt(2*sum(df * (orf.^2 .* Omega_GW.^2) ./ (ff.^6 .* psd1_noise .* psd2_noise)));
   %SNR = SNR / fac^2;
   fprintf('SNR optimal: %.3e\n',SNR);

   Sn1 = psd1_noise;
   Sn2 = psd2_noise;

elseif strcmp(dataSet,'MDC2nonoise')
   Sn = Sn .* 10^-10;
   Sn1 = Sn;
   Sn2 = Sn;

elseif strcmp(dataSet,'SBnonoise')
   Sn = Sn .* 10^-10;
   Sn1 = Sn;
   Sn2 = Sn;
elseif strcmp(dataSet,'simple') || strcmp(dataSet,'simpleSB') || strcmp(dataSet,'simplePoint') || strcmp(dataSet,'SBpoint') || strcmp(dataSet,'simpleCBC') || strcmp(dataSet,'simpleCBCNagamine')
   Sn1 = npsd1;
   Sn2 = npsd2;
   Sn = Sn1;
else
   Sn = abs(rbartilde1.*conj(rbartilde1)) + abs(rbartilde2.*conj(rbartilde2));
   Sn = 0.73*sqrt(2)*4*Sn;

   Sn1 = psd1;
   Sn2 = psd2;

   %psd1_noise = (2)*abs(rbartilde1).^2;
   %psd2_noise = (2)*abs(rbartilde2).^2;

   %Sn1 = psd1_noise;
   %Sn2 = psd2_noise;

end


