function [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = ...
   bayesian_get_data(jobNumber,dataSet,plotDir);

if strcmp(dataSet,'MDC2nonoise') || strcmp(dataSet,'MDC2noise') || strcmp(dataSet,'MDC2noiseflat') || strcmp(dataSet,'MDC2noise0')
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_2_nonoise/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir); 
elseif strcmp(dataSet,'MDC0all');
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_0/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);
elseif strcmp(dataSet,'MDC2all');
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_2/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);
elseif strcmp(dataSet,'MDC3all');
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_3/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);
elseif strcmp(dataSet,'MDC4all');
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_4/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);
elseif strcmp(dataSet,'MDC5all');
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_5/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);
elseif strcmp(dataSet,'SBnonoise') || strcmp(dataSet,'SBnoise') || strcmp(dataSet,'SBnoiseflat')
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_inj_SB/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);
elseif strcmp(dataSet,'none') 
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_none/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);
elseif strcmp(dataSet,'noneflat')
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_noneflat/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data(plotDir,fileDir);
elseif strcmp(dataSet,'SBpoint')
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_SBpoint/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data_point(plotDir,fileDir);
elseif strcmp(dataSet,'SBpointnone')
   fileDir = '/home/mcoughlin/Stochastic/Bayesian/output/output_SBpointnone/';
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_load_data_point(plotDir,fileDir);
end


