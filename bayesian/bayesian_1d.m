
baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_1d';
plotDir = [baseplotDir];
createpath(plotDir);

si = 1+1j;
%si = 2;
orf = 1;

Sh_true = 2*si*conj(si);
Sn = 0.1;

Shs = linspace(0,10,1000);
logLs = [];

for i = 1:length(Shs)
   Sh = Shs(i);
   logLs(i) = (1/((1-orf^2)*Sh^2))*exp(-4*si*conj(si)/(Sh + orf*Sh));
   logLs(i) = sum(log(1./((1-orf^2).*Sh.^2 + 2*Sh.*Sn + Sn.^2)) + (-4*si.*conj(si)./(Sh + orf*Sh + Sn)));
end
logLs = real(logLs);
Ls = 10.^logLs;

[maxLogL,index] = max(logLs);
fprintf('Max Sh: %.5f\n',Shs(index));

plotName = [plotDir '/test'];
figure;
plot(Shs,Ls,'k*')
hold on
plot([Sh_true Sh_true],[0 0.2],'r--')
hold off
print('-dpng',plotName);
print('-depsc2',plotName);
close;

