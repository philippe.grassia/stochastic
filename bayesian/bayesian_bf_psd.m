function mnPE(jobNumber,analysisType,dataSet,doRun);

warning('off','all');

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

psdfile = '/home/mcoughlin/MDC_Generation/MDC_Generation/trunk/Detectors/aLIGO/ZERO_DET_high_P.txt';

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_bf';
plotDir = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber)];
createpath(plotDir);

%rbartilde1 = 150*rbartilde1;
%rbartilde2 = 150*rbartilde2;

[ff,rbartilde1,rbartilde2,orf] = bayesian_get_data(jobNumber,dataSet,plotDir);

if strcmp(dataSet,'MDC0all') || strcmp(dataSet,'MDC2all') || strcmp(dataSet,'MDC3all') || strcmp(dataSet,'MDC4all') || strcmp(dataSet,'MDC5all')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 10240;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2inj') || strcmp(dataSet,'MDC3inj') || strcmp(dataSet,'MDC4inj') || strcmp(dataSet,'MDC5inj') || strcmp(dataSet,'MDC2inj_mid') || strcmp(dataSet,'MDC2inj_low') || strcmp(dataSet,'none')
   windowconst = 0.375;
   L = 2048*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1;
   deltaFconstant = 1;
   %deltaFconstant = 1024;
   orf = 1;
else
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1540;
   deltaFconstant = 10240;
end

a = -35;
k = 2/3 - 3;
%ff = [10 100 1000];
powerlaw_data = powerlaw(ff,a,k);
%rbartilde1 = powerlaw(ff,-40,2/3 - 3);
%rbartilde1 = sqrt(rbartilde1/2);
%rbartilde2 = rbartilde1;

%ff = [1 10 100];
si = powerlaw(ff,-40,2/3 - 3);
si = sqrt(si/2);
%rbartilde1 = si; rbartilde2 = si;
%rbartilde1 = abs(rbartilde1); rbartilde2 = abs(rbartilde2);
%Sn = 0*ones(size(si));
%Sn = 0.01*rbartilde1.^2;
%si = 2;
%orf = 1;
%rbartilde1 = si;
%rbartilde2 = si;
powerlaw_data = powerlaw(ff,a,k);

factor = averages*L*(windowconst/(2*fs*deltaFconstant.^2));
fprintf('%.10f\n',factor);
%keyboard
psd = load(psdfile);
Sn = interp1(psd(:,1),psd(:,2),ff).^2;
Sn = Sn*factor;
%Sn = Sn*8;
%Sn = Sn*10;
%Sn = zeros(size(Sn));
%Sn = 0.01*abs(rbartilde1).^2;
%orf = 0;
%orf = ones(size(rbartilde1));
omegafile = '/home/stochastic/MDC/aligo_v1/Gaussian_Noise/DataSet_2_BNS_nonDet/omega.txt';
omega = load(omegafile);
ff_omega = omega(:,1);
omega_data = omega(:,2);
omega_psd_data = omega(:,3);
omega_psd_data = omega_psd_data*averages*L*(windowconst/(2*fs*deltaFconstant.^2));

plotName = [plotDir '/fft'];
figure;
loglog(ff,real(rbartilde1.*rbartilde2),'k--');
%loglog(ff,abs(rbartilde1),'k--');
hold on
loglog(ff,real(rbartilde1.*rbartilde1),'b');
loglog(ff,real(rbartilde2.*rbartilde2),'r');
%loglog(ff,sqrt(powerlaw(ff,-39,2/3 - 3)/2),'r')
loglog(ff,Sn,'g');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/omega'];
figure;
%loglog(ff,abs(rbartilde1).*abs(rbartilde2),'k--');
loglog(ff,2*abs(rbartilde1.*rbartilde2)/(L*fs*windowconst*deltaT*deltaT),'k--');
hold on
loglog(ff,powerlaw_data,'b');
loglog(ff_omega,omega_psd_data*1e10,'r');
loglog(ff,Sn,'g');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/omega_mdc'];
figure;
%loglog(ff,abs(rbartilde1).*abs(rbartilde2),'k--');
loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
hold on
%loglog(ff,powerlaw_data,'b');
%loglog(ff_omega,omega_psd_data*averages*L*(windowconst/(2*fs*deltaFconstant.^2)),'r');
loglog(ff,Sn,'g');
hold off
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

plotName = [plotDir '/omega_mdc_residual'];
figure;
%loglog(ff,abs(rbartilde1).*abs(rbartilde2),'k--');
loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
xlim([0 256]);
%ylim([1e5 1e15]);
xlabel('Frequency [Hz]');
ylabel('S_{h}');
print('-dpng',plotName);
print('-depsc2',plotName);
close;

likelihood = @bayesian_logL;

if strcmp(dataSet,'MDC2') || strcmp(dataSet,'MDC3') || strcmp(dataSet,'MDC4') || strcmp(dataSet,'MDC5') || strcmp(dataSet,'none') || strcmp(dataSet,'MDC2inj_mid') || strcmp(dataSet,'MDC2inj_low')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -55, -35, 'fixed'; ...
           'k', 'uniform', -6, 2, 'fixed'};
   elseif strcmp(analysisType,'PSD');
      model = @powerlaw_model;
      prior = {'A', 'uniform', -1, 1, 'fixed'};
   end
elseif strcmp(dataSet,'MDC0all') || strcmp(dataSet,'MDC2all') || strcmp(dataSet,'MDC3all') || strcmp(dataSet,'MDC4all') || strcmp(dataSet,'MDC5all')
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -50, -35, 'fixed'; ...
           'k', 'uniform', -4, 2, 'fixed'};
   elseif strcmp(analysisType,'PSD');
      model = @powerlaw_model;
      prior = {'A', 'uniform', -2, 2, 'fixed'};
   end

elseif strcmp(dataSet,'MDC2inj') || strcmp(dataSet,'MDC3inj') || strcmp(dataSet,'MDC4inj') || strcmp(dataSet,'MDC5inj') 
   if strcmp(analysisType,'powerlaw');
      model = @powerlaw_model;
      prior = {'a', 'uniform', -45, -20, 'fixed'; ...
           'k', 'uniform', -4, 0, 'fixed'};
      prior = {'a', 'uniform', -42, -41, 'fixed'; ...
           'k', 'uniform', -2.6, -2.2, 'fixed'};
   elseif strcmp(analysisType,'PSD');
      model = @powerlaw_model;
      prior = {'A', 'uniform', -2, 2, 'fixed'};
   end
end

data = {};
data{1} = Sn;
data{2} = rbartilde1;
data{3} = rbartilde2;
data{4} = orf;
data{5} = ff;

extraparams = {};

params.plotdir = plotDir;
params.savePlots = 1;

[size_prior_x,size_prior_y] = size(prior);

matFile = [params.plotdir '/multinest.mat'];
if doRun
   parnames = prior(:,1);
   parvalsmin = cell2mat(prior(:,3));
   parvalsmax = cell2mat(prior(:,4));
   num = 1000;
   %num = 10;

   for i = 1:length(parnames)
      prior{i,6} = linspace(parvalsmin(i),parvalsmax(i),num+i);
   end
   parnames = {'a','k','A'};

   lik = zeros(num,1);
   for i = 1:num+1
      %fprintf('%d\n',i);
      parvals = {-10000 0 prior{1,6}(i)};
      %fprintf('%.5f\n',prior{1,6}(i));
      logL = bayesian_logL_psd(data, model, parnames, parvals);
      %logL = 1/std(abs(rbartilde1.*rbartilde2)-Sn.*10.^prior{1,6}(i));
      lik(i) = logL;
   end

   save(matFile);
else
   %load(matFile);
   load(matFile,'prior','lik');
end

parnames = {'a','k'};
parvals = {-100,0};
logL_background = bayesian_logL(data, model, parnames, parvals);

if params.savePlots

   [C,I] = max(lik(:));
   I1 = ind2sub(size(lik),I);
   lik_max = lik(I1);

   if strcmp(analysisType,'PSD');

      A = prior{1,6}(I1);

      [junk,index] = max(lik(:));
      fprintf('A: %.5f, likelihood: %.5e\n',A,lik(index));

      plotName = [plotDir '/omega_best'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,2*abs(rbartilde1).^2/(L*fs*windowconst*deltaT*deltaT),'k--');
      hold on
      %loglog(ff,2*powerlaw_data/(L*fs*windowconst*deltaT*deltaT),'b');
      loglog(ff_omega,omega_psd_data*1e60,'r');
      hold off
      xlim([0 256]);
      ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      plotName = [plotDir '/omega_best_mdc'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
      hold on
      %loglog(ff,powerlaw_data,'b');
      loglog(ff,Sn.*10.^A,'g');
      loglog(ff_omega,omega_psd_data,'r');
      hold off
      xlim([0 256]);
      %ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      plotName = [plotDir '/omega_best_mdc_residual'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,abs(abs(rbartilde1.*rbartilde2)-Sn.*10.^A),'k--');
      xlim([0 256]);
      %ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

   end

   xlab = get_name(prior{1,1});

   plotName = [plotDir '/loglikelihood'];
   figure()
   plot(prior{1,6},lik);
   xlabel(xlab);
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   norm = max(lik(:));
   lik = exp(lik - norm);
   Vq = lik';

   plotName = [plotDir '/likelihood'];
   figure()
   plot(prior{1,6},lik);
   xlabel(xlab);
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

end




