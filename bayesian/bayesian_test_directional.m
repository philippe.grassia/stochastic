
baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_test';
plotDir = baseplotDir;
createpath(plotDir);

gpss = 800000000:60:800086400;
gpsStart = 800000000;
dur = 60;
gpsEnd = gpsStart + 2*86400;
gpss = gpsStart:dur:gpsEnd;
F1p = [];
F1x = [];
F2p = [];
F2x = [];
tau = [];

det1 = getdetector('LHO');
det2 = getdetector('LLO');
ra = 6.0;
decl = 30.0;

%ra = 9.0;
%decl = -60;

for i = 1:length(gpss)
   gps = gpss(i);
   siderealtime=GPStoGreenwichMeanSiderealTime(gps);
   g=orfIntegrandSymbolic(det1,det2,siderealtime,ra,decl);

   F1p = [F1p g.F1p];
   F1x = [F1x g.F1x];
   F2p = [F2p g.F2p];
   F2x = [F2x g.F2x];
   tau = [tau g.tau];
end

figure;
plot(gpss-gpss(1),tau,'b')
%xlabel('Frequency [Hz]');
%ylabel('');
print('-dpng',[plotDir '/tau.png'])
close;

figure;
plot(gpss-gpss(1),F1p,'b')
hold on
plot(gpss-gpss(1),F1x,'g')
plot(gpss-gpss(1),F2p,'r')
plot(gpss-gpss(1),F2x,'m')
hold off
%xlabel('Frequency [Hz]');
%ylabel('');
print('-dpng',[plotDir '/F.png'])
close;


