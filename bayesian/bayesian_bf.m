function mnPE(jobNumber,analysisType,dataSet,doRun);

set(0,'DefaultAxesFontSize',20);
set(0,'DefaultTextFontSize',20);

baseplotDir = '/home/mcoughlin/Stochastic/Bayesian/plots_bf';
plotDir = [baseplotDir '/' dataSet '/' analysisType '/' num2str(jobNumber)];
createpath(plotDir);

%rbartilde1 = 150*rbartilde1;
%rbartilde2 = 150*rbartilde2;

injSNR = 1;
if strcmp(dataSet,'simple')
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = bayesian_gen_data(injSNR);
elseif strcmp(dataSet,'simpleSB')
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2,npsd1,npsd2,hpsd1] = bayesian_gen_data_SB(injSNR);
else
   [ff,rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,orf,psd1,psd2] = bayesian_get_data(jobNumber,dataSet,plotDir);
   npsd1 = 1; npsd2 = 1; hpsd1 = 1;
end

if strcmp(dataSet,'MDC0all') || strcmp(dataSet,'MDC2all') || strcmp(dataSet,'MDC3all') || strcmp(dataSet,'MDC4all') || strcmp(dataSet,'MDC5all')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDCDiff02all')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2nonoise')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2nonoiseplus')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2noise') || strcmp(dataSet,'MDC2noiseflat') || strcmp(dataSet,'MDC2noise0')
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 2*1540;
   deltaFconstant = 1;
elseif strcmp(dataSet,'MDC2inj') || strcmp(dataSet,'MDC3inj') || strcmp(dataSet,'MDC4inj') || strcmp(dataSet,'MDC5inj') || strcmp(dataSet,'MDC2inj_mid') || strcmp(dataSet,'MDC2inj_low') || strcmp(dataSet,'none')
   windowconst = 0.375;
   L = 2048*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1;
   deltaFconstant = 1;
   %deltaFconstant = 1024;
   orf = 1;
elseif strcmp(dataSet,'SB') || strcmp(dataSet,'SBnoise') || strcmp(dataSet,'SBnoiseflat')
   windowconst = 0.375;
   L = 2048*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1;
   deltaFconstant = 1;
   %deltaFconstant = 1024;
else
   windowconst = 0.375;
   L = 20480*512;
   fs = 512;
   deltaT = 1/fs;
   averages = 1540;
   deltaFconstant = 10240;
end

if strcmp(analysisType,'powerlawPSDpowerlaw')
   likelihood = @bayesian_logL_psd_multi;
elseif strcmp(analysisType,'powerlawPSD')
   likelihood = @bayesian_logL_psdscale_multi;
else
   likelihood = @bayesian_logL_multi;
end

likelihood = @bayesian_logL_multi;
[model,prior] = bayesian_get_model(dataSet,analysisType);
[rbartilde1,rbartilde2,r1r1,r1r2,r2r1,r2r2,Sn1,Sn2] = ...
   bayesian_get_Sn(jobNumber,dataSet,plotDir,injSNR,ff,rbartilde1,rbartilde2,...
      r1r1,r1r2,r2r1,r2r2,psd1,psd2,npsd1,npsd2,orf);
bayesian_get_diagnostic_plots(jobNumber,dataSet,plotDir,injSNR,ff,rbartilde1,rbartilde2,...
      r1r1,r1r2,r2r1,r2r2,psd1,psd2,npsd1,npsd2,orf,Sn1,Sn2);

data = {};
data{1} = Sn1;
data{2} = Sn2;
data{3} = r1r1;
data{4} = r1r2;
data{5} = r2r1;
data{6} = r2r2;
data{7} = orf;
data{8} = ff;

extraparams = {};

params.plotdir = plotDir;
params.savePlots = 1;

[size_prior_x,size_prior_y] = size(prior);

matFile = [params.plotdir '/multinest.mat'];
if doRun
   parnames = prior(:,1);
   parvalsmin = cell2mat(prior(:,3));
   parvalsmax = cell2mat(prior(:,4));
   num = 100;
   %num = 10;

   for i = 1:length(parnames)
      prior{i,6} = linspace(parvalsmin(i),parvalsmax(i),num+i);
   end

   lik = zeros(num,num);
   for i = 1:num+1
      fprintf('%d\n',i);
      for j = 1:num+2
         parvals = {prior{1,6}(i) prior{2,6}(j)};
         %logL = bayesian_logL(data, model, parnames, parvals);
         logL = bayesian_logL_multi(data, model, parnames, parvals);
         lik(i,j) = logL;
      end
   end

   save(matFile);
else
   %load(matFile);
   load(matFile,'prior','lik');
end

parnames = {'a','k'};
parvals = {-100,0};
logL_background = bayesian_logL(data, model, parnames, parvals);

if params.savePlots

   [C,I] = max(lik(:));
   [I1,I2] = ind2sub(size(lik),I);
   lik_max = lik(I1,I2);

   if strcmp(analysisType,'powerlaw');

      a = prior{1,6}(I1);
      k = prior{2,6}(I2);
      powerlaw_data = powerlaw(ff,a,k);

      [junk,index] = max(lik(:));
      fprintf('a: %.5f, k: %.5f, likelihood: %.5e\n',a,k,lik(index));

      plotName = [plotDir '/omega_best'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,2*abs(rbartilde1).^2/(L*fs*windowconst*deltaT*deltaT),'k--');
      hold on
      loglog(ff,2*powerlaw_data/(L*fs*windowconst*deltaT*deltaT),'b');
      loglog(ff_omega,omega_psd_data*1e60,'r');
      hold off
      xlim([0 256]);
      ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

      plotName = [plotDir '/omega_best_mdc'];
      figure;
      %loglog(ff,real(rbartilde1.*rbartilde2),'k--');
      loglog(ff,abs(rbartilde1.*rbartilde2),'k--');
      hold on
      loglog(ff,powerlaw_data,'b');
      loglog(ff,Sn,'g');
      loglog(ff_omega,omega_psd_data,'r');
      hold off
      xlim([0 256]);
      %ylim([1e5 1e15]);
      xlabel('Frequency [Hz]');
      ylabel('S_{h}');
      print('-dpng',plotName);
      print('-depsc2',plotName);
      close;

   end

   xlab = get_name(prior{1,1});
   ylab = get_name(prior{2,1});

   %lik = lik-max(lik(:));
   lik = real(lik);
   Ls = 10.^lik;

   %plotName = [plotDir '/contour'];
   %figure()
   %imagesc(prior{1,6},prior{2,6},lik');
   %set(gca,'YDir','normal')
   %set(gca,'XScale','lin');
   %xlabel(xlab);
   %ylabel(ylab);
   %cbar = colorbar;
   %set(get(cbar,'title'),'String','log10(Likelihood)');
   %vmin = max(lik(:))*0.9; vmax = max(lik(:));
   %caxis([vmin vmax]);
   %colorbar();
   %print('-dpng',plotName);
   %print('-depsc2',plotName);
   %close;

   vmax = max(lik(:));
   if vmax > 0
      vmin = 0.9*vmax;
   else
      vmin = 1.1*vmax; 
   end

   [Xq, Yq] = meshgrid(prior{1,6},prior{2,6});
   Vq = lik';

   plotName = [plotDir '/loglikelihood'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   norm = max(lik(:));
   lik = exp(lik - norm);
   Vq = lik';

   plotName = [plotDir '/likelihood'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   %caxis([vmin vmax]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   plotName = [plotDir '/likelihood_cbar'];
   figure()
   pcolor(Xq,Yq,Vq);
   xlabel(xlab);
   ylabel(ylab);
   cbar = colorbar;
   caxis([1-1e-11 1.0]);
   set(get(cbar,'title'),'String','log10(Likelihood)');
   colorbar();
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

   zmax = max(lik(:));
   zvals = 0:zmax/2000:zmax;
   znorm = sum(lik(:));
   for ll=1:length(zvals)
      prob(ll) = sum(sum(lik(lik>=zvals(ll))));
   end
   prob = prob/znorm;

   if strcmp(analysisType,'powerlaw') || strcmp(analysisType,'powerlawPSD') || strcmp(analysisType,'DNS');
      prob = fliplr(prob);
   end
   zvals68 = zvals(abs(prob-0.68)==min(abs(prob-0.68)));
   zvals95 = zvals(abs(prob-0.95)==min(abs(prob-0.95)));
   zvals99 = zvals(abs(prob-0.99)==min(abs(prob-0.99)));
   zcontours = [zvals68(1) zvals95(1) zvals99(1)];
   zvals10 = zvals(abs(prob-0.10)==min(abs(prob-0.10)));
   zcontours = [zvals10(1) zvals68(1) zvals95(1) zvals99(1)];
   %[C, h1] = contour(aa, epsilon, lik, zvals95);

   plotName = [plotDir '/likecontour'];
   figure;
   [C, h] = contour(Xq, Yq, Vq, zcontours(1),'k');
   hold on
   [C, h] = contour(Xq, Yq, Vq, zcontours(2),'b');
   [C, h] = contour(Xq, Yq, Vq, zcontours(3),'r');
   [C, h] = contour(Xq, Yq, Vq, zcontours(4),'m');
   hold off
   xlabel(xlab);
   ylabel(ylab);
   %cbar = colorbar;
   %set(get(cbar,'ylabel'),'String','Probability');

   bayesian_get_injplot(analysisType,dataSet);

   %legend('68 CL','95 CL','99 CL','True value','Location','northeast','Orientation','horizontal')
   legend('10 CL','68 CL','95 CL','99 CL','True value','Location','northeast','Orientation','horizontal')
   pretty;
   print('-dpng',plotName);
   print('-depsc2',plotName);
   close;

end




