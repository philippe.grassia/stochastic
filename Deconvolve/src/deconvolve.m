function cleanMap = deconvolve (b,dirtyMap)

% The deconvolve routine
% Currently uses MATLAB inbuilt or other third party routines
%
% cleanMap = deconvolve (b,dirtyMap)
% 
% b              Real matrix. Unnormalized beam matrix
% dirtyMap       Real vector. Raw/dirty/concolved map of the sky
%                Each component corresponds to a pixel
%
% cleanMap       Real vector. Estimated/clean/deconvolved map of the sky
%                Each component corresponds to a pixel

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


% Choose method below

% Simplest method
% cleanMap = inv(b) * (dirtyMap);
% cleanMap = inv(b'*b) * (b'*dirtyMap);

% MATLAB's inbuilt linear equation solver
% cleanMap = b \ dirtyMap;
% cleanMap = (b'*b) \ (b'*dirtyMap);

% Least square solver
% cleanMap = lsqr (b, dirtyMap);

% Symmetric LQ method
% cleanMap = symmlq (b'*b, b'*dirtyMap);

% Minimum residual solver
% cleanMap = minres (b'*b, b'*dirtyMap);
% cleanMap = qmr    (b'*b, b'*dirtyMap);
% cleanMap = gmres  (b'*b, b'*dirtyMap);

% Conjugate gradient iterative technique
% cleanMap = pcg      (b, dirtyMap);
% cleanMap = cgs      (b, dirtyMap);
% cleanMap = bicg     (b, dirtyMap);
% cleanMap = bicgstab (b, dirtyMap);

cleanMap = zeros(length(dirtyMap),1); cleanMap = minres(b,dirtyMap,[],20);

% cleanMap = bicgstab (b, dirtyMap,1e-8,5000);

% Use conjugate gradient with manually preconditioned beam matrix?


return
