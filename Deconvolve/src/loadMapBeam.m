function [ theta, phi, sourceMap, dirtyMap, cleanMap, variance, b ] = ...
    loadMapBeam (mapFile, beamFile)

% Load beam and maps from files and UNNORMALIZE
% Useful for deconvolution only works
%
% [ theta, phi, sourceMap, dirtyMap, cleanMap, variance, B] = ...
%   loadMapBeam (mapFile, beamFile)
%
% mapFile    String. File containing the maps
% beamFile   String. File containing the beam
%
% theta      Real vector. Polar angle of the pixels
% phi        Real vector. Azimuthal angle of the pixels
% sourceMap  Real vector. The injected map
% dirtyMap   Real vector. The raw map
% cleanMap   Real vector. The clean map
% variance   Real vector. Normalization constants
% b          Real matrix. The beam matrix

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


% Load pixels and maps
tic; fprintf ('\nLoading pixels and maps, unnormalizing dirty map ');
data = load(mapFile);

% Pixels
theta = data(:,1);
phi = data(:,2);

% Due to round off errors , pixels get shifted
% so need to correct for that
for iPixel = 1:length(theta)
    
    % 0 <= theta <= pi
    if (theta(iPixel) > pi)
        theta = pi;
    elseif (theta(iPixel) < 0)
        theta = 0.0;
    end

    % -pi <= phi < pi
    if (phi(iPixel) >= pi)
        phi(iPixel) = pi - eps(phi(iPixel)); % eps(X) is machine precision for X
    elseif (phi(iPixel) < -pi)
        phi(iPixel) = -pi;
    end

end

% Maps
sourceMap = data(:,3);
dirtyMap  = data(:,4)./data(:,6)/4; % UNNORMALIZE dirty map
cleanMap  = data(:,5);
variance  = data(:,6);

clear data;
fprintf ('DONE\n'); toc


% Load Beam
tic; fprintf ('\nLoading and unnormalizing beam ');
b = zeros (length(theta));
beamData = load(beamFile);
for idx = 1:length(beamData);
    b(beamData(idx,1),beamData(idx,2)) = beamData(idx,3)./beamData(idx,4)/4;
end
clear beamData;
fprintf ('DONE\n');
