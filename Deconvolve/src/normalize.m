function Lambda = normalize (Gamma, fSpectrum, deltaFreq, deltaTime)

% Calculate the normalization constant for the beam at each pixel
%
% Lambda = normalize (Gamma, fSpectrum, deltaFreq, deltaTime)
%
% Gamma      Real matrix. Combination of antenna patterns for all directions
%            and sideral times. Index 1: pixel, Index 2: sidereal time
%            *** CAUTION: Gamma is NOT the full overlap reduction function ***
% fSpectrum  Real matrix. Combination of the source & detector PSDs 
%            [GTilde(t,f) in text]. Index-1: Time, Index-2: frequency
% deltaTime  Real. Width of time bins in sec
% deltaFreq  Real. Width of frequency bins in Hz
%
% Lambda     Real vector. Normalization constant for each pixel

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


Lambda = zeros (length(Gamma(:,1)),1);


fSum = (2.0 * sum (fSpectrum,2));
tSum = (Gamma.*Gamma) * fSum;


Lambda = (1.0/deltaTime/deltaFreq) ./ tSum;


return
