function [ listPix ] = ...
   selectPixels (theta, phi, minTheta, maxTheta, minPhi, maxPhi)

% Select all the pixels within a given area bounded by latitude and longitudes
% The pixel indices are returned
%
% function [ listPix ] = ...
%  selectPixels (theta, phi, minTheta, maxTheta, minPhi, maxPhi)
%
% theta    Real vector. Theta of all the pixels
% phi      real vector. Phi of all the pixels
% minTheta Real. Minimum theta
% maxTheta Real. Maximum theta
% minPhi   Real. Minimum phi
% maxPhi   Real. Maximum phi
%
% listPix  Integer vector. List of pixel numbers falling in the range

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


% Option for crossing the meridian
if (maxPhi < minPhi)
   maxPhi = maxPhi + pi;
   minPhi = minPhi + pi;
   phi = phi + pi;
end


% If a pixel is inside the region, add its index to the list
count = 0;
for iPix = 1:length(theta)
   if ((minTheta <= theta(iPix) && theta(iPix) <= maxTheta) ...
         && (minPhi <= phi(iPix) && phi(iPix) <= maxPhi))
      count = count + 1;
      listPix (count) = iPix;
   end
end

