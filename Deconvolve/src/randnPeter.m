function [ rslt ] = randnPeter(Npnt)

% Use rand.m and use the Box-Muller method (Numerical Recipes, section 7.2)
% to transform to a gaussian distribution - by Peter Fritschel
%
% Npnt  Integer. Number of numbers in the sequence to be generated
%
% rslt  Real vector. The sequence of Gaussian random numbers

Ngood = 0;
while (Ngood < Npnt)
   w = 2 * rand(Npnt * 1.5, 2) - 1;
   s = w(:, 1).^2 + w(:,2).^2;
   n = find(s > 0 & s < 1.0);
   Ngood = length(n);
end
nUse = n(1:Npnt);
rslt = w(nUse,1) .* sqrt(-2 * log(s(nUse)) ./ s(nUse));
