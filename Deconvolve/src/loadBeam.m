function [ b, variance ] = loadMapBeam (beamFile)

% Load beam from file and UNNORMALIZE
% Useful for deconvolution only works
%
% [ b, variance] = loadBeam (beamFile)
%
% mapFile    String. File containing the maps
% beamFile   String. File containing the beam
%
% b          Real matrix. The beam matrix
% variance   Real vector. Normalization constants

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


% Load Beam
fprintf ('\nLoading and unnormalizing beam ');

beamData = load(beamFile);

b = zeros (sqrt(length(beamData)));
variance = zeros (sqrt(length(beamData)),1);

for idx = 1:length(beamData);
    variance(beamData(idx,1)) = beamData(idx,4);
    b(beamData(idx,1),beamData(idx,2)) = beamData(idx,3)/beamData(idx,4)/4;
end

clear beamData;

fprintf ('DONE\n');


return
