function b = ...
    beamMatrix (pixOmega, deltaTime, freqBins, fSpectrum, Gamma, detSep)

% Calculate the UNNORMALIZED beam matrix
%
% b = beamMatrix (pixOmega, deltaTime, freqBins, fSpectrum, Gamma, detSep)
%
% pixOmega  Real matrix. Array of direction cosines of pixels
%           Index 1: pixel number, Index 2: axis index
% deltaTime Real. Width of time bins.
% freqBins  Real vector. Position os frequency bins
% fSpectrum Real matrix. Combination of the source & detector PSDs 
%           [GTilde(t,f) in text]. Index-1: Time, Index-2: frequency
% Gamma     Real matrix. Combination of antenna patterns for all directions
%           and sideral times. Index 1: pixel, Index 2: sidereal time
%           *** CAUTION: Gamma is NOT the full overlap reduction function ***
% detSep    Real matrix. Detector separation vector at all sideral times
%           Index 1: Vector component, Index 2: sidereal time
%
% b         Real matrix. The beam matrix. Each index is a pixel number

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


% Need to think of a faster loop structure


deltaFreq = freqBins(2) - freqBins(1);


% First calculate unnormalized beam matrices - it is symmetric
% Calculating the lower triangular beam matrix
% A vectorizable loop would be better, but ...
% loops are non-uniform in size and we need to parallelize them
fprintf ('\n*** NOTE: Computation time increases proportionally with column index ***')
for iPixel = 1:length(pixOmega(1,:))
    fprintf ('\nProcessing %d of %d',iPixel,length(pixOmega(1,:)));
    for iPixel0 = 1:iPixel
        b(iPixel,iPixel0) = beamMatrixElement (iPixel, iPixel0, pixOmega, ...
            deltaTime, freqBins, fSpectrum, Gamma, detSep);
    end
end

% Copy the upper triangular matrix from the lower triangular one
fprintf ('\nFilling up symmetric upper triangle of beam matrix ');
for iPixel = 1:length(pixOmega(1,:))-1
    for iPixel0 = iPixel+1:length(pixOmega(1,:))
        b(iPixel,iPixel0) = b(iPixel0,iPixel);
    end
end
fprintf ('DONE\n');


return
