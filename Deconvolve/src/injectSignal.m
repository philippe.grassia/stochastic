function [ h1h2 ] = ...
    injectSignal (sourceMap, pixOmega, freqBins, H, deltaTime, detSep, Gamma)

% Generates the product h1*(t,f)h2(t,f) directly for the supplied
% source map of the sky. h1(t,f),h2(t,f) are not calculated individually
%
% [ h1h2 ] = ...
%   injectSignal (sourceMap, pixOmega, freqBins, H, deltaTime, detSep, Gamma)
%
% sourceMap  Real vector. Strengths of injected point sources at each pixel
% pixOmega   Real matrix. Array of direction cosines of pixels
%            Index 1: pixel number, Index 2: axis
% freqBins   Vector. Position of frequency bins
% H          Real vector. Frequency power spectrum of the source
% deltaTime  Real. Width of time bins in sec
% detSep     Real matrix. Detector separation vector at all sideral times
%            Index 1: Vector component, Index 2: sidereal time
% Gamma      Real matrix. Combination of antenna patterns for all directions
%            and sideral times. Index 1: pixel, Index 2: sidereal time
%            *** CAUTION: Gamma is NOT the full overlap reduction function ***
%
% h1h2       Complex matrix. Contains h1*(f)h2(f) at every time bin
%            Index 1: time, Index 2: frequency

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


h1h2 = complex (zeros(length(Gamma(1,:)),length(freqBins)), ...
                zeros(length(Gamma(1,:)),length(freqBins)));


velLight = 2.99792458e10;


% Inject point sources if sourceMap is supplied
if (length(sourceMap) == length(pixOmega(1,:)))
   for iPixel = 1:length(sourceMap)
       h1h2 = h1h2 + deltaTime*sourceMap(iPixel)*((H * Gamma(iPixel,:))' .* ...
           exp(2.0*pi*i*((detSep'*pixOmega(:,iPixel))*freqBins)/velLight));
   end
else
   fprintf ('\n*** Not injecting source map ***\n');
end

return
