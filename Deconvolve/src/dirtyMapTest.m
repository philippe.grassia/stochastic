function [ theta, phi, sourceMap, dirtyMap, variance ] = dirtyMapTest ...
    (detector1, detector2, pixelFile, inputMap, addNoise, mapFile, projType)

% Routine to compute beam pattern for a certain pointing direction (addNoise=0)
% OR to create a dirty map from simulated data with noise (addnoise!=0)
%
% [ theta, phi, sourceMap, dirtyMap, variance ] = dirtyMapTest ...
%    (detector1, detector2, pixelFile, inputMap, addNoise, mapFile, projType)
%
% detector1      String. Detector name 1 (like 'L1', 'H1', ...)
% detector2      String. Detector name 2 (like 'L1', 'H1', ...)
% pixelFile      String. File containing the pixel coordinates in radian
%                Column 1: longitude, Column 2: latitude
% inputMap       Real Matrix or String. Source map or filename
%                Column 1: pixel numbers, Column 2: corresponding strength
% addNoise       Real. Multiply noise by this factor.
%                NO noise is added if addNoise=0
% mapFile        String. File to write the maps to
% projType       String. Optional. If specified the skymaps will be plotted
%                using that projType as the projection type, e.g., 'mollweid'
%
% theta          Real vector. Polar angles of the pixels
% phi            Real vector. Azimuthal angles of the pixels
% sourceMap      Real vector. (Optional) Injected map of the sky
% dirtyMap       Real vector. Raw/dirty/concolved map of the sky
%                Each component corresponds to a pixel
% variance       Real vector. Variance at each pixel

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


% Open the output file first
mapFileID = fopen (mapFile, 'w');
if (mapFileID == -1)
    error(['Could not open file: ' mapFile]);
end


% Assuming integration for one day and 60sec chunk
% Assuming one day is 86400s, instead of 86164s
totalTime = 86400.0;
deltaTime = 192.0;
timeBins = deltaTime/2:deltaTime:totalTime;


% Assuming 512Hz upper cut frequency and 1Hz bin size
upperFreq=512.0;
deltaFreq=2.0;
freqBins = deltaFreq/2:deltaFreq:upperFreq;


% Load the pixel file
tic; fprintf ('\nLoading and processing pixels ');
pixels = load(pixelFile);

% Convert pixels to angles
theta = pi/2 - pixels(:,2);
phi = pixels(:,1);
% We use phi = [-pi,pi)
for iPixel=1:length(phi)
    if (phi(iPixel)) >= pi
        phi(iPixel) = phi(iPixel) - 2.0*pi;
    end
end

% Convert angles to direction cosines
pixOmega = [sin(theta').*cos(phi'); sin(theta').*sin(phi'); cos(theta')];

% Free memory
clear pixels;
fprintf('DONE\n'); toc;


% Get detector response matrices "d^{ab}_i".
%      If input argument is a detector/site name, retrieve this data by
%      calling LoadDetectorData.  If the input argument is a 3x3 numerical
%      array, use that for the detector response matrix.
tic; fprintf ('\nLoading and processing detector data ');
% Detector 1
if (ischar(detector1))
    DetData = LoadDetectorData(detector1);
    d1 = DetData.d;
    theta1 = (90-DetData.phi)*pi/180;
    phi1 = (DetData.lambda)*pi/180;
elseif (isnumeric(detector1) && isequal(size(detector1),[3 3]))
    % Keeping the option of a customized detector
    d1 = detector1.d;
    theta1 = (90-detector1.phi)*pi/180;
    phi1 = (detector1.lambda)*pi/180;
else
   error('Detector-1 not recognized. 4th argument should be a detector/site name.');  
end

% Detector 2
if (ischar(detector2))
    DetData = LoadDetectorData(detector2);
    d2 = DetData.d;
    theta2 = (90-DetData.phi)*pi/180;
    phi2 = (DetData.lambda)*pi/180;
elseif (isnumeric(detector2) && isequal(size(detector2),[3 3]))
    % Keeping the option of a customized detector
    d2 = detector2;
    theta2 = (90-detector2.phi)*pi/180;
    phi2 = (detector2.lambda)*pi/180;
else
    error('Detector-2 not recognized. 4th argument should be a detector/site name.');  
end

% Detector separation vector
earthRadius = 6.378137e8;
detSep0 = [ sin(theta2)*cos(phi2) - sin(theta1)*cos(phi1); ...
            sin(theta2)*sin(phi2) - sin(theta1)*sin(phi1); ...
            cos(theta2) - cos(theta1) ] * earthRadius;

fprintf('DONE\n'); toc;


% Load the source file or array if necessary
if (isnumeric(inputMap))
    sourceMap = inputMap;
else
    tic; fprintf ('\nLoading source map ');
    sourceData = load(inputMap);
    sourcePixel = sourceData(:,1);
    sourceSNR = sourceData(:,2); % Not really SNR... proportional to SNR
    % If a overall scaling factor is supplied
    if (nargin > 8 && isnumeric(strength))
        sourceSNR = strength*sourceSNR;
    end
    
    % Check the source injection points
    if (any(sourcePixel) <= 0 || any(sourcePixel) > length(theta))
        error(['One or more pixel indices are invalid' sprintf('%d ',sourcePixel)]);
    else
        fprintf ('\nInjection chart:\n');
        fprintf ('Pixel # %d,  Lat = %+7.2fdeg,  Long = %+7.2fdeg,  Strength = %e\n', ...
        [sourcePixel';90.0-theta(sourcePixel)'*180/pi;phi(sourcePixel)'*180/pi;sourceSNR']);
    end
    
    % Make a map of sources
    sourceMap = zeros(length(theta),1);
    % Source pixels can be repeated, be careful!!
    % sourceMap(sourcePixel) = sourceMap(sourcePixel) + sourceSNR;
    for iSourcePixel = 1:length(sourcePixel)
        sourceMap(sourcePixel(iSourcePixel)) = ...
        sourceMap(sourcePixel(iSourcePixel)) + sourceSNR(iSourcePixel);
    end
    
    % Free memory
    clear sourceData;
    clear sourcePixel;
    clear sourceSNR;
    
    fprintf('DONE\n'); toc;
end


% If all the above inputs are read successfully...
% check the remaining arguments before starting costly computation

% Check if plotting is enabled
if (nargin > 6 && ischar(projType))
    fprintf ('\nMap will be plotted using projection type: %s\n\n',projType);
else
    fprintf ('\nPlotting is disabled\n\n');
end
        

% Calculate the Euler rotation matrices at all times
tic; fprintf ('\nConstructing Euler rotation matrices ');
EulerZ = EulerRotationZ (timeBins);
fprintf('DONE\n'); toc;


% Get the detector separation vector at all times
tic; fprintf ('\nConstructing detector separation time array ');
detSep = rotateDetSep (detSep0, EulerZ);
fprintf('DONE\n'); toc;


% Get the overlap function Gamma for each pixel and time bin
% CAUTION: NOT the overlap reduction function "small" gamma
tic; fprintf ('\nConstructing capital Gamma ');
Gamma = overlap (theta, phi, EulerZ, d1, d2);
fprintf('DONE\n'); toc;


% Get data, noise PSDs
% Getting s1s2 is neccessary if simulate flag is on
tic; fprintf ('\nGenerate/read data, get noise PSDs');
[ s1s2,P1,P2,invP1,invP2 ] = getData(detector1,detector2,timeBins,freqBins);
fprintf('DONE\n'); toc;


% Get source PSD
[ H, invH ] = sourcePSD (freqBins);

% Inject sources in the source map - it's fast
% can inject a full sky instead of only the injection points

% Generating cross-correlation signal
tic; fprintf ('\nGetting and injecting signal in data, getting source PSD ');
[ h1h2 ] = ...
    injectSignal (sourceMap, pixOmega, freqBins,H,deltaTime, detSep, Gamma);

% Inject signal in data if addNoise is not zero
% s1*(f) s2(f) = <h1*(f) h2(f)> + [pure detector noise n(f)]
% noise due to stochastic nature of h(f) is neglected, as, statistically:
% h1*(f) h2(f) + n1*(f) h2(f) + h1*(f) n2(f) << n1*(f) n2(f)
s1s2 = h1h2 + addNoise*s1s2;

fprintf('DONE\n'); toc;


% Precompute the spectral info
tic; fprintf ('\nPrecomputing spectral info ');
fSpectrum = repmat((H.*H)',length(timeBins),1).*(invP1.*invP2);
fprintf('DONE\n'); toc;


% Get normalization constant vector
tic; fprintf ('\nComputing normalization coefficients ');
variance = normalize (Gamma, fSpectrum, deltaFreq, deltaTime)/4;
fprintf('DONE\n'); toc;


% Make unnormalized "dirty" map
% Map will be normalized later, efficient for deconvolution
tic; fprintf ('\nMaking UNNORMALIZED dirty map ');
dirtyMap = makeRawMap (s1s2, pixOmega, freqBins, detSep, Gamma, H, invP1,invP2);
fprintf('DONE\n'); toc;


% Write maps to mapFile
tic; fprintf ('\nWriting map file ');
fprintf (mapFileID,'%f\t%f\t%e\t%e\t%e\n', ...
[ theta'; phi'; sourceMap'; (4*variance.*dirtyMap)'; variance' ]);
fclose(mapFileID);
fprintf('DONE\n'); toc;


% If 7th argument is specified plot the maps
if (nargin > 6 && ischar(projType))
    
    % Plot the source map
    figure;
    xproject ([theta,phi], sourceMap, projType);
    grid off;
    axis ('off');
    colorbar('SouthOutside');
    
    % Plot the dirty map
    figure;
    xproject ([theta,phi], 4*variance.*dirtyMap, projType);
    grid off;
    axis ('off');
    colorbar('SouthOutside');
    
end

return
