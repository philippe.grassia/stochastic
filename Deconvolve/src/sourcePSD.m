function [ H, invH ] = sourcePSD (freqBins)

% Returns the arrays containing H(f) and 1/H(f)
%
% [ H, invH ] = sourcePSD (freqBins)
%
% freqBins   Vector. Position of frequency bins
%
% H          Real vector. Frequency power spectrum of the source
% invH       Real vector. Inverse of frequency power spectrum of the source

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


H = zeros (length(freqBins),1);
invH = zeros (length(freqBins),1);


% Using a power law source PSD

%alpha = -3;
%HAlpha = 3.0 * (1.1e-28*velLight*0.7)^2 / 32.0 / pi^3;

alpha = 0;
HAlpha = 5e-47;

H = HAlpha*((freqBins'/100.0) .^ alpha);
invH = 1.0 ./ H;

% Use true astrophysical PSD generated by Tania?


return
