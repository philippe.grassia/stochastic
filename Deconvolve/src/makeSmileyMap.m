function sourceMap = makeSmileyMap (theta, phi)

% Make a smiley looking map of the sky, which can be used as injected map
%
% sourceMap = makeSmileyMap (theta, phi)
%
% theta     Real vector. Polar angles of the pixels
% phi       Real vector. Azimuthal angles of the pixels
%
% sourceMap Real vector. Returned smiley map

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%


% Input for lips
theta0 = 1.0;
phi0 = 0.0;
rad = 1.5;
thick=0.1;
width=1.2;

% Input for eyes
eyePos = 1.0;
eyeSep = 0.8;
eyeRad = 0.2;

% Input for nose
noseLength = 0.35;
noseHeight = 0.65;
noseWidth = 0.05;


sourceMap = zeros(length(theta),1);


% Not so efficient but clean code
for iPix = 1:length(theta)
    % Lips
    distance = norm([theta(iPix)-theta0, phi(iPix)-phi0]);
    if ( abs(distance-rad) <= thick && ...
            abs(phi(iPix)-phi0) <= width && theta(iPix) >= theta0)
        sourceMap(iPix) = 1;
    end
    
    % Eyes
    leftDist = norm([theta(iPix)-eyePos, phi(iPix)-(phi0-eyeSep)]);
    rightDist = norm([theta(iPix)-eyePos, phi(iPix)-(phi0+eyeSep)]);
    if (leftDist <= eyeRad || rightDist <= eyeRad)
        sourceMap(iPix) = 1;
    end
    
    % Nose
    noseDist = norm([theta(iPix)-theta0-noseHeight, phi(iPix)-phi0]);
    if ( abs(phi(iPix)-phi0) <= noseWidth && noseDist <= noseLength)
        sourceMap(iPix) = 1.5;
    end
end


return;
