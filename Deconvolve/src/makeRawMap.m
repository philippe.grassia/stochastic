function dirtyMap = ...
    makeRawMap (s1s2, pixOmega, freqBins, detSep, Gamma, H, invP1, invP2)

% Make skymap from the Fourier transforms of chunks of time series data
% using the directed (radiometer) search algorithm
%
% dirtyMap = ...
%  makeRawMap (s1s2, pixOmega, freqBins, detSep, Gamma, H, invP1, invP2)
%
% s1s2        Complex matrix. s1*(f) s2(f)
%             Index 1: sidereal time, Index 2: frequency
% pixOmega    Real matrix. Array of direction cosines of pixels
%             Index 1: pixel number, Index 2: axis
% freqBins    Real vector. Position os frequency bins
% detSep      Real matrix. Detector separation vector at all sideral times
%             Index 1: Vector component, Index 2: sidereal time
% Gamma       Real matrix. Combination of antenna patterns for all directions
%             and sideral times. Index 1: pixel, Index 2: sidereal time
%             ***CAUTION: Gamma is NOT the full overlap reduction function***
% H           Real vector. Frequency power spectrum of the source
% invP1,invP2 Real matrices. Inverse of PSD of detectors at different times.
%             Index 1: time, Index 2: frequency
%
% dirtyMap    Real vector. Raw correlation values at each pixel

%
% Author: Sanjit Mitra <sanjit.mitra@ligo.org>
%

dirtyMap = zeros(length(pixOmega(1,:)),1);


deltaFreq = freqBins(2)-freqBins(1);

velLight = 2.99792458e10;


for iPixel = 1:length(pixOmega(1,:))
    fSum(:,iPixel) = 2.0 * ((invP1 .* invP2) .* ...
        real(s1s2 .* exp(-2.0*pi*i*((detSep'*pixOmega(:,iPixel))*freqBins)/velLight))) * H;
end

tSum = sum ((fSum' .* Gamma),2);


dirtyMap = deltaFreq * tSum;


return
